package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Account_reconcile_model_templateDTO]
 */
@Data
public class Account_reconcile_model_templateDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MATCH_AMOUNT_MIN]
     *
     */
    @JSONField(name = "match_amount_min")
    @JsonProperty("match_amount_min")
    private Double matchAmountMin;

    /**
     * 属性 [FORCE_TAX_INCLUDED]
     *
     */
    @JSONField(name = "force_tax_included")
    @JsonProperty("force_tax_included")
    private Boolean forceTaxIncluded;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [MATCH_PARTNER_CATEGORY_IDS]
     *
     */
    @JSONField(name = "match_partner_category_ids")
    @JsonProperty("match_partner_category_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String matchPartnerCategoryIds;

    /**
     * 属性 [SECOND_AMOUNT_TYPE]
     *
     */
    @JSONField(name = "second_amount_type")
    @JsonProperty("second_amount_type")
    @NotBlank(message = "[第二金额类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String secondAmountType;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    @NotNull(message = "[核销金额]不允许为空!")
    private Double amount;

    /**
     * 属性 [MATCH_PARTNER_IDS]
     *
     */
    @JSONField(name = "match_partner_ids")
    @JsonProperty("match_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String matchPartnerIds;

    /**
     * 属性 [MATCH_JOURNAL_IDS]
     *
     */
    @JSONField(name = "match_journal_ids")
    @JsonProperty("match_journal_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String matchJournalIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [LABEL]
     *
     */
    @JSONField(name = "label")
    @JsonProperty("label")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String label;

    /**
     * 属性 [MATCH_TOTAL_AMOUNT_PARAM]
     *
     */
    @JSONField(name = "match_total_amount_param")
    @JsonProperty("match_total_amount_param")
    private Double matchTotalAmountParam;

    /**
     * 属性 [RULE_TYPE]
     *
     */
    @JSONField(name = "rule_type")
    @JsonProperty("rule_type")
    @NotBlank(message = "[类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String ruleType;

    /**
     * 属性 [MATCH_AMOUNT]
     *
     */
    @JSONField(name = "match_amount")
    @JsonProperty("match_amount")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String matchAmount;

    /**
     * 属性 [SECOND_AMOUNT]
     *
     */
    @JSONField(name = "second_amount")
    @JsonProperty("second_amount")
    @NotNull(message = "[第二核销金额]不允许为空!")
    private Double secondAmount;

    /**
     * 属性 [AMOUNT_TYPE]
     *
     */
    @JSONField(name = "amount_type")
    @JsonProperty("amount_type")
    @NotBlank(message = "[金额类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String amountType;

    /**
     * 属性 [MATCH_SAME_CURRENCY]
     *
     */
    @JSONField(name = "match_same_currency")
    @JsonProperty("match_same_currency")
    private Boolean matchSameCurrency;

    /**
     * 属性 [MATCH_AMOUNT_MAX]
     *
     */
    @JSONField(name = "match_amount_max")
    @JsonProperty("match_amount_max")
    private Double matchAmountMax;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[按钮标签]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [AUTO_RECONCILE]
     *
     */
    @JSONField(name = "auto_reconcile")
    @JsonProperty("auto_reconcile")
    private Boolean autoReconcile;

    /**
     * 属性 [MATCH_TOTAL_AMOUNT]
     *
     */
    @JSONField(name = "match_total_amount")
    @JsonProperty("match_total_amount")
    private Boolean matchTotalAmount;

    /**
     * 属性 [HAS_SECOND_LINE]
     *
     */
    @JSONField(name = "has_second_line")
    @JsonProperty("has_second_line")
    private Boolean hasSecondLine;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    @NotNull(message = "[序号]不允许为空!")
    private Integer sequence;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [MATCH_NATURE]
     *
     */
    @JSONField(name = "match_nature")
    @JsonProperty("match_nature")
    @NotBlank(message = "[数量性质]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String matchNature;

    /**
     * 属性 [SECOND_LABEL]
     *
     */
    @JSONField(name = "second_label")
    @JsonProperty("second_label")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String secondLabel;

    /**
     * 属性 [FORCE_SECOND_TAX_INCLUDED]
     *
     */
    @JSONField(name = "force_second_tax_included")
    @JsonProperty("force_second_tax_included")
    private Boolean forceSecondTaxIncluded;

    /**
     * 属性 [MATCH_LABEL_PARAM]
     *
     */
    @JSONField(name = "match_label_param")
    @JsonProperty("match_label_param")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String matchLabelParam;

    /**
     * 属性 [MATCH_LABEL]
     *
     */
    @JSONField(name = "match_label")
    @JsonProperty("match_label")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String matchLabel;

    /**
     * 属性 [MATCH_PARTNER]
     *
     */
    @JSONField(name = "match_partner")
    @JsonProperty("match_partner")
    private Boolean matchPartner;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [TAX_ID_TEXT]
     *
     */
    @JSONField(name = "tax_id_text")
    @JsonProperty("tax_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String taxIdText;

    /**
     * 属性 [SECOND_TAX_ID_TEXT]
     *
     */
    @JSONField(name = "second_tax_id_text")
    @JsonProperty("second_tax_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String secondTaxIdText;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accountIdText;

    /**
     * 属性 [CHART_TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "chart_template_id_text")
    @JsonProperty("chart_template_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String chartTemplateIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [SECOND_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "second_account_id_text")
    @JsonProperty("second_account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String secondAccountIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [SECOND_ACCOUNT_ID]
     *
     */
    @JSONField(name = "second_account_id")
    @JsonProperty("second_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long secondAccountId;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long accountId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CHART_TEMPLATE_ID]
     *
     */
    @JSONField(name = "chart_template_id")
    @JsonProperty("chart_template_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[表模板]不允许为空!")
    private Long chartTemplateId;

    /**
     * 属性 [SECOND_TAX_ID]
     *
     */
    @JSONField(name = "second_tax_id")
    @JsonProperty("second_tax_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long secondTaxId;

    /**
     * 属性 [TAX_ID]
     *
     */
    @JSONField(name = "tax_id")
    @JsonProperty("tax_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long taxId;


    /**
     * 设置 [MATCH_AMOUNT_MIN]
     */
    public void setMatchAmountMin(Double  matchAmountMin){
        this.matchAmountMin = matchAmountMin ;
        this.modify("match_amount_min",matchAmountMin);
    }

    /**
     * 设置 [FORCE_TAX_INCLUDED]
     */
    public void setForceTaxIncluded(Boolean  forceTaxIncluded){
        this.forceTaxIncluded = forceTaxIncluded ;
        this.modify("force_tax_included",forceTaxIncluded);
    }

    /**
     * 设置 [SECOND_AMOUNT_TYPE]
     */
    public void setSecondAmountType(String  secondAmountType){
        this.secondAmountType = secondAmountType ;
        this.modify("second_amount_type",secondAmountType);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [LABEL]
     */
    public void setLabel(String  label){
        this.label = label ;
        this.modify("label",label);
    }

    /**
     * 设置 [MATCH_TOTAL_AMOUNT_PARAM]
     */
    public void setMatchTotalAmountParam(Double  matchTotalAmountParam){
        this.matchTotalAmountParam = matchTotalAmountParam ;
        this.modify("match_total_amount_param",matchTotalAmountParam);
    }

    /**
     * 设置 [RULE_TYPE]
     */
    public void setRuleType(String  ruleType){
        this.ruleType = ruleType ;
        this.modify("rule_type",ruleType);
    }

    /**
     * 设置 [MATCH_AMOUNT]
     */
    public void setMatchAmount(String  matchAmount){
        this.matchAmount = matchAmount ;
        this.modify("match_amount",matchAmount);
    }

    /**
     * 设置 [SECOND_AMOUNT]
     */
    public void setSecondAmount(Double  secondAmount){
        this.secondAmount = secondAmount ;
        this.modify("second_amount",secondAmount);
    }

    /**
     * 设置 [AMOUNT_TYPE]
     */
    public void setAmountType(String  amountType){
        this.amountType = amountType ;
        this.modify("amount_type",amountType);
    }

    /**
     * 设置 [MATCH_SAME_CURRENCY]
     */
    public void setMatchSameCurrency(Boolean  matchSameCurrency){
        this.matchSameCurrency = matchSameCurrency ;
        this.modify("match_same_currency",matchSameCurrency);
    }

    /**
     * 设置 [MATCH_AMOUNT_MAX]
     */
    public void setMatchAmountMax(Double  matchAmountMax){
        this.matchAmountMax = matchAmountMax ;
        this.modify("match_amount_max",matchAmountMax);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [AUTO_RECONCILE]
     */
    public void setAutoReconcile(Boolean  autoReconcile){
        this.autoReconcile = autoReconcile ;
        this.modify("auto_reconcile",autoReconcile);
    }

    /**
     * 设置 [MATCH_TOTAL_AMOUNT]
     */
    public void setMatchTotalAmount(Boolean  matchTotalAmount){
        this.matchTotalAmount = matchTotalAmount ;
        this.modify("match_total_amount",matchTotalAmount);
    }

    /**
     * 设置 [HAS_SECOND_LINE]
     */
    public void setHasSecondLine(Boolean  hasSecondLine){
        this.hasSecondLine = hasSecondLine ;
        this.modify("has_second_line",hasSecondLine);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [MATCH_NATURE]
     */
    public void setMatchNature(String  matchNature){
        this.matchNature = matchNature ;
        this.modify("match_nature",matchNature);
    }

    /**
     * 设置 [SECOND_LABEL]
     */
    public void setSecondLabel(String  secondLabel){
        this.secondLabel = secondLabel ;
        this.modify("second_label",secondLabel);
    }

    /**
     * 设置 [FORCE_SECOND_TAX_INCLUDED]
     */
    public void setForceSecondTaxIncluded(Boolean  forceSecondTaxIncluded){
        this.forceSecondTaxIncluded = forceSecondTaxIncluded ;
        this.modify("force_second_tax_included",forceSecondTaxIncluded);
    }

    /**
     * 设置 [MATCH_LABEL_PARAM]
     */
    public void setMatchLabelParam(String  matchLabelParam){
        this.matchLabelParam = matchLabelParam ;
        this.modify("match_label_param",matchLabelParam);
    }

    /**
     * 设置 [MATCH_LABEL]
     */
    public void setMatchLabel(String  matchLabel){
        this.matchLabel = matchLabel ;
        this.modify("match_label",matchLabel);
    }

    /**
     * 设置 [MATCH_PARTNER]
     */
    public void setMatchPartner(Boolean  matchPartner){
        this.matchPartner = matchPartner ;
        this.modify("match_partner",matchPartner);
    }

    /**
     * 设置 [SECOND_ACCOUNT_ID]
     */
    public void setSecondAccountId(Long  secondAccountId){
        this.secondAccountId = secondAccountId ;
        this.modify("second_account_id",secondAccountId);
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    public void setAccountId(Long  accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [CHART_TEMPLATE_ID]
     */
    public void setChartTemplateId(Long  chartTemplateId){
        this.chartTemplateId = chartTemplateId ;
        this.modify("chart_template_id",chartTemplateId);
    }

    /**
     * 设置 [SECOND_TAX_ID]
     */
    public void setSecondTaxId(Long  secondTaxId){
        this.secondTaxId = secondTaxId ;
        this.modify("second_tax_id",secondTaxId);
    }

    /**
     * 设置 [TAX_ID]
     */
    public void setTaxId(Long  taxId){
        this.taxId = taxId ;
        this.modify("tax_id",taxId);
    }


}


