package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_aliasService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_aliasSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"EMail别名" })
@RestController("Core-mail_alias")
@RequestMapping("")
public class Mail_aliasResource {

    @Autowired
    public IMail_aliasService mail_aliasService;

    @Autowired
    @Lazy
    public Mail_aliasMapping mail_aliasMapping;

    @PreAuthorize("hasPermission(this.mail_aliasMapping.toDomain(#mail_aliasdto),'iBizBusinessCentral-Mail_alias-Create')")
    @ApiOperation(value = "新建EMail别名", tags = {"EMail别名" },  notes = "新建EMail别名")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_aliases")
    public ResponseEntity<Mail_aliasDTO> create(@Validated @RequestBody Mail_aliasDTO mail_aliasdto) {
        Mail_alias domain = mail_aliasMapping.toDomain(mail_aliasdto);
		mail_aliasService.create(domain);
        Mail_aliasDTO dto = mail_aliasMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_aliasMapping.toDomain(#mail_aliasdtos),'iBizBusinessCentral-Mail_alias-Create')")
    @ApiOperation(value = "批量新建EMail别名", tags = {"EMail别名" },  notes = "批量新建EMail别名")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_aliases/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_aliasDTO> mail_aliasdtos) {
        mail_aliasService.createBatch(mail_aliasMapping.toDomain(mail_aliasdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_alias" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_aliasService.get(#mail_alias_id),'iBizBusinessCentral-Mail_alias-Update')")
    @ApiOperation(value = "更新EMail别名", tags = {"EMail别名" },  notes = "更新EMail别名")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_aliases/{mail_alias_id}")
    public ResponseEntity<Mail_aliasDTO> update(@PathVariable("mail_alias_id") Long mail_alias_id, @RequestBody Mail_aliasDTO mail_aliasdto) {
		Mail_alias domain  = mail_aliasMapping.toDomain(mail_aliasdto);
        domain .setId(mail_alias_id);
		mail_aliasService.update(domain );
		Mail_aliasDTO dto = mail_aliasMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_aliasService.getMailAliasByEntities(this.mail_aliasMapping.toDomain(#mail_aliasdtos)),'iBizBusinessCentral-Mail_alias-Update')")
    @ApiOperation(value = "批量更新EMail别名", tags = {"EMail别名" },  notes = "批量更新EMail别名")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_aliases/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_aliasDTO> mail_aliasdtos) {
        mail_aliasService.updateBatch(mail_aliasMapping.toDomain(mail_aliasdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_aliasService.get(#mail_alias_id),'iBizBusinessCentral-Mail_alias-Remove')")
    @ApiOperation(value = "删除EMail别名", tags = {"EMail别名" },  notes = "删除EMail别名")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_aliases/{mail_alias_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_alias_id") Long mail_alias_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_aliasService.remove(mail_alias_id));
    }

    @PreAuthorize("hasPermission(this.mail_aliasService.getMailAliasByIds(#ids),'iBizBusinessCentral-Mail_alias-Remove')")
    @ApiOperation(value = "批量删除EMail别名", tags = {"EMail别名" },  notes = "批量删除EMail别名")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_aliases/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_aliasService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_aliasMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_alias-Get')")
    @ApiOperation(value = "获取EMail别名", tags = {"EMail别名" },  notes = "获取EMail别名")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_aliases/{mail_alias_id}")
    public ResponseEntity<Mail_aliasDTO> get(@PathVariable("mail_alias_id") Long mail_alias_id) {
        Mail_alias domain = mail_aliasService.get(mail_alias_id);
        Mail_aliasDTO dto = mail_aliasMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取EMail别名草稿", tags = {"EMail别名" },  notes = "获取EMail别名草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_aliases/getdraft")
    public ResponseEntity<Mail_aliasDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_aliasMapping.toDto(mail_aliasService.getDraft(new Mail_alias())));
    }

    @ApiOperation(value = "检查EMail别名", tags = {"EMail别名" },  notes = "检查EMail别名")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_aliases/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_aliasDTO mail_aliasdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_aliasService.checkKey(mail_aliasMapping.toDomain(mail_aliasdto)));
    }

    @PreAuthorize("hasPermission(this.mail_aliasMapping.toDomain(#mail_aliasdto),'iBizBusinessCentral-Mail_alias-Save')")
    @ApiOperation(value = "保存EMail别名", tags = {"EMail别名" },  notes = "保存EMail别名")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_aliases/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_aliasDTO mail_aliasdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_aliasService.save(mail_aliasMapping.toDomain(mail_aliasdto)));
    }

    @PreAuthorize("hasPermission(this.mail_aliasMapping.toDomain(#mail_aliasdtos),'iBizBusinessCentral-Mail_alias-Save')")
    @ApiOperation(value = "批量保存EMail别名", tags = {"EMail别名" },  notes = "批量保存EMail别名")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_aliases/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_aliasDTO> mail_aliasdtos) {
        mail_aliasService.saveBatch(mail_aliasMapping.toDomain(mail_aliasdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_alias-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_alias-Get')")
	@ApiOperation(value = "获取数据集", tags = {"EMail别名" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_aliases/fetchdefault")
	public ResponseEntity<List<Mail_aliasDTO>> fetchDefault(Mail_aliasSearchContext context) {
        Page<Mail_alias> domains = mail_aliasService.searchDefault(context) ;
        List<Mail_aliasDTO> list = mail_aliasMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_alias-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_alias-Get')")
	@ApiOperation(value = "查询数据集", tags = {"EMail别名" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_aliases/searchdefault")
	public ResponseEntity<Page<Mail_aliasDTO>> searchDefault(@RequestBody Mail_aliasSearchContext context) {
        Page<Mail_alias> domains = mail_aliasService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_aliasMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

