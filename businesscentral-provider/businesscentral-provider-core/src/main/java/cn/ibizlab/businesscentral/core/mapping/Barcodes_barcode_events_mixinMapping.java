package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin;
import cn.ibizlab.businesscentral.core.dto.Barcodes_barcode_events_mixinDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreBarcodes_barcode_events_mixinMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Barcodes_barcode_events_mixinMapping extends MappingBase<Barcodes_barcode_events_mixinDTO, Barcodes_barcode_events_mixin> {


}

