package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconciliation_widget;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_reconciliation_widgetService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_reconciliation_widgetSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"会计核销挂账" })
@RestController("Core-account_reconciliation_widget")
@RequestMapping("")
public class Account_reconciliation_widgetResource {

    @Autowired
    public IAccount_reconciliation_widgetService account_reconciliation_widgetService;

    @Autowired
    @Lazy
    public Account_reconciliation_widgetMapping account_reconciliation_widgetMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconciliation_widget-Create-all')")
    @ApiOperation(value = "新建会计核销挂账", tags = {"会计核销挂账" },  notes = "新建会计核销挂账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconciliation_widgets")
    public ResponseEntity<Account_reconciliation_widgetDTO> create(@Validated @RequestBody Account_reconciliation_widgetDTO account_reconciliation_widgetdto) {
        Account_reconciliation_widget domain = account_reconciliation_widgetMapping.toDomain(account_reconciliation_widgetdto);
		account_reconciliation_widgetService.create(domain);
        Account_reconciliation_widgetDTO dto = account_reconciliation_widgetMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconciliation_widget-Create-all')")
    @ApiOperation(value = "批量新建会计核销挂账", tags = {"会计核销挂账" },  notes = "批量新建会计核销挂账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconciliation_widgets/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_reconciliation_widgetDTO> account_reconciliation_widgetdtos) {
        account_reconciliation_widgetService.createBatch(account_reconciliation_widgetMapping.toDomain(account_reconciliation_widgetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconciliation_widget-Update-all')")
    @ApiOperation(value = "更新会计核销挂账", tags = {"会计核销挂账" },  notes = "更新会计核销挂账")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_reconciliation_widgets/{account_reconciliation_widget_id}")
    public ResponseEntity<Account_reconciliation_widgetDTO> update(@PathVariable("account_reconciliation_widget_id") Long account_reconciliation_widget_id, @RequestBody Account_reconciliation_widgetDTO account_reconciliation_widgetdto) {
		Account_reconciliation_widget domain  = account_reconciliation_widgetMapping.toDomain(account_reconciliation_widgetdto);
        domain .setId(account_reconciliation_widget_id);
		account_reconciliation_widgetService.update(domain );
		Account_reconciliation_widgetDTO dto = account_reconciliation_widgetMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconciliation_widget-Update-all')")
    @ApiOperation(value = "批量更新会计核销挂账", tags = {"会计核销挂账" },  notes = "批量更新会计核销挂账")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_reconciliation_widgets/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_reconciliation_widgetDTO> account_reconciliation_widgetdtos) {
        account_reconciliation_widgetService.updateBatch(account_reconciliation_widgetMapping.toDomain(account_reconciliation_widgetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconciliation_widget-Remove-all')")
    @ApiOperation(value = "删除会计核销挂账", tags = {"会计核销挂账" },  notes = "删除会计核销挂账")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_reconciliation_widgets/{account_reconciliation_widget_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_reconciliation_widget_id") Long account_reconciliation_widget_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_reconciliation_widgetService.remove(account_reconciliation_widget_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconciliation_widget-Remove-all')")
    @ApiOperation(value = "批量删除会计核销挂账", tags = {"会计核销挂账" },  notes = "批量删除会计核销挂账")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_reconciliation_widgets/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_reconciliation_widgetService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconciliation_widget-Get-all')")
    @ApiOperation(value = "获取会计核销挂账", tags = {"会计核销挂账" },  notes = "获取会计核销挂账")
	@RequestMapping(method = RequestMethod.GET, value = "/account_reconciliation_widgets/{account_reconciliation_widget_id}")
    public ResponseEntity<Account_reconciliation_widgetDTO> get(@PathVariable("account_reconciliation_widget_id") Long account_reconciliation_widget_id) {
        Account_reconciliation_widget domain = account_reconciliation_widgetService.get(account_reconciliation_widget_id);
        Account_reconciliation_widgetDTO dto = account_reconciliation_widgetMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取会计核销挂账草稿", tags = {"会计核销挂账" },  notes = "获取会计核销挂账草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_reconciliation_widgets/getdraft")
    public ResponseEntity<Account_reconciliation_widgetDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_reconciliation_widgetMapping.toDto(account_reconciliation_widgetService.getDraft(new Account_reconciliation_widget())));
    }

    @ApiOperation(value = "检查会计核销挂账", tags = {"会计核销挂账" },  notes = "检查会计核销挂账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconciliation_widgets/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_reconciliation_widgetDTO account_reconciliation_widgetdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_reconciliation_widgetService.checkKey(account_reconciliation_widgetMapping.toDomain(account_reconciliation_widgetdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconciliation_widget-Save-all')")
    @ApiOperation(value = "保存会计核销挂账", tags = {"会计核销挂账" },  notes = "保存会计核销挂账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconciliation_widgets/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_reconciliation_widgetDTO account_reconciliation_widgetdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_reconciliation_widgetService.save(account_reconciliation_widgetMapping.toDomain(account_reconciliation_widgetdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconciliation_widget-Save-all')")
    @ApiOperation(value = "批量保存会计核销挂账", tags = {"会计核销挂账" },  notes = "批量保存会计核销挂账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconciliation_widgets/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_reconciliation_widgetDTO> account_reconciliation_widgetdtos) {
        account_reconciliation_widgetService.saveBatch(account_reconciliation_widgetMapping.toDomain(account_reconciliation_widgetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconciliation_widget-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"会计核销挂账" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_reconciliation_widgets/fetchdefault")
	public ResponseEntity<List<Account_reconciliation_widgetDTO>> fetchDefault(Account_reconciliation_widgetSearchContext context) {
        Page<Account_reconciliation_widget> domains = account_reconciliation_widgetService.searchDefault(context) ;
        List<Account_reconciliation_widgetDTO> list = account_reconciliation_widgetMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_reconciliation_widget-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"会计核销挂账" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_reconciliation_widgets/searchdefault")
	public ResponseEntity<Page<Account_reconciliation_widgetDTO>> searchDefault(@RequestBody Account_reconciliation_widgetSearchContext context) {
        Page<Account_reconciliation_widget> domains = account_reconciliation_widgetService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_reconciliation_widgetMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

