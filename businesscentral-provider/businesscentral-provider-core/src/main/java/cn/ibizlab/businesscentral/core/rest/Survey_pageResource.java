package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_page;
import cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_pageService;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_pageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"问卷页面" })
@RestController("Core-survey_page")
@RequestMapping("")
public class Survey_pageResource {

    @Autowired
    public ISurvey_pageService survey_pageService;

    @Autowired
    @Lazy
    public Survey_pageMapping survey_pageMapping;

    @PreAuthorize("hasPermission(this.survey_pageMapping.toDomain(#survey_pagedto),'iBizBusinessCentral-Survey_page-Create')")
    @ApiOperation(value = "新建问卷页面", tags = {"问卷页面" },  notes = "新建问卷页面")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_pages")
    public ResponseEntity<Survey_pageDTO> create(@Validated @RequestBody Survey_pageDTO survey_pagedto) {
        Survey_page domain = survey_pageMapping.toDomain(survey_pagedto);
		survey_pageService.create(domain);
        Survey_pageDTO dto = survey_pageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_pageMapping.toDomain(#survey_pagedtos),'iBizBusinessCentral-Survey_page-Create')")
    @ApiOperation(value = "批量新建问卷页面", tags = {"问卷页面" },  notes = "批量新建问卷页面")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_pages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_pageDTO> survey_pagedtos) {
        survey_pageService.createBatch(survey_pageMapping.toDomain(survey_pagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "survey_page" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.survey_pageService.get(#survey_page_id),'iBizBusinessCentral-Survey_page-Update')")
    @ApiOperation(value = "更新问卷页面", tags = {"问卷页面" },  notes = "更新问卷页面")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_pages/{survey_page_id}")
    public ResponseEntity<Survey_pageDTO> update(@PathVariable("survey_page_id") Long survey_page_id, @RequestBody Survey_pageDTO survey_pagedto) {
		Survey_page domain  = survey_pageMapping.toDomain(survey_pagedto);
        domain .setId(survey_page_id);
		survey_pageService.update(domain );
		Survey_pageDTO dto = survey_pageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_pageService.getSurveyPageByEntities(this.survey_pageMapping.toDomain(#survey_pagedtos)),'iBizBusinessCentral-Survey_page-Update')")
    @ApiOperation(value = "批量更新问卷页面", tags = {"问卷页面" },  notes = "批量更新问卷页面")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_pages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_pageDTO> survey_pagedtos) {
        survey_pageService.updateBatch(survey_pageMapping.toDomain(survey_pagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.survey_pageService.get(#survey_page_id),'iBizBusinessCentral-Survey_page-Remove')")
    @ApiOperation(value = "删除问卷页面", tags = {"问卷页面" },  notes = "删除问卷页面")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_pages/{survey_page_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("survey_page_id") Long survey_page_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_pageService.remove(survey_page_id));
    }

    @PreAuthorize("hasPermission(this.survey_pageService.getSurveyPageByIds(#ids),'iBizBusinessCentral-Survey_page-Remove')")
    @ApiOperation(value = "批量删除问卷页面", tags = {"问卷页面" },  notes = "批量删除问卷页面")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_pages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        survey_pageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.survey_pageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Survey_page-Get')")
    @ApiOperation(value = "获取问卷页面", tags = {"问卷页面" },  notes = "获取问卷页面")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_pages/{survey_page_id}")
    public ResponseEntity<Survey_pageDTO> get(@PathVariable("survey_page_id") Long survey_page_id) {
        Survey_page domain = survey_pageService.get(survey_page_id);
        Survey_pageDTO dto = survey_pageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取问卷页面草稿", tags = {"问卷页面" },  notes = "获取问卷页面草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_pages/getdraft")
    public ResponseEntity<Survey_pageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(survey_pageMapping.toDto(survey_pageService.getDraft(new Survey_page())));
    }

    @ApiOperation(value = "检查问卷页面", tags = {"问卷页面" },  notes = "检查问卷页面")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_pages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Survey_pageDTO survey_pagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(survey_pageService.checkKey(survey_pageMapping.toDomain(survey_pagedto)));
    }

    @PreAuthorize("hasPermission(this.survey_pageMapping.toDomain(#survey_pagedto),'iBizBusinessCentral-Survey_page-Save')")
    @ApiOperation(value = "保存问卷页面", tags = {"问卷页面" },  notes = "保存问卷页面")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_pages/save")
    public ResponseEntity<Boolean> save(@RequestBody Survey_pageDTO survey_pagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(survey_pageService.save(survey_pageMapping.toDomain(survey_pagedto)));
    }

    @PreAuthorize("hasPermission(this.survey_pageMapping.toDomain(#survey_pagedtos),'iBizBusinessCentral-Survey_page-Save')")
    @ApiOperation(value = "批量保存问卷页面", tags = {"问卷页面" },  notes = "批量保存问卷页面")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_pages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Survey_pageDTO> survey_pagedtos) {
        survey_pageService.saveBatch(survey_pageMapping.toDomain(survey_pagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_page-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_page-Get')")
	@ApiOperation(value = "获取数据集", tags = {"问卷页面" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/survey_pages/fetchdefault")
	public ResponseEntity<List<Survey_pageDTO>> fetchDefault(Survey_pageSearchContext context) {
        Page<Survey_page> domains = survey_pageService.searchDefault(context) ;
        List<Survey_pageDTO> list = survey_pageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_page-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_page-Get')")
	@ApiOperation(value = "查询数据集", tags = {"问卷页面" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/survey_pages/searchdefault")
	public ResponseEntity<Page<Survey_pageDTO>> searchDefault(@RequestBody Survey_pageSearchContext context) {
        Page<Survey_page> domains = survey_pageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_pageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

