package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium;
import cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_mediumService;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_mediumSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"UTM媒体" })
@RestController("Core-utm_medium")
@RequestMapping("")
public class Utm_mediumResource {

    @Autowired
    public IUtm_mediumService utm_mediumService;

    @Autowired
    @Lazy
    public Utm_mediumMapping utm_mediumMapping;

    @PreAuthorize("hasPermission(this.utm_mediumMapping.toDomain(#utm_mediumdto),'iBizBusinessCentral-Utm_medium-Create')")
    @ApiOperation(value = "新建UTM媒体", tags = {"UTM媒体" },  notes = "新建UTM媒体")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_media")
    public ResponseEntity<Utm_mediumDTO> create(@Validated @RequestBody Utm_mediumDTO utm_mediumdto) {
        Utm_medium domain = utm_mediumMapping.toDomain(utm_mediumdto);
		utm_mediumService.create(domain);
        Utm_mediumDTO dto = utm_mediumMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.utm_mediumMapping.toDomain(#utm_mediumdtos),'iBizBusinessCentral-Utm_medium-Create')")
    @ApiOperation(value = "批量新建UTM媒体", tags = {"UTM媒体" },  notes = "批量新建UTM媒体")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_media/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Utm_mediumDTO> utm_mediumdtos) {
        utm_mediumService.createBatch(utm_mediumMapping.toDomain(utm_mediumdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "utm_medium" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.utm_mediumService.get(#utm_medium_id),'iBizBusinessCentral-Utm_medium-Update')")
    @ApiOperation(value = "更新UTM媒体", tags = {"UTM媒体" },  notes = "更新UTM媒体")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_media/{utm_medium_id}")
    public ResponseEntity<Utm_mediumDTO> update(@PathVariable("utm_medium_id") Long utm_medium_id, @RequestBody Utm_mediumDTO utm_mediumdto) {
		Utm_medium domain  = utm_mediumMapping.toDomain(utm_mediumdto);
        domain .setId(utm_medium_id);
		utm_mediumService.update(domain );
		Utm_mediumDTO dto = utm_mediumMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.utm_mediumService.getUtmMediumByEntities(this.utm_mediumMapping.toDomain(#utm_mediumdtos)),'iBizBusinessCentral-Utm_medium-Update')")
    @ApiOperation(value = "批量更新UTM媒体", tags = {"UTM媒体" },  notes = "批量更新UTM媒体")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_media/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Utm_mediumDTO> utm_mediumdtos) {
        utm_mediumService.updateBatch(utm_mediumMapping.toDomain(utm_mediumdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.utm_mediumService.get(#utm_medium_id),'iBizBusinessCentral-Utm_medium-Remove')")
    @ApiOperation(value = "删除UTM媒体", tags = {"UTM媒体" },  notes = "删除UTM媒体")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_media/{utm_medium_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("utm_medium_id") Long utm_medium_id) {
         return ResponseEntity.status(HttpStatus.OK).body(utm_mediumService.remove(utm_medium_id));
    }

    @PreAuthorize("hasPermission(this.utm_mediumService.getUtmMediumByIds(#ids),'iBizBusinessCentral-Utm_medium-Remove')")
    @ApiOperation(value = "批量删除UTM媒体", tags = {"UTM媒体" },  notes = "批量删除UTM媒体")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_media/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        utm_mediumService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.utm_mediumMapping.toDomain(returnObject.body),'iBizBusinessCentral-Utm_medium-Get')")
    @ApiOperation(value = "获取UTM媒体", tags = {"UTM媒体" },  notes = "获取UTM媒体")
	@RequestMapping(method = RequestMethod.GET, value = "/utm_media/{utm_medium_id}")
    public ResponseEntity<Utm_mediumDTO> get(@PathVariable("utm_medium_id") Long utm_medium_id) {
        Utm_medium domain = utm_mediumService.get(utm_medium_id);
        Utm_mediumDTO dto = utm_mediumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取UTM媒体草稿", tags = {"UTM媒体" },  notes = "获取UTM媒体草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/utm_media/getdraft")
    public ResponseEntity<Utm_mediumDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(utm_mediumMapping.toDto(utm_mediumService.getDraft(new Utm_medium())));
    }

    @ApiOperation(value = "检查UTM媒体", tags = {"UTM媒体" },  notes = "检查UTM媒体")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_media/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Utm_mediumDTO utm_mediumdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(utm_mediumService.checkKey(utm_mediumMapping.toDomain(utm_mediumdto)));
    }

    @PreAuthorize("hasPermission(this.utm_mediumMapping.toDomain(#utm_mediumdto),'iBizBusinessCentral-Utm_medium-Save')")
    @ApiOperation(value = "保存UTM媒体", tags = {"UTM媒体" },  notes = "保存UTM媒体")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_media/save")
    public ResponseEntity<Boolean> save(@RequestBody Utm_mediumDTO utm_mediumdto) {
        return ResponseEntity.status(HttpStatus.OK).body(utm_mediumService.save(utm_mediumMapping.toDomain(utm_mediumdto)));
    }

    @PreAuthorize("hasPermission(this.utm_mediumMapping.toDomain(#utm_mediumdtos),'iBizBusinessCentral-Utm_medium-Save')")
    @ApiOperation(value = "批量保存UTM媒体", tags = {"UTM媒体" },  notes = "批量保存UTM媒体")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_media/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Utm_mediumDTO> utm_mediumdtos) {
        utm_mediumService.saveBatch(utm_mediumMapping.toDomain(utm_mediumdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_medium-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Utm_medium-Get')")
	@ApiOperation(value = "获取数据集", tags = {"UTM媒体" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/utm_media/fetchdefault")
	public ResponseEntity<List<Utm_mediumDTO>> fetchDefault(Utm_mediumSearchContext context) {
        Page<Utm_medium> domains = utm_mediumService.searchDefault(context) ;
        List<Utm_mediumDTO> list = utm_mediumMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_medium-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Utm_medium-Get')")
	@ApiOperation(value = "查询数据集", tags = {"UTM媒体" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/utm_media/searchdefault")
	public ResponseEntity<Page<Utm_mediumDTO>> searchDefault(@RequestBody Utm_mediumSearchContext context) {
        Page<Utm_medium> domains = utm_mediumService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(utm_mediumMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

