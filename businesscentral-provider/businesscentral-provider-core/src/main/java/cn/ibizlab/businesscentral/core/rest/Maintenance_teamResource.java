package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_team;
import cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_teamService;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_teamSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"保养团队" })
@RestController("Core-maintenance_team")
@RequestMapping("")
public class Maintenance_teamResource {

    @Autowired
    public IMaintenance_teamService maintenance_teamService;

    @Autowired
    @Lazy
    public Maintenance_teamMapping maintenance_teamMapping;

    @PreAuthorize("hasPermission(this.maintenance_teamMapping.toDomain(#maintenance_teamdto),'iBizBusinessCentral-Maintenance_team-Create')")
    @ApiOperation(value = "新建保养团队", tags = {"保养团队" },  notes = "新建保养团队")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_teams")
    public ResponseEntity<Maintenance_teamDTO> create(@Validated @RequestBody Maintenance_teamDTO maintenance_teamdto) {
        Maintenance_team domain = maintenance_teamMapping.toDomain(maintenance_teamdto);
		maintenance_teamService.create(domain);
        Maintenance_teamDTO dto = maintenance_teamMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.maintenance_teamMapping.toDomain(#maintenance_teamdtos),'iBizBusinessCentral-Maintenance_team-Create')")
    @ApiOperation(value = "批量新建保养团队", tags = {"保养团队" },  notes = "批量新建保养团队")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_teams/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Maintenance_teamDTO> maintenance_teamdtos) {
        maintenance_teamService.createBatch(maintenance_teamMapping.toDomain(maintenance_teamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "maintenance_team" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.maintenance_teamService.get(#maintenance_team_id),'iBizBusinessCentral-Maintenance_team-Update')")
    @ApiOperation(value = "更新保养团队", tags = {"保养团队" },  notes = "更新保养团队")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_teams/{maintenance_team_id}")
    public ResponseEntity<Maintenance_teamDTO> update(@PathVariable("maintenance_team_id") Long maintenance_team_id, @RequestBody Maintenance_teamDTO maintenance_teamdto) {
		Maintenance_team domain  = maintenance_teamMapping.toDomain(maintenance_teamdto);
        domain .setId(maintenance_team_id);
		maintenance_teamService.update(domain );
		Maintenance_teamDTO dto = maintenance_teamMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.maintenance_teamService.getMaintenanceTeamByEntities(this.maintenance_teamMapping.toDomain(#maintenance_teamdtos)),'iBizBusinessCentral-Maintenance_team-Update')")
    @ApiOperation(value = "批量更新保养团队", tags = {"保养团队" },  notes = "批量更新保养团队")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_teams/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_teamDTO> maintenance_teamdtos) {
        maintenance_teamService.updateBatch(maintenance_teamMapping.toDomain(maintenance_teamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.maintenance_teamService.get(#maintenance_team_id),'iBizBusinessCentral-Maintenance_team-Remove')")
    @ApiOperation(value = "删除保养团队", tags = {"保养团队" },  notes = "删除保养团队")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_teams/{maintenance_team_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_team_id") Long maintenance_team_id) {
         return ResponseEntity.status(HttpStatus.OK).body(maintenance_teamService.remove(maintenance_team_id));
    }

    @PreAuthorize("hasPermission(this.maintenance_teamService.getMaintenanceTeamByIds(#ids),'iBizBusinessCentral-Maintenance_team-Remove')")
    @ApiOperation(value = "批量删除保养团队", tags = {"保养团队" },  notes = "批量删除保养团队")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_teams/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        maintenance_teamService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.maintenance_teamMapping.toDomain(returnObject.body),'iBizBusinessCentral-Maintenance_team-Get')")
    @ApiOperation(value = "获取保养团队", tags = {"保养团队" },  notes = "获取保养团队")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_teams/{maintenance_team_id}")
    public ResponseEntity<Maintenance_teamDTO> get(@PathVariable("maintenance_team_id") Long maintenance_team_id) {
        Maintenance_team domain = maintenance_teamService.get(maintenance_team_id);
        Maintenance_teamDTO dto = maintenance_teamMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取保养团队草稿", tags = {"保养团队" },  notes = "获取保养团队草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_teams/getdraft")
    public ResponseEntity<Maintenance_teamDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(maintenance_teamMapping.toDto(maintenance_teamService.getDraft(new Maintenance_team())));
    }

    @ApiOperation(value = "检查保养团队", tags = {"保养团队" },  notes = "检查保养团队")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_teams/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Maintenance_teamDTO maintenance_teamdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(maintenance_teamService.checkKey(maintenance_teamMapping.toDomain(maintenance_teamdto)));
    }

    @PreAuthorize("hasPermission(this.maintenance_teamMapping.toDomain(#maintenance_teamdto),'iBizBusinessCentral-Maintenance_team-Save')")
    @ApiOperation(value = "保存保养团队", tags = {"保养团队" },  notes = "保存保养团队")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_teams/save")
    public ResponseEntity<Boolean> save(@RequestBody Maintenance_teamDTO maintenance_teamdto) {
        return ResponseEntity.status(HttpStatus.OK).body(maintenance_teamService.save(maintenance_teamMapping.toDomain(maintenance_teamdto)));
    }

    @PreAuthorize("hasPermission(this.maintenance_teamMapping.toDomain(#maintenance_teamdtos),'iBizBusinessCentral-Maintenance_team-Save')")
    @ApiOperation(value = "批量保存保养团队", tags = {"保养团队" },  notes = "批量保存保养团队")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_teams/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Maintenance_teamDTO> maintenance_teamdtos) {
        maintenance_teamService.saveBatch(maintenance_teamMapping.toDomain(maintenance_teamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Maintenance_team-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Maintenance_team-Get')")
	@ApiOperation(value = "获取数据集", tags = {"保养团队" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_teams/fetchdefault")
	public ResponseEntity<List<Maintenance_teamDTO>> fetchDefault(Maintenance_teamSearchContext context) {
        Page<Maintenance_team> domains = maintenance_teamService.searchDefault(context) ;
        List<Maintenance_teamDTO> list = maintenance_teamMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Maintenance_team-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Maintenance_team-Get')")
	@ApiOperation(value = "查询数据集", tags = {"保养团队" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/maintenance_teams/searchdefault")
	public ResponseEntity<Page<Maintenance_teamDTO>> searchDefault(@RequestBody Maintenance_teamSearchContext context) {
        Page<Maintenance_team> domains = maintenance_teamService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(maintenance_teamMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

