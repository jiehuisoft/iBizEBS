package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_page;
import cn.ibizlab.businesscentral.core.odoo_website.service.IWebsite_pageService;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_pageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"页" })
@RestController("Core-website_page")
@RequestMapping("")
public class Website_pageResource {

    @Autowired
    public IWebsite_pageService website_pageService;

    @Autowired
    @Lazy
    public Website_pageMapping website_pageMapping;

    @PreAuthorize("hasPermission(this.website_pageMapping.toDomain(#website_pagedto),'iBizBusinessCentral-Website_page-Create')")
    @ApiOperation(value = "新建页", tags = {"页" },  notes = "新建页")
	@RequestMapping(method = RequestMethod.POST, value = "/website_pages")
    public ResponseEntity<Website_pageDTO> create(@Validated @RequestBody Website_pageDTO website_pagedto) {
        Website_page domain = website_pageMapping.toDomain(website_pagedto);
		website_pageService.create(domain);
        Website_pageDTO dto = website_pageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.website_pageMapping.toDomain(#website_pagedtos),'iBizBusinessCentral-Website_page-Create')")
    @ApiOperation(value = "批量新建页", tags = {"页" },  notes = "批量新建页")
	@RequestMapping(method = RequestMethod.POST, value = "/website_pages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Website_pageDTO> website_pagedtos) {
        website_pageService.createBatch(website_pageMapping.toDomain(website_pagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "website_page" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.website_pageService.get(#website_page_id),'iBizBusinessCentral-Website_page-Update')")
    @ApiOperation(value = "更新页", tags = {"页" },  notes = "更新页")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_pages/{website_page_id}")
    public ResponseEntity<Website_pageDTO> update(@PathVariable("website_page_id") Long website_page_id, @RequestBody Website_pageDTO website_pagedto) {
		Website_page domain  = website_pageMapping.toDomain(website_pagedto);
        domain .setId(website_page_id);
		website_pageService.update(domain );
		Website_pageDTO dto = website_pageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.website_pageService.getWebsitePageByEntities(this.website_pageMapping.toDomain(#website_pagedtos)),'iBizBusinessCentral-Website_page-Update')")
    @ApiOperation(value = "批量更新页", tags = {"页" },  notes = "批量更新页")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_pages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_pageDTO> website_pagedtos) {
        website_pageService.updateBatch(website_pageMapping.toDomain(website_pagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.website_pageService.get(#website_page_id),'iBizBusinessCentral-Website_page-Remove')")
    @ApiOperation(value = "删除页", tags = {"页" },  notes = "删除页")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_pages/{website_page_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("website_page_id") Long website_page_id) {
         return ResponseEntity.status(HttpStatus.OK).body(website_pageService.remove(website_page_id));
    }

    @PreAuthorize("hasPermission(this.website_pageService.getWebsitePageByIds(#ids),'iBizBusinessCentral-Website_page-Remove')")
    @ApiOperation(value = "批量删除页", tags = {"页" },  notes = "批量删除页")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_pages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        website_pageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.website_pageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Website_page-Get')")
    @ApiOperation(value = "获取页", tags = {"页" },  notes = "获取页")
	@RequestMapping(method = RequestMethod.GET, value = "/website_pages/{website_page_id}")
    public ResponseEntity<Website_pageDTO> get(@PathVariable("website_page_id") Long website_page_id) {
        Website_page domain = website_pageService.get(website_page_id);
        Website_pageDTO dto = website_pageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取页草稿", tags = {"页" },  notes = "获取页草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/website_pages/getdraft")
    public ResponseEntity<Website_pageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(website_pageMapping.toDto(website_pageService.getDraft(new Website_page())));
    }

    @ApiOperation(value = "检查页", tags = {"页" },  notes = "检查页")
	@RequestMapping(method = RequestMethod.POST, value = "/website_pages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Website_pageDTO website_pagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(website_pageService.checkKey(website_pageMapping.toDomain(website_pagedto)));
    }

    @PreAuthorize("hasPermission(this.website_pageMapping.toDomain(#website_pagedto),'iBizBusinessCentral-Website_page-Save')")
    @ApiOperation(value = "保存页", tags = {"页" },  notes = "保存页")
	@RequestMapping(method = RequestMethod.POST, value = "/website_pages/save")
    public ResponseEntity<Boolean> save(@RequestBody Website_pageDTO website_pagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(website_pageService.save(website_pageMapping.toDomain(website_pagedto)));
    }

    @PreAuthorize("hasPermission(this.website_pageMapping.toDomain(#website_pagedtos),'iBizBusinessCentral-Website_page-Save')")
    @ApiOperation(value = "批量保存页", tags = {"页" },  notes = "批量保存页")
	@RequestMapping(method = RequestMethod.POST, value = "/website_pages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Website_pageDTO> website_pagedtos) {
        website_pageService.saveBatch(website_pageMapping.toDomain(website_pagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_page-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Website_page-Get')")
	@ApiOperation(value = "获取数据集", tags = {"页" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/website_pages/fetchdefault")
	public ResponseEntity<List<Website_pageDTO>> fetchDefault(Website_pageSearchContext context) {
        Page<Website_page> domains = website_pageService.searchDefault(context) ;
        List<Website_pageDTO> list = website_pageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_page-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Website_page-Get')")
	@ApiOperation(value = "查询数据集", tags = {"页" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/website_pages/searchdefault")
	public ResponseEntity<Page<Website_pageDTO>> searchDefault(@RequestBody Website_pageSearchContext context) {
        Page<Website_page> domains = website_pageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(website_pageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

