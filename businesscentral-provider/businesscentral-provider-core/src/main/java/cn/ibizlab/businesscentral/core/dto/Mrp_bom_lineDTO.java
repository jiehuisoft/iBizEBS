package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Mrp_bom_lineDTO]
 */
@Data
public class Mrp_bom_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ATTRIBUTE_VALUE_IDS]
     *
     */
    @JSONField(name = "attribute_value_ids")
    @JsonProperty("attribute_value_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String attributeValueIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    @NotNull(message = "[数量]不允许为空!")
    private Double productQty;

    /**
     * 属性 [CHILD_LINE_IDS]
     *
     */
    @JSONField(name = "child_line_ids")
    @JsonProperty("child_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String childLineIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_VALUE_IDS]
     *
     */
    @JSONField(name = "valid_product_attribute_value_ids")
    @JsonProperty("valid_product_attribute_value_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String validProductAttributeValueIds;

    /**
     * 属性 [HAS_ATTACHMENTS]
     *
     */
    @JSONField(name = "has_attachments")
    @JsonProperty("has_attachments")
    private Boolean hasAttachments;

    /**
     * 属性 [CHILD_BOM_ID]
     *
     */
    @JSONField(name = "child_bom_id")
    @JsonProperty("child_bom_id")
    private Integer childBomId;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productTmplId;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productUomIdText;

    /**
     * 属性 [ROUTING_ID_TEXT]
     *
     */
    @JSONField(name = "routing_id_text")
    @JsonProperty("routing_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String routingIdText;

    /**
     * 属性 [PARENT_PRODUCT_TMPL_ID]
     *
     */
    @JSONField(name = "parent_product_tmpl_id")
    @JsonProperty("parent_product_tmpl_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentProductTmplId;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [OPERATION_ID_TEXT]
     *
     */
    @JSONField(name = "operation_id_text")
    @JsonProperty("operation_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String operationIdText;

    /**
     * 属性 [OPERATION_ID]
     *
     */
    @JSONField(name = "operation_id")
    @JsonProperty("operation_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long operationId;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[计量单位]不允许为空!")
    private Long productUomId;

    /**
     * 属性 [BOM_ID]
     *
     */
    @JSONField(name = "bom_id")
    @JsonProperty("bom_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[父级 BoM]不允许为空!")
    private Long bomId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [ROUTING_ID]
     *
     */
    @JSONField(name = "routing_id")
    @JsonProperty("routing_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long routingId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[零件]不允许为空!")
    private Long productId;


    /**
     * 设置 [PRODUCT_QTY]
     */
    public void setProductQty(Double  productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [OPERATION_ID]
     */
    public void setOperationId(Long  operationId){
        this.operationId = operationId ;
        this.modify("operation_id",operationId);
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    public void setProductUomId(Long  productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [BOM_ID]
     */
    public void setBomId(Long  bomId){
        this.bomId = bomId ;
        this.modify("bom_id",bomId);
    }

    /**
     * 设置 [ROUTING_ID]
     */
    public void setRoutingId(Long  routingId){
        this.routingId = routingId ;
        this.modify("routing_id",routingId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }


}


