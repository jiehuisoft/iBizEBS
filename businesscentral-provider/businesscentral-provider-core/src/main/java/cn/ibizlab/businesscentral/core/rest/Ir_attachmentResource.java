package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_attachment;
import cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_attachmentService;
import cn.ibizlab.businesscentral.core.odoo_ir.filter.Ir_attachmentSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"附件" })
@RestController("Core-ir_attachment")
@RequestMapping("")
public class Ir_attachmentResource {

    @Autowired
    public IIr_attachmentService ir_attachmentService;

    @Autowired
    @Lazy
    public Ir_attachmentMapping ir_attachmentMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_attachment-Create-all')")
    @ApiOperation(value = "新建附件", tags = {"附件" },  notes = "新建附件")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_attachments")
    public ResponseEntity<Ir_attachmentDTO> create(@Validated @RequestBody Ir_attachmentDTO ir_attachmentdto) {
        Ir_attachment domain = ir_attachmentMapping.toDomain(ir_attachmentdto);
		ir_attachmentService.create(domain);
        Ir_attachmentDTO dto = ir_attachmentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_attachment-Create-all')")
    @ApiOperation(value = "批量新建附件", tags = {"附件" },  notes = "批量新建附件")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_attachments/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Ir_attachmentDTO> ir_attachmentdtos) {
        ir_attachmentService.createBatch(ir_attachmentMapping.toDomain(ir_attachmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_attachment-Update-all')")
    @ApiOperation(value = "更新附件", tags = {"附件" },  notes = "更新附件")
	@RequestMapping(method = RequestMethod.PUT, value = "/ir_attachments/{ir_attachment_id}")
    public ResponseEntity<Ir_attachmentDTO> update(@PathVariable("ir_attachment_id") Long ir_attachment_id, @RequestBody Ir_attachmentDTO ir_attachmentdto) {
		Ir_attachment domain  = ir_attachmentMapping.toDomain(ir_attachmentdto);
        domain .setId(ir_attachment_id);
		ir_attachmentService.update(domain );
		Ir_attachmentDTO dto = ir_attachmentMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_attachment-Update-all')")
    @ApiOperation(value = "批量更新附件", tags = {"附件" },  notes = "批量更新附件")
	@RequestMapping(method = RequestMethod.PUT, value = "/ir_attachments/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Ir_attachmentDTO> ir_attachmentdtos) {
        ir_attachmentService.updateBatch(ir_attachmentMapping.toDomain(ir_attachmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_attachment-Remove-all')")
    @ApiOperation(value = "删除附件", tags = {"附件" },  notes = "删除附件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ir_attachments/{ir_attachment_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("ir_attachment_id") Long ir_attachment_id) {
         return ResponseEntity.status(HttpStatus.OK).body(ir_attachmentService.remove(ir_attachment_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_attachment-Remove-all')")
    @ApiOperation(value = "批量删除附件", tags = {"附件" },  notes = "批量删除附件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ir_attachments/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        ir_attachmentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_attachment-Get-all')")
    @ApiOperation(value = "获取附件", tags = {"附件" },  notes = "获取附件")
	@RequestMapping(method = RequestMethod.GET, value = "/ir_attachments/{ir_attachment_id}")
    public ResponseEntity<Ir_attachmentDTO> get(@PathVariable("ir_attachment_id") Long ir_attachment_id) {
        Ir_attachment domain = ir_attachmentService.get(ir_attachment_id);
        Ir_attachmentDTO dto = ir_attachmentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取附件草稿", tags = {"附件" },  notes = "获取附件草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/ir_attachments/getdraft")
    public ResponseEntity<Ir_attachmentDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(ir_attachmentMapping.toDto(ir_attachmentService.getDraft(new Ir_attachment())));
    }

    @ApiOperation(value = "检查附件", tags = {"附件" },  notes = "检查附件")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_attachments/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Ir_attachmentDTO ir_attachmentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(ir_attachmentService.checkKey(ir_attachmentMapping.toDomain(ir_attachmentdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_attachment-Save-all')")
    @ApiOperation(value = "保存附件", tags = {"附件" },  notes = "保存附件")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_attachments/save")
    public ResponseEntity<Boolean> save(@RequestBody Ir_attachmentDTO ir_attachmentdto) {
        return ResponseEntity.status(HttpStatus.OK).body(ir_attachmentService.save(ir_attachmentMapping.toDomain(ir_attachmentdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_attachment-Save-all')")
    @ApiOperation(value = "批量保存附件", tags = {"附件" },  notes = "批量保存附件")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_attachments/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Ir_attachmentDTO> ir_attachmentdtos) {
        ir_attachmentService.saveBatch(ir_attachmentMapping.toDomain(ir_attachmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_attachment-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"附件" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/ir_attachments/fetchdefault")
	public ResponseEntity<List<Ir_attachmentDTO>> fetchDefault(Ir_attachmentSearchContext context) {
        Page<Ir_attachment> domains = ir_attachmentService.searchDefault(context) ;
        List<Ir_attachmentDTO> list = ir_attachmentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_attachment-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"附件" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/ir_attachments/searchdefault")
	public ResponseEntity<Page<Ir_attachmentDTO>> searchDefault(@RequestBody Ir_attachmentSearchContext context) {
        Page<Ir_attachment> domains = ir_attachmentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(ir_attachmentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

