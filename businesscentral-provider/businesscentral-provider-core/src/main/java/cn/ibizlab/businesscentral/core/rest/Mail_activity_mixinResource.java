package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity_mixin;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_activity_mixinService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_activity_mixinSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"活动Mixin" })
@RestController("Core-mail_activity_mixin")
@RequestMapping("")
public class Mail_activity_mixinResource {

    @Autowired
    public IMail_activity_mixinService mail_activity_mixinService;

    @Autowired
    @Lazy
    public Mail_activity_mixinMapping mail_activity_mixinMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity_mixin-Create-all')")
    @ApiOperation(value = "新建活动Mixin", tags = {"活动Mixin" },  notes = "新建活动Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_mixins")
    public ResponseEntity<Mail_activity_mixinDTO> create(@Validated @RequestBody Mail_activity_mixinDTO mail_activity_mixindto) {
        Mail_activity_mixin domain = mail_activity_mixinMapping.toDomain(mail_activity_mixindto);
		mail_activity_mixinService.create(domain);
        Mail_activity_mixinDTO dto = mail_activity_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity_mixin-Create-all')")
    @ApiOperation(value = "批量新建活动Mixin", tags = {"活动Mixin" },  notes = "批量新建活动Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_activity_mixinDTO> mail_activity_mixindtos) {
        mail_activity_mixinService.createBatch(mail_activity_mixinMapping.toDomain(mail_activity_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity_mixin-Update-all')")
    @ApiOperation(value = "更新活动Mixin", tags = {"活动Mixin" },  notes = "更新活动Mixin")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_mixins/{mail_activity_mixin_id}")
    public ResponseEntity<Mail_activity_mixinDTO> update(@PathVariable("mail_activity_mixin_id") Long mail_activity_mixin_id, @RequestBody Mail_activity_mixinDTO mail_activity_mixindto) {
		Mail_activity_mixin domain  = mail_activity_mixinMapping.toDomain(mail_activity_mixindto);
        domain .setId(mail_activity_mixin_id);
		mail_activity_mixinService.update(domain );
		Mail_activity_mixinDTO dto = mail_activity_mixinMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity_mixin-Update-all')")
    @ApiOperation(value = "批量更新活动Mixin", tags = {"活动Mixin" },  notes = "批量更新活动Mixin")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_activity_mixinDTO> mail_activity_mixindtos) {
        mail_activity_mixinService.updateBatch(mail_activity_mixinMapping.toDomain(mail_activity_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity_mixin-Remove-all')")
    @ApiOperation(value = "删除活动Mixin", tags = {"活动Mixin" },  notes = "删除活动Mixin")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_mixins/{mail_activity_mixin_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_activity_mixin_id") Long mail_activity_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_activity_mixinService.remove(mail_activity_mixin_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity_mixin-Remove-all')")
    @ApiOperation(value = "批量删除活动Mixin", tags = {"活动Mixin" },  notes = "批量删除活动Mixin")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_activity_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity_mixin-Get-all')")
    @ApiOperation(value = "获取活动Mixin", tags = {"活动Mixin" },  notes = "获取活动Mixin")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activity_mixins/{mail_activity_mixin_id}")
    public ResponseEntity<Mail_activity_mixinDTO> get(@PathVariable("mail_activity_mixin_id") Long mail_activity_mixin_id) {
        Mail_activity_mixin domain = mail_activity_mixinService.get(mail_activity_mixin_id);
        Mail_activity_mixinDTO dto = mail_activity_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取活动Mixin草稿", tags = {"活动Mixin" },  notes = "获取活动Mixin草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activity_mixins/getdraft")
    public ResponseEntity<Mail_activity_mixinDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_mixinMapping.toDto(mail_activity_mixinService.getDraft(new Mail_activity_mixin())));
    }

    @ApiOperation(value = "检查活动Mixin", tags = {"活动Mixin" },  notes = "检查活动Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_mixins/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_activity_mixinDTO mail_activity_mixindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_activity_mixinService.checkKey(mail_activity_mixinMapping.toDomain(mail_activity_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity_mixin-Save-all')")
    @ApiOperation(value = "保存活动Mixin", tags = {"活动Mixin" },  notes = "保存活动Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_mixins/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_activity_mixinDTO mail_activity_mixindto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_mixinService.save(mail_activity_mixinMapping.toDomain(mail_activity_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity_mixin-Save-all')")
    @ApiOperation(value = "批量保存活动Mixin", tags = {"活动Mixin" },  notes = "批量保存活动Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_mixins/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_activity_mixinDTO> mail_activity_mixindtos) {
        mail_activity_mixinService.saveBatch(mail_activity_mixinMapping.toDomain(mail_activity_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity_mixin-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"活动Mixin" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_activity_mixins/fetchdefault")
	public ResponseEntity<List<Mail_activity_mixinDTO>> fetchDefault(Mail_activity_mixinSearchContext context) {
        Page<Mail_activity_mixin> domains = mail_activity_mixinService.searchDefault(context) ;
        List<Mail_activity_mixinDTO> list = mail_activity_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity_mixin-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"活动Mixin" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_activity_mixins/searchdefault")
	public ResponseEntity<Page<Mail_activity_mixinDTO>> searchDefault(@RequestBody Mail_activity_mixinSearchContext context) {
        Page<Mail_activity_mixin> domains = mail_activity_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_activity_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

