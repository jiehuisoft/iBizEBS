package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_fixed_putaway_strat;
import cn.ibizlab.businesscentral.core.dto.Stock_fixed_putaway_stratDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreStock_fixed_putaway_stratMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_fixed_putaway_stratMapping extends MappingBase<Stock_fixed_putaway_stratDTO, Stock_fixed_putaway_strat> {


}

