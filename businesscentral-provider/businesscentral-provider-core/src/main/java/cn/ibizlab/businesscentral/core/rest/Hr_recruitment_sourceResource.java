package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_source;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_recruitment_sourceService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_recruitment_sourceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"应聘者来源" })
@RestController("Core-hr_recruitment_source")
@RequestMapping("")
public class Hr_recruitment_sourceResource {

    @Autowired
    public IHr_recruitment_sourceService hr_recruitment_sourceService;

    @Autowired
    @Lazy
    public Hr_recruitment_sourceMapping hr_recruitment_sourceMapping;

    @PreAuthorize("hasPermission(this.hr_recruitment_sourceMapping.toDomain(#hr_recruitment_sourcedto),'iBizBusinessCentral-Hr_recruitment_source-Create')")
    @ApiOperation(value = "新建应聘者来源", tags = {"应聘者来源" },  notes = "新建应聘者来源")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources")
    public ResponseEntity<Hr_recruitment_sourceDTO> create(@Validated @RequestBody Hr_recruitment_sourceDTO hr_recruitment_sourcedto) {
        Hr_recruitment_source domain = hr_recruitment_sourceMapping.toDomain(hr_recruitment_sourcedto);
		hr_recruitment_sourceService.create(domain);
        Hr_recruitment_sourceDTO dto = hr_recruitment_sourceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_sourceMapping.toDomain(#hr_recruitment_sourcedtos),'iBizBusinessCentral-Hr_recruitment_source-Create')")
    @ApiOperation(value = "批量新建应聘者来源", tags = {"应聘者来源" },  notes = "批量新建应聘者来源")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_recruitment_sourceDTO> hr_recruitment_sourcedtos) {
        hr_recruitment_sourceService.createBatch(hr_recruitment_sourceMapping.toDomain(hr_recruitment_sourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_recruitment_source" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_recruitment_sourceService.get(#hr_recruitment_source_id),'iBizBusinessCentral-Hr_recruitment_source-Update')")
    @ApiOperation(value = "更新应聘者来源", tags = {"应聘者来源" },  notes = "更新应聘者来源")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_sources/{hr_recruitment_source_id}")
    public ResponseEntity<Hr_recruitment_sourceDTO> update(@PathVariable("hr_recruitment_source_id") Long hr_recruitment_source_id, @RequestBody Hr_recruitment_sourceDTO hr_recruitment_sourcedto) {
		Hr_recruitment_source domain  = hr_recruitment_sourceMapping.toDomain(hr_recruitment_sourcedto);
        domain .setId(hr_recruitment_source_id);
		hr_recruitment_sourceService.update(domain );
		Hr_recruitment_sourceDTO dto = hr_recruitment_sourceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_sourceService.getHrRecruitmentSourceByEntities(this.hr_recruitment_sourceMapping.toDomain(#hr_recruitment_sourcedtos)),'iBizBusinessCentral-Hr_recruitment_source-Update')")
    @ApiOperation(value = "批量更新应聘者来源", tags = {"应聘者来源" },  notes = "批量更新应聘者来源")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_sources/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_recruitment_sourceDTO> hr_recruitment_sourcedtos) {
        hr_recruitment_sourceService.updateBatch(hr_recruitment_sourceMapping.toDomain(hr_recruitment_sourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_sourceService.get(#hr_recruitment_source_id),'iBizBusinessCentral-Hr_recruitment_source-Remove')")
    @ApiOperation(value = "删除应聘者来源", tags = {"应聘者来源" },  notes = "删除应聘者来源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_sources/{hr_recruitment_source_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_recruitment_source_id") Long hr_recruitment_source_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_sourceService.remove(hr_recruitment_source_id));
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_sourceService.getHrRecruitmentSourceByIds(#ids),'iBizBusinessCentral-Hr_recruitment_source-Remove')")
    @ApiOperation(value = "批量删除应聘者来源", tags = {"应聘者来源" },  notes = "批量删除应聘者来源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_sources/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_recruitment_sourceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_recruitment_sourceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_recruitment_source-Get')")
    @ApiOperation(value = "获取应聘者来源", tags = {"应聘者来源" },  notes = "获取应聘者来源")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_sources/{hr_recruitment_source_id}")
    public ResponseEntity<Hr_recruitment_sourceDTO> get(@PathVariable("hr_recruitment_source_id") Long hr_recruitment_source_id) {
        Hr_recruitment_source domain = hr_recruitment_sourceService.get(hr_recruitment_source_id);
        Hr_recruitment_sourceDTO dto = hr_recruitment_sourceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取应聘者来源草稿", tags = {"应聘者来源" },  notes = "获取应聘者来源草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_sources/getdraft")
    public ResponseEntity<Hr_recruitment_sourceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_sourceMapping.toDto(hr_recruitment_sourceService.getDraft(new Hr_recruitment_source())));
    }

    @ApiOperation(value = "检查应聘者来源", tags = {"应聘者来源" },  notes = "检查应聘者来源")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_recruitment_sourceDTO hr_recruitment_sourcedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_sourceService.checkKey(hr_recruitment_sourceMapping.toDomain(hr_recruitment_sourcedto)));
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_sourceMapping.toDomain(#hr_recruitment_sourcedto),'iBizBusinessCentral-Hr_recruitment_source-Save')")
    @ApiOperation(value = "保存应聘者来源", tags = {"应聘者来源" },  notes = "保存应聘者来源")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_recruitment_sourceDTO hr_recruitment_sourcedto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_sourceService.save(hr_recruitment_sourceMapping.toDomain(hr_recruitment_sourcedto)));
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_sourceMapping.toDomain(#hr_recruitment_sourcedtos),'iBizBusinessCentral-Hr_recruitment_source-Save')")
    @ApiOperation(value = "批量保存应聘者来源", tags = {"应聘者来源" },  notes = "批量保存应聘者来源")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_recruitment_sourceDTO> hr_recruitment_sourcedtos) {
        hr_recruitment_sourceService.saveBatch(hr_recruitment_sourceMapping.toDomain(hr_recruitment_sourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_recruitment_source-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_recruitment_source-Get')")
	@ApiOperation(value = "获取数据集", tags = {"应聘者来源" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_recruitment_sources/fetchdefault")
	public ResponseEntity<List<Hr_recruitment_sourceDTO>> fetchDefault(Hr_recruitment_sourceSearchContext context) {
        Page<Hr_recruitment_source> domains = hr_recruitment_sourceService.searchDefault(context) ;
        List<Hr_recruitment_sourceDTO> list = hr_recruitment_sourceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_recruitment_source-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_recruitment_source-Get')")
	@ApiOperation(value = "查询数据集", tags = {"应聘者来源" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_recruitment_sources/searchdefault")
	public ResponseEntity<Page<Hr_recruitment_sourceDTO>> searchDefault(@RequestBody Hr_recruitment_sourceSearchContext context) {
        Page<Hr_recruitment_source> domains = hr_recruitment_sourceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_recruitment_sourceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

