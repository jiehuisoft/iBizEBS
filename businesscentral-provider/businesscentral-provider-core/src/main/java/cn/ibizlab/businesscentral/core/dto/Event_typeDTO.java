package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Event_typeDTO]
 */
@Data
public class Event_typeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DEFAULT_REGISTRATION_MAX]
     *
     */
    @JSONField(name = "default_registration_max")
    @JsonProperty("default_registration_max")
    private Integer defaultRegistrationMax;

    /**
     * 属性 [EVENT_TYPE_MAIL_IDS]
     *
     */
    @JSONField(name = "event_type_mail_ids")
    @JsonProperty("event_type_mail_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String eventTypeMailIds;

    /**
     * 属性 [DEFAULT_HASHTAG]
     *
     */
    @JSONField(name = "default_hashtag")
    @JsonProperty("default_hashtag")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String defaultHashtag;

    /**
     * 属性 [DEFAULT_TIMEZONE]
     *
     */
    @JSONField(name = "default_timezone")
    @JsonProperty("default_timezone")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String defaultTimezone;

    /**
     * 属性 [DEFAULT_REGISTRATION_MIN]
     *
     */
    @JSONField(name = "default_registration_min")
    @JsonProperty("default_registration_min")
    private Integer defaultRegistrationMin;

    /**
     * 属性 [EVENT_TICKET_IDS]
     *
     */
    @JSONField(name = "event_ticket_ids")
    @JsonProperty("event_ticket_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String eventTicketIds;

    /**
     * 属性 [USE_TICKETING]
     *
     */
    @JSONField(name = "use_ticketing")
    @JsonProperty("use_ticketing")
    private Boolean useTicketing;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [IS_ONLINE]
     *
     */
    @JSONField(name = "is_online")
    @JsonProperty("is_online")
    private Boolean isOnline;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[活动类别]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [AUTO_CONFIRM]
     *
     */
    @JSONField(name = "auto_confirm")
    @JsonProperty("auto_confirm")
    private Boolean autoConfirm;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WEBSITE_MENU]
     *
     */
    @JSONField(name = "website_menu")
    @JsonProperty("website_menu")
    private Boolean websiteMenu;

    /**
     * 属性 [USE_HASHTAG]
     *
     */
    @JSONField(name = "use_hashtag")
    @JsonProperty("use_hashtag")
    private Boolean useHashtag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [HAS_SEATS_LIMITATION]
     *
     */
    @JSONField(name = "has_seats_limitation")
    @JsonProperty("has_seats_limitation")
    private Boolean hasSeatsLimitation;

    /**
     * 属性 [USE_MAIL_SCHEDULE]
     *
     */
    @JSONField(name = "use_mail_schedule")
    @JsonProperty("use_mail_schedule")
    private Boolean useMailSchedule;

    /**
     * 属性 [USE_TIMEZONE]
     *
     */
    @JSONField(name = "use_timezone")
    @JsonProperty("use_timezone")
    private Boolean useTimezone;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [DEFAULT_REGISTRATION_MAX]
     */
    public void setDefaultRegistrationMax(Integer  defaultRegistrationMax){
        this.defaultRegistrationMax = defaultRegistrationMax ;
        this.modify("default_registration_max",defaultRegistrationMax);
    }

    /**
     * 设置 [DEFAULT_HASHTAG]
     */
    public void setDefaultHashtag(String  defaultHashtag){
        this.defaultHashtag = defaultHashtag ;
        this.modify("default_hashtag",defaultHashtag);
    }

    /**
     * 设置 [DEFAULT_TIMEZONE]
     */
    public void setDefaultTimezone(String  defaultTimezone){
        this.defaultTimezone = defaultTimezone ;
        this.modify("default_timezone",defaultTimezone);
    }

    /**
     * 设置 [DEFAULT_REGISTRATION_MIN]
     */
    public void setDefaultRegistrationMin(Integer  defaultRegistrationMin){
        this.defaultRegistrationMin = defaultRegistrationMin ;
        this.modify("default_registration_min",defaultRegistrationMin);
    }

    /**
     * 设置 [USE_TICKETING]
     */
    public void setUseTicketing(Boolean  useTicketing){
        this.useTicketing = useTicketing ;
        this.modify("use_ticketing",useTicketing);
    }

    /**
     * 设置 [IS_ONLINE]
     */
    public void setIsOnline(Boolean  isOnline){
        this.isOnline = isOnline ;
        this.modify("is_online",isOnline);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [AUTO_CONFIRM]
     */
    public void setAutoConfirm(Boolean  autoConfirm){
        this.autoConfirm = autoConfirm ;
        this.modify("auto_confirm",autoConfirm);
    }

    /**
     * 设置 [WEBSITE_MENU]
     */
    public void setWebsiteMenu(Boolean  websiteMenu){
        this.websiteMenu = websiteMenu ;
        this.modify("website_menu",websiteMenu);
    }

    /**
     * 设置 [USE_HASHTAG]
     */
    public void setUseHashtag(Boolean  useHashtag){
        this.useHashtag = useHashtag ;
        this.modify("use_hashtag",useHashtag);
    }

    /**
     * 设置 [HAS_SEATS_LIMITATION]
     */
    public void setHasSeatsLimitation(Boolean  hasSeatsLimitation){
        this.hasSeatsLimitation = hasSeatsLimitation ;
        this.modify("has_seats_limitation",hasSeatsLimitation);
    }

    /**
     * 设置 [USE_MAIL_SCHEDULE]
     */
    public void setUseMailSchedule(Boolean  useMailSchedule){
        this.useMailSchedule = useMailSchedule ;
        this.modify("use_mail_schedule",useMailSchedule);
    }

    /**
     * 设置 [USE_TIMEZONE]
     */
    public void setUseTimezone(Boolean  useTimezone){
        this.useTimezone = useTimezone ;
        this.modify("use_timezone",useTimezone);
    }


}


