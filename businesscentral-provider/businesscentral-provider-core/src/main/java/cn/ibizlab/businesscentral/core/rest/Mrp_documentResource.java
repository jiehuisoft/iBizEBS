package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_document;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_documentService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_documentSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"生产文档" })
@RestController("Core-mrp_document")
@RequestMapping("")
public class Mrp_documentResource {

    @Autowired
    public IMrp_documentService mrp_documentService;

    @Autowired
    @Lazy
    public Mrp_documentMapping mrp_documentMapping;

    @PreAuthorize("hasPermission(this.mrp_documentMapping.toDomain(#mrp_documentdto),'iBizBusinessCentral-Mrp_document-Create')")
    @ApiOperation(value = "新建生产文档", tags = {"生产文档" },  notes = "新建生产文档")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_documents")
    public ResponseEntity<Mrp_documentDTO> create(@Validated @RequestBody Mrp_documentDTO mrp_documentdto) {
        Mrp_document domain = mrp_documentMapping.toDomain(mrp_documentdto);
		mrp_documentService.create(domain);
        Mrp_documentDTO dto = mrp_documentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_documentMapping.toDomain(#mrp_documentdtos),'iBizBusinessCentral-Mrp_document-Create')")
    @ApiOperation(value = "批量新建生产文档", tags = {"生产文档" },  notes = "批量新建生产文档")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_documents/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_documentDTO> mrp_documentdtos) {
        mrp_documentService.createBatch(mrp_documentMapping.toDomain(mrp_documentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_document" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_documentService.get(#mrp_document_id),'iBizBusinessCentral-Mrp_document-Update')")
    @ApiOperation(value = "更新生产文档", tags = {"生产文档" },  notes = "更新生产文档")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_documents/{mrp_document_id}")
    public ResponseEntity<Mrp_documentDTO> update(@PathVariable("mrp_document_id") Long mrp_document_id, @RequestBody Mrp_documentDTO mrp_documentdto) {
		Mrp_document domain  = mrp_documentMapping.toDomain(mrp_documentdto);
        domain .setId(mrp_document_id);
		mrp_documentService.update(domain );
		Mrp_documentDTO dto = mrp_documentMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_documentService.getMrpDocumentByEntities(this.mrp_documentMapping.toDomain(#mrp_documentdtos)),'iBizBusinessCentral-Mrp_document-Update')")
    @ApiOperation(value = "批量更新生产文档", tags = {"生产文档" },  notes = "批量更新生产文档")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_documents/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_documentDTO> mrp_documentdtos) {
        mrp_documentService.updateBatch(mrp_documentMapping.toDomain(mrp_documentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_documentService.get(#mrp_document_id),'iBizBusinessCentral-Mrp_document-Remove')")
    @ApiOperation(value = "删除生产文档", tags = {"生产文档" },  notes = "删除生产文档")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_documents/{mrp_document_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_document_id") Long mrp_document_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_documentService.remove(mrp_document_id));
    }

    @PreAuthorize("hasPermission(this.mrp_documentService.getMrpDocumentByIds(#ids),'iBizBusinessCentral-Mrp_document-Remove')")
    @ApiOperation(value = "批量删除生产文档", tags = {"生产文档" },  notes = "批量删除生产文档")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_documents/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_documentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_documentMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_document-Get')")
    @ApiOperation(value = "获取生产文档", tags = {"生产文档" },  notes = "获取生产文档")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_documents/{mrp_document_id}")
    public ResponseEntity<Mrp_documentDTO> get(@PathVariable("mrp_document_id") Long mrp_document_id) {
        Mrp_document domain = mrp_documentService.get(mrp_document_id);
        Mrp_documentDTO dto = mrp_documentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取生产文档草稿", tags = {"生产文档" },  notes = "获取生产文档草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_documents/getdraft")
    public ResponseEntity<Mrp_documentDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_documentMapping.toDto(mrp_documentService.getDraft(new Mrp_document())));
    }

    @ApiOperation(value = "检查生产文档", tags = {"生产文档" },  notes = "检查生产文档")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_documents/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_documentDTO mrp_documentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_documentService.checkKey(mrp_documentMapping.toDomain(mrp_documentdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_documentMapping.toDomain(#mrp_documentdto),'iBizBusinessCentral-Mrp_document-Save')")
    @ApiOperation(value = "保存生产文档", tags = {"生产文档" },  notes = "保存生产文档")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_documents/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_documentDTO mrp_documentdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_documentService.save(mrp_documentMapping.toDomain(mrp_documentdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_documentMapping.toDomain(#mrp_documentdtos),'iBizBusinessCentral-Mrp_document-Save')")
    @ApiOperation(value = "批量保存生产文档", tags = {"生产文档" },  notes = "批量保存生产文档")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_documents/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_documentDTO> mrp_documentdtos) {
        mrp_documentService.saveBatch(mrp_documentMapping.toDomain(mrp_documentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_document-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_document-Get')")
	@ApiOperation(value = "获取数据集", tags = {"生产文档" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_documents/fetchdefault")
	public ResponseEntity<List<Mrp_documentDTO>> fetchDefault(Mrp_documentSearchContext context) {
        Page<Mrp_document> domains = mrp_documentService.searchDefault(context) ;
        List<Mrp_documentDTO> list = mrp_documentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_document-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_document-Get')")
	@ApiOperation(value = "查询数据集", tags = {"生产文档" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_documents/searchdefault")
	public ResponseEntity<Page<Mrp_documentDTO>> searchDefault(@RequestBody Mrp_documentSearchContext context) {
        Page<Mrp_document> domains = mrp_documentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_documentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

