package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_language_install;
import cn.ibizlab.businesscentral.core.odoo_base.service.IBase_language_installService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_language_installSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"安装语言" })
@RestController("Core-base_language_install")
@RequestMapping("")
public class Base_language_installResource {

    @Autowired
    public IBase_language_installService base_language_installService;

    @Autowired
    @Lazy
    public Base_language_installMapping base_language_installMapping;

    @PreAuthorize("hasPermission(this.base_language_installMapping.toDomain(#base_language_installdto),'iBizBusinessCentral-Base_language_install-Create')")
    @ApiOperation(value = "新建安装语言", tags = {"安装语言" },  notes = "新建安装语言")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_installs")
    public ResponseEntity<Base_language_installDTO> create(@Validated @RequestBody Base_language_installDTO base_language_installdto) {
        Base_language_install domain = base_language_installMapping.toDomain(base_language_installdto);
		base_language_installService.create(domain);
        Base_language_installDTO dto = base_language_installMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_language_installMapping.toDomain(#base_language_installdtos),'iBizBusinessCentral-Base_language_install-Create')")
    @ApiOperation(value = "批量新建安装语言", tags = {"安装语言" },  notes = "批量新建安装语言")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_installs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_language_installDTO> base_language_installdtos) {
        base_language_installService.createBatch(base_language_installMapping.toDomain(base_language_installdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_language_install" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_language_installService.get(#base_language_install_id),'iBizBusinessCentral-Base_language_install-Update')")
    @ApiOperation(value = "更新安装语言", tags = {"安装语言" },  notes = "更新安装语言")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_language_installs/{base_language_install_id}")
    public ResponseEntity<Base_language_installDTO> update(@PathVariable("base_language_install_id") Long base_language_install_id, @RequestBody Base_language_installDTO base_language_installdto) {
		Base_language_install domain  = base_language_installMapping.toDomain(base_language_installdto);
        domain .setId(base_language_install_id);
		base_language_installService.update(domain );
		Base_language_installDTO dto = base_language_installMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_language_installService.getBaseLanguageInstallByEntities(this.base_language_installMapping.toDomain(#base_language_installdtos)),'iBizBusinessCentral-Base_language_install-Update')")
    @ApiOperation(value = "批量更新安装语言", tags = {"安装语言" },  notes = "批量更新安装语言")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_language_installs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_language_installDTO> base_language_installdtos) {
        base_language_installService.updateBatch(base_language_installMapping.toDomain(base_language_installdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_language_installService.get(#base_language_install_id),'iBizBusinessCentral-Base_language_install-Remove')")
    @ApiOperation(value = "删除安装语言", tags = {"安装语言" },  notes = "删除安装语言")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_language_installs/{base_language_install_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_language_install_id") Long base_language_install_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_language_installService.remove(base_language_install_id));
    }

    @PreAuthorize("hasPermission(this.base_language_installService.getBaseLanguageInstallByIds(#ids),'iBizBusinessCentral-Base_language_install-Remove')")
    @ApiOperation(value = "批量删除安装语言", tags = {"安装语言" },  notes = "批量删除安装语言")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_language_installs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_language_installService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_language_installMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_language_install-Get')")
    @ApiOperation(value = "获取安装语言", tags = {"安装语言" },  notes = "获取安装语言")
	@RequestMapping(method = RequestMethod.GET, value = "/base_language_installs/{base_language_install_id}")
    public ResponseEntity<Base_language_installDTO> get(@PathVariable("base_language_install_id") Long base_language_install_id) {
        Base_language_install domain = base_language_installService.get(base_language_install_id);
        Base_language_installDTO dto = base_language_installMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取安装语言草稿", tags = {"安装语言" },  notes = "获取安装语言草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_language_installs/getdraft")
    public ResponseEntity<Base_language_installDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_language_installMapping.toDto(base_language_installService.getDraft(new Base_language_install())));
    }

    @ApiOperation(value = "检查安装语言", tags = {"安装语言" },  notes = "检查安装语言")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_installs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_language_installDTO base_language_installdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_language_installService.checkKey(base_language_installMapping.toDomain(base_language_installdto)));
    }

    @PreAuthorize("hasPermission(this.base_language_installMapping.toDomain(#base_language_installdto),'iBizBusinessCentral-Base_language_install-Save')")
    @ApiOperation(value = "保存安装语言", tags = {"安装语言" },  notes = "保存安装语言")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_installs/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_language_installDTO base_language_installdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_language_installService.save(base_language_installMapping.toDomain(base_language_installdto)));
    }

    @PreAuthorize("hasPermission(this.base_language_installMapping.toDomain(#base_language_installdtos),'iBizBusinessCentral-Base_language_install-Save')")
    @ApiOperation(value = "批量保存安装语言", tags = {"安装语言" },  notes = "批量保存安装语言")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_installs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_language_installDTO> base_language_installdtos) {
        base_language_installService.saveBatch(base_language_installMapping.toDomain(base_language_installdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_language_install-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_language_install-Get')")
	@ApiOperation(value = "获取数据集", tags = {"安装语言" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_language_installs/fetchdefault")
	public ResponseEntity<List<Base_language_installDTO>> fetchDefault(Base_language_installSearchContext context) {
        Page<Base_language_install> domains = base_language_installService.searchDefault(context) ;
        List<Base_language_installDTO> list = base_language_installMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_language_install-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_language_install-Get')")
	@ApiOperation(value = "查询数据集", tags = {"安装语言" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_language_installs/searchdefault")
	public ResponseEntity<Page<Base_language_installDTO>> searchDefault(@RequestBody Base_language_installSearchContext context) {
        Page<Base_language_install> domains = base_language_installService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_language_installMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

