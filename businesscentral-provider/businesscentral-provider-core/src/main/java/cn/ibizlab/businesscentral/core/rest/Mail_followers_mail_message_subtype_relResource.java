package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_followers_mail_message_subtype_rel;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_followers_mail_message_subtype_relService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_followers_mail_message_subtype_relSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"关注消息类型" })
@RestController("Core-mail_followers_mail_message_subtype_rel")
@RequestMapping("")
public class Mail_followers_mail_message_subtype_relResource {

    @Autowired
    public IMail_followers_mail_message_subtype_relService mail_followers_mail_message_subtype_relService;

    @Autowired
    @Lazy
    public Mail_followers_mail_message_subtype_relMapping mail_followers_mail_message_subtype_relMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers_mail_message_subtype_rel-Create-all')")
    @ApiOperation(value = "新建关注消息类型", tags = {"关注消息类型" },  notes = "新建关注消息类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_followers_mail_message_subtype_rels")
    public ResponseEntity<Mail_followers_mail_message_subtype_relDTO> create(@Validated @RequestBody Mail_followers_mail_message_subtype_relDTO mail_followers_mail_message_subtype_reldto) {
        Mail_followers_mail_message_subtype_rel domain = mail_followers_mail_message_subtype_relMapping.toDomain(mail_followers_mail_message_subtype_reldto);
		mail_followers_mail_message_subtype_relService.create(domain);
        Mail_followers_mail_message_subtype_relDTO dto = mail_followers_mail_message_subtype_relMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers_mail_message_subtype_rel-Create-all')")
    @ApiOperation(value = "批量新建关注消息类型", tags = {"关注消息类型" },  notes = "批量新建关注消息类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_followers_mail_message_subtype_rels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_followers_mail_message_subtype_relDTO> mail_followers_mail_message_subtype_reldtos) {
        mail_followers_mail_message_subtype_relService.createBatch(mail_followers_mail_message_subtype_relMapping.toDomain(mail_followers_mail_message_subtype_reldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers_mail_message_subtype_rel-Update-all')")
    @ApiOperation(value = "更新关注消息类型", tags = {"关注消息类型" },  notes = "更新关注消息类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_followers_mail_message_subtype_rels/{mail_followers_mail_message_subtype_rel_id}")
    public ResponseEntity<Mail_followers_mail_message_subtype_relDTO> update(@PathVariable("mail_followers_mail_message_subtype_rel_id") String mail_followers_mail_message_subtype_rel_id, @RequestBody Mail_followers_mail_message_subtype_relDTO mail_followers_mail_message_subtype_reldto) {
		Mail_followers_mail_message_subtype_rel domain  = mail_followers_mail_message_subtype_relMapping.toDomain(mail_followers_mail_message_subtype_reldto);
        domain .setId(mail_followers_mail_message_subtype_rel_id);
		mail_followers_mail_message_subtype_relService.update(domain );
		Mail_followers_mail_message_subtype_relDTO dto = mail_followers_mail_message_subtype_relMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers_mail_message_subtype_rel-Update-all')")
    @ApiOperation(value = "批量更新关注消息类型", tags = {"关注消息类型" },  notes = "批量更新关注消息类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_followers_mail_message_subtype_rels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_followers_mail_message_subtype_relDTO> mail_followers_mail_message_subtype_reldtos) {
        mail_followers_mail_message_subtype_relService.updateBatch(mail_followers_mail_message_subtype_relMapping.toDomain(mail_followers_mail_message_subtype_reldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers_mail_message_subtype_rel-Remove-all')")
    @ApiOperation(value = "删除关注消息类型", tags = {"关注消息类型" },  notes = "删除关注消息类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_followers_mail_message_subtype_rels/{mail_followers_mail_message_subtype_rel_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_followers_mail_message_subtype_rel_id") String mail_followers_mail_message_subtype_rel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_followers_mail_message_subtype_relService.remove(mail_followers_mail_message_subtype_rel_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers_mail_message_subtype_rel-Remove-all')")
    @ApiOperation(value = "批量删除关注消息类型", tags = {"关注消息类型" },  notes = "批量删除关注消息类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_followers_mail_message_subtype_rels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        mail_followers_mail_message_subtype_relService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers_mail_message_subtype_rel-Get-all')")
    @ApiOperation(value = "获取关注消息类型", tags = {"关注消息类型" },  notes = "获取关注消息类型")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_followers_mail_message_subtype_rels/{mail_followers_mail_message_subtype_rel_id}")
    public ResponseEntity<Mail_followers_mail_message_subtype_relDTO> get(@PathVariable("mail_followers_mail_message_subtype_rel_id") String mail_followers_mail_message_subtype_rel_id) {
        Mail_followers_mail_message_subtype_rel domain = mail_followers_mail_message_subtype_relService.get(mail_followers_mail_message_subtype_rel_id);
        Mail_followers_mail_message_subtype_relDTO dto = mail_followers_mail_message_subtype_relMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取关注消息类型草稿", tags = {"关注消息类型" },  notes = "获取关注消息类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_followers_mail_message_subtype_rels/getdraft")
    public ResponseEntity<Mail_followers_mail_message_subtype_relDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_followers_mail_message_subtype_relMapping.toDto(mail_followers_mail_message_subtype_relService.getDraft(new Mail_followers_mail_message_subtype_rel())));
    }

    @ApiOperation(value = "检查关注消息类型", tags = {"关注消息类型" },  notes = "检查关注消息类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_followers_mail_message_subtype_rels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_followers_mail_message_subtype_relDTO mail_followers_mail_message_subtype_reldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_followers_mail_message_subtype_relService.checkKey(mail_followers_mail_message_subtype_relMapping.toDomain(mail_followers_mail_message_subtype_reldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers_mail_message_subtype_rel-Save-all')")
    @ApiOperation(value = "保存关注消息类型", tags = {"关注消息类型" },  notes = "保存关注消息类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_followers_mail_message_subtype_rels/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_followers_mail_message_subtype_relDTO mail_followers_mail_message_subtype_reldto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_followers_mail_message_subtype_relService.save(mail_followers_mail_message_subtype_relMapping.toDomain(mail_followers_mail_message_subtype_reldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers_mail_message_subtype_rel-Save-all')")
    @ApiOperation(value = "批量保存关注消息类型", tags = {"关注消息类型" },  notes = "批量保存关注消息类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_followers_mail_message_subtype_rels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_followers_mail_message_subtype_relDTO> mail_followers_mail_message_subtype_reldtos) {
        mail_followers_mail_message_subtype_relService.saveBatch(mail_followers_mail_message_subtype_relMapping.toDomain(mail_followers_mail_message_subtype_reldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers_mail_message_subtype_rel-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"关注消息类型" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_followers_mail_message_subtype_rels/fetchdefault")
	public ResponseEntity<List<Mail_followers_mail_message_subtype_relDTO>> fetchDefault(Mail_followers_mail_message_subtype_relSearchContext context) {
        Page<Mail_followers_mail_message_subtype_rel> domains = mail_followers_mail_message_subtype_relService.searchDefault(context) ;
        List<Mail_followers_mail_message_subtype_relDTO> list = mail_followers_mail_message_subtype_relMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_followers_mail_message_subtype_rel-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"关注消息类型" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_followers_mail_message_subtype_rels/searchdefault")
	public ResponseEntity<Page<Mail_followers_mail_message_subtype_relDTO>> searchDefault(@RequestBody Mail_followers_mail_message_subtype_relSearchContext context) {
        Page<Mail_followers_mail_message_subtype_rel> domains = mail_followers_mail_message_subtype_relService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_followers_mail_message_subtype_relMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

