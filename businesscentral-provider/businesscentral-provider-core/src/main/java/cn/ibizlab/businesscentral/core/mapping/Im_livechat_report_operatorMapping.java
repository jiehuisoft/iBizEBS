package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_report_operator;
import cn.ibizlab.businesscentral.core.dto.Im_livechat_report_operatorDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreIm_livechat_report_operatorMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Im_livechat_report_operatorMapping extends MappingBase<Im_livechat_report_operatorDTO, Im_livechat_report_operator> {


}

