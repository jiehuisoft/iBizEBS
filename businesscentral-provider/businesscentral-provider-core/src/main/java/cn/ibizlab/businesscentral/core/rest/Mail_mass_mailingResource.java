package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailingService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailingSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"群发邮件" })
@RestController("Core-mail_mass_mailing")
@RequestMapping("")
public class Mail_mass_mailingResource {

    @Autowired
    public IMail_mass_mailingService mail_mass_mailingService;

    @Autowired
    @Lazy
    public Mail_mass_mailingMapping mail_mass_mailingMapping;

    @PreAuthorize("hasPermission(this.mail_mass_mailingMapping.toDomain(#mail_mass_mailingdto),'iBizBusinessCentral-Mail_mass_mailing-Create')")
    @ApiOperation(value = "新建群发邮件", tags = {"群发邮件" },  notes = "新建群发邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailings")
    public ResponseEntity<Mail_mass_mailingDTO> create(@Validated @RequestBody Mail_mass_mailingDTO mail_mass_mailingdto) {
        Mail_mass_mailing domain = mail_mass_mailingMapping.toDomain(mail_mass_mailingdto);
		mail_mass_mailingService.create(domain);
        Mail_mass_mailingDTO dto = mail_mass_mailingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailingMapping.toDomain(#mail_mass_mailingdtos),'iBizBusinessCentral-Mail_mass_mailing-Create')")
    @ApiOperation(value = "批量新建群发邮件", tags = {"群发邮件" },  notes = "批量新建群发邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailingDTO> mail_mass_mailingdtos) {
        mail_mass_mailingService.createBatch(mail_mass_mailingMapping.toDomain(mail_mass_mailingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_mass_mailing" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_mass_mailingService.get(#mail_mass_mailing_id),'iBizBusinessCentral-Mail_mass_mailing-Update')")
    @ApiOperation(value = "更新群发邮件", tags = {"群发邮件" },  notes = "更新群发邮件")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailings/{mail_mass_mailing_id}")
    public ResponseEntity<Mail_mass_mailingDTO> update(@PathVariable("mail_mass_mailing_id") Long mail_mass_mailing_id, @RequestBody Mail_mass_mailingDTO mail_mass_mailingdto) {
		Mail_mass_mailing domain  = mail_mass_mailingMapping.toDomain(mail_mass_mailingdto);
        domain .setId(mail_mass_mailing_id);
		mail_mass_mailingService.update(domain );
		Mail_mass_mailingDTO dto = mail_mass_mailingMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailingService.getMailMassMailingByEntities(this.mail_mass_mailingMapping.toDomain(#mail_mass_mailingdtos)),'iBizBusinessCentral-Mail_mass_mailing-Update')")
    @ApiOperation(value = "批量更新群发邮件", tags = {"群发邮件" },  notes = "批量更新群发邮件")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailingDTO> mail_mass_mailingdtos) {
        mail_mass_mailingService.updateBatch(mail_mass_mailingMapping.toDomain(mail_mass_mailingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailingService.get(#mail_mass_mailing_id),'iBizBusinessCentral-Mail_mass_mailing-Remove')")
    @ApiOperation(value = "删除群发邮件", tags = {"群发邮件" },  notes = "删除群发邮件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailings/{mail_mass_mailing_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_id") Long mail_mass_mailing_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailingService.remove(mail_mass_mailing_id));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailingService.getMailMassMailingByIds(#ids),'iBizBusinessCentral-Mail_mass_mailing-Remove')")
    @ApiOperation(value = "批量删除群发邮件", tags = {"群发邮件" },  notes = "批量删除群发邮件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_mass_mailingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_mass_mailingMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_mass_mailing-Get')")
    @ApiOperation(value = "获取群发邮件", tags = {"群发邮件" },  notes = "获取群发邮件")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailings/{mail_mass_mailing_id}")
    public ResponseEntity<Mail_mass_mailingDTO> get(@PathVariable("mail_mass_mailing_id") Long mail_mass_mailing_id) {
        Mail_mass_mailing domain = mail_mass_mailingService.get(mail_mass_mailing_id);
        Mail_mass_mailingDTO dto = mail_mass_mailingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取群发邮件草稿", tags = {"群发邮件" },  notes = "获取群发邮件草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailings/getdraft")
    public ResponseEntity<Mail_mass_mailingDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailingMapping.toDto(mail_mass_mailingService.getDraft(new Mail_mass_mailing())));
    }

    @ApiOperation(value = "检查群发邮件", tags = {"群发邮件" },  notes = "检查群发邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailings/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_mass_mailingDTO mail_mass_mailingdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailingService.checkKey(mail_mass_mailingMapping.toDomain(mail_mass_mailingdto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailingMapping.toDomain(#mail_mass_mailingdto),'iBizBusinessCentral-Mail_mass_mailing-Save')")
    @ApiOperation(value = "保存群发邮件", tags = {"群发邮件" },  notes = "保存群发邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailings/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_mass_mailingDTO mail_mass_mailingdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailingService.save(mail_mass_mailingMapping.toDomain(mail_mass_mailingdto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailingMapping.toDomain(#mail_mass_mailingdtos),'iBizBusinessCentral-Mail_mass_mailing-Save')")
    @ApiOperation(value = "批量保存群发邮件", tags = {"群发邮件" },  notes = "批量保存群发邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailings/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_mass_mailingDTO> mail_mass_mailingdtos) {
        mail_mass_mailingService.saveBatch(mail_mass_mailingMapping.toDomain(mail_mass_mailingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing-Get')")
	@ApiOperation(value = "获取数据集", tags = {"群发邮件" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailings/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailingDTO>> fetchDefault(Mail_mass_mailingSearchContext context) {
        Page<Mail_mass_mailing> domains = mail_mass_mailingService.searchDefault(context) ;
        List<Mail_mass_mailingDTO> list = mail_mass_mailingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing-Get')")
	@ApiOperation(value = "查询数据集", tags = {"群发邮件" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_mass_mailings/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailingDTO>> searchDefault(@RequestBody Mail_mass_mailingSearchContext context) {
        Page<Mail_mass_mailing> domains = mail_mass_mailingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

