package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_survey;
import cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_surveyService;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_surveySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"问卷" })
@RestController("Core-survey_survey")
@RequestMapping("")
public class Survey_surveyResource {

    @Autowired
    public ISurvey_surveyService survey_surveyService;

    @Autowired
    @Lazy
    public Survey_surveyMapping survey_surveyMapping;

    @PreAuthorize("hasPermission(this.survey_surveyMapping.toDomain(#survey_surveydto),'iBizBusinessCentral-Survey_survey-Create')")
    @ApiOperation(value = "新建问卷", tags = {"问卷" },  notes = "新建问卷")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_surveys")
    public ResponseEntity<Survey_surveyDTO> create(@Validated @RequestBody Survey_surveyDTO survey_surveydto) {
        Survey_survey domain = survey_surveyMapping.toDomain(survey_surveydto);
		survey_surveyService.create(domain);
        Survey_surveyDTO dto = survey_surveyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_surveyMapping.toDomain(#survey_surveydtos),'iBizBusinessCentral-Survey_survey-Create')")
    @ApiOperation(value = "批量新建问卷", tags = {"问卷" },  notes = "批量新建问卷")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_surveys/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_surveyDTO> survey_surveydtos) {
        survey_surveyService.createBatch(survey_surveyMapping.toDomain(survey_surveydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "survey_survey" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.survey_surveyService.get(#survey_survey_id),'iBizBusinessCentral-Survey_survey-Update')")
    @ApiOperation(value = "更新问卷", tags = {"问卷" },  notes = "更新问卷")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_surveys/{survey_survey_id}")
    public ResponseEntity<Survey_surveyDTO> update(@PathVariable("survey_survey_id") Long survey_survey_id, @RequestBody Survey_surveyDTO survey_surveydto) {
		Survey_survey domain  = survey_surveyMapping.toDomain(survey_surveydto);
        domain .setId(survey_survey_id);
		survey_surveyService.update(domain );
		Survey_surveyDTO dto = survey_surveyMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_surveyService.getSurveySurveyByEntities(this.survey_surveyMapping.toDomain(#survey_surveydtos)),'iBizBusinessCentral-Survey_survey-Update')")
    @ApiOperation(value = "批量更新问卷", tags = {"问卷" },  notes = "批量更新问卷")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_surveys/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_surveyDTO> survey_surveydtos) {
        survey_surveyService.updateBatch(survey_surveyMapping.toDomain(survey_surveydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.survey_surveyService.get(#survey_survey_id),'iBizBusinessCentral-Survey_survey-Remove')")
    @ApiOperation(value = "删除问卷", tags = {"问卷" },  notes = "删除问卷")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_surveys/{survey_survey_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("survey_survey_id") Long survey_survey_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_surveyService.remove(survey_survey_id));
    }

    @PreAuthorize("hasPermission(this.survey_surveyService.getSurveySurveyByIds(#ids),'iBizBusinessCentral-Survey_survey-Remove')")
    @ApiOperation(value = "批量删除问卷", tags = {"问卷" },  notes = "批量删除问卷")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_surveys/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        survey_surveyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.survey_surveyMapping.toDomain(returnObject.body),'iBizBusinessCentral-Survey_survey-Get')")
    @ApiOperation(value = "获取问卷", tags = {"问卷" },  notes = "获取问卷")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_surveys/{survey_survey_id}")
    public ResponseEntity<Survey_surveyDTO> get(@PathVariable("survey_survey_id") Long survey_survey_id) {
        Survey_survey domain = survey_surveyService.get(survey_survey_id);
        Survey_surveyDTO dto = survey_surveyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取问卷草稿", tags = {"问卷" },  notes = "获取问卷草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_surveys/getdraft")
    public ResponseEntity<Survey_surveyDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(survey_surveyMapping.toDto(survey_surveyService.getDraft(new Survey_survey())));
    }

    @ApiOperation(value = "检查问卷", tags = {"问卷" },  notes = "检查问卷")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_surveys/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Survey_surveyDTO survey_surveydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(survey_surveyService.checkKey(survey_surveyMapping.toDomain(survey_surveydto)));
    }

    @PreAuthorize("hasPermission(this.survey_surveyMapping.toDomain(#survey_surveydto),'iBizBusinessCentral-Survey_survey-Save')")
    @ApiOperation(value = "保存问卷", tags = {"问卷" },  notes = "保存问卷")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_surveys/save")
    public ResponseEntity<Boolean> save(@RequestBody Survey_surveyDTO survey_surveydto) {
        return ResponseEntity.status(HttpStatus.OK).body(survey_surveyService.save(survey_surveyMapping.toDomain(survey_surveydto)));
    }

    @PreAuthorize("hasPermission(this.survey_surveyMapping.toDomain(#survey_surveydtos),'iBizBusinessCentral-Survey_survey-Save')")
    @ApiOperation(value = "批量保存问卷", tags = {"问卷" },  notes = "批量保存问卷")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_surveys/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Survey_surveyDTO> survey_surveydtos) {
        survey_surveyService.saveBatch(survey_surveyMapping.toDomain(survey_surveydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_survey-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_survey-Get')")
	@ApiOperation(value = "获取数据集", tags = {"问卷" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/survey_surveys/fetchdefault")
	public ResponseEntity<List<Survey_surveyDTO>> fetchDefault(Survey_surveySearchContext context) {
        Page<Survey_survey> domains = survey_surveyService.searchDefault(context) ;
        List<Survey_surveyDTO> list = survey_surveyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_survey-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_survey-Get')")
	@ApiOperation(value = "查询数据集", tags = {"问卷" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/survey_surveys/searchdefault")
	public ResponseEntity<Page<Survey_surveyDTO>> searchDefault(@RequestBody Survey_surveySearchContext context) {
        Page<Survey_survey> domains = survey_surveyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_surveyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

