package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_wizard_user;
import cn.ibizlab.businesscentral.core.dto.Portal_wizard_userDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CorePortal_wizard_userMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Portal_wizard_userMapping extends MappingBase<Portal_wizard_userDTO, Portal_wizard_user> {


}

