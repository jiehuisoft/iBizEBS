package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar_attendance;
import cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendar_attendanceService;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_calendar_attendanceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工作细节" })
@RestController("Core-resource_calendar_attendance")
@RequestMapping("")
public class Resource_calendar_attendanceResource {

    @Autowired
    public IResource_calendar_attendanceService resource_calendar_attendanceService;

    @Autowired
    @Lazy
    public Resource_calendar_attendanceMapping resource_calendar_attendanceMapping;

    @PreAuthorize("hasPermission(this.resource_calendar_attendanceMapping.toDomain(#resource_calendar_attendancedto),'iBizBusinessCentral-Resource_calendar_attendance-Create')")
    @ApiOperation(value = "新建工作细节", tags = {"工作细节" },  notes = "新建工作细节")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances")
    public ResponseEntity<Resource_calendar_attendanceDTO> create(@Validated @RequestBody Resource_calendar_attendanceDTO resource_calendar_attendancedto) {
        Resource_calendar_attendance domain = resource_calendar_attendanceMapping.toDomain(resource_calendar_attendancedto);
		resource_calendar_attendanceService.create(domain);
        Resource_calendar_attendanceDTO dto = resource_calendar_attendanceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.resource_calendar_attendanceMapping.toDomain(#resource_calendar_attendancedtos),'iBizBusinessCentral-Resource_calendar_attendance-Create')")
    @ApiOperation(value = "批量新建工作细节", tags = {"工作细节" },  notes = "批量新建工作细节")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Resource_calendar_attendanceDTO> resource_calendar_attendancedtos) {
        resource_calendar_attendanceService.createBatch(resource_calendar_attendanceMapping.toDomain(resource_calendar_attendancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "resource_calendar_attendance" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.resource_calendar_attendanceService.get(#resource_calendar_attendance_id),'iBizBusinessCentral-Resource_calendar_attendance-Update')")
    @ApiOperation(value = "更新工作细节", tags = {"工作细节" },  notes = "更新工作细节")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_calendar_attendances/{resource_calendar_attendance_id}")
    public ResponseEntity<Resource_calendar_attendanceDTO> update(@PathVariable("resource_calendar_attendance_id") Long resource_calendar_attendance_id, @RequestBody Resource_calendar_attendanceDTO resource_calendar_attendancedto) {
		Resource_calendar_attendance domain  = resource_calendar_attendanceMapping.toDomain(resource_calendar_attendancedto);
        domain .setId(resource_calendar_attendance_id);
		resource_calendar_attendanceService.update(domain );
		Resource_calendar_attendanceDTO dto = resource_calendar_attendanceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.resource_calendar_attendanceService.getResourceCalendarAttendanceByEntities(this.resource_calendar_attendanceMapping.toDomain(#resource_calendar_attendancedtos)),'iBizBusinessCentral-Resource_calendar_attendance-Update')")
    @ApiOperation(value = "批量更新工作细节", tags = {"工作细节" },  notes = "批量更新工作细节")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_calendar_attendances/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_calendar_attendanceDTO> resource_calendar_attendancedtos) {
        resource_calendar_attendanceService.updateBatch(resource_calendar_attendanceMapping.toDomain(resource_calendar_attendancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.resource_calendar_attendanceService.get(#resource_calendar_attendance_id),'iBizBusinessCentral-Resource_calendar_attendance-Remove')")
    @ApiOperation(value = "删除工作细节", tags = {"工作细节" },  notes = "删除工作细节")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendar_attendances/{resource_calendar_attendance_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("resource_calendar_attendance_id") Long resource_calendar_attendance_id) {
         return ResponseEntity.status(HttpStatus.OK).body(resource_calendar_attendanceService.remove(resource_calendar_attendance_id));
    }

    @PreAuthorize("hasPermission(this.resource_calendar_attendanceService.getResourceCalendarAttendanceByIds(#ids),'iBizBusinessCentral-Resource_calendar_attendance-Remove')")
    @ApiOperation(value = "批量删除工作细节", tags = {"工作细节" },  notes = "批量删除工作细节")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendar_attendances/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        resource_calendar_attendanceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.resource_calendar_attendanceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Resource_calendar_attendance-Get')")
    @ApiOperation(value = "获取工作细节", tags = {"工作细节" },  notes = "获取工作细节")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_calendar_attendances/{resource_calendar_attendance_id}")
    public ResponseEntity<Resource_calendar_attendanceDTO> get(@PathVariable("resource_calendar_attendance_id") Long resource_calendar_attendance_id) {
        Resource_calendar_attendance domain = resource_calendar_attendanceService.get(resource_calendar_attendance_id);
        Resource_calendar_attendanceDTO dto = resource_calendar_attendanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工作细节草稿", tags = {"工作细节" },  notes = "获取工作细节草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_calendar_attendances/getdraft")
    public ResponseEntity<Resource_calendar_attendanceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(resource_calendar_attendanceMapping.toDto(resource_calendar_attendanceService.getDraft(new Resource_calendar_attendance())));
    }

    @ApiOperation(value = "检查工作细节", tags = {"工作细节" },  notes = "检查工作细节")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Resource_calendar_attendanceDTO resource_calendar_attendancedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(resource_calendar_attendanceService.checkKey(resource_calendar_attendanceMapping.toDomain(resource_calendar_attendancedto)));
    }

    @PreAuthorize("hasPermission(this.resource_calendar_attendanceMapping.toDomain(#resource_calendar_attendancedto),'iBizBusinessCentral-Resource_calendar_attendance-Save')")
    @ApiOperation(value = "保存工作细节", tags = {"工作细节" },  notes = "保存工作细节")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances/save")
    public ResponseEntity<Boolean> save(@RequestBody Resource_calendar_attendanceDTO resource_calendar_attendancedto) {
        return ResponseEntity.status(HttpStatus.OK).body(resource_calendar_attendanceService.save(resource_calendar_attendanceMapping.toDomain(resource_calendar_attendancedto)));
    }

    @PreAuthorize("hasPermission(this.resource_calendar_attendanceMapping.toDomain(#resource_calendar_attendancedtos),'iBizBusinessCentral-Resource_calendar_attendance-Save')")
    @ApiOperation(value = "批量保存工作细节", tags = {"工作细节" },  notes = "批量保存工作细节")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Resource_calendar_attendanceDTO> resource_calendar_attendancedtos) {
        resource_calendar_attendanceService.saveBatch(resource_calendar_attendanceMapping.toDomain(resource_calendar_attendancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_calendar_attendance-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Resource_calendar_attendance-Get')")
	@ApiOperation(value = "获取数据集", tags = {"工作细节" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/resource_calendar_attendances/fetchdefault")
	public ResponseEntity<List<Resource_calendar_attendanceDTO>> fetchDefault(Resource_calendar_attendanceSearchContext context) {
        Page<Resource_calendar_attendance> domains = resource_calendar_attendanceService.searchDefault(context) ;
        List<Resource_calendar_attendanceDTO> list = resource_calendar_attendanceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_calendar_attendance-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Resource_calendar_attendance-Get')")
	@ApiOperation(value = "查询数据集", tags = {"工作细节" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/resource_calendar_attendances/searchdefault")
	public ResponseEntity<Page<Resource_calendar_attendanceDTO>> searchDefault(@RequestBody Resource_calendar_attendanceSearchContext context) {
        Page<Resource_calendar_attendance> domains = resource_calendar_attendanceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(resource_calendar_attendanceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

