package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_contract;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_contractService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_contractSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"合同" })
@RestController("Core-hr_contract")
@RequestMapping("")
public class Hr_contractResource {

    @Autowired
    public IHr_contractService hr_contractService;

    @Autowired
    @Lazy
    public Hr_contractMapping hr_contractMapping;

    @PreAuthorize("hasPermission(this.hr_contractMapping.toDomain(#hr_contractdto),'iBizBusinessCentral-Hr_contract-Create')")
    @ApiOperation(value = "新建合同", tags = {"合同" },  notes = "新建合同")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contracts")
    public ResponseEntity<Hr_contractDTO> create(@Validated @RequestBody Hr_contractDTO hr_contractdto) {
        Hr_contract domain = hr_contractMapping.toDomain(hr_contractdto);
		hr_contractService.create(domain);
        Hr_contractDTO dto = hr_contractMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_contractMapping.toDomain(#hr_contractdtos),'iBizBusinessCentral-Hr_contract-Create')")
    @ApiOperation(value = "批量新建合同", tags = {"合同" },  notes = "批量新建合同")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contracts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_contractDTO> hr_contractdtos) {
        hr_contractService.createBatch(hr_contractMapping.toDomain(hr_contractdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_contract" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_contractService.get(#hr_contract_id),'iBizBusinessCentral-Hr_contract-Update')")
    @ApiOperation(value = "更新合同", tags = {"合同" },  notes = "更新合同")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_contracts/{hr_contract_id}")
    public ResponseEntity<Hr_contractDTO> update(@PathVariable("hr_contract_id") Long hr_contract_id, @RequestBody Hr_contractDTO hr_contractdto) {
		Hr_contract domain  = hr_contractMapping.toDomain(hr_contractdto);
        domain .setId(hr_contract_id);
		hr_contractService.update(domain );
		Hr_contractDTO dto = hr_contractMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_contractService.getHrContractByEntities(this.hr_contractMapping.toDomain(#hr_contractdtos)),'iBizBusinessCentral-Hr_contract-Update')")
    @ApiOperation(value = "批量更新合同", tags = {"合同" },  notes = "批量更新合同")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_contracts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_contractDTO> hr_contractdtos) {
        hr_contractService.updateBatch(hr_contractMapping.toDomain(hr_contractdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_contractService.get(#hr_contract_id),'iBizBusinessCentral-Hr_contract-Remove')")
    @ApiOperation(value = "删除合同", tags = {"合同" },  notes = "删除合同")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_contracts/{hr_contract_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_contract_id") Long hr_contract_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_contractService.remove(hr_contract_id));
    }

    @PreAuthorize("hasPermission(this.hr_contractService.getHrContractByIds(#ids),'iBizBusinessCentral-Hr_contract-Remove')")
    @ApiOperation(value = "批量删除合同", tags = {"合同" },  notes = "批量删除合同")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_contracts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_contractService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_contractMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_contract-Get')")
    @ApiOperation(value = "获取合同", tags = {"合同" },  notes = "获取合同")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_contracts/{hr_contract_id}")
    public ResponseEntity<Hr_contractDTO> get(@PathVariable("hr_contract_id") Long hr_contract_id) {
        Hr_contract domain = hr_contractService.get(hr_contract_id);
        Hr_contractDTO dto = hr_contractMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取合同草稿", tags = {"合同" },  notes = "获取合同草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_contracts/getdraft")
    public ResponseEntity<Hr_contractDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_contractMapping.toDto(hr_contractService.getDraft(new Hr_contract())));
    }

    @ApiOperation(value = "检查合同", tags = {"合同" },  notes = "检查合同")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contracts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_contractDTO hr_contractdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_contractService.checkKey(hr_contractMapping.toDomain(hr_contractdto)));
    }

    @PreAuthorize("hasPermission(this.hr_contractMapping.toDomain(#hr_contractdto),'iBizBusinessCentral-Hr_contract-Save')")
    @ApiOperation(value = "保存合同", tags = {"合同" },  notes = "保存合同")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contracts/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_contractDTO hr_contractdto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_contractService.save(hr_contractMapping.toDomain(hr_contractdto)));
    }

    @PreAuthorize("hasPermission(this.hr_contractMapping.toDomain(#hr_contractdtos),'iBizBusinessCentral-Hr_contract-Save')")
    @ApiOperation(value = "批量保存合同", tags = {"合同" },  notes = "批量保存合同")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contracts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_contractDTO> hr_contractdtos) {
        hr_contractService.saveBatch(hr_contractMapping.toDomain(hr_contractdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_contract-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_contract-Get')")
	@ApiOperation(value = "获取数据集", tags = {"合同" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_contracts/fetchdefault")
	public ResponseEntity<List<Hr_contractDTO>> fetchDefault(Hr_contractSearchContext context) {
        Page<Hr_contract> domains = hr_contractService.searchDefault(context) ;
        List<Hr_contractDTO> list = hr_contractMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_contract-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_contract-Get')")
	@ApiOperation(value = "查询数据集", tags = {"合同" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_contracts/searchdefault")
	public ResponseEntity<Page<Hr_contractDTO>> searchDefault(@RequestBody Hr_contractSearchContext context) {
        Page<Hr_contract> domains = hr_contractService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_contractMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasPermission(this.hr_contractMapping.toDomain(#hr_contractdto),'iBizBusinessCentral-Hr_contract-Create')")
    @ApiOperation(value = "根据员工建立合同", tags = {"合同" },  notes = "根据员工建立合同")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_contracts")
    public ResponseEntity<Hr_contractDTO> createByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_contractDTO hr_contractdto) {
        Hr_contract domain = hr_contractMapping.toDomain(hr_contractdto);
        domain.setEmployeeId(hr_employee_id);
		hr_contractService.create(domain);
        Hr_contractDTO dto = hr_contractMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_contractMapping.toDomain(#hr_contractdtos),'iBizBusinessCentral-Hr_contract-Create')")
    @ApiOperation(value = "根据员工批量建立合同", tags = {"合同" },  notes = "根据员工批量建立合同")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_contracts/batch")
    public ResponseEntity<Boolean> createBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Hr_contractDTO> hr_contractdtos) {
        List<Hr_contract> domainlist=hr_contractMapping.toDomain(hr_contractdtos);
        for(Hr_contract domain:domainlist){
            domain.setEmployeeId(hr_employee_id);
        }
        hr_contractService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_contract" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_contractService.get(#hr_contract_id),'iBizBusinessCentral-Hr_contract-Update')")
    @ApiOperation(value = "根据员工更新合同", tags = {"合同" },  notes = "根据员工更新合同")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/{hr_employee_id}/hr_contracts/{hr_contract_id}")
    public ResponseEntity<Hr_contractDTO> updateByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("hr_contract_id") Long hr_contract_id, @RequestBody Hr_contractDTO hr_contractdto) {
        Hr_contract domain = hr_contractMapping.toDomain(hr_contractdto);
        domain.setEmployeeId(hr_employee_id);
        domain.setId(hr_contract_id);
		hr_contractService.update(domain);
        Hr_contractDTO dto = hr_contractMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_contractService.getHrContractByEntities(this.hr_contractMapping.toDomain(#hr_contractdtos)),'iBizBusinessCentral-Hr_contract-Update')")
    @ApiOperation(value = "根据员工批量更新合同", tags = {"合同" },  notes = "根据员工批量更新合同")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/{hr_employee_id}/hr_contracts/batch")
    public ResponseEntity<Boolean> updateBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Hr_contractDTO> hr_contractdtos) {
        List<Hr_contract> domainlist=hr_contractMapping.toDomain(hr_contractdtos);
        for(Hr_contract domain:domainlist){
            domain.setEmployeeId(hr_employee_id);
        }
        hr_contractService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_contractService.get(#hr_contract_id),'iBizBusinessCentral-Hr_contract-Remove')")
    @ApiOperation(value = "根据员工删除合同", tags = {"合同" },  notes = "根据员工删除合同")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/{hr_employee_id}/hr_contracts/{hr_contract_id}")
    public ResponseEntity<Boolean> removeByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("hr_contract_id") Long hr_contract_id) {
		return ResponseEntity.status(HttpStatus.OK).body(hr_contractService.remove(hr_contract_id));
    }

    @PreAuthorize("hasPermission(this.hr_contractService.getHrContractByIds(#ids),'iBizBusinessCentral-Hr_contract-Remove')")
    @ApiOperation(value = "根据员工批量删除合同", tags = {"合同" },  notes = "根据员工批量删除合同")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/{hr_employee_id}/hr_contracts/batch")
    public ResponseEntity<Boolean> removeBatchByHr_employee(@RequestBody List<Long> ids) {
        hr_contractService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_contractMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_contract-Get')")
    @ApiOperation(value = "根据员工获取合同", tags = {"合同" },  notes = "根据员工获取合同")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_employees/{hr_employee_id}/hr_contracts/{hr_contract_id}")
    public ResponseEntity<Hr_contractDTO> getByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("hr_contract_id") Long hr_contract_id) {
        Hr_contract domain = hr_contractService.get(hr_contract_id);
        Hr_contractDTO dto = hr_contractMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据员工获取合同草稿", tags = {"合同" },  notes = "根据员工获取合同草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/hr_employees/{hr_employee_id}/hr_contracts/getdraft")
    public ResponseEntity<Hr_contractDTO> getDraftByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id) {
        Hr_contract domain = new Hr_contract();
        domain.setEmployeeId(hr_employee_id);
        return ResponseEntity.status(HttpStatus.OK).body(hr_contractMapping.toDto(hr_contractService.getDraft(domain)));
    }

    @ApiOperation(value = "根据员工检查合同", tags = {"合同" },  notes = "根据员工检查合同")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_contracts/checkkey")
    public ResponseEntity<Boolean> checkKeyByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_contractDTO hr_contractdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_contractService.checkKey(hr_contractMapping.toDomain(hr_contractdto)));
    }

    @PreAuthorize("hasPermission(this.hr_contractMapping.toDomain(#hr_contractdto),'iBizBusinessCentral-Hr_contract-Save')")
    @ApiOperation(value = "根据员工保存合同", tags = {"合同" },  notes = "根据员工保存合同")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_contracts/save")
    public ResponseEntity<Boolean> saveByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_contractDTO hr_contractdto) {
        Hr_contract domain = hr_contractMapping.toDomain(hr_contractdto);
        domain.setEmployeeId(hr_employee_id);
        return ResponseEntity.status(HttpStatus.OK).body(hr_contractService.save(domain));
    }

    @PreAuthorize("hasPermission(this.hr_contractMapping.toDomain(#hr_contractdtos),'iBizBusinessCentral-Hr_contract-Save')")
    @ApiOperation(value = "根据员工批量保存合同", tags = {"合同" },  notes = "根据员工批量保存合同")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_contracts/savebatch")
    public ResponseEntity<Boolean> saveBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Hr_contractDTO> hr_contractdtos) {
        List<Hr_contract> domainlist=hr_contractMapping.toDomain(hr_contractdtos);
        for(Hr_contract domain:domainlist){
             domain.setEmployeeId(hr_employee_id);
        }
        hr_contractService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_contract-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_contract-Get')")
	@ApiOperation(value = "根据员工获取数据集", tags = {"合同" } ,notes = "根据员工获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_employees/{hr_employee_id}/hr_contracts/fetchdefault")
	public ResponseEntity<List<Hr_contractDTO>> fetchHr_contractDefaultByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id,Hr_contractSearchContext context) {
        context.setN_employee_id_eq(hr_employee_id);
        Page<Hr_contract> domains = hr_contractService.searchDefault(context) ;
        List<Hr_contractDTO> list = hr_contractMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_contract-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_contract-Get')")
	@ApiOperation(value = "根据员工查询数据集", tags = {"合同" } ,notes = "根据员工查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_employees/{hr_employee_id}/hr_contracts/searchdefault")
	public ResponseEntity<Page<Hr_contractDTO>> searchHr_contractDefaultByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_contractSearchContext context) {
        context.setN_employee_id_eq(hr_employee_id);
        Page<Hr_contract> domains = hr_contractService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_contractMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

