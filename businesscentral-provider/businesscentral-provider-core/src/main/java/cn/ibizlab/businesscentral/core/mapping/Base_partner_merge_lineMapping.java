package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_partner_merge_line;
import cn.ibizlab.businesscentral.core.dto.Base_partner_merge_lineDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreBase_partner_merge_lineMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Base_partner_merge_lineMapping extends MappingBase<Base_partner_merge_lineDTO, Base_partner_merge_line> {


}

