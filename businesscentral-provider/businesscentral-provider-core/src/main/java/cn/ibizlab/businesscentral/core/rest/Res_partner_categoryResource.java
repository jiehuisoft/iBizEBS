package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_category;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_categoryService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_categorySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"业务伙伴标签" })
@RestController("Core-res_partner_category")
@RequestMapping("")
public class Res_partner_categoryResource {

    @Autowired
    public IRes_partner_categoryService res_partner_categoryService;

    @Autowired
    @Lazy
    public Res_partner_categoryMapping res_partner_categoryMapping;

    @PreAuthorize("hasPermission(this.res_partner_categoryMapping.toDomain(#res_partner_categorydto),'iBizBusinessCentral-Res_partner_category-Create')")
    @ApiOperation(value = "新建业务伙伴标签", tags = {"业务伙伴标签" },  notes = "新建业务伙伴标签")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_categories")
    public ResponseEntity<Res_partner_categoryDTO> create(@Validated @RequestBody Res_partner_categoryDTO res_partner_categorydto) {
        Res_partner_category domain = res_partner_categoryMapping.toDomain(res_partner_categorydto);
		res_partner_categoryService.create(domain);
        Res_partner_categoryDTO dto = res_partner_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partner_categoryMapping.toDomain(#res_partner_categorydtos),'iBizBusinessCentral-Res_partner_category-Create')")
    @ApiOperation(value = "批量新建业务伙伴标签", tags = {"业务伙伴标签" },  notes = "批量新建业务伙伴标签")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_partner_categoryDTO> res_partner_categorydtos) {
        res_partner_categoryService.createBatch(res_partner_categoryMapping.toDomain(res_partner_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_partner_category" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_partner_categoryService.get(#res_partner_category_id),'iBizBusinessCentral-Res_partner_category-Update')")
    @ApiOperation(value = "更新业务伙伴标签", tags = {"业务伙伴标签" },  notes = "更新业务伙伴标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_categories/{res_partner_category_id}")
    public ResponseEntity<Res_partner_categoryDTO> update(@PathVariable("res_partner_category_id") Long res_partner_category_id, @RequestBody Res_partner_categoryDTO res_partner_categorydto) {
		Res_partner_category domain  = res_partner_categoryMapping.toDomain(res_partner_categorydto);
        domain .setId(res_partner_category_id);
		res_partner_categoryService.update(domain );
		Res_partner_categoryDTO dto = res_partner_categoryMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partner_categoryService.getResPartnerCategoryByEntities(this.res_partner_categoryMapping.toDomain(#res_partner_categorydtos)),'iBizBusinessCentral-Res_partner_category-Update')")
    @ApiOperation(value = "批量更新业务伙伴标签", tags = {"业务伙伴标签" },  notes = "批量更新业务伙伴标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_categoryDTO> res_partner_categorydtos) {
        res_partner_categoryService.updateBatch(res_partner_categoryMapping.toDomain(res_partner_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_partner_categoryService.get(#res_partner_category_id),'iBizBusinessCentral-Res_partner_category-Remove')")
    @ApiOperation(value = "删除业务伙伴标签", tags = {"业务伙伴标签" },  notes = "删除业务伙伴标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_categories/{res_partner_category_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_category_id") Long res_partner_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_partner_categoryService.remove(res_partner_category_id));
    }

    @PreAuthorize("hasPermission(this.res_partner_categoryService.getResPartnerCategoryByIds(#ids),'iBizBusinessCentral-Res_partner_category-Remove')")
    @ApiOperation(value = "批量删除业务伙伴标签", tags = {"业务伙伴标签" },  notes = "批量删除业务伙伴标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_partner_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_partner_categoryMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_partner_category-Get')")
    @ApiOperation(value = "获取业务伙伴标签", tags = {"业务伙伴标签" },  notes = "获取业务伙伴标签")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_categories/{res_partner_category_id}")
    public ResponseEntity<Res_partner_categoryDTO> get(@PathVariable("res_partner_category_id") Long res_partner_category_id) {
        Res_partner_category domain = res_partner_categoryService.get(res_partner_category_id);
        Res_partner_categoryDTO dto = res_partner_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取业务伙伴标签草稿", tags = {"业务伙伴标签" },  notes = "获取业务伙伴标签草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_categories/getdraft")
    public ResponseEntity<Res_partner_categoryDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_categoryMapping.toDto(res_partner_categoryService.getDraft(new Res_partner_category())));
    }

    @ApiOperation(value = "检查业务伙伴标签", tags = {"业务伙伴标签" },  notes = "检查业务伙伴标签")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_categories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_partner_categoryDTO res_partner_categorydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_partner_categoryService.checkKey(res_partner_categoryMapping.toDomain(res_partner_categorydto)));
    }

    @PreAuthorize("hasPermission(this.res_partner_categoryMapping.toDomain(#res_partner_categorydto),'iBizBusinessCentral-Res_partner_category-Save')")
    @ApiOperation(value = "保存业务伙伴标签", tags = {"业务伙伴标签" },  notes = "保存业务伙伴标签")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_categories/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_partner_categoryDTO res_partner_categorydto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_categoryService.save(res_partner_categoryMapping.toDomain(res_partner_categorydto)));
    }

    @PreAuthorize("hasPermission(this.res_partner_categoryMapping.toDomain(#res_partner_categorydtos),'iBizBusinessCentral-Res_partner_category-Save')")
    @ApiOperation(value = "批量保存业务伙伴标签", tags = {"业务伙伴标签" },  notes = "批量保存业务伙伴标签")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_categories/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_partner_categoryDTO> res_partner_categorydtos) {
        res_partner_categoryService.saveBatch(res_partner_categoryMapping.toDomain(res_partner_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner_category-Get')")
	@ApiOperation(value = "获取数据集", tags = {"业务伙伴标签" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_categories/fetchdefault")
	public ResponseEntity<List<Res_partner_categoryDTO>> fetchDefault(Res_partner_categorySearchContext context) {
        Page<Res_partner_category> domains = res_partner_categoryService.searchDefault(context) ;
        List<Res_partner_categoryDTO> list = res_partner_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner_category-Get')")
	@ApiOperation(value = "查询数据集", tags = {"业务伙伴标签" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_partner_categories/searchdefault")
	public ResponseEntity<Page<Res_partner_categoryDTO>> searchDefault(@RequestBody Res_partner_categorySearchContext context) {
        Page<Res_partner_category> domains = res_partner_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

