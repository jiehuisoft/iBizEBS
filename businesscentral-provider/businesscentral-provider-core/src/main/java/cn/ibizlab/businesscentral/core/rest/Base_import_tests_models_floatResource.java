package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_float;
import cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_floatService;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_floatSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"测试:基本导入模型浮动" })
@RestController("Core-base_import_tests_models_float")
@RequestMapping("")
public class Base_import_tests_models_floatResource {

    @Autowired
    public IBase_import_tests_models_floatService base_import_tests_models_floatService;

    @Autowired
    @Lazy
    public Base_import_tests_models_floatMapping base_import_tests_models_floatMapping;

    @PreAuthorize("hasPermission(this.base_import_tests_models_floatMapping.toDomain(#base_import_tests_models_floatdto),'iBizBusinessCentral-Base_import_tests_models_float-Create')")
    @ApiOperation(value = "新建测试:基本导入模型浮动", tags = {"测试:基本导入模型浮动" },  notes = "新建测试:基本导入模型浮动")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats")
    public ResponseEntity<Base_import_tests_models_floatDTO> create(@Validated @RequestBody Base_import_tests_models_floatDTO base_import_tests_models_floatdto) {
        Base_import_tests_models_float domain = base_import_tests_models_floatMapping.toDomain(base_import_tests_models_floatdto);
		base_import_tests_models_floatService.create(domain);
        Base_import_tests_models_floatDTO dto = base_import_tests_models_floatMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_floatMapping.toDomain(#base_import_tests_models_floatdtos),'iBizBusinessCentral-Base_import_tests_models_float-Create')")
    @ApiOperation(value = "批量新建测试:基本导入模型浮动", tags = {"测试:基本导入模型浮动" },  notes = "批量新建测试:基本导入模型浮动")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_floatDTO> base_import_tests_models_floatdtos) {
        base_import_tests_models_floatService.createBatch(base_import_tests_models_floatMapping.toDomain(base_import_tests_models_floatdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_import_tests_models_float" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_import_tests_models_floatService.get(#base_import_tests_models_float_id),'iBizBusinessCentral-Base_import_tests_models_float-Update')")
    @ApiOperation(value = "更新测试:基本导入模型浮动", tags = {"测试:基本导入模型浮动" },  notes = "更新测试:基本导入模型浮动")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_floats/{base_import_tests_models_float_id}")
    public ResponseEntity<Base_import_tests_models_floatDTO> update(@PathVariable("base_import_tests_models_float_id") Long base_import_tests_models_float_id, @RequestBody Base_import_tests_models_floatDTO base_import_tests_models_floatdto) {
		Base_import_tests_models_float domain  = base_import_tests_models_floatMapping.toDomain(base_import_tests_models_floatdto);
        domain .setId(base_import_tests_models_float_id);
		base_import_tests_models_floatService.update(domain );
		Base_import_tests_models_floatDTO dto = base_import_tests_models_floatMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_floatService.getBaseImportTestsModelsFloatByEntities(this.base_import_tests_models_floatMapping.toDomain(#base_import_tests_models_floatdtos)),'iBizBusinessCentral-Base_import_tests_models_float-Update')")
    @ApiOperation(value = "批量更新测试:基本导入模型浮动", tags = {"测试:基本导入模型浮动" },  notes = "批量更新测试:基本导入模型浮动")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_floats/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_floatDTO> base_import_tests_models_floatdtos) {
        base_import_tests_models_floatService.updateBatch(base_import_tests_models_floatMapping.toDomain(base_import_tests_models_floatdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_floatService.get(#base_import_tests_models_float_id),'iBizBusinessCentral-Base_import_tests_models_float-Remove')")
    @ApiOperation(value = "删除测试:基本导入模型浮动", tags = {"测试:基本导入模型浮动" },  notes = "删除测试:基本导入模型浮动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_floats/{base_import_tests_models_float_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_float_id") Long base_import_tests_models_float_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_floatService.remove(base_import_tests_models_float_id));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_floatService.getBaseImportTestsModelsFloatByIds(#ids),'iBizBusinessCentral-Base_import_tests_models_float-Remove')")
    @ApiOperation(value = "批量删除测试:基本导入模型浮动", tags = {"测试:基本导入模型浮动" },  notes = "批量删除测试:基本导入模型浮动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_floats/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_import_tests_models_floatService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_import_tests_models_floatMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_import_tests_models_float-Get')")
    @ApiOperation(value = "获取测试:基本导入模型浮动", tags = {"测试:基本导入模型浮动" },  notes = "获取测试:基本导入模型浮动")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_floats/{base_import_tests_models_float_id}")
    public ResponseEntity<Base_import_tests_models_floatDTO> get(@PathVariable("base_import_tests_models_float_id") Long base_import_tests_models_float_id) {
        Base_import_tests_models_float domain = base_import_tests_models_floatService.get(base_import_tests_models_float_id);
        Base_import_tests_models_floatDTO dto = base_import_tests_models_floatMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取测试:基本导入模型浮动草稿", tags = {"测试:基本导入模型浮动" },  notes = "获取测试:基本导入模型浮动草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_floats/getdraft")
    public ResponseEntity<Base_import_tests_models_floatDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_floatMapping.toDto(base_import_tests_models_floatService.getDraft(new Base_import_tests_models_float())));
    }

    @ApiOperation(value = "检查测试:基本导入模型浮动", tags = {"测试:基本导入模型浮动" },  notes = "检查测试:基本导入模型浮动")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_import_tests_models_floatDTO base_import_tests_models_floatdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_floatService.checkKey(base_import_tests_models_floatMapping.toDomain(base_import_tests_models_floatdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_floatMapping.toDomain(#base_import_tests_models_floatdto),'iBizBusinessCentral-Base_import_tests_models_float-Save')")
    @ApiOperation(value = "保存测试:基本导入模型浮动", tags = {"测试:基本导入模型浮动" },  notes = "保存测试:基本导入模型浮动")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_import_tests_models_floatDTO base_import_tests_models_floatdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_floatService.save(base_import_tests_models_floatMapping.toDomain(base_import_tests_models_floatdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_floatMapping.toDomain(#base_import_tests_models_floatdtos),'iBizBusinessCentral-Base_import_tests_models_float-Save')")
    @ApiOperation(value = "批量保存测试:基本导入模型浮动", tags = {"测试:基本导入模型浮动" },  notes = "批量保存测试:基本导入模型浮动")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_import_tests_models_floatDTO> base_import_tests_models_floatdtos) {
        base_import_tests_models_floatService.saveBatch(base_import_tests_models_floatMapping.toDomain(base_import_tests_models_floatdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_float-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_float-Get')")
	@ApiOperation(value = "获取数据集", tags = {"测试:基本导入模型浮动" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_floats/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_floatDTO>> fetchDefault(Base_import_tests_models_floatSearchContext context) {
        Page<Base_import_tests_models_float> domains = base_import_tests_models_floatService.searchDefault(context) ;
        List<Base_import_tests_models_floatDTO> list = base_import_tests_models_floatMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_float-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_float-Get')")
	@ApiOperation(value = "查询数据集", tags = {"测试:基本导入模型浮动" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_import_tests_models_floats/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_floatDTO>> searchDefault(@RequestBody Base_import_tests_models_floatSearchContext context) {
        Page<Base_import_tests_models_float> domains = base_import_tests_models_floatService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_floatMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

