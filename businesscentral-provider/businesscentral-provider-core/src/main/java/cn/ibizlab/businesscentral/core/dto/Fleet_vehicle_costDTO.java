package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Fleet_vehicle_costDTO]
 */
@Data
public class Fleet_vehicle_costDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ODOMETER]
     *
     */
    @JSONField(name = "odometer")
    @JsonProperty("odometer")
    private Double odometer;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [COST_TYPE]
     *
     */
    @JSONField(name = "cost_type")
    @JsonProperty("cost_type")
    @NotBlank(message = "[费用所属类别]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String costType;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String description;

    /**
     * 属性 [AUTO_GENERATED]
     *
     */
    @JSONField(name = "auto_generated")
    @JsonProperty("auto_generated")
    private Boolean autoGenerated;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [COST_IDS]
     *
     */
    @JSONField(name = "cost_ids")
    @JsonProperty("cost_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String costIds;

    /**
     * 属性 [CONTRACT_ID_TEXT]
     *
     */
    @JSONField(name = "contract_id_text")
    @JsonProperty("contract_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String contractIdText;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String parentIdText;

    /**
     * 属性 [COST_SUBTYPE_ID_TEXT]
     *
     */
    @JSONField(name = "cost_subtype_id_text")
    @JsonProperty("cost_subtype_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String costSubtypeIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [ODOMETER_UNIT]
     *
     */
    @JSONField(name = "odometer_unit")
    @JsonProperty("odometer_unit")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String odometerUnit;

    /**
     * 属性 [ODOMETER_ID_TEXT]
     *
     */
    @JSONField(name = "odometer_id_text")
    @JsonProperty("odometer_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String odometerIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [COST_SUBTYPE_ID]
     *
     */
    @JSONField(name = "cost_subtype_id")
    @JsonProperty("cost_subtype_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long costSubtypeId;

    /**
     * 属性 [CONTRACT_ID]
     *
     */
    @JSONField(name = "contract_id")
    @JsonProperty("contract_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long contractId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /**
     * 属性 [VEHICLE_ID]
     *
     */
    @JSONField(name = "vehicle_id")
    @JsonProperty("vehicle_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[车辆]不允许为空!")
    private Long vehicleId;

    /**
     * 属性 [ODOMETER_ID]
     *
     */
    @JSONField(name = "odometer_id")
    @JsonProperty("odometer_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long odometerId;


    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [COST_TYPE]
     */
    public void setCostType(String  costType){
        this.costType = costType ;
        this.modify("cost_type",costType);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [AUTO_GENERATED]
     */
    public void setAutoGenerated(Boolean  autoGenerated){
        this.autoGenerated = autoGenerated ;
        this.modify("auto_generated",autoGenerated);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [COST_SUBTYPE_ID]
     */
    public void setCostSubtypeId(Long  costSubtypeId){
        this.costSubtypeId = costSubtypeId ;
        this.modify("cost_subtype_id",costSubtypeId);
    }

    /**
     * 设置 [CONTRACT_ID]
     */
    public void setContractId(Long  contractId){
        this.contractId = contractId ;
        this.modify("contract_id",contractId);
    }

    /**
     * 设置 [PARENT_ID]
     */
    public void setParentId(Long  parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [VEHICLE_ID]
     */
    public void setVehicleId(Long  vehicleId){
        this.vehicleId = vehicleId ;
        this.modify("vehicle_id",vehicleId);
    }

    /**
     * 设置 [ODOMETER_ID]
     */
    public void setOdometerId(Long  odometerId){
        this.odometerId = odometerId ;
        this.modify("odometer_id",odometerId);
    }


}


