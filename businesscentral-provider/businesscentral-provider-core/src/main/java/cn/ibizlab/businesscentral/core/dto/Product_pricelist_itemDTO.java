package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Product_pricelist_itemDTO]
 */
@Data
public class Product_pricelist_itemDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [FIXED_PRICE]
     *
     */
    @JSONField(name = "fixed_price")
    @JsonProperty("fixed_price")
    private Double fixedPrice;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String price;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [BASE]
     *
     */
    @JSONField(name = "base")
    @JsonProperty("base")
    @NotBlank(message = "[基于]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String base;

    /**
     * 属性 [DATE_END]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_end" , format="yyyy-MM-dd")
    @JsonProperty("date_end")
    private Timestamp dateEnd;

    /**
     * 属性 [PERCENT_PRICE]
     *
     */
    @JSONField(name = "percent_price")
    @JsonProperty("percent_price")
    private Double percentPrice;

    /**
     * 属性 [PRICE_ROUND]
     *
     */
    @JSONField(name = "price_round")
    @JsonProperty("price_round")
    private Double priceRound;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [COMPUTE_PRICE]
     *
     */
    @JSONField(name = "compute_price")
    @JsonProperty("compute_price")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String computePrice;

    /**
     * 属性 [PRICE_MIN_MARGIN]
     *
     */
    @JSONField(name = "price_min_margin")
    @JsonProperty("price_min_margin")
    private Double priceMinMargin;

    /**
     * 属性 [DATE_START]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd")
    @JsonProperty("date_start")
    private Timestamp dateStart;

    /**
     * 属性 [PRICE_MAX_MARGIN]
     *
     */
    @JSONField(name = "price_max_margin")
    @JsonProperty("price_max_margin")
    private Double priceMaxMargin;

    /**
     * 属性 [APPLIED_ON]
     *
     */
    @JSONField(name = "applied_on")
    @JsonProperty("applied_on")
    @NotBlank(message = "[应用于]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String appliedOn;

    /**
     * 属性 [MIN_QUANTITY]
     *
     */
    @JSONField(name = "min_quantity")
    @JsonProperty("min_quantity")
    private Integer minQuantity;

    /**
     * 属性 [PRICE_SURCHARGE]
     *
     */
    @JSONField(name = "price_surcharge")
    @JsonProperty("price_surcharge")
    private Double priceSurcharge;

    /**
     * 属性 [PRICE_DISCOUNT]
     *
     */
    @JSONField(name = "price_discount")
    @JsonProperty("price_discount")
    private Double priceDiscount;

    /**
     * 属性 [PRICELIST_ID_TEXT]
     *
     */
    @JSONField(name = "pricelist_id_text")
    @JsonProperty("pricelist_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pricelistIdText;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currencyIdText;

    /**
     * 属性 [CATEG_ID_TEXT]
     *
     */
    @JSONField(name = "categ_id_text")
    @JsonProperty("categ_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String categIdText;

    /**
     * 属性 [BASE_PRICELIST_ID_TEXT]
     *
     */
    @JSONField(name = "base_pricelist_id_text")
    @JsonProperty("base_pricelist_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String basePricelistIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [PRODUCT_TMPL_ID_TEXT]
     *
     */
    @JSONField(name = "product_tmpl_id_text")
    @JsonProperty("product_tmpl_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productTmplIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;

    /**
     * 属性 [PRICELIST_ID]
     *
     */
    @JSONField(name = "pricelist_id")
    @JsonProperty("pricelist_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pricelistId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [BASE_PRICELIST_ID]
     *
     */
    @JSONField(name = "base_pricelist_id")
    @JsonProperty("base_pricelist_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long basePricelistId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long currencyId;

    /**
     * 属性 [CATEG_ID]
     *
     */
    @JSONField(name = "categ_id")
    @JsonProperty("categ_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long categId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productTmplId;


    /**
     * 设置 [FIXED_PRICE]
     */
    public void setFixedPrice(Double  fixedPrice){
        this.fixedPrice = fixedPrice ;
        this.modify("fixed_price",fixedPrice);
    }

    /**
     * 设置 [BASE]
     */
    public void setBase(String  base){
        this.base = base ;
        this.modify("base",base);
    }

    /**
     * 设置 [DATE_END]
     */
    public void setDateEnd(Timestamp  dateEnd){
        this.dateEnd = dateEnd ;
        this.modify("date_end",dateEnd);
    }

    /**
     * 设置 [PERCENT_PRICE]
     */
    public void setPercentPrice(Double  percentPrice){
        this.percentPrice = percentPrice ;
        this.modify("percent_price",percentPrice);
    }

    /**
     * 设置 [PRICE_ROUND]
     */
    public void setPriceRound(Double  priceRound){
        this.priceRound = priceRound ;
        this.modify("price_round",priceRound);
    }

    /**
     * 设置 [COMPUTE_PRICE]
     */
    public void setComputePrice(String  computePrice){
        this.computePrice = computePrice ;
        this.modify("compute_price",computePrice);
    }

    /**
     * 设置 [PRICE_MIN_MARGIN]
     */
    public void setPriceMinMargin(Double  priceMinMargin){
        this.priceMinMargin = priceMinMargin ;
        this.modify("price_min_margin",priceMinMargin);
    }

    /**
     * 设置 [DATE_START]
     */
    public void setDateStart(Timestamp  dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }

    /**
     * 设置 [PRICE_MAX_MARGIN]
     */
    public void setPriceMaxMargin(Double  priceMaxMargin){
        this.priceMaxMargin = priceMaxMargin ;
        this.modify("price_max_margin",priceMaxMargin);
    }

    /**
     * 设置 [APPLIED_ON]
     */
    public void setAppliedOn(String  appliedOn){
        this.appliedOn = appliedOn ;
        this.modify("applied_on",appliedOn);
    }

    /**
     * 设置 [MIN_QUANTITY]
     */
    public void setMinQuantity(Integer  minQuantity){
        this.minQuantity = minQuantity ;
        this.modify("min_quantity",minQuantity);
    }

    /**
     * 设置 [PRICE_SURCHARGE]
     */
    public void setPriceSurcharge(Double  priceSurcharge){
        this.priceSurcharge = priceSurcharge ;
        this.modify("price_surcharge",priceSurcharge);
    }

    /**
     * 设置 [PRICE_DISCOUNT]
     */
    public void setPriceDiscount(Double  priceDiscount){
        this.priceDiscount = priceDiscount ;
        this.modify("price_discount",priceDiscount);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [PRICELIST_ID]
     */
    public void setPricelistId(Long  pricelistId){
        this.pricelistId = pricelistId ;
        this.modify("pricelist_id",pricelistId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [BASE_PRICELIST_ID]
     */
    public void setBasePricelistId(Long  basePricelistId){
        this.basePricelistId = basePricelistId ;
        this.modify("base_pricelist_id",basePricelistId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [CATEG_ID]
     */
    public void setCategId(Long  categId){
        this.categId = categId ;
        this.modify("categ_id",categId);
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    public void setProductTmplId(Long  productTmplId){
        this.productTmplId = productTmplId ;
        this.modify("product_tmpl_id",productTmplId);
    }


}


