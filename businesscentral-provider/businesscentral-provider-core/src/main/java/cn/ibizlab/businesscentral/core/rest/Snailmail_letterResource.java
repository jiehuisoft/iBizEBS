package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_snailmail.domain.Snailmail_letter;
import cn.ibizlab.businesscentral.core.odoo_snailmail.service.ISnailmail_letterService;
import cn.ibizlab.businesscentral.core.odoo_snailmail.filter.Snailmail_letterSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Snailmail 信纸" })
@RestController("Core-snailmail_letter")
@RequestMapping("")
public class Snailmail_letterResource {

    @Autowired
    public ISnailmail_letterService snailmail_letterService;

    @Autowired
    @Lazy
    public Snailmail_letterMapping snailmail_letterMapping;

    @PreAuthorize("hasPermission(this.snailmail_letterMapping.toDomain(#snailmail_letterdto),'iBizBusinessCentral-Snailmail_letter-Create')")
    @ApiOperation(value = "新建Snailmail 信纸", tags = {"Snailmail 信纸" },  notes = "新建Snailmail 信纸")
	@RequestMapping(method = RequestMethod.POST, value = "/snailmail_letters")
    public ResponseEntity<Snailmail_letterDTO> create(@Validated @RequestBody Snailmail_letterDTO snailmail_letterdto) {
        Snailmail_letter domain = snailmail_letterMapping.toDomain(snailmail_letterdto);
		snailmail_letterService.create(domain);
        Snailmail_letterDTO dto = snailmail_letterMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.snailmail_letterMapping.toDomain(#snailmail_letterdtos),'iBizBusinessCentral-Snailmail_letter-Create')")
    @ApiOperation(value = "批量新建Snailmail 信纸", tags = {"Snailmail 信纸" },  notes = "批量新建Snailmail 信纸")
	@RequestMapping(method = RequestMethod.POST, value = "/snailmail_letters/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Snailmail_letterDTO> snailmail_letterdtos) {
        snailmail_letterService.createBatch(snailmail_letterMapping.toDomain(snailmail_letterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "snailmail_letter" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.snailmail_letterService.get(#snailmail_letter_id),'iBizBusinessCentral-Snailmail_letter-Update')")
    @ApiOperation(value = "更新Snailmail 信纸", tags = {"Snailmail 信纸" },  notes = "更新Snailmail 信纸")
	@RequestMapping(method = RequestMethod.PUT, value = "/snailmail_letters/{snailmail_letter_id}")
    public ResponseEntity<Snailmail_letterDTO> update(@PathVariable("snailmail_letter_id") Long snailmail_letter_id, @RequestBody Snailmail_letterDTO snailmail_letterdto) {
		Snailmail_letter domain  = snailmail_letterMapping.toDomain(snailmail_letterdto);
        domain .setId(snailmail_letter_id);
		snailmail_letterService.update(domain );
		Snailmail_letterDTO dto = snailmail_letterMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.snailmail_letterService.getSnailmailLetterByEntities(this.snailmail_letterMapping.toDomain(#snailmail_letterdtos)),'iBizBusinessCentral-Snailmail_letter-Update')")
    @ApiOperation(value = "批量更新Snailmail 信纸", tags = {"Snailmail 信纸" },  notes = "批量更新Snailmail 信纸")
	@RequestMapping(method = RequestMethod.PUT, value = "/snailmail_letters/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Snailmail_letterDTO> snailmail_letterdtos) {
        snailmail_letterService.updateBatch(snailmail_letterMapping.toDomain(snailmail_letterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.snailmail_letterService.get(#snailmail_letter_id),'iBizBusinessCentral-Snailmail_letter-Remove')")
    @ApiOperation(value = "删除Snailmail 信纸", tags = {"Snailmail 信纸" },  notes = "删除Snailmail 信纸")
	@RequestMapping(method = RequestMethod.DELETE, value = "/snailmail_letters/{snailmail_letter_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("snailmail_letter_id") Long snailmail_letter_id) {
         return ResponseEntity.status(HttpStatus.OK).body(snailmail_letterService.remove(snailmail_letter_id));
    }

    @PreAuthorize("hasPermission(this.snailmail_letterService.getSnailmailLetterByIds(#ids),'iBizBusinessCentral-Snailmail_letter-Remove')")
    @ApiOperation(value = "批量删除Snailmail 信纸", tags = {"Snailmail 信纸" },  notes = "批量删除Snailmail 信纸")
	@RequestMapping(method = RequestMethod.DELETE, value = "/snailmail_letters/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        snailmail_letterService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.snailmail_letterMapping.toDomain(returnObject.body),'iBizBusinessCentral-Snailmail_letter-Get')")
    @ApiOperation(value = "获取Snailmail 信纸", tags = {"Snailmail 信纸" },  notes = "获取Snailmail 信纸")
	@RequestMapping(method = RequestMethod.GET, value = "/snailmail_letters/{snailmail_letter_id}")
    public ResponseEntity<Snailmail_letterDTO> get(@PathVariable("snailmail_letter_id") Long snailmail_letter_id) {
        Snailmail_letter domain = snailmail_letterService.get(snailmail_letter_id);
        Snailmail_letterDTO dto = snailmail_letterMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Snailmail 信纸草稿", tags = {"Snailmail 信纸" },  notes = "获取Snailmail 信纸草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/snailmail_letters/getdraft")
    public ResponseEntity<Snailmail_letterDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(snailmail_letterMapping.toDto(snailmail_letterService.getDraft(new Snailmail_letter())));
    }

    @ApiOperation(value = "检查Snailmail 信纸", tags = {"Snailmail 信纸" },  notes = "检查Snailmail 信纸")
	@RequestMapping(method = RequestMethod.POST, value = "/snailmail_letters/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Snailmail_letterDTO snailmail_letterdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(snailmail_letterService.checkKey(snailmail_letterMapping.toDomain(snailmail_letterdto)));
    }

    @PreAuthorize("hasPermission(this.snailmail_letterMapping.toDomain(#snailmail_letterdto),'iBizBusinessCentral-Snailmail_letter-Save')")
    @ApiOperation(value = "保存Snailmail 信纸", tags = {"Snailmail 信纸" },  notes = "保存Snailmail 信纸")
	@RequestMapping(method = RequestMethod.POST, value = "/snailmail_letters/save")
    public ResponseEntity<Boolean> save(@RequestBody Snailmail_letterDTO snailmail_letterdto) {
        return ResponseEntity.status(HttpStatus.OK).body(snailmail_letterService.save(snailmail_letterMapping.toDomain(snailmail_letterdto)));
    }

    @PreAuthorize("hasPermission(this.snailmail_letterMapping.toDomain(#snailmail_letterdtos),'iBizBusinessCentral-Snailmail_letter-Save')")
    @ApiOperation(value = "批量保存Snailmail 信纸", tags = {"Snailmail 信纸" },  notes = "批量保存Snailmail 信纸")
	@RequestMapping(method = RequestMethod.POST, value = "/snailmail_letters/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Snailmail_letterDTO> snailmail_letterdtos) {
        snailmail_letterService.saveBatch(snailmail_letterMapping.toDomain(snailmail_letterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Snailmail_letter-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Snailmail_letter-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Snailmail 信纸" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/snailmail_letters/fetchdefault")
	public ResponseEntity<List<Snailmail_letterDTO>> fetchDefault(Snailmail_letterSearchContext context) {
        Page<Snailmail_letter> domains = snailmail_letterService.searchDefault(context) ;
        List<Snailmail_letterDTO> list = snailmail_letterMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Snailmail_letter-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Snailmail_letter-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Snailmail 信纸" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/snailmail_letters/searchdefault")
	public ResponseEntity<Page<Snailmail_letterDTO>> searchDefault(@RequestBody Snailmail_letterSearchContext context) {
        Page<Snailmail_letter> domains = snailmail_letterService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(snailmail_letterMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

