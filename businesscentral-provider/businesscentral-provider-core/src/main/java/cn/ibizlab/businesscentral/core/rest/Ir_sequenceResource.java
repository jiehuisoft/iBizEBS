package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_sequence;
import cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_sequenceService;
import cn.ibizlab.businesscentral.core.odoo_ir.filter.Ir_sequenceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"序列" })
@RestController("Core-ir_sequence")
@RequestMapping("")
public class Ir_sequenceResource {

    @Autowired
    public IIr_sequenceService ir_sequenceService;

    @Autowired
    @Lazy
    public Ir_sequenceMapping ir_sequenceMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Create-all')")
    @ApiOperation(value = "新建序列", tags = {"序列" },  notes = "新建序列")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences")
    public ResponseEntity<Ir_sequenceDTO> create(@Validated @RequestBody Ir_sequenceDTO ir_sequencedto) {
        Ir_sequence domain = ir_sequenceMapping.toDomain(ir_sequencedto);
		ir_sequenceService.create(domain);
        Ir_sequenceDTO dto = ir_sequenceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Create-all')")
    @ApiOperation(value = "批量新建序列", tags = {"序列" },  notes = "批量新建序列")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Ir_sequenceDTO> ir_sequencedtos) {
        ir_sequenceService.createBatch(ir_sequenceMapping.toDomain(ir_sequencedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Update-all')")
    @ApiOperation(value = "更新序列", tags = {"序列" },  notes = "更新序列")
	@RequestMapping(method = RequestMethod.PUT, value = "/ir_sequences/{ir_sequence_id}")
    public ResponseEntity<Ir_sequenceDTO> update(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody Ir_sequenceDTO ir_sequencedto) {
		Ir_sequence domain  = ir_sequenceMapping.toDomain(ir_sequencedto);
        domain .setId(ir_sequence_id);
		ir_sequenceService.update(domain );
		Ir_sequenceDTO dto = ir_sequenceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Update-all')")
    @ApiOperation(value = "批量更新序列", tags = {"序列" },  notes = "批量更新序列")
	@RequestMapping(method = RequestMethod.PUT, value = "/ir_sequences/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Ir_sequenceDTO> ir_sequencedtos) {
        ir_sequenceService.updateBatch(ir_sequenceMapping.toDomain(ir_sequencedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Remove-all')")
    @ApiOperation(value = "删除序列", tags = {"序列" },  notes = "删除序列")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ir_sequences/{ir_sequence_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("ir_sequence_id") Long ir_sequence_id) {
         return ResponseEntity.status(HttpStatus.OK).body(ir_sequenceService.remove(ir_sequence_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Remove-all')")
    @ApiOperation(value = "批量删除序列", tags = {"序列" },  notes = "批量删除序列")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ir_sequences/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        ir_sequenceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Get-all')")
    @ApiOperation(value = "获取序列", tags = {"序列" },  notes = "获取序列")
	@RequestMapping(method = RequestMethod.GET, value = "/ir_sequences/{ir_sequence_id}")
    public ResponseEntity<Ir_sequenceDTO> get(@PathVariable("ir_sequence_id") Long ir_sequence_id) {
        Ir_sequence domain = ir_sequenceService.get(ir_sequence_id);
        Ir_sequenceDTO dto = ir_sequenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取序列草稿", tags = {"序列" },  notes = "获取序列草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/ir_sequences/getdraft")
    public ResponseEntity<Ir_sequenceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequenceMapping.toDto(ir_sequenceService.getDraft(new Ir_sequence())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Alter_sequence-all')")
    @ApiOperation(value = "修改序列", tags = {"序列" },  notes = "修改序列")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/{ir_sequence_id}/alter_sequence")
    public ResponseEntity<Ir_sequenceDTO> alter_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody Ir_sequenceDTO ir_sequencedto) {
        Ir_sequence domain = ir_sequenceMapping.toDomain(ir_sequencedto);
        domain.setId(ir_sequence_id);
        domain = ir_sequenceService.alter_sequence(domain);
        ir_sequencedto = ir_sequenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequencedto);
    }

    @ApiOperation(value = "检查序列", tags = {"序列" },  notes = "检查序列")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Ir_sequenceDTO ir_sequencedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(ir_sequenceService.checkKey(ir_sequenceMapping.toDomain(ir_sequencedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Create_sequence-all')")
    @ApiOperation(value = "创建序列", tags = {"序列" },  notes = "创建序列")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/{ir_sequence_id}/create_sequence")
    public ResponseEntity<Ir_sequenceDTO> create_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody Ir_sequenceDTO ir_sequencedto) {
        Ir_sequence domain = ir_sequenceMapping.toDomain(ir_sequencedto);
        domain.setId(ir_sequence_id);
        domain = ir_sequenceService.create_sequence(domain);
        ir_sequencedto = ir_sequenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequencedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Drop_sequence-all')")
    @ApiOperation(value = "删除序列", tags = {"序列" },  notes = "删除序列")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/{ir_sequence_id}/drop_sequence")
    public ResponseEntity<Ir_sequenceDTO> drop_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody Ir_sequenceDTO ir_sequencedto) {
        Ir_sequence domain = ir_sequenceMapping.toDomain(ir_sequencedto);
        domain.setId(ir_sequence_id);
        domain = ir_sequenceService.drop_sequence(domain);
        ir_sequencedto = ir_sequenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequencedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Get_next-all')")
    @ApiOperation(value = "get_next", tags = {"序列" },  notes = "get_next")
	@RequestMapping(method = RequestMethod.GET, value = "/ir_sequences/{ir_sequence_id}/get_next")
    public ResponseEntity<Ir_sequenceDTO> get_next(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody Ir_sequenceDTO ir_sequencedto) {
        Ir_sequence domain = ir_sequenceMapping.toDomain(ir_sequencedto);
        domain.setId(ir_sequence_id);
        domain = ir_sequenceService.get_next(domain);
        ir_sequencedto = ir_sequenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequencedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Get_next_by_code-all')")
    @ApiOperation(value = "get_next_by_code", tags = {"序列" },  notes = "get_next_by_code")
	@RequestMapping(method = RequestMethod.GET, value = "/ir_sequences/{ir_sequence_id}/get_next_by_code")
    public ResponseEntity<Ir_sequenceDTO> get_next_by_code(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody Ir_sequenceDTO ir_sequencedto) {
        Ir_sequence domain = ir_sequenceMapping.toDomain(ir_sequencedto);
        domain.setId(ir_sequence_id);
        domain = ir_sequenceService.get_next_by_code(domain);
        ir_sequencedto = ir_sequenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequencedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Predict_nextval-all')")
    @ApiOperation(value = "预言nextval", tags = {"序列" },  notes = "预言nextval")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/{ir_sequence_id}/predict_nextval")
    public ResponseEntity<Ir_sequenceDTO> predict_nextval(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody Ir_sequenceDTO ir_sequencedto) {
        Ir_sequence domain = ir_sequenceMapping.toDomain(ir_sequencedto);
        domain.setId(ir_sequence_id);
        domain = ir_sequenceService.predict_nextval(domain);
        ir_sequencedto = ir_sequenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequencedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Save-all')")
    @ApiOperation(value = "保存序列", tags = {"序列" },  notes = "保存序列")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/save")
    public ResponseEntity<Boolean> save(@RequestBody Ir_sequenceDTO ir_sequencedto) {
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequenceService.save(ir_sequenceMapping.toDomain(ir_sequencedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Save-all')")
    @ApiOperation(value = "批量保存序列", tags = {"序列" },  notes = "批量保存序列")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Ir_sequenceDTO> ir_sequencedtos) {
        ir_sequenceService.saveBatch(ir_sequenceMapping.toDomain(ir_sequencedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Select_nextval-all')")
    @ApiOperation(value = "获取nextval", tags = {"序列" },  notes = "获取nextval")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/{ir_sequence_id}/select_nextval")
    public ResponseEntity<Ir_sequenceDTO> select_nextval(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody Ir_sequenceDTO ir_sequencedto) {
        Ir_sequence domain = ir_sequenceMapping.toDomain(ir_sequencedto);
        domain.setId(ir_sequence_id);
        domain = ir_sequenceService.select_nextval(domain);
        ir_sequencedto = ir_sequenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequencedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-Update_nogap-all')")
    @ApiOperation(value = "update_nogap", tags = {"序列" },  notes = "update_nogap")
	@RequestMapping(method = RequestMethod.PUT, value = "/ir_sequences/{ir_sequence_id}/update_nogap")
    public ResponseEntity<Ir_sequenceDTO> update_nogap(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody Ir_sequenceDTO ir_sequencedto) {
        Ir_sequence domain = ir_sequenceMapping.toDomain(ir_sequencedto);
        domain.setId(ir_sequence_id);
        domain = ir_sequenceService.update_nogap(domain);
        ir_sequencedto = ir_sequenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequencedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"序列" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/ir_sequences/fetchdefault")
	public ResponseEntity<List<Ir_sequenceDTO>> fetchDefault(Ir_sequenceSearchContext context) {
        Page<Ir_sequence> domains = ir_sequenceService.searchDefault(context) ;
        List<Ir_sequenceDTO> list = ir_sequenceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"序列" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/ir_sequences/searchdefault")
	public ResponseEntity<Page<Ir_sequenceDTO>> searchDefault(@RequestBody Ir_sequenceSearchContext context) {
        Page<Ir_sequence> domains = ir_sequenceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(ir_sequenceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

