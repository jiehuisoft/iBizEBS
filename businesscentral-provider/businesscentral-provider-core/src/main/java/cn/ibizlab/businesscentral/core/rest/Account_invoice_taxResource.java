package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_tax;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_taxService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_taxSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"发票税率" })
@RestController("Core-account_invoice_tax")
@RequestMapping("")
public class Account_invoice_taxResource {

    @Autowired
    public IAccount_invoice_taxService account_invoice_taxService;

    @Autowired
    @Lazy
    public Account_invoice_taxMapping account_invoice_taxMapping;

    @PreAuthorize("hasPermission(this.account_invoice_taxMapping.toDomain(#account_invoice_taxdto),'iBizBusinessCentral-Account_invoice_tax-Create')")
    @ApiOperation(value = "新建发票税率", tags = {"发票税率" },  notes = "新建发票税率")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes")
    public ResponseEntity<Account_invoice_taxDTO> create(@Validated @RequestBody Account_invoice_taxDTO account_invoice_taxdto) {
        Account_invoice_tax domain = account_invoice_taxMapping.toDomain(account_invoice_taxdto);
		account_invoice_taxService.create(domain);
        Account_invoice_taxDTO dto = account_invoice_taxMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoice_taxMapping.toDomain(#account_invoice_taxdtos),'iBizBusinessCentral-Account_invoice_tax-Create')")
    @ApiOperation(value = "批量新建发票税率", tags = {"发票税率" },  notes = "批量新建发票税率")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_taxDTO> account_invoice_taxdtos) {
        account_invoice_taxService.createBatch(account_invoice_taxMapping.toDomain(account_invoice_taxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_invoice_tax" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_invoice_taxService.get(#account_invoice_tax_id),'iBizBusinessCentral-Account_invoice_tax-Update')")
    @ApiOperation(value = "更新发票税率", tags = {"发票税率" },  notes = "更新发票税率")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_taxes/{account_invoice_tax_id}")
    public ResponseEntity<Account_invoice_taxDTO> update(@PathVariable("account_invoice_tax_id") Long account_invoice_tax_id, @RequestBody Account_invoice_taxDTO account_invoice_taxdto) {
		Account_invoice_tax domain  = account_invoice_taxMapping.toDomain(account_invoice_taxdto);
        domain .setId(account_invoice_tax_id);
		account_invoice_taxService.update(domain );
		Account_invoice_taxDTO dto = account_invoice_taxMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoice_taxService.getAccountInvoiceTaxByEntities(this.account_invoice_taxMapping.toDomain(#account_invoice_taxdtos)),'iBizBusinessCentral-Account_invoice_tax-Update')")
    @ApiOperation(value = "批量更新发票税率", tags = {"发票税率" },  notes = "批量更新发票税率")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_taxes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_taxDTO> account_invoice_taxdtos) {
        account_invoice_taxService.updateBatch(account_invoice_taxMapping.toDomain(account_invoice_taxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_invoice_taxService.get(#account_invoice_tax_id),'iBizBusinessCentral-Account_invoice_tax-Remove')")
    @ApiOperation(value = "删除发票税率", tags = {"发票税率" },  notes = "删除发票税率")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_taxes/{account_invoice_tax_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_tax_id") Long account_invoice_tax_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_taxService.remove(account_invoice_tax_id));
    }

    @PreAuthorize("hasPermission(this.account_invoice_taxService.getAccountInvoiceTaxByIds(#ids),'iBizBusinessCentral-Account_invoice_tax-Remove')")
    @ApiOperation(value = "批量删除发票税率", tags = {"发票税率" },  notes = "批量删除发票税率")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_taxes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_invoice_taxService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_invoice_taxMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_invoice_tax-Get')")
    @ApiOperation(value = "获取发票税率", tags = {"发票税率" },  notes = "获取发票税率")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_taxes/{account_invoice_tax_id}")
    public ResponseEntity<Account_invoice_taxDTO> get(@PathVariable("account_invoice_tax_id") Long account_invoice_tax_id) {
        Account_invoice_tax domain = account_invoice_taxService.get(account_invoice_tax_id);
        Account_invoice_taxDTO dto = account_invoice_taxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取发票税率草稿", tags = {"发票税率" },  notes = "获取发票税率草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_taxes/getdraft")
    public ResponseEntity<Account_invoice_taxDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_taxMapping.toDto(account_invoice_taxService.getDraft(new Account_invoice_tax())));
    }

    @ApiOperation(value = "检查发票税率", tags = {"发票税率" },  notes = "检查发票税率")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoice_taxDTO account_invoice_taxdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_invoice_taxService.checkKey(account_invoice_taxMapping.toDomain(account_invoice_taxdto)));
    }

    @PreAuthorize("hasPermission(this.account_invoice_taxMapping.toDomain(#account_invoice_taxdto),'iBizBusinessCentral-Account_invoice_tax-Save')")
    @ApiOperation(value = "保存发票税率", tags = {"发票税率" },  notes = "保存发票税率")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_invoice_taxDTO account_invoice_taxdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_taxService.save(account_invoice_taxMapping.toDomain(account_invoice_taxdto)));
    }

    @PreAuthorize("hasPermission(this.account_invoice_taxMapping.toDomain(#account_invoice_taxdtos),'iBizBusinessCentral-Account_invoice_tax-Save')")
    @ApiOperation(value = "批量保存发票税率", tags = {"发票税率" },  notes = "批量保存发票税率")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_invoice_taxDTO> account_invoice_taxdtos) {
        account_invoice_taxService.saveBatch(account_invoice_taxMapping.toDomain(account_invoice_taxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_tax-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice_tax-Get')")
	@ApiOperation(value = "获取数据集", tags = {"发票税率" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_taxes/fetchdefault")
	public ResponseEntity<List<Account_invoice_taxDTO>> fetchDefault(Account_invoice_taxSearchContext context) {
        Page<Account_invoice_tax> domains = account_invoice_taxService.searchDefault(context) ;
        List<Account_invoice_taxDTO> list = account_invoice_taxMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_tax-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice_tax-Get')")
	@ApiOperation(value = "查询数据集", tags = {"发票税率" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_invoice_taxes/searchdefault")
	public ResponseEntity<Page<Account_invoice_taxDTO>> searchDefault(@RequestBody Account_invoice_taxSearchContext context) {
        Page<Account_invoice_tax> domains = account_invoice_taxService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_taxMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

