package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_question;
import cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_questionService;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_questionSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"调查问题" })
@RestController("Core-survey_question")
@RequestMapping("")
public class Survey_questionResource {

    @Autowired
    public ISurvey_questionService survey_questionService;

    @Autowired
    @Lazy
    public Survey_questionMapping survey_questionMapping;

    @PreAuthorize("hasPermission(this.survey_questionMapping.toDomain(#survey_questiondto),'iBizBusinessCentral-Survey_question-Create')")
    @ApiOperation(value = "新建调查问题", tags = {"调查问题" },  notes = "新建调查问题")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_questions")
    public ResponseEntity<Survey_questionDTO> create(@Validated @RequestBody Survey_questionDTO survey_questiondto) {
        Survey_question domain = survey_questionMapping.toDomain(survey_questiondto);
		survey_questionService.create(domain);
        Survey_questionDTO dto = survey_questionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_questionMapping.toDomain(#survey_questiondtos),'iBizBusinessCentral-Survey_question-Create')")
    @ApiOperation(value = "批量新建调查问题", tags = {"调查问题" },  notes = "批量新建调查问题")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_questions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_questionDTO> survey_questiondtos) {
        survey_questionService.createBatch(survey_questionMapping.toDomain(survey_questiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "survey_question" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.survey_questionService.get(#survey_question_id),'iBizBusinessCentral-Survey_question-Update')")
    @ApiOperation(value = "更新调查问题", tags = {"调查问题" },  notes = "更新调查问题")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_questions/{survey_question_id}")
    public ResponseEntity<Survey_questionDTO> update(@PathVariable("survey_question_id") Long survey_question_id, @RequestBody Survey_questionDTO survey_questiondto) {
		Survey_question domain  = survey_questionMapping.toDomain(survey_questiondto);
        domain .setId(survey_question_id);
		survey_questionService.update(domain );
		Survey_questionDTO dto = survey_questionMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_questionService.getSurveyQuestionByEntities(this.survey_questionMapping.toDomain(#survey_questiondtos)),'iBizBusinessCentral-Survey_question-Update')")
    @ApiOperation(value = "批量更新调查问题", tags = {"调查问题" },  notes = "批量更新调查问题")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_questions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_questionDTO> survey_questiondtos) {
        survey_questionService.updateBatch(survey_questionMapping.toDomain(survey_questiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.survey_questionService.get(#survey_question_id),'iBizBusinessCentral-Survey_question-Remove')")
    @ApiOperation(value = "删除调查问题", tags = {"调查问题" },  notes = "删除调查问题")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_questions/{survey_question_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("survey_question_id") Long survey_question_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_questionService.remove(survey_question_id));
    }

    @PreAuthorize("hasPermission(this.survey_questionService.getSurveyQuestionByIds(#ids),'iBizBusinessCentral-Survey_question-Remove')")
    @ApiOperation(value = "批量删除调查问题", tags = {"调查问题" },  notes = "批量删除调查问题")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_questions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        survey_questionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.survey_questionMapping.toDomain(returnObject.body),'iBizBusinessCentral-Survey_question-Get')")
    @ApiOperation(value = "获取调查问题", tags = {"调查问题" },  notes = "获取调查问题")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_questions/{survey_question_id}")
    public ResponseEntity<Survey_questionDTO> get(@PathVariable("survey_question_id") Long survey_question_id) {
        Survey_question domain = survey_questionService.get(survey_question_id);
        Survey_questionDTO dto = survey_questionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取调查问题草稿", tags = {"调查问题" },  notes = "获取调查问题草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_questions/getdraft")
    public ResponseEntity<Survey_questionDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(survey_questionMapping.toDto(survey_questionService.getDraft(new Survey_question())));
    }

    @ApiOperation(value = "检查调查问题", tags = {"调查问题" },  notes = "检查调查问题")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_questions/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Survey_questionDTO survey_questiondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(survey_questionService.checkKey(survey_questionMapping.toDomain(survey_questiondto)));
    }

    @PreAuthorize("hasPermission(this.survey_questionMapping.toDomain(#survey_questiondto),'iBizBusinessCentral-Survey_question-Save')")
    @ApiOperation(value = "保存调查问题", tags = {"调查问题" },  notes = "保存调查问题")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_questions/save")
    public ResponseEntity<Boolean> save(@RequestBody Survey_questionDTO survey_questiondto) {
        return ResponseEntity.status(HttpStatus.OK).body(survey_questionService.save(survey_questionMapping.toDomain(survey_questiondto)));
    }

    @PreAuthorize("hasPermission(this.survey_questionMapping.toDomain(#survey_questiondtos),'iBizBusinessCentral-Survey_question-Save')")
    @ApiOperation(value = "批量保存调查问题", tags = {"调查问题" },  notes = "批量保存调查问题")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_questions/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Survey_questionDTO> survey_questiondtos) {
        survey_questionService.saveBatch(survey_questionMapping.toDomain(survey_questiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_question-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_question-Get')")
	@ApiOperation(value = "获取数据集", tags = {"调查问题" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/survey_questions/fetchdefault")
	public ResponseEntity<List<Survey_questionDTO>> fetchDefault(Survey_questionSearchContext context) {
        Page<Survey_question> domains = survey_questionService.searchDefault(context) ;
        List<Survey_questionDTO> list = survey_questionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_question-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_question-Get')")
	@ApiOperation(value = "查询数据集", tags = {"调查问题" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/survey_questions/searchdefault")
	public ResponseEntity<Page<Survey_questionDTO>> searchDefault(@RequestBody Survey_questionSearchContext context) {
        Page<Survey_question> domains = survey_questionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_questionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

