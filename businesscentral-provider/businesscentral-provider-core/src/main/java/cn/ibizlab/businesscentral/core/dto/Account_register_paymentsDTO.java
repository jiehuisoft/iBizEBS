package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Account_register_paymentsDTO]
 */
@Data
public class Account_register_paymentsDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SHOW_COMMUNICATION_FIELD]
     *
     */
    @JSONField(name = "show_communication_field")
    @JsonProperty("show_communication_field")
    private Boolean showCommunicationField;

    /**
     * 属性 [PAYMENT_TYPE]
     *
     */
    @JSONField(name = "payment_type")
    @JsonProperty("payment_type")
    @NotBlank(message = "[付款类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String paymentType;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [PARTNER_TYPE]
     *
     */
    @JSONField(name = "partner_type")
    @JsonProperty("partner_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerType;

    /**
     * 属性 [COMMUNICATION]
     *
     */
    @JSONField(name = "communication")
    @JsonProperty("communication")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String communication;

    /**
     * 属性 [PAYMENT_DIFFERENCE]
     *
     */
    @JSONField(name = "payment_difference")
    @JsonProperty("payment_difference")
    private BigDecimal paymentDifference;

    /**
     * 属性 [SHOW_PARTNER_BANK_ACCOUNT]
     *
     */
    @JSONField(name = "show_partner_bank_account")
    @JsonProperty("show_partner_bank_account")
    private Boolean showPartnerBankAccount;

    /**
     * 属性 [GROUP_INVOICES]
     *
     */
    @JSONField(name = "group_invoices")
    @JsonProperty("group_invoices")
    private Boolean groupInvoices;

    /**
     * 属性 [HIDE_PAYMENT_METHOD]
     *
     */
    @JSONField(name = "hide_payment_method")
    @JsonProperty("hide_payment_method")
    private Boolean hidePaymentMethod;

    /**
     * 属性 [INVOICE_IDS]
     *
     */
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String invoiceIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PAYMENT_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "payment_date" , format="yyyy-MM-dd")
    @JsonProperty("payment_date")
    @NotNull(message = "[付款日期]不允许为空!")
    private Timestamp paymentDate;

    /**
     * 属性 [WRITEOFF_LABEL]
     *
     */
    @JSONField(name = "writeoff_label")
    @JsonProperty("writeoff_label")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String writeoffLabel;

    /**
     * 属性 [MULTI]
     *
     */
    @JSONField(name = "multi")
    @JsonProperty("multi")
    private Boolean multi;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [PAYMENT_DIFFERENCE_HANDLING]
     *
     */
    @JSONField(name = "payment_difference_handling")
    @JsonProperty("payment_difference_handling")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String paymentDifferenceHandling;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    @NotNull(message = "[付款金额]不允许为空!")
    private BigDecimal amount;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currencyIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [PAYMENT_METHOD_CODE]
     *
     */
    @JSONField(name = "payment_method_code")
    @JsonProperty("payment_method_code")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String paymentMethodCode;

    /**
     * 属性 [PAYMENT_METHOD_ID_TEXT]
     *
     */
    @JSONField(name = "payment_method_id_text")
    @JsonProperty("payment_method_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String paymentMethodIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String journalIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [WRITEOFF_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "writeoff_account_id_text")
    @JsonProperty("writeoff_account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeoffAccountIdText;

    /**
     * 属性 [WRITEOFF_ACCOUNT_ID]
     *
     */
    @JSONField(name = "writeoff_account_id")
    @JsonProperty("writeoff_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeoffAccountId;

    /**
     * 属性 [PARTNER_BANK_ACCOUNT_ID]
     *
     */
    @JSONField(name = "partner_bank_account_id")
    @JsonProperty("partner_bank_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerBankAccountId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[付款日记账]不允许为空!")
    private Long journalId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[币种]不允许为空!")
    private Long currencyId;

    /**
     * 属性 [PAYMENT_METHOD_ID]
     *
     */
    @JSONField(name = "payment_method_id")
    @JsonProperty("payment_method_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[付款方法类型]不允许为空!")
    private Long paymentMethodId;


    /**
     * 设置 [PAYMENT_TYPE]
     */
    public void setPaymentType(String  paymentType){
        this.paymentType = paymentType ;
        this.modify("payment_type",paymentType);
    }

    /**
     * 设置 [PARTNER_TYPE]
     */
    public void setPartnerType(String  partnerType){
        this.partnerType = partnerType ;
        this.modify("partner_type",partnerType);
    }

    /**
     * 设置 [COMMUNICATION]
     */
    public void setCommunication(String  communication){
        this.communication = communication ;
        this.modify("communication",communication);
    }

    /**
     * 设置 [GROUP_INVOICES]
     */
    public void setGroupInvoices(Boolean  groupInvoices){
        this.groupInvoices = groupInvoices ;
        this.modify("group_invoices",groupInvoices);
    }

    /**
     * 设置 [PAYMENT_DATE]
     */
    public void setPaymentDate(Timestamp  paymentDate){
        this.paymentDate = paymentDate ;
        this.modify("payment_date",paymentDate);
    }

    /**
     * 设置 [WRITEOFF_LABEL]
     */
    public void setWriteoffLabel(String  writeoffLabel){
        this.writeoffLabel = writeoffLabel ;
        this.modify("writeoff_label",writeoffLabel);
    }

    /**
     * 设置 [MULTI]
     */
    public void setMulti(Boolean  multi){
        this.multi = multi ;
        this.modify("multi",multi);
    }

    /**
     * 设置 [PAYMENT_DIFFERENCE_HANDLING]
     */
    public void setPaymentDifferenceHandling(String  paymentDifferenceHandling){
        this.paymentDifferenceHandling = paymentDifferenceHandling ;
        this.modify("payment_difference_handling",paymentDifferenceHandling);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(BigDecimal  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [WRITEOFF_ACCOUNT_ID]
     */
    public void setWriteoffAccountId(Long  writeoffAccountId){
        this.writeoffAccountId = writeoffAccountId ;
        this.modify("writeoff_account_id",writeoffAccountId);
    }

    /**
     * 设置 [PARTNER_BANK_ACCOUNT_ID]
     */
    public void setPartnerBankAccountId(Long  partnerBankAccountId){
        this.partnerBankAccountId = partnerBankAccountId ;
        this.modify("partner_bank_account_id",partnerBankAccountId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    public void setJournalId(Long  journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [PAYMENT_METHOD_ID]
     */
    public void setPaymentMethodId(Long  paymentMethodId){
        this.paymentMethodId = paymentMethodId ;
        this.modify("payment_method_id",paymentMethodId);
    }


}


