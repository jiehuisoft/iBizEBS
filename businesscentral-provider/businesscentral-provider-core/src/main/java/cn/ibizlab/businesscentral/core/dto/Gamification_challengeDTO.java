package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Gamification_challengeDTO]
 */
@Data
public class Gamification_challengeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[挑战名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [START_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd")
    @JsonProperty("start_date")
    private Timestamp startDate;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [LAST_REPORT_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_report_date" , format="yyyy-MM-dd")
    @JsonProperty("last_report_date")
    private Timestamp lastReportDate;

    /**
     * 属性 [END_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "end_date" , format="yyyy-MM-dd")
    @JsonProperty("end_date")
    private Timestamp endDate;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [USER_DOMAIN]
     *
     */
    @JSONField(name = "user_domain")
    @JsonProperty("user_domain")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String userDomain;

    /**
     * 属性 [CATEGORY]
     *
     */
    @JSONField(name = "category")
    @JsonProperty("category")
    @NotBlank(message = "[出现在]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String category;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String description;

    /**
     * 属性 [USER_IDS]
     *
     */
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String userIds;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [LINE_IDS]
     *
     */
    @JSONField(name = "line_ids")
    @JsonProperty("line_ids")
    @NotBlank(message = "[明细行]不允许为空!")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String lineIds;

    /**
     * 属性 [REPORT_MESSAGE_FREQUENCY]
     *
     */
    @JSONField(name = "report_message_frequency")
    @JsonProperty("report_message_frequency")
    @NotBlank(message = "[报告的频率]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String reportMessageFrequency;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [REWARD_REALTIME]
     *
     */
    @JSONField(name = "reward_realtime")
    @JsonProperty("reward_realtime")
    private Boolean rewardRealtime;

    /**
     * 属性 [REWARD_FAILURE]
     *
     */
    @JSONField(name = "reward_failure")
    @JsonProperty("reward_failure")
    private Boolean rewardFailure;

    /**
     * 属性 [PERIOD]
     *
     */
    @JSONField(name = "period")
    @JsonProperty("period")
    @NotBlank(message = "[周期]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String period;

    /**
     * 属性 [VISIBILITY_MODE]
     *
     */
    @JSONField(name = "visibility_mode")
    @JsonProperty("visibility_mode")
    @NotBlank(message = "[显示模式]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String visibilityMode;

    /**
     * 属性 [REMIND_UPDATE_DELAY]
     *
     */
    @JSONField(name = "remind_update_delay")
    @JsonProperty("remind_update_delay")
    private Integer remindUpdateDelay;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [INVITED_USER_IDS]
     *
     */
    @JSONField(name = "invited_user_ids")
    @JsonProperty("invited_user_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String invitedUserIds;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @NotBlank(message = "[状态]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [NEXT_REPORT_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "next_report_date" , format="yyyy-MM-dd")
    @JsonProperty("next_report_date")
    private Timestamp nextReportDate;

    /**
     * 属性 [MANAGER_ID_TEXT]
     *
     */
    @JSONField(name = "manager_id_text")
    @JsonProperty("manager_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String managerIdText;

    /**
     * 属性 [REPORT_MESSAGE_GROUP_ID_TEXT]
     *
     */
    @JSONField(name = "report_message_group_id_text")
    @JsonProperty("report_message_group_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String reportMessageGroupIdText;

    /**
     * 属性 [REPORT_TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "report_template_id_text")
    @JsonProperty("report_template_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String reportTemplateIdText;

    /**
     * 属性 [REWARD_ID_TEXT]
     *
     */
    @JSONField(name = "reward_id_text")
    @JsonProperty("reward_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rewardIdText;

    /**
     * 属性 [REWARD_FIRST_ID_TEXT]
     *
     */
    @JSONField(name = "reward_first_id_text")
    @JsonProperty("reward_first_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rewardFirstIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [REWARD_SECOND_ID_TEXT]
     *
     */
    @JSONField(name = "reward_second_id_text")
    @JsonProperty("reward_second_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rewardSecondIdText;

    /**
     * 属性 [REWARD_THIRD_ID_TEXT]
     *
     */
    @JSONField(name = "reward_third_id_text")
    @JsonProperty("reward_third_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String rewardThirdIdText;

    /**
     * 属性 [REWARD_FIRST_ID]
     *
     */
    @JSONField(name = "reward_first_id")
    @JsonProperty("reward_first_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long rewardFirstId;

    /**
     * 属性 [REPORT_TEMPLATE_ID]
     *
     */
    @JSONField(name = "report_template_id")
    @JsonProperty("report_template_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[报告模板]不允许为空!")
    private Long reportTemplateId;

    /**
     * 属性 [REWARD_SECOND_ID]
     *
     */
    @JSONField(name = "reward_second_id")
    @JsonProperty("reward_second_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long rewardSecondId;

    /**
     * 属性 [REWARD_THIRD_ID]
     *
     */
    @JSONField(name = "reward_third_id")
    @JsonProperty("reward_third_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long rewardThirdId;

    /**
     * 属性 [REPORT_MESSAGE_GROUP_ID]
     *
     */
    @JSONField(name = "report_message_group_id")
    @JsonProperty("report_message_group_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long reportMessageGroupId;

    /**
     * 属性 [REWARD_ID]
     *
     */
    @JSONField(name = "reward_id")
    @JsonProperty("reward_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long rewardId;

    /**
     * 属性 [MANAGER_ID]
     *
     */
    @JSONField(name = "manager_id")
    @JsonProperty("manager_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long managerId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [START_DATE]
     */
    public void setStartDate(Timestamp  startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [LAST_REPORT_DATE]
     */
    public void setLastReportDate(Timestamp  lastReportDate){
        this.lastReportDate = lastReportDate ;
        this.modify("last_report_date",lastReportDate);
    }

    /**
     * 设置 [END_DATE]
     */
    public void setEndDate(Timestamp  endDate){
        this.endDate = endDate ;
        this.modify("end_date",endDate);
    }

    /**
     * 设置 [USER_DOMAIN]
     */
    public void setUserDomain(String  userDomain){
        this.userDomain = userDomain ;
        this.modify("user_domain",userDomain);
    }

    /**
     * 设置 [CATEGORY]
     */
    public void setCategory(String  category){
        this.category = category ;
        this.modify("category",category);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [REPORT_MESSAGE_FREQUENCY]
     */
    public void setReportMessageFrequency(String  reportMessageFrequency){
        this.reportMessageFrequency = reportMessageFrequency ;
        this.modify("report_message_frequency",reportMessageFrequency);
    }

    /**
     * 设置 [REWARD_REALTIME]
     */
    public void setRewardRealtime(Boolean  rewardRealtime){
        this.rewardRealtime = rewardRealtime ;
        this.modify("reward_realtime",rewardRealtime);
    }

    /**
     * 设置 [REWARD_FAILURE]
     */
    public void setRewardFailure(Boolean  rewardFailure){
        this.rewardFailure = rewardFailure ;
        this.modify("reward_failure",rewardFailure);
    }

    /**
     * 设置 [PERIOD]
     */
    public void setPeriod(String  period){
        this.period = period ;
        this.modify("period",period);
    }

    /**
     * 设置 [VISIBILITY_MODE]
     */
    public void setVisibilityMode(String  visibilityMode){
        this.visibilityMode = visibilityMode ;
        this.modify("visibility_mode",visibilityMode);
    }

    /**
     * 设置 [REMIND_UPDATE_DELAY]
     */
    public void setRemindUpdateDelay(Integer  remindUpdateDelay){
        this.remindUpdateDelay = remindUpdateDelay ;
        this.modify("remind_update_delay",remindUpdateDelay);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [NEXT_REPORT_DATE]
     */
    public void setNextReportDate(Timestamp  nextReportDate){
        this.nextReportDate = nextReportDate ;
        this.modify("next_report_date",nextReportDate);
    }

    /**
     * 设置 [REWARD_FIRST_ID]
     */
    public void setRewardFirstId(Long  rewardFirstId){
        this.rewardFirstId = rewardFirstId ;
        this.modify("reward_first_id",rewardFirstId);
    }

    /**
     * 设置 [REPORT_TEMPLATE_ID]
     */
    public void setReportTemplateId(Long  reportTemplateId){
        this.reportTemplateId = reportTemplateId ;
        this.modify("report_template_id",reportTemplateId);
    }

    /**
     * 设置 [REWARD_SECOND_ID]
     */
    public void setRewardSecondId(Long  rewardSecondId){
        this.rewardSecondId = rewardSecondId ;
        this.modify("reward_second_id",rewardSecondId);
    }

    /**
     * 设置 [REWARD_THIRD_ID]
     */
    public void setRewardThirdId(Long  rewardThirdId){
        this.rewardThirdId = rewardThirdId ;
        this.modify("reward_third_id",rewardThirdId);
    }

    /**
     * 设置 [REPORT_MESSAGE_GROUP_ID]
     */
    public void setReportMessageGroupId(Long  reportMessageGroupId){
        this.reportMessageGroupId = reportMessageGroupId ;
        this.modify("report_message_group_id",reportMessageGroupId);
    }

    /**
     * 设置 [REWARD_ID]
     */
    public void setRewardId(Long  rewardId){
        this.rewardId = rewardId ;
        this.modify("reward_id",rewardId);
    }

    /**
     * 设置 [MANAGER_ID]
     */
    public void setManagerId(Long  managerId){
        this.managerId = managerId ;
        this.modify("manager_id",managerId);
    }


}


