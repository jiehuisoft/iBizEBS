package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead2opportunity_partner_mass;
import cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lead2opportunity_partner_massService;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_lead2opportunity_partner_massSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"转化线索为商机（批量）" })
@RestController("Core-crm_lead2opportunity_partner_mass")
@RequestMapping("")
public class Crm_lead2opportunity_partner_massResource {

    @Autowired
    public ICrm_lead2opportunity_partner_massService crm_lead2opportunity_partner_massService;

    @Autowired
    @Lazy
    public Crm_lead2opportunity_partner_massMapping crm_lead2opportunity_partner_massMapping;

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partner_massMapping.toDomain(#crm_lead2opportunity_partner_massdto),'iBizBusinessCentral-Crm_lead2opportunity_partner_mass-Create')")
    @ApiOperation(value = "新建转化线索为商机（批量）", tags = {"转化线索为商机（批量）" },  notes = "新建转化线索为商机（批量）")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partner_masses")
    public ResponseEntity<Crm_lead2opportunity_partner_massDTO> create(@Validated @RequestBody Crm_lead2opportunity_partner_massDTO crm_lead2opportunity_partner_massdto) {
        Crm_lead2opportunity_partner_mass domain = crm_lead2opportunity_partner_massMapping.toDomain(crm_lead2opportunity_partner_massdto);
		crm_lead2opportunity_partner_massService.create(domain);
        Crm_lead2opportunity_partner_massDTO dto = crm_lead2opportunity_partner_massMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partner_massMapping.toDomain(#crm_lead2opportunity_partner_massdtos),'iBizBusinessCentral-Crm_lead2opportunity_partner_mass-Create')")
    @ApiOperation(value = "批量新建转化线索为商机（批量）", tags = {"转化线索为商机（批量）" },  notes = "批量新建转化线索为商机（批量）")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partner_masses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_lead2opportunity_partner_massDTO> crm_lead2opportunity_partner_massdtos) {
        crm_lead2opportunity_partner_massService.createBatch(crm_lead2opportunity_partner_massMapping.toDomain(crm_lead2opportunity_partner_massdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "crm_lead2opportunity_partner_mass" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partner_massService.get(#crm_lead2opportunity_partner_mass_id),'iBizBusinessCentral-Crm_lead2opportunity_partner_mass-Update')")
    @ApiOperation(value = "更新转化线索为商机（批量）", tags = {"转化线索为商机（批量）" },  notes = "更新转化线索为商机（批量）")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")
    public ResponseEntity<Crm_lead2opportunity_partner_massDTO> update(@PathVariable("crm_lead2opportunity_partner_mass_id") Long crm_lead2opportunity_partner_mass_id, @RequestBody Crm_lead2opportunity_partner_massDTO crm_lead2opportunity_partner_massdto) {
		Crm_lead2opportunity_partner_mass domain  = crm_lead2opportunity_partner_massMapping.toDomain(crm_lead2opportunity_partner_massdto);
        domain .setId(crm_lead2opportunity_partner_mass_id);
		crm_lead2opportunity_partner_massService.update(domain );
		Crm_lead2opportunity_partner_massDTO dto = crm_lead2opportunity_partner_massMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partner_massService.getCrmLead2opportunityPartnerMassByEntities(this.crm_lead2opportunity_partner_massMapping.toDomain(#crm_lead2opportunity_partner_massdtos)),'iBizBusinessCentral-Crm_lead2opportunity_partner_mass-Update')")
    @ApiOperation(value = "批量更新转化线索为商机（批量）", tags = {"转化线索为商机（批量）" },  notes = "批量更新转化线索为商机（批量）")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partner_masses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lead2opportunity_partner_massDTO> crm_lead2opportunity_partner_massdtos) {
        crm_lead2opportunity_partner_massService.updateBatch(crm_lead2opportunity_partner_massMapping.toDomain(crm_lead2opportunity_partner_massdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partner_massService.get(#crm_lead2opportunity_partner_mass_id),'iBizBusinessCentral-Crm_lead2opportunity_partner_mass-Remove')")
    @ApiOperation(value = "删除转化线索为商机（批量）", tags = {"转化线索为商机（批量）" },  notes = "删除转化线索为商机（批量）")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead2opportunity_partner_mass_id") Long crm_lead2opportunity_partner_mass_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_massService.remove(crm_lead2opportunity_partner_mass_id));
    }

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partner_massService.getCrmLead2opportunityPartnerMassByIds(#ids),'iBizBusinessCentral-Crm_lead2opportunity_partner_mass-Remove')")
    @ApiOperation(value = "批量删除转化线索为商机（批量）", tags = {"转化线索为商机（批量）" },  notes = "批量删除转化线索为商机（批量）")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partner_masses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        crm_lead2opportunity_partner_massService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.crm_lead2opportunity_partner_massMapping.toDomain(returnObject.body),'iBizBusinessCentral-Crm_lead2opportunity_partner_mass-Get')")
    @ApiOperation(value = "获取转化线索为商机（批量）", tags = {"转化线索为商机（批量）" },  notes = "获取转化线索为商机（批量）")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partner_masses/{crm_lead2opportunity_partner_mass_id}")
    public ResponseEntity<Crm_lead2opportunity_partner_massDTO> get(@PathVariable("crm_lead2opportunity_partner_mass_id") Long crm_lead2opportunity_partner_mass_id) {
        Crm_lead2opportunity_partner_mass domain = crm_lead2opportunity_partner_massService.get(crm_lead2opportunity_partner_mass_id);
        Crm_lead2opportunity_partner_massDTO dto = crm_lead2opportunity_partner_massMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取转化线索为商机（批量）草稿", tags = {"转化线索为商机（批量）" },  notes = "获取转化线索为商机（批量）草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partner_masses/getdraft")
    public ResponseEntity<Crm_lead2opportunity_partner_massDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_massMapping.toDto(crm_lead2opportunity_partner_massService.getDraft(new Crm_lead2opportunity_partner_mass())));
    }

    @ApiOperation(value = "检查转化线索为商机（批量）", tags = {"转化线索为商机（批量）" },  notes = "检查转化线索为商机（批量）")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partner_masses/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Crm_lead2opportunity_partner_massDTO crm_lead2opportunity_partner_massdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_massService.checkKey(crm_lead2opportunity_partner_massMapping.toDomain(crm_lead2opportunity_partner_massdto)));
    }

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partner_massMapping.toDomain(#crm_lead2opportunity_partner_massdto),'iBizBusinessCentral-Crm_lead2opportunity_partner_mass-Save')")
    @ApiOperation(value = "保存转化线索为商机（批量）", tags = {"转化线索为商机（批量）" },  notes = "保存转化线索为商机（批量）")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partner_masses/save")
    public ResponseEntity<Boolean> save(@RequestBody Crm_lead2opportunity_partner_massDTO crm_lead2opportunity_partner_massdto) {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partner_massService.save(crm_lead2opportunity_partner_massMapping.toDomain(crm_lead2opportunity_partner_massdto)));
    }

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partner_massMapping.toDomain(#crm_lead2opportunity_partner_massdtos),'iBizBusinessCentral-Crm_lead2opportunity_partner_mass-Save')")
    @ApiOperation(value = "批量保存转化线索为商机（批量）", tags = {"转化线索为商机（批量）" },  notes = "批量保存转化线索为商机（批量）")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partner_masses/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Crm_lead2opportunity_partner_massDTO> crm_lead2opportunity_partner_massdtos) {
        crm_lead2opportunity_partner_massService.saveBatch(crm_lead2opportunity_partner_massMapping.toDomain(crm_lead2opportunity_partner_massdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_lead2opportunity_partner_mass-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_lead2opportunity_partner_mass-Get')")
	@ApiOperation(value = "获取数据集", tags = {"转化线索为商机（批量）" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/crm_lead2opportunity_partner_masses/fetchdefault")
	public ResponseEntity<List<Crm_lead2opportunity_partner_massDTO>> fetchDefault(Crm_lead2opportunity_partner_massSearchContext context) {
        Page<Crm_lead2opportunity_partner_mass> domains = crm_lead2opportunity_partner_massService.searchDefault(context) ;
        List<Crm_lead2opportunity_partner_massDTO> list = crm_lead2opportunity_partner_massMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_lead2opportunity_partner_mass-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_lead2opportunity_partner_mass-Get')")
	@ApiOperation(value = "查询数据集", tags = {"转化线索为商机（批量）" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/crm_lead2opportunity_partner_masses/searchdefault")
	public ResponseEntity<Page<Crm_lead2opportunity_partner_massDTO>> searchDefault(@RequestBody Crm_lead2opportunity_partner_massSearchContext context) {
        Page<Crm_lead2opportunity_partner_mass> domains = crm_lead2opportunity_partner_massService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_lead2opportunity_partner_massMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

