package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_currencySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"币种" })
@RestController("Core-res_currency")
@RequestMapping("")
public class Res_currencyResource {

    @Autowired
    public IRes_currencyService res_currencyService;

    @Autowired
    @Lazy
    public Res_currencyMapping res_currencyMapping;

    @PreAuthorize("hasPermission(this.res_currencyMapping.toDomain(#res_currencydto),'iBizBusinessCentral-Res_currency-Create')")
    @ApiOperation(value = "新建币种", tags = {"币种" },  notes = "新建币种")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currencies")
    public ResponseEntity<Res_currencyDTO> create(@Validated @RequestBody Res_currencyDTO res_currencydto) {
        Res_currency domain = res_currencyMapping.toDomain(res_currencydto);
		res_currencyService.create(domain);
        Res_currencyDTO dto = res_currencyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_currencyMapping.toDomain(#res_currencydtos),'iBizBusinessCentral-Res_currency-Create')")
    @ApiOperation(value = "批量新建币种", tags = {"币种" },  notes = "批量新建币种")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currencies/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_currencyDTO> res_currencydtos) {
        res_currencyService.createBatch(res_currencyMapping.toDomain(res_currencydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_currency" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_currencyService.get(#res_currency_id),'iBizBusinessCentral-Res_currency-Update')")
    @ApiOperation(value = "更新币种", tags = {"币种" },  notes = "更新币种")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_currencies/{res_currency_id}")
    public ResponseEntity<Res_currencyDTO> update(@PathVariable("res_currency_id") Long res_currency_id, @RequestBody Res_currencyDTO res_currencydto) {
		Res_currency domain  = res_currencyMapping.toDomain(res_currencydto);
        domain .setId(res_currency_id);
		res_currencyService.update(domain );
		Res_currencyDTO dto = res_currencyMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_currencyService.getResCurrencyByEntities(this.res_currencyMapping.toDomain(#res_currencydtos)),'iBizBusinessCentral-Res_currency-Update')")
    @ApiOperation(value = "批量更新币种", tags = {"币种" },  notes = "批量更新币种")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_currencies/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_currencyDTO> res_currencydtos) {
        res_currencyService.updateBatch(res_currencyMapping.toDomain(res_currencydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_currencyService.get(#res_currency_id),'iBizBusinessCentral-Res_currency-Remove')")
    @ApiOperation(value = "删除币种", tags = {"币种" },  notes = "删除币种")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_currencies/{res_currency_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_currency_id") Long res_currency_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_currencyService.remove(res_currency_id));
    }

    @PreAuthorize("hasPermission(this.res_currencyService.getResCurrencyByIds(#ids),'iBizBusinessCentral-Res_currency-Remove')")
    @ApiOperation(value = "批量删除币种", tags = {"币种" },  notes = "批量删除币种")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_currencies/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_currencyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_currencyMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_currency-Get')")
    @ApiOperation(value = "获取币种", tags = {"币种" },  notes = "获取币种")
	@RequestMapping(method = RequestMethod.GET, value = "/res_currencies/{res_currency_id}")
    public ResponseEntity<Res_currencyDTO> get(@PathVariable("res_currency_id") Long res_currency_id) {
        Res_currency domain = res_currencyService.get(res_currency_id);
        Res_currencyDTO dto = res_currencyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取币种草稿", tags = {"币种" },  notes = "获取币种草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_currencies/getdraft")
    public ResponseEntity<Res_currencyDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_currencyMapping.toDto(res_currencyService.getDraft(new Res_currency())));
    }

    @ApiOperation(value = "检查币种", tags = {"币种" },  notes = "检查币种")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currencies/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_currencyDTO res_currencydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_currencyService.checkKey(res_currencyMapping.toDomain(res_currencydto)));
    }

    @PreAuthorize("hasPermission(this.res_currencyMapping.toDomain(#res_currencydto),'iBizBusinessCentral-Res_currency-Save')")
    @ApiOperation(value = "保存币种", tags = {"币种" },  notes = "保存币种")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currencies/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_currencyDTO res_currencydto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_currencyService.save(res_currencyMapping.toDomain(res_currencydto)));
    }

    @PreAuthorize("hasPermission(this.res_currencyMapping.toDomain(#res_currencydtos),'iBizBusinessCentral-Res_currency-Save')")
    @ApiOperation(value = "批量保存币种", tags = {"币种" },  notes = "批量保存币种")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currencies/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_currencyDTO> res_currencydtos) {
        res_currencyService.saveBatch(res_currencyMapping.toDomain(res_currencydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_currency-searchDEFAULT-all') and hasPermission(#context,'iBizBusinessCentral-Res_currency-Get')")
	@ApiOperation(value = "获取数据查询", tags = {"币种" } ,notes = "获取数据查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_currencies/fetchdefault")
	public ResponseEntity<List<Res_currencyDTO>> fetchDEFAULT(Res_currencySearchContext context) {
        Page<Res_currency> domains = res_currencyService.searchDEFAULT(context) ;
        List<Res_currencyDTO> list = res_currencyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_currency-searchDEFAULT-all') and hasPermission(#context,'iBizBusinessCentral-Res_currency-Get')")
	@ApiOperation(value = "查询数据查询", tags = {"币种" } ,notes = "查询数据查询")
    @RequestMapping(method= RequestMethod.POST , value="/res_currencies/searchdefault")
	public ResponseEntity<Page<Res_currencyDTO>> searchDEFAULT(@RequestBody Res_currencySearchContext context) {
        Page<Res_currency> domains = res_currencyService.searchDEFAULT(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_currencyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

