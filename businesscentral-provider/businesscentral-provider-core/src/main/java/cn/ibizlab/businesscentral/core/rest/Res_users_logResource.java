package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users_log;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_users_logService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_users_logSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"用户日志" })
@RestController("Core-res_users_log")
@RequestMapping("")
public class Res_users_logResource {

    @Autowired
    public IRes_users_logService res_users_logService;

    @Autowired
    @Lazy
    public Res_users_logMapping res_users_logMapping;

    @PreAuthorize("hasPermission(this.res_users_logMapping.toDomain(#res_users_logdto),'iBizBusinessCentral-Res_users_log-Create')")
    @ApiOperation(value = "新建用户日志", tags = {"用户日志" },  notes = "新建用户日志")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users_logs")
    public ResponseEntity<Res_users_logDTO> create(@Validated @RequestBody Res_users_logDTO res_users_logdto) {
        Res_users_log domain = res_users_logMapping.toDomain(res_users_logdto);
		res_users_logService.create(domain);
        Res_users_logDTO dto = res_users_logMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_users_logMapping.toDomain(#res_users_logdtos),'iBizBusinessCentral-Res_users_log-Create')")
    @ApiOperation(value = "批量新建用户日志", tags = {"用户日志" },  notes = "批量新建用户日志")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users_logs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_users_logDTO> res_users_logdtos) {
        res_users_logService.createBatch(res_users_logMapping.toDomain(res_users_logdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_users_log" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_users_logService.get(#res_users_log_id),'iBizBusinessCentral-Res_users_log-Update')")
    @ApiOperation(value = "更新用户日志", tags = {"用户日志" },  notes = "更新用户日志")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_users_logs/{res_users_log_id}")
    public ResponseEntity<Res_users_logDTO> update(@PathVariable("res_users_log_id") Long res_users_log_id, @RequestBody Res_users_logDTO res_users_logdto) {
		Res_users_log domain  = res_users_logMapping.toDomain(res_users_logdto);
        domain .setId(res_users_log_id);
		res_users_logService.update(domain );
		Res_users_logDTO dto = res_users_logMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_users_logService.getResUsersLogByEntities(this.res_users_logMapping.toDomain(#res_users_logdtos)),'iBizBusinessCentral-Res_users_log-Update')")
    @ApiOperation(value = "批量更新用户日志", tags = {"用户日志" },  notes = "批量更新用户日志")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_users_logs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_users_logDTO> res_users_logdtos) {
        res_users_logService.updateBatch(res_users_logMapping.toDomain(res_users_logdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_users_logService.get(#res_users_log_id),'iBizBusinessCentral-Res_users_log-Remove')")
    @ApiOperation(value = "删除用户日志", tags = {"用户日志" },  notes = "删除用户日志")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_users_logs/{res_users_log_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_users_log_id") Long res_users_log_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_users_logService.remove(res_users_log_id));
    }

    @PreAuthorize("hasPermission(this.res_users_logService.getResUsersLogByIds(#ids),'iBizBusinessCentral-Res_users_log-Remove')")
    @ApiOperation(value = "批量删除用户日志", tags = {"用户日志" },  notes = "批量删除用户日志")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_users_logs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_users_logService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_users_logMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_users_log-Get')")
    @ApiOperation(value = "获取用户日志", tags = {"用户日志" },  notes = "获取用户日志")
	@RequestMapping(method = RequestMethod.GET, value = "/res_users_logs/{res_users_log_id}")
    public ResponseEntity<Res_users_logDTO> get(@PathVariable("res_users_log_id") Long res_users_log_id) {
        Res_users_log domain = res_users_logService.get(res_users_log_id);
        Res_users_logDTO dto = res_users_logMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取用户日志草稿", tags = {"用户日志" },  notes = "获取用户日志草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_users_logs/getdraft")
    public ResponseEntity<Res_users_logDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_users_logMapping.toDto(res_users_logService.getDraft(new Res_users_log())));
    }

    @ApiOperation(value = "检查用户日志", tags = {"用户日志" },  notes = "检查用户日志")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users_logs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_users_logDTO res_users_logdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_users_logService.checkKey(res_users_logMapping.toDomain(res_users_logdto)));
    }

    @PreAuthorize("hasPermission(this.res_users_logMapping.toDomain(#res_users_logdto),'iBizBusinessCentral-Res_users_log-Save')")
    @ApiOperation(value = "保存用户日志", tags = {"用户日志" },  notes = "保存用户日志")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users_logs/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_users_logDTO res_users_logdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_users_logService.save(res_users_logMapping.toDomain(res_users_logdto)));
    }

    @PreAuthorize("hasPermission(this.res_users_logMapping.toDomain(#res_users_logdtos),'iBizBusinessCentral-Res_users_log-Save')")
    @ApiOperation(value = "批量保存用户日志", tags = {"用户日志" },  notes = "批量保存用户日志")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users_logs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_users_logDTO> res_users_logdtos) {
        res_users_logService.saveBatch(res_users_logMapping.toDomain(res_users_logdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_users_log-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_users_log-Get')")
	@ApiOperation(value = "获取数据集", tags = {"用户日志" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_users_logs/fetchdefault")
	public ResponseEntity<List<Res_users_logDTO>> fetchDefault(Res_users_logSearchContext context) {
        Page<Res_users_log> domains = res_users_logService.searchDefault(context) ;
        List<Res_users_logDTO> list = res_users_logMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_users_log-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_users_log-Get')")
	@ApiOperation(value = "查询数据集", tags = {"用户日志" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_users_logs/searchdefault")
	public ResponseEntity<Page<Res_users_logDTO>> searchDefault(@RequestBody Res_users_logSearchContext context) {
        Page<Res_users_log> domains = res_users_logService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_users_logMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

