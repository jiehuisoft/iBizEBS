package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_bom;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bomService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_bomSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"物料清单" })
@RestController("Core-mrp_bom")
@RequestMapping("")
public class Mrp_bomResource {

    @Autowired
    public IMrp_bomService mrp_bomService;

    @Autowired
    @Lazy
    public Mrp_bomMapping mrp_bomMapping;

    @PreAuthorize("hasPermission(this.mrp_bomMapping.toDomain(#mrp_bomdto),'iBizBusinessCentral-Mrp_bom-Create')")
    @ApiOperation(value = "新建物料清单", tags = {"物料清单" },  notes = "新建物料清单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_boms")
    public ResponseEntity<Mrp_bomDTO> create(@Validated @RequestBody Mrp_bomDTO mrp_bomdto) {
        Mrp_bom domain = mrp_bomMapping.toDomain(mrp_bomdto);
		mrp_bomService.create(domain);
        Mrp_bomDTO dto = mrp_bomMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_bomMapping.toDomain(#mrp_bomdtos),'iBizBusinessCentral-Mrp_bom-Create')")
    @ApiOperation(value = "批量新建物料清单", tags = {"物料清单" },  notes = "批量新建物料清单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_boms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_bomDTO> mrp_bomdtos) {
        mrp_bomService.createBatch(mrp_bomMapping.toDomain(mrp_bomdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_bom" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_bomService.get(#mrp_bom_id),'iBizBusinessCentral-Mrp_bom-Update')")
    @ApiOperation(value = "更新物料清单", tags = {"物料清单" },  notes = "更新物料清单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_boms/{mrp_bom_id}")
    public ResponseEntity<Mrp_bomDTO> update(@PathVariable("mrp_bom_id") Long mrp_bom_id, @RequestBody Mrp_bomDTO mrp_bomdto) {
		Mrp_bom domain  = mrp_bomMapping.toDomain(mrp_bomdto);
        domain .setId(mrp_bom_id);
		mrp_bomService.update(domain );
		Mrp_bomDTO dto = mrp_bomMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_bomService.getMrpBomByEntities(this.mrp_bomMapping.toDomain(#mrp_bomdtos)),'iBizBusinessCentral-Mrp_bom-Update')")
    @ApiOperation(value = "批量更新物料清单", tags = {"物料清单" },  notes = "批量更新物料清单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_boms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_bomDTO> mrp_bomdtos) {
        mrp_bomService.updateBatch(mrp_bomMapping.toDomain(mrp_bomdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_bomService.get(#mrp_bom_id),'iBizBusinessCentral-Mrp_bom-Remove')")
    @ApiOperation(value = "删除物料清单", tags = {"物料清单" },  notes = "删除物料清单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_boms/{mrp_bom_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_bom_id") Long mrp_bom_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_bomService.remove(mrp_bom_id));
    }

    @PreAuthorize("hasPermission(this.mrp_bomService.getMrpBomByIds(#ids),'iBizBusinessCentral-Mrp_bom-Remove')")
    @ApiOperation(value = "批量删除物料清单", tags = {"物料清单" },  notes = "批量删除物料清单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_boms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_bomService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_bomMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_bom-Get')")
    @ApiOperation(value = "获取物料清单", tags = {"物料清单" },  notes = "获取物料清单")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_boms/{mrp_bom_id}")
    public ResponseEntity<Mrp_bomDTO> get(@PathVariable("mrp_bom_id") Long mrp_bom_id) {
        Mrp_bom domain = mrp_bomService.get(mrp_bom_id);
        Mrp_bomDTO dto = mrp_bomMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取物料清单草稿", tags = {"物料清单" },  notes = "获取物料清单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_boms/getdraft")
    public ResponseEntity<Mrp_bomDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_bomMapping.toDto(mrp_bomService.getDraft(new Mrp_bom())));
    }

    @ApiOperation(value = "检查物料清单", tags = {"物料清单" },  notes = "检查物料清单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_boms/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_bomDTO mrp_bomdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_bomService.checkKey(mrp_bomMapping.toDomain(mrp_bomdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_bomMapping.toDomain(#mrp_bomdto),'iBizBusinessCentral-Mrp_bom-Save')")
    @ApiOperation(value = "保存物料清单", tags = {"物料清单" },  notes = "保存物料清单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_boms/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_bomDTO mrp_bomdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_bomService.save(mrp_bomMapping.toDomain(mrp_bomdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_bomMapping.toDomain(#mrp_bomdtos),'iBizBusinessCentral-Mrp_bom-Save')")
    @ApiOperation(value = "批量保存物料清单", tags = {"物料清单" },  notes = "批量保存物料清单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_boms/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_bomDTO> mrp_bomdtos) {
        mrp_bomService.saveBatch(mrp_bomMapping.toDomain(mrp_bomdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_bom-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_bom-Get')")
	@ApiOperation(value = "获取数据集", tags = {"物料清单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_boms/fetchdefault")
	public ResponseEntity<List<Mrp_bomDTO>> fetchDefault(Mrp_bomSearchContext context) {
        Page<Mrp_bom> domains = mrp_bomService.searchDefault(context) ;
        List<Mrp_bomDTO> list = mrp_bomMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_bom-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_bom-Get')")
	@ApiOperation(value = "查询数据集", tags = {"物料清单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_boms/searchdefault")
	public ResponseEntity<Page<Mrp_bomDTO>> searchDefault(@RequestBody Mrp_bomSearchContext context) {
        Page<Mrp_bom> domains = mrp_bomService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_bomMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

