package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_resend_message;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_resend_messageService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_resend_messageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"EMail重发向导" })
@RestController("Core-mail_resend_message")
@RequestMapping("")
public class Mail_resend_messageResource {

    @Autowired
    public IMail_resend_messageService mail_resend_messageService;

    @Autowired
    @Lazy
    public Mail_resend_messageMapping mail_resend_messageMapping;

    @PreAuthorize("hasPermission(this.mail_resend_messageMapping.toDomain(#mail_resend_messagedto),'iBizBusinessCentral-Mail_resend_message-Create')")
    @ApiOperation(value = "新建EMail重发向导", tags = {"EMail重发向导" },  notes = "新建EMail重发向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages")
    public ResponseEntity<Mail_resend_messageDTO> create(@Validated @RequestBody Mail_resend_messageDTO mail_resend_messagedto) {
        Mail_resend_message domain = mail_resend_messageMapping.toDomain(mail_resend_messagedto);
		mail_resend_messageService.create(domain);
        Mail_resend_messageDTO dto = mail_resend_messageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_resend_messageMapping.toDomain(#mail_resend_messagedtos),'iBizBusinessCentral-Mail_resend_message-Create')")
    @ApiOperation(value = "批量新建EMail重发向导", tags = {"EMail重发向导" },  notes = "批量新建EMail重发向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_resend_messageDTO> mail_resend_messagedtos) {
        mail_resend_messageService.createBatch(mail_resend_messageMapping.toDomain(mail_resend_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_resend_message" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_resend_messageService.get(#mail_resend_message_id),'iBizBusinessCentral-Mail_resend_message-Update')")
    @ApiOperation(value = "更新EMail重发向导", tags = {"EMail重发向导" },  notes = "更新EMail重发向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_messages/{mail_resend_message_id}")
    public ResponseEntity<Mail_resend_messageDTO> update(@PathVariable("mail_resend_message_id") Long mail_resend_message_id, @RequestBody Mail_resend_messageDTO mail_resend_messagedto) {
		Mail_resend_message domain  = mail_resend_messageMapping.toDomain(mail_resend_messagedto);
        domain .setId(mail_resend_message_id);
		mail_resend_messageService.update(domain );
		Mail_resend_messageDTO dto = mail_resend_messageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_resend_messageService.getMailResendMessageByEntities(this.mail_resend_messageMapping.toDomain(#mail_resend_messagedtos)),'iBizBusinessCentral-Mail_resend_message-Update')")
    @ApiOperation(value = "批量更新EMail重发向导", tags = {"EMail重发向导" },  notes = "批量更新EMail重发向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_messages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_resend_messageDTO> mail_resend_messagedtos) {
        mail_resend_messageService.updateBatch(mail_resend_messageMapping.toDomain(mail_resend_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_resend_messageService.get(#mail_resend_message_id),'iBizBusinessCentral-Mail_resend_message-Remove')")
    @ApiOperation(value = "删除EMail重发向导", tags = {"EMail重发向导" },  notes = "删除EMail重发向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_messages/{mail_resend_message_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_resend_message_id") Long mail_resend_message_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_resend_messageService.remove(mail_resend_message_id));
    }

    @PreAuthorize("hasPermission(this.mail_resend_messageService.getMailResendMessageByIds(#ids),'iBizBusinessCentral-Mail_resend_message-Remove')")
    @ApiOperation(value = "批量删除EMail重发向导", tags = {"EMail重发向导" },  notes = "批量删除EMail重发向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_messages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_resend_messageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_resend_messageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_resend_message-Get')")
    @ApiOperation(value = "获取EMail重发向导", tags = {"EMail重发向导" },  notes = "获取EMail重发向导")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_resend_messages/{mail_resend_message_id}")
    public ResponseEntity<Mail_resend_messageDTO> get(@PathVariable("mail_resend_message_id") Long mail_resend_message_id) {
        Mail_resend_message domain = mail_resend_messageService.get(mail_resend_message_id);
        Mail_resend_messageDTO dto = mail_resend_messageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取EMail重发向导草稿", tags = {"EMail重发向导" },  notes = "获取EMail重发向导草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_resend_messages/getdraft")
    public ResponseEntity<Mail_resend_messageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_resend_messageMapping.toDto(mail_resend_messageService.getDraft(new Mail_resend_message())));
    }

    @ApiOperation(value = "检查EMail重发向导", tags = {"EMail重发向导" },  notes = "检查EMail重发向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_resend_messageDTO mail_resend_messagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_resend_messageService.checkKey(mail_resend_messageMapping.toDomain(mail_resend_messagedto)));
    }

    @PreAuthorize("hasPermission(this.mail_resend_messageMapping.toDomain(#mail_resend_messagedto),'iBizBusinessCentral-Mail_resend_message-Save')")
    @ApiOperation(value = "保存EMail重发向导", tags = {"EMail重发向导" },  notes = "保存EMail重发向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_resend_messageDTO mail_resend_messagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_resend_messageService.save(mail_resend_messageMapping.toDomain(mail_resend_messagedto)));
    }

    @PreAuthorize("hasPermission(this.mail_resend_messageMapping.toDomain(#mail_resend_messagedtos),'iBizBusinessCentral-Mail_resend_message-Save')")
    @ApiOperation(value = "批量保存EMail重发向导", tags = {"EMail重发向导" },  notes = "批量保存EMail重发向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_resend_messageDTO> mail_resend_messagedtos) {
        mail_resend_messageService.saveBatch(mail_resend_messageMapping.toDomain(mail_resend_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_resend_message-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_resend_message-Get')")
	@ApiOperation(value = "获取数据集", tags = {"EMail重发向导" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_resend_messages/fetchdefault")
	public ResponseEntity<List<Mail_resend_messageDTO>> fetchDefault(Mail_resend_messageSearchContext context) {
        Page<Mail_resend_message> domains = mail_resend_messageService.searchDefault(context) ;
        List<Mail_resend_messageDTO> list = mail_resend_messageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_resend_message-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_resend_message-Get')")
	@ApiOperation(value = "查询数据集", tags = {"EMail重发向导" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_resend_messages/searchdefault")
	public ResponseEntity<Page<Mail_resend_messageDTO>> searchDefault(@RequestBody Mail_resend_messageSearchContext context) {
        Page<Mail_resend_message> domains = mail_resend_messageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_resend_messageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

