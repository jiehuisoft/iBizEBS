package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expenseService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expenseSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"费用" })
@RestController("Core-hr_expense")
@RequestMapping("")
public class Hr_expenseResource {

    @Autowired
    public IHr_expenseService hr_expenseService;

    @Autowired
    @Lazy
    public Hr_expenseMapping hr_expenseMapping;

    @PreAuthorize("hasPermission(this.hr_expenseMapping.toDomain(#hr_expensedto),'iBizBusinessCentral-Hr_expense-Create')")
    @ApiOperation(value = "新建费用", tags = {"费用" },  notes = "新建费用")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expenses")
    public ResponseEntity<Hr_expenseDTO> create(@Validated @RequestBody Hr_expenseDTO hr_expensedto) {
        Hr_expense domain = hr_expenseMapping.toDomain(hr_expensedto);
		hr_expenseService.create(domain);
        Hr_expenseDTO dto = hr_expenseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_expenseMapping.toDomain(#hr_expensedtos),'iBizBusinessCentral-Hr_expense-Create')")
    @ApiOperation(value = "批量新建费用", tags = {"费用" },  notes = "批量新建费用")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expenses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_expenseDTO> hr_expensedtos) {
        hr_expenseService.createBatch(hr_expenseMapping.toDomain(hr_expensedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_expense" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_expenseService.get(#hr_expense_id),'iBizBusinessCentral-Hr_expense-Update')")
    @ApiOperation(value = "更新费用", tags = {"费用" },  notes = "更新费用")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_expenses/{hr_expense_id}")
    public ResponseEntity<Hr_expenseDTO> update(@PathVariable("hr_expense_id") Long hr_expense_id, @RequestBody Hr_expenseDTO hr_expensedto) {
		Hr_expense domain  = hr_expenseMapping.toDomain(hr_expensedto);
        domain .setId(hr_expense_id);
		hr_expenseService.update(domain );
		Hr_expenseDTO dto = hr_expenseMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_expenseService.getHrExpenseByEntities(this.hr_expenseMapping.toDomain(#hr_expensedtos)),'iBizBusinessCentral-Hr_expense-Update')")
    @ApiOperation(value = "批量更新费用", tags = {"费用" },  notes = "批量更新费用")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_expenses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_expenseDTO> hr_expensedtos) {
        hr_expenseService.updateBatch(hr_expenseMapping.toDomain(hr_expensedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_expenseService.get(#hr_expense_id),'iBizBusinessCentral-Hr_expense-Remove')")
    @ApiOperation(value = "删除费用", tags = {"费用" },  notes = "删除费用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_expenses/{hr_expense_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_expense_id") Long hr_expense_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_expenseService.remove(hr_expense_id));
    }

    @PreAuthorize("hasPermission(this.hr_expenseService.getHrExpenseByIds(#ids),'iBizBusinessCentral-Hr_expense-Remove')")
    @ApiOperation(value = "批量删除费用", tags = {"费用" },  notes = "批量删除费用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_expenses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_expenseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_expenseMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_expense-Get')")
    @ApiOperation(value = "获取费用", tags = {"费用" },  notes = "获取费用")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_expenses/{hr_expense_id}")
    public ResponseEntity<Hr_expenseDTO> get(@PathVariable("hr_expense_id") Long hr_expense_id) {
        Hr_expense domain = hr_expenseService.get(hr_expense_id);
        Hr_expenseDTO dto = hr_expenseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取费用草稿", tags = {"费用" },  notes = "获取费用草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_expenses/getdraft")
    public ResponseEntity<Hr_expenseDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_expenseMapping.toDto(hr_expenseService.getDraft(new Hr_expense())));
    }

    @ApiOperation(value = "检查费用", tags = {"费用" },  notes = "检查费用")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expenses/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_expenseDTO hr_expensedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_expenseService.checkKey(hr_expenseMapping.toDomain(hr_expensedto)));
    }

    @PreAuthorize("hasPermission(this.hr_expenseMapping.toDomain(#hr_expensedto),'iBizBusinessCentral-Hr_expense-Save')")
    @ApiOperation(value = "保存费用", tags = {"费用" },  notes = "保存费用")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expenses/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_expenseDTO hr_expensedto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_expenseService.save(hr_expenseMapping.toDomain(hr_expensedto)));
    }

    @PreAuthorize("hasPermission(this.hr_expenseMapping.toDomain(#hr_expensedtos),'iBizBusinessCentral-Hr_expense-Save')")
    @ApiOperation(value = "批量保存费用", tags = {"费用" },  notes = "批量保存费用")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expenses/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_expenseDTO> hr_expensedtos) {
        hr_expenseService.saveBatch(hr_expenseMapping.toDomain(hr_expensedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_expense-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_expense-Get')")
	@ApiOperation(value = "获取数据集", tags = {"费用" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_expenses/fetchdefault")
	public ResponseEntity<List<Hr_expenseDTO>> fetchDefault(Hr_expenseSearchContext context) {
        Page<Hr_expense> domains = hr_expenseService.searchDefault(context) ;
        List<Hr_expenseDTO> list = hr_expenseMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_expense-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_expense-Get')")
	@ApiOperation(value = "查询数据集", tags = {"费用" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_expenses/searchdefault")
	public ResponseEntity<Page<Hr_expenseDTO>> searchDefault(@RequestBody Hr_expenseSearchContext context) {
        Page<Hr_expense> domains = hr_expenseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_expenseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

