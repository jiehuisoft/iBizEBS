package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Hr_expenseDTO]
 */
@Data
public class Hr_expenseDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ATTACHMENT_NUMBER]
     *
     */
    @JSONField(name = "attachment_number")
    @JsonProperty("attachment_number")
    private Integer attachmentNumber;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[说明]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activityIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String activitySummary;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityState;

    /**
     * 属性 [TOTAL_AMOUNT]
     *
     */
    @JSONField(name = "total_amount")
    @JsonProperty("total_amount")
    private BigDecimal totalAmount;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [TAX_IDS]
     *
     */
    @JSONField(name = "tax_ids")
    @JsonProperty("tax_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String taxIds;

    /**
     * 属性 [UNIT_AMOUNT]
     *
     */
    @JSONField(name = "unit_amount")
    @JsonProperty("unit_amount")
    @NotNull(message = "[单价]不允许为空!")
    private Double unitAmount;

    /**
     * 属性 [PAYMENT_MODE]
     *
     */
    @JSONField(name = "payment_mode")
    @JsonProperty("payment_mode")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String paymentMode;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [TOTAL_AMOUNT_COMPANY]
     *
     */
    @JSONField(name = "total_amount_company")
    @JsonProperty("total_amount_company")
    private BigDecimal totalAmountCompany;

    /**
     * 属性 [IS_REFUSED]
     *
     */
    @JSONField(name = "is_refused")
    @JsonProperty("is_refused")
    private Boolean isRefused;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [QUANTITY]
     *
     */
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    @NotNull(message = "[数量]不允许为空!")
    private Double quantity;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [UNTAXED_AMOUNT]
     *
     */
    @JSONField(name = "untaxed_amount")
    @JsonProperty("untaxed_amount")
    private Double untaxedAmount;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [ANALYTIC_TAG_IDS]
     *
     */
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String analyticTagIds;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String description;

    /**
     * 属性 [REFERENCE]
     *
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String reference;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [SHEET_ID_TEXT]
     *
     */
    @JSONField(name = "sheet_id_text")
    @JsonProperty("sheet_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sheetIdText;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currencyIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [COMPANY_CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "company_currency_id_text")
    @JsonProperty("company_currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyCurrencyIdText;

    /**
     * 属性 [EMPLOYEE_ID_TEXT]
     *
     */
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String employeeIdText;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productUomIdText;

    /**
     * 属性 [SALE_ORDER_ID_TEXT]
     *
     */
    @JSONField(name = "sale_order_id_text")
    @JsonProperty("sale_order_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String saleOrderIdText;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "analytic_account_id_text")
    @JsonProperty("analytic_account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String analyticAccountIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accountIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [EMPLOYEE_ID]
     *
     */
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[员工]不允许为空!")
    private Long employeeId;

    /**
     * 属性 [SALE_ORDER_ID]
     *
     */
    @JSONField(name = "sale_order_id")
    @JsonProperty("sale_order_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long saleOrderId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long currencyId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[产品]不允许为空!")
    private Long productId;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[单位]不允许为空!")
    private Long productUomId;

    /**
     * 属性 [SHEET_ID]
     *
     */
    @JSONField(name = "sheet_id")
    @JsonProperty("sheet_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long sheetId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long accountId;

    /**
     * 属性 [COMPANY_CURRENCY_ID]
     *
     */
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyCurrencyId;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID]
     *
     */
    @JSONField(name = "analytic_account_id")
    @JsonProperty("analytic_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long analyticAccountId;


    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [TOTAL_AMOUNT]
     */
    public void setTotalAmount(BigDecimal  totalAmount){
        this.totalAmount = totalAmount ;
        this.modify("total_amount",totalAmount);
    }

    /**
     * 设置 [UNIT_AMOUNT]
     */
    public void setUnitAmount(Double  unitAmount){
        this.unitAmount = unitAmount ;
        this.modify("unit_amount",unitAmount);
    }

    /**
     * 设置 [PAYMENT_MODE]
     */
    public void setPaymentMode(String  paymentMode){
        this.paymentMode = paymentMode ;
        this.modify("payment_mode",paymentMode);
    }

    /**
     * 设置 [TOTAL_AMOUNT_COMPANY]
     */
    public void setTotalAmountCompany(BigDecimal  totalAmountCompany){
        this.totalAmountCompany = totalAmountCompany ;
        this.modify("total_amount_company",totalAmountCompany);
    }

    /**
     * 设置 [IS_REFUSED]
     */
    public void setIsRefused(Boolean  isRefused){
        this.isRefused = isRefused ;
        this.modify("is_refused",isRefused);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [QUANTITY]
     */
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [UNTAXED_AMOUNT]
     */
    public void setUntaxedAmount(Double  untaxedAmount){
        this.untaxedAmount = untaxedAmount ;
        this.modify("untaxed_amount",untaxedAmount);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [REFERENCE]
     */
    public void setReference(String  reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [EMPLOYEE_ID]
     */
    public void setEmployeeId(Long  employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }

    /**
     * 设置 [SALE_ORDER_ID]
     */
    public void setSaleOrderId(Long  saleOrderId){
        this.saleOrderId = saleOrderId ;
        this.modify("sale_order_id",saleOrderId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    public void setProductUomId(Long  productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [SHEET_ID]
     */
    public void setSheetId(Long  sheetId){
        this.sheetId = sheetId ;
        this.modify("sheet_id",sheetId);
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    public void setAccountId(Long  accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [COMPANY_CURRENCY_ID]
     */
    public void setCompanyCurrencyId(Long  companyCurrencyId){
        this.companyCurrencyId = companyCurrencyId ;
        this.modify("company_currency_id",companyCurrencyId);
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID]
     */
    public void setAnalyticAccountId(Long  analyticAccountId){
        this.analyticAccountId = analyticAccountId ;
        this.modify("analytic_account_id",analyticAccountId);
    }


}


