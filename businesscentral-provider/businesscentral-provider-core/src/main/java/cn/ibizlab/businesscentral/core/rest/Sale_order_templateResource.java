package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_templateService;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_templateSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"报价单模板" })
@RestController("Core-sale_order_template")
@RequestMapping("")
public class Sale_order_templateResource {

    @Autowired
    public ISale_order_templateService sale_order_templateService;

    @Autowired
    @Lazy
    public Sale_order_templateMapping sale_order_templateMapping;

    @PreAuthorize("hasPermission(this.sale_order_templateMapping.toDomain(#sale_order_templatedto),'iBizBusinessCentral-Sale_order_template-Create')")
    @ApiOperation(value = "新建报价单模板", tags = {"报价单模板" },  notes = "新建报价单模板")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_templates")
    public ResponseEntity<Sale_order_templateDTO> create(@Validated @RequestBody Sale_order_templateDTO sale_order_templatedto) {
        Sale_order_template domain = sale_order_templateMapping.toDomain(sale_order_templatedto);
		sale_order_templateService.create(domain);
        Sale_order_templateDTO dto = sale_order_templateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_order_templateMapping.toDomain(#sale_order_templatedtos),'iBizBusinessCentral-Sale_order_template-Create')")
    @ApiOperation(value = "批量新建报价单模板", tags = {"报价单模板" },  notes = "批量新建报价单模板")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_templates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sale_order_templateDTO> sale_order_templatedtos) {
        sale_order_templateService.createBatch(sale_order_templateMapping.toDomain(sale_order_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "sale_order_template" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.sale_order_templateService.get(#sale_order_template_id),'iBizBusinessCentral-Sale_order_template-Update')")
    @ApiOperation(value = "更新报价单模板", tags = {"报价单模板" },  notes = "更新报价单模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_order_templates/{sale_order_template_id}")
    public ResponseEntity<Sale_order_templateDTO> update(@PathVariable("sale_order_template_id") Long sale_order_template_id, @RequestBody Sale_order_templateDTO sale_order_templatedto) {
		Sale_order_template domain  = sale_order_templateMapping.toDomain(sale_order_templatedto);
        domain .setId(sale_order_template_id);
		sale_order_templateService.update(domain );
		Sale_order_templateDTO dto = sale_order_templateMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_order_templateService.getSaleOrderTemplateByEntities(this.sale_order_templateMapping.toDomain(#sale_order_templatedtos)),'iBizBusinessCentral-Sale_order_template-Update')")
    @ApiOperation(value = "批量更新报价单模板", tags = {"报价单模板" },  notes = "批量更新报价单模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_order_templates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_order_templateDTO> sale_order_templatedtos) {
        sale_order_templateService.updateBatch(sale_order_templateMapping.toDomain(sale_order_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.sale_order_templateService.get(#sale_order_template_id),'iBizBusinessCentral-Sale_order_template-Remove')")
    @ApiOperation(value = "删除报价单模板", tags = {"报价单模板" },  notes = "删除报价单模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_templates/{sale_order_template_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("sale_order_template_id") Long sale_order_template_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sale_order_templateService.remove(sale_order_template_id));
    }

    @PreAuthorize("hasPermission(this.sale_order_templateService.getSaleOrderTemplateByIds(#ids),'iBizBusinessCentral-Sale_order_template-Remove')")
    @ApiOperation(value = "批量删除报价单模板", tags = {"报价单模板" },  notes = "批量删除报价单模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_templates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        sale_order_templateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.sale_order_templateMapping.toDomain(returnObject.body),'iBizBusinessCentral-Sale_order_template-Get')")
    @ApiOperation(value = "获取报价单模板", tags = {"报价单模板" },  notes = "获取报价单模板")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_order_templates/{sale_order_template_id}")
    public ResponseEntity<Sale_order_templateDTO> get(@PathVariable("sale_order_template_id") Long sale_order_template_id) {
        Sale_order_template domain = sale_order_templateService.get(sale_order_template_id);
        Sale_order_templateDTO dto = sale_order_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取报价单模板草稿", tags = {"报价单模板" },  notes = "获取报价单模板草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_order_templates/getdraft")
    public ResponseEntity<Sale_order_templateDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(sale_order_templateMapping.toDto(sale_order_templateService.getDraft(new Sale_order_template())));
    }

    @ApiOperation(value = "检查报价单模板", tags = {"报价单模板" },  notes = "检查报价单模板")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_templates/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sale_order_templateDTO sale_order_templatedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(sale_order_templateService.checkKey(sale_order_templateMapping.toDomain(sale_order_templatedto)));
    }

    @PreAuthorize("hasPermission(this.sale_order_templateMapping.toDomain(#sale_order_templatedto),'iBizBusinessCentral-Sale_order_template-Save')")
    @ApiOperation(value = "保存报价单模板", tags = {"报价单模板" },  notes = "保存报价单模板")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_templates/save")
    public ResponseEntity<Boolean> save(@RequestBody Sale_order_templateDTO sale_order_templatedto) {
        return ResponseEntity.status(HttpStatus.OK).body(sale_order_templateService.save(sale_order_templateMapping.toDomain(sale_order_templatedto)));
    }

    @PreAuthorize("hasPermission(this.sale_order_templateMapping.toDomain(#sale_order_templatedtos),'iBizBusinessCentral-Sale_order_template-Save')")
    @ApiOperation(value = "批量保存报价单模板", tags = {"报价单模板" },  notes = "批量保存报价单模板")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_templates/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Sale_order_templateDTO> sale_order_templatedtos) {
        sale_order_templateService.saveBatch(sale_order_templateMapping.toDomain(sale_order_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_order_template-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_order_template-Get')")
	@ApiOperation(value = "获取数据集", tags = {"报价单模板" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/sale_order_templates/fetchdefault")
	public ResponseEntity<List<Sale_order_templateDTO>> fetchDefault(Sale_order_templateSearchContext context) {
        Page<Sale_order_template> domains = sale_order_templateService.searchDefault(context) ;
        List<Sale_order_templateDTO> list = sale_order_templateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_order_template-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_order_template-Get')")
	@ApiOperation(value = "查询数据集", tags = {"报价单模板" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/sale_order_templates/searchdefault")
	public ResponseEntity<Page<Sale_order_templateDTO>> searchDefault(@RequestBody Sale_order_templateSearchContext context) {
        Page<Sale_order_template> domains = sale_order_templateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sale_order_templateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

