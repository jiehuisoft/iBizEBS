package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoiceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"发票" })
@RestController("Core-account_invoice")
@RequestMapping("")
public class Account_invoiceResource {

    @Autowired
    public IAccount_invoiceService account_invoiceService;

    @Autowired
    @Lazy
    public Account_invoiceMapping account_invoiceMapping;

    @PreAuthorize("hasPermission(this.account_invoiceMapping.toDomain(#account_invoicedto),'iBizBusinessCentral-Account_invoice-Create')")
    @ApiOperation(value = "新建发票", tags = {"发票" },  notes = "新建发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices")
    public ResponseEntity<Account_invoiceDTO> create(@Validated @RequestBody Account_invoiceDTO account_invoicedto) {
        Account_invoice domain = account_invoiceMapping.toDomain(account_invoicedto);
		account_invoiceService.create(domain);
        Account_invoiceDTO dto = account_invoiceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoiceMapping.toDomain(#account_invoicedtos),'iBizBusinessCentral-Account_invoice-Create')")
    @ApiOperation(value = "批量新建发票", tags = {"发票" },  notes = "批量新建发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        account_invoiceService.createBatch(account_invoiceMapping.toDomain(account_invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_invoice" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_invoiceService.get(#account_invoice_id),'iBizBusinessCentral-Account_invoice-Update')")
    @ApiOperation(value = "更新发票", tags = {"发票" },  notes = "更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoices/{account_invoice_id}")
    public ResponseEntity<Account_invoiceDTO> update(@PathVariable("account_invoice_id") Long account_invoice_id, @RequestBody Account_invoiceDTO account_invoicedto) {
		Account_invoice domain  = account_invoiceMapping.toDomain(account_invoicedto);
        domain .setId(account_invoice_id);
		account_invoiceService.update(domain );
		Account_invoiceDTO dto = account_invoiceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoiceService.getAccountInvoiceByEntities(this.account_invoiceMapping.toDomain(#account_invoicedtos)),'iBizBusinessCentral-Account_invoice-Update')")
    @ApiOperation(value = "批量更新发票", tags = {"发票" },  notes = "批量更新发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoices/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        account_invoiceService.updateBatch(account_invoiceMapping.toDomain(account_invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_invoiceService.get(#account_invoice_id),'iBizBusinessCentral-Account_invoice-Remove')")
    @ApiOperation(value = "删除发票", tags = {"发票" },  notes = "删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoices/{account_invoice_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_id") Long account_invoice_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoiceService.remove(account_invoice_id));
    }

    @PreAuthorize("hasPermission(this.account_invoiceService.getAccountInvoiceByIds(#ids),'iBizBusinessCentral-Account_invoice-Remove')")
    @ApiOperation(value = "批量删除发票", tags = {"发票" },  notes = "批量删除发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoices/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_invoiceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_invoiceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_invoice-Get')")
    @ApiOperation(value = "获取发票", tags = {"发票" },  notes = "获取发票")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoices/{account_invoice_id}")
    public ResponseEntity<Account_invoiceDTO> get(@PathVariable("account_invoice_id") Long account_invoice_id) {
        Account_invoice domain = account_invoiceService.get(account_invoice_id);
        Account_invoiceDTO dto = account_invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取发票草稿", tags = {"发票" },  notes = "获取发票草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoices/getdraft")
    public ResponseEntity<Account_invoiceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoiceMapping.toDto(account_invoiceService.getDraft(new Account_invoice())));
    }

    @ApiOperation(value = "检查发票", tags = {"发票" },  notes = "检查发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoiceDTO account_invoicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_invoiceService.checkKey(account_invoiceMapping.toDomain(account_invoicedto)));
    }

    @PreAuthorize("hasPermission(this.account_invoiceMapping.toDomain(#account_invoicedto),'iBizBusinessCentral-Account_invoice-Save')")
    @ApiOperation(value = "保存发票", tags = {"发票" },  notes = "保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_invoiceDTO account_invoicedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoiceService.save(account_invoiceMapping.toDomain(account_invoicedto)));
    }

    @PreAuthorize("hasPermission(this.account_invoiceMapping.toDomain(#account_invoicedtos),'iBizBusinessCentral-Account_invoice-Save')")
    @ApiOperation(value = "批量保存发票", tags = {"发票" },  notes = "批量保存发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        account_invoiceService.saveBatch(account_invoiceMapping.toDomain(account_invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice-Get')")
	@ApiOperation(value = "获取数据集", tags = {"发票" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoices/fetchdefault")
	public ResponseEntity<List<Account_invoiceDTO>> fetchDefault(Account_invoiceSearchContext context) {
        Page<Account_invoice> domains = account_invoiceService.searchDefault(context) ;
        List<Account_invoiceDTO> list = account_invoiceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice-Get')")
	@ApiOperation(value = "查询数据集", tags = {"发票" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_invoices/searchdefault")
	public ResponseEntity<Page<Account_invoiceDTO>> searchDefault(@RequestBody Account_invoiceSearchContext context) {
        Page<Account_invoice> domains = account_invoiceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

