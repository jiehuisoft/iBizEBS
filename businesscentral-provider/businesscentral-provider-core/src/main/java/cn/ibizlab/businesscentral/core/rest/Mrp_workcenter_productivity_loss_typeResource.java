package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenter_productivity_loss_typeService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenter_productivity_loss_typeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"MRP工单生产力损失" })
@RestController("Core-mrp_workcenter_productivity_loss_type")
@RequestMapping("")
public class Mrp_workcenter_productivity_loss_typeResource {

    @Autowired
    public IMrp_workcenter_productivity_loss_typeService mrp_workcenter_productivity_loss_typeService;

    @Autowired
    @Lazy
    public Mrp_workcenter_productivity_loss_typeMapping mrp_workcenter_productivity_loss_typeMapping;

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_loss_typeMapping.toDomain(#mrp_workcenter_productivity_loss_typedto),'iBizBusinessCentral-Mrp_workcenter_productivity_loss_type-Create')")
    @ApiOperation(value = "新建MRP工单生产力损失", tags = {"MRP工单生产力损失" },  notes = "新建MRP工单生产力损失")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types")
    public ResponseEntity<Mrp_workcenter_productivity_loss_typeDTO> create(@Validated @RequestBody Mrp_workcenter_productivity_loss_typeDTO mrp_workcenter_productivity_loss_typedto) {
        Mrp_workcenter_productivity_loss_type domain = mrp_workcenter_productivity_loss_typeMapping.toDomain(mrp_workcenter_productivity_loss_typedto);
		mrp_workcenter_productivity_loss_typeService.create(domain);
        Mrp_workcenter_productivity_loss_typeDTO dto = mrp_workcenter_productivity_loss_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_loss_typeMapping.toDomain(#mrp_workcenter_productivity_loss_typedtos),'iBizBusinessCentral-Mrp_workcenter_productivity_loss_type-Create')")
    @ApiOperation(value = "批量新建MRP工单生产力损失", tags = {"MRP工单生产力损失" },  notes = "批量新建MRP工单生产力损失")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_workcenter_productivity_loss_typeDTO> mrp_workcenter_productivity_loss_typedtos) {
        mrp_workcenter_productivity_loss_typeService.createBatch(mrp_workcenter_productivity_loss_typeMapping.toDomain(mrp_workcenter_productivity_loss_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_workcenter_productivity_loss_type" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_loss_typeService.get(#mrp_workcenter_productivity_loss_type_id),'iBizBusinessCentral-Mrp_workcenter_productivity_loss_type-Update')")
    @ApiOperation(value = "更新MRP工单生产力损失", tags = {"MRP工单生产力损失" },  notes = "更新MRP工单生产力损失")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivity_loss_types/{mrp_workcenter_productivity_loss_type_id}")
    public ResponseEntity<Mrp_workcenter_productivity_loss_typeDTO> update(@PathVariable("mrp_workcenter_productivity_loss_type_id") Long mrp_workcenter_productivity_loss_type_id, @RequestBody Mrp_workcenter_productivity_loss_typeDTO mrp_workcenter_productivity_loss_typedto) {
		Mrp_workcenter_productivity_loss_type domain  = mrp_workcenter_productivity_loss_typeMapping.toDomain(mrp_workcenter_productivity_loss_typedto);
        domain .setId(mrp_workcenter_productivity_loss_type_id);
		mrp_workcenter_productivity_loss_typeService.update(domain );
		Mrp_workcenter_productivity_loss_typeDTO dto = mrp_workcenter_productivity_loss_typeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_loss_typeService.getMrpWorkcenterProductivityLossTypeByEntities(this.mrp_workcenter_productivity_loss_typeMapping.toDomain(#mrp_workcenter_productivity_loss_typedtos)),'iBizBusinessCentral-Mrp_workcenter_productivity_loss_type-Update')")
    @ApiOperation(value = "批量更新MRP工单生产力损失", tags = {"MRP工单生产力损失" },  notes = "批量更新MRP工单生产力损失")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivity_loss_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workcenter_productivity_loss_typeDTO> mrp_workcenter_productivity_loss_typedtos) {
        mrp_workcenter_productivity_loss_typeService.updateBatch(mrp_workcenter_productivity_loss_typeMapping.toDomain(mrp_workcenter_productivity_loss_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_loss_typeService.get(#mrp_workcenter_productivity_loss_type_id),'iBizBusinessCentral-Mrp_workcenter_productivity_loss_type-Remove')")
    @ApiOperation(value = "删除MRP工单生产力损失", tags = {"MRP工单生产力损失" },  notes = "删除MRP工单生产力损失")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivity_loss_types/{mrp_workcenter_productivity_loss_type_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workcenter_productivity_loss_type_id") Long mrp_workcenter_productivity_loss_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivity_loss_typeService.remove(mrp_workcenter_productivity_loss_type_id));
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_loss_typeService.getMrpWorkcenterProductivityLossTypeByIds(#ids),'iBizBusinessCentral-Mrp_workcenter_productivity_loss_type-Remove')")
    @ApiOperation(value = "批量删除MRP工单生产力损失", tags = {"MRP工单生产力损失" },  notes = "批量删除MRP工单生产力损失")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivity_loss_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_workcenter_productivity_loss_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_workcenter_productivity_loss_typeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_workcenter_productivity_loss_type-Get')")
    @ApiOperation(value = "获取MRP工单生产力损失", tags = {"MRP工单生产力损失" },  notes = "获取MRP工单生产力损失")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivity_loss_types/{mrp_workcenter_productivity_loss_type_id}")
    public ResponseEntity<Mrp_workcenter_productivity_loss_typeDTO> get(@PathVariable("mrp_workcenter_productivity_loss_type_id") Long mrp_workcenter_productivity_loss_type_id) {
        Mrp_workcenter_productivity_loss_type domain = mrp_workcenter_productivity_loss_typeService.get(mrp_workcenter_productivity_loss_type_id);
        Mrp_workcenter_productivity_loss_typeDTO dto = mrp_workcenter_productivity_loss_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取MRP工单生产力损失草稿", tags = {"MRP工单生产力损失" },  notes = "获取MRP工单生产力损失草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivity_loss_types/getdraft")
    public ResponseEntity<Mrp_workcenter_productivity_loss_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivity_loss_typeMapping.toDto(mrp_workcenter_productivity_loss_typeService.getDraft(new Mrp_workcenter_productivity_loss_type())));
    }

    @ApiOperation(value = "检查MRP工单生产力损失", tags = {"MRP工单生产力损失" },  notes = "检查MRP工单生产力损失")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_workcenter_productivity_loss_typeDTO mrp_workcenter_productivity_loss_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivity_loss_typeService.checkKey(mrp_workcenter_productivity_loss_typeMapping.toDomain(mrp_workcenter_productivity_loss_typedto)));
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_loss_typeMapping.toDomain(#mrp_workcenter_productivity_loss_typedto),'iBizBusinessCentral-Mrp_workcenter_productivity_loss_type-Save')")
    @ApiOperation(value = "保存MRP工单生产力损失", tags = {"MRP工单生产力损失" },  notes = "保存MRP工单生产力损失")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_workcenter_productivity_loss_typeDTO mrp_workcenter_productivity_loss_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivity_loss_typeService.save(mrp_workcenter_productivity_loss_typeMapping.toDomain(mrp_workcenter_productivity_loss_typedto)));
    }

    @PreAuthorize("hasPermission(this.mrp_workcenter_productivity_loss_typeMapping.toDomain(#mrp_workcenter_productivity_loss_typedtos),'iBizBusinessCentral-Mrp_workcenter_productivity_loss_type-Save')")
    @ApiOperation(value = "批量保存MRP工单生产力损失", tags = {"MRP工单生产力损失" },  notes = "批量保存MRP工单生产力损失")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_workcenter_productivity_loss_typeDTO> mrp_workcenter_productivity_loss_typedtos) {
        mrp_workcenter_productivity_loss_typeService.saveBatch(mrp_workcenter_productivity_loss_typeMapping.toDomain(mrp_workcenter_productivity_loss_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_workcenter_productivity_loss_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_workcenter_productivity_loss_type-Get')")
	@ApiOperation(value = "获取数据集", tags = {"MRP工单生产力损失" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workcenter_productivity_loss_types/fetchdefault")
	public ResponseEntity<List<Mrp_workcenter_productivity_loss_typeDTO>> fetchDefault(Mrp_workcenter_productivity_loss_typeSearchContext context) {
        Page<Mrp_workcenter_productivity_loss_type> domains = mrp_workcenter_productivity_loss_typeService.searchDefault(context) ;
        List<Mrp_workcenter_productivity_loss_typeDTO> list = mrp_workcenter_productivity_loss_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_workcenter_productivity_loss_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_workcenter_productivity_loss_type-Get')")
	@ApiOperation(value = "查询数据集", tags = {"MRP工单生产力损失" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_workcenter_productivity_loss_types/searchdefault")
	public ResponseEntity<Page<Mrp_workcenter_productivity_loss_typeDTO>> searchDefault(@RequestBody Mrp_workcenter_productivity_loss_typeSearchContext context) {
        Page<Mrp_workcenter_productivity_loss_type> domains = mrp_workcenter_productivity_loss_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_workcenter_productivity_loss_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

