package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Stock_change_standard_priceDTO]
 */
@Data
public class Stock_change_standard_priceDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [COUNTERPART_ACCOUNT_ID_REQUIRED]
     *
     */
    @JSONField(name = "counterpart_account_id_required")
    @JsonProperty("counterpart_account_id_required")
    private Boolean counterpartAccountIdRequired;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [NEW_PRICE]
     *
     */
    @JSONField(name = "new_price")
    @JsonProperty("new_price")
    @NotNull(message = "[价格]不允许为空!")
    private Double newPrice;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [COUNTERPART_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "counterpart_account_id_text")
    @JsonProperty("counterpart_account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String counterpartAccountIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [COUNTERPART_ACCOUNT_ID]
     *
     */
    @JSONField(name = "counterpart_account_id")
    @JsonProperty("counterpart_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long counterpartAccountId;


    /**
     * 设置 [COUNTERPART_ACCOUNT_ID_REQUIRED]
     */
    public void setCounterpartAccountIdRequired(Boolean  counterpartAccountIdRequired){
        this.counterpartAccountIdRequired = counterpartAccountIdRequired ;
        this.modify("counterpart_account_id_required",counterpartAccountIdRequired);
    }

    /**
     * 设置 [NEW_PRICE]
     */
    public void setNewPrice(Double  newPrice){
        this.newPrice = newPrice ;
        this.modify("new_price",newPrice);
    }

    /**
     * 设置 [COUNTERPART_ACCOUNT_ID]
     */
    public void setCounterpartAccountId(Long  counterpartAccountId){
        this.counterpartAccountId = counterpartAccountId ;
        this.modify("counterpart_account_id",counterpartAccountId);
    }


}


