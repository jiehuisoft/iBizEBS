package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quantity_history;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quantity_historyService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_quantity_historySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库存数量历史" })
@RestController("Core-stock_quantity_history")
@RequestMapping("")
public class Stock_quantity_historyResource {

    @Autowired
    public IStock_quantity_historyService stock_quantity_historyService;

    @Autowired
    @Lazy
    public Stock_quantity_historyMapping stock_quantity_historyMapping;

    @PreAuthorize("hasPermission(this.stock_quantity_historyMapping.toDomain(#stock_quantity_historydto),'iBizBusinessCentral-Stock_quantity_history-Create')")
    @ApiOperation(value = "新建库存数量历史", tags = {"库存数量历史" },  notes = "新建库存数量历史")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quantity_histories")
    public ResponseEntity<Stock_quantity_historyDTO> create(@Validated @RequestBody Stock_quantity_historyDTO stock_quantity_historydto) {
        Stock_quantity_history domain = stock_quantity_historyMapping.toDomain(stock_quantity_historydto);
		stock_quantity_historyService.create(domain);
        Stock_quantity_historyDTO dto = stock_quantity_historyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_quantity_historyMapping.toDomain(#stock_quantity_historydtos),'iBizBusinessCentral-Stock_quantity_history-Create')")
    @ApiOperation(value = "批量新建库存数量历史", tags = {"库存数量历史" },  notes = "批量新建库存数量历史")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quantity_histories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_quantity_historyDTO> stock_quantity_historydtos) {
        stock_quantity_historyService.createBatch(stock_quantity_historyMapping.toDomain(stock_quantity_historydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_quantity_history" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_quantity_historyService.get(#stock_quantity_history_id),'iBizBusinessCentral-Stock_quantity_history-Update')")
    @ApiOperation(value = "更新库存数量历史", tags = {"库存数量历史" },  notes = "更新库存数量历史")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_quantity_histories/{stock_quantity_history_id}")
    public ResponseEntity<Stock_quantity_historyDTO> update(@PathVariable("stock_quantity_history_id") Long stock_quantity_history_id, @RequestBody Stock_quantity_historyDTO stock_quantity_historydto) {
		Stock_quantity_history domain  = stock_quantity_historyMapping.toDomain(stock_quantity_historydto);
        domain .setId(stock_quantity_history_id);
		stock_quantity_historyService.update(domain );
		Stock_quantity_historyDTO dto = stock_quantity_historyMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_quantity_historyService.getStockQuantityHistoryByEntities(this.stock_quantity_historyMapping.toDomain(#stock_quantity_historydtos)),'iBizBusinessCentral-Stock_quantity_history-Update')")
    @ApiOperation(value = "批量更新库存数量历史", tags = {"库存数量历史" },  notes = "批量更新库存数量历史")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_quantity_histories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_quantity_historyDTO> stock_quantity_historydtos) {
        stock_quantity_historyService.updateBatch(stock_quantity_historyMapping.toDomain(stock_quantity_historydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_quantity_historyService.get(#stock_quantity_history_id),'iBizBusinessCentral-Stock_quantity_history-Remove')")
    @ApiOperation(value = "删除库存数量历史", tags = {"库存数量历史" },  notes = "删除库存数量历史")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_quantity_histories/{stock_quantity_history_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_quantity_history_id") Long stock_quantity_history_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_quantity_historyService.remove(stock_quantity_history_id));
    }

    @PreAuthorize("hasPermission(this.stock_quantity_historyService.getStockQuantityHistoryByIds(#ids),'iBizBusinessCentral-Stock_quantity_history-Remove')")
    @ApiOperation(value = "批量删除库存数量历史", tags = {"库存数量历史" },  notes = "批量删除库存数量历史")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_quantity_histories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_quantity_historyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_quantity_historyMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_quantity_history-Get')")
    @ApiOperation(value = "获取库存数量历史", tags = {"库存数量历史" },  notes = "获取库存数量历史")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_quantity_histories/{stock_quantity_history_id}")
    public ResponseEntity<Stock_quantity_historyDTO> get(@PathVariable("stock_quantity_history_id") Long stock_quantity_history_id) {
        Stock_quantity_history domain = stock_quantity_historyService.get(stock_quantity_history_id);
        Stock_quantity_historyDTO dto = stock_quantity_historyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库存数量历史草稿", tags = {"库存数量历史" },  notes = "获取库存数量历史草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_quantity_histories/getdraft")
    public ResponseEntity<Stock_quantity_historyDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_quantity_historyMapping.toDto(stock_quantity_historyService.getDraft(new Stock_quantity_history())));
    }

    @ApiOperation(value = "检查库存数量历史", tags = {"库存数量历史" },  notes = "检查库存数量历史")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quantity_histories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_quantity_historyDTO stock_quantity_historydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_quantity_historyService.checkKey(stock_quantity_historyMapping.toDomain(stock_quantity_historydto)));
    }

    @PreAuthorize("hasPermission(this.stock_quantity_historyMapping.toDomain(#stock_quantity_historydto),'iBizBusinessCentral-Stock_quantity_history-Save')")
    @ApiOperation(value = "保存库存数量历史", tags = {"库存数量历史" },  notes = "保存库存数量历史")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quantity_histories/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_quantity_historyDTO stock_quantity_historydto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_quantity_historyService.save(stock_quantity_historyMapping.toDomain(stock_quantity_historydto)));
    }

    @PreAuthorize("hasPermission(this.stock_quantity_historyMapping.toDomain(#stock_quantity_historydtos),'iBizBusinessCentral-Stock_quantity_history-Save')")
    @ApiOperation(value = "批量保存库存数量历史", tags = {"库存数量历史" },  notes = "批量保存库存数量历史")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quantity_histories/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_quantity_historyDTO> stock_quantity_historydtos) {
        stock_quantity_historyService.saveBatch(stock_quantity_historyMapping.toDomain(stock_quantity_historydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_quantity_history-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_quantity_history-Get')")
	@ApiOperation(value = "获取数据集", tags = {"库存数量历史" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_quantity_histories/fetchdefault")
	public ResponseEntity<List<Stock_quantity_historyDTO>> fetchDefault(Stock_quantity_historySearchContext context) {
        Page<Stock_quantity_history> domains = stock_quantity_historyService.searchDefault(context) ;
        List<Stock_quantity_historyDTO> list = stock_quantity_historyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_quantity_history-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_quantity_history-Get')")
	@ApiOperation(value = "查询数据集", tags = {"库存数量历史" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_quantity_histories/searchdefault")
	public ResponseEntity<Page<Stock_quantity_historyDTO>> searchDefault(@RequestBody Stock_quantity_historySearchContext context) {
        Page<Stock_quantity_history> domains = stock_quantity_historyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_quantity_historyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

