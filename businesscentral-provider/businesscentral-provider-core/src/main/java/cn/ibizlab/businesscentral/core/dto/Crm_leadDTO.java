package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Crm_leadDTO]
 */
@Data
public class Crm_leadDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [MEETING_COUNT]
     *
     */
    @JSONField(name = "meeting_count")
    @JsonProperty("meeting_count")
    private Integer meetingCount;

    /**
     * 属性 [MESSAGE_BOUNCE]
     *
     */
    @JSONField(name = "message_bounce")
    @JsonProperty("message_bounce")
    private Integer messageBounce;

    /**
     * 属性 [CITY]
     *
     */
    @JSONField(name = "city")
    @JsonProperty("city")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String city;

    /**
     * 属性 [DAY_CLOSE]
     *
     */
    @JSONField(name = "day_close")
    @JsonProperty("day_close")
    private Double dayClose;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activityIds;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [EXPECTED_REVENUE]
     *
     */
    @JSONField(name = "expected_revenue")
    @JsonProperty("expected_revenue")
    private BigDecimal expectedRevenue;

    /**
     * 属性 [DATE_CLOSED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_closed" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_closed")
    private Timestamp dateClosed;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [EMAIL_CC]
     *
     */
    @JSONField(name = "email_cc")
    @JsonProperty("email_cc")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String emailCc;

    /**
     * 属性 [CONTACT_NAME]
     *
     */
    @JSONField(name = "contact_name")
    @JsonProperty("contact_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String contactName;

    /**
     * 属性 [DATE_LAST_STAGE_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_last_stage_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_last_stage_update")
    private Timestamp dateLastStageUpdate;

    /**
     * 属性 [PLANNED_REVENUE]
     *
     */
    @JSONField(name = "planned_revenue")
    @JsonProperty("planned_revenue")
    private BigDecimal plannedRevenue;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("date_deadline")
    private Timestamp dateDeadline;

    /**
     * 属性 [ZIP]
     *
     */
    @JSONField(name = "zip")
    @JsonProperty("zip")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String zip;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[商机]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String description;

    /**
     * 属性 [MOBILE]
     *
     */
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mobile;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityState;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    @NotBlank(message = "[类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String type;

    /**
     * 属性 [WEBSITE]
     *
     */
    @JSONField(name = "website")
    @JsonProperty("website")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String website;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emailFrom;

    /**
     * 属性 [DATE_CONVERSION]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_conversion" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_conversion")
    private Timestamp dateConversion;

    /**
     * 属性 [PARTNER_NAME]
     *
     */
    @JSONField(name = "partner_name")
    @JsonProperty("partner_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String partnerName;

    /**
     * 属性 [SALE_AMOUNT_TOTAL]
     *
     */
    @JSONField(name = "sale_amount_total")
    @JsonProperty("sale_amount_total")
    private BigDecimal saleAmountTotal;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [KANBAN_STATE]
     *
     */
    @JSONField(name = "kanban_state")
    @JsonProperty("kanban_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String kanbanState;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [REFERRED]
     *
     */
    @JSONField(name = "referred")
    @JsonProperty("referred")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String referred;

    /**
     * 属性 [PROBABILITY]
     *
     */
    @JSONField(name = "probability")
    @JsonProperty("probability")
    private Double probability;

    /**
     * 属性 [DATE_ACTION_LAST]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_action_last" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_action_last")
    private Timestamp dateActionLast;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [DATE_OPEN]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_open" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_open")
    private Timestamp dateOpen;

    /**
     * 属性 [PHONE]
     *
     */
    @JSONField(name = "phone")
    @JsonProperty("phone")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String phone;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [DAY_OPEN]
     *
     */
    @JSONField(name = "day_open")
    @JsonProperty("day_open")
    private Double dayOpen;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String activitySummary;

    /**
     * 属性 [SALE_NUMBER]
     *
     */
    @JSONField(name = "sale_number")
    @JsonProperty("sale_number")
    private Integer saleNumber;

    /**
     * 属性 [FUNCTION]
     *
     */
    @JSONField(name = "function")
    @JsonProperty("function")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String function;

    /**
     * 属性 [STREET2]
     *
     */
    @JSONField(name = "street2")
    @JsonProperty("street2")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String street2;

    /**
     * 属性 [ORDER_IDS]
     *
     */
    @JSONField(name = "order_ids")
    @JsonProperty("order_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String orderIds;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [STREET]
     *
     */
    @JSONField(name = "street")
    @JsonProperty("street")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String street;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String tagIds;

    /**
     * 属性 [IS_BLACKLISTED]
     *
     */
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private Boolean isBlacklisted;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [PRIORITY]
     *
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String priority;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [SOURCE_ID_TEXT]
     *
     */
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sourceIdText;

    /**
     * 属性 [PARTNER_ADDRESS_NAME]
     *
     */
    @JSONField(name = "partner_address_name")
    @JsonProperty("partner_address_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String partnerAddressName;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mediumIdText;

    /**
     * 属性 [COMPANY_CURRENCY]
     *
     */
    @JSONField(name = "company_currency")
    @JsonProperty("company_currency")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyCurrency;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String teamIdText;

    /**
     * 属性 [PARTNER_ADDRESS_MOBILE]
     *
     */
    @JSONField(name = "partner_address_mobile")
    @JsonProperty("partner_address_mobile")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String partnerAddressMobile;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [USER_EMAIL]
     *
     */
    @JSONField(name = "user_email")
    @JsonProperty("user_email")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String userEmail;

    /**
     * 属性 [USER_LOGIN]
     *
     */
    @JSONField(name = "user_login")
    @JsonProperty("user_login")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String userLogin;

    /**
     * 属性 [PARTNER_ADDRESS_PHONE]
     *
     */
    @JSONField(name = "partner_address_phone")
    @JsonProperty("partner_address_phone")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String partnerAddressPhone;

    /**
     * 属性 [STATE_ID_TEXT]
     *
     */
    @JSONField(name = "state_id_text")
    @JsonProperty("state_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String stateIdText;

    /**
     * 属性 [CAMPAIGN_ID_TEXT]
     *
     */
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String campaignIdText;

    /**
     * 属性 [PARTNER_IS_BLACKLISTED]
     *
     */
    @JSONField(name = "partner_is_blacklisted")
    @JsonProperty("partner_is_blacklisted")
    private Boolean partnerIsBlacklisted;

    /**
     * 属性 [PARTNER_ADDRESS_EMAIL]
     *
     */
    @JSONField(name = "partner_address_email")
    @JsonProperty("partner_address_email")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String partnerAddressEmail;

    /**
     * 属性 [STAGE_ID_TEXT]
     *
     */
    @JSONField(name = "stage_id_text")
    @JsonProperty("stage_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String stageIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String countryIdText;

    /**
     * 属性 [TITLE_TEXT]
     *
     */
    @JSONField(name = "title_text")
    @JsonProperty("title_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String titleText;

    /**
     * 属性 [LOST_REASON_TEXT]
     *
     */
    @JSONField(name = "lost_reason_text")
    @JsonProperty("lost_reason_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lostReasonText;

    /**
     * 属性 [LOST_REASON]
     *
     */
    @JSONField(name = "lost_reason")
    @JsonProperty("lost_reason")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long lostReason;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [STATE_ID]
     *
     */
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stateId;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long mediumId;

    /**
     * 属性 [STAGE_ID]
     *
     */
    @JSONField(name = "stage_id")
    @JsonProperty("stage_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stageId;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long sourceId;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long countryId;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long campaignId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long teamId;

    /**
     * 属性 [TITLE]
     *
     */
    @JSONField(name = "title")
    @JsonProperty("title")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long title;


    /**
     * 设置 [MESSAGE_BOUNCE]
     */
    public void setMessageBounce(Integer  messageBounce){
        this.messageBounce = messageBounce ;
        this.modify("message_bounce",messageBounce);
    }

    /**
     * 设置 [CITY]
     */
    public void setCity(String  city){
        this.city = city ;
        this.modify("city",city);
    }

    /**
     * 设置 [DAY_CLOSE]
     */
    public void setDayClose(Double  dayClose){
        this.dayClose = dayClose ;
        this.modify("day_close",dayClose);
    }

    /**
     * 设置 [EXPECTED_REVENUE]
     */
    public void setExpectedRevenue(BigDecimal  expectedRevenue){
        this.expectedRevenue = expectedRevenue ;
        this.modify("expected_revenue",expectedRevenue);
    }

    /**
     * 设置 [DATE_CLOSED]
     */
    public void setDateClosed(Timestamp  dateClosed){
        this.dateClosed = dateClosed ;
        this.modify("date_closed",dateClosed);
    }

    /**
     * 设置 [EMAIL_CC]
     */
    public void setEmailCc(String  emailCc){
        this.emailCc = emailCc ;
        this.modify("email_cc",emailCc);
    }

    /**
     * 设置 [CONTACT_NAME]
     */
    public void setContactName(String  contactName){
        this.contactName = contactName ;
        this.modify("contact_name",contactName);
    }

    /**
     * 设置 [DATE_LAST_STAGE_UPDATE]
     */
    public void setDateLastStageUpdate(Timestamp  dateLastStageUpdate){
        this.dateLastStageUpdate = dateLastStageUpdate ;
        this.modify("date_last_stage_update",dateLastStageUpdate);
    }

    /**
     * 设置 [PLANNED_REVENUE]
     */
    public void setPlannedRevenue(BigDecimal  plannedRevenue){
        this.plannedRevenue = plannedRevenue ;
        this.modify("planned_revenue",plannedRevenue);
    }

    /**
     * 设置 [DATE_DEADLINE]
     */
    public void setDateDeadline(Timestamp  dateDeadline){
        this.dateDeadline = dateDeadline ;
        this.modify("date_deadline",dateDeadline);
    }

    /**
     * 设置 [ZIP]
     */
    public void setZip(String  zip){
        this.zip = zip ;
        this.modify("zip",zip);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [MOBILE]
     */
    public void setMobile(String  mobile){
        this.mobile = mobile ;
        this.modify("mobile",mobile);
    }

    /**
     * 设置 [TYPE]
     */
    public void setType(String  type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [WEBSITE]
     */
    public void setWebsite(String  website){
        this.website = website ;
        this.modify("website",website);
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    public void setEmailFrom(String  emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }

    /**
     * 设置 [DATE_CONVERSION]
     */
    public void setDateConversion(Timestamp  dateConversion){
        this.dateConversion = dateConversion ;
        this.modify("date_conversion",dateConversion);
    }

    /**
     * 设置 [PARTNER_NAME]
     */
    public void setPartnerName(String  partnerName){
        this.partnerName = partnerName ;
        this.modify("partner_name",partnerName);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [REFERRED]
     */
    public void setReferred(String  referred){
        this.referred = referred ;
        this.modify("referred",referred);
    }

    /**
     * 设置 [PROBABILITY]
     */
    public void setProbability(Double  probability){
        this.probability = probability ;
        this.modify("probability",probability);
    }

    /**
     * 设置 [DATE_ACTION_LAST]
     */
    public void setDateActionLast(Timestamp  dateActionLast){
        this.dateActionLast = dateActionLast ;
        this.modify("date_action_last",dateActionLast);
    }

    /**
     * 设置 [DATE_OPEN]
     */
    public void setDateOpen(Timestamp  dateOpen){
        this.dateOpen = dateOpen ;
        this.modify("date_open",dateOpen);
    }

    /**
     * 设置 [PHONE]
     */
    public void setPhone(String  phone){
        this.phone = phone ;
        this.modify("phone",phone);
    }

    /**
     * 设置 [DAY_OPEN]
     */
    public void setDayOpen(Double  dayOpen){
        this.dayOpen = dayOpen ;
        this.modify("day_open",dayOpen);
    }

    /**
     * 设置 [FUNCTION]
     */
    public void setFunction(String  function){
        this.function = function ;
        this.modify("function",function);
    }

    /**
     * 设置 [STREET2]
     */
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.modify("street2",street2);
    }

    /**
     * 设置 [STREET]
     */
    public void setStreet(String  street){
        this.street = street ;
        this.modify("street",street);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [PRIORITY]
     */
    public void setPriority(String  priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [LOST_REASON]
     */
    public void setLostReason(Long  lostReason){
        this.lostReason = lostReason ;
        this.modify("lost_reason",lostReason);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [STATE_ID]
     */
    public void setStateId(Long  stateId){
        this.stateId = stateId ;
        this.modify("state_id",stateId);
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    public void setMediumId(Long  mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }

    /**
     * 设置 [STAGE_ID]
     */
    public void setStageId(Long  stageId){
        this.stageId = stageId ;
        this.modify("stage_id",stageId);
    }

    /**
     * 设置 [SOURCE_ID]
     */
    public void setSourceId(Long  sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    public void setCountryId(Long  countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    public void setCampaignId(Long  campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [TEAM_ID]
     */
    public void setTeamId(Long  teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }

    /**
     * 设置 [TITLE]
     */
    public void setTitle(Long  title){
        this.title = title ;
        this.modify("title",title);
    }


}


