package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_fixed_putaway_strat;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_fixed_putaway_stratService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_fixed_putaway_stratSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"位置固定上架策略" })
@RestController("Core-stock_fixed_putaway_strat")
@RequestMapping("")
public class Stock_fixed_putaway_stratResource {

    @Autowired
    public IStock_fixed_putaway_stratService stock_fixed_putaway_stratService;

    @Autowired
    @Lazy
    public Stock_fixed_putaway_stratMapping stock_fixed_putaway_stratMapping;

    @PreAuthorize("hasPermission(this.stock_fixed_putaway_stratMapping.toDomain(#stock_fixed_putaway_stratdto),'iBizBusinessCentral-Stock_fixed_putaway_strat-Create')")
    @ApiOperation(value = "新建位置固定上架策略", tags = {"位置固定上架策略" },  notes = "新建位置固定上架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats")
    public ResponseEntity<Stock_fixed_putaway_stratDTO> create(@Validated @RequestBody Stock_fixed_putaway_stratDTO stock_fixed_putaway_stratdto) {
        Stock_fixed_putaway_strat domain = stock_fixed_putaway_stratMapping.toDomain(stock_fixed_putaway_stratdto);
		stock_fixed_putaway_stratService.create(domain);
        Stock_fixed_putaway_stratDTO dto = stock_fixed_putaway_stratMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_fixed_putaway_stratMapping.toDomain(#stock_fixed_putaway_stratdtos),'iBizBusinessCentral-Stock_fixed_putaway_strat-Create')")
    @ApiOperation(value = "批量新建位置固定上架策略", tags = {"位置固定上架策略" },  notes = "批量新建位置固定上架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_fixed_putaway_stratDTO> stock_fixed_putaway_stratdtos) {
        stock_fixed_putaway_stratService.createBatch(stock_fixed_putaway_stratMapping.toDomain(stock_fixed_putaway_stratdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_fixed_putaway_strat" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_fixed_putaway_stratService.get(#stock_fixed_putaway_strat_id),'iBizBusinessCentral-Stock_fixed_putaway_strat-Update')")
    @ApiOperation(value = "更新位置固定上架策略", tags = {"位置固定上架策略" },  notes = "更新位置固定上架策略")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_fixed_putaway_strats/{stock_fixed_putaway_strat_id}")
    public ResponseEntity<Stock_fixed_putaway_stratDTO> update(@PathVariable("stock_fixed_putaway_strat_id") Long stock_fixed_putaway_strat_id, @RequestBody Stock_fixed_putaway_stratDTO stock_fixed_putaway_stratdto) {
		Stock_fixed_putaway_strat domain  = stock_fixed_putaway_stratMapping.toDomain(stock_fixed_putaway_stratdto);
        domain .setId(stock_fixed_putaway_strat_id);
		stock_fixed_putaway_stratService.update(domain );
		Stock_fixed_putaway_stratDTO dto = stock_fixed_putaway_stratMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_fixed_putaway_stratService.getStockFixedPutawayStratByEntities(this.stock_fixed_putaway_stratMapping.toDomain(#stock_fixed_putaway_stratdtos)),'iBizBusinessCentral-Stock_fixed_putaway_strat-Update')")
    @ApiOperation(value = "批量更新位置固定上架策略", tags = {"位置固定上架策略" },  notes = "批量更新位置固定上架策略")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_fixed_putaway_strats/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_fixed_putaway_stratDTO> stock_fixed_putaway_stratdtos) {
        stock_fixed_putaway_stratService.updateBatch(stock_fixed_putaway_stratMapping.toDomain(stock_fixed_putaway_stratdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_fixed_putaway_stratService.get(#stock_fixed_putaway_strat_id),'iBizBusinessCentral-Stock_fixed_putaway_strat-Remove')")
    @ApiOperation(value = "删除位置固定上架策略", tags = {"位置固定上架策略" },  notes = "删除位置固定上架策略")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_fixed_putaway_strats/{stock_fixed_putaway_strat_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_fixed_putaway_strat_id") Long stock_fixed_putaway_strat_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_fixed_putaway_stratService.remove(stock_fixed_putaway_strat_id));
    }

    @PreAuthorize("hasPermission(this.stock_fixed_putaway_stratService.getStockFixedPutawayStratByIds(#ids),'iBizBusinessCentral-Stock_fixed_putaway_strat-Remove')")
    @ApiOperation(value = "批量删除位置固定上架策略", tags = {"位置固定上架策略" },  notes = "批量删除位置固定上架策略")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_fixed_putaway_strats/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_fixed_putaway_stratService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_fixed_putaway_stratMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_fixed_putaway_strat-Get')")
    @ApiOperation(value = "获取位置固定上架策略", tags = {"位置固定上架策略" },  notes = "获取位置固定上架策略")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_fixed_putaway_strats/{stock_fixed_putaway_strat_id}")
    public ResponseEntity<Stock_fixed_putaway_stratDTO> get(@PathVariable("stock_fixed_putaway_strat_id") Long stock_fixed_putaway_strat_id) {
        Stock_fixed_putaway_strat domain = stock_fixed_putaway_stratService.get(stock_fixed_putaway_strat_id);
        Stock_fixed_putaway_stratDTO dto = stock_fixed_putaway_stratMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取位置固定上架策略草稿", tags = {"位置固定上架策略" },  notes = "获取位置固定上架策略草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_fixed_putaway_strats/getdraft")
    public ResponseEntity<Stock_fixed_putaway_stratDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_fixed_putaway_stratMapping.toDto(stock_fixed_putaway_stratService.getDraft(new Stock_fixed_putaway_strat())));
    }

    @ApiOperation(value = "检查位置固定上架策略", tags = {"位置固定上架策略" },  notes = "检查位置固定上架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_fixed_putaway_stratDTO stock_fixed_putaway_stratdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_fixed_putaway_stratService.checkKey(stock_fixed_putaway_stratMapping.toDomain(stock_fixed_putaway_stratdto)));
    }

    @PreAuthorize("hasPermission(this.stock_fixed_putaway_stratMapping.toDomain(#stock_fixed_putaway_stratdto),'iBizBusinessCentral-Stock_fixed_putaway_strat-Save')")
    @ApiOperation(value = "保存位置固定上架策略", tags = {"位置固定上架策略" },  notes = "保存位置固定上架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_fixed_putaway_stratDTO stock_fixed_putaway_stratdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_fixed_putaway_stratService.save(stock_fixed_putaway_stratMapping.toDomain(stock_fixed_putaway_stratdto)));
    }

    @PreAuthorize("hasPermission(this.stock_fixed_putaway_stratMapping.toDomain(#stock_fixed_putaway_stratdtos),'iBizBusinessCentral-Stock_fixed_putaway_strat-Save')")
    @ApiOperation(value = "批量保存位置固定上架策略", tags = {"位置固定上架策略" },  notes = "批量保存位置固定上架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_fixed_putaway_stratDTO> stock_fixed_putaway_stratdtos) {
        stock_fixed_putaway_stratService.saveBatch(stock_fixed_putaway_stratMapping.toDomain(stock_fixed_putaway_stratdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_fixed_putaway_strat-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_fixed_putaway_strat-Get')")
	@ApiOperation(value = "获取数据集", tags = {"位置固定上架策略" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_fixed_putaway_strats/fetchdefault")
	public ResponseEntity<List<Stock_fixed_putaway_stratDTO>> fetchDefault(Stock_fixed_putaway_stratSearchContext context) {
        Page<Stock_fixed_putaway_strat> domains = stock_fixed_putaway_stratService.searchDefault(context) ;
        List<Stock_fixed_putaway_stratDTO> list = stock_fixed_putaway_stratMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_fixed_putaway_strat-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_fixed_putaway_strat-Get')")
	@ApiOperation(value = "查询数据集", tags = {"位置固定上架策略" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_fixed_putaway_strats/searchdefault")
	public ResponseEntity<Page<Stock_fixed_putaway_stratDTO>> searchDefault(@RequestBody Stock_fixed_putaway_stratSearchContext context) {
        Page<Stock_fixed_putaway_strat> domains = stock_fixed_putaway_stratService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_fixed_putaway_stratMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

