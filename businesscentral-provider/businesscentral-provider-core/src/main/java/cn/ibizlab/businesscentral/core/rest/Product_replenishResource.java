package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_replenish;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_replenishService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_replenishSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"补料" })
@RestController("Core-product_replenish")
@RequestMapping("")
public class Product_replenishResource {

    @Autowired
    public IProduct_replenishService product_replenishService;

    @Autowired
    @Lazy
    public Product_replenishMapping product_replenishMapping;

    @PreAuthorize("hasPermission(this.product_replenishMapping.toDomain(#product_replenishdto),'iBizBusinessCentral-Product_replenish-Create')")
    @ApiOperation(value = "新建补料", tags = {"补料" },  notes = "新建补料")
	@RequestMapping(method = RequestMethod.POST, value = "/product_replenishes")
    public ResponseEntity<Product_replenishDTO> create(@Validated @RequestBody Product_replenishDTO product_replenishdto) {
        Product_replenish domain = product_replenishMapping.toDomain(product_replenishdto);
		product_replenishService.create(domain);
        Product_replenishDTO dto = product_replenishMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_replenishMapping.toDomain(#product_replenishdtos),'iBizBusinessCentral-Product_replenish-Create')")
    @ApiOperation(value = "批量新建补料", tags = {"补料" },  notes = "批量新建补料")
	@RequestMapping(method = RequestMethod.POST, value = "/product_replenishes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_replenishDTO> product_replenishdtos) {
        product_replenishService.createBatch(product_replenishMapping.toDomain(product_replenishdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_replenish" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_replenishService.get(#product_replenish_id),'iBizBusinessCentral-Product_replenish-Update')")
    @ApiOperation(value = "更新补料", tags = {"补料" },  notes = "更新补料")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_replenishes/{product_replenish_id}")
    public ResponseEntity<Product_replenishDTO> update(@PathVariable("product_replenish_id") Long product_replenish_id, @RequestBody Product_replenishDTO product_replenishdto) {
		Product_replenish domain  = product_replenishMapping.toDomain(product_replenishdto);
        domain .setId(product_replenish_id);
		product_replenishService.update(domain );
		Product_replenishDTO dto = product_replenishMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_replenishService.getProductReplenishByEntities(this.product_replenishMapping.toDomain(#product_replenishdtos)),'iBizBusinessCentral-Product_replenish-Update')")
    @ApiOperation(value = "批量更新补料", tags = {"补料" },  notes = "批量更新补料")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_replenishes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_replenishDTO> product_replenishdtos) {
        product_replenishService.updateBatch(product_replenishMapping.toDomain(product_replenishdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_replenishService.get(#product_replenish_id),'iBizBusinessCentral-Product_replenish-Remove')")
    @ApiOperation(value = "删除补料", tags = {"补料" },  notes = "删除补料")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_replenishes/{product_replenish_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_replenish_id") Long product_replenish_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_replenishService.remove(product_replenish_id));
    }

    @PreAuthorize("hasPermission(this.product_replenishService.getProductReplenishByIds(#ids),'iBizBusinessCentral-Product_replenish-Remove')")
    @ApiOperation(value = "批量删除补料", tags = {"补料" },  notes = "批量删除补料")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_replenishes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_replenishService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_replenishMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_replenish-Get')")
    @ApiOperation(value = "获取补料", tags = {"补料" },  notes = "获取补料")
	@RequestMapping(method = RequestMethod.GET, value = "/product_replenishes/{product_replenish_id}")
    public ResponseEntity<Product_replenishDTO> get(@PathVariable("product_replenish_id") Long product_replenish_id) {
        Product_replenish domain = product_replenishService.get(product_replenish_id);
        Product_replenishDTO dto = product_replenishMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取补料草稿", tags = {"补料" },  notes = "获取补料草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_replenishes/getdraft")
    public ResponseEntity<Product_replenishDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_replenishMapping.toDto(product_replenishService.getDraft(new Product_replenish())));
    }

    @ApiOperation(value = "检查补料", tags = {"补料" },  notes = "检查补料")
	@RequestMapping(method = RequestMethod.POST, value = "/product_replenishes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_replenishDTO product_replenishdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_replenishService.checkKey(product_replenishMapping.toDomain(product_replenishdto)));
    }

    @PreAuthorize("hasPermission(this.product_replenishMapping.toDomain(#product_replenishdto),'iBizBusinessCentral-Product_replenish-Save')")
    @ApiOperation(value = "保存补料", tags = {"补料" },  notes = "保存补料")
	@RequestMapping(method = RequestMethod.POST, value = "/product_replenishes/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_replenishDTO product_replenishdto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_replenishService.save(product_replenishMapping.toDomain(product_replenishdto)));
    }

    @PreAuthorize("hasPermission(this.product_replenishMapping.toDomain(#product_replenishdtos),'iBizBusinessCentral-Product_replenish-Save')")
    @ApiOperation(value = "批量保存补料", tags = {"补料" },  notes = "批量保存补料")
	@RequestMapping(method = RequestMethod.POST, value = "/product_replenishes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_replenishDTO> product_replenishdtos) {
        product_replenishService.saveBatch(product_replenishMapping.toDomain(product_replenishdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_replenish-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_replenish-Get')")
	@ApiOperation(value = "获取数据集", tags = {"补料" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_replenishes/fetchdefault")
	public ResponseEntity<List<Product_replenishDTO>> fetchDefault(Product_replenishSearchContext context) {
        Page<Product_replenish> domains = product_replenishService.searchDefault(context) ;
        List<Product_replenishDTO> list = product_replenishMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_replenish-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_replenish-Get')")
	@ApiOperation(value = "查询数据集", tags = {"补料" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_replenishes/searchdefault")
	public ResponseEntity<Page<Product_replenishDTO>> searchDefault(@RequestBody Product_replenishSearchContext context) {
        Page<Product_replenish> domains = product_replenishService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_replenishMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

