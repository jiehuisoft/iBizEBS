package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_seo_metadata;
import cn.ibizlab.businesscentral.core.odoo_website.service.IWebsite_seo_metadataService;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_seo_metadataSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"SEO元数据" })
@RestController("Core-website_seo_metadata")
@RequestMapping("")
public class Website_seo_metadataResource {

    @Autowired
    public IWebsite_seo_metadataService website_seo_metadataService;

    @Autowired
    @Lazy
    public Website_seo_metadataMapping website_seo_metadataMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_seo_metadata-Create-all')")
    @ApiOperation(value = "新建SEO元数据", tags = {"SEO元数据" },  notes = "新建SEO元数据")
	@RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata")
    public ResponseEntity<Website_seo_metadataDTO> create(@Validated @RequestBody Website_seo_metadataDTO website_seo_metadatadto) {
        Website_seo_metadata domain = website_seo_metadataMapping.toDomain(website_seo_metadatadto);
		website_seo_metadataService.create(domain);
        Website_seo_metadataDTO dto = website_seo_metadataMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_seo_metadata-Create-all')")
    @ApiOperation(value = "批量新建SEO元数据", tags = {"SEO元数据" },  notes = "批量新建SEO元数据")
	@RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Website_seo_metadataDTO> website_seo_metadatadtos) {
        website_seo_metadataService.createBatch(website_seo_metadataMapping.toDomain(website_seo_metadatadtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_seo_metadata-Update-all')")
    @ApiOperation(value = "更新SEO元数据", tags = {"SEO元数据" },  notes = "更新SEO元数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_seo_metadata/{website_seo_metadata_id}")
    public ResponseEntity<Website_seo_metadataDTO> update(@PathVariable("website_seo_metadata_id") Long website_seo_metadata_id, @RequestBody Website_seo_metadataDTO website_seo_metadatadto) {
		Website_seo_metadata domain  = website_seo_metadataMapping.toDomain(website_seo_metadatadto);
        domain .setId(website_seo_metadata_id);
		website_seo_metadataService.update(domain );
		Website_seo_metadataDTO dto = website_seo_metadataMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_seo_metadata-Update-all')")
    @ApiOperation(value = "批量更新SEO元数据", tags = {"SEO元数据" },  notes = "批量更新SEO元数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_seo_metadata/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_seo_metadataDTO> website_seo_metadatadtos) {
        website_seo_metadataService.updateBatch(website_seo_metadataMapping.toDomain(website_seo_metadatadtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_seo_metadata-Remove-all')")
    @ApiOperation(value = "删除SEO元数据", tags = {"SEO元数据" },  notes = "删除SEO元数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_seo_metadata/{website_seo_metadata_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("website_seo_metadata_id") Long website_seo_metadata_id) {
         return ResponseEntity.status(HttpStatus.OK).body(website_seo_metadataService.remove(website_seo_metadata_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_seo_metadata-Remove-all')")
    @ApiOperation(value = "批量删除SEO元数据", tags = {"SEO元数据" },  notes = "批量删除SEO元数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_seo_metadata/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        website_seo_metadataService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_seo_metadata-Get-all')")
    @ApiOperation(value = "获取SEO元数据", tags = {"SEO元数据" },  notes = "获取SEO元数据")
	@RequestMapping(method = RequestMethod.GET, value = "/website_seo_metadata/{website_seo_metadata_id}")
    public ResponseEntity<Website_seo_metadataDTO> get(@PathVariable("website_seo_metadata_id") Long website_seo_metadata_id) {
        Website_seo_metadata domain = website_seo_metadataService.get(website_seo_metadata_id);
        Website_seo_metadataDTO dto = website_seo_metadataMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取SEO元数据草稿", tags = {"SEO元数据" },  notes = "获取SEO元数据草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/website_seo_metadata/getdraft")
    public ResponseEntity<Website_seo_metadataDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(website_seo_metadataMapping.toDto(website_seo_metadataService.getDraft(new Website_seo_metadata())));
    }

    @ApiOperation(value = "检查SEO元数据", tags = {"SEO元数据" },  notes = "检查SEO元数据")
	@RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Website_seo_metadataDTO website_seo_metadatadto) {
        return  ResponseEntity.status(HttpStatus.OK).body(website_seo_metadataService.checkKey(website_seo_metadataMapping.toDomain(website_seo_metadatadto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_seo_metadata-Save-all')")
    @ApiOperation(value = "保存SEO元数据", tags = {"SEO元数据" },  notes = "保存SEO元数据")
	@RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata/save")
    public ResponseEntity<Boolean> save(@RequestBody Website_seo_metadataDTO website_seo_metadatadto) {
        return ResponseEntity.status(HttpStatus.OK).body(website_seo_metadataService.save(website_seo_metadataMapping.toDomain(website_seo_metadatadto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_seo_metadata-Save-all')")
    @ApiOperation(value = "批量保存SEO元数据", tags = {"SEO元数据" },  notes = "批量保存SEO元数据")
	@RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Website_seo_metadataDTO> website_seo_metadatadtos) {
        website_seo_metadataService.saveBatch(website_seo_metadataMapping.toDomain(website_seo_metadatadtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_seo_metadata-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"SEO元数据" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/website_seo_metadata/fetchdefault")
	public ResponseEntity<List<Website_seo_metadataDTO>> fetchDefault(Website_seo_metadataSearchContext context) {
        Page<Website_seo_metadata> domains = website_seo_metadataService.searchDefault(context) ;
        List<Website_seo_metadataDTO> list = website_seo_metadataMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_seo_metadata-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"SEO元数据" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/website_seo_metadata/searchdefault")
	public ResponseEntity<Page<Website_seo_metadataDTO>> searchDefault(@RequestBody Website_seo_metadataSearchContext context) {
        Page<Website_seo_metadata> domains = website_seo_metadataService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(website_seo_metadataMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

