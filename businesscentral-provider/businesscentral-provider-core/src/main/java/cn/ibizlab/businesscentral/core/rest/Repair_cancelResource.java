package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_cancel;
import cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_cancelService;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_cancelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"取消维修" })
@RestController("Core-repair_cancel")
@RequestMapping("")
public class Repair_cancelResource {

    @Autowired
    public IRepair_cancelService repair_cancelService;

    @Autowired
    @Lazy
    public Repair_cancelMapping repair_cancelMapping;

    @PreAuthorize("hasPermission(this.repair_cancelMapping.toDomain(#repair_canceldto),'iBizBusinessCentral-Repair_cancel-Create')")
    @ApiOperation(value = "新建取消维修", tags = {"取消维修" },  notes = "新建取消维修")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_cancels")
    public ResponseEntity<Repair_cancelDTO> create(@Validated @RequestBody Repair_cancelDTO repair_canceldto) {
        Repair_cancel domain = repair_cancelMapping.toDomain(repair_canceldto);
		repair_cancelService.create(domain);
        Repair_cancelDTO dto = repair_cancelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.repair_cancelMapping.toDomain(#repair_canceldtos),'iBizBusinessCentral-Repair_cancel-Create')")
    @ApiOperation(value = "批量新建取消维修", tags = {"取消维修" },  notes = "批量新建取消维修")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_cancels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Repair_cancelDTO> repair_canceldtos) {
        repair_cancelService.createBatch(repair_cancelMapping.toDomain(repair_canceldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "repair_cancel" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.repair_cancelService.get(#repair_cancel_id),'iBizBusinessCentral-Repair_cancel-Update')")
    @ApiOperation(value = "更新取消维修", tags = {"取消维修" },  notes = "更新取消维修")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_cancels/{repair_cancel_id}")
    public ResponseEntity<Repair_cancelDTO> update(@PathVariable("repair_cancel_id") Long repair_cancel_id, @RequestBody Repair_cancelDTO repair_canceldto) {
		Repair_cancel domain  = repair_cancelMapping.toDomain(repair_canceldto);
        domain .setId(repair_cancel_id);
		repair_cancelService.update(domain );
		Repair_cancelDTO dto = repair_cancelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.repair_cancelService.getRepairCancelByEntities(this.repair_cancelMapping.toDomain(#repair_canceldtos)),'iBizBusinessCentral-Repair_cancel-Update')")
    @ApiOperation(value = "批量更新取消维修", tags = {"取消维修" },  notes = "批量更新取消维修")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_cancels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_cancelDTO> repair_canceldtos) {
        repair_cancelService.updateBatch(repair_cancelMapping.toDomain(repair_canceldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.repair_cancelService.get(#repair_cancel_id),'iBizBusinessCentral-Repair_cancel-Remove')")
    @ApiOperation(value = "删除取消维修", tags = {"取消维修" },  notes = "删除取消维修")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_cancels/{repair_cancel_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("repair_cancel_id") Long repair_cancel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(repair_cancelService.remove(repair_cancel_id));
    }

    @PreAuthorize("hasPermission(this.repair_cancelService.getRepairCancelByIds(#ids),'iBizBusinessCentral-Repair_cancel-Remove')")
    @ApiOperation(value = "批量删除取消维修", tags = {"取消维修" },  notes = "批量删除取消维修")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_cancels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        repair_cancelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.repair_cancelMapping.toDomain(returnObject.body),'iBizBusinessCentral-Repair_cancel-Get')")
    @ApiOperation(value = "获取取消维修", tags = {"取消维修" },  notes = "获取取消维修")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_cancels/{repair_cancel_id}")
    public ResponseEntity<Repair_cancelDTO> get(@PathVariable("repair_cancel_id") Long repair_cancel_id) {
        Repair_cancel domain = repair_cancelService.get(repair_cancel_id);
        Repair_cancelDTO dto = repair_cancelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取取消维修草稿", tags = {"取消维修" },  notes = "获取取消维修草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_cancels/getdraft")
    public ResponseEntity<Repair_cancelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(repair_cancelMapping.toDto(repair_cancelService.getDraft(new Repair_cancel())));
    }

    @ApiOperation(value = "检查取消维修", tags = {"取消维修" },  notes = "检查取消维修")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_cancels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Repair_cancelDTO repair_canceldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(repair_cancelService.checkKey(repair_cancelMapping.toDomain(repair_canceldto)));
    }

    @PreAuthorize("hasPermission(this.repair_cancelMapping.toDomain(#repair_canceldto),'iBizBusinessCentral-Repair_cancel-Save')")
    @ApiOperation(value = "保存取消维修", tags = {"取消维修" },  notes = "保存取消维修")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_cancels/save")
    public ResponseEntity<Boolean> save(@RequestBody Repair_cancelDTO repair_canceldto) {
        return ResponseEntity.status(HttpStatus.OK).body(repair_cancelService.save(repair_cancelMapping.toDomain(repair_canceldto)));
    }

    @PreAuthorize("hasPermission(this.repair_cancelMapping.toDomain(#repair_canceldtos),'iBizBusinessCentral-Repair_cancel-Save')")
    @ApiOperation(value = "批量保存取消维修", tags = {"取消维修" },  notes = "批量保存取消维修")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_cancels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Repair_cancelDTO> repair_canceldtos) {
        repair_cancelService.saveBatch(repair_cancelMapping.toDomain(repair_canceldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Repair_cancel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Repair_cancel-Get')")
	@ApiOperation(value = "获取数据集", tags = {"取消维修" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/repair_cancels/fetchdefault")
	public ResponseEntity<List<Repair_cancelDTO>> fetchDefault(Repair_cancelSearchContext context) {
        Page<Repair_cancel> domains = repair_cancelService.searchDefault(context) ;
        List<Repair_cancelDTO> list = repair_cancelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Repair_cancel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Repair_cancel-Get')")
	@ApiOperation(value = "查询数据集", tags = {"取消维修" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/repair_cancels/searchdefault")
	public ResponseEntity<Page<Repair_cancelDTO>> searchDefault(@RequestBody Repair_cancelSearchContext context) {
        Page<Repair_cancel> domains = repair_cancelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(repair_cancelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

