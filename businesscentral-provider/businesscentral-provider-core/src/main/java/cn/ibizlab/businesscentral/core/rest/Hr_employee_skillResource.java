package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee_skill;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employee_skillService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_employee_skillSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"员工技能" })
@RestController("Core-hr_employee_skill")
@RequestMapping("")
public class Hr_employee_skillResource {

    @Autowired
    public IHr_employee_skillService hr_employee_skillService;

    @Autowired
    @Lazy
    public Hr_employee_skillMapping hr_employee_skillMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Create-all')")
    @ApiOperation(value = "新建员工技能", tags = {"员工技能" },  notes = "新建员工技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employee_skills")
    public ResponseEntity<Hr_employee_skillDTO> create(@Validated @RequestBody Hr_employee_skillDTO hr_employee_skilldto) {
        Hr_employee_skill domain = hr_employee_skillMapping.toDomain(hr_employee_skilldto);
		hr_employee_skillService.create(domain);
        Hr_employee_skillDTO dto = hr_employee_skillMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Create-all')")
    @ApiOperation(value = "批量新建员工技能", tags = {"员工技能" },  notes = "批量新建员工技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employee_skills/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_employee_skillDTO> hr_employee_skilldtos) {
        hr_employee_skillService.createBatch(hr_employee_skillMapping.toDomain(hr_employee_skilldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Update-all')")
    @ApiOperation(value = "更新员工技能", tags = {"员工技能" },  notes = "更新员工技能")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employee_skills/{hr_employee_skill_id}")
    public ResponseEntity<Hr_employee_skillDTO> update(@PathVariable("hr_employee_skill_id") Long hr_employee_skill_id, @RequestBody Hr_employee_skillDTO hr_employee_skilldto) {
		Hr_employee_skill domain  = hr_employee_skillMapping.toDomain(hr_employee_skilldto);
        domain .setId(hr_employee_skill_id);
		hr_employee_skillService.update(domain );
		Hr_employee_skillDTO dto = hr_employee_skillMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Update-all')")
    @ApiOperation(value = "批量更新员工技能", tags = {"员工技能" },  notes = "批量更新员工技能")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employee_skills/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_employee_skillDTO> hr_employee_skilldtos) {
        hr_employee_skillService.updateBatch(hr_employee_skillMapping.toDomain(hr_employee_skilldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Remove-all')")
    @ApiOperation(value = "删除员工技能", tags = {"员工技能" },  notes = "删除员工技能")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employee_skills/{hr_employee_skill_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_employee_skill_id") Long hr_employee_skill_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_employee_skillService.remove(hr_employee_skill_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Remove-all')")
    @ApiOperation(value = "批量删除员工技能", tags = {"员工技能" },  notes = "批量删除员工技能")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employee_skills/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_employee_skillService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Get-all')")
    @ApiOperation(value = "获取员工技能", tags = {"员工技能" },  notes = "获取员工技能")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_employee_skills/{hr_employee_skill_id}")
    public ResponseEntity<Hr_employee_skillDTO> get(@PathVariable("hr_employee_skill_id") Long hr_employee_skill_id) {
        Hr_employee_skill domain = hr_employee_skillService.get(hr_employee_skill_id);
        Hr_employee_skillDTO dto = hr_employee_skillMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取员工技能草稿", tags = {"员工技能" },  notes = "获取员工技能草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_employee_skills/getdraft")
    public ResponseEntity<Hr_employee_skillDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_employee_skillMapping.toDto(hr_employee_skillService.getDraft(new Hr_employee_skill())));
    }

    @ApiOperation(value = "检查员工技能", tags = {"员工技能" },  notes = "检查员工技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employee_skills/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_employee_skillDTO hr_employee_skilldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_employee_skillService.checkKey(hr_employee_skillMapping.toDomain(hr_employee_skilldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Save-all')")
    @ApiOperation(value = "保存员工技能", tags = {"员工技能" },  notes = "保存员工技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employee_skills/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_employee_skillDTO hr_employee_skilldto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_employee_skillService.save(hr_employee_skillMapping.toDomain(hr_employee_skilldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Save-all')")
    @ApiOperation(value = "批量保存员工技能", tags = {"员工技能" },  notes = "批量保存员工技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employee_skills/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_employee_skillDTO> hr_employee_skilldtos) {
        hr_employee_skillService.saveBatch(hr_employee_skillMapping.toDomain(hr_employee_skilldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"员工技能" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_employee_skills/fetchdefault")
	public ResponseEntity<List<Hr_employee_skillDTO>> fetchDefault(Hr_employee_skillSearchContext context) {
        Page<Hr_employee_skill> domains = hr_employee_skillService.searchDefault(context) ;
        List<Hr_employee_skillDTO> list = hr_employee_skillMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"员工技能" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_employee_skills/searchdefault")
	public ResponseEntity<Page<Hr_employee_skillDTO>> searchDefault(@RequestBody Hr_employee_skillSearchContext context) {
        Page<Hr_employee_skill> domains = hr_employee_skillService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_employee_skillMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Create-all')")
    @ApiOperation(value = "根据员工建立员工技能", tags = {"员工技能" },  notes = "根据员工建立员工技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_employee_skills")
    public ResponseEntity<Hr_employee_skillDTO> createByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_employee_skillDTO hr_employee_skilldto) {
        Hr_employee_skill domain = hr_employee_skillMapping.toDomain(hr_employee_skilldto);
        domain.setEmployeeId(hr_employee_id);
		hr_employee_skillService.create(domain);
        Hr_employee_skillDTO dto = hr_employee_skillMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Create-all')")
    @ApiOperation(value = "根据员工批量建立员工技能", tags = {"员工技能" },  notes = "根据员工批量建立员工技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_employee_skills/batch")
    public ResponseEntity<Boolean> createBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Hr_employee_skillDTO> hr_employee_skilldtos) {
        List<Hr_employee_skill> domainlist=hr_employee_skillMapping.toDomain(hr_employee_skilldtos);
        for(Hr_employee_skill domain:domainlist){
            domain.setEmployeeId(hr_employee_id);
        }
        hr_employee_skillService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Update-all')")
    @ApiOperation(value = "根据员工更新员工技能", tags = {"员工技能" },  notes = "根据员工更新员工技能")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/{hr_employee_id}/hr_employee_skills/{hr_employee_skill_id}")
    public ResponseEntity<Hr_employee_skillDTO> updateByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("hr_employee_skill_id") Long hr_employee_skill_id, @RequestBody Hr_employee_skillDTO hr_employee_skilldto) {
        Hr_employee_skill domain = hr_employee_skillMapping.toDomain(hr_employee_skilldto);
        domain.setEmployeeId(hr_employee_id);
        domain.setId(hr_employee_skill_id);
		hr_employee_skillService.update(domain);
        Hr_employee_skillDTO dto = hr_employee_skillMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Update-all')")
    @ApiOperation(value = "根据员工批量更新员工技能", tags = {"员工技能" },  notes = "根据员工批量更新员工技能")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/{hr_employee_id}/hr_employee_skills/batch")
    public ResponseEntity<Boolean> updateBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Hr_employee_skillDTO> hr_employee_skilldtos) {
        List<Hr_employee_skill> domainlist=hr_employee_skillMapping.toDomain(hr_employee_skilldtos);
        for(Hr_employee_skill domain:domainlist){
            domain.setEmployeeId(hr_employee_id);
        }
        hr_employee_skillService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Remove-all')")
    @ApiOperation(value = "根据员工删除员工技能", tags = {"员工技能" },  notes = "根据员工删除员工技能")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/{hr_employee_id}/hr_employee_skills/{hr_employee_skill_id}")
    public ResponseEntity<Boolean> removeByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("hr_employee_skill_id") Long hr_employee_skill_id) {
		return ResponseEntity.status(HttpStatus.OK).body(hr_employee_skillService.remove(hr_employee_skill_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Remove-all')")
    @ApiOperation(value = "根据员工批量删除员工技能", tags = {"员工技能" },  notes = "根据员工批量删除员工技能")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/{hr_employee_id}/hr_employee_skills/batch")
    public ResponseEntity<Boolean> removeBatchByHr_employee(@RequestBody List<Long> ids) {
        hr_employee_skillService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Get-all')")
    @ApiOperation(value = "根据员工获取员工技能", tags = {"员工技能" },  notes = "根据员工获取员工技能")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_employees/{hr_employee_id}/hr_employee_skills/{hr_employee_skill_id}")
    public ResponseEntity<Hr_employee_skillDTO> getByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("hr_employee_skill_id") Long hr_employee_skill_id) {
        Hr_employee_skill domain = hr_employee_skillService.get(hr_employee_skill_id);
        Hr_employee_skillDTO dto = hr_employee_skillMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据员工获取员工技能草稿", tags = {"员工技能" },  notes = "根据员工获取员工技能草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/hr_employees/{hr_employee_id}/hr_employee_skills/getdraft")
    public ResponseEntity<Hr_employee_skillDTO> getDraftByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id) {
        Hr_employee_skill domain = new Hr_employee_skill();
        domain.setEmployeeId(hr_employee_id);
        return ResponseEntity.status(HttpStatus.OK).body(hr_employee_skillMapping.toDto(hr_employee_skillService.getDraft(domain)));
    }

    @ApiOperation(value = "根据员工检查员工技能", tags = {"员工技能" },  notes = "根据员工检查员工技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_employee_skills/checkkey")
    public ResponseEntity<Boolean> checkKeyByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_employee_skillDTO hr_employee_skilldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_employee_skillService.checkKey(hr_employee_skillMapping.toDomain(hr_employee_skilldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Save-all')")
    @ApiOperation(value = "根据员工保存员工技能", tags = {"员工技能" },  notes = "根据员工保存员工技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_employee_skills/save")
    public ResponseEntity<Boolean> saveByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_employee_skillDTO hr_employee_skilldto) {
        Hr_employee_skill domain = hr_employee_skillMapping.toDomain(hr_employee_skilldto);
        domain.setEmployeeId(hr_employee_id);
        return ResponseEntity.status(HttpStatus.OK).body(hr_employee_skillService.save(domain));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-Save-all')")
    @ApiOperation(value = "根据员工批量保存员工技能", tags = {"员工技能" },  notes = "根据员工批量保存员工技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_employee_skills/savebatch")
    public ResponseEntity<Boolean> saveBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Hr_employee_skillDTO> hr_employee_skilldtos) {
        List<Hr_employee_skill> domainlist=hr_employee_skillMapping.toDomain(hr_employee_skilldtos);
        for(Hr_employee_skill domain:domainlist){
             domain.setEmployeeId(hr_employee_id);
        }
        hr_employee_skillService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-searchDefault-all')")
	@ApiOperation(value = "根据员工获取数据集", tags = {"员工技能" } ,notes = "根据员工获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_employees/{hr_employee_id}/hr_employee_skills/fetchdefault")
	public ResponseEntity<List<Hr_employee_skillDTO>> fetchHr_employee_skillDefaultByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id,Hr_employee_skillSearchContext context) {
        context.setN_employee_id_eq(hr_employee_id);
        Page<Hr_employee_skill> domains = hr_employee_skillService.searchDefault(context) ;
        List<Hr_employee_skillDTO> list = hr_employee_skillMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee_skill-searchDefault-all')")
	@ApiOperation(value = "根据员工查询数据集", tags = {"员工技能" } ,notes = "根据员工查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_employees/{hr_employee_id}/hr_employee_skills/searchdefault")
	public ResponseEntity<Page<Hr_employee_skillDTO>> searchHr_employee_skillDefaultByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_employee_skillSearchContext context) {
        context.setN_employee_id_eq(hr_employee_id);
        Page<Hr_employee_skill> domains = hr_employee_skillService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_employee_skillMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

