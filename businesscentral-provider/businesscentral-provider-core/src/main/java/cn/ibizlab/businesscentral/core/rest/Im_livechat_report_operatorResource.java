package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_report_operator;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_report_operatorService;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.filter.Im_livechat_report_operatorSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"实时聊天支持操作员报告" })
@RestController("Core-im_livechat_report_operator")
@RequestMapping("")
public class Im_livechat_report_operatorResource {

    @Autowired
    public IIm_livechat_report_operatorService im_livechat_report_operatorService;

    @Autowired
    @Lazy
    public Im_livechat_report_operatorMapping im_livechat_report_operatorMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_operator-Create-all')")
    @ApiOperation(value = "新建实时聊天支持操作员报告", tags = {"实时聊天支持操作员报告" },  notes = "新建实时聊天支持操作员报告")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators")
    public ResponseEntity<Im_livechat_report_operatorDTO> create(@Validated @RequestBody Im_livechat_report_operatorDTO im_livechat_report_operatordto) {
        Im_livechat_report_operator domain = im_livechat_report_operatorMapping.toDomain(im_livechat_report_operatordto);
		im_livechat_report_operatorService.create(domain);
        Im_livechat_report_operatorDTO dto = im_livechat_report_operatorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_operator-Create-all')")
    @ApiOperation(value = "批量新建实时聊天支持操作员报告", tags = {"实时聊天支持操作员报告" },  notes = "批量新建实时聊天支持操作员报告")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Im_livechat_report_operatorDTO> im_livechat_report_operatordtos) {
        im_livechat_report_operatorService.createBatch(im_livechat_report_operatorMapping.toDomain(im_livechat_report_operatordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_operator-Update-all')")
    @ApiOperation(value = "更新实时聊天支持操作员报告", tags = {"实时聊天支持操作员报告" },  notes = "更新实时聊天支持操作员报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_report_operators/{im_livechat_report_operator_id}")
    public ResponseEntity<Im_livechat_report_operatorDTO> update(@PathVariable("im_livechat_report_operator_id") Long im_livechat_report_operator_id, @RequestBody Im_livechat_report_operatorDTO im_livechat_report_operatordto) {
		Im_livechat_report_operator domain  = im_livechat_report_operatorMapping.toDomain(im_livechat_report_operatordto);
        domain .setId(im_livechat_report_operator_id);
		im_livechat_report_operatorService.update(domain );
		Im_livechat_report_operatorDTO dto = im_livechat_report_operatorMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_operator-Update-all')")
    @ApiOperation(value = "批量更新实时聊天支持操作员报告", tags = {"实时聊天支持操作员报告" },  notes = "批量更新实时聊天支持操作员报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_report_operators/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Im_livechat_report_operatorDTO> im_livechat_report_operatordtos) {
        im_livechat_report_operatorService.updateBatch(im_livechat_report_operatorMapping.toDomain(im_livechat_report_operatordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_operator-Remove-all')")
    @ApiOperation(value = "删除实时聊天支持操作员报告", tags = {"实时聊天支持操作员报告" },  notes = "删除实时聊天支持操作员报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_report_operators/{im_livechat_report_operator_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("im_livechat_report_operator_id") Long im_livechat_report_operator_id) {
         return ResponseEntity.status(HttpStatus.OK).body(im_livechat_report_operatorService.remove(im_livechat_report_operator_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_operator-Remove-all')")
    @ApiOperation(value = "批量删除实时聊天支持操作员报告", tags = {"实时聊天支持操作员报告" },  notes = "批量删除实时聊天支持操作员报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_report_operators/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        im_livechat_report_operatorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_operator-Get-all')")
    @ApiOperation(value = "获取实时聊天支持操作员报告", tags = {"实时聊天支持操作员报告" },  notes = "获取实时聊天支持操作员报告")
	@RequestMapping(method = RequestMethod.GET, value = "/im_livechat_report_operators/{im_livechat_report_operator_id}")
    public ResponseEntity<Im_livechat_report_operatorDTO> get(@PathVariable("im_livechat_report_operator_id") Long im_livechat_report_operator_id) {
        Im_livechat_report_operator domain = im_livechat_report_operatorService.get(im_livechat_report_operator_id);
        Im_livechat_report_operatorDTO dto = im_livechat_report_operatorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取实时聊天支持操作员报告草稿", tags = {"实时聊天支持操作员报告" },  notes = "获取实时聊天支持操作员报告草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/im_livechat_report_operators/getdraft")
    public ResponseEntity<Im_livechat_report_operatorDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(im_livechat_report_operatorMapping.toDto(im_livechat_report_operatorService.getDraft(new Im_livechat_report_operator())));
    }

    @ApiOperation(value = "检查实时聊天支持操作员报告", tags = {"实时聊天支持操作员报告" },  notes = "检查实时聊天支持操作员报告")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Im_livechat_report_operatorDTO im_livechat_report_operatordto) {
        return  ResponseEntity.status(HttpStatus.OK).body(im_livechat_report_operatorService.checkKey(im_livechat_report_operatorMapping.toDomain(im_livechat_report_operatordto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_operator-Save-all')")
    @ApiOperation(value = "保存实时聊天支持操作员报告", tags = {"实时聊天支持操作员报告" },  notes = "保存实时聊天支持操作员报告")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators/save")
    public ResponseEntity<Boolean> save(@RequestBody Im_livechat_report_operatorDTO im_livechat_report_operatordto) {
        return ResponseEntity.status(HttpStatus.OK).body(im_livechat_report_operatorService.save(im_livechat_report_operatorMapping.toDomain(im_livechat_report_operatordto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_operator-Save-all')")
    @ApiOperation(value = "批量保存实时聊天支持操作员报告", tags = {"实时聊天支持操作员报告" },  notes = "批量保存实时聊天支持操作员报告")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Im_livechat_report_operatorDTO> im_livechat_report_operatordtos) {
        im_livechat_report_operatorService.saveBatch(im_livechat_report_operatorMapping.toDomain(im_livechat_report_operatordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_operator-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"实时聊天支持操作员报告" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/im_livechat_report_operators/fetchdefault")
	public ResponseEntity<List<Im_livechat_report_operatorDTO>> fetchDefault(Im_livechat_report_operatorSearchContext context) {
        Page<Im_livechat_report_operator> domains = im_livechat_report_operatorService.searchDefault(context) ;
        List<Im_livechat_report_operatorDTO> list = im_livechat_report_operatorMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_report_operator-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"实时聊天支持操作员报告" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/im_livechat_report_operators/searchdefault")
	public ResponseEntity<Page<Im_livechat_report_operatorDTO>> searchDefault(@RequestBody Im_livechat_report_operatorSearchContext context) {
        Page<Im_livechat_report_operator> domains = im_livechat_report_operatorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(im_livechat_report_operatorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

