package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_register_payments;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_register_paymentsService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_register_paymentsSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"登记付款" })
@RestController("Core-account_register_payments")
@RequestMapping("")
public class Account_register_paymentsResource {

    @Autowired
    public IAccount_register_paymentsService account_register_paymentsService;

    @Autowired
    @Lazy
    public Account_register_paymentsMapping account_register_paymentsMapping;

    @PreAuthorize("hasPermission(this.account_register_paymentsMapping.toDomain(#account_register_paymentsdto),'iBizBusinessCentral-Account_register_payments-Create')")
    @ApiOperation(value = "新建登记付款", tags = {"登记付款" },  notes = "新建登记付款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_register_payments")
    public ResponseEntity<Account_register_paymentsDTO> create(@Validated @RequestBody Account_register_paymentsDTO account_register_paymentsdto) {
        Account_register_payments domain = account_register_paymentsMapping.toDomain(account_register_paymentsdto);
		account_register_paymentsService.create(domain);
        Account_register_paymentsDTO dto = account_register_paymentsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_register_paymentsMapping.toDomain(#account_register_paymentsdtos),'iBizBusinessCentral-Account_register_payments-Create')")
    @ApiOperation(value = "批量新建登记付款", tags = {"登记付款" },  notes = "批量新建登记付款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_register_payments/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_register_paymentsDTO> account_register_paymentsdtos) {
        account_register_paymentsService.createBatch(account_register_paymentsMapping.toDomain(account_register_paymentsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_register_payments" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_register_paymentsService.get(#account_register_payments_id),'iBizBusinessCentral-Account_register_payments-Update')")
    @ApiOperation(value = "更新登记付款", tags = {"登记付款" },  notes = "更新登记付款")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_register_payments/{account_register_payments_id}")
    public ResponseEntity<Account_register_paymentsDTO> update(@PathVariable("account_register_payments_id") Long account_register_payments_id, @RequestBody Account_register_paymentsDTO account_register_paymentsdto) {
		Account_register_payments domain  = account_register_paymentsMapping.toDomain(account_register_paymentsdto);
        domain .setId(account_register_payments_id);
		account_register_paymentsService.update(domain );
		Account_register_paymentsDTO dto = account_register_paymentsMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_register_paymentsService.getAccountRegisterPaymentsByEntities(this.account_register_paymentsMapping.toDomain(#account_register_paymentsdtos)),'iBizBusinessCentral-Account_register_payments-Update')")
    @ApiOperation(value = "批量更新登记付款", tags = {"登记付款" },  notes = "批量更新登记付款")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_register_payments/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_register_paymentsDTO> account_register_paymentsdtos) {
        account_register_paymentsService.updateBatch(account_register_paymentsMapping.toDomain(account_register_paymentsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_register_paymentsService.get(#account_register_payments_id),'iBizBusinessCentral-Account_register_payments-Remove')")
    @ApiOperation(value = "删除登记付款", tags = {"登记付款" },  notes = "删除登记付款")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_register_payments/{account_register_payments_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_register_payments_id") Long account_register_payments_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_register_paymentsService.remove(account_register_payments_id));
    }

    @PreAuthorize("hasPermission(this.account_register_paymentsService.getAccountRegisterPaymentsByIds(#ids),'iBizBusinessCentral-Account_register_payments-Remove')")
    @ApiOperation(value = "批量删除登记付款", tags = {"登记付款" },  notes = "批量删除登记付款")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_register_payments/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_register_paymentsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_register_paymentsMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_register_payments-Get')")
    @ApiOperation(value = "获取登记付款", tags = {"登记付款" },  notes = "获取登记付款")
	@RequestMapping(method = RequestMethod.GET, value = "/account_register_payments/{account_register_payments_id}")
    public ResponseEntity<Account_register_paymentsDTO> get(@PathVariable("account_register_payments_id") Long account_register_payments_id) {
        Account_register_payments domain = account_register_paymentsService.get(account_register_payments_id);
        Account_register_paymentsDTO dto = account_register_paymentsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取登记付款草稿", tags = {"登记付款" },  notes = "获取登记付款草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_register_payments/getdraft")
    public ResponseEntity<Account_register_paymentsDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_register_paymentsMapping.toDto(account_register_paymentsService.getDraft(new Account_register_payments())));
    }

    @ApiOperation(value = "检查登记付款", tags = {"登记付款" },  notes = "检查登记付款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_register_payments/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_register_paymentsDTO account_register_paymentsdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_register_paymentsService.checkKey(account_register_paymentsMapping.toDomain(account_register_paymentsdto)));
    }

    @PreAuthorize("hasPermission(this.account_register_paymentsMapping.toDomain(#account_register_paymentsdto),'iBizBusinessCentral-Account_register_payments-Save')")
    @ApiOperation(value = "保存登记付款", tags = {"登记付款" },  notes = "保存登记付款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_register_payments/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_register_paymentsDTO account_register_paymentsdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_register_paymentsService.save(account_register_paymentsMapping.toDomain(account_register_paymentsdto)));
    }

    @PreAuthorize("hasPermission(this.account_register_paymentsMapping.toDomain(#account_register_paymentsdtos),'iBizBusinessCentral-Account_register_payments-Save')")
    @ApiOperation(value = "批量保存登记付款", tags = {"登记付款" },  notes = "批量保存登记付款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_register_payments/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_register_paymentsDTO> account_register_paymentsdtos) {
        account_register_paymentsService.saveBatch(account_register_paymentsMapping.toDomain(account_register_paymentsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_register_payments-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_register_payments-Get')")
	@ApiOperation(value = "获取数据集", tags = {"登记付款" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_register_payments/fetchdefault")
	public ResponseEntity<List<Account_register_paymentsDTO>> fetchDefault(Account_register_paymentsSearchContext context) {
        Page<Account_register_payments> domains = account_register_paymentsService.searchDefault(context) ;
        List<Account_register_paymentsDTO> list = account_register_paymentsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_register_payments-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_register_payments-Get')")
	@ApiOperation(value = "查询数据集", tags = {"登记付款" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_register_payments/searchdefault")
	public ResponseEntity<Page<Account_register_paymentsDTO>> searchDefault(@RequestBody Account_register_paymentsSearchContext context) {
        Page<Account_register_payments> domains = account_register_paymentsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_register_paymentsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

