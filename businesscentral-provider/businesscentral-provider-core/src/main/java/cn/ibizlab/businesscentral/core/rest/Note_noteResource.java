package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_note.domain.Note_note;
import cn.ibizlab.businesscentral.core.odoo_note.service.INote_noteService;
import cn.ibizlab.businesscentral.core.odoo_note.filter.Note_noteSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"便签" })
@RestController("Core-note_note")
@RequestMapping("")
public class Note_noteResource {

    @Autowired
    public INote_noteService note_noteService;

    @Autowired
    @Lazy
    public Note_noteMapping note_noteMapping;

    @PreAuthorize("hasPermission(this.note_noteMapping.toDomain(#note_notedto),'iBizBusinessCentral-Note_note-Create')")
    @ApiOperation(value = "新建便签", tags = {"便签" },  notes = "新建便签")
	@RequestMapping(method = RequestMethod.POST, value = "/note_notes")
    public ResponseEntity<Note_noteDTO> create(@Validated @RequestBody Note_noteDTO note_notedto) {
        Note_note domain = note_noteMapping.toDomain(note_notedto);
		note_noteService.create(domain);
        Note_noteDTO dto = note_noteMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.note_noteMapping.toDomain(#note_notedtos),'iBizBusinessCentral-Note_note-Create')")
    @ApiOperation(value = "批量新建便签", tags = {"便签" },  notes = "批量新建便签")
	@RequestMapping(method = RequestMethod.POST, value = "/note_notes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Note_noteDTO> note_notedtos) {
        note_noteService.createBatch(note_noteMapping.toDomain(note_notedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "note_note" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.note_noteService.get(#note_note_id),'iBizBusinessCentral-Note_note-Update')")
    @ApiOperation(value = "更新便签", tags = {"便签" },  notes = "更新便签")
	@RequestMapping(method = RequestMethod.PUT, value = "/note_notes/{note_note_id}")
    public ResponseEntity<Note_noteDTO> update(@PathVariable("note_note_id") Long note_note_id, @RequestBody Note_noteDTO note_notedto) {
		Note_note domain  = note_noteMapping.toDomain(note_notedto);
        domain .setId(note_note_id);
		note_noteService.update(domain );
		Note_noteDTO dto = note_noteMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.note_noteService.getNoteNoteByEntities(this.note_noteMapping.toDomain(#note_notedtos)),'iBizBusinessCentral-Note_note-Update')")
    @ApiOperation(value = "批量更新便签", tags = {"便签" },  notes = "批量更新便签")
	@RequestMapping(method = RequestMethod.PUT, value = "/note_notes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Note_noteDTO> note_notedtos) {
        note_noteService.updateBatch(note_noteMapping.toDomain(note_notedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.note_noteService.get(#note_note_id),'iBizBusinessCentral-Note_note-Remove')")
    @ApiOperation(value = "删除便签", tags = {"便签" },  notes = "删除便签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/note_notes/{note_note_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("note_note_id") Long note_note_id) {
         return ResponseEntity.status(HttpStatus.OK).body(note_noteService.remove(note_note_id));
    }

    @PreAuthorize("hasPermission(this.note_noteService.getNoteNoteByIds(#ids),'iBizBusinessCentral-Note_note-Remove')")
    @ApiOperation(value = "批量删除便签", tags = {"便签" },  notes = "批量删除便签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/note_notes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        note_noteService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.note_noteMapping.toDomain(returnObject.body),'iBizBusinessCentral-Note_note-Get')")
    @ApiOperation(value = "获取便签", tags = {"便签" },  notes = "获取便签")
	@RequestMapping(method = RequestMethod.GET, value = "/note_notes/{note_note_id}")
    public ResponseEntity<Note_noteDTO> get(@PathVariable("note_note_id") Long note_note_id) {
        Note_note domain = note_noteService.get(note_note_id);
        Note_noteDTO dto = note_noteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取便签草稿", tags = {"便签" },  notes = "获取便签草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/note_notes/getdraft")
    public ResponseEntity<Note_noteDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(note_noteMapping.toDto(note_noteService.getDraft(new Note_note())));
    }

    @ApiOperation(value = "检查便签", tags = {"便签" },  notes = "检查便签")
	@RequestMapping(method = RequestMethod.POST, value = "/note_notes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Note_noteDTO note_notedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(note_noteService.checkKey(note_noteMapping.toDomain(note_notedto)));
    }

    @PreAuthorize("hasPermission(this.note_noteMapping.toDomain(#note_notedto),'iBizBusinessCentral-Note_note-Save')")
    @ApiOperation(value = "保存便签", tags = {"便签" },  notes = "保存便签")
	@RequestMapping(method = RequestMethod.POST, value = "/note_notes/save")
    public ResponseEntity<Boolean> save(@RequestBody Note_noteDTO note_notedto) {
        return ResponseEntity.status(HttpStatus.OK).body(note_noteService.save(note_noteMapping.toDomain(note_notedto)));
    }

    @PreAuthorize("hasPermission(this.note_noteMapping.toDomain(#note_notedtos),'iBizBusinessCentral-Note_note-Save')")
    @ApiOperation(value = "批量保存便签", tags = {"便签" },  notes = "批量保存便签")
	@RequestMapping(method = RequestMethod.POST, value = "/note_notes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Note_noteDTO> note_notedtos) {
        note_noteService.saveBatch(note_noteMapping.toDomain(note_notedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Note_note-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Note_note-Get')")
	@ApiOperation(value = "获取数据集", tags = {"便签" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/note_notes/fetchdefault")
	public ResponseEntity<List<Note_noteDTO>> fetchDefault(Note_noteSearchContext context) {
        Page<Note_note> domains = note_noteService.searchDefault(context) ;
        List<Note_noteDTO> list = note_noteMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Note_note-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Note_note-Get')")
	@ApiOperation(value = "查询数据集", tags = {"便签" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/note_notes/searchdefault")
	public ResponseEntity<Page<Note_noteDTO>> searchDefault(@RequestBody Note_noteSearchContext context) {
        Page<Note_note> domains = note_noteService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(note_noteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

