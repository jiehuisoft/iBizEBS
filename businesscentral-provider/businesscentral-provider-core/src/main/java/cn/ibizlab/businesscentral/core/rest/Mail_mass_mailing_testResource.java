package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_test;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_testService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_testSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"示例邮件向导" })
@RestController("Core-mail_mass_mailing_test")
@RequestMapping("")
public class Mail_mass_mailing_testResource {

    @Autowired
    public IMail_mass_mailing_testService mail_mass_mailing_testService;

    @Autowired
    @Lazy
    public Mail_mass_mailing_testMapping mail_mass_mailing_testMapping;

    @PreAuthorize("hasPermission(this.mail_mass_mailing_testMapping.toDomain(#mail_mass_mailing_testdto),'iBizBusinessCentral-Mail_mass_mailing_test-Create')")
    @ApiOperation(value = "新建示例邮件向导", tags = {"示例邮件向导" },  notes = "新建示例邮件向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tests")
    public ResponseEntity<Mail_mass_mailing_testDTO> create(@Validated @RequestBody Mail_mass_mailing_testDTO mail_mass_mailing_testdto) {
        Mail_mass_mailing_test domain = mail_mass_mailing_testMapping.toDomain(mail_mass_mailing_testdto);
		mail_mass_mailing_testService.create(domain);
        Mail_mass_mailing_testDTO dto = mail_mass_mailing_testMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_testMapping.toDomain(#mail_mass_mailing_testdtos),'iBizBusinessCentral-Mail_mass_mailing_test-Create')")
    @ApiOperation(value = "批量新建示例邮件向导", tags = {"示例邮件向导" },  notes = "批量新建示例邮件向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailing_testDTO> mail_mass_mailing_testdtos) {
        mail_mass_mailing_testService.createBatch(mail_mass_mailing_testMapping.toDomain(mail_mass_mailing_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_mass_mailing_test" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_mass_mailing_testService.get(#mail_mass_mailing_test_id),'iBizBusinessCentral-Mail_mass_mailing_test-Update')")
    @ApiOperation(value = "更新示例邮件向导", tags = {"示例邮件向导" },  notes = "更新示例邮件向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_tests/{mail_mass_mailing_test_id}")
    public ResponseEntity<Mail_mass_mailing_testDTO> update(@PathVariable("mail_mass_mailing_test_id") Long mail_mass_mailing_test_id, @RequestBody Mail_mass_mailing_testDTO mail_mass_mailing_testdto) {
		Mail_mass_mailing_test domain  = mail_mass_mailing_testMapping.toDomain(mail_mass_mailing_testdto);
        domain .setId(mail_mass_mailing_test_id);
		mail_mass_mailing_testService.update(domain );
		Mail_mass_mailing_testDTO dto = mail_mass_mailing_testMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_testService.getMailMassMailingTestByEntities(this.mail_mass_mailing_testMapping.toDomain(#mail_mass_mailing_testdtos)),'iBizBusinessCentral-Mail_mass_mailing_test-Update')")
    @ApiOperation(value = "批量更新示例邮件向导", tags = {"示例邮件向导" },  notes = "批量更新示例邮件向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_tests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_testDTO> mail_mass_mailing_testdtos) {
        mail_mass_mailing_testService.updateBatch(mail_mass_mailing_testMapping.toDomain(mail_mass_mailing_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_testService.get(#mail_mass_mailing_test_id),'iBizBusinessCentral-Mail_mass_mailing_test-Remove')")
    @ApiOperation(value = "删除示例邮件向导", tags = {"示例邮件向导" },  notes = "删除示例邮件向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_tests/{mail_mass_mailing_test_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_test_id") Long mail_mass_mailing_test_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_testService.remove(mail_mass_mailing_test_id));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_testService.getMailMassMailingTestByIds(#ids),'iBizBusinessCentral-Mail_mass_mailing_test-Remove')")
    @ApiOperation(value = "批量删除示例邮件向导", tags = {"示例邮件向导" },  notes = "批量删除示例邮件向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_tests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_mass_mailing_testService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_mass_mailing_testMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_mass_mailing_test-Get')")
    @ApiOperation(value = "获取示例邮件向导", tags = {"示例邮件向导" },  notes = "获取示例邮件向导")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_tests/{mail_mass_mailing_test_id}")
    public ResponseEntity<Mail_mass_mailing_testDTO> get(@PathVariable("mail_mass_mailing_test_id") Long mail_mass_mailing_test_id) {
        Mail_mass_mailing_test domain = mail_mass_mailing_testService.get(mail_mass_mailing_test_id);
        Mail_mass_mailing_testDTO dto = mail_mass_mailing_testMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取示例邮件向导草稿", tags = {"示例邮件向导" },  notes = "获取示例邮件向导草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_tests/getdraft")
    public ResponseEntity<Mail_mass_mailing_testDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_testMapping.toDto(mail_mass_mailing_testService.getDraft(new Mail_mass_mailing_test())));
    }

    @ApiOperation(value = "检查示例邮件向导", tags = {"示例邮件向导" },  notes = "检查示例邮件向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tests/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_mass_mailing_testDTO mail_mass_mailing_testdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_testService.checkKey(mail_mass_mailing_testMapping.toDomain(mail_mass_mailing_testdto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_testMapping.toDomain(#mail_mass_mailing_testdto),'iBizBusinessCentral-Mail_mass_mailing_test-Save')")
    @ApiOperation(value = "保存示例邮件向导", tags = {"示例邮件向导" },  notes = "保存示例邮件向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tests/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_mass_mailing_testDTO mail_mass_mailing_testdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_testService.save(mail_mass_mailing_testMapping.toDomain(mail_mass_mailing_testdto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_testMapping.toDomain(#mail_mass_mailing_testdtos),'iBizBusinessCentral-Mail_mass_mailing_test-Save')")
    @ApiOperation(value = "批量保存示例邮件向导", tags = {"示例邮件向导" },  notes = "批量保存示例邮件向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tests/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_mass_mailing_testDTO> mail_mass_mailing_testdtos) {
        mail_mass_mailing_testService.saveBatch(mail_mass_mailing_testMapping.toDomain(mail_mass_mailing_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing_test-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing_test-Get')")
	@ApiOperation(value = "获取数据集", tags = {"示例邮件向导" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_tests/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailing_testDTO>> fetchDefault(Mail_mass_mailing_testSearchContext context) {
        Page<Mail_mass_mailing_test> domains = mail_mass_mailing_testService.searchDefault(context) ;
        List<Mail_mass_mailing_testDTO> list = mail_mass_mailing_testMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing_test-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing_test-Get')")
	@ApiOperation(value = "查询数据集", tags = {"示例邮件向导" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_mass_mailing_tests/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_testDTO>> searchDefault(@RequestBody Mail_mass_mailing_testSearchContext context) {
        Page<Mail_mass_mailing_test> domains = mail_mass_mailing_testService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailing_testMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

