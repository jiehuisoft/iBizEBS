package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_service_type;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_service_typeService;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_service_typeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"车辆服务类型" })
@RestController("Core-fleet_service_type")
@RequestMapping("")
public class Fleet_service_typeResource {

    @Autowired
    public IFleet_service_typeService fleet_service_typeService;

    @Autowired
    @Lazy
    public Fleet_service_typeMapping fleet_service_typeMapping;

    @PreAuthorize("hasPermission(this.fleet_service_typeMapping.toDomain(#fleet_service_typedto),'iBizBusinessCentral-Fleet_service_type-Create')")
    @ApiOperation(value = "新建车辆服务类型", tags = {"车辆服务类型" },  notes = "新建车辆服务类型")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types")
    public ResponseEntity<Fleet_service_typeDTO> create(@Validated @RequestBody Fleet_service_typeDTO fleet_service_typedto) {
        Fleet_service_type domain = fleet_service_typeMapping.toDomain(fleet_service_typedto);
		fleet_service_typeService.create(domain);
        Fleet_service_typeDTO dto = fleet_service_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_service_typeMapping.toDomain(#fleet_service_typedtos),'iBizBusinessCentral-Fleet_service_type-Create')")
    @ApiOperation(value = "批量新建车辆服务类型", tags = {"车辆服务类型" },  notes = "批量新建车辆服务类型")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_service_typeDTO> fleet_service_typedtos) {
        fleet_service_typeService.createBatch(fleet_service_typeMapping.toDomain(fleet_service_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fleet_service_type" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.fleet_service_typeService.get(#fleet_service_type_id),'iBizBusinessCentral-Fleet_service_type-Update')")
    @ApiOperation(value = "更新车辆服务类型", tags = {"车辆服务类型" },  notes = "更新车辆服务类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_service_types/{fleet_service_type_id}")
    public ResponseEntity<Fleet_service_typeDTO> update(@PathVariable("fleet_service_type_id") Long fleet_service_type_id, @RequestBody Fleet_service_typeDTO fleet_service_typedto) {
		Fleet_service_type domain  = fleet_service_typeMapping.toDomain(fleet_service_typedto);
        domain .setId(fleet_service_type_id);
		fleet_service_typeService.update(domain );
		Fleet_service_typeDTO dto = fleet_service_typeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_service_typeService.getFleetServiceTypeByEntities(this.fleet_service_typeMapping.toDomain(#fleet_service_typedtos)),'iBizBusinessCentral-Fleet_service_type-Update')")
    @ApiOperation(value = "批量更新车辆服务类型", tags = {"车辆服务类型" },  notes = "批量更新车辆服务类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_service_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_service_typeDTO> fleet_service_typedtos) {
        fleet_service_typeService.updateBatch(fleet_service_typeMapping.toDomain(fleet_service_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.fleet_service_typeService.get(#fleet_service_type_id),'iBizBusinessCentral-Fleet_service_type-Remove')")
    @ApiOperation(value = "删除车辆服务类型", tags = {"车辆服务类型" },  notes = "删除车辆服务类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_service_types/{fleet_service_type_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fleet_service_type_id") Long fleet_service_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_service_typeService.remove(fleet_service_type_id));
    }

    @PreAuthorize("hasPermission(this.fleet_service_typeService.getFleetServiceTypeByIds(#ids),'iBizBusinessCentral-Fleet_service_type-Remove')")
    @ApiOperation(value = "批量删除车辆服务类型", tags = {"车辆服务类型" },  notes = "批量删除车辆服务类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_service_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        fleet_service_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.fleet_service_typeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fleet_service_type-Get')")
    @ApiOperation(value = "获取车辆服务类型", tags = {"车辆服务类型" },  notes = "获取车辆服务类型")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_service_types/{fleet_service_type_id}")
    public ResponseEntity<Fleet_service_typeDTO> get(@PathVariable("fleet_service_type_id") Long fleet_service_type_id) {
        Fleet_service_type domain = fleet_service_typeService.get(fleet_service_type_id);
        Fleet_service_typeDTO dto = fleet_service_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取车辆服务类型草稿", tags = {"车辆服务类型" },  notes = "获取车辆服务类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_service_types/getdraft")
    public ResponseEntity<Fleet_service_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_service_typeMapping.toDto(fleet_service_typeService.getDraft(new Fleet_service_type())));
    }

    @ApiOperation(value = "检查车辆服务类型", tags = {"车辆服务类型" },  notes = "检查车辆服务类型")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Fleet_service_typeDTO fleet_service_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(fleet_service_typeService.checkKey(fleet_service_typeMapping.toDomain(fleet_service_typedto)));
    }

    @PreAuthorize("hasPermission(this.fleet_service_typeMapping.toDomain(#fleet_service_typedto),'iBizBusinessCentral-Fleet_service_type-Save')")
    @ApiOperation(value = "保存车辆服务类型", tags = {"车辆服务类型" },  notes = "保存车辆服务类型")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Fleet_service_typeDTO fleet_service_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_service_typeService.save(fleet_service_typeMapping.toDomain(fleet_service_typedto)));
    }

    @PreAuthorize("hasPermission(this.fleet_service_typeMapping.toDomain(#fleet_service_typedtos),'iBizBusinessCentral-Fleet_service_type-Save')")
    @ApiOperation(value = "批量保存车辆服务类型", tags = {"车辆服务类型" },  notes = "批量保存车辆服务类型")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Fleet_service_typeDTO> fleet_service_typedtos) {
        fleet_service_typeService.saveBatch(fleet_service_typeMapping.toDomain(fleet_service_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_service_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_service_type-Get')")
	@ApiOperation(value = "获取数据集", tags = {"车辆服务类型" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_service_types/fetchdefault")
	public ResponseEntity<List<Fleet_service_typeDTO>> fetchDefault(Fleet_service_typeSearchContext context) {
        Page<Fleet_service_type> domains = fleet_service_typeService.searchDefault(context) ;
        List<Fleet_service_typeDTO> list = fleet_service_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_service_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_service_type-Get')")
	@ApiOperation(value = "查询数据集", tags = {"车辆服务类型" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/fleet_service_types/searchdefault")
	public ResponseEntity<Page<Fleet_service_typeDTO>> searchDefault(@RequestBody Fleet_service_typeSearchContext context) {
        Page<Fleet_service_type> domains = fleet_service_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_service_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

