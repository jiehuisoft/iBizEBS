package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Project_projectDTO]
 */
@Data
public class Project_projectDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACCESS_WARNING]
     *
     */
    @JSONField(name = "access_warning")
    @JsonProperty("access_warning")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String accessWarning;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [TASKS]
     *
     */
    @JSONField(name = "tasks")
    @JsonProperty("tasks")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String tasks;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [PERCENTAGE_SATISFACTION_TASK]
     *
     */
    @JSONField(name = "percentage_satisfaction_task")
    @JsonProperty("percentage_satisfaction_task")
    private Integer percentageSatisfactionTask;

    /**
     * 属性 [ACCESS_URL]
     *
     */
    @JSONField(name = "access_url")
    @JsonProperty("access_url")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String accessUrl;

    /**
     * 属性 [TYPE_IDS]
     *
     */
    @JSONField(name = "type_ids")
    @JsonProperty("type_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String typeIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ACCESS_TOKEN]
     *
     */
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String accessToken;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [PRIVACY_VISIBILITY]
     *
     */
    @JSONField(name = "privacy_visibility")
    @JsonProperty("privacy_visibility")
    @NotBlank(message = "[隐私]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String privacyVisibility;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [RATING_STATUS_PERIOD]
     *
     */
    @JSONField(name = "rating_status_period")
    @JsonProperty("rating_status_period")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String ratingStatusPeriod;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [PERCENTAGE_SATISFACTION_PROJECT]
     *
     */
    @JSONField(name = "percentage_satisfaction_project")
    @JsonProperty("percentage_satisfaction_project")
    private Integer percentageSatisfactionProject;

    /**
     * 属性 [DOC_COUNT]
     *
     */
    @JSONField(name = "doc_count")
    @JsonProperty("doc_count")
    private Integer docCount;

    /**
     * 属性 [FAVORITE_USER_IDS]
     *
     */
    @JSONField(name = "favorite_user_ids")
    @JsonProperty("favorite_user_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String favoriteUserIds;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [TASK_COUNT]
     *
     */
    @JSONField(name = "task_count")
    @JsonProperty("task_count")
    private Integer taskCount;

    /**
     * 属性 [IS_FAVORITE]
     *
     */
    @JSONField(name = "is_favorite")
    @JsonProperty("is_favorite")
    private Boolean isFavorite;

    /**
     * 属性 [PORTAL_SHOW_RATING]
     *
     */
    @JSONField(name = "portal_show_rating")
    @JsonProperty("portal_show_rating")
    private Boolean portalShowRating;

    /**
     * 属性 [RATING_REQUEST_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "rating_request_deadline" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("rating_request_deadline")
    private Timestamp ratingRequestDeadline;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [RATING_STATUS]
     *
     */
    @JSONField(name = "rating_status")
    @JsonProperty("rating_status")
    @NotBlank(message = "[客户点评]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String ratingStatus;

    /**
     * 属性 [LABEL_TASKS]
     *
     */
    @JSONField(name = "label_tasks")
    @JsonProperty("label_tasks")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String labelTasks;

    /**
     * 属性 [TASK_IDS]
     *
     */
    @JSONField(name = "task_ids")
    @JsonProperty("task_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String taskIds;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [DATE_START]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd")
    @JsonProperty("date_start")
    private Timestamp dateStart;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long currencyId;

    /**
     * 属性 [ALIAS_PARENT_THREAD_ID]
     *
     */
    @JSONField(name = "alias_parent_thread_id")
    @JsonProperty("alias_parent_thread_id")
    private Integer aliasParentThreadId;

    /**
     * 属性 [ALIAS_FORCE_THREAD_ID]
     *
     */
    @JSONField(name = "alias_force_thread_id")
    @JsonProperty("alias_force_thread_id")
    private Integer aliasForceThreadId;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String aliasDomain;

    /**
     * 属性 [ALIAS_CONTACT]
     *
     */
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    @NotBlank(message = "[别名联系人安全]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String aliasContact;

    /**
     * 属性 [ALIAS_PARENT_MODEL_ID]
     *
     */
    @JSONField(name = "alias_parent_model_id")
    @JsonProperty("alias_parent_model_id")
    private Integer aliasParentModelId;

    /**
     * 属性 [ALIAS_MODEL_ID]
     *
     */
    @JSONField(name = "alias_model_id")
    @JsonProperty("alias_model_id")
    @NotNull(message = "[别名的模型]不允许为空!")
    private Integer aliasModelId;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [ALIAS_USER_ID]
     *
     */
    @JSONField(name = "alias_user_id")
    @JsonProperty("alias_user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long aliasUserId;

    /**
     * 属性 [ALIAS_DEFAULTS]
     *
     */
    @JSONField(name = "alias_defaults")
    @JsonProperty("alias_defaults")
    @NotBlank(message = "[默认值]不允许为空!")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String aliasDefaults;

    /**
     * 属性 [RESOURCE_CALENDAR_ID_TEXT]
     *
     */
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String resourceCalendarIdText;

    /**
     * 属性 [SUBTASK_PROJECT_ID_TEXT]
     *
     */
    @JSONField(name = "subtask_project_id_text")
    @JsonProperty("subtask_project_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String subtaskProjectIdText;

    /**
     * 属性 [ALIAS_NAME]
     *
     */
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String aliasName;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[公司]不允许为空!")
    private Long companyId;

    /**
     * 属性 [SUBTASK_PROJECT_ID]
     *
     */
    @JSONField(name = "subtask_project_id")
    @JsonProperty("subtask_project_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long subtaskProjectId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long resourceCalendarId;

    /**
     * 属性 [ALIAS_ID]
     *
     */
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[别名]不允许为空!")
    private Long aliasId;


    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [PERCENTAGE_SATISFACTION_TASK]
     */
    public void setPercentageSatisfactionTask(Integer  percentageSatisfactionTask){
        this.percentageSatisfactionTask = percentageSatisfactionTask ;
        this.modify("percentage_satisfaction_task",percentageSatisfactionTask);
    }

    /**
     * 设置 [ACCESS_TOKEN]
     */
    public void setAccessToken(String  accessToken){
        this.accessToken = accessToken ;
        this.modify("access_token",accessToken);
    }

    /**
     * 设置 [PRIVACY_VISIBILITY]
     */
    public void setPrivacyVisibility(String  privacyVisibility){
        this.privacyVisibility = privacyVisibility ;
        this.modify("privacy_visibility",privacyVisibility);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [RATING_STATUS_PERIOD]
     */
    public void setRatingStatusPeriod(String  ratingStatusPeriod){
        this.ratingStatusPeriod = ratingStatusPeriod ;
        this.modify("rating_status_period",ratingStatusPeriod);
    }

    /**
     * 设置 [PERCENTAGE_SATISFACTION_PROJECT]
     */
    public void setPercentageSatisfactionProject(Integer  percentageSatisfactionProject){
        this.percentageSatisfactionProject = percentageSatisfactionProject ;
        this.modify("percentage_satisfaction_project",percentageSatisfactionProject);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [PORTAL_SHOW_RATING]
     */
    public void setPortalShowRating(Boolean  portalShowRating){
        this.portalShowRating = portalShowRating ;
        this.modify("portal_show_rating",portalShowRating);
    }

    /**
     * 设置 [RATING_REQUEST_DEADLINE]
     */
    public void setRatingRequestDeadline(Timestamp  ratingRequestDeadline){
        this.ratingRequestDeadline = ratingRequestDeadline ;
        this.modify("rating_request_deadline",ratingRequestDeadline);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [RATING_STATUS]
     */
    public void setRatingStatus(String  ratingStatus){
        this.ratingStatus = ratingStatus ;
        this.modify("rating_status",ratingStatus);
    }

    /**
     * 设置 [LABEL_TASKS]
     */
    public void setLabelTasks(String  labelTasks){
        this.labelTasks = labelTasks ;
        this.modify("label_tasks",labelTasks);
    }

    /**
     * 设置 [DATE_START]
     */
    public void setDateStart(Timestamp  dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [SUBTASK_PROJECT_ID]
     */
    public void setSubtaskProjectId(Long  subtaskProjectId){
        this.subtaskProjectId = subtaskProjectId ;
        this.modify("subtask_project_id",subtaskProjectId);
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID]
     */
    public void setResourceCalendarId(Long  resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }

    /**
     * 设置 [ALIAS_ID]
     */
    public void setAliasId(Long  aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }


}


