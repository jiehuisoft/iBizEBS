package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Stock_move_lineDTO]
 */
@Data
public class Stock_move_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [QTY_DONE]
     *
     */
    @JSONField(name = "qty_done")
    @JsonProperty("qty_done")
    private Double qtyDone;

    /**
     * 属性 [PICKING_TYPE_USE_EXISTING_LOTS]
     *
     */
    @JSONField(name = "picking_type_use_existing_lots")
    @JsonProperty("picking_type_use_existing_lots")
    private Boolean pickingTypeUseExistingLots;

    /**
     * 属性 [CONSUME_LINE_IDS]
     *
     */
    @JSONField(name = "consume_line_ids")
    @JsonProperty("consume_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String consumeLineIds;

    /**
     * 属性 [LOTS_VISIBLE]
     *
     */
    @JSONField(name = "lots_visible")
    @JsonProperty("lots_visible")
    private Boolean lotsVisible;

    /**
     * 属性 [PRODUCT_UOM_QTY]
     *
     */
    @JSONField(name = "product_uom_qty")
    @JsonProperty("product_uom_qty")
    @NotNull(message = "[已保留]不允许为空!")
    private Double productUomQty;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DONE_WO]
     *
     */
    @JSONField(name = "done_wo")
    @JsonProperty("done_wo")
    private Boolean doneWo;

    /**
     * 属性 [PICKING_TYPE_USE_CREATE_LOTS]
     *
     */
    @JSONField(name = "picking_type_use_create_lots")
    @JsonProperty("picking_type_use_create_lots")
    private Boolean pickingTypeUseCreateLots;

    /**
     * 属性 [PRODUCE_LINE_IDS]
     *
     */
    @JSONField(name = "produce_line_ids")
    @JsonProperty("produce_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String produceLineIds;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    @NotNull(message = "[日期]不允许为空!")
    private Timestamp date;

    /**
     * 属性 [PICKING_TYPE_ENTIRE_PACKS]
     *
     */
    @JSONField(name = "picking_type_entire_packs")
    @JsonProperty("picking_type_entire_packs")
    private Boolean pickingTypeEntirePacks;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [LOT_NAME]
     *
     */
    @JSONField(name = "lot_name")
    @JsonProperty("lot_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String lotName;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;

    /**
     * 属性 [LOT_PRODUCED_QTY]
     *
     */
    @JSONField(name = "lot_produced_qty")
    @JsonProperty("lot_produced_qty")
    private Double lotProducedQty;

    /**
     * 属性 [PACKAGE_ID_TEXT]
     *
     */
    @JSONField(name = "package_id_text")
    @JsonProperty("package_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String packageIdText;

    /**
     * 属性 [LOT_PRODUCED_ID_TEXT]
     *
     */
    @JSONField(name = "lot_produced_id_text")
    @JsonProperty("lot_produced_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lotProducedIdText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [WORKORDER_ID_TEXT]
     *
     */
    @JSONField(name = "workorder_id_text")
    @JsonProperty("workorder_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String workorderIdText;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productUomIdText;

    /**
     * 属性 [TRACKING]
     *
     */
    @JSONField(name = "tracking")
    @JsonProperty("tracking")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String tracking;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [MOVE_ID_TEXT]
     *
     */
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String moveIdText;

    /**
     * 属性 [LOCATION_DEST_ID_TEXT]
     *
     */
    @JSONField(name = "location_dest_id_text")
    @JsonProperty("location_dest_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String locationDestIdText;

    /**
     * 属性 [IS_LOCKED]
     *
     */
    @JSONField(name = "is_locked")
    @JsonProperty("is_locked")
    private Boolean isLocked;

    /**
     * 属性 [IS_INITIAL_DEMAND_EDITABLE]
     *
     */
    @JSONField(name = "is_initial_demand_editable")
    @JsonProperty("is_initial_demand_editable")
    private Boolean isInitialDemandEditable;

    /**
     * 属性 [PRODUCTION_ID_TEXT]
     *
     */
    @JSONField(name = "production_id_text")
    @JsonProperty("production_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productionIdText;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [LOT_ID_TEXT]
     *
     */
    @JSONField(name = "lot_id_text")
    @JsonProperty("lot_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lotIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [REFERENCE]
     *
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String reference;

    /**
     * 属性 [RESULT_PACKAGE_ID_TEXT]
     *
     */
    @JSONField(name = "result_package_id_text")
    @JsonProperty("result_package_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String resultPackageIdText;

    /**
     * 属性 [DONE_MOVE]
     *
     */
    @JSONField(name = "done_move")
    @JsonProperty("done_move")
    private Boolean doneMove;

    /**
     * 属性 [PICKING_ID_TEXT]
     *
     */
    @JSONField(name = "picking_id_text")
    @JsonProperty("picking_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pickingIdText;

    /**
     * 属性 [OWNER_ID_TEXT]
     *
     */
    @JSONField(name = "owner_id_text")
    @JsonProperty("owner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String ownerIdText;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String locationIdText;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[单位]不允许为空!")
    private Long productUomId;

    /**
     * 属性 [MOVE_ID]
     *
     */
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long moveId;

    /**
     * 属性 [RESULT_PACKAGE_ID]
     *
     */
    @JSONField(name = "result_package_id")
    @JsonProperty("result_package_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long resultPackageId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [LOT_PRODUCED_ID]
     *
     */
    @JSONField(name = "lot_produced_id")
    @JsonProperty("lot_produced_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long lotProducedId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [LOT_ID]
     *
     */
    @JSONField(name = "lot_id")
    @JsonProperty("lot_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long lotId;

    /**
     * 属性 [PICKING_ID]
     *
     */
    @JSONField(name = "picking_id")
    @JsonProperty("picking_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pickingId;

    /**
     * 属性 [WORKORDER_ID]
     *
     */
    @JSONField(name = "workorder_id")
    @JsonProperty("workorder_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long workorderId;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[从]不允许为空!")
    private Long locationId;

    /**
     * 属性 [PACKAGE_ID]
     *
     */
    @JSONField(name = "package_id")
    @JsonProperty("package_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long packageId;

    /**
     * 属性 [OWNER_ID]
     *
     */
    @JSONField(name = "owner_id")
    @JsonProperty("owner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long ownerId;

    /**
     * 属性 [PACKAGE_LEVEL_ID]
     *
     */
    @JSONField(name = "package_level_id")
    @JsonProperty("package_level_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long packageLevelId;

    /**
     * 属性 [LOCATION_DEST_ID]
     *
     */
    @JSONField(name = "location_dest_id")
    @JsonProperty("location_dest_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[至]不允许为空!")
    private Long locationDestId;

    /**
     * 属性 [PRODUCTION_ID]
     *
     */
    @JSONField(name = "production_id")
    @JsonProperty("production_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productionId;


    /**
     * 设置 [QTY_DONE]
     */
    public void setQtyDone(Double  qtyDone){
        this.qtyDone = qtyDone ;
        this.modify("qty_done",qtyDone);
    }

    /**
     * 设置 [PRODUCT_UOM_QTY]
     */
    public void setProductUomQty(Double  productUomQty){
        this.productUomQty = productUomQty ;
        this.modify("product_uom_qty",productUomQty);
    }

    /**
     * 设置 [DONE_WO]
     */
    public void setDoneWo(Boolean  doneWo){
        this.doneWo = doneWo ;
        this.modify("done_wo",doneWo);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [LOT_NAME]
     */
    public void setLotName(String  lotName){
        this.lotName = lotName ;
        this.modify("lot_name",lotName);
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    public void setProductQty(Double  productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [LOT_PRODUCED_QTY]
     */
    public void setLotProducedQty(Double  lotProducedQty){
        this.lotProducedQty = lotProducedQty ;
        this.modify("lot_produced_qty",lotProducedQty);
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    public void setProductUomId(Long  productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [MOVE_ID]
     */
    public void setMoveId(Long  moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }

    /**
     * 设置 [RESULT_PACKAGE_ID]
     */
    public void setResultPackageId(Long  resultPackageId){
        this.resultPackageId = resultPackageId ;
        this.modify("result_package_id",resultPackageId);
    }

    /**
     * 设置 [LOT_PRODUCED_ID]
     */
    public void setLotProducedId(Long  lotProducedId){
        this.lotProducedId = lotProducedId ;
        this.modify("lot_produced_id",lotProducedId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [LOT_ID]
     */
    public void setLotId(Long  lotId){
        this.lotId = lotId ;
        this.modify("lot_id",lotId);
    }

    /**
     * 设置 [PICKING_ID]
     */
    public void setPickingId(Long  pickingId){
        this.pickingId = pickingId ;
        this.modify("picking_id",pickingId);
    }

    /**
     * 设置 [WORKORDER_ID]
     */
    public void setWorkorderId(Long  workorderId){
        this.workorderId = workorderId ;
        this.modify("workorder_id",workorderId);
    }

    /**
     * 设置 [LOCATION_ID]
     */
    public void setLocationId(Long  locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }

    /**
     * 设置 [PACKAGE_ID]
     */
    public void setPackageId(Long  packageId){
        this.packageId = packageId ;
        this.modify("package_id",packageId);
    }

    /**
     * 设置 [OWNER_ID]
     */
    public void setOwnerId(Long  ownerId){
        this.ownerId = ownerId ;
        this.modify("owner_id",ownerId);
    }

    /**
     * 设置 [PACKAGE_LEVEL_ID]
     */
    public void setPackageLevelId(Long  packageLevelId){
        this.packageLevelId = packageLevelId ;
        this.modify("package_level_id",packageLevelId);
    }

    /**
     * 设置 [LOCATION_DEST_ID]
     */
    public void setLocationDestId(Long  locationDestId){
        this.locationDestId = locationDestId ;
        this.modify("location_dest_id",locationDestId);
    }

    /**
     * 设置 [PRODUCTION_ID]
     */
    public void setProductionId(Long  productionId){
        this.productionId = productionId ;
        this.modify("production_id",productionId);
    }


}


