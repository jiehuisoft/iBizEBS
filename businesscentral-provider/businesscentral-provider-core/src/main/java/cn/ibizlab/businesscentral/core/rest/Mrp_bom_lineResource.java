package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_bom_line;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bom_lineService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_bom_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"物料清单明细行" })
@RestController("Core-mrp_bom_line")
@RequestMapping("")
public class Mrp_bom_lineResource {

    @Autowired
    public IMrp_bom_lineService mrp_bom_lineService;

    @Autowired
    @Lazy
    public Mrp_bom_lineMapping mrp_bom_lineMapping;

    @PreAuthorize("hasPermission(this.mrp_bom_lineMapping.toDomain(#mrp_bom_linedto),'iBizBusinessCentral-Mrp_bom_line-Create')")
    @ApiOperation(value = "新建物料清单明细行", tags = {"物料清单明细行" },  notes = "新建物料清单明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines")
    public ResponseEntity<Mrp_bom_lineDTO> create(@Validated @RequestBody Mrp_bom_lineDTO mrp_bom_linedto) {
        Mrp_bom_line domain = mrp_bom_lineMapping.toDomain(mrp_bom_linedto);
		mrp_bom_lineService.create(domain);
        Mrp_bom_lineDTO dto = mrp_bom_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_bom_lineMapping.toDomain(#mrp_bom_linedtos),'iBizBusinessCentral-Mrp_bom_line-Create')")
    @ApiOperation(value = "批量新建物料清单明细行", tags = {"物料清单明细行" },  notes = "批量新建物料清单明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_bom_lineDTO> mrp_bom_linedtos) {
        mrp_bom_lineService.createBatch(mrp_bom_lineMapping.toDomain(mrp_bom_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_bom_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_bom_lineService.get(#mrp_bom_line_id),'iBizBusinessCentral-Mrp_bom_line-Update')")
    @ApiOperation(value = "更新物料清单明细行", tags = {"物料清单明细行" },  notes = "更新物料清单明细行")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_bom_lines/{mrp_bom_line_id}")
    public ResponseEntity<Mrp_bom_lineDTO> update(@PathVariable("mrp_bom_line_id") Long mrp_bom_line_id, @RequestBody Mrp_bom_lineDTO mrp_bom_linedto) {
		Mrp_bom_line domain  = mrp_bom_lineMapping.toDomain(mrp_bom_linedto);
        domain .setId(mrp_bom_line_id);
		mrp_bom_lineService.update(domain );
		Mrp_bom_lineDTO dto = mrp_bom_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_bom_lineService.getMrpBomLineByEntities(this.mrp_bom_lineMapping.toDomain(#mrp_bom_linedtos)),'iBizBusinessCentral-Mrp_bom_line-Update')")
    @ApiOperation(value = "批量更新物料清单明细行", tags = {"物料清单明细行" },  notes = "批量更新物料清单明细行")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_bom_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_bom_lineDTO> mrp_bom_linedtos) {
        mrp_bom_lineService.updateBatch(mrp_bom_lineMapping.toDomain(mrp_bom_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_bom_lineService.get(#mrp_bom_line_id),'iBizBusinessCentral-Mrp_bom_line-Remove')")
    @ApiOperation(value = "删除物料清单明细行", tags = {"物料清单明细行" },  notes = "删除物料清单明细行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_bom_lines/{mrp_bom_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_bom_line_id") Long mrp_bom_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_bom_lineService.remove(mrp_bom_line_id));
    }

    @PreAuthorize("hasPermission(this.mrp_bom_lineService.getMrpBomLineByIds(#ids),'iBizBusinessCentral-Mrp_bom_line-Remove')")
    @ApiOperation(value = "批量删除物料清单明细行", tags = {"物料清单明细行" },  notes = "批量删除物料清单明细行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_bom_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_bom_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_bom_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_bom_line-Get')")
    @ApiOperation(value = "获取物料清单明细行", tags = {"物料清单明细行" },  notes = "获取物料清单明细行")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_bom_lines/{mrp_bom_line_id}")
    public ResponseEntity<Mrp_bom_lineDTO> get(@PathVariable("mrp_bom_line_id") Long mrp_bom_line_id) {
        Mrp_bom_line domain = mrp_bom_lineService.get(mrp_bom_line_id);
        Mrp_bom_lineDTO dto = mrp_bom_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取物料清单明细行草稿", tags = {"物料清单明细行" },  notes = "获取物料清单明细行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_bom_lines/getdraft")
    public ResponseEntity<Mrp_bom_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_bom_lineMapping.toDto(mrp_bom_lineService.getDraft(new Mrp_bom_line())));
    }

    @ApiOperation(value = "检查物料清单明细行", tags = {"物料清单明细行" },  notes = "检查物料清单明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_bom_lineDTO mrp_bom_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_bom_lineService.checkKey(mrp_bom_lineMapping.toDomain(mrp_bom_linedto)));
    }

    @PreAuthorize("hasPermission(this.mrp_bom_lineMapping.toDomain(#mrp_bom_linedto),'iBizBusinessCentral-Mrp_bom_line-Save')")
    @ApiOperation(value = "保存物料清单明细行", tags = {"物料清单明细行" },  notes = "保存物料清单明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_bom_lineDTO mrp_bom_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_bom_lineService.save(mrp_bom_lineMapping.toDomain(mrp_bom_linedto)));
    }

    @PreAuthorize("hasPermission(this.mrp_bom_lineMapping.toDomain(#mrp_bom_linedtos),'iBizBusinessCentral-Mrp_bom_line-Save')")
    @ApiOperation(value = "批量保存物料清单明细行", tags = {"物料清单明细行" },  notes = "批量保存物料清单明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_bom_lineDTO> mrp_bom_linedtos) {
        mrp_bom_lineService.saveBatch(mrp_bom_lineMapping.toDomain(mrp_bom_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_bom_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_bom_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"物料清单明细行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_bom_lines/fetchdefault")
	public ResponseEntity<List<Mrp_bom_lineDTO>> fetchDefault(Mrp_bom_lineSearchContext context) {
        Page<Mrp_bom_line> domains = mrp_bom_lineService.searchDefault(context) ;
        List<Mrp_bom_lineDTO> list = mrp_bom_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_bom_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_bom_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"物料清单明细行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_bom_lines/searchdefault")
	public ResponseEntity<Page<Mrp_bom_lineDTO>> searchDefault(@RequestBody Mrp_bom_lineSearchContext context) {
        Page<Mrp_bom_line> domains = mrp_bom_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_bom_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

