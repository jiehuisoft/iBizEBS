package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_tags;
import cn.ibizlab.businesscentral.core.odoo_project.service.IProject_tagsService;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_tagsSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"项目标签" })
@RestController("Core-project_tags")
@RequestMapping("")
public class Project_tagsResource {

    @Autowired
    public IProject_tagsService project_tagsService;

    @Autowired
    @Lazy
    public Project_tagsMapping project_tagsMapping;

    @PreAuthorize("hasPermission(this.project_tagsMapping.toDomain(#project_tagsdto),'iBizBusinessCentral-Project_tags-Create')")
    @ApiOperation(value = "新建项目标签", tags = {"项目标签" },  notes = "新建项目标签")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tags")
    public ResponseEntity<Project_tagsDTO> create(@Validated @RequestBody Project_tagsDTO project_tagsdto) {
        Project_tags domain = project_tagsMapping.toDomain(project_tagsdto);
		project_tagsService.create(domain);
        Project_tagsDTO dto = project_tagsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.project_tagsMapping.toDomain(#project_tagsdtos),'iBizBusinessCentral-Project_tags-Create')")
    @ApiOperation(value = "批量新建项目标签", tags = {"项目标签" },  notes = "批量新建项目标签")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tags/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Project_tagsDTO> project_tagsdtos) {
        project_tagsService.createBatch(project_tagsMapping.toDomain(project_tagsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "project_tags" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.project_tagsService.get(#project_tags_id),'iBizBusinessCentral-Project_tags-Update')")
    @ApiOperation(value = "更新项目标签", tags = {"项目标签" },  notes = "更新项目标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_tags/{project_tags_id}")
    public ResponseEntity<Project_tagsDTO> update(@PathVariable("project_tags_id") Long project_tags_id, @RequestBody Project_tagsDTO project_tagsdto) {
		Project_tags domain  = project_tagsMapping.toDomain(project_tagsdto);
        domain .setId(project_tags_id);
		project_tagsService.update(domain );
		Project_tagsDTO dto = project_tagsMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.project_tagsService.getProjectTagsByEntities(this.project_tagsMapping.toDomain(#project_tagsdtos)),'iBizBusinessCentral-Project_tags-Update')")
    @ApiOperation(value = "批量更新项目标签", tags = {"项目标签" },  notes = "批量更新项目标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_tags/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Project_tagsDTO> project_tagsdtos) {
        project_tagsService.updateBatch(project_tagsMapping.toDomain(project_tagsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.project_tagsService.get(#project_tags_id),'iBizBusinessCentral-Project_tags-Remove')")
    @ApiOperation(value = "删除项目标签", tags = {"项目标签" },  notes = "删除项目标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_tags/{project_tags_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("project_tags_id") Long project_tags_id) {
         return ResponseEntity.status(HttpStatus.OK).body(project_tagsService.remove(project_tags_id));
    }

    @PreAuthorize("hasPermission(this.project_tagsService.getProjectTagsByIds(#ids),'iBizBusinessCentral-Project_tags-Remove')")
    @ApiOperation(value = "批量删除项目标签", tags = {"项目标签" },  notes = "批量删除项目标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_tags/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        project_tagsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.project_tagsMapping.toDomain(returnObject.body),'iBizBusinessCentral-Project_tags-Get')")
    @ApiOperation(value = "获取项目标签", tags = {"项目标签" },  notes = "获取项目标签")
	@RequestMapping(method = RequestMethod.GET, value = "/project_tags/{project_tags_id}")
    public ResponseEntity<Project_tagsDTO> get(@PathVariable("project_tags_id") Long project_tags_id) {
        Project_tags domain = project_tagsService.get(project_tags_id);
        Project_tagsDTO dto = project_tagsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取项目标签草稿", tags = {"项目标签" },  notes = "获取项目标签草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/project_tags/getdraft")
    public ResponseEntity<Project_tagsDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(project_tagsMapping.toDto(project_tagsService.getDraft(new Project_tags())));
    }

    @ApiOperation(value = "检查项目标签", tags = {"项目标签" },  notes = "检查项目标签")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tags/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Project_tagsDTO project_tagsdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(project_tagsService.checkKey(project_tagsMapping.toDomain(project_tagsdto)));
    }

    @PreAuthorize("hasPermission(this.project_tagsMapping.toDomain(#project_tagsdto),'iBizBusinessCentral-Project_tags-Save')")
    @ApiOperation(value = "保存项目标签", tags = {"项目标签" },  notes = "保存项目标签")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tags/save")
    public ResponseEntity<Boolean> save(@RequestBody Project_tagsDTO project_tagsdto) {
        return ResponseEntity.status(HttpStatus.OK).body(project_tagsService.save(project_tagsMapping.toDomain(project_tagsdto)));
    }

    @PreAuthorize("hasPermission(this.project_tagsMapping.toDomain(#project_tagsdtos),'iBizBusinessCentral-Project_tags-Save')")
    @ApiOperation(value = "批量保存项目标签", tags = {"项目标签" },  notes = "批量保存项目标签")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tags/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Project_tagsDTO> project_tagsdtos) {
        project_tagsService.saveBatch(project_tagsMapping.toDomain(project_tagsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Project_tags-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Project_tags-Get')")
	@ApiOperation(value = "获取数据集", tags = {"项目标签" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/project_tags/fetchdefault")
	public ResponseEntity<List<Project_tagsDTO>> fetchDefault(Project_tagsSearchContext context) {
        Page<Project_tags> domains = project_tagsService.searchDefault(context) ;
        List<Project_tagsDTO> list = project_tagsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Project_tags-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Project_tags-Get')")
	@ApiOperation(value = "查询数据集", tags = {"项目标签" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/project_tags/searchdefault")
	public ResponseEntity<Page<Project_tagsDTO>> searchDefault(@RequestBody Project_tagsSearchContext context) {
        Page<Project_tags> domains = project_tagsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(project_tagsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

