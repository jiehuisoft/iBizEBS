package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_cashbox;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_cashboxService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statement_cashboxSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"银行资金调节表" })
@RestController("Core-account_bank_statement_cashbox")
@RequestMapping("")
public class Account_bank_statement_cashboxResource {

    @Autowired
    public IAccount_bank_statement_cashboxService account_bank_statement_cashboxService;

    @Autowired
    @Lazy
    public Account_bank_statement_cashboxMapping account_bank_statement_cashboxMapping;

    @PreAuthorize("hasPermission(this.account_bank_statement_cashboxMapping.toDomain(#account_bank_statement_cashboxdto),'iBizBusinessCentral-Account_bank_statement_cashbox-Create')")
    @ApiOperation(value = "新建银行资金调节表", tags = {"银行资金调节表" },  notes = "新建银行资金调节表")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_cashboxes")
    public ResponseEntity<Account_bank_statement_cashboxDTO> create(@Validated @RequestBody Account_bank_statement_cashboxDTO account_bank_statement_cashboxdto) {
        Account_bank_statement_cashbox domain = account_bank_statement_cashboxMapping.toDomain(account_bank_statement_cashboxdto);
		account_bank_statement_cashboxService.create(domain);
        Account_bank_statement_cashboxDTO dto = account_bank_statement_cashboxMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_cashboxMapping.toDomain(#account_bank_statement_cashboxdtos),'iBizBusinessCentral-Account_bank_statement_cashbox-Create')")
    @ApiOperation(value = "批量新建银行资金调节表", tags = {"银行资金调节表" },  notes = "批量新建银行资金调节表")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_cashboxes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_bank_statement_cashboxDTO> account_bank_statement_cashboxdtos) {
        account_bank_statement_cashboxService.createBatch(account_bank_statement_cashboxMapping.toDomain(account_bank_statement_cashboxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_bank_statement_cashbox" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_bank_statement_cashboxService.get(#account_bank_statement_cashbox_id),'iBizBusinessCentral-Account_bank_statement_cashbox-Update')")
    @ApiOperation(value = "更新银行资金调节表", tags = {"银行资金调节表" },  notes = "更新银行资金调节表")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_cashboxes/{account_bank_statement_cashbox_id}")
    public ResponseEntity<Account_bank_statement_cashboxDTO> update(@PathVariable("account_bank_statement_cashbox_id") Long account_bank_statement_cashbox_id, @RequestBody Account_bank_statement_cashboxDTO account_bank_statement_cashboxdto) {
		Account_bank_statement_cashbox domain  = account_bank_statement_cashboxMapping.toDomain(account_bank_statement_cashboxdto);
        domain .setId(account_bank_statement_cashbox_id);
		account_bank_statement_cashboxService.update(domain );
		Account_bank_statement_cashboxDTO dto = account_bank_statement_cashboxMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_cashboxService.getAccountBankStatementCashboxByEntities(this.account_bank_statement_cashboxMapping.toDomain(#account_bank_statement_cashboxdtos)),'iBizBusinessCentral-Account_bank_statement_cashbox-Update')")
    @ApiOperation(value = "批量更新银行资金调节表", tags = {"银行资金调节表" },  notes = "批量更新银行资金调节表")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_cashboxes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statement_cashboxDTO> account_bank_statement_cashboxdtos) {
        account_bank_statement_cashboxService.updateBatch(account_bank_statement_cashboxMapping.toDomain(account_bank_statement_cashboxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_cashboxService.get(#account_bank_statement_cashbox_id),'iBizBusinessCentral-Account_bank_statement_cashbox-Remove')")
    @ApiOperation(value = "删除银行资金调节表", tags = {"银行资金调节表" },  notes = "删除银行资金调节表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_cashboxes/{account_bank_statement_cashbox_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_cashbox_id") Long account_bank_statement_cashbox_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_cashboxService.remove(account_bank_statement_cashbox_id));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_cashboxService.getAccountBankStatementCashboxByIds(#ids),'iBizBusinessCentral-Account_bank_statement_cashbox-Remove')")
    @ApiOperation(value = "批量删除银行资金调节表", tags = {"银行资金调节表" },  notes = "批量删除银行资金调节表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_cashboxes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_bank_statement_cashboxService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_bank_statement_cashboxMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_bank_statement_cashbox-Get')")
    @ApiOperation(value = "获取银行资金调节表", tags = {"银行资金调节表" },  notes = "获取银行资金调节表")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_cashboxes/{account_bank_statement_cashbox_id}")
    public ResponseEntity<Account_bank_statement_cashboxDTO> get(@PathVariable("account_bank_statement_cashbox_id") Long account_bank_statement_cashbox_id) {
        Account_bank_statement_cashbox domain = account_bank_statement_cashboxService.get(account_bank_statement_cashbox_id);
        Account_bank_statement_cashboxDTO dto = account_bank_statement_cashboxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取银行资金调节表草稿", tags = {"银行资金调节表" },  notes = "获取银行资金调节表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_cashboxes/getdraft")
    public ResponseEntity<Account_bank_statement_cashboxDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_cashboxMapping.toDto(account_bank_statement_cashboxService.getDraft(new Account_bank_statement_cashbox())));
    }

    @ApiOperation(value = "检查银行资金调节表", tags = {"银行资金调节表" },  notes = "检查银行资金调节表")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_cashboxes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_bank_statement_cashboxDTO account_bank_statement_cashboxdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_cashboxService.checkKey(account_bank_statement_cashboxMapping.toDomain(account_bank_statement_cashboxdto)));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_cashboxMapping.toDomain(#account_bank_statement_cashboxdto),'iBizBusinessCentral-Account_bank_statement_cashbox-Save')")
    @ApiOperation(value = "保存银行资金调节表", tags = {"银行资金调节表" },  notes = "保存银行资金调节表")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_cashboxes/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_bank_statement_cashboxDTO account_bank_statement_cashboxdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_cashboxService.save(account_bank_statement_cashboxMapping.toDomain(account_bank_statement_cashboxdto)));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_cashboxMapping.toDomain(#account_bank_statement_cashboxdtos),'iBizBusinessCentral-Account_bank_statement_cashbox-Save')")
    @ApiOperation(value = "批量保存银行资金调节表", tags = {"银行资金调节表" },  notes = "批量保存银行资金调节表")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_cashboxes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_bank_statement_cashboxDTO> account_bank_statement_cashboxdtos) {
        account_bank_statement_cashboxService.saveBatch(account_bank_statement_cashboxMapping.toDomain(account_bank_statement_cashboxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_bank_statement_cashbox-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_bank_statement_cashbox-Get')")
	@ApiOperation(value = "获取数据集", tags = {"银行资金调节表" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statement_cashboxes/fetchdefault")
	public ResponseEntity<List<Account_bank_statement_cashboxDTO>> fetchDefault(Account_bank_statement_cashboxSearchContext context) {
        Page<Account_bank_statement_cashbox> domains = account_bank_statement_cashboxService.searchDefault(context) ;
        List<Account_bank_statement_cashboxDTO> list = account_bank_statement_cashboxMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_bank_statement_cashbox-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_bank_statement_cashbox-Get')")
	@ApiOperation(value = "查询数据集", tags = {"银行资金调节表" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_bank_statement_cashboxes/searchdefault")
	public ResponseEntity<Page<Account_bank_statement_cashboxDTO>> searchDefault(@RequestBody Account_bank_statement_cashboxSearchContext context) {
        Page<Account_bank_statement_cashbox> domains = account_bank_statement_cashboxService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_bank_statement_cashboxMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

