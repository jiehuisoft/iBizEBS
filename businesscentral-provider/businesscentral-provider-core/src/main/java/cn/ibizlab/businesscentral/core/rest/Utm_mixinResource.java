package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_mixin;
import cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_mixinService;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_mixinSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"UTM Mixin" })
@RestController("Core-utm_mixin")
@RequestMapping("")
public class Utm_mixinResource {

    @Autowired
    public IUtm_mixinService utm_mixinService;

    @Autowired
    @Lazy
    public Utm_mixinMapping utm_mixinMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_mixin-Create-all')")
    @ApiOperation(value = "新建UTM Mixin", tags = {"UTM Mixin" },  notes = "新建UTM Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_mixins")
    public ResponseEntity<Utm_mixinDTO> create(@Validated @RequestBody Utm_mixinDTO utm_mixindto) {
        Utm_mixin domain = utm_mixinMapping.toDomain(utm_mixindto);
		utm_mixinService.create(domain);
        Utm_mixinDTO dto = utm_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_mixin-Create-all')")
    @ApiOperation(value = "批量新建UTM Mixin", tags = {"UTM Mixin" },  notes = "批量新建UTM Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Utm_mixinDTO> utm_mixindtos) {
        utm_mixinService.createBatch(utm_mixinMapping.toDomain(utm_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_mixin-Update-all')")
    @ApiOperation(value = "更新UTM Mixin", tags = {"UTM Mixin" },  notes = "更新UTM Mixin")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_mixins/{utm_mixin_id}")
    public ResponseEntity<Utm_mixinDTO> update(@PathVariable("utm_mixin_id") Long utm_mixin_id, @RequestBody Utm_mixinDTO utm_mixindto) {
		Utm_mixin domain  = utm_mixinMapping.toDomain(utm_mixindto);
        domain .setId(utm_mixin_id);
		utm_mixinService.update(domain );
		Utm_mixinDTO dto = utm_mixinMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_mixin-Update-all')")
    @ApiOperation(value = "批量更新UTM Mixin", tags = {"UTM Mixin" },  notes = "批量更新UTM Mixin")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Utm_mixinDTO> utm_mixindtos) {
        utm_mixinService.updateBatch(utm_mixinMapping.toDomain(utm_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_mixin-Remove-all')")
    @ApiOperation(value = "删除UTM Mixin", tags = {"UTM Mixin" },  notes = "删除UTM Mixin")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_mixins/{utm_mixin_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("utm_mixin_id") Long utm_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(utm_mixinService.remove(utm_mixin_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_mixin-Remove-all')")
    @ApiOperation(value = "批量删除UTM Mixin", tags = {"UTM Mixin" },  notes = "批量删除UTM Mixin")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        utm_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_mixin-Get-all')")
    @ApiOperation(value = "获取UTM Mixin", tags = {"UTM Mixin" },  notes = "获取UTM Mixin")
	@RequestMapping(method = RequestMethod.GET, value = "/utm_mixins/{utm_mixin_id}")
    public ResponseEntity<Utm_mixinDTO> get(@PathVariable("utm_mixin_id") Long utm_mixin_id) {
        Utm_mixin domain = utm_mixinService.get(utm_mixin_id);
        Utm_mixinDTO dto = utm_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取UTM Mixin草稿", tags = {"UTM Mixin" },  notes = "获取UTM Mixin草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/utm_mixins/getdraft")
    public ResponseEntity<Utm_mixinDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(utm_mixinMapping.toDto(utm_mixinService.getDraft(new Utm_mixin())));
    }

    @ApiOperation(value = "检查UTM Mixin", tags = {"UTM Mixin" },  notes = "检查UTM Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_mixins/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Utm_mixinDTO utm_mixindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(utm_mixinService.checkKey(utm_mixinMapping.toDomain(utm_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_mixin-Save-all')")
    @ApiOperation(value = "保存UTM Mixin", tags = {"UTM Mixin" },  notes = "保存UTM Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_mixins/save")
    public ResponseEntity<Boolean> save(@RequestBody Utm_mixinDTO utm_mixindto) {
        return ResponseEntity.status(HttpStatus.OK).body(utm_mixinService.save(utm_mixinMapping.toDomain(utm_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_mixin-Save-all')")
    @ApiOperation(value = "批量保存UTM Mixin", tags = {"UTM Mixin" },  notes = "批量保存UTM Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_mixins/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Utm_mixinDTO> utm_mixindtos) {
        utm_mixinService.saveBatch(utm_mixinMapping.toDomain(utm_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_mixin-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"UTM Mixin" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/utm_mixins/fetchdefault")
	public ResponseEntity<List<Utm_mixinDTO>> fetchDefault(Utm_mixinSearchContext context) {
        Page<Utm_mixin> domains = utm_mixinService.searchDefault(context) ;
        List<Utm_mixinDTO> list = utm_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Utm_mixin-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"UTM Mixin" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/utm_mixins/searchdefault")
	public ResponseEntity<Page<Utm_mixinDTO>> searchDefault(@RequestBody Utm_mixinSearchContext context) {
        Page<Utm_mixin> domains = utm_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(utm_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

