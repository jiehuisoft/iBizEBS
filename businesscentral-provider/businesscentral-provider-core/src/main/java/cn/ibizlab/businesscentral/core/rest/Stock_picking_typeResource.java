package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_picking_typeService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_picking_typeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"拣货类型" })
@RestController("Core-stock_picking_type")
@RequestMapping("")
public class Stock_picking_typeResource {

    @Autowired
    public IStock_picking_typeService stock_picking_typeService;

    @Autowired
    @Lazy
    public Stock_picking_typeMapping stock_picking_typeMapping;

    @PreAuthorize("hasPermission(this.stock_picking_typeMapping.toDomain(#stock_picking_typedto),'iBizBusinessCentral-Stock_picking_type-Create')")
    @ApiOperation(value = "新建拣货类型", tags = {"拣货类型" },  notes = "新建拣货类型")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_picking_types")
    public ResponseEntity<Stock_picking_typeDTO> create(@Validated @RequestBody Stock_picking_typeDTO stock_picking_typedto) {
        Stock_picking_type domain = stock_picking_typeMapping.toDomain(stock_picking_typedto);
		stock_picking_typeService.create(domain);
        Stock_picking_typeDTO dto = stock_picking_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_picking_typeMapping.toDomain(#stock_picking_typedtos),'iBizBusinessCentral-Stock_picking_type-Create')")
    @ApiOperation(value = "批量新建拣货类型", tags = {"拣货类型" },  notes = "批量新建拣货类型")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_picking_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_picking_typeDTO> stock_picking_typedtos) {
        stock_picking_typeService.createBatch(stock_picking_typeMapping.toDomain(stock_picking_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_picking_type" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_picking_typeService.get(#stock_picking_type_id),'iBizBusinessCentral-Stock_picking_type-Update')")
    @ApiOperation(value = "更新拣货类型", tags = {"拣货类型" },  notes = "更新拣货类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_picking_types/{stock_picking_type_id}")
    public ResponseEntity<Stock_picking_typeDTO> update(@PathVariable("stock_picking_type_id") Long stock_picking_type_id, @RequestBody Stock_picking_typeDTO stock_picking_typedto) {
		Stock_picking_type domain  = stock_picking_typeMapping.toDomain(stock_picking_typedto);
        domain .setId(stock_picking_type_id);
		stock_picking_typeService.update(domain );
		Stock_picking_typeDTO dto = stock_picking_typeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_picking_typeService.getStockPickingTypeByEntities(this.stock_picking_typeMapping.toDomain(#stock_picking_typedtos)),'iBizBusinessCentral-Stock_picking_type-Update')")
    @ApiOperation(value = "批量更新拣货类型", tags = {"拣货类型" },  notes = "批量更新拣货类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_picking_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_picking_typeDTO> stock_picking_typedtos) {
        stock_picking_typeService.updateBatch(stock_picking_typeMapping.toDomain(stock_picking_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_picking_typeService.get(#stock_picking_type_id),'iBizBusinessCentral-Stock_picking_type-Remove')")
    @ApiOperation(value = "删除拣货类型", tags = {"拣货类型" },  notes = "删除拣货类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_picking_types/{stock_picking_type_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_picking_type_id") Long stock_picking_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_picking_typeService.remove(stock_picking_type_id));
    }

    @PreAuthorize("hasPermission(this.stock_picking_typeService.getStockPickingTypeByIds(#ids),'iBizBusinessCentral-Stock_picking_type-Remove')")
    @ApiOperation(value = "批量删除拣货类型", tags = {"拣货类型" },  notes = "批量删除拣货类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_picking_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_picking_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_picking_typeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_picking_type-Get')")
    @ApiOperation(value = "获取拣货类型", tags = {"拣货类型" },  notes = "获取拣货类型")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_picking_types/{stock_picking_type_id}")
    public ResponseEntity<Stock_picking_typeDTO> get(@PathVariable("stock_picking_type_id") Long stock_picking_type_id) {
        Stock_picking_type domain = stock_picking_typeService.get(stock_picking_type_id);
        Stock_picking_typeDTO dto = stock_picking_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取拣货类型草稿", tags = {"拣货类型" },  notes = "获取拣货类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_picking_types/getdraft")
    public ResponseEntity<Stock_picking_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_picking_typeMapping.toDto(stock_picking_typeService.getDraft(new Stock_picking_type())));
    }

    @ApiOperation(value = "检查拣货类型", tags = {"拣货类型" },  notes = "检查拣货类型")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_picking_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_picking_typeDTO stock_picking_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_picking_typeService.checkKey(stock_picking_typeMapping.toDomain(stock_picking_typedto)));
    }

    @PreAuthorize("hasPermission(this.stock_picking_typeMapping.toDomain(#stock_picking_typedto),'iBizBusinessCentral-Stock_picking_type-Save')")
    @ApiOperation(value = "保存拣货类型", tags = {"拣货类型" },  notes = "保存拣货类型")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_picking_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_picking_typeDTO stock_picking_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_picking_typeService.save(stock_picking_typeMapping.toDomain(stock_picking_typedto)));
    }

    @PreAuthorize("hasPermission(this.stock_picking_typeMapping.toDomain(#stock_picking_typedtos),'iBizBusinessCentral-Stock_picking_type-Save')")
    @ApiOperation(value = "批量保存拣货类型", tags = {"拣货类型" },  notes = "批量保存拣货类型")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_picking_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_picking_typeDTO> stock_picking_typedtos) {
        stock_picking_typeService.saveBatch(stock_picking_typeMapping.toDomain(stock_picking_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_picking_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_picking_type-Get')")
	@ApiOperation(value = "获取数据集", tags = {"拣货类型" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_picking_types/fetchdefault")
	public ResponseEntity<List<Stock_picking_typeDTO>> fetchDefault(Stock_picking_typeSearchContext context) {
        Page<Stock_picking_type> domains = stock_picking_typeService.searchDefault(context) ;
        List<Stock_picking_typeDTO> list = stock_picking_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_picking_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_picking_type-Get')")
	@ApiOperation(value = "查询数据集", tags = {"拣货类型" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_picking_types/searchdefault")
	public ResponseEntity<Page<Stock_picking_typeDTO>> searchDefault(@RequestBody Stock_picking_typeSearchContext context) {
        Page<Stock_picking_type> domains = stock_picking_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_picking_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

