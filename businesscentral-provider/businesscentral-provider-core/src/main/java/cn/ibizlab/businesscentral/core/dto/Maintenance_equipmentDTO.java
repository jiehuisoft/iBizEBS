package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Maintenance_equipmentDTO]
 */
@Data
public class Maintenance_equipmentDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String activitySummary;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activityIds;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [EQUIPMENT_ASSIGN_TO]
     *
     */
    @JSONField(name = "equipment_assign_to")
    @JsonProperty("equipment_assign_to")
    @NotBlank(message = "[用于]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String equipmentAssignTo;

    /**
     * 属性 [LOCATION]
     *
     */
    @JSONField(name = "location")
    @JsonProperty("location")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String location;

    /**
     * 属性 [WARRANTY_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "warranty_date" , format="yyyy-MM-dd")
    @JsonProperty("warranty_date")
    private Timestamp warrantyDate;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[设备名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [MAINTENANCE_DURATION]
     *
     */
    @JSONField(name = "maintenance_duration")
    @JsonProperty("maintenance_duration")
    private Double maintenanceDuration;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [MODEL]
     *
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String model;

    /**
     * 属性 [MAINTENANCE_OPEN_COUNT]
     *
     */
    @JSONField(name = "maintenance_open_count")
    @JsonProperty("maintenance_open_count")
    private Integer maintenanceOpenCount;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityState;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [MAINTENANCE_COUNT]
     *
     */
    @JSONField(name = "maintenance_count")
    @JsonProperty("maintenance_count")
    private Integer maintenanceCount;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [NOTE]
     *
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String note;

    /**
     * 属性 [PARTNER_REF]
     *
     */
    @JSONField(name = "partner_ref")
    @JsonProperty("partner_ref")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String partnerRef;

    /**
     * 属性 [MAINTENANCE_IDS]
     *
     */
    @JSONField(name = "maintenance_ids")
    @JsonProperty("maintenance_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String maintenanceIds;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [SERIAL_NO]
     *
     */
    @JSONField(name = "serial_no")
    @JsonProperty("serial_no")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String serialNo;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [COST]
     *
     */
    @JSONField(name = "cost")
    @JsonProperty("cost")
    private Double cost;

    /**
     * 属性 [NEXT_ACTION_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "next_action_date" , format="yyyy-MM-dd")
    @JsonProperty("next_action_date")
    private Timestamp nextActionDate;

    /**
     * 属性 [ASSIGN_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "assign_date" , format="yyyy-MM-dd")
    @JsonProperty("assign_date")
    private Timestamp assignDate;

    /**
     * 属性 [EFFECTIVE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effective_date" , format="yyyy-MM-dd")
    @JsonProperty("effective_date")
    @NotNull(message = "[实际日期]不允许为空!")
    private Timestamp effectiveDate;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [SCRAP_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scrap_date" , format="yyyy-MM-dd")
    @JsonProperty("scrap_date")
    private Timestamp scrapDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [PERIOD]
     *
     */
    @JSONField(name = "period")
    @JsonProperty("period")
    private Integer period;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [DEPARTMENT_ID_TEXT]
     *
     */
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String departmentIdText;

    /**
     * 属性 [EMPLOYEE_ID_TEXT]
     *
     */
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String employeeIdText;

    /**
     * 属性 [TECHNICIAN_USER_ID_TEXT]
     *
     */
    @JSONField(name = "technician_user_id_text")
    @JsonProperty("technician_user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String technicianUserIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [CATEGORY_ID_TEXT]
     *
     */
    @JSONField(name = "category_id_text")
    @JsonProperty("category_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String categoryIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [OWNER_USER_ID_TEXT]
     *
     */
    @JSONField(name = "owner_user_id_text")
    @JsonProperty("owner_user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String ownerUserIdText;

    /**
     * 属性 [MAINTENANCE_TEAM_ID_TEXT]
     *
     */
    @JSONField(name = "maintenance_team_id_text")
    @JsonProperty("maintenance_team_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String maintenanceTeamIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [TECHNICIAN_USER_ID]
     *
     */
    @JSONField(name = "technician_user_id")
    @JsonProperty("technician_user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long technicianUserId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long categoryId;

    /**
     * 属性 [EMPLOYEE_ID]
     *
     */
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long employeeId;

    /**
     * 属性 [MAINTENANCE_TEAM_ID]
     *
     */
    @JSONField(name = "maintenance_team_id")
    @JsonProperty("maintenance_team_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long maintenanceTeamId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [OWNER_USER_ID]
     *
     */
    @JSONField(name = "owner_user_id")
    @JsonProperty("owner_user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long ownerUserId;

    /**
     * 属性 [DEPARTMENT_ID]
     *
     */
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long departmentId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [EQUIPMENT_ASSIGN_TO]
     */
    public void setEquipmentAssignTo(String  equipmentAssignTo){
        this.equipmentAssignTo = equipmentAssignTo ;
        this.modify("equipment_assign_to",equipmentAssignTo);
    }

    /**
     * 设置 [LOCATION]
     */
    public void setLocation(String  location){
        this.location = location ;
        this.modify("location",location);
    }

    /**
     * 设置 [WARRANTY_DATE]
     */
    public void setWarrantyDate(Timestamp  warrantyDate){
        this.warrantyDate = warrantyDate ;
        this.modify("warranty_date",warrantyDate);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [MAINTENANCE_DURATION]
     */
    public void setMaintenanceDuration(Double  maintenanceDuration){
        this.maintenanceDuration = maintenanceDuration ;
        this.modify("maintenance_duration",maintenanceDuration);
    }

    /**
     * 设置 [MODEL]
     */
    public void setModel(String  model){
        this.model = model ;
        this.modify("model",model);
    }

    /**
     * 设置 [MAINTENANCE_OPEN_COUNT]
     */
    public void setMaintenanceOpenCount(Integer  maintenanceOpenCount){
        this.maintenanceOpenCount = maintenanceOpenCount ;
        this.modify("maintenance_open_count",maintenanceOpenCount);
    }

    /**
     * 设置 [MAINTENANCE_COUNT]
     */
    public void setMaintenanceCount(Integer  maintenanceCount){
        this.maintenanceCount = maintenanceCount ;
        this.modify("maintenance_count",maintenanceCount);
    }

    /**
     * 设置 [NOTE]
     */
    public void setNote(String  note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [PARTNER_REF]
     */
    public void setPartnerRef(String  partnerRef){
        this.partnerRef = partnerRef ;
        this.modify("partner_ref",partnerRef);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [SERIAL_NO]
     */
    public void setSerialNo(String  serialNo){
        this.serialNo = serialNo ;
        this.modify("serial_no",serialNo);
    }

    /**
     * 设置 [COST]
     */
    public void setCost(Double  cost){
        this.cost = cost ;
        this.modify("cost",cost);
    }

    /**
     * 设置 [NEXT_ACTION_DATE]
     */
    public void setNextActionDate(Timestamp  nextActionDate){
        this.nextActionDate = nextActionDate ;
        this.modify("next_action_date",nextActionDate);
    }

    /**
     * 设置 [ASSIGN_DATE]
     */
    public void setAssignDate(Timestamp  assignDate){
        this.assignDate = assignDate ;
        this.modify("assign_date",assignDate);
    }

    /**
     * 设置 [EFFECTIVE_DATE]
     */
    public void setEffectiveDate(Timestamp  effectiveDate){
        this.effectiveDate = effectiveDate ;
        this.modify("effective_date",effectiveDate);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [SCRAP_DATE]
     */
    public void setScrapDate(Timestamp  scrapDate){
        this.scrapDate = scrapDate ;
        this.modify("scrap_date",scrapDate);
    }

    /**
     * 设置 [PERIOD]
     */
    public void setPeriod(Integer  period){
        this.period = period ;
        this.modify("period",period);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [TECHNICIAN_USER_ID]
     */
    public void setTechnicianUserId(Long  technicianUserId){
        this.technicianUserId = technicianUserId ;
        this.modify("technician_user_id",technicianUserId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    public void setCategoryId(Long  categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }

    /**
     * 设置 [EMPLOYEE_ID]
     */
    public void setEmployeeId(Long  employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }

    /**
     * 设置 [MAINTENANCE_TEAM_ID]
     */
    public void setMaintenanceTeamId(Long  maintenanceTeamId){
        this.maintenanceTeamId = maintenanceTeamId ;
        this.modify("maintenance_team_id",maintenanceTeamId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [OWNER_USER_ID]
     */
    public void setOwnerUserId(Long  ownerUserId){
        this.ownerUserId = ownerUserId ;
        this.modify("owner_user_id",ownerUserId);
    }

    /**
     * 设置 [DEPARTMENT_ID]
     */
    public void setDepartmentId(Long  departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }


}


