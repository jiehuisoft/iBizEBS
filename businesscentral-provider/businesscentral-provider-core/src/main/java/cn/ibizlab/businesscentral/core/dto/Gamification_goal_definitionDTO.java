package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Gamification_goal_definitionDTO]
 */
@Data
public class Gamification_goal_definitionDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SUFFIX]
     *
     */
    @JSONField(name = "suffix")
    @JsonProperty("suffix")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String suffix;

    /**
     * 属性 [BATCH_DISTINCTIVE_FIELD]
     *
     */
    @JSONField(name = "batch_distinctive_field")
    @JsonProperty("batch_distinctive_field")
    private Integer batchDistinctiveField;

    /**
     * 属性 [MONETARY]
     *
     */
    @JSONField(name = "monetary")
    @JsonProperty("monetary")
    private Boolean monetary;

    /**
     * 属性 [CONDITION]
     *
     */
    @JSONField(name = "condition")
    @JsonProperty("condition")
    @NotBlank(message = "[目标绩效]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String condition;

    /**
     * 属性 [COMPUTE_CODE]
     *
     */
    @JSONField(name = "compute_code")
    @JsonProperty("compute_code")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String computeCode;

    /**
     * 属性 [COMPUTATION_MODE]
     *
     */
    @JSONField(name = "computation_mode")
    @JsonProperty("computation_mode")
    @NotBlank(message = "[计算模式]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String computationMode;

    /**
     * 属性 [ACTION_ID]
     *
     */
    @JSONField(name = "action_id")
    @JsonProperty("action_id")
    private Integer actionId;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DOMAIN]
     *
     */
    @JSONField(name = "domain")
    @JsonProperty("domain")
    @NotBlank(message = "[筛选域]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String domain;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [FIELD_ID]
     *
     */
    @JSONField(name = "field_id")
    @JsonProperty("field_id")
    private Integer fieldId;

    /**
     * 属性 [RES_ID_FIELD]
     *
     */
    @JSONField(name = "res_id_field")
    @JsonProperty("res_id_field")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String resIdField;

    /**
     * 属性 [BATCH_MODE]
     *
     */
    @JSONField(name = "batch_mode")
    @JsonProperty("batch_mode")
    private Boolean batchMode;

    /**
     * 属性 [MODEL_ID]
     *
     */
    @JSONField(name = "model_id")
    @JsonProperty("model_id")
    private Integer modelId;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String description;

    /**
     * 属性 [FIELD_DATE_ID]
     *
     */
    @JSONField(name = "field_date_id")
    @JsonProperty("field_date_id")
    private Integer fieldDateId;

    /**
     * 属性 [DISPLAY_MODE]
     *
     */
    @JSONField(name = "display_mode")
    @JsonProperty("display_mode")
    @NotBlank(message = "[显示为]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String displayMode;

    /**
     * 属性 [FULL_SUFFIX]
     *
     */
    @JSONField(name = "full_suffix")
    @JsonProperty("full_suffix")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String fullSuffix;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [BATCH_USER_EXPRESSION]
     *
     */
    @JSONField(name = "batch_user_expression")
    @JsonProperty("batch_user_expression")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String batchUserExpression;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[目标定义]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [SUFFIX]
     */
    public void setSuffix(String  suffix){
        this.suffix = suffix ;
        this.modify("suffix",suffix);
    }

    /**
     * 设置 [BATCH_DISTINCTIVE_FIELD]
     */
    public void setBatchDistinctiveField(Integer  batchDistinctiveField){
        this.batchDistinctiveField = batchDistinctiveField ;
        this.modify("batch_distinctive_field",batchDistinctiveField);
    }

    /**
     * 设置 [MONETARY]
     */
    public void setMonetary(Boolean  monetary){
        this.monetary = monetary ;
        this.modify("monetary",monetary);
    }

    /**
     * 设置 [CONDITION]
     */
    public void setCondition(String  condition){
        this.condition = condition ;
        this.modify("condition",condition);
    }

    /**
     * 设置 [COMPUTE_CODE]
     */
    public void setComputeCode(String  computeCode){
        this.computeCode = computeCode ;
        this.modify("compute_code",computeCode);
    }

    /**
     * 设置 [COMPUTATION_MODE]
     */
    public void setComputationMode(String  computationMode){
        this.computationMode = computationMode ;
        this.modify("computation_mode",computationMode);
    }

    /**
     * 设置 [ACTION_ID]
     */
    public void setActionId(Integer  actionId){
        this.actionId = actionId ;
        this.modify("action_id",actionId);
    }

    /**
     * 设置 [DOMAIN]
     */
    public void setDomain(String  domain){
        this.domain = domain ;
        this.modify("domain",domain);
    }

    /**
     * 设置 [FIELD_ID]
     */
    public void setFieldId(Integer  fieldId){
        this.fieldId = fieldId ;
        this.modify("field_id",fieldId);
    }

    /**
     * 设置 [RES_ID_FIELD]
     */
    public void setResIdField(String  resIdField){
        this.resIdField = resIdField ;
        this.modify("res_id_field",resIdField);
    }

    /**
     * 设置 [BATCH_MODE]
     */
    public void setBatchMode(Boolean  batchMode){
        this.batchMode = batchMode ;
        this.modify("batch_mode",batchMode);
    }

    /**
     * 设置 [MODEL_ID]
     */
    public void setModelId(Integer  modelId){
        this.modelId = modelId ;
        this.modify("model_id",modelId);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [FIELD_DATE_ID]
     */
    public void setFieldDateId(Integer  fieldDateId){
        this.fieldDateId = fieldDateId ;
        this.modify("field_date_id",fieldDateId);
    }

    /**
     * 设置 [DISPLAY_MODE]
     */
    public void setDisplayMode(String  displayMode){
        this.displayMode = displayMode ;
        this.modify("display_mode",displayMode);
    }

    /**
     * 设置 [BATCH_USER_EXPRESSION]
     */
    public void setBatchUserExpression(String  batchUserExpression){
        this.batchUserExpression = batchUserExpression ;
        this.modify("batch_user_expression",batchUserExpression);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }


}


