package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty_repair;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qty_repairService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warn_insufficient_qty_repairSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"警告维修数量不足" })
@RestController("Core-stock_warn_insufficient_qty_repair")
@RequestMapping("")
public class Stock_warn_insufficient_qty_repairResource {

    @Autowired
    public IStock_warn_insufficient_qty_repairService stock_warn_insufficient_qty_repairService;

    @Autowired
    @Lazy
    public Stock_warn_insufficient_qty_repairMapping stock_warn_insufficient_qty_repairMapping;

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_repairMapping.toDomain(#stock_warn_insufficient_qty_repairdto),'iBizBusinessCentral-Stock_warn_insufficient_qty_repair-Create')")
    @ApiOperation(value = "新建警告维修数量不足", tags = {"警告维修数量不足" },  notes = "新建警告维修数量不足")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_repairs")
    public ResponseEntity<Stock_warn_insufficient_qty_repairDTO> create(@Validated @RequestBody Stock_warn_insufficient_qty_repairDTO stock_warn_insufficient_qty_repairdto) {
        Stock_warn_insufficient_qty_repair domain = stock_warn_insufficient_qty_repairMapping.toDomain(stock_warn_insufficient_qty_repairdto);
		stock_warn_insufficient_qty_repairService.create(domain);
        Stock_warn_insufficient_qty_repairDTO dto = stock_warn_insufficient_qty_repairMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_repairMapping.toDomain(#stock_warn_insufficient_qty_repairdtos),'iBizBusinessCentral-Stock_warn_insufficient_qty_repair-Create')")
    @ApiOperation(value = "批量新建警告维修数量不足", tags = {"警告维修数量不足" },  notes = "批量新建警告维修数量不足")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_repairs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_warn_insufficient_qty_repairDTO> stock_warn_insufficient_qty_repairdtos) {
        stock_warn_insufficient_qty_repairService.createBatch(stock_warn_insufficient_qty_repairMapping.toDomain(stock_warn_insufficient_qty_repairdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_warn_insufficient_qty_repair" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_repairService.get(#stock_warn_insufficient_qty_repair_id),'iBizBusinessCentral-Stock_warn_insufficient_qty_repair-Update')")
    @ApiOperation(value = "更新警告维修数量不足", tags = {"警告维修数量不足" },  notes = "更新警告维修数量不足")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_repairs/{stock_warn_insufficient_qty_repair_id}")
    public ResponseEntity<Stock_warn_insufficient_qty_repairDTO> update(@PathVariable("stock_warn_insufficient_qty_repair_id") Long stock_warn_insufficient_qty_repair_id, @RequestBody Stock_warn_insufficient_qty_repairDTO stock_warn_insufficient_qty_repairdto) {
		Stock_warn_insufficient_qty_repair domain  = stock_warn_insufficient_qty_repairMapping.toDomain(stock_warn_insufficient_qty_repairdto);
        domain .setId(stock_warn_insufficient_qty_repair_id);
		stock_warn_insufficient_qty_repairService.update(domain );
		Stock_warn_insufficient_qty_repairDTO dto = stock_warn_insufficient_qty_repairMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_repairService.getStockWarnInsufficientQtyRepairByEntities(this.stock_warn_insufficient_qty_repairMapping.toDomain(#stock_warn_insufficient_qty_repairdtos)),'iBizBusinessCentral-Stock_warn_insufficient_qty_repair-Update')")
    @ApiOperation(value = "批量更新警告维修数量不足", tags = {"警告维修数量不足" },  notes = "批量更新警告维修数量不足")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_repairs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warn_insufficient_qty_repairDTO> stock_warn_insufficient_qty_repairdtos) {
        stock_warn_insufficient_qty_repairService.updateBatch(stock_warn_insufficient_qty_repairMapping.toDomain(stock_warn_insufficient_qty_repairdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_repairService.get(#stock_warn_insufficient_qty_repair_id),'iBizBusinessCentral-Stock_warn_insufficient_qty_repair-Remove')")
    @ApiOperation(value = "删除警告维修数量不足", tags = {"警告维修数量不足" },  notes = "删除警告维修数量不足")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_repairs/{stock_warn_insufficient_qty_repair_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_warn_insufficient_qty_repair_id") Long stock_warn_insufficient_qty_repair_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qty_repairService.remove(stock_warn_insufficient_qty_repair_id));
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_repairService.getStockWarnInsufficientQtyRepairByIds(#ids),'iBizBusinessCentral-Stock_warn_insufficient_qty_repair-Remove')")
    @ApiOperation(value = "批量删除警告维修数量不足", tags = {"警告维修数量不足" },  notes = "批量删除警告维修数量不足")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_repairs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_warn_insufficient_qty_repairService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_warn_insufficient_qty_repairMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_warn_insufficient_qty_repair-Get')")
    @ApiOperation(value = "获取警告维修数量不足", tags = {"警告维修数量不足" },  notes = "获取警告维修数量不足")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_repairs/{stock_warn_insufficient_qty_repair_id}")
    public ResponseEntity<Stock_warn_insufficient_qty_repairDTO> get(@PathVariable("stock_warn_insufficient_qty_repair_id") Long stock_warn_insufficient_qty_repair_id) {
        Stock_warn_insufficient_qty_repair domain = stock_warn_insufficient_qty_repairService.get(stock_warn_insufficient_qty_repair_id);
        Stock_warn_insufficient_qty_repairDTO dto = stock_warn_insufficient_qty_repairMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取警告维修数量不足草稿", tags = {"警告维修数量不足" },  notes = "获取警告维修数量不足草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_repairs/getdraft")
    public ResponseEntity<Stock_warn_insufficient_qty_repairDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qty_repairMapping.toDto(stock_warn_insufficient_qty_repairService.getDraft(new Stock_warn_insufficient_qty_repair())));
    }

    @ApiOperation(value = "检查警告维修数量不足", tags = {"警告维修数量不足" },  notes = "检查警告维修数量不足")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_repairs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_warn_insufficient_qty_repairDTO stock_warn_insufficient_qty_repairdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qty_repairService.checkKey(stock_warn_insufficient_qty_repairMapping.toDomain(stock_warn_insufficient_qty_repairdto)));
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_repairMapping.toDomain(#stock_warn_insufficient_qty_repairdto),'iBizBusinessCentral-Stock_warn_insufficient_qty_repair-Save')")
    @ApiOperation(value = "保存警告维修数量不足", tags = {"警告维修数量不足" },  notes = "保存警告维修数量不足")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_repairs/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_warn_insufficient_qty_repairDTO stock_warn_insufficient_qty_repairdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qty_repairService.save(stock_warn_insufficient_qty_repairMapping.toDomain(stock_warn_insufficient_qty_repairdto)));
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_repairMapping.toDomain(#stock_warn_insufficient_qty_repairdtos),'iBizBusinessCentral-Stock_warn_insufficient_qty_repair-Save')")
    @ApiOperation(value = "批量保存警告维修数量不足", tags = {"警告维修数量不足" },  notes = "批量保存警告维修数量不足")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_repairs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_warn_insufficient_qty_repairDTO> stock_warn_insufficient_qty_repairdtos) {
        stock_warn_insufficient_qty_repairService.saveBatch(stock_warn_insufficient_qty_repairMapping.toDomain(stock_warn_insufficient_qty_repairdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty_repair-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_warn_insufficient_qty_repair-Get')")
	@ApiOperation(value = "获取数据集", tags = {"警告维修数量不足" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_warn_insufficient_qty_repairs/fetchdefault")
	public ResponseEntity<List<Stock_warn_insufficient_qty_repairDTO>> fetchDefault(Stock_warn_insufficient_qty_repairSearchContext context) {
        Page<Stock_warn_insufficient_qty_repair> domains = stock_warn_insufficient_qty_repairService.searchDefault(context) ;
        List<Stock_warn_insufficient_qty_repairDTO> list = stock_warn_insufficient_qty_repairMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty_repair-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_warn_insufficient_qty_repair-Get')")
	@ApiOperation(value = "查询数据集", tags = {"警告维修数量不足" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_warn_insufficient_qty_repairs/searchdefault")
	public ResponseEntity<Page<Stock_warn_insufficient_qty_repairDTO>> searchDefault(@RequestBody Stock_warn_insufficient_qty_repairSearchContext context) {
        Page<Stock_warn_insufficient_qty_repair> domains = stock_warn_insufficient_qty_repairService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_warn_insufficient_qty_repairMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

