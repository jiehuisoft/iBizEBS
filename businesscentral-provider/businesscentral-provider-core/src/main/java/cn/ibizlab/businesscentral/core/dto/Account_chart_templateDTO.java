package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Account_chart_templateDTO]
 */
@Data
public class Account_chart_templateDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CODE_DIGITS]
     *
     */
    @JSONField(name = "code_digits")
    @JsonProperty("code_digits")
    @NotNull(message = "[# 数字]不允许为空!")
    private Integer codeDigits;

    /**
     * 属性 [TRANSFER_ACCOUNT_CODE_PREFIX]
     *
     */
    @JSONField(name = "transfer_account_code_prefix")
    @JsonProperty("transfer_account_code_prefix")
    @NotBlank(message = "[主转账帐户的前缀]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String transferAccountCodePrefix;

    /**
     * 属性 [BANK_ACCOUNT_CODE_PREFIX]
     *
     */
    @JSONField(name = "bank_account_code_prefix")
    @JsonProperty("bank_account_code_prefix")
    @NotBlank(message = "[银行科目的前缀]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String bankAccountCodePrefix;

    /**
     * 属性 [SPOKEN_LANGUAGES]
     *
     */
    @JSONField(name = "spoken_languages")
    @JsonProperty("spoken_languages")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String spokenLanguages;

    /**
     * 属性 [TAX_TEMPLATE_IDS]
     *
     */
    @JSONField(name = "tax_template_ids")
    @JsonProperty("tax_template_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String taxTemplateIds;

    /**
     * 属性 [VISIBLE]
     *
     */
    @JSONField(name = "visible")
    @JsonProperty("visible")
    private Boolean visible;

    /**
     * 属性 [ACCOUNT_IDS]
     *
     */
    @JSONField(name = "account_ids")
    @JsonProperty("account_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String accountIds;

    /**
     * 属性 [COMPLETE_TAX_SET]
     *
     */
    @JSONField(name = "complete_tax_set")
    @JsonProperty("complete_tax_set")
    private Boolean completeTaxSet;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CASH_ACCOUNT_CODE_PREFIX]
     *
     */
    @JSONField(name = "cash_account_code_prefix")
    @JsonProperty("cash_account_code_prefix")
    @NotBlank(message = "[主现金科目的前缀]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String cashAccountCodePrefix;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [USE_ANGLO_SAXON]
     *
     */
    @JSONField(name = "use_anglo_saxon")
    @JsonProperty("use_anglo_saxon")
    private Boolean useAngloSaxon;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "expense_currency_exchange_account_id_text")
    @JsonProperty("expense_currency_exchange_account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String expenseCurrencyExchangeAccountIdText;

    /**
     * 属性 [PROPERTY_ACCOUNT_RECEIVABLE_ID_TEXT]
     *
     */
    @JSONField(name = "property_account_receivable_id_text")
    @JsonProperty("property_account_receivable_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propertyAccountReceivableIdText;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currencyIdText;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_ID_TEXT]
     *
     */
    @JSONField(name = "property_account_expense_id_text")
    @JsonProperty("property_account_expense_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propertyAccountExpenseIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String parentIdText;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID_TEXT]
     *
     */
    @JSONField(name = "property_stock_account_input_categ_id_text")
    @JsonProperty("property_stock_account_input_categ_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propertyStockAccountInputCategIdText;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_ID_TEXT]
     *
     */
    @JSONField(name = "property_account_income_id_text")
    @JsonProperty("property_account_income_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propertyAccountIncomeIdText;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID_TEXT]
     *
     */
    @JSONField(name = "property_account_expense_categ_id_text")
    @JsonProperty("property_account_expense_categ_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propertyAccountExpenseCategIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "income_currency_exchange_account_id_text")
    @JsonProperty("income_currency_exchange_account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String incomeCurrencyExchangeAccountIdText;

    /**
     * 属性 [PROPERTY_ACCOUNT_PAYABLE_ID_TEXT]
     *
     */
    @JSONField(name = "property_account_payable_id_text")
    @JsonProperty("property_account_payable_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propertyAccountPayableIdText;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_CATEG_ID_TEXT]
     *
     */
    @JSONField(name = "property_account_income_categ_id_text")
    @JsonProperty("property_account_income_categ_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propertyAccountIncomeCategIdText;

    /**
     * 属性 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "property_stock_valuation_account_id_text")
    @JsonProperty("property_stock_valuation_account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propertyStockValuationAccountIdText;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID_TEXT]
     *
     */
    @JSONField(name = "property_stock_account_output_categ_id_text")
    @JsonProperty("property_stock_account_output_categ_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propertyStockAccountOutputCategIdText;

    /**
     * 属性 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     *
     */
    @JSONField(name = "property_stock_valuation_account_id")
    @JsonProperty("property_stock_valuation_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyStockValuationAccountId;

    /**
     * 属性 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID]
     *
     */
    @JSONField(name = "expense_currency_exchange_account_id")
    @JsonProperty("expense_currency_exchange_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long expenseCurrencyExchangeAccountId;

    /**
     * 属性 [PROPERTY_ACCOUNT_PAYABLE_ID]
     *
     */
    @JSONField(name = "property_account_payable_id")
    @JsonProperty("property_account_payable_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyAccountPayableId;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_CATEG_ID]
     *
     */
    @JSONField(name = "property_account_income_categ_id")
    @JsonProperty("property_account_income_categ_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyAccountIncomeCategId;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     *
     */
    @JSONField(name = "property_stock_account_output_categ_id")
    @JsonProperty("property_stock_account_output_categ_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyStockAccountOutputCategId;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_ID]
     *
     */
    @JSONField(name = "property_account_expense_id")
    @JsonProperty("property_account_expense_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyAccountExpenseId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[币种]不允许为空!")
    private Long currencyId;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /**
     * 属性 [PROPERTY_ACCOUNT_RECEIVABLE_ID]
     *
     */
    @JSONField(name = "property_account_receivable_id")
    @JsonProperty("property_account_receivable_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyAccountReceivableId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID]
     *
     */
    @JSONField(name = "property_account_expense_categ_id")
    @JsonProperty("property_account_expense_categ_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyAccountExpenseCategId;

    /**
     * 属性 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID]
     *
     */
    @JSONField(name = "income_currency_exchange_account_id")
    @JsonProperty("income_currency_exchange_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long incomeCurrencyExchangeAccountId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     *
     */
    @JSONField(name = "property_stock_account_input_categ_id")
    @JsonProperty("property_stock_account_input_categ_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyStockAccountInputCategId;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_ID]
     *
     */
    @JSONField(name = "property_account_income_id")
    @JsonProperty("property_account_income_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propertyAccountIncomeId;


    /**
     * 设置 [CODE_DIGITS]
     */
    public void setCodeDigits(Integer  codeDigits){
        this.codeDigits = codeDigits ;
        this.modify("code_digits",codeDigits);
    }

    /**
     * 设置 [TRANSFER_ACCOUNT_CODE_PREFIX]
     */
    public void setTransferAccountCodePrefix(String  transferAccountCodePrefix){
        this.transferAccountCodePrefix = transferAccountCodePrefix ;
        this.modify("transfer_account_code_prefix",transferAccountCodePrefix);
    }

    /**
     * 设置 [BANK_ACCOUNT_CODE_PREFIX]
     */
    public void setBankAccountCodePrefix(String  bankAccountCodePrefix){
        this.bankAccountCodePrefix = bankAccountCodePrefix ;
        this.modify("bank_account_code_prefix",bankAccountCodePrefix);
    }

    /**
     * 设置 [SPOKEN_LANGUAGES]
     */
    public void setSpokenLanguages(String  spokenLanguages){
        this.spokenLanguages = spokenLanguages ;
        this.modify("spoken_languages",spokenLanguages);
    }

    /**
     * 设置 [VISIBLE]
     */
    public void setVisible(Boolean  visible){
        this.visible = visible ;
        this.modify("visible",visible);
    }

    /**
     * 设置 [COMPLETE_TAX_SET]
     */
    public void setCompleteTaxSet(Boolean  completeTaxSet){
        this.completeTaxSet = completeTaxSet ;
        this.modify("complete_tax_set",completeTaxSet);
    }

    /**
     * 设置 [CASH_ACCOUNT_CODE_PREFIX]
     */
    public void setCashAccountCodePrefix(String  cashAccountCodePrefix){
        this.cashAccountCodePrefix = cashAccountCodePrefix ;
        this.modify("cash_account_code_prefix",cashAccountCodePrefix);
    }

    /**
     * 设置 [USE_ANGLO_SAXON]
     */
    public void setUseAngloSaxon(Boolean  useAngloSaxon){
        this.useAngloSaxon = useAngloSaxon ;
        this.modify("use_anglo_saxon",useAngloSaxon);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     */
    public void setPropertyStockValuationAccountId(Long  propertyStockValuationAccountId){
        this.propertyStockValuationAccountId = propertyStockValuationAccountId ;
        this.modify("property_stock_valuation_account_id",propertyStockValuationAccountId);
    }

    /**
     * 设置 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID]
     */
    public void setExpenseCurrencyExchangeAccountId(Long  expenseCurrencyExchangeAccountId){
        this.expenseCurrencyExchangeAccountId = expenseCurrencyExchangeAccountId ;
        this.modify("expense_currency_exchange_account_id",expenseCurrencyExchangeAccountId);
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_PAYABLE_ID]
     */
    public void setPropertyAccountPayableId(Long  propertyAccountPayableId){
        this.propertyAccountPayableId = propertyAccountPayableId ;
        this.modify("property_account_payable_id",propertyAccountPayableId);
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_INCOME_CATEG_ID]
     */
    public void setPropertyAccountIncomeCategId(Long  propertyAccountIncomeCategId){
        this.propertyAccountIncomeCategId = propertyAccountIncomeCategId ;
        this.modify("property_account_income_categ_id",propertyAccountIncomeCategId);
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     */
    public void setPropertyStockAccountOutputCategId(Long  propertyStockAccountOutputCategId){
        this.propertyStockAccountOutputCategId = propertyStockAccountOutputCategId ;
        this.modify("property_stock_account_output_categ_id",propertyStockAccountOutputCategId);
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_EXPENSE_ID]
     */
    public void setPropertyAccountExpenseId(Long  propertyAccountExpenseId){
        this.propertyAccountExpenseId = propertyAccountExpenseId ;
        this.modify("property_account_expense_id",propertyAccountExpenseId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [PARENT_ID]
     */
    public void setParentId(Long  parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_RECEIVABLE_ID]
     */
    public void setPropertyAccountReceivableId(Long  propertyAccountReceivableId){
        this.propertyAccountReceivableId = propertyAccountReceivableId ;
        this.modify("property_account_receivable_id",propertyAccountReceivableId);
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID]
     */
    public void setPropertyAccountExpenseCategId(Long  propertyAccountExpenseCategId){
        this.propertyAccountExpenseCategId = propertyAccountExpenseCategId ;
        this.modify("property_account_expense_categ_id",propertyAccountExpenseCategId);
    }

    /**
     * 设置 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID]
     */
    public void setIncomeCurrencyExchangeAccountId(Long  incomeCurrencyExchangeAccountId){
        this.incomeCurrencyExchangeAccountId = incomeCurrencyExchangeAccountId ;
        this.modify("income_currency_exchange_account_id",incomeCurrencyExchangeAccountId);
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     */
    public void setPropertyStockAccountInputCategId(Long  propertyStockAccountInputCategId){
        this.propertyStockAccountInputCategId = propertyStockAccountInputCategId ;
        this.modify("property_stock_account_input_categ_id",propertyStockAccountInputCategId);
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_INCOME_ID]
     */
    public void setPropertyAccountIncomeId(Long  propertyAccountIncomeId){
        this.propertyAccountIncomeId = propertyAccountIncomeId ;
        this.modify("property_account_income_id",propertyAccountIncomeId);
    }


}


