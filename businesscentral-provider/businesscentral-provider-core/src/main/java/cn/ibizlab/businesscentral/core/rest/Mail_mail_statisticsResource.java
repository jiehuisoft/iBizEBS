package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mail_statistics;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mail_statisticsService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mail_statisticsSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"邮件统计" })
@RestController("Core-mail_mail_statistics")
@RequestMapping("")
public class Mail_mail_statisticsResource {

    @Autowired
    public IMail_mail_statisticsService mail_mail_statisticsService;

    @Autowired
    @Lazy
    public Mail_mail_statisticsMapping mail_mail_statisticsMapping;

    @PreAuthorize("hasPermission(this.mail_mail_statisticsMapping.toDomain(#mail_mail_statisticsdto),'iBizBusinessCentral-Mail_mail_statistics-Create')")
    @ApiOperation(value = "新建邮件统计", tags = {"邮件统计" },  notes = "新建邮件统计")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics")
    public ResponseEntity<Mail_mail_statisticsDTO> create(@Validated @RequestBody Mail_mail_statisticsDTO mail_mail_statisticsdto) {
        Mail_mail_statistics domain = mail_mail_statisticsMapping.toDomain(mail_mail_statisticsdto);
		mail_mail_statisticsService.create(domain);
        Mail_mail_statisticsDTO dto = mail_mail_statisticsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mail_statisticsMapping.toDomain(#mail_mail_statisticsdtos),'iBizBusinessCentral-Mail_mail_statistics-Create')")
    @ApiOperation(value = "批量新建邮件统计", tags = {"邮件统计" },  notes = "批量新建邮件统计")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mail_statisticsDTO> mail_mail_statisticsdtos) {
        mail_mail_statisticsService.createBatch(mail_mail_statisticsMapping.toDomain(mail_mail_statisticsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_mail_statistics" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_mail_statisticsService.get(#mail_mail_statistics_id),'iBizBusinessCentral-Mail_mail_statistics-Update')")
    @ApiOperation(value = "更新邮件统计", tags = {"邮件统计" },  notes = "更新邮件统计")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mail_statistics/{mail_mail_statistics_id}")
    public ResponseEntity<Mail_mail_statisticsDTO> update(@PathVariable("mail_mail_statistics_id") Long mail_mail_statistics_id, @RequestBody Mail_mail_statisticsDTO mail_mail_statisticsdto) {
		Mail_mail_statistics domain  = mail_mail_statisticsMapping.toDomain(mail_mail_statisticsdto);
        domain .setId(mail_mail_statistics_id);
		mail_mail_statisticsService.update(domain );
		Mail_mail_statisticsDTO dto = mail_mail_statisticsMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mail_statisticsService.getMailMailStatisticsByEntities(this.mail_mail_statisticsMapping.toDomain(#mail_mail_statisticsdtos)),'iBizBusinessCentral-Mail_mail_statistics-Update')")
    @ApiOperation(value = "批量更新邮件统计", tags = {"邮件统计" },  notes = "批量更新邮件统计")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mail_statistics/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mail_statisticsDTO> mail_mail_statisticsdtos) {
        mail_mail_statisticsService.updateBatch(mail_mail_statisticsMapping.toDomain(mail_mail_statisticsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_mail_statisticsService.get(#mail_mail_statistics_id),'iBizBusinessCentral-Mail_mail_statistics-Remove')")
    @ApiOperation(value = "删除邮件统计", tags = {"邮件统计" },  notes = "删除邮件统计")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mail_statistics/{mail_mail_statistics_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_mail_statistics_id") Long mail_mail_statistics_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mail_statisticsService.remove(mail_mail_statistics_id));
    }

    @PreAuthorize("hasPermission(this.mail_mail_statisticsService.getMailMailStatisticsByIds(#ids),'iBizBusinessCentral-Mail_mail_statistics-Remove')")
    @ApiOperation(value = "批量删除邮件统计", tags = {"邮件统计" },  notes = "批量删除邮件统计")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mail_statistics/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_mail_statisticsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_mail_statisticsMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_mail_statistics-Get')")
    @ApiOperation(value = "获取邮件统计", tags = {"邮件统计" },  notes = "获取邮件统计")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mail_statistics/{mail_mail_statistics_id}")
    public ResponseEntity<Mail_mail_statisticsDTO> get(@PathVariable("mail_mail_statistics_id") Long mail_mail_statistics_id) {
        Mail_mail_statistics domain = mail_mail_statisticsService.get(mail_mail_statistics_id);
        Mail_mail_statisticsDTO dto = mail_mail_statisticsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取邮件统计草稿", tags = {"邮件统计" },  notes = "获取邮件统计草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mail_statistics/getdraft")
    public ResponseEntity<Mail_mail_statisticsDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mail_statisticsMapping.toDto(mail_mail_statisticsService.getDraft(new Mail_mail_statistics())));
    }

    @ApiOperation(value = "检查邮件统计", tags = {"邮件统计" },  notes = "检查邮件统计")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_mail_statisticsDTO mail_mail_statisticsdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_mail_statisticsService.checkKey(mail_mail_statisticsMapping.toDomain(mail_mail_statisticsdto)));
    }

    @PreAuthorize("hasPermission(this.mail_mail_statisticsMapping.toDomain(#mail_mail_statisticsdto),'iBizBusinessCentral-Mail_mail_statistics-Save')")
    @ApiOperation(value = "保存邮件统计", tags = {"邮件统计" },  notes = "保存邮件统计")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_mail_statisticsDTO mail_mail_statisticsdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mail_statisticsService.save(mail_mail_statisticsMapping.toDomain(mail_mail_statisticsdto)));
    }

    @PreAuthorize("hasPermission(this.mail_mail_statisticsMapping.toDomain(#mail_mail_statisticsdtos),'iBizBusinessCentral-Mail_mail_statistics-Save')")
    @ApiOperation(value = "批量保存邮件统计", tags = {"邮件统计" },  notes = "批量保存邮件统计")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_mail_statisticsDTO> mail_mail_statisticsdtos) {
        mail_mail_statisticsService.saveBatch(mail_mail_statisticsMapping.toDomain(mail_mail_statisticsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mail_statistics-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mail_statistics-Get')")
	@ApiOperation(value = "获取数据集", tags = {"邮件统计" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mail_statistics/fetchdefault")
	public ResponseEntity<List<Mail_mail_statisticsDTO>> fetchDefault(Mail_mail_statisticsSearchContext context) {
        Page<Mail_mail_statistics> domains = mail_mail_statisticsService.searchDefault(context) ;
        List<Mail_mail_statisticsDTO> list = mail_mail_statisticsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mail_statistics-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mail_statistics-Get')")
	@ApiOperation(value = "查询数据集", tags = {"邮件统计" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_mail_statistics/searchdefault")
	public ResponseEntity<Page<Mail_mail_statisticsDTO>> searchDefault(@RequestBody Mail_mail_statisticsSearchContext context) {
        Page<Mail_mail_statistics> domains = mail_mail_statisticsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mail_statisticsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

