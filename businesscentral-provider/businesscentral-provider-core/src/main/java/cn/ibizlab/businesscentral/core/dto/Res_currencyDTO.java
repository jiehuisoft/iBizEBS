package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Res_currencyDTO]
 */
@Data
public class Res_currencyDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DECIMAL_PLACES]
     *
     */
    @JSONField(name = "decimal_places")
    @JsonProperty("decimal_places")
    private Integer decimalPlaces;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[币种]不允许为空!")
    @Size(min = 0, max = 3, message = "内容长度必须小于等于[3]")
    private String name;

    /**
     * 属性 [CURRENCY_UNIT_LABEL]
     *
     */
    @JSONField(name = "currency_unit_label")
    @JsonProperty("currency_unit_label")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String currencyUnitLabel;

    /**
     * 属性 [ROUNDING]
     *
     */
    @JSONField(name = "rounding")
    @JsonProperty("rounding")
    private Double rounding;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [POSITION]
     *
     */
    @JSONField(name = "position")
    @JsonProperty("position")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String position;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [RATE]
     *
     */
    @JSONField(name = "rate")
    @JsonProperty("rate")
    private Double rate;

    /**
     * 属性 [RATE_IDS]
     *
     */
    @JSONField(name = "rate_ids")
    @JsonProperty("rate_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String rateIds;

    /**
     * 属性 [SYMBOL]
     *
     */
    @JSONField(name = "symbol")
    @JsonProperty("symbol")
    @NotBlank(message = "[货币符号]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String symbol;

    /**
     * 属性 [CURRENCY_SUBUNIT_LABEL]
     *
     */
    @JSONField(name = "currency_subunit_label")
    @JsonProperty("currency_subunit_label")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String currencySubunitLabel;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [DECIMAL_PLACES]
     */
    public void setDecimalPlaces(Integer  decimalPlaces){
        this.decimalPlaces = decimalPlaces ;
        this.modify("decimal_places",decimalPlaces);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [CURRENCY_UNIT_LABEL]
     */
    public void setCurrencyUnitLabel(String  currencyUnitLabel){
        this.currencyUnitLabel = currencyUnitLabel ;
        this.modify("currency_unit_label",currencyUnitLabel);
    }

    /**
     * 设置 [ROUNDING]
     */
    public void setRounding(Double  rounding){
        this.rounding = rounding ;
        this.modify("rounding",rounding);
    }

    /**
     * 设置 [POSITION]
     */
    public void setPosition(String  position){
        this.position = position ;
        this.modify("position",position);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [SYMBOL]
     */
    public void setSymbol(String  symbol){
        this.symbol = symbol ;
        this.modify("symbol",symbol);
    }

    /**
     * 设置 [CURRENCY_SUBUNIT_LABEL]
     */
    public void setCurrencySubunitLabel(String  currencySubunitLabel){
        this.currencySubunitLabel = currencySubunitLabel ;
        this.modify("currency_subunit_label",currencySubunitLabel);
    }


}


