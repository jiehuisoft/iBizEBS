package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse_orderpoint;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouse_orderpointService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warehouse_orderpointSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"最小库存规则" })
@RestController("Core-stock_warehouse_orderpoint")
@RequestMapping("")
public class Stock_warehouse_orderpointResource {

    @Autowired
    public IStock_warehouse_orderpointService stock_warehouse_orderpointService;

    @Autowired
    @Lazy
    public Stock_warehouse_orderpointMapping stock_warehouse_orderpointMapping;

    @PreAuthorize("hasPermission(this.stock_warehouse_orderpointMapping.toDomain(#stock_warehouse_orderpointdto),'iBizBusinessCentral-Stock_warehouse_orderpoint-Create')")
    @ApiOperation(value = "新建最小库存规则", tags = {"最小库存规则" },  notes = "新建最小库存规则")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouse_orderpoints")
    public ResponseEntity<Stock_warehouse_orderpointDTO> create(@Validated @RequestBody Stock_warehouse_orderpointDTO stock_warehouse_orderpointdto) {
        Stock_warehouse_orderpoint domain = stock_warehouse_orderpointMapping.toDomain(stock_warehouse_orderpointdto);
		stock_warehouse_orderpointService.create(domain);
        Stock_warehouse_orderpointDTO dto = stock_warehouse_orderpointMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_warehouse_orderpointMapping.toDomain(#stock_warehouse_orderpointdtos),'iBizBusinessCentral-Stock_warehouse_orderpoint-Create')")
    @ApiOperation(value = "批量新建最小库存规则", tags = {"最小库存规则" },  notes = "批量新建最小库存规则")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouse_orderpoints/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_warehouse_orderpointDTO> stock_warehouse_orderpointdtos) {
        stock_warehouse_orderpointService.createBatch(stock_warehouse_orderpointMapping.toDomain(stock_warehouse_orderpointdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_warehouse_orderpoint" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_warehouse_orderpointService.get(#stock_warehouse_orderpoint_id),'iBizBusinessCentral-Stock_warehouse_orderpoint-Update')")
    @ApiOperation(value = "更新最小库存规则", tags = {"最小库存规则" },  notes = "更新最小库存规则")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warehouse_orderpoints/{stock_warehouse_orderpoint_id}")
    public ResponseEntity<Stock_warehouse_orderpointDTO> update(@PathVariable("stock_warehouse_orderpoint_id") Long stock_warehouse_orderpoint_id, @RequestBody Stock_warehouse_orderpointDTO stock_warehouse_orderpointdto) {
		Stock_warehouse_orderpoint domain  = stock_warehouse_orderpointMapping.toDomain(stock_warehouse_orderpointdto);
        domain .setId(stock_warehouse_orderpoint_id);
		stock_warehouse_orderpointService.update(domain );
		Stock_warehouse_orderpointDTO dto = stock_warehouse_orderpointMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_warehouse_orderpointService.getStockWarehouseOrderpointByEntities(this.stock_warehouse_orderpointMapping.toDomain(#stock_warehouse_orderpointdtos)),'iBizBusinessCentral-Stock_warehouse_orderpoint-Update')")
    @ApiOperation(value = "批量更新最小库存规则", tags = {"最小库存规则" },  notes = "批量更新最小库存规则")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warehouse_orderpoints/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warehouse_orderpointDTO> stock_warehouse_orderpointdtos) {
        stock_warehouse_orderpointService.updateBatch(stock_warehouse_orderpointMapping.toDomain(stock_warehouse_orderpointdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_warehouse_orderpointService.get(#stock_warehouse_orderpoint_id),'iBizBusinessCentral-Stock_warehouse_orderpoint-Remove')")
    @ApiOperation(value = "删除最小库存规则", tags = {"最小库存规则" },  notes = "删除最小库存规则")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warehouse_orderpoints/{stock_warehouse_orderpoint_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_warehouse_orderpoint_id") Long stock_warehouse_orderpoint_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_warehouse_orderpointService.remove(stock_warehouse_orderpoint_id));
    }

    @PreAuthorize("hasPermission(this.stock_warehouse_orderpointService.getStockWarehouseOrderpointByIds(#ids),'iBizBusinessCentral-Stock_warehouse_orderpoint-Remove')")
    @ApiOperation(value = "批量删除最小库存规则", tags = {"最小库存规则" },  notes = "批量删除最小库存规则")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warehouse_orderpoints/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_warehouse_orderpointService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_warehouse_orderpointMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_warehouse_orderpoint-Get')")
    @ApiOperation(value = "获取最小库存规则", tags = {"最小库存规则" },  notes = "获取最小库存规则")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warehouse_orderpoints/{stock_warehouse_orderpoint_id}")
    public ResponseEntity<Stock_warehouse_orderpointDTO> get(@PathVariable("stock_warehouse_orderpoint_id") Long stock_warehouse_orderpoint_id) {
        Stock_warehouse_orderpoint domain = stock_warehouse_orderpointService.get(stock_warehouse_orderpoint_id);
        Stock_warehouse_orderpointDTO dto = stock_warehouse_orderpointMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取最小库存规则草稿", tags = {"最小库存规则" },  notes = "获取最小库存规则草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warehouse_orderpoints/getdraft")
    public ResponseEntity<Stock_warehouse_orderpointDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_warehouse_orderpointMapping.toDto(stock_warehouse_orderpointService.getDraft(new Stock_warehouse_orderpoint())));
    }

    @ApiOperation(value = "检查最小库存规则", tags = {"最小库存规则" },  notes = "检查最小库存规则")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouse_orderpoints/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_warehouse_orderpointDTO stock_warehouse_orderpointdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_warehouse_orderpointService.checkKey(stock_warehouse_orderpointMapping.toDomain(stock_warehouse_orderpointdto)));
    }

    @PreAuthorize("hasPermission(this.stock_warehouse_orderpointMapping.toDomain(#stock_warehouse_orderpointdto),'iBizBusinessCentral-Stock_warehouse_orderpoint-Save')")
    @ApiOperation(value = "保存最小库存规则", tags = {"最小库存规则" },  notes = "保存最小库存规则")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouse_orderpoints/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_warehouse_orderpointDTO stock_warehouse_orderpointdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_warehouse_orderpointService.save(stock_warehouse_orderpointMapping.toDomain(stock_warehouse_orderpointdto)));
    }

    @PreAuthorize("hasPermission(this.stock_warehouse_orderpointMapping.toDomain(#stock_warehouse_orderpointdtos),'iBizBusinessCentral-Stock_warehouse_orderpoint-Save')")
    @ApiOperation(value = "批量保存最小库存规则", tags = {"最小库存规则" },  notes = "批量保存最小库存规则")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouse_orderpoints/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_warehouse_orderpointDTO> stock_warehouse_orderpointdtos) {
        stock_warehouse_orderpointService.saveBatch(stock_warehouse_orderpointMapping.toDomain(stock_warehouse_orderpointdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warehouse_orderpoint-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_warehouse_orderpoint-Get')")
	@ApiOperation(value = "获取数据集", tags = {"最小库存规则" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_warehouse_orderpoints/fetchdefault")
	public ResponseEntity<List<Stock_warehouse_orderpointDTO>> fetchDefault(Stock_warehouse_orderpointSearchContext context) {
        Page<Stock_warehouse_orderpoint> domains = stock_warehouse_orderpointService.searchDefault(context) ;
        List<Stock_warehouse_orderpointDTO> list = stock_warehouse_orderpointMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warehouse_orderpoint-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_warehouse_orderpoint-Get')")
	@ApiOperation(value = "查询数据集", tags = {"最小库存规则" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_warehouse_orderpoints/searchdefault")
	public ResponseEntity<Page<Stock_warehouse_orderpointDTO>> searchDefault(@RequestBody Stock_warehouse_orderpointSearchContext context) {
        Page<Stock_warehouse_orderpoint> domains = stock_warehouse_orderpointService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_warehouse_orderpointMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

