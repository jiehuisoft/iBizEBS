package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Product_supplierinfoDTO]
 */
@Data
public class Product_supplierinfoDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PRODUCT_CODE]
     *
     */
    @JSONField(name = "product_code")
    @JsonProperty("product_code")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String productCode;

    /**
     * 属性 [DATE_END]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_end" , format="yyyy-MM-dd")
    @JsonProperty("date_end")
    private Timestamp dateEnd;

    /**
     * 属性 [DATE_START]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd")
    @JsonProperty("date_start")
    private Timestamp dateStart;

    /**
     * 属性 [MIN_QTY]
     *
     */
    @JSONField(name = "min_qty")
    @JsonProperty("min_qty")
    @NotNull(message = "[最少数量]不允许为空!")
    private Double minQty;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [PRODUCT_NAME]
     *
     */
    @JSONField(name = "product_name")
    @JsonProperty("product_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String productName;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [DELAY]
     *
     */
    @JSONField(name = "delay")
    @JsonProperty("delay")
    @NotNull(message = "[交货提前时间]不允许为空!")
    private Integer delay;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    @NotNull(message = "[价格]不允许为空!")
    private Double price;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currencyIdText;

    /**
     * 属性 [PRODUCT_TMPL_ID_TEXT]
     *
     */
    @JSONField(name = "product_tmpl_id_text")
    @JsonProperty("product_tmpl_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productTmplIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productUom;

    /**
     * 属性 [PRODUCT_VARIANT_COUNT]
     *
     */
    @JSONField(name = "product_variant_count")
    @JsonProperty("product_variant_count")
    private Integer productVariantCount;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [NAME_TEXT]
     *
     */
    @JSONField(name = "name_text")
    @JsonProperty("name_text")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String nameText;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productTmplId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;

    /**
     * 属性 [PURCHASE_REQUISITION_LINE_ID]
     *
     */
    @JSONField(name = "purchase_requisition_line_id")
    @JsonProperty("purchase_requisition_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long purchaseRequisitionLineId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[币种]不允许为空!")
    private Long currencyId;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long name;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [PRODUCT_CODE]
     */
    public void setProductCode(String  productCode){
        this.productCode = productCode ;
        this.modify("product_code",productCode);
    }

    /**
     * 设置 [DATE_END]
     */
    public void setDateEnd(Timestamp  dateEnd){
        this.dateEnd = dateEnd ;
        this.modify("date_end",dateEnd);
    }

    /**
     * 设置 [DATE_START]
     */
    public void setDateStart(Timestamp  dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }

    /**
     * 设置 [MIN_QTY]
     */
    public void setMinQty(Double  minQty){
        this.minQty = minQty ;
        this.modify("min_qty",minQty);
    }

    /**
     * 设置 [PRODUCT_NAME]
     */
    public void setProductName(String  productName){
        this.productName = productName ;
        this.modify("product_name",productName);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [DELAY]
     */
    public void setDelay(Integer  delay){
        this.delay = delay ;
        this.modify("delay",delay);
    }

    /**
     * 设置 [PRICE]
     */
    public void setPrice(Double  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    public void setProductTmplId(Long  productTmplId){
        this.productTmplId = productTmplId ;
        this.modify("product_tmpl_id",productTmplId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [PURCHASE_REQUISITION_LINE_ID]
     */
    public void setPurchaseRequisitionLineId(Long  purchaseRequisitionLineId){
        this.purchaseRequisitionLineId = purchaseRequisitionLineId ;
        this.modify("purchase_requisition_line_id",purchaseRequisitionLineId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(Long  name){
        this.name = name ;
        this.modify("name",name);
    }


}


