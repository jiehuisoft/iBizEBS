package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_rating.domain.Rating_mixin;
import cn.ibizlab.businesscentral.core.odoo_rating.service.IRating_mixinService;
import cn.ibizlab.businesscentral.core.odoo_rating.filter.Rating_mixinSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"混合评级" })
@RestController("Core-rating_mixin")
@RequestMapping("")
public class Rating_mixinResource {

    @Autowired
    public IRating_mixinService rating_mixinService;

    @Autowired
    @Lazy
    public Rating_mixinMapping rating_mixinMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Rating_mixin-Create-all')")
    @ApiOperation(value = "新建混合评级", tags = {"混合评级" },  notes = "新建混合评级")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_mixins")
    public ResponseEntity<Rating_mixinDTO> create(@Validated @RequestBody Rating_mixinDTO rating_mixindto) {
        Rating_mixin domain = rating_mixinMapping.toDomain(rating_mixindto);
		rating_mixinService.create(domain);
        Rating_mixinDTO dto = rating_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Rating_mixin-Create-all')")
    @ApiOperation(value = "批量新建混合评级", tags = {"混合评级" },  notes = "批量新建混合评级")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Rating_mixinDTO> rating_mixindtos) {
        rating_mixinService.createBatch(rating_mixinMapping.toDomain(rating_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Rating_mixin-Update-all')")
    @ApiOperation(value = "更新混合评级", tags = {"混合评级" },  notes = "更新混合评级")
	@RequestMapping(method = RequestMethod.PUT, value = "/rating_mixins/{rating_mixin_id}")
    public ResponseEntity<Rating_mixinDTO> update(@PathVariable("rating_mixin_id") Long rating_mixin_id, @RequestBody Rating_mixinDTO rating_mixindto) {
		Rating_mixin domain  = rating_mixinMapping.toDomain(rating_mixindto);
        domain .setId(rating_mixin_id);
		rating_mixinService.update(domain );
		Rating_mixinDTO dto = rating_mixinMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Rating_mixin-Update-all')")
    @ApiOperation(value = "批量更新混合评级", tags = {"混合评级" },  notes = "批量更新混合评级")
	@RequestMapping(method = RequestMethod.PUT, value = "/rating_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Rating_mixinDTO> rating_mixindtos) {
        rating_mixinService.updateBatch(rating_mixinMapping.toDomain(rating_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Rating_mixin-Remove-all')")
    @ApiOperation(value = "删除混合评级", tags = {"混合评级" },  notes = "删除混合评级")
	@RequestMapping(method = RequestMethod.DELETE, value = "/rating_mixins/{rating_mixin_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("rating_mixin_id") Long rating_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(rating_mixinService.remove(rating_mixin_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Rating_mixin-Remove-all')")
    @ApiOperation(value = "批量删除混合评级", tags = {"混合评级" },  notes = "批量删除混合评级")
	@RequestMapping(method = RequestMethod.DELETE, value = "/rating_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        rating_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Rating_mixin-Get-all')")
    @ApiOperation(value = "获取混合评级", tags = {"混合评级" },  notes = "获取混合评级")
	@RequestMapping(method = RequestMethod.GET, value = "/rating_mixins/{rating_mixin_id}")
    public ResponseEntity<Rating_mixinDTO> get(@PathVariable("rating_mixin_id") Long rating_mixin_id) {
        Rating_mixin domain = rating_mixinService.get(rating_mixin_id);
        Rating_mixinDTO dto = rating_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取混合评级草稿", tags = {"混合评级" },  notes = "获取混合评级草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/rating_mixins/getdraft")
    public ResponseEntity<Rating_mixinDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(rating_mixinMapping.toDto(rating_mixinService.getDraft(new Rating_mixin())));
    }

    @ApiOperation(value = "检查混合评级", tags = {"混合评级" },  notes = "检查混合评级")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_mixins/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Rating_mixinDTO rating_mixindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(rating_mixinService.checkKey(rating_mixinMapping.toDomain(rating_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Rating_mixin-Save-all')")
    @ApiOperation(value = "保存混合评级", tags = {"混合评级" },  notes = "保存混合评级")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_mixins/save")
    public ResponseEntity<Boolean> save(@RequestBody Rating_mixinDTO rating_mixindto) {
        return ResponseEntity.status(HttpStatus.OK).body(rating_mixinService.save(rating_mixinMapping.toDomain(rating_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Rating_mixin-Save-all')")
    @ApiOperation(value = "批量保存混合评级", tags = {"混合评级" },  notes = "批量保存混合评级")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_mixins/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Rating_mixinDTO> rating_mixindtos) {
        rating_mixinService.saveBatch(rating_mixinMapping.toDomain(rating_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Rating_mixin-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"混合评级" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/rating_mixins/fetchdefault")
	public ResponseEntity<List<Rating_mixinDTO>> fetchDefault(Rating_mixinSearchContext context) {
        Page<Rating_mixin> domains = rating_mixinService.searchDefault(context) ;
        List<Rating_mixinDTO> list = rating_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Rating_mixin-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"混合评级" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/rating_mixins/searchdefault")
	public ResponseEntity<Page<Rating_mixinDTO>> searchDefault(@RequestBody Rating_mixinSearchContext context) {
        Page<Rating_mixin> domains = rating_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(rating_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

