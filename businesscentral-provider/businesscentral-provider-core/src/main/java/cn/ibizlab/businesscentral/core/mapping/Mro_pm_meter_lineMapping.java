package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_line;
import cn.ibizlab.businesscentral.core.dto.Mro_pm_meter_lineDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMro_pm_meter_lineMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mro_pm_meter_lineMapping extends MappingBase<Mro_pm_meter_lineDTO, Mro_pm_meter_line> {


}

