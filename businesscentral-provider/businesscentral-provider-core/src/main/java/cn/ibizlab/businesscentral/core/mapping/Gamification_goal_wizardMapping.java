package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal_wizard;
import cn.ibizlab.businesscentral.core.dto.Gamification_goal_wizardDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreGamification_goal_wizardMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Gamification_goal_wizardMapping extends MappingBase<Gamification_goal_wizardDTO, Gamification_goal_wizard> {


}

