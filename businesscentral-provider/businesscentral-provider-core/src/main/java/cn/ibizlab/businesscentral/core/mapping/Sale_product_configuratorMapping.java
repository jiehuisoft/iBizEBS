package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_product_configurator;
import cn.ibizlab.businesscentral.core.dto.Sale_product_configuratorDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreSale_product_configuratorMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Sale_product_configuratorMapping extends MappingBase<Sale_product_configuratorDTO, Sale_product_configurator> {


}

