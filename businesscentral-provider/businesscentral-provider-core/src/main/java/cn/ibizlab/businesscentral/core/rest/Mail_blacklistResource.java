package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_blacklist;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_blacklistService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_blacklistSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"邮件黑名单" })
@RestController("Core-mail_blacklist")
@RequestMapping("")
public class Mail_blacklistResource {

    @Autowired
    public IMail_blacklistService mail_blacklistService;

    @Autowired
    @Lazy
    public Mail_blacklistMapping mail_blacklistMapping;

    @PreAuthorize("hasPermission(this.mail_blacklistMapping.toDomain(#mail_blacklistdto),'iBizBusinessCentral-Mail_blacklist-Create')")
    @ApiOperation(value = "新建邮件黑名单", tags = {"邮件黑名单" },  notes = "新建邮件黑名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists")
    public ResponseEntity<Mail_blacklistDTO> create(@Validated @RequestBody Mail_blacklistDTO mail_blacklistdto) {
        Mail_blacklist domain = mail_blacklistMapping.toDomain(mail_blacklistdto);
		mail_blacklistService.create(domain);
        Mail_blacklistDTO dto = mail_blacklistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_blacklistMapping.toDomain(#mail_blacklistdtos),'iBizBusinessCentral-Mail_blacklist-Create')")
    @ApiOperation(value = "批量新建邮件黑名单", tags = {"邮件黑名单" },  notes = "批量新建邮件黑名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_blacklistDTO> mail_blacklistdtos) {
        mail_blacklistService.createBatch(mail_blacklistMapping.toDomain(mail_blacklistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_blacklist" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_blacklistService.get(#mail_blacklist_id),'iBizBusinessCentral-Mail_blacklist-Update')")
    @ApiOperation(value = "更新邮件黑名单", tags = {"邮件黑名单" },  notes = "更新邮件黑名单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_blacklists/{mail_blacklist_id}")
    public ResponseEntity<Mail_blacklistDTO> update(@PathVariable("mail_blacklist_id") Long mail_blacklist_id, @RequestBody Mail_blacklistDTO mail_blacklistdto) {
		Mail_blacklist domain  = mail_blacklistMapping.toDomain(mail_blacklistdto);
        domain .setId(mail_blacklist_id);
		mail_blacklistService.update(domain );
		Mail_blacklistDTO dto = mail_blacklistMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_blacklistService.getMailBlacklistByEntities(this.mail_blacklistMapping.toDomain(#mail_blacklistdtos)),'iBizBusinessCentral-Mail_blacklist-Update')")
    @ApiOperation(value = "批量更新邮件黑名单", tags = {"邮件黑名单" },  notes = "批量更新邮件黑名单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_blacklists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_blacklistDTO> mail_blacklistdtos) {
        mail_blacklistService.updateBatch(mail_blacklistMapping.toDomain(mail_blacklistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_blacklistService.get(#mail_blacklist_id),'iBizBusinessCentral-Mail_blacklist-Remove')")
    @ApiOperation(value = "删除邮件黑名单", tags = {"邮件黑名单" },  notes = "删除邮件黑名单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_blacklists/{mail_blacklist_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_blacklist_id") Long mail_blacklist_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_blacklistService.remove(mail_blacklist_id));
    }

    @PreAuthorize("hasPermission(this.mail_blacklistService.getMailBlacklistByIds(#ids),'iBizBusinessCentral-Mail_blacklist-Remove')")
    @ApiOperation(value = "批量删除邮件黑名单", tags = {"邮件黑名单" },  notes = "批量删除邮件黑名单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_blacklists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_blacklistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_blacklistMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_blacklist-Get')")
    @ApiOperation(value = "获取邮件黑名单", tags = {"邮件黑名单" },  notes = "获取邮件黑名单")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_blacklists/{mail_blacklist_id}")
    public ResponseEntity<Mail_blacklistDTO> get(@PathVariable("mail_blacklist_id") Long mail_blacklist_id) {
        Mail_blacklist domain = mail_blacklistService.get(mail_blacklist_id);
        Mail_blacklistDTO dto = mail_blacklistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取邮件黑名单草稿", tags = {"邮件黑名单" },  notes = "获取邮件黑名单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_blacklists/getdraft")
    public ResponseEntity<Mail_blacklistDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_blacklistMapping.toDto(mail_blacklistService.getDraft(new Mail_blacklist())));
    }

    @ApiOperation(value = "检查邮件黑名单", tags = {"邮件黑名单" },  notes = "检查邮件黑名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_blacklistDTO mail_blacklistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_blacklistService.checkKey(mail_blacklistMapping.toDomain(mail_blacklistdto)));
    }

    @PreAuthorize("hasPermission(this.mail_blacklistMapping.toDomain(#mail_blacklistdto),'iBizBusinessCentral-Mail_blacklist-Save')")
    @ApiOperation(value = "保存邮件黑名单", tags = {"邮件黑名单" },  notes = "保存邮件黑名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_blacklistDTO mail_blacklistdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_blacklistService.save(mail_blacklistMapping.toDomain(mail_blacklistdto)));
    }

    @PreAuthorize("hasPermission(this.mail_blacklistMapping.toDomain(#mail_blacklistdtos),'iBizBusinessCentral-Mail_blacklist-Save')")
    @ApiOperation(value = "批量保存邮件黑名单", tags = {"邮件黑名单" },  notes = "批量保存邮件黑名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_blacklistDTO> mail_blacklistdtos) {
        mail_blacklistService.saveBatch(mail_blacklistMapping.toDomain(mail_blacklistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_blacklist-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_blacklist-Get')")
	@ApiOperation(value = "获取数据集", tags = {"邮件黑名单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_blacklists/fetchdefault")
	public ResponseEntity<List<Mail_blacklistDTO>> fetchDefault(Mail_blacklistSearchContext context) {
        Page<Mail_blacklist> domains = mail_blacklistService.searchDefault(context) ;
        List<Mail_blacklistDTO> list = mail_blacklistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_blacklist-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_blacklist-Get')")
	@ApiOperation(value = "查询数据集", tags = {"邮件黑名单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_blacklists/searchdefault")
	public ResponseEntity<Page<Mail_blacklistDTO>> searchDefault(@RequestBody Mail_blacklistSearchContext context) {
        Page<Mail_blacklist> domains = mail_blacklistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_blacklistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

