package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Message_attachment_rel;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMessage_attachment_relService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Message_attachment_relSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"消息附件" })
@RestController("Core-message_attachment_rel")
@RequestMapping("")
public class Message_attachment_relResource {

    @Autowired
    public IMessage_attachment_relService message_attachment_relService;

    @Autowired
    @Lazy
    public Message_attachment_relMapping message_attachment_relMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Message_attachment_rel-Create-all')")
    @ApiOperation(value = "新建消息附件", tags = {"消息附件" },  notes = "新建消息附件")
	@RequestMapping(method = RequestMethod.POST, value = "/message_attachment_rels")
    public ResponseEntity<Message_attachment_relDTO> create(@Validated @RequestBody Message_attachment_relDTO message_attachment_reldto) {
        Message_attachment_rel domain = message_attachment_relMapping.toDomain(message_attachment_reldto);
		message_attachment_relService.create(domain);
        Message_attachment_relDTO dto = message_attachment_relMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Message_attachment_rel-Create-all')")
    @ApiOperation(value = "批量新建消息附件", tags = {"消息附件" },  notes = "批量新建消息附件")
	@RequestMapping(method = RequestMethod.POST, value = "/message_attachment_rels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Message_attachment_relDTO> message_attachment_reldtos) {
        message_attachment_relService.createBatch(message_attachment_relMapping.toDomain(message_attachment_reldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Message_attachment_rel-Update-all')")
    @ApiOperation(value = "更新消息附件", tags = {"消息附件" },  notes = "更新消息附件")
	@RequestMapping(method = RequestMethod.PUT, value = "/message_attachment_rels/{message_attachment_rel_id}")
    public ResponseEntity<Message_attachment_relDTO> update(@PathVariable("message_attachment_rel_id") Long message_attachment_rel_id, @RequestBody Message_attachment_relDTO message_attachment_reldto) {
		Message_attachment_rel domain  = message_attachment_relMapping.toDomain(message_attachment_reldto);
        domain .setId(message_attachment_rel_id);
		message_attachment_relService.update(domain );
		Message_attachment_relDTO dto = message_attachment_relMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Message_attachment_rel-Update-all')")
    @ApiOperation(value = "批量更新消息附件", tags = {"消息附件" },  notes = "批量更新消息附件")
	@RequestMapping(method = RequestMethod.PUT, value = "/message_attachment_rels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Message_attachment_relDTO> message_attachment_reldtos) {
        message_attachment_relService.updateBatch(message_attachment_relMapping.toDomain(message_attachment_reldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Message_attachment_rel-Remove-all')")
    @ApiOperation(value = "删除消息附件", tags = {"消息附件" },  notes = "删除消息附件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/message_attachment_rels/{message_attachment_rel_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("message_attachment_rel_id") Long message_attachment_rel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(message_attachment_relService.remove(message_attachment_rel_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Message_attachment_rel-Remove-all')")
    @ApiOperation(value = "批量删除消息附件", tags = {"消息附件" },  notes = "批量删除消息附件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/message_attachment_rels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        message_attachment_relService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Message_attachment_rel-Get-all')")
    @ApiOperation(value = "获取消息附件", tags = {"消息附件" },  notes = "获取消息附件")
	@RequestMapping(method = RequestMethod.GET, value = "/message_attachment_rels/{message_attachment_rel_id}")
    public ResponseEntity<Message_attachment_relDTO> get(@PathVariable("message_attachment_rel_id") Long message_attachment_rel_id) {
        Message_attachment_rel domain = message_attachment_relService.get(message_attachment_rel_id);
        Message_attachment_relDTO dto = message_attachment_relMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取消息附件草稿", tags = {"消息附件" },  notes = "获取消息附件草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/message_attachment_rels/getdraft")
    public ResponseEntity<Message_attachment_relDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(message_attachment_relMapping.toDto(message_attachment_relService.getDraft(new Message_attachment_rel())));
    }

    @ApiOperation(value = "检查消息附件", tags = {"消息附件" },  notes = "检查消息附件")
	@RequestMapping(method = RequestMethod.POST, value = "/message_attachment_rels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Message_attachment_relDTO message_attachment_reldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(message_attachment_relService.checkKey(message_attachment_relMapping.toDomain(message_attachment_reldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Message_attachment_rel-Save-all')")
    @ApiOperation(value = "保存消息附件", tags = {"消息附件" },  notes = "保存消息附件")
	@RequestMapping(method = RequestMethod.POST, value = "/message_attachment_rels/save")
    public ResponseEntity<Boolean> save(@RequestBody Message_attachment_relDTO message_attachment_reldto) {
        return ResponseEntity.status(HttpStatus.OK).body(message_attachment_relService.save(message_attachment_relMapping.toDomain(message_attachment_reldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Message_attachment_rel-Save-all')")
    @ApiOperation(value = "批量保存消息附件", tags = {"消息附件" },  notes = "批量保存消息附件")
	@RequestMapping(method = RequestMethod.POST, value = "/message_attachment_rels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Message_attachment_relDTO> message_attachment_reldtos) {
        message_attachment_relService.saveBatch(message_attachment_relMapping.toDomain(message_attachment_reldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Message_attachment_rel-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"消息附件" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/message_attachment_rels/fetchdefault")
	public ResponseEntity<List<Message_attachment_relDTO>> fetchDefault(Message_attachment_relSearchContext context) {
        Page<Message_attachment_rel> domains = message_attachment_relService.searchDefault(context) ;
        List<Message_attachment_relDTO> list = message_attachment_relMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Message_attachment_rel-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"消息附件" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/message_attachment_rels/searchdefault")
	public ResponseEntity<Page<Message_attachment_relDTO>> searchDefault(@RequestBody Message_attachment_relSearchContext context) {
        Page<Message_attachment_rel> domains = message_attachment_relService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(message_attachment_relMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

