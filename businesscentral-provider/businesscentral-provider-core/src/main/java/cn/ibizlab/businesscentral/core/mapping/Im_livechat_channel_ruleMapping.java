package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel_rule;
import cn.ibizlab.businesscentral.core.dto.Im_livechat_channel_ruleDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreIm_livechat_channel_ruleMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Im_livechat_channel_ruleMapping extends MappingBase<Im_livechat_channel_ruleDTO, Im_livechat_channel_rule> {


}

