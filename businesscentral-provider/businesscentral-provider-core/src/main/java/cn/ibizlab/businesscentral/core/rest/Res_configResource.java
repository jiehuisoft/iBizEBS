package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_config;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_configService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_configSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"配置" })
@RestController("Core-res_config")
@RequestMapping("")
public class Res_configResource {

    @Autowired
    public IRes_configService res_configService;

    @Autowired
    @Lazy
    public Res_configMapping res_configMapping;

    @PreAuthorize("hasPermission(this.res_configMapping.toDomain(#res_configdto),'iBizBusinessCentral-Res_config-Create')")
    @ApiOperation(value = "新建配置", tags = {"配置" },  notes = "新建配置")
	@RequestMapping(method = RequestMethod.POST, value = "/res_configs")
    public ResponseEntity<Res_configDTO> create(@Validated @RequestBody Res_configDTO res_configdto) {
        Res_config domain = res_configMapping.toDomain(res_configdto);
		res_configService.create(domain);
        Res_configDTO dto = res_configMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_configMapping.toDomain(#res_configdtos),'iBizBusinessCentral-Res_config-Create')")
    @ApiOperation(value = "批量新建配置", tags = {"配置" },  notes = "批量新建配置")
	@RequestMapping(method = RequestMethod.POST, value = "/res_configs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_configDTO> res_configdtos) {
        res_configService.createBatch(res_configMapping.toDomain(res_configdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_config" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_configService.get(#res_config_id),'iBizBusinessCentral-Res_config-Update')")
    @ApiOperation(value = "更新配置", tags = {"配置" },  notes = "更新配置")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_configs/{res_config_id}")
    public ResponseEntity<Res_configDTO> update(@PathVariable("res_config_id") Long res_config_id, @RequestBody Res_configDTO res_configdto) {
		Res_config domain  = res_configMapping.toDomain(res_configdto);
        domain .setId(res_config_id);
		res_configService.update(domain );
		Res_configDTO dto = res_configMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_configService.getResConfigByEntities(this.res_configMapping.toDomain(#res_configdtos)),'iBizBusinessCentral-Res_config-Update')")
    @ApiOperation(value = "批量更新配置", tags = {"配置" },  notes = "批量更新配置")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_configs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_configDTO> res_configdtos) {
        res_configService.updateBatch(res_configMapping.toDomain(res_configdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_configService.get(#res_config_id),'iBizBusinessCentral-Res_config-Remove')")
    @ApiOperation(value = "删除配置", tags = {"配置" },  notes = "删除配置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_configs/{res_config_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_config_id") Long res_config_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_configService.remove(res_config_id));
    }

    @PreAuthorize("hasPermission(this.res_configService.getResConfigByIds(#ids),'iBizBusinessCentral-Res_config-Remove')")
    @ApiOperation(value = "批量删除配置", tags = {"配置" },  notes = "批量删除配置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_configs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_configService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_configMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_config-Get')")
    @ApiOperation(value = "获取配置", tags = {"配置" },  notes = "获取配置")
	@RequestMapping(method = RequestMethod.GET, value = "/res_configs/{res_config_id}")
    public ResponseEntity<Res_configDTO> get(@PathVariable("res_config_id") Long res_config_id) {
        Res_config domain = res_configService.get(res_config_id);
        Res_configDTO dto = res_configMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取配置草稿", tags = {"配置" },  notes = "获取配置草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_configs/getdraft")
    public ResponseEntity<Res_configDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_configMapping.toDto(res_configService.getDraft(new Res_config())));
    }

    @ApiOperation(value = "检查配置", tags = {"配置" },  notes = "检查配置")
	@RequestMapping(method = RequestMethod.POST, value = "/res_configs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_configDTO res_configdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_configService.checkKey(res_configMapping.toDomain(res_configdto)));
    }

    @PreAuthorize("hasPermission(this.res_configMapping.toDomain(#res_configdto),'iBizBusinessCentral-Res_config-Save')")
    @ApiOperation(value = "保存配置", tags = {"配置" },  notes = "保存配置")
	@RequestMapping(method = RequestMethod.POST, value = "/res_configs/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_configDTO res_configdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_configService.save(res_configMapping.toDomain(res_configdto)));
    }

    @PreAuthorize("hasPermission(this.res_configMapping.toDomain(#res_configdtos),'iBizBusinessCentral-Res_config-Save')")
    @ApiOperation(value = "批量保存配置", tags = {"配置" },  notes = "批量保存配置")
	@RequestMapping(method = RequestMethod.POST, value = "/res_configs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_configDTO> res_configdtos) {
        res_configService.saveBatch(res_configMapping.toDomain(res_configdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_config-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_config-Get')")
	@ApiOperation(value = "获取数据集", tags = {"配置" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_configs/fetchdefault")
	public ResponseEntity<List<Res_configDTO>> fetchDefault(Res_configSearchContext context) {
        Page<Res_config> domains = res_configService.searchDefault(context) ;
        List<Res_configDTO> list = res_configMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_config-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_config-Get')")
	@ApiOperation(value = "查询数据集", tags = {"配置" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_configs/searchdefault")
	public ResponseEntity<Page<Res_configDTO>> searchDefault(@RequestBody Res_configSearchContext context) {
        Page<Res_config> domains = res_configService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_configMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

