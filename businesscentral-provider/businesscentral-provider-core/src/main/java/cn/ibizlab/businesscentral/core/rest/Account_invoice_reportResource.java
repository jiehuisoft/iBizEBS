package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_report;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_reportService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_reportSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"发票统计" })
@RestController("Core-account_invoice_report")
@RequestMapping("")
public class Account_invoice_reportResource {

    @Autowired
    public IAccount_invoice_reportService account_invoice_reportService;

    @Autowired
    @Lazy
    public Account_invoice_reportMapping account_invoice_reportMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_report-Create-all')")
    @ApiOperation(value = "新建发票统计", tags = {"发票统计" },  notes = "新建发票统计")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_reports")
    public ResponseEntity<Account_invoice_reportDTO> create(@Validated @RequestBody Account_invoice_reportDTO account_invoice_reportdto) {
        Account_invoice_report domain = account_invoice_reportMapping.toDomain(account_invoice_reportdto);
		account_invoice_reportService.create(domain);
        Account_invoice_reportDTO dto = account_invoice_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_report-Create-all')")
    @ApiOperation(value = "批量新建发票统计", tags = {"发票统计" },  notes = "批量新建发票统计")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_reportDTO> account_invoice_reportdtos) {
        account_invoice_reportService.createBatch(account_invoice_reportMapping.toDomain(account_invoice_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_report-Update-all')")
    @ApiOperation(value = "更新发票统计", tags = {"发票统计" },  notes = "更新发票统计")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_reports/{account_invoice_report_id}")
    public ResponseEntity<Account_invoice_reportDTO> update(@PathVariable("account_invoice_report_id") Long account_invoice_report_id, @RequestBody Account_invoice_reportDTO account_invoice_reportdto) {
		Account_invoice_report domain  = account_invoice_reportMapping.toDomain(account_invoice_reportdto);
        domain .setId(account_invoice_report_id);
		account_invoice_reportService.update(domain );
		Account_invoice_reportDTO dto = account_invoice_reportMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_report-Update-all')")
    @ApiOperation(value = "批量更新发票统计", tags = {"发票统计" },  notes = "批量更新发票统计")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_reportDTO> account_invoice_reportdtos) {
        account_invoice_reportService.updateBatch(account_invoice_reportMapping.toDomain(account_invoice_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_report-Remove-all')")
    @ApiOperation(value = "删除发票统计", tags = {"发票统计" },  notes = "删除发票统计")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_reports/{account_invoice_report_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_report_id") Long account_invoice_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_reportService.remove(account_invoice_report_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_report-Remove-all')")
    @ApiOperation(value = "批量删除发票统计", tags = {"发票统计" },  notes = "批量删除发票统计")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_invoice_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_report-Get-all')")
    @ApiOperation(value = "获取发票统计", tags = {"发票统计" },  notes = "获取发票统计")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_reports/{account_invoice_report_id}")
    public ResponseEntity<Account_invoice_reportDTO> get(@PathVariable("account_invoice_report_id") Long account_invoice_report_id) {
        Account_invoice_report domain = account_invoice_reportService.get(account_invoice_report_id);
        Account_invoice_reportDTO dto = account_invoice_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取发票统计草稿", tags = {"发票统计" },  notes = "获取发票统计草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_reports/getdraft")
    public ResponseEntity<Account_invoice_reportDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_reportMapping.toDto(account_invoice_reportService.getDraft(new Account_invoice_report())));
    }

    @ApiOperation(value = "检查发票统计", tags = {"发票统计" },  notes = "检查发票统计")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_reports/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoice_reportDTO account_invoice_reportdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_invoice_reportService.checkKey(account_invoice_reportMapping.toDomain(account_invoice_reportdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_report-Save-all')")
    @ApiOperation(value = "保存发票统计", tags = {"发票统计" },  notes = "保存发票统计")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_reports/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_invoice_reportDTO account_invoice_reportdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_reportService.save(account_invoice_reportMapping.toDomain(account_invoice_reportdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_report-Save-all')")
    @ApiOperation(value = "批量保存发票统计", tags = {"发票统计" },  notes = "批量保存发票统计")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_reports/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_invoice_reportDTO> account_invoice_reportdtos) {
        account_invoice_reportService.saveBatch(account_invoice_reportMapping.toDomain(account_invoice_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_report-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"发票统计" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_reports/fetchdefault")
	public ResponseEntity<List<Account_invoice_reportDTO>> fetchDefault(Account_invoice_reportSearchContext context) {
        Page<Account_invoice_report> domains = account_invoice_reportService.searchDefault(context) ;
        List<Account_invoice_reportDTO> list = account_invoice_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_report-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"发票统计" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_invoice_reports/searchdefault")
	public ResponseEntity<Page<Account_invoice_reportDTO>> searchDefault(@RequestBody Account_invoice_reportSearchContext context) {
        Page<Account_invoice_report> domains = account_invoice_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

