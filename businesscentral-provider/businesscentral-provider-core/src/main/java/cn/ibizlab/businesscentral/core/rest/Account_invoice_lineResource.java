package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_lineService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"发票行" })
@RestController("Core-account_invoice_line")
@RequestMapping("")
public class Account_invoice_lineResource {

    @Autowired
    public IAccount_invoice_lineService account_invoice_lineService;

    @Autowired
    @Lazy
    public Account_invoice_lineMapping account_invoice_lineMapping;

    @PreAuthorize("hasPermission(this.account_invoice_lineMapping.toDomain(#account_invoice_linedto),'iBizBusinessCentral-Account_invoice_line-Create')")
    @ApiOperation(value = "新建发票行", tags = {"发票行" },  notes = "新建发票行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines")
    public ResponseEntity<Account_invoice_lineDTO> create(@Validated @RequestBody Account_invoice_lineDTO account_invoice_linedto) {
        Account_invoice_line domain = account_invoice_lineMapping.toDomain(account_invoice_linedto);
		account_invoice_lineService.create(domain);
        Account_invoice_lineDTO dto = account_invoice_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoice_lineMapping.toDomain(#account_invoice_linedtos),'iBizBusinessCentral-Account_invoice_line-Create')")
    @ApiOperation(value = "批量新建发票行", tags = {"发票行" },  notes = "批量新建发票行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_lineDTO> account_invoice_linedtos) {
        account_invoice_lineService.createBatch(account_invoice_lineMapping.toDomain(account_invoice_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_invoice_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_invoice_lineService.get(#account_invoice_line_id),'iBizBusinessCentral-Account_invoice_line-Update')")
    @ApiOperation(value = "更新发票行", tags = {"发票行" },  notes = "更新发票行")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_lines/{account_invoice_line_id}")
    public ResponseEntity<Account_invoice_lineDTO> update(@PathVariable("account_invoice_line_id") Long account_invoice_line_id, @RequestBody Account_invoice_lineDTO account_invoice_linedto) {
		Account_invoice_line domain  = account_invoice_lineMapping.toDomain(account_invoice_linedto);
        domain .setId(account_invoice_line_id);
		account_invoice_lineService.update(domain );
		Account_invoice_lineDTO dto = account_invoice_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoice_lineService.getAccountInvoiceLineByEntities(this.account_invoice_lineMapping.toDomain(#account_invoice_linedtos)),'iBizBusinessCentral-Account_invoice_line-Update')")
    @ApiOperation(value = "批量更新发票行", tags = {"发票行" },  notes = "批量更新发票行")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_lineDTO> account_invoice_linedtos) {
        account_invoice_lineService.updateBatch(account_invoice_lineMapping.toDomain(account_invoice_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_invoice_lineService.get(#account_invoice_line_id),'iBizBusinessCentral-Account_invoice_line-Remove')")
    @ApiOperation(value = "删除发票行", tags = {"发票行" },  notes = "删除发票行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_lines/{account_invoice_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_line_id") Long account_invoice_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_lineService.remove(account_invoice_line_id));
    }

    @PreAuthorize("hasPermission(this.account_invoice_lineService.getAccountInvoiceLineByIds(#ids),'iBizBusinessCentral-Account_invoice_line-Remove')")
    @ApiOperation(value = "批量删除发票行", tags = {"发票行" },  notes = "批量删除发票行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_invoice_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_invoice_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_invoice_line-Get')")
    @ApiOperation(value = "获取发票行", tags = {"发票行" },  notes = "获取发票行")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_lines/{account_invoice_line_id}")
    public ResponseEntity<Account_invoice_lineDTO> get(@PathVariable("account_invoice_line_id") Long account_invoice_line_id) {
        Account_invoice_line domain = account_invoice_lineService.get(account_invoice_line_id);
        Account_invoice_lineDTO dto = account_invoice_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取发票行草稿", tags = {"发票行" },  notes = "获取发票行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_lines/getdraft")
    public ResponseEntity<Account_invoice_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_lineMapping.toDto(account_invoice_lineService.getDraft(new Account_invoice_line())));
    }

    @ApiOperation(value = "检查发票行", tags = {"发票行" },  notes = "检查发票行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoice_lineDTO account_invoice_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_invoice_lineService.checkKey(account_invoice_lineMapping.toDomain(account_invoice_linedto)));
    }

    @PreAuthorize("hasPermission(this.account_invoice_lineMapping.toDomain(#account_invoice_linedto),'iBizBusinessCentral-Account_invoice_line-Save')")
    @ApiOperation(value = "保存发票行", tags = {"发票行" },  notes = "保存发票行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_invoice_lineDTO account_invoice_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_lineService.save(account_invoice_lineMapping.toDomain(account_invoice_linedto)));
    }

    @PreAuthorize("hasPermission(this.account_invoice_lineMapping.toDomain(#account_invoice_linedtos),'iBizBusinessCentral-Account_invoice_line-Save')")
    @ApiOperation(value = "批量保存发票行", tags = {"发票行" },  notes = "批量保存发票行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_invoice_lineDTO> account_invoice_linedtos) {
        account_invoice_lineService.saveBatch(account_invoice_lineMapping.toDomain(account_invoice_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"发票行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_lines/fetchdefault")
	public ResponseEntity<List<Account_invoice_lineDTO>> fetchDefault(Account_invoice_lineSearchContext context) {
        Page<Account_invoice_line> domains = account_invoice_lineService.searchDefault(context) ;
        List<Account_invoice_lineDTO> list = account_invoice_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"发票行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_invoice_lines/searchdefault")
	public ResponseEntity<Page<Account_invoice_lineDTO>> searchDefault(@RequestBody Account_invoice_lineSearchContext context) {
        Page<Account_invoice_line> domains = account_invoice_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

