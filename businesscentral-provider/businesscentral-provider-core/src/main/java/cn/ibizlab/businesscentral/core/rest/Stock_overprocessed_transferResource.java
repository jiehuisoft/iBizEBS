package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_overprocessed_transfer;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_overprocessed_transferService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_overprocessed_transferSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"已过帐移动" })
@RestController("Core-stock_overprocessed_transfer")
@RequestMapping("")
public class Stock_overprocessed_transferResource {

    @Autowired
    public IStock_overprocessed_transferService stock_overprocessed_transferService;

    @Autowired
    @Lazy
    public Stock_overprocessed_transferMapping stock_overprocessed_transferMapping;

    @PreAuthorize("hasPermission(this.stock_overprocessed_transferMapping.toDomain(#stock_overprocessed_transferdto),'iBizBusinessCentral-Stock_overprocessed_transfer-Create')")
    @ApiOperation(value = "新建已过帐移动", tags = {"已过帐移动" },  notes = "新建已过帐移动")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_overprocessed_transfers")
    public ResponseEntity<Stock_overprocessed_transferDTO> create(@Validated @RequestBody Stock_overprocessed_transferDTO stock_overprocessed_transferdto) {
        Stock_overprocessed_transfer domain = stock_overprocessed_transferMapping.toDomain(stock_overprocessed_transferdto);
		stock_overprocessed_transferService.create(domain);
        Stock_overprocessed_transferDTO dto = stock_overprocessed_transferMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_overprocessed_transferMapping.toDomain(#stock_overprocessed_transferdtos),'iBizBusinessCentral-Stock_overprocessed_transfer-Create')")
    @ApiOperation(value = "批量新建已过帐移动", tags = {"已过帐移动" },  notes = "批量新建已过帐移动")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_overprocessed_transfers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_overprocessed_transferDTO> stock_overprocessed_transferdtos) {
        stock_overprocessed_transferService.createBatch(stock_overprocessed_transferMapping.toDomain(stock_overprocessed_transferdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_overprocessed_transfer" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_overprocessed_transferService.get(#stock_overprocessed_transfer_id),'iBizBusinessCentral-Stock_overprocessed_transfer-Update')")
    @ApiOperation(value = "更新已过帐移动", tags = {"已过帐移动" },  notes = "更新已过帐移动")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_overprocessed_transfers/{stock_overprocessed_transfer_id}")
    public ResponseEntity<Stock_overprocessed_transferDTO> update(@PathVariable("stock_overprocessed_transfer_id") Long stock_overprocessed_transfer_id, @RequestBody Stock_overprocessed_transferDTO stock_overprocessed_transferdto) {
		Stock_overprocessed_transfer domain  = stock_overprocessed_transferMapping.toDomain(stock_overprocessed_transferdto);
        domain .setId(stock_overprocessed_transfer_id);
		stock_overprocessed_transferService.update(domain );
		Stock_overprocessed_transferDTO dto = stock_overprocessed_transferMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_overprocessed_transferService.getStockOverprocessedTransferByEntities(this.stock_overprocessed_transferMapping.toDomain(#stock_overprocessed_transferdtos)),'iBizBusinessCentral-Stock_overprocessed_transfer-Update')")
    @ApiOperation(value = "批量更新已过帐移动", tags = {"已过帐移动" },  notes = "批量更新已过帐移动")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_overprocessed_transfers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_overprocessed_transferDTO> stock_overprocessed_transferdtos) {
        stock_overprocessed_transferService.updateBatch(stock_overprocessed_transferMapping.toDomain(stock_overprocessed_transferdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_overprocessed_transferService.get(#stock_overprocessed_transfer_id),'iBizBusinessCentral-Stock_overprocessed_transfer-Remove')")
    @ApiOperation(value = "删除已过帐移动", tags = {"已过帐移动" },  notes = "删除已过帐移动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_overprocessed_transfers/{stock_overprocessed_transfer_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_overprocessed_transfer_id") Long stock_overprocessed_transfer_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_overprocessed_transferService.remove(stock_overprocessed_transfer_id));
    }

    @PreAuthorize("hasPermission(this.stock_overprocessed_transferService.getStockOverprocessedTransferByIds(#ids),'iBizBusinessCentral-Stock_overprocessed_transfer-Remove')")
    @ApiOperation(value = "批量删除已过帐移动", tags = {"已过帐移动" },  notes = "批量删除已过帐移动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_overprocessed_transfers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_overprocessed_transferService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_overprocessed_transferMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_overprocessed_transfer-Get')")
    @ApiOperation(value = "获取已过帐移动", tags = {"已过帐移动" },  notes = "获取已过帐移动")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_overprocessed_transfers/{stock_overprocessed_transfer_id}")
    public ResponseEntity<Stock_overprocessed_transferDTO> get(@PathVariable("stock_overprocessed_transfer_id") Long stock_overprocessed_transfer_id) {
        Stock_overprocessed_transfer domain = stock_overprocessed_transferService.get(stock_overprocessed_transfer_id);
        Stock_overprocessed_transferDTO dto = stock_overprocessed_transferMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取已过帐移动草稿", tags = {"已过帐移动" },  notes = "获取已过帐移动草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_overprocessed_transfers/getdraft")
    public ResponseEntity<Stock_overprocessed_transferDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_overprocessed_transferMapping.toDto(stock_overprocessed_transferService.getDraft(new Stock_overprocessed_transfer())));
    }

    @ApiOperation(value = "检查已过帐移动", tags = {"已过帐移动" },  notes = "检查已过帐移动")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_overprocessed_transfers/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_overprocessed_transferDTO stock_overprocessed_transferdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_overprocessed_transferService.checkKey(stock_overprocessed_transferMapping.toDomain(stock_overprocessed_transferdto)));
    }

    @PreAuthorize("hasPermission(this.stock_overprocessed_transferMapping.toDomain(#stock_overprocessed_transferdto),'iBizBusinessCentral-Stock_overprocessed_transfer-Save')")
    @ApiOperation(value = "保存已过帐移动", tags = {"已过帐移动" },  notes = "保存已过帐移动")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_overprocessed_transfers/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_overprocessed_transferDTO stock_overprocessed_transferdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_overprocessed_transferService.save(stock_overprocessed_transferMapping.toDomain(stock_overprocessed_transferdto)));
    }

    @PreAuthorize("hasPermission(this.stock_overprocessed_transferMapping.toDomain(#stock_overprocessed_transferdtos),'iBizBusinessCentral-Stock_overprocessed_transfer-Save')")
    @ApiOperation(value = "批量保存已过帐移动", tags = {"已过帐移动" },  notes = "批量保存已过帐移动")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_overprocessed_transfers/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_overprocessed_transferDTO> stock_overprocessed_transferdtos) {
        stock_overprocessed_transferService.saveBatch(stock_overprocessed_transferMapping.toDomain(stock_overprocessed_transferdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_overprocessed_transfer-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_overprocessed_transfer-Get')")
	@ApiOperation(value = "获取数据集", tags = {"已过帐移动" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_overprocessed_transfers/fetchdefault")
	public ResponseEntity<List<Stock_overprocessed_transferDTO>> fetchDefault(Stock_overprocessed_transferSearchContext context) {
        Page<Stock_overprocessed_transfer> domains = stock_overprocessed_transferService.searchDefault(context) ;
        List<Stock_overprocessed_transferDTO> list = stock_overprocessed_transferMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_overprocessed_transfer-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_overprocessed_transfer-Get')")
	@ApiOperation(value = "查询数据集", tags = {"已过帐移动" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_overprocessed_transfers/searchdefault")
	public ResponseEntity<Page<Stock_overprocessed_transferDTO>> searchDefault(@RequestBody Stock_overprocessed_transferSearchContext context) {
        Page<Stock_overprocessed_transfer> domains = stock_overprocessed_transferService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_overprocessed_transferMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

