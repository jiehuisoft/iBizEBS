package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_user_input;
import cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_user_inputService;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_user_inputSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"调查用户输入" })
@RestController("Core-survey_user_input")
@RequestMapping("")
public class Survey_user_inputResource {

    @Autowired
    public ISurvey_user_inputService survey_user_inputService;

    @Autowired
    @Lazy
    public Survey_user_inputMapping survey_user_inputMapping;

    @PreAuthorize("hasPermission(this.survey_user_inputMapping.toDomain(#survey_user_inputdto),'iBizBusinessCentral-Survey_user_input-Create')")
    @ApiOperation(value = "新建调查用户输入", tags = {"调查用户输入" },  notes = "新建调查用户输入")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_inputs")
    public ResponseEntity<Survey_user_inputDTO> create(@Validated @RequestBody Survey_user_inputDTO survey_user_inputdto) {
        Survey_user_input domain = survey_user_inputMapping.toDomain(survey_user_inputdto);
		survey_user_inputService.create(domain);
        Survey_user_inputDTO dto = survey_user_inputMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_user_inputMapping.toDomain(#survey_user_inputdtos),'iBizBusinessCentral-Survey_user_input-Create')")
    @ApiOperation(value = "批量新建调查用户输入", tags = {"调查用户输入" },  notes = "批量新建调查用户输入")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_inputs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_user_inputDTO> survey_user_inputdtos) {
        survey_user_inputService.createBatch(survey_user_inputMapping.toDomain(survey_user_inputdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "survey_user_input" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.survey_user_inputService.get(#survey_user_input_id),'iBizBusinessCentral-Survey_user_input-Update')")
    @ApiOperation(value = "更新调查用户输入", tags = {"调查用户输入" },  notes = "更新调查用户输入")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_user_inputs/{survey_user_input_id}")
    public ResponseEntity<Survey_user_inputDTO> update(@PathVariable("survey_user_input_id") Long survey_user_input_id, @RequestBody Survey_user_inputDTO survey_user_inputdto) {
		Survey_user_input domain  = survey_user_inputMapping.toDomain(survey_user_inputdto);
        domain .setId(survey_user_input_id);
		survey_user_inputService.update(domain );
		Survey_user_inputDTO dto = survey_user_inputMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_user_inputService.getSurveyUserInputByEntities(this.survey_user_inputMapping.toDomain(#survey_user_inputdtos)),'iBizBusinessCentral-Survey_user_input-Update')")
    @ApiOperation(value = "批量更新调查用户输入", tags = {"调查用户输入" },  notes = "批量更新调查用户输入")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_user_inputs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_user_inputDTO> survey_user_inputdtos) {
        survey_user_inputService.updateBatch(survey_user_inputMapping.toDomain(survey_user_inputdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.survey_user_inputService.get(#survey_user_input_id),'iBizBusinessCentral-Survey_user_input-Remove')")
    @ApiOperation(value = "删除调查用户输入", tags = {"调查用户输入" },  notes = "删除调查用户输入")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_user_inputs/{survey_user_input_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("survey_user_input_id") Long survey_user_input_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_user_inputService.remove(survey_user_input_id));
    }

    @PreAuthorize("hasPermission(this.survey_user_inputService.getSurveyUserInputByIds(#ids),'iBizBusinessCentral-Survey_user_input-Remove')")
    @ApiOperation(value = "批量删除调查用户输入", tags = {"调查用户输入" },  notes = "批量删除调查用户输入")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_user_inputs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        survey_user_inputService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.survey_user_inputMapping.toDomain(returnObject.body),'iBizBusinessCentral-Survey_user_input-Get')")
    @ApiOperation(value = "获取调查用户输入", tags = {"调查用户输入" },  notes = "获取调查用户输入")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_user_inputs/{survey_user_input_id}")
    public ResponseEntity<Survey_user_inputDTO> get(@PathVariable("survey_user_input_id") Long survey_user_input_id) {
        Survey_user_input domain = survey_user_inputService.get(survey_user_input_id);
        Survey_user_inputDTO dto = survey_user_inputMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取调查用户输入草稿", tags = {"调查用户输入" },  notes = "获取调查用户输入草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_user_inputs/getdraft")
    public ResponseEntity<Survey_user_inputDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(survey_user_inputMapping.toDto(survey_user_inputService.getDraft(new Survey_user_input())));
    }

    @ApiOperation(value = "检查调查用户输入", tags = {"调查用户输入" },  notes = "检查调查用户输入")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_inputs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Survey_user_inputDTO survey_user_inputdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(survey_user_inputService.checkKey(survey_user_inputMapping.toDomain(survey_user_inputdto)));
    }

    @PreAuthorize("hasPermission(this.survey_user_inputMapping.toDomain(#survey_user_inputdto),'iBizBusinessCentral-Survey_user_input-Save')")
    @ApiOperation(value = "保存调查用户输入", tags = {"调查用户输入" },  notes = "保存调查用户输入")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_inputs/save")
    public ResponseEntity<Boolean> save(@RequestBody Survey_user_inputDTO survey_user_inputdto) {
        return ResponseEntity.status(HttpStatus.OK).body(survey_user_inputService.save(survey_user_inputMapping.toDomain(survey_user_inputdto)));
    }

    @PreAuthorize("hasPermission(this.survey_user_inputMapping.toDomain(#survey_user_inputdtos),'iBizBusinessCentral-Survey_user_input-Save')")
    @ApiOperation(value = "批量保存调查用户输入", tags = {"调查用户输入" },  notes = "批量保存调查用户输入")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_inputs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Survey_user_inputDTO> survey_user_inputdtos) {
        survey_user_inputService.saveBatch(survey_user_inputMapping.toDomain(survey_user_inputdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_user_input-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_user_input-Get')")
	@ApiOperation(value = "获取数据集", tags = {"调查用户输入" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/survey_user_inputs/fetchdefault")
	public ResponseEntity<List<Survey_user_inputDTO>> fetchDefault(Survey_user_inputSearchContext context) {
        Page<Survey_user_input> domains = survey_user_inputService.searchDefault(context) ;
        List<Survey_user_inputDTO> list = survey_user_inputMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_user_input-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_user_input-Get')")
	@ApiOperation(value = "查询数据集", tags = {"调查用户输入" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/survey_user_inputs/searchdefault")
	public ResponseEntity<Page<Survey_user_inputDTO>> searchDefault(@RequestBody Survey_user_inputSearchContext context) {
        Page<Survey_user_input> domains = survey_user_inputService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_user_inputMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

