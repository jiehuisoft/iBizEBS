package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_stage;
import cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_stageService;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_stageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"CRM 阶段" })
@RestController("Core-crm_stage")
@RequestMapping("")
public class Crm_stageResource {

    @Autowired
    public ICrm_stageService crm_stageService;

    @Autowired
    @Lazy
    public Crm_stageMapping crm_stageMapping;

    @PreAuthorize("hasPermission(this.crm_stageMapping.toDomain(#crm_stagedto),'iBizBusinessCentral-Crm_stage-Create')")
    @ApiOperation(value = "新建CRM 阶段", tags = {"CRM 阶段" },  notes = "新建CRM 阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_stages")
    public ResponseEntity<Crm_stageDTO> create(@Validated @RequestBody Crm_stageDTO crm_stagedto) {
        Crm_stage domain = crm_stageMapping.toDomain(crm_stagedto);
		crm_stageService.create(domain);
        Crm_stageDTO dto = crm_stageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_stageMapping.toDomain(#crm_stagedtos),'iBizBusinessCentral-Crm_stage-Create')")
    @ApiOperation(value = "批量新建CRM 阶段", tags = {"CRM 阶段" },  notes = "批量新建CRM 阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_stages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_stageDTO> crm_stagedtos) {
        crm_stageService.createBatch(crm_stageMapping.toDomain(crm_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "crm_stage" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.crm_stageService.get(#crm_stage_id),'iBizBusinessCentral-Crm_stage-Update')")
    @ApiOperation(value = "更新CRM 阶段", tags = {"CRM 阶段" },  notes = "更新CRM 阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_stages/{crm_stage_id}")
    public ResponseEntity<Crm_stageDTO> update(@PathVariable("crm_stage_id") Long crm_stage_id, @RequestBody Crm_stageDTO crm_stagedto) {
		Crm_stage domain  = crm_stageMapping.toDomain(crm_stagedto);
        domain .setId(crm_stage_id);
		crm_stageService.update(domain );
		Crm_stageDTO dto = crm_stageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_stageService.getCrmStageByEntities(this.crm_stageMapping.toDomain(#crm_stagedtos)),'iBizBusinessCentral-Crm_stage-Update')")
    @ApiOperation(value = "批量更新CRM 阶段", tags = {"CRM 阶段" },  notes = "批量更新CRM 阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_stages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_stageDTO> crm_stagedtos) {
        crm_stageService.updateBatch(crm_stageMapping.toDomain(crm_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.crm_stageService.get(#crm_stage_id),'iBizBusinessCentral-Crm_stage-Remove')")
    @ApiOperation(value = "删除CRM 阶段", tags = {"CRM 阶段" },  notes = "删除CRM 阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_stages/{crm_stage_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_stage_id") Long crm_stage_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_stageService.remove(crm_stage_id));
    }

    @PreAuthorize("hasPermission(this.crm_stageService.getCrmStageByIds(#ids),'iBizBusinessCentral-Crm_stage-Remove')")
    @ApiOperation(value = "批量删除CRM 阶段", tags = {"CRM 阶段" },  notes = "批量删除CRM 阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_stages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        crm_stageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.crm_stageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Crm_stage-Get')")
    @ApiOperation(value = "获取CRM 阶段", tags = {"CRM 阶段" },  notes = "获取CRM 阶段")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_stages/{crm_stage_id}")
    public ResponseEntity<Crm_stageDTO> get(@PathVariable("crm_stage_id") Long crm_stage_id) {
        Crm_stage domain = crm_stageService.get(crm_stage_id);
        Crm_stageDTO dto = crm_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取CRM 阶段草稿", tags = {"CRM 阶段" },  notes = "获取CRM 阶段草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_stages/getdraft")
    public ResponseEntity<Crm_stageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_stageMapping.toDto(crm_stageService.getDraft(new Crm_stage())));
    }

    @ApiOperation(value = "检查CRM 阶段", tags = {"CRM 阶段" },  notes = "检查CRM 阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_stages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Crm_stageDTO crm_stagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(crm_stageService.checkKey(crm_stageMapping.toDomain(crm_stagedto)));
    }

    @PreAuthorize("hasPermission(this.crm_stageMapping.toDomain(#crm_stagedto),'iBizBusinessCentral-Crm_stage-Save')")
    @ApiOperation(value = "保存CRM 阶段", tags = {"CRM 阶段" },  notes = "保存CRM 阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_stages/save")
    public ResponseEntity<Boolean> save(@RequestBody Crm_stageDTO crm_stagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(crm_stageService.save(crm_stageMapping.toDomain(crm_stagedto)));
    }

    @PreAuthorize("hasPermission(this.crm_stageMapping.toDomain(#crm_stagedtos),'iBizBusinessCentral-Crm_stage-Save')")
    @ApiOperation(value = "批量保存CRM 阶段", tags = {"CRM 阶段" },  notes = "批量保存CRM 阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_stages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Crm_stageDTO> crm_stagedtos) {
        crm_stageService.saveBatch(crm_stageMapping.toDomain(crm_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_stage-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_stage-Get')")
	@ApiOperation(value = "获取数据集", tags = {"CRM 阶段" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/crm_stages/fetchdefault")
	public ResponseEntity<List<Crm_stageDTO>> fetchDefault(Crm_stageSearchContext context) {
        Page<Crm_stage> domains = crm_stageService.searchDefault(context) ;
        List<Crm_stageDTO> list = crm_stageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_stage-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_stage-Get')")
	@ApiOperation(value = "查询数据集", tags = {"CRM 阶段" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/crm_stages/searchdefault")
	public ResponseEntity<Page<Crm_stageDTO>> searchDefault(@RequestBody Crm_stageSearchContext context) {
        Page<Crm_stage> domains = crm_stageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_stageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

