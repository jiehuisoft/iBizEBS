package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_bus.domain.Bus_bus;
import cn.ibizlab.businesscentral.core.odoo_bus.service.IBus_busService;
import cn.ibizlab.businesscentral.core.odoo_bus.filter.Bus_busSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"通讯总线" })
@RestController("Core-bus_bus")
@RequestMapping("")
public class Bus_busResource {

    @Autowired
    public IBus_busService bus_busService;

    @Autowired
    @Lazy
    public Bus_busMapping bus_busMapping;

    @PreAuthorize("hasPermission(this.bus_busMapping.toDomain(#bus_busdto),'iBizBusinessCentral-Bus_bus-Create')")
    @ApiOperation(value = "新建通讯总线", tags = {"通讯总线" },  notes = "新建通讯总线")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_buses")
    public ResponseEntity<Bus_busDTO> create(@Validated @RequestBody Bus_busDTO bus_busdto) {
        Bus_bus domain = bus_busMapping.toDomain(bus_busdto);
		bus_busService.create(domain);
        Bus_busDTO dto = bus_busMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.bus_busMapping.toDomain(#bus_busdtos),'iBizBusinessCentral-Bus_bus-Create')")
    @ApiOperation(value = "批量新建通讯总线", tags = {"通讯总线" },  notes = "批量新建通讯总线")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_buses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Bus_busDTO> bus_busdtos) {
        bus_busService.createBatch(bus_busMapping.toDomain(bus_busdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "bus_bus" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.bus_busService.get(#bus_bus_id),'iBizBusinessCentral-Bus_bus-Update')")
    @ApiOperation(value = "更新通讯总线", tags = {"通讯总线" },  notes = "更新通讯总线")
	@RequestMapping(method = RequestMethod.PUT, value = "/bus_buses/{bus_bus_id}")
    public ResponseEntity<Bus_busDTO> update(@PathVariable("bus_bus_id") Long bus_bus_id, @RequestBody Bus_busDTO bus_busdto) {
		Bus_bus domain  = bus_busMapping.toDomain(bus_busdto);
        domain .setId(bus_bus_id);
		bus_busService.update(domain );
		Bus_busDTO dto = bus_busMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.bus_busService.getBusBusByEntities(this.bus_busMapping.toDomain(#bus_busdtos)),'iBizBusinessCentral-Bus_bus-Update')")
    @ApiOperation(value = "批量更新通讯总线", tags = {"通讯总线" },  notes = "批量更新通讯总线")
	@RequestMapping(method = RequestMethod.PUT, value = "/bus_buses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Bus_busDTO> bus_busdtos) {
        bus_busService.updateBatch(bus_busMapping.toDomain(bus_busdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.bus_busService.get(#bus_bus_id),'iBizBusinessCentral-Bus_bus-Remove')")
    @ApiOperation(value = "删除通讯总线", tags = {"通讯总线" },  notes = "删除通讯总线")
	@RequestMapping(method = RequestMethod.DELETE, value = "/bus_buses/{bus_bus_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("bus_bus_id") Long bus_bus_id) {
         return ResponseEntity.status(HttpStatus.OK).body(bus_busService.remove(bus_bus_id));
    }

    @PreAuthorize("hasPermission(this.bus_busService.getBusBusByIds(#ids),'iBizBusinessCentral-Bus_bus-Remove')")
    @ApiOperation(value = "批量删除通讯总线", tags = {"通讯总线" },  notes = "批量删除通讯总线")
	@RequestMapping(method = RequestMethod.DELETE, value = "/bus_buses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        bus_busService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.bus_busMapping.toDomain(returnObject.body),'iBizBusinessCentral-Bus_bus-Get')")
    @ApiOperation(value = "获取通讯总线", tags = {"通讯总线" },  notes = "获取通讯总线")
	@RequestMapping(method = RequestMethod.GET, value = "/bus_buses/{bus_bus_id}")
    public ResponseEntity<Bus_busDTO> get(@PathVariable("bus_bus_id") Long bus_bus_id) {
        Bus_bus domain = bus_busService.get(bus_bus_id);
        Bus_busDTO dto = bus_busMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取通讯总线草稿", tags = {"通讯总线" },  notes = "获取通讯总线草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/bus_buses/getdraft")
    public ResponseEntity<Bus_busDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(bus_busMapping.toDto(bus_busService.getDraft(new Bus_bus())));
    }

    @ApiOperation(value = "检查通讯总线", tags = {"通讯总线" },  notes = "检查通讯总线")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_buses/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Bus_busDTO bus_busdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(bus_busService.checkKey(bus_busMapping.toDomain(bus_busdto)));
    }

    @PreAuthorize("hasPermission(this.bus_busMapping.toDomain(#bus_busdto),'iBizBusinessCentral-Bus_bus-Save')")
    @ApiOperation(value = "保存通讯总线", tags = {"通讯总线" },  notes = "保存通讯总线")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_buses/save")
    public ResponseEntity<Boolean> save(@RequestBody Bus_busDTO bus_busdto) {
        return ResponseEntity.status(HttpStatus.OK).body(bus_busService.save(bus_busMapping.toDomain(bus_busdto)));
    }

    @PreAuthorize("hasPermission(this.bus_busMapping.toDomain(#bus_busdtos),'iBizBusinessCentral-Bus_bus-Save')")
    @ApiOperation(value = "批量保存通讯总线", tags = {"通讯总线" },  notes = "批量保存通讯总线")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_buses/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Bus_busDTO> bus_busdtos) {
        bus_busService.saveBatch(bus_busMapping.toDomain(bus_busdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Bus_bus-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Bus_bus-Get')")
	@ApiOperation(value = "获取数据集", tags = {"通讯总线" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/bus_buses/fetchdefault")
	public ResponseEntity<List<Bus_busDTO>> fetchDefault(Bus_busSearchContext context) {
        Page<Bus_bus> domains = bus_busService.searchDefault(context) ;
        List<Bus_busDTO> list = bus_busMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Bus_bus-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Bus_bus-Get')")
	@ApiOperation(value = "查询数据集", tags = {"通讯总线" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/bus_buses/searchdefault")
	public ResponseEntity<Page<Bus_busDTO>> searchDefault(@RequestBody Bus_busSearchContext context) {
        Page<Bus_bus> domains = bus_busService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(bus_busMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

