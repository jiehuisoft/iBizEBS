package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_routingService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_routingSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工艺" })
@RestController("Core-mrp_routing")
@RequestMapping("")
public class Mrp_routingResource {

    @Autowired
    public IMrp_routingService mrp_routingService;

    @Autowired
    @Lazy
    public Mrp_routingMapping mrp_routingMapping;

    @PreAuthorize("hasPermission(this.mrp_routingMapping.toDomain(#mrp_routingdto),'iBizBusinessCentral-Mrp_routing-Create')")
    @ApiOperation(value = "新建工艺", tags = {"工艺" },  notes = "新建工艺")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_routings")
    public ResponseEntity<Mrp_routingDTO> create(@Validated @RequestBody Mrp_routingDTO mrp_routingdto) {
        Mrp_routing domain = mrp_routingMapping.toDomain(mrp_routingdto);
		mrp_routingService.create(domain);
        Mrp_routingDTO dto = mrp_routingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_routingMapping.toDomain(#mrp_routingdtos),'iBizBusinessCentral-Mrp_routing-Create')")
    @ApiOperation(value = "批量新建工艺", tags = {"工艺" },  notes = "批量新建工艺")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_routings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_routingDTO> mrp_routingdtos) {
        mrp_routingService.createBatch(mrp_routingMapping.toDomain(mrp_routingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_routing" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_routingService.get(#mrp_routing_id),'iBizBusinessCentral-Mrp_routing-Update')")
    @ApiOperation(value = "更新工艺", tags = {"工艺" },  notes = "更新工艺")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_routings/{mrp_routing_id}")
    public ResponseEntity<Mrp_routingDTO> update(@PathVariable("mrp_routing_id") Long mrp_routing_id, @RequestBody Mrp_routingDTO mrp_routingdto) {
		Mrp_routing domain  = mrp_routingMapping.toDomain(mrp_routingdto);
        domain .setId(mrp_routing_id);
		mrp_routingService.update(domain );
		Mrp_routingDTO dto = mrp_routingMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_routingService.getMrpRoutingByEntities(this.mrp_routingMapping.toDomain(#mrp_routingdtos)),'iBizBusinessCentral-Mrp_routing-Update')")
    @ApiOperation(value = "批量更新工艺", tags = {"工艺" },  notes = "批量更新工艺")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_routings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_routingDTO> mrp_routingdtos) {
        mrp_routingService.updateBatch(mrp_routingMapping.toDomain(mrp_routingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_routingService.get(#mrp_routing_id),'iBizBusinessCentral-Mrp_routing-Remove')")
    @ApiOperation(value = "删除工艺", tags = {"工艺" },  notes = "删除工艺")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_routings/{mrp_routing_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_routing_id") Long mrp_routing_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_routingService.remove(mrp_routing_id));
    }

    @PreAuthorize("hasPermission(this.mrp_routingService.getMrpRoutingByIds(#ids),'iBizBusinessCentral-Mrp_routing-Remove')")
    @ApiOperation(value = "批量删除工艺", tags = {"工艺" },  notes = "批量删除工艺")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_routings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_routingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_routingMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_routing-Get')")
    @ApiOperation(value = "获取工艺", tags = {"工艺" },  notes = "获取工艺")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_routings/{mrp_routing_id}")
    public ResponseEntity<Mrp_routingDTO> get(@PathVariable("mrp_routing_id") Long mrp_routing_id) {
        Mrp_routing domain = mrp_routingService.get(mrp_routing_id);
        Mrp_routingDTO dto = mrp_routingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工艺草稿", tags = {"工艺" },  notes = "获取工艺草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_routings/getdraft")
    public ResponseEntity<Mrp_routingDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_routingMapping.toDto(mrp_routingService.getDraft(new Mrp_routing())));
    }

    @ApiOperation(value = "检查工艺", tags = {"工艺" },  notes = "检查工艺")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_routings/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_routingDTO mrp_routingdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_routingService.checkKey(mrp_routingMapping.toDomain(mrp_routingdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_routingMapping.toDomain(#mrp_routingdto),'iBizBusinessCentral-Mrp_routing-Save')")
    @ApiOperation(value = "保存工艺", tags = {"工艺" },  notes = "保存工艺")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_routings/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_routingDTO mrp_routingdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_routingService.save(mrp_routingMapping.toDomain(mrp_routingdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_routingMapping.toDomain(#mrp_routingdtos),'iBizBusinessCentral-Mrp_routing-Save')")
    @ApiOperation(value = "批量保存工艺", tags = {"工艺" },  notes = "批量保存工艺")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_routings/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_routingDTO> mrp_routingdtos) {
        mrp_routingService.saveBatch(mrp_routingMapping.toDomain(mrp_routingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_routing-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_routing-Get')")
	@ApiOperation(value = "获取数据集", tags = {"工艺" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_routings/fetchdefault")
	public ResponseEntity<List<Mrp_routingDTO>> fetchDefault(Mrp_routingSearchContext context) {
        Page<Mrp_routing> domains = mrp_routingService.searchDefault(context) ;
        List<Mrp_routingDTO> list = mrp_routingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_routing-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_routing-Get')")
	@ApiOperation(value = "查询数据集", tags = {"工艺" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_routings/searchdefault")
	public ResponseEntity<Page<Mrp_routingDTO>> searchDefault(@RequestBody Mrp_routingSearchContext context) {
        Page<Mrp_routing> domains = mrp_routingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_routingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

