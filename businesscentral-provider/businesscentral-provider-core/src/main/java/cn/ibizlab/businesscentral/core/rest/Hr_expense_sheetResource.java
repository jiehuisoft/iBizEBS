package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_sheet;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheetService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expense_sheetSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"费用报表" })
@RestController("Core-hr_expense_sheet")
@RequestMapping("")
public class Hr_expense_sheetResource {

    @Autowired
    public IHr_expense_sheetService hr_expense_sheetService;

    @Autowired
    @Lazy
    public Hr_expense_sheetMapping hr_expense_sheetMapping;

    @PreAuthorize("hasPermission(this.hr_expense_sheetMapping.toDomain(#hr_expense_sheetdto),'iBizBusinessCentral-Hr_expense_sheet-Create')")
    @ApiOperation(value = "新建费用报表", tags = {"费用报表" },  notes = "新建费用报表")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets")
    public ResponseEntity<Hr_expense_sheetDTO> create(@Validated @RequestBody Hr_expense_sheetDTO hr_expense_sheetdto) {
        Hr_expense_sheet domain = hr_expense_sheetMapping.toDomain(hr_expense_sheetdto);
		hr_expense_sheetService.create(domain);
        Hr_expense_sheetDTO dto = hr_expense_sheetMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_expense_sheetMapping.toDomain(#hr_expense_sheetdtos),'iBizBusinessCentral-Hr_expense_sheet-Create')")
    @ApiOperation(value = "批量新建费用报表", tags = {"费用报表" },  notes = "批量新建费用报表")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_expense_sheetDTO> hr_expense_sheetdtos) {
        hr_expense_sheetService.createBatch(hr_expense_sheetMapping.toDomain(hr_expense_sheetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_expense_sheet" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_expense_sheetService.get(#hr_expense_sheet_id),'iBizBusinessCentral-Hr_expense_sheet-Update')")
    @ApiOperation(value = "更新费用报表", tags = {"费用报表" },  notes = "更新费用报表")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_sheets/{hr_expense_sheet_id}")
    public ResponseEntity<Hr_expense_sheetDTO> update(@PathVariable("hr_expense_sheet_id") Long hr_expense_sheet_id, @RequestBody Hr_expense_sheetDTO hr_expense_sheetdto) {
		Hr_expense_sheet domain  = hr_expense_sheetMapping.toDomain(hr_expense_sheetdto);
        domain .setId(hr_expense_sheet_id);
		hr_expense_sheetService.update(domain );
		Hr_expense_sheetDTO dto = hr_expense_sheetMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_expense_sheetService.getHrExpenseSheetByEntities(this.hr_expense_sheetMapping.toDomain(#hr_expense_sheetdtos)),'iBizBusinessCentral-Hr_expense_sheet-Update')")
    @ApiOperation(value = "批量更新费用报表", tags = {"费用报表" },  notes = "批量更新费用报表")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_sheets/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_expense_sheetDTO> hr_expense_sheetdtos) {
        hr_expense_sheetService.updateBatch(hr_expense_sheetMapping.toDomain(hr_expense_sheetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_expense_sheetService.get(#hr_expense_sheet_id),'iBizBusinessCentral-Hr_expense_sheet-Remove')")
    @ApiOperation(value = "删除费用报表", tags = {"费用报表" },  notes = "删除费用报表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_sheets/{hr_expense_sheet_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_expense_sheet_id") Long hr_expense_sheet_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_expense_sheetService.remove(hr_expense_sheet_id));
    }

    @PreAuthorize("hasPermission(this.hr_expense_sheetService.getHrExpenseSheetByIds(#ids),'iBizBusinessCentral-Hr_expense_sheet-Remove')")
    @ApiOperation(value = "批量删除费用报表", tags = {"费用报表" },  notes = "批量删除费用报表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_sheets/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_expense_sheetService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_expense_sheetMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_expense_sheet-Get')")
    @ApiOperation(value = "获取费用报表", tags = {"费用报表" },  notes = "获取费用报表")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_expense_sheets/{hr_expense_sheet_id}")
    public ResponseEntity<Hr_expense_sheetDTO> get(@PathVariable("hr_expense_sheet_id") Long hr_expense_sheet_id) {
        Hr_expense_sheet domain = hr_expense_sheetService.get(hr_expense_sheet_id);
        Hr_expense_sheetDTO dto = hr_expense_sheetMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取费用报表草稿", tags = {"费用报表" },  notes = "获取费用报表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_expense_sheets/getdraft")
    public ResponseEntity<Hr_expense_sheetDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_expense_sheetMapping.toDto(hr_expense_sheetService.getDraft(new Hr_expense_sheet())));
    }

    @ApiOperation(value = "检查费用报表", tags = {"费用报表" },  notes = "检查费用报表")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_expense_sheetDTO hr_expense_sheetdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_expense_sheetService.checkKey(hr_expense_sheetMapping.toDomain(hr_expense_sheetdto)));
    }

    @PreAuthorize("hasPermission(this.hr_expense_sheetMapping.toDomain(#hr_expense_sheetdto),'iBizBusinessCentral-Hr_expense_sheet-Save')")
    @ApiOperation(value = "保存费用报表", tags = {"费用报表" },  notes = "保存费用报表")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_expense_sheetDTO hr_expense_sheetdto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_expense_sheetService.save(hr_expense_sheetMapping.toDomain(hr_expense_sheetdto)));
    }

    @PreAuthorize("hasPermission(this.hr_expense_sheetMapping.toDomain(#hr_expense_sheetdtos),'iBizBusinessCentral-Hr_expense_sheet-Save')")
    @ApiOperation(value = "批量保存费用报表", tags = {"费用报表" },  notes = "批量保存费用报表")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_expense_sheetDTO> hr_expense_sheetdtos) {
        hr_expense_sheetService.saveBatch(hr_expense_sheetMapping.toDomain(hr_expense_sheetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_expense_sheet-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_expense_sheet-Get')")
	@ApiOperation(value = "获取数据集", tags = {"费用报表" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_expense_sheets/fetchdefault")
	public ResponseEntity<List<Hr_expense_sheetDTO>> fetchDefault(Hr_expense_sheetSearchContext context) {
        Page<Hr_expense_sheet> domains = hr_expense_sheetService.searchDefault(context) ;
        List<Hr_expense_sheetDTO> list = hr_expense_sheetMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_expense_sheet-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_expense_sheet-Get')")
	@ApiOperation(value = "查询数据集", tags = {"费用报表" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_expense_sheets/searchdefault")
	public ResponseEntity<Page<Hr_expense_sheetDTO>> searchDefault(@RequestBody Hr_expense_sheetSearchContext context) {
        Page<Hr_expense_sheet> domains = hr_expense_sheetService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_expense_sheetMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

