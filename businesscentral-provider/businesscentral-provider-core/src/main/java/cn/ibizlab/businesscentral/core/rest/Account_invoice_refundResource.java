package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_refund;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_refundService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_refundSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"信用票" })
@RestController("Core-account_invoice_refund")
@RequestMapping("")
public class Account_invoice_refundResource {

    @Autowired
    public IAccount_invoice_refundService account_invoice_refundService;

    @Autowired
    @Lazy
    public Account_invoice_refundMapping account_invoice_refundMapping;

    @PreAuthorize("hasPermission(this.account_invoice_refundMapping.toDomain(#account_invoice_refunddto),'iBizBusinessCentral-Account_invoice_refund-Create')")
    @ApiOperation(value = "新建信用票", tags = {"信用票" },  notes = "新建信用票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds")
    public ResponseEntity<Account_invoice_refundDTO> create(@Validated @RequestBody Account_invoice_refundDTO account_invoice_refunddto) {
        Account_invoice_refund domain = account_invoice_refundMapping.toDomain(account_invoice_refunddto);
		account_invoice_refundService.create(domain);
        Account_invoice_refundDTO dto = account_invoice_refundMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoice_refundMapping.toDomain(#account_invoice_refunddtos),'iBizBusinessCentral-Account_invoice_refund-Create')")
    @ApiOperation(value = "批量新建信用票", tags = {"信用票" },  notes = "批量新建信用票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_refundDTO> account_invoice_refunddtos) {
        account_invoice_refundService.createBatch(account_invoice_refundMapping.toDomain(account_invoice_refunddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_invoice_refund" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_invoice_refundService.get(#account_invoice_refund_id),'iBizBusinessCentral-Account_invoice_refund-Update')")
    @ApiOperation(value = "更新信用票", tags = {"信用票" },  notes = "更新信用票")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_refunds/{account_invoice_refund_id}")
    public ResponseEntity<Account_invoice_refundDTO> update(@PathVariable("account_invoice_refund_id") Long account_invoice_refund_id, @RequestBody Account_invoice_refundDTO account_invoice_refunddto) {
		Account_invoice_refund domain  = account_invoice_refundMapping.toDomain(account_invoice_refunddto);
        domain .setId(account_invoice_refund_id);
		account_invoice_refundService.update(domain );
		Account_invoice_refundDTO dto = account_invoice_refundMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoice_refundService.getAccountInvoiceRefundByEntities(this.account_invoice_refundMapping.toDomain(#account_invoice_refunddtos)),'iBizBusinessCentral-Account_invoice_refund-Update')")
    @ApiOperation(value = "批量更新信用票", tags = {"信用票" },  notes = "批量更新信用票")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_refunds/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_refundDTO> account_invoice_refunddtos) {
        account_invoice_refundService.updateBatch(account_invoice_refundMapping.toDomain(account_invoice_refunddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_invoice_refundService.get(#account_invoice_refund_id),'iBizBusinessCentral-Account_invoice_refund-Remove')")
    @ApiOperation(value = "删除信用票", tags = {"信用票" },  notes = "删除信用票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_refunds/{account_invoice_refund_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_refund_id") Long account_invoice_refund_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_refundService.remove(account_invoice_refund_id));
    }

    @PreAuthorize("hasPermission(this.account_invoice_refundService.getAccountInvoiceRefundByIds(#ids),'iBizBusinessCentral-Account_invoice_refund-Remove')")
    @ApiOperation(value = "批量删除信用票", tags = {"信用票" },  notes = "批量删除信用票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_refunds/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_invoice_refundService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_invoice_refundMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_invoice_refund-Get')")
    @ApiOperation(value = "获取信用票", tags = {"信用票" },  notes = "获取信用票")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_refunds/{account_invoice_refund_id}")
    public ResponseEntity<Account_invoice_refundDTO> get(@PathVariable("account_invoice_refund_id") Long account_invoice_refund_id) {
        Account_invoice_refund domain = account_invoice_refundService.get(account_invoice_refund_id);
        Account_invoice_refundDTO dto = account_invoice_refundMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取信用票草稿", tags = {"信用票" },  notes = "获取信用票草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_refunds/getdraft")
    public ResponseEntity<Account_invoice_refundDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_refundMapping.toDto(account_invoice_refundService.getDraft(new Account_invoice_refund())));
    }

    @ApiOperation(value = "检查信用票", tags = {"信用票" },  notes = "检查信用票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoice_refundDTO account_invoice_refunddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_invoice_refundService.checkKey(account_invoice_refundMapping.toDomain(account_invoice_refunddto)));
    }

    @PreAuthorize("hasPermission(this.account_invoice_refundMapping.toDomain(#account_invoice_refunddto),'iBizBusinessCentral-Account_invoice_refund-Save')")
    @ApiOperation(value = "保存信用票", tags = {"信用票" },  notes = "保存信用票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_invoice_refundDTO account_invoice_refunddto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_refundService.save(account_invoice_refundMapping.toDomain(account_invoice_refunddto)));
    }

    @PreAuthorize("hasPermission(this.account_invoice_refundMapping.toDomain(#account_invoice_refunddtos),'iBizBusinessCentral-Account_invoice_refund-Save')")
    @ApiOperation(value = "批量保存信用票", tags = {"信用票" },  notes = "批量保存信用票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_invoice_refundDTO> account_invoice_refunddtos) {
        account_invoice_refundService.saveBatch(account_invoice_refundMapping.toDomain(account_invoice_refunddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_refund-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice_refund-Get')")
	@ApiOperation(value = "获取数据集", tags = {"信用票" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_refunds/fetchdefault")
	public ResponseEntity<List<Account_invoice_refundDTO>> fetchDefault(Account_invoice_refundSearchContext context) {
        Page<Account_invoice_refund> domains = account_invoice_refundService.searchDefault(context) ;
        List<Account_invoice_refundDTO> list = account_invoice_refundMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_refund-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice_refund-Get')")
	@ApiOperation(value = "查询数据集", tags = {"信用票" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_invoice_refunds/searchdefault")
	public ResponseEntity<Page<Account_invoice_refundDTO>> searchDefault(@RequestBody Account_invoice_refundSearchContext context) {
        Page<Account_invoice_refund> domains = account_invoice_refundService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_refundMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

