package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Ir_attachmentDTO]
 */
@Data
public class Ir_attachmentDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [RES_FIELD]
     *
     */
    @JSONField(name = "res_field")
    @JsonProperty("res_field")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String resField;

    /**
     * 属性 [FILE_SIZE]
     *
     */
    @JSONField(name = "file_size")
    @JsonProperty("file_size")
    private Integer fileSize;

    /**
     * 属性 [STORE_FNAME]
     *
     */
    @JSONField(name = "store_fname")
    @JsonProperty("store_fname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String storeFname;

    /**
     * 属性 [PUBLIC]
     *
     */
    @JSONField(name = "ibizpublic")
    @JsonProperty("ibizpublic")
    private Boolean ibizpublic;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String type;

    /**
     * 属性 [RES_ID]
     *
     */
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 属性 [CHECKSUM]
     *
     */
    @JSONField(name = "checksum")
    @JsonProperty("checksum")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    private String checksum;

    /**
     * 属性 [RES_MODEL]
     *
     */
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String resModel;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String description;

    /**
     * 属性 [ACCESS_TOKEN]
     *
     */
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String accessToken;

    /**
     * 属性 [URL]
     *
     */
    @JSONField(name = "url")
    @JsonProperty("url")
    @Size(min = 0, max = 1024, message = "内容长度必须小于等于[1024]")
    private String url;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [KEY]
     *
     */
    @JSONField(name = "key")
    @JsonProperty("key")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String key;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [MIMETYPE]
     *
     */
    @JSONField(name = "mimetype")
    @JsonProperty("mimetype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mimetype;

    /**
     * 属性 [INDEX_CONTENT]
     *
     */
    @JSONField(name = "index_content")
    @JsonProperty("index_content")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String indexContent;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;


    /**
     * 设置 [RES_FIELD]
     */
    public void setResField(String  resField){
        this.resField = resField ;
        this.modify("res_field",resField);
    }

    /**
     * 设置 [FILE_SIZE]
     */
    public void setFileSize(Integer  fileSize){
        this.fileSize = fileSize ;
        this.modify("file_size",fileSize);
    }

    /**
     * 设置 [STORE_FNAME]
     */
    public void setStoreFname(String  storeFname){
        this.storeFname = storeFname ;
        this.modify("store_fname",storeFname);
    }

    /**
     * 设置 [PUBLIC]
     */
    public void setIbizpublic(Boolean  ibizpublic){
        this.ibizpublic = ibizpublic ;
        this.modify("public",ibizpublic);
    }

    /**
     * 设置 [TYPE]
     */
    public void setType(String  type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [RES_ID]
     */
    public void setResId(Integer  resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [CHECKSUM]
     */
    public void setChecksum(String  checksum){
        this.checksum = checksum ;
        this.modify("checksum",checksum);
    }

    /**
     * 设置 [RES_MODEL]
     */
    public void setResModel(String  resModel){
        this.resModel = resModel ;
        this.modify("res_model",resModel);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [ACCESS_TOKEN]
     */
    public void setAccessToken(String  accessToken){
        this.accessToken = accessToken ;
        this.modify("access_token",accessToken);
    }

    /**
     * 设置 [URL]
     */
    public void setUrl(String  url){
        this.url = url ;
        this.modify("url",url);
    }

    /**
     * 设置 [KEY]
     */
    public void setKey(String  key){
        this.key = key ;
        this.modify("key",key);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [MIMETYPE]
     */
    public void setMimetype(String  mimetype){
        this.mimetype = mimetype ;
        this.modify("mimetype",mimetype);
    }

    /**
     * 设置 [INDEX_CONTENT]
     */
    public void setIndexContent(String  indexContent){
        this.indexContent = indexContent ;
        this.modify("index_content",indexContent);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }


}


