package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order_make_invoice;
import cn.ibizlab.businesscentral.core.dto.Repair_order_make_invoiceDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreRepair_order_make_invoiceMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Repair_order_make_invoiceMapping extends MappingBase<Repair_order_make_invoiceDTO, Repair_order_make_invoice> {


}

