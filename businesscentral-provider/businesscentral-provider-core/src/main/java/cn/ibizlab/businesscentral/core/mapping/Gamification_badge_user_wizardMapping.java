package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge_user_wizard;
import cn.ibizlab.businesscentral.core.dto.Gamification_badge_user_wizardDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreGamification_badge_user_wizardMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Gamification_badge_user_wizardMapping extends MappingBase<Gamification_badge_user_wizardDTO, Gamification_badge_user_wizard> {


}

