package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_mail_registration;
import cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_mail_registrationService;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_mail_registrationSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"登记邮件调度" })
@RestController("Core-event_mail_registration")
@RequestMapping("")
public class Event_mail_registrationResource {

    @Autowired
    public IEvent_mail_registrationService event_mail_registrationService;

    @Autowired
    @Lazy
    public Event_mail_registrationMapping event_mail_registrationMapping;

    @PreAuthorize("hasPermission(this.event_mail_registrationMapping.toDomain(#event_mail_registrationdto),'iBizBusinessCentral-Event_mail_registration-Create')")
    @ApiOperation(value = "新建登记邮件调度", tags = {"登记邮件调度" },  notes = "新建登记邮件调度")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mail_registrations")
    public ResponseEntity<Event_mail_registrationDTO> create(@Validated @RequestBody Event_mail_registrationDTO event_mail_registrationdto) {
        Event_mail_registration domain = event_mail_registrationMapping.toDomain(event_mail_registrationdto);
		event_mail_registrationService.create(domain);
        Event_mail_registrationDTO dto = event_mail_registrationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_mail_registrationMapping.toDomain(#event_mail_registrationdtos),'iBizBusinessCentral-Event_mail_registration-Create')")
    @ApiOperation(value = "批量新建登记邮件调度", tags = {"登记邮件调度" },  notes = "批量新建登记邮件调度")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mail_registrations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_mail_registrationDTO> event_mail_registrationdtos) {
        event_mail_registrationService.createBatch(event_mail_registrationMapping.toDomain(event_mail_registrationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "event_mail_registration" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.event_mail_registrationService.get(#event_mail_registration_id),'iBizBusinessCentral-Event_mail_registration-Update')")
    @ApiOperation(value = "更新登记邮件调度", tags = {"登记邮件调度" },  notes = "更新登记邮件调度")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_mail_registrations/{event_mail_registration_id}")
    public ResponseEntity<Event_mail_registrationDTO> update(@PathVariable("event_mail_registration_id") Long event_mail_registration_id, @RequestBody Event_mail_registrationDTO event_mail_registrationdto) {
		Event_mail_registration domain  = event_mail_registrationMapping.toDomain(event_mail_registrationdto);
        domain .setId(event_mail_registration_id);
		event_mail_registrationService.update(domain );
		Event_mail_registrationDTO dto = event_mail_registrationMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_mail_registrationService.getEventMailRegistrationByEntities(this.event_mail_registrationMapping.toDomain(#event_mail_registrationdtos)),'iBizBusinessCentral-Event_mail_registration-Update')")
    @ApiOperation(value = "批量更新登记邮件调度", tags = {"登记邮件调度" },  notes = "批量更新登记邮件调度")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_mail_registrations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_mail_registrationDTO> event_mail_registrationdtos) {
        event_mail_registrationService.updateBatch(event_mail_registrationMapping.toDomain(event_mail_registrationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.event_mail_registrationService.get(#event_mail_registration_id),'iBizBusinessCentral-Event_mail_registration-Remove')")
    @ApiOperation(value = "删除登记邮件调度", tags = {"登记邮件调度" },  notes = "删除登记邮件调度")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_mail_registrations/{event_mail_registration_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("event_mail_registration_id") Long event_mail_registration_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_mail_registrationService.remove(event_mail_registration_id));
    }

    @PreAuthorize("hasPermission(this.event_mail_registrationService.getEventMailRegistrationByIds(#ids),'iBizBusinessCentral-Event_mail_registration-Remove')")
    @ApiOperation(value = "批量删除登记邮件调度", tags = {"登记邮件调度" },  notes = "批量删除登记邮件调度")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_mail_registrations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        event_mail_registrationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.event_mail_registrationMapping.toDomain(returnObject.body),'iBizBusinessCentral-Event_mail_registration-Get')")
    @ApiOperation(value = "获取登记邮件调度", tags = {"登记邮件调度" },  notes = "获取登记邮件调度")
	@RequestMapping(method = RequestMethod.GET, value = "/event_mail_registrations/{event_mail_registration_id}")
    public ResponseEntity<Event_mail_registrationDTO> get(@PathVariable("event_mail_registration_id") Long event_mail_registration_id) {
        Event_mail_registration domain = event_mail_registrationService.get(event_mail_registration_id);
        Event_mail_registrationDTO dto = event_mail_registrationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取登记邮件调度草稿", tags = {"登记邮件调度" },  notes = "获取登记邮件调度草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/event_mail_registrations/getdraft")
    public ResponseEntity<Event_mail_registrationDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(event_mail_registrationMapping.toDto(event_mail_registrationService.getDraft(new Event_mail_registration())));
    }

    @ApiOperation(value = "检查登记邮件调度", tags = {"登记邮件调度" },  notes = "检查登记邮件调度")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mail_registrations/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Event_mail_registrationDTO event_mail_registrationdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(event_mail_registrationService.checkKey(event_mail_registrationMapping.toDomain(event_mail_registrationdto)));
    }

    @PreAuthorize("hasPermission(this.event_mail_registrationMapping.toDomain(#event_mail_registrationdto),'iBizBusinessCentral-Event_mail_registration-Save')")
    @ApiOperation(value = "保存登记邮件调度", tags = {"登记邮件调度" },  notes = "保存登记邮件调度")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mail_registrations/save")
    public ResponseEntity<Boolean> save(@RequestBody Event_mail_registrationDTO event_mail_registrationdto) {
        return ResponseEntity.status(HttpStatus.OK).body(event_mail_registrationService.save(event_mail_registrationMapping.toDomain(event_mail_registrationdto)));
    }

    @PreAuthorize("hasPermission(this.event_mail_registrationMapping.toDomain(#event_mail_registrationdtos),'iBizBusinessCentral-Event_mail_registration-Save')")
    @ApiOperation(value = "批量保存登记邮件调度", tags = {"登记邮件调度" },  notes = "批量保存登记邮件调度")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mail_registrations/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Event_mail_registrationDTO> event_mail_registrationdtos) {
        event_mail_registrationService.saveBatch(event_mail_registrationMapping.toDomain(event_mail_registrationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_mail_registration-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_mail_registration-Get')")
	@ApiOperation(value = "获取数据集", tags = {"登记邮件调度" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/event_mail_registrations/fetchdefault")
	public ResponseEntity<List<Event_mail_registrationDTO>> fetchDefault(Event_mail_registrationSearchContext context) {
        Page<Event_mail_registration> domains = event_mail_registrationService.searchDefault(context) ;
        List<Event_mail_registrationDTO> list = event_mail_registrationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_mail_registration-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_mail_registration-Get')")
	@ApiOperation(value = "查询数据集", tags = {"登记邮件调度" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/event_mail_registrations/searchdefault")
	public ResponseEntity<Page<Event_mail_registrationDTO>> searchDefault(@RequestBody Event_mail_registrationSearchContext context) {
        Page<Event_mail_registration> domains = event_mail_registrationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_mail_registrationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

