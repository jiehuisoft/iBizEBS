package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Res_usersDTO]
 */
@Data
public class Res_usersDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [IS_MODERATOR]
     *
     */
    @JSONField(name = "is_moderator")
    @JsonProperty("is_moderator")
    private Boolean isModerator;

    /**
     * 属性 [RESOURCE_IDS]
     *
     */
    @JSONField(name = "resource_ids")
    @JsonProperty("resource_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String resourceIds;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String categoryId;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Integer resourceCalendarId;

    /**
     * 属性 [LOG_IDS]
     *
     */
    @JSONField(name = "log_ids")
    @JsonProperty("log_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String logIds;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [BADGE_IDS]
     *
     */
    @JSONField(name = "badge_ids")
    @JsonProperty("badge_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String badgeIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [COMPANY_IDS]
     *
     */
    @JSONField(name = "company_ids")
    @JsonProperty("company_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String companyIds;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String childIds;

    /**
     * 属性 [TZ_OFFSET]
     *
     */
    @JSONField(name = "tz_offset")
    @JsonProperty("tz_offset")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String tzOffset;

    /**
     * 属性 [TARGET_SALES_DONE]
     *
     */
    @JSONField(name = "target_sales_done")
    @JsonProperty("target_sales_done")
    private Integer targetSalesDone;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [NOTIFICATION_TYPE]
     *
     */
    @JSONField(name = "notification_type")
    @JsonProperty("notification_type")
    @NotBlank(message = "[通知管理]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String notificationType;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [IM_STATUS]
     *
     */
    @JSONField(name = "im_status")
    @JsonProperty("im_status")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String imStatus;

    /**
     * 属性 [KARMA]
     *
     */
    @JSONField(name = "karma")
    @JsonProperty("karma")
    private Integer karma;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activityIds;

    /**
     * 属性 [GOLD_BADGE]
     *
     */
    @JSONField(name = "gold_badge")
    @JsonProperty("gold_badge")
    private Integer goldBadge;

    /**
     * 属性 [EMPLOYEE_IDS]
     *
     */
    @JSONField(name = "employee_ids")
    @JsonProperty("employee_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String employeeIds;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [MODERATION_CHANNEL_IDS]
     *
     */
    @JSONField(name = "moderation_channel_ids")
    @JsonProperty("moderation_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String moderationChannelIds;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [SILVER_BADGE]
     *
     */
    @JSONField(name = "silver_badge")
    @JsonProperty("silver_badge")
    private Integer silverBadge;

    /**
     * 属性 [PAYMENT_TOKEN_IDS]
     *
     */
    @JSONField(name = "payment_token_ids")
    @JsonProperty("payment_token_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String paymentTokenIds;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [COMPANIES_COUNT]
     *
     */
    @JSONField(name = "companies_count")
    @JsonProperty("companies_count")
    private Integer companiesCount;

    /**
     * 属性 [TARGET_SALES_INVOICED]
     *
     */
    @JSONField(name = "target_sales_invoiced")
    @JsonProperty("target_sales_invoiced")
    private Integer targetSalesInvoiced;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [LOGIN_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "login_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("login_date")
    private Timestamp loginDate;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String channelIds;

    /**
     * 属性 [GROUPS_ID]
     *
     */
    @JSONField(name = "groups_id")
    @JsonProperty("groups_id")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String groupsId;

    /**
     * 属性 [SHARE]
     *
     */
    @JSONField(name = "share")
    @JsonProperty("share")
    private Boolean share;

    /**
     * 属性 [BANK_IDS]
     *
     */
    @JSONField(name = "bank_ids")
    @JsonProperty("bank_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String bankIds;

    /**
     * 属性 [SALE_ORDER_IDS]
     *
     */
    @JSONField(name = "sale_order_ids")
    @JsonProperty("sale_order_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String saleOrderIds;

    /**
     * 属性 [NEW_PASSWORD]
     *
     */
    @JSONField(name = "new_password")
    @JsonProperty("new_password")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String newPassword;

    /**
     * 属性 [ODOOBOT_STATE]
     *
     */
    @JSONField(name = "odoobot_state")
    @JsonProperty("odoobot_state")
    @NotBlank(message = "[OdooBot 状态]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String odoobotState;

    /**
     * 属性 [REF_COMPANY_IDS]
     *
     */
    @JSONField(name = "ref_company_ids")
    @JsonProperty("ref_company_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String refCompanyIds;

    /**
     * 属性 [PASSWORD]
     *
     */
    @JSONField(name = "password")
    @JsonProperty("password")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String password;

    /**
     * 属性 [BRONZE_BADGE]
     *
     */
    @JSONField(name = "bronze_badge")
    @JsonProperty("bronze_badge")
    private Integer bronzeBadge;

    /**
     * 属性 [MEETING_IDS]
     *
     */
    @JSONField(name = "meeting_ids")
    @JsonProperty("meeting_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String meetingIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [FORUM_WAITING_POSTS_COUNT]
     *
     */
    @JSONField(name = "forum_waiting_posts_count")
    @JsonProperty("forum_waiting_posts_count")
    private Integer forumWaitingPostsCount;

    /**
     * 属性 [GOAL_IDS]
     *
     */
    @JSONField(name = "goal_ids")
    @JsonProperty("goal_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String goalIds;

    /**
     * 属性 [TARGET_SALES_WON]
     *
     */
    @JSONField(name = "target_sales_won")
    @JsonProperty("target_sales_won")
    private Integer targetSalesWon;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [ACTION_ID]
     *
     */
    @JSONField(name = "action_id")
    @JsonProperty("action_id")
    private Integer actionId;

    /**
     * 属性 [LOGIN]
     *
     */
    @JSONField(name = "login")
    @JsonProperty("login")
    @NotBlank(message = "[登录]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String login;

    /**
     * 属性 [CONTRACT_IDS]
     *
     */
    @JSONField(name = "contract_ids")
    @JsonProperty("contract_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String contractIds;

    /**
     * 属性 [MODERATION_COUNTER]
     *
     */
    @JSONField(name = "moderation_counter")
    @JsonProperty("moderation_counter")
    private Integer moderationCounter;

    /**
     * 属性 [SIGNATURE]
     *
     */
    @JSONField(name = "signature")
    @JsonProperty("signature")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String signature;

    /**
     * 属性 [USER_IDS]
     *
     */
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String userIds;

    /**
     * 属性 [OPPORTUNITY_IDS]
     *
     */
    @JSONField(name = "opportunity_ids")
    @JsonProperty("opportunity_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String opportunityIds;

    /**
     * 属性 [TASK_IDS]
     *
     */
    @JSONField(name = "task_ids")
    @JsonProperty("task_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String taskIds;

    /**
     * 属性 [INVOICE_IDS]
     *
     */
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String invoiceIds;

    /**
     * 属性 [REF]
     *
     */
    @JSONField(name = "ref")
    @JsonProperty("ref")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String ref;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [LAST_WEBSITE_SO_ID]
     *
     */
    @JSONField(name = "last_website_so_id")
    @JsonProperty("last_website_so_id")
    private Integer lastWebsiteSoId;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [PROPERTY_STOCK_CUSTOMER]
     *
     */
    @JSONField(name = "property_stock_customer")
    @JsonProperty("property_stock_customer")
    private Integer propertyStockCustomer;

    /**
     * 属性 [LAST_TIME_ENTRIES_CHECKED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_time_entries_checked" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("last_time_entries_checked")
    private Timestamp lastTimeEntriesChecked;

    /**
     * 属性 [LANG]
     *
     */
    @JSONField(name = "lang")
    @JsonProperty("lang")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lang;

    /**
     * 属性 [SALE_WARN]
     *
     */
    @JSONField(name = "sale_warn")
    @JsonProperty("sale_warn")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String saleWarn;

    /**
     * 属性 [MEETING_COUNT]
     *
     */
    @JSONField(name = "meeting_count")
    @JsonProperty("meeting_count")
    private Integer meetingCount;

    /**
     * 属性 [STREET]
     *
     */
    @JSONField(name = "street")
    @JsonProperty("street")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String street;

    /**
     * 属性 [INVOICE_WARN]
     *
     */
    @JSONField(name = "invoice_warn")
    @JsonProperty("invoice_warn")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String invoiceWarn;

    /**
     * 属性 [SIGNUP_TOKEN]
     *
     */
    @JSONField(name = "signup_token")
    @JsonProperty("signup_token")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String signupToken;

    /**
     * 属性 [TASK_COUNT]
     *
     */
    @JSONField(name = "task_count")
    @JsonProperty("task_count")
    private Integer taskCount;

    /**
     * 属性 [SIGNUP_VALID]
     *
     */
    @JSONField(name = "signup_valid")
    @JsonProperty("signup_valid")
    private Boolean signupValid;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [SIGNUP_TYPE]
     *
     */
    @JSONField(name = "signup_type")
    @JsonProperty("signup_type")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String signupType;

    /**
     * 属性 [PROPERTY_ACCOUNT_RECEIVABLE_ID]
     *
     */
    @JSONField(name = "property_account_receivable_id")
    @JsonProperty("property_account_receivable_id")
    @NotNull(message = "[应收账款]不允许为空!")
    private Integer propertyAccountReceivableId;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteMetaOgImg;

    /**
     * 属性 [EVENT_COUNT]
     *
     */
    @JSONField(name = "event_count")
    @JsonProperty("event_count")
    private Integer eventCount;

    /**
     * 属性 [JOURNAL_ITEM_COUNT]
     *
     */
    @JSONField(name = "journal_item_count")
    @JsonProperty("journal_item_count")
    private Integer journalItemCount;

    /**
     * 属性 [PARENT_NAME]
     *
     */
    @JSONField(name = "parent_name")
    @JsonProperty("parent_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String parentName;

    /**
     * 属性 [SALE_TEAM_ID_TEXT]
     *
     */
    @JSONField(name = "sale_team_id_text")
    @JsonProperty("sale_team_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String saleTeamIdText;

    /**
     * 属性 [CREDIT]
     *
     */
    @JSONField(name = "credit")
    @JsonProperty("credit")
    private BigDecimal credit;

    /**
     * 属性 [OPPORTUNITY_COUNT]
     *
     */
    @JSONField(name = "opportunity_count")
    @JsonProperty("opportunity_count")
    private Integer opportunityCount;

    /**
     * 属性 [SIGNUP_URL]
     *
     */
    @JSONField(name = "signup_url")
    @JsonProperty("signup_url")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String signupUrl;

    /**
     * 属性 [PROPERTY_ACCOUNT_PAYABLE_ID]
     *
     */
    @JSONField(name = "property_account_payable_id")
    @JsonProperty("property_account_payable_id")
    @NotNull(message = "[应付账款]不允许为空!")
    private Integer propertyAccountPayableId;

    /**
     * 属性 [STATE_ID]
     *
     */
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stateId;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [SALE_WARN_MSG]
     *
     */
    @JSONField(name = "sale_warn_msg")
    @JsonProperty("sale_warn_msg")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String saleWarnMsg;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;

    /**
     * 属性 [TOTAL_INVOICED]
     *
     */
    @JSONField(name = "total_invoiced")
    @JsonProperty("total_invoiced")
    private BigDecimal totalInvoiced;

    /**
     * 属性 [DEBIT]
     *
     */
    @JSONField(name = "debit")
    @JsonProperty("debit")
    private BigDecimal debit;

    /**
     * 属性 [POS_ORDER_COUNT]
     *
     */
    @JSONField(name = "pos_order_count")
    @JsonProperty("pos_order_count")
    private Integer posOrderCount;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [TITLE]
     *
     */
    @JSONField(name = "title")
    @JsonProperty("title")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long title;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [SUPPLIER_INVOICE_COUNT]
     *
     */
    @JSONField(name = "supplier_invoice_count")
    @JsonProperty("supplier_invoice_count")
    private Integer supplierInvoiceCount;

    /**
     * 属性 [CITY]
     *
     */
    @JSONField(name = "city")
    @JsonProperty("city")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String city;

    /**
     * 属性 [PICKING_WARN]
     *
     */
    @JSONField(name = "picking_warn")
    @JsonProperty("picking_warn")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pickingWarn;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [ADDITIONAL_INFO]
     *
     */
    @JSONField(name = "additional_info")
    @JsonProperty("additional_info")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String additionalInfo;

    /**
     * 属性 [FUNCTION]
     *
     */
    @JSONField(name = "function")
    @JsonProperty("function")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String function;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteUrl;

    /**
     * 属性 [CALENDAR_LAST_NOTIF_ACK]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "calendar_last_notif_ack" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("calendar_last_notif_ack")
    private Timestamp calendarLastNotifAck;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [EMPLOYEE]
     *
     */
    @JSONField(name = "employee")
    @JsonProperty("employee")
    private Boolean employee;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityState;

    /**
     * 属性 [BARCODE]
     *
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String barcode;

    /**
     * 属性 [PARTNER_GID]
     *
     */
    @JSONField(name = "partner_gid")
    @JsonProperty("partner_gid")
    private Integer partnerGid;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String type;

    /**
     * 属性 [VAT]
     *
     */
    @JSONField(name = "vat")
    @JsonProperty("vat")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String vat;

    /**
     * 属性 [PURCHASE_WARN_MSG]
     *
     */
    @JSONField(name = "purchase_warn_msg")
    @JsonProperty("purchase_warn_msg")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String purchaseWarnMsg;

    /**
     * 属性 [COMMENT]
     *
     */
    @JSONField(name = "comment")
    @JsonProperty("comment")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String comment;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteMetaKeywords;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /**
     * 属性 [PURCHASE_WARN]
     *
     */
    @JSONField(name = "purchase_warn")
    @JsonProperty("purchase_warn")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String purchaseWarn;

    /**
     * 属性 [ACTIVE_PARTNER]
     *
     */
    @JSONField(name = "active_partner")
    @JsonProperty("active_partner")
    private Boolean activePartner;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [INDUSTRY_ID]
     *
     */
    @JSONField(name = "industry_id")
    @JsonProperty("industry_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long industryId;

    /**
     * 属性 [PROPERTY_STOCK_SUPPLIER]
     *
     */
    @JSONField(name = "property_stock_supplier")
    @JsonProperty("property_stock_supplier")
    private Integer propertyStockSupplier;

    /**
     * 属性 [PAYMENT_TOKEN_COUNT]
     *
     */
    @JSONField(name = "payment_token_count")
    @JsonProperty("payment_token_count")
    private Integer paymentTokenCount;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [PROPERTY_PAYMENT_TERM_ID]
     *
     */
    @JSONField(name = "property_payment_term_id")
    @JsonProperty("property_payment_term_id")
    private Integer propertyPaymentTermId;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [CONTRACTS_COUNT]
     *
     */
    @JSONField(name = "contracts_count")
    @JsonProperty("contracts_count")
    private Integer contractsCount;

    /**
     * 属性 [SELF]
     *
     */
    @JSONField(name = "self")
    @JsonProperty("self")
    private Integer self;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMetaDescription;

    /**
     * 属性 [IMAGE]
     *
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 属性 [EMAIL]
     *
     */
    @JSONField(name = "email")
    @JsonProperty("email")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String email;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String activitySummary;

    /**
     * 属性 [DEBIT_LIMIT]
     *
     */
    @JSONField(name = "debit_limit")
    @JsonProperty("debit_limit")
    private BigDecimal debitLimit;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long countryId;

    /**
     * 属性 [CREDIT_LIMIT]
     *
     */
    @JSONField(name = "credit_limit")
    @JsonProperty("credit_limit")
    private Double creditLimit;

    /**
     * 属性 [COMMERCIAL_COMPANY_NAME]
     *
     */
    @JSONField(name = "commercial_company_name")
    @JsonProperty("commercial_company_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String commercialCompanyName;

    /**
     * 属性 [INVOICE_WARN_MSG]
     *
     */
    @JSONField(name = "invoice_warn_msg")
    @JsonProperty("invoice_warn_msg")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String invoiceWarnMsg;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private Boolean isPublished;

    /**
     * 属性 [TRUST]
     *
     */
    @JSONField(name = "trust")
    @JsonProperty("trust")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String trust;

    /**
     * 属性 [MOBILE]
     *
     */
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String mobile;

    /**
     * 属性 [EMAIL_FORMATTED]
     *
     */
    @JSONField(name = "email_formatted")
    @JsonProperty("email_formatted")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emailFormatted;

    /**
     * 属性 [IS_COMPANY]
     *
     */
    @JSONField(name = "is_company")
    @JsonProperty("is_company")
    private Boolean isCompany;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long teamId;

    /**
     * 属性 [IS_BLACKLISTED]
     *
     */
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private Boolean isBlacklisted;

    /**
     * 属性 [BANK_ACCOUNT_COUNT]
     *
     */
    @JSONField(name = "bank_account_count")
    @JsonProperty("bank_account_count")
    private Integer bankAccountCount;

    /**
     * 属性 [PROPERTY_PRODUCT_PRICELIST]
     *
     */
    @JSONField(name = "property_product_pricelist")
    @JsonProperty("property_product_pricelist")
    private Integer propertyProductPricelist;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [PICKING_WARN_MSG]
     *
     */
    @JSONField(name = "picking_warn_msg")
    @JsonProperty("picking_warn_msg")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String pickingWarnMsg;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [WEBSITE]
     *
     */
    @JSONField(name = "website")
    @JsonProperty("website")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String website;

    /**
     * 属性 [PHONE]
     *
     */
    @JSONField(name = "phone")
    @JsonProperty("phone")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String phone;

    /**
     * 属性 [STREET2]
     *
     */
    @JSONField(name = "street2")
    @JsonProperty("street2")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String street2;

    /**
     * 属性 [HAS_UNRECONCILED_ENTRIES]
     *
     */
    @JSONField(name = "has_unreconciled_entries")
    @JsonProperty("has_unreconciled_entries")
    private Boolean hasUnreconciledEntries;

    /**
     * 属性 [SIGNUP_EXPIRATION]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "signup_expiration" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("signup_expiration")
    private Timestamp signupExpiration;

    /**
     * 属性 [TZ]
     *
     */
    @JSONField(name = "tz")
    @JsonProperty("tz")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String tz;

    /**
     * 属性 [CONTACT_ADDRESS]
     *
     */
    @JSONField(name = "contact_address")
    @JsonProperty("contact_address")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String contactAddress;

    /**
     * 属性 [WEBSITE_SHORT_DESCRIPTION]
     *
     */
    @JSONField(name = "website_short_description")
    @JsonProperty("website_short_description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteShortDescription;

    /**
     * 属性 [PARTNER_SHARE]
     *
     */
    @JSONField(name = "partner_share")
    @JsonProperty("partner_share")
    private Boolean partnerShare;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [ZIP]
     *
     */
    @JSONField(name = "zip")
    @JsonProperty("zip")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String zip;

    /**
     * 属性 [SALE_ORDER_COUNT]
     *
     */
    @JSONField(name = "sale_order_count")
    @JsonProperty("sale_order_count")
    private Integer saleOrderCount;

    /**
     * 属性 [COMPANY_TYPE]
     *
     */
    @JSONField(name = "company_type")
    @JsonProperty("company_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyType;

    /**
     * 属性 [PROPERTY_ACCOUNT_POSITION_ID]
     *
     */
    @JSONField(name = "property_account_position_id")
    @JsonProperty("property_account_position_id")
    private Integer propertyAccountPositionId;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private Boolean isSeoOptimized;

    /**
     * 属性 [MESSAGE_BOUNCE]
     *
     */
    @JSONField(name = "message_bounce")
    @JsonProperty("message_bounce")
    private Integer messageBounce;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteMetaTitle;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 属性 [PROPERTY_PURCHASE_CURRENCY_ID]
     *
     */
    @JSONField(name = "property_purchase_currency_id")
    @JsonProperty("property_purchase_currency_id")
    private Integer propertyPurchaseCurrencyId;

    /**
     * 属性 [PURCHASE_ORDER_COUNT]
     *
     */
    @JSONField(name = "purchase_order_count")
    @JsonProperty("purchase_order_count")
    private Integer purchaseOrderCount;

    /**
     * 属性 [WEBSITE_DESCRIPTION]
     *
     */
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteDescription;

    /**
     * 属性 [PROPERTY_SUPPLIER_PAYMENT_TERM_ID]
     *
     */
    @JSONField(name = "property_supplier_payment_term_id")
    @JsonProperty("property_supplier_payment_term_id")
    private Integer propertySupplierPaymentTermId;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID]
     *
     */
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long commercialPartnerId;

    /**
     * 属性 [ALIAS_CONTACT]
     *
     */
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String aliasContact;

    /**
     * 属性 [COMPANY_NAME]
     *
     */
    @JSONField(name = "company_name")
    @JsonProperty("company_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String companyName;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[公司]不允许为空!")
    private Long companyId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[相关的业务伙伴]不允许为空!")
    private Long partnerId;

    /**
     * 属性 [ALIAS_ID]
     *
     */
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long aliasId;

    /**
     * 属性 [SALE_TEAM_ID]
     *
     */
    @JSONField(name = "sale_team_id")
    @JsonProperty("sale_team_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long saleTeamId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [TARGET_SALES_DONE]
     */
    public void setTargetSalesDone(Integer  targetSalesDone){
        this.targetSalesDone = targetSalesDone ;
        this.modify("target_sales_done",targetSalesDone);
    }

    /**
     * 设置 [NOTIFICATION_TYPE]
     */
    public void setNotificationType(String  notificationType){
        this.notificationType = notificationType ;
        this.modify("notification_type",notificationType);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [KARMA]
     */
    public void setKarma(Integer  karma){
        this.karma = karma ;
        this.modify("karma",karma);
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    public void setWebsiteId(Integer  websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [TARGET_SALES_INVOICED]
     */
    public void setTargetSalesInvoiced(Integer  targetSalesInvoiced){
        this.targetSalesInvoiced = targetSalesInvoiced ;
        this.modify("target_sales_invoiced",targetSalesInvoiced);
    }

    /**
     * 设置 [SHARE]
     */
    public void setShare(Boolean  share){
        this.share = share ;
        this.modify("share",share);
    }

    /**
     * 设置 [ODOOBOT_STATE]
     */
    public void setOdoobotState(String  odoobotState){
        this.odoobotState = odoobotState ;
        this.modify("odoobot_state",odoobotState);
    }

    /**
     * 设置 [TARGET_SALES_WON]
     */
    public void setTargetSalesWon(Integer  targetSalesWon){
        this.targetSalesWon = targetSalesWon ;
        this.modify("target_sales_won",targetSalesWon);
    }

    /**
     * 设置 [ACTION_ID]
     */
    public void setActionId(Integer  actionId){
        this.actionId = actionId ;
        this.modify("action_id",actionId);
    }

    /**
     * 设置 [LOGIN]
     */
    public void setLogin(String  login){
        this.login = login ;
        this.modify("login",login);
    }

    /**
     * 设置 [SIGNATURE]
     */
    public void setSignature(String  signature){
        this.signature = signature ;
        this.modify("signature",signature);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [ALIAS_ID]
     */
    public void setAliasId(Long  aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }

    /**
     * 设置 [SALE_TEAM_ID]
     */
    public void setSaleTeamId(Long  saleTeamId){
        this.saleTeamId = saleTeamId ;
        this.modify("sale_team_id",saleTeamId);
    }


}


