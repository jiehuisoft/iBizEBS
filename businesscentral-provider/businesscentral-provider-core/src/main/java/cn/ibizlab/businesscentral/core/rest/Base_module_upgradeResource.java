package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_module_upgrade;
import cn.ibizlab.businesscentral.core.odoo_base.service.IBase_module_upgradeService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_module_upgradeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"升级模块" })
@RestController("Core-base_module_upgrade")
@RequestMapping("")
public class Base_module_upgradeResource {

    @Autowired
    public IBase_module_upgradeService base_module_upgradeService;

    @Autowired
    @Lazy
    public Base_module_upgradeMapping base_module_upgradeMapping;

    @PreAuthorize("hasPermission(this.base_module_upgradeMapping.toDomain(#base_module_upgradedto),'iBizBusinessCentral-Base_module_upgrade-Create')")
    @ApiOperation(value = "新建升级模块", tags = {"升级模块" },  notes = "新建升级模块")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_upgrades")
    public ResponseEntity<Base_module_upgradeDTO> create(@Validated @RequestBody Base_module_upgradeDTO base_module_upgradedto) {
        Base_module_upgrade domain = base_module_upgradeMapping.toDomain(base_module_upgradedto);
		base_module_upgradeService.create(domain);
        Base_module_upgradeDTO dto = base_module_upgradeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_module_upgradeMapping.toDomain(#base_module_upgradedtos),'iBizBusinessCentral-Base_module_upgrade-Create')")
    @ApiOperation(value = "批量新建升级模块", tags = {"升级模块" },  notes = "批量新建升级模块")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_upgrades/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_module_upgradeDTO> base_module_upgradedtos) {
        base_module_upgradeService.createBatch(base_module_upgradeMapping.toDomain(base_module_upgradedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_module_upgrade" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_module_upgradeService.get(#base_module_upgrade_id),'iBizBusinessCentral-Base_module_upgrade-Update')")
    @ApiOperation(value = "更新升级模块", tags = {"升级模块" },  notes = "更新升级模块")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_module_upgrades/{base_module_upgrade_id}")
    public ResponseEntity<Base_module_upgradeDTO> update(@PathVariable("base_module_upgrade_id") Long base_module_upgrade_id, @RequestBody Base_module_upgradeDTO base_module_upgradedto) {
		Base_module_upgrade domain  = base_module_upgradeMapping.toDomain(base_module_upgradedto);
        domain .setId(base_module_upgrade_id);
		base_module_upgradeService.update(domain );
		Base_module_upgradeDTO dto = base_module_upgradeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_module_upgradeService.getBaseModuleUpgradeByEntities(this.base_module_upgradeMapping.toDomain(#base_module_upgradedtos)),'iBizBusinessCentral-Base_module_upgrade-Update')")
    @ApiOperation(value = "批量更新升级模块", tags = {"升级模块" },  notes = "批量更新升级模块")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_module_upgrades/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_module_upgradeDTO> base_module_upgradedtos) {
        base_module_upgradeService.updateBatch(base_module_upgradeMapping.toDomain(base_module_upgradedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_module_upgradeService.get(#base_module_upgrade_id),'iBizBusinessCentral-Base_module_upgrade-Remove')")
    @ApiOperation(value = "删除升级模块", tags = {"升级模块" },  notes = "删除升级模块")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_module_upgrades/{base_module_upgrade_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_module_upgrade_id") Long base_module_upgrade_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_module_upgradeService.remove(base_module_upgrade_id));
    }

    @PreAuthorize("hasPermission(this.base_module_upgradeService.getBaseModuleUpgradeByIds(#ids),'iBizBusinessCentral-Base_module_upgrade-Remove')")
    @ApiOperation(value = "批量删除升级模块", tags = {"升级模块" },  notes = "批量删除升级模块")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_module_upgrades/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_module_upgradeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_module_upgradeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_module_upgrade-Get')")
    @ApiOperation(value = "获取升级模块", tags = {"升级模块" },  notes = "获取升级模块")
	@RequestMapping(method = RequestMethod.GET, value = "/base_module_upgrades/{base_module_upgrade_id}")
    public ResponseEntity<Base_module_upgradeDTO> get(@PathVariable("base_module_upgrade_id") Long base_module_upgrade_id) {
        Base_module_upgrade domain = base_module_upgradeService.get(base_module_upgrade_id);
        Base_module_upgradeDTO dto = base_module_upgradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取升级模块草稿", tags = {"升级模块" },  notes = "获取升级模块草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_module_upgrades/getdraft")
    public ResponseEntity<Base_module_upgradeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_module_upgradeMapping.toDto(base_module_upgradeService.getDraft(new Base_module_upgrade())));
    }

    @ApiOperation(value = "检查升级模块", tags = {"升级模块" },  notes = "检查升级模块")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_upgrades/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_module_upgradeDTO base_module_upgradedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_module_upgradeService.checkKey(base_module_upgradeMapping.toDomain(base_module_upgradedto)));
    }

    @PreAuthorize("hasPermission(this.base_module_upgradeMapping.toDomain(#base_module_upgradedto),'iBizBusinessCentral-Base_module_upgrade-Save')")
    @ApiOperation(value = "保存升级模块", tags = {"升级模块" },  notes = "保存升级模块")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_upgrades/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_module_upgradeDTO base_module_upgradedto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_module_upgradeService.save(base_module_upgradeMapping.toDomain(base_module_upgradedto)));
    }

    @PreAuthorize("hasPermission(this.base_module_upgradeMapping.toDomain(#base_module_upgradedtos),'iBizBusinessCentral-Base_module_upgrade-Save')")
    @ApiOperation(value = "批量保存升级模块", tags = {"升级模块" },  notes = "批量保存升级模块")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_upgrades/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_module_upgradeDTO> base_module_upgradedtos) {
        base_module_upgradeService.saveBatch(base_module_upgradeMapping.toDomain(base_module_upgradedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_module_upgrade-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_module_upgrade-Get')")
	@ApiOperation(value = "获取数据集", tags = {"升级模块" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_module_upgrades/fetchdefault")
	public ResponseEntity<List<Base_module_upgradeDTO>> fetchDefault(Base_module_upgradeSearchContext context) {
        Page<Base_module_upgrade> domains = base_module_upgradeService.searchDefault(context) ;
        List<Base_module_upgradeDTO> list = base_module_upgradeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_module_upgrade-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_module_upgrade-Get')")
	@ApiOperation(value = "查询数据集", tags = {"升级模块" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_module_upgrades/searchdefault")
	public ResponseEntity<Page<Base_module_upgradeDTO>> searchDefault(@RequestBody Base_module_upgradeSearchContext context) {
        Page<Base_module_upgrade> domains = base_module_upgradeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_module_upgradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

