package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workorderService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workorderSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工单" })
@RestController("Core-mrp_workorder")
@RequestMapping("")
public class Mrp_workorderResource {

    @Autowired
    public IMrp_workorderService mrp_workorderService;

    @Autowired
    @Lazy
    public Mrp_workorderMapping mrp_workorderMapping;

    @PreAuthorize("hasPermission(this.mrp_workorderMapping.toDomain(#mrp_workorderdto),'iBizBusinessCentral-Mrp_workorder-Create')")
    @ApiOperation(value = "新建工单", tags = {"工单" },  notes = "新建工单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders")
    public ResponseEntity<Mrp_workorderDTO> create(@Validated @RequestBody Mrp_workorderDTO mrp_workorderdto) {
        Mrp_workorder domain = mrp_workorderMapping.toDomain(mrp_workorderdto);
		mrp_workorderService.create(domain);
        Mrp_workorderDTO dto = mrp_workorderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_workorderMapping.toDomain(#mrp_workorderdtos),'iBizBusinessCentral-Mrp_workorder-Create')")
    @ApiOperation(value = "批量新建工单", tags = {"工单" },  notes = "批量新建工单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_workorderDTO> mrp_workorderdtos) {
        mrp_workorderService.createBatch(mrp_workorderMapping.toDomain(mrp_workorderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_workorder" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_workorderService.get(#mrp_workorder_id),'iBizBusinessCentral-Mrp_workorder-Update')")
    @ApiOperation(value = "更新工单", tags = {"工单" },  notes = "更新工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workorders/{mrp_workorder_id}")
    public ResponseEntity<Mrp_workorderDTO> update(@PathVariable("mrp_workorder_id") Long mrp_workorder_id, @RequestBody Mrp_workorderDTO mrp_workorderdto) {
		Mrp_workorder domain  = mrp_workorderMapping.toDomain(mrp_workorderdto);
        domain .setId(mrp_workorder_id);
		mrp_workorderService.update(domain );
		Mrp_workorderDTO dto = mrp_workorderMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_workorderService.getMrpWorkorderByEntities(this.mrp_workorderMapping.toDomain(#mrp_workorderdtos)),'iBizBusinessCentral-Mrp_workorder-Update')")
    @ApiOperation(value = "批量更新工单", tags = {"工单" },  notes = "批量更新工单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workorders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workorderDTO> mrp_workorderdtos) {
        mrp_workorderService.updateBatch(mrp_workorderMapping.toDomain(mrp_workorderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_workorderService.get(#mrp_workorder_id),'iBizBusinessCentral-Mrp_workorder-Remove')")
    @ApiOperation(value = "删除工单", tags = {"工单" },  notes = "删除工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workorders/{mrp_workorder_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workorder_id") Long mrp_workorder_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_workorderService.remove(mrp_workorder_id));
    }

    @PreAuthorize("hasPermission(this.mrp_workorderService.getMrpWorkorderByIds(#ids),'iBizBusinessCentral-Mrp_workorder-Remove')")
    @ApiOperation(value = "批量删除工单", tags = {"工单" },  notes = "批量删除工单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workorders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_workorderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_workorderMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_workorder-Get')")
    @ApiOperation(value = "获取工单", tags = {"工单" },  notes = "获取工单")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workorders/{mrp_workorder_id}")
    public ResponseEntity<Mrp_workorderDTO> get(@PathVariable("mrp_workorder_id") Long mrp_workorder_id) {
        Mrp_workorder domain = mrp_workorderService.get(mrp_workorder_id);
        Mrp_workorderDTO dto = mrp_workorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工单草稿", tags = {"工单" },  notes = "获取工单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workorders/getdraft")
    public ResponseEntity<Mrp_workorderDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_workorderMapping.toDto(mrp_workorderService.getDraft(new Mrp_workorder())));
    }

    @ApiOperation(value = "检查工单", tags = {"工单" },  notes = "检查工单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_workorderDTO mrp_workorderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_workorderService.checkKey(mrp_workorderMapping.toDomain(mrp_workorderdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_workorderMapping.toDomain(#mrp_workorderdto),'iBizBusinessCentral-Mrp_workorder-Save')")
    @ApiOperation(value = "保存工单", tags = {"工单" },  notes = "保存工单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_workorderDTO mrp_workorderdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_workorderService.save(mrp_workorderMapping.toDomain(mrp_workorderdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_workorderMapping.toDomain(#mrp_workorderdtos),'iBizBusinessCentral-Mrp_workorder-Save')")
    @ApiOperation(value = "批量保存工单", tags = {"工单" },  notes = "批量保存工单")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_workorderDTO> mrp_workorderdtos) {
        mrp_workorderService.saveBatch(mrp_workorderMapping.toDomain(mrp_workorderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_workorder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_workorder-Get')")
	@ApiOperation(value = "获取数据集", tags = {"工单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workorders/fetchdefault")
	public ResponseEntity<List<Mrp_workorderDTO>> fetchDefault(Mrp_workorderSearchContext context) {
        Page<Mrp_workorder> domains = mrp_workorderService.searchDefault(context) ;
        List<Mrp_workorderDTO> list = mrp_workorderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_workorder-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_workorder-Get')")
	@ApiOperation(value = "查询数据集", tags = {"工单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_workorders/searchdefault")
	public ResponseEntity<Page<Mrp_workorderDTO>> searchDefault(@RequestBody Mrp_workorderSearchContext context) {
        Page<Mrp_workorder> domains = mrp_workorderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_workorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

