package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition;
import cn.ibizlab.businesscentral.core.dto.Purchase_requisitionDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CorePurchase_requisitionMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Purchase_requisitionMapping extends MappingBase<Purchase_requisitionDTO, Purchase_requisition> {


}

