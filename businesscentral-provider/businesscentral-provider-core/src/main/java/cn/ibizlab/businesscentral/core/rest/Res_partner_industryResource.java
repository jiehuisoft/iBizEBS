package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_industry;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_industryService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_industrySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工业" })
@RestController("Core-res_partner_industry")
@RequestMapping("")
public class Res_partner_industryResource {

    @Autowired
    public IRes_partner_industryService res_partner_industryService;

    @Autowired
    @Lazy
    public Res_partner_industryMapping res_partner_industryMapping;

    @PreAuthorize("hasPermission(this.res_partner_industryMapping.toDomain(#res_partner_industrydto),'iBizBusinessCentral-Res_partner_industry-Create')")
    @ApiOperation(value = "新建工业", tags = {"工业" },  notes = "新建工业")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries")
    public ResponseEntity<Res_partner_industryDTO> create(@Validated @RequestBody Res_partner_industryDTO res_partner_industrydto) {
        Res_partner_industry domain = res_partner_industryMapping.toDomain(res_partner_industrydto);
		res_partner_industryService.create(domain);
        Res_partner_industryDTO dto = res_partner_industryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partner_industryMapping.toDomain(#res_partner_industrydtos),'iBizBusinessCentral-Res_partner_industry-Create')")
    @ApiOperation(value = "批量新建工业", tags = {"工业" },  notes = "批量新建工业")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_partner_industryDTO> res_partner_industrydtos) {
        res_partner_industryService.createBatch(res_partner_industryMapping.toDomain(res_partner_industrydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_partner_industry" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_partner_industryService.get(#res_partner_industry_id),'iBizBusinessCentral-Res_partner_industry-Update')")
    @ApiOperation(value = "更新工业", tags = {"工业" },  notes = "更新工业")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_industries/{res_partner_industry_id}")
    public ResponseEntity<Res_partner_industryDTO> update(@PathVariable("res_partner_industry_id") Long res_partner_industry_id, @RequestBody Res_partner_industryDTO res_partner_industrydto) {
		Res_partner_industry domain  = res_partner_industryMapping.toDomain(res_partner_industrydto);
        domain .setId(res_partner_industry_id);
		res_partner_industryService.update(domain );
		Res_partner_industryDTO dto = res_partner_industryMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partner_industryService.getResPartnerIndustryByEntities(this.res_partner_industryMapping.toDomain(#res_partner_industrydtos)),'iBizBusinessCentral-Res_partner_industry-Update')")
    @ApiOperation(value = "批量更新工业", tags = {"工业" },  notes = "批量更新工业")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_industries/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_industryDTO> res_partner_industrydtos) {
        res_partner_industryService.updateBatch(res_partner_industryMapping.toDomain(res_partner_industrydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_partner_industryService.get(#res_partner_industry_id),'iBizBusinessCentral-Res_partner_industry-Remove')")
    @ApiOperation(value = "删除工业", tags = {"工业" },  notes = "删除工业")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_industries/{res_partner_industry_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_industry_id") Long res_partner_industry_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_partner_industryService.remove(res_partner_industry_id));
    }

    @PreAuthorize("hasPermission(this.res_partner_industryService.getResPartnerIndustryByIds(#ids),'iBizBusinessCentral-Res_partner_industry-Remove')")
    @ApiOperation(value = "批量删除工业", tags = {"工业" },  notes = "批量删除工业")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_industries/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_partner_industryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_partner_industryMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_partner_industry-Get')")
    @ApiOperation(value = "获取工业", tags = {"工业" },  notes = "获取工业")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_industries/{res_partner_industry_id}")
    public ResponseEntity<Res_partner_industryDTO> get(@PathVariable("res_partner_industry_id") Long res_partner_industry_id) {
        Res_partner_industry domain = res_partner_industryService.get(res_partner_industry_id);
        Res_partner_industryDTO dto = res_partner_industryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工业草稿", tags = {"工业" },  notes = "获取工业草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_industries/getdraft")
    public ResponseEntity<Res_partner_industryDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_industryMapping.toDto(res_partner_industryService.getDraft(new Res_partner_industry())));
    }

    @ApiOperation(value = "检查工业", tags = {"工业" },  notes = "检查工业")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_partner_industryDTO res_partner_industrydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_partner_industryService.checkKey(res_partner_industryMapping.toDomain(res_partner_industrydto)));
    }

    @PreAuthorize("hasPermission(this.res_partner_industryMapping.toDomain(#res_partner_industrydto),'iBizBusinessCentral-Res_partner_industry-Save')")
    @ApiOperation(value = "保存工业", tags = {"工业" },  notes = "保存工业")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_partner_industryDTO res_partner_industrydto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_industryService.save(res_partner_industryMapping.toDomain(res_partner_industrydto)));
    }

    @PreAuthorize("hasPermission(this.res_partner_industryMapping.toDomain(#res_partner_industrydtos),'iBizBusinessCentral-Res_partner_industry-Save')")
    @ApiOperation(value = "批量保存工业", tags = {"工业" },  notes = "批量保存工业")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_partner_industryDTO> res_partner_industrydtos) {
        res_partner_industryService.saveBatch(res_partner_industryMapping.toDomain(res_partner_industrydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner_industry-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner_industry-Get')")
	@ApiOperation(value = "获取数据集", tags = {"工业" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_industries/fetchdefault")
	public ResponseEntity<List<Res_partner_industryDTO>> fetchDefault(Res_partner_industrySearchContext context) {
        Page<Res_partner_industry> domains = res_partner_industryService.searchDefault(context) ;
        List<Res_partner_industryDTO> list = res_partner_industryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner_industry-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner_industry-Get')")
	@ApiOperation(value = "查询数据集", tags = {"工业" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_partner_industries/searchdefault")
	public ResponseEntity<Page<Res_partner_industryDTO>> searchDefault(@RequestBody Res_partner_industrySearchContext context) {
        Page<Res_partner_industry> domains = res_partner_industryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_industryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

