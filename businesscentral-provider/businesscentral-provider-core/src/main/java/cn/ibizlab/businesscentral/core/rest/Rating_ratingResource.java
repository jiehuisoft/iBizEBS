package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_rating.domain.Rating_rating;
import cn.ibizlab.businesscentral.core.odoo_rating.service.IRating_ratingService;
import cn.ibizlab.businesscentral.core.odoo_rating.filter.Rating_ratingSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"评级" })
@RestController("Core-rating_rating")
@RequestMapping("")
public class Rating_ratingResource {

    @Autowired
    public IRating_ratingService rating_ratingService;

    @Autowired
    @Lazy
    public Rating_ratingMapping rating_ratingMapping;

    @PreAuthorize("hasPermission(this.rating_ratingMapping.toDomain(#rating_ratingdto),'iBizBusinessCentral-Rating_rating-Create')")
    @ApiOperation(value = "新建评级", tags = {"评级" },  notes = "新建评级")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_ratings")
    public ResponseEntity<Rating_ratingDTO> create(@Validated @RequestBody Rating_ratingDTO rating_ratingdto) {
        Rating_rating domain = rating_ratingMapping.toDomain(rating_ratingdto);
		rating_ratingService.create(domain);
        Rating_ratingDTO dto = rating_ratingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.rating_ratingMapping.toDomain(#rating_ratingdtos),'iBizBusinessCentral-Rating_rating-Create')")
    @ApiOperation(value = "批量新建评级", tags = {"评级" },  notes = "批量新建评级")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_ratings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Rating_ratingDTO> rating_ratingdtos) {
        rating_ratingService.createBatch(rating_ratingMapping.toDomain(rating_ratingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "rating_rating" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.rating_ratingService.get(#rating_rating_id),'iBizBusinessCentral-Rating_rating-Update')")
    @ApiOperation(value = "更新评级", tags = {"评级" },  notes = "更新评级")
	@RequestMapping(method = RequestMethod.PUT, value = "/rating_ratings/{rating_rating_id}")
    public ResponseEntity<Rating_ratingDTO> update(@PathVariable("rating_rating_id") Long rating_rating_id, @RequestBody Rating_ratingDTO rating_ratingdto) {
		Rating_rating domain  = rating_ratingMapping.toDomain(rating_ratingdto);
        domain .setId(rating_rating_id);
		rating_ratingService.update(domain );
		Rating_ratingDTO dto = rating_ratingMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.rating_ratingService.getRatingRatingByEntities(this.rating_ratingMapping.toDomain(#rating_ratingdtos)),'iBizBusinessCentral-Rating_rating-Update')")
    @ApiOperation(value = "批量更新评级", tags = {"评级" },  notes = "批量更新评级")
	@RequestMapping(method = RequestMethod.PUT, value = "/rating_ratings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Rating_ratingDTO> rating_ratingdtos) {
        rating_ratingService.updateBatch(rating_ratingMapping.toDomain(rating_ratingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.rating_ratingService.get(#rating_rating_id),'iBizBusinessCentral-Rating_rating-Remove')")
    @ApiOperation(value = "删除评级", tags = {"评级" },  notes = "删除评级")
	@RequestMapping(method = RequestMethod.DELETE, value = "/rating_ratings/{rating_rating_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("rating_rating_id") Long rating_rating_id) {
         return ResponseEntity.status(HttpStatus.OK).body(rating_ratingService.remove(rating_rating_id));
    }

    @PreAuthorize("hasPermission(this.rating_ratingService.getRatingRatingByIds(#ids),'iBizBusinessCentral-Rating_rating-Remove')")
    @ApiOperation(value = "批量删除评级", tags = {"评级" },  notes = "批量删除评级")
	@RequestMapping(method = RequestMethod.DELETE, value = "/rating_ratings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        rating_ratingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.rating_ratingMapping.toDomain(returnObject.body),'iBizBusinessCentral-Rating_rating-Get')")
    @ApiOperation(value = "获取评级", tags = {"评级" },  notes = "获取评级")
	@RequestMapping(method = RequestMethod.GET, value = "/rating_ratings/{rating_rating_id}")
    public ResponseEntity<Rating_ratingDTO> get(@PathVariable("rating_rating_id") Long rating_rating_id) {
        Rating_rating domain = rating_ratingService.get(rating_rating_id);
        Rating_ratingDTO dto = rating_ratingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取评级草稿", tags = {"评级" },  notes = "获取评级草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/rating_ratings/getdraft")
    public ResponseEntity<Rating_ratingDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(rating_ratingMapping.toDto(rating_ratingService.getDraft(new Rating_rating())));
    }

    @ApiOperation(value = "检查评级", tags = {"评级" },  notes = "检查评级")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_ratings/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Rating_ratingDTO rating_ratingdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(rating_ratingService.checkKey(rating_ratingMapping.toDomain(rating_ratingdto)));
    }

    @PreAuthorize("hasPermission(this.rating_ratingMapping.toDomain(#rating_ratingdto),'iBizBusinessCentral-Rating_rating-Save')")
    @ApiOperation(value = "保存评级", tags = {"评级" },  notes = "保存评级")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_ratings/save")
    public ResponseEntity<Boolean> save(@RequestBody Rating_ratingDTO rating_ratingdto) {
        return ResponseEntity.status(HttpStatus.OK).body(rating_ratingService.save(rating_ratingMapping.toDomain(rating_ratingdto)));
    }

    @PreAuthorize("hasPermission(this.rating_ratingMapping.toDomain(#rating_ratingdtos),'iBizBusinessCentral-Rating_rating-Save')")
    @ApiOperation(value = "批量保存评级", tags = {"评级" },  notes = "批量保存评级")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_ratings/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Rating_ratingDTO> rating_ratingdtos) {
        rating_ratingService.saveBatch(rating_ratingMapping.toDomain(rating_ratingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Rating_rating-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Rating_rating-Get')")
	@ApiOperation(value = "获取数据集", tags = {"评级" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/rating_ratings/fetchdefault")
	public ResponseEntity<List<Rating_ratingDTO>> fetchDefault(Rating_ratingSearchContext context) {
        Page<Rating_rating> domains = rating_ratingService.searchDefault(context) ;
        List<Rating_ratingDTO> list = rating_ratingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Rating_rating-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Rating_rating-Get')")
	@ApiOperation(value = "查询数据集", tags = {"评级" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/rating_ratings/searchdefault")
	public ResponseEntity<Page<Rating_ratingDTO>> searchDefault(@RequestBody Rating_ratingSearchContext context) {
        Page<Rating_rating> domains = rating_ratingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(rating_ratingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

