package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Mro_orderDTO]
 */
@Data
public class Mro_orderDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [MAINTENANCE_TYPE]
     *
     */
    @JSONField(name = "maintenance_type")
    @JsonProperty("maintenance_type")
    @NotBlank(message = "[保养类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String maintenanceType;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [DOCUMENTATION_DESCRIPTION]
     *
     */
    @JSONField(name = "documentation_description")
    @JsonProperty("documentation_description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String documentationDescription;

    /**
     * 属性 [PARTS_MOVED_LINES]
     *
     */
    @JSONField(name = "parts_moved_lines")
    @JsonProperty("parts_moved_lines")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String partsMovedLines;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [CATEGORY_IDS]
     *
     */
    @JSONField(name = "category_ids")
    @JsonProperty("category_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String categoryIds;

    /**
     * 属性 [DATE_SCHEDULED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_scheduled" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_scheduled")
    @NotNull(message = "[计划日期]不允许为空!")
    private Timestamp dateScheduled;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [LABOR_DESCRIPTION]
     *
     */
    @JSONField(name = "labor_description")
    @JsonProperty("labor_description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String laborDescription;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [ORIGIN]
     *
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    @Size(min = 0, max = 64, message = "内容长度必须小于等于[64]")
    private String origin;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @NotBlank(message = "[说明]不允许为空!")
    @Size(min = 0, max = 64, message = "内容长度必须小于等于[64]")
    private String description;

    /**
     * 属性 [PARTS_READY_LINES]
     *
     */
    @JSONField(name = "parts_ready_lines")
    @JsonProperty("parts_ready_lines")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String partsReadyLines;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [PARTS_MOVE_LINES]
     *
     */
    @JSONField(name = "parts_move_lines")
    @JsonProperty("parts_move_lines")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String partsMoveLines;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [OPERATIONS_DESCRIPTION]
     *
     */
    @JSONField(name = "operations_description")
    @JsonProperty("operations_description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String operationsDescription;

    /**
     * 属性 [PROBLEM_DESCRIPTION]
     *
     */
    @JSONField(name = "problem_description")
    @JsonProperty("problem_description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String problemDescription;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [DATE_PLANNED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_planned" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_planned")
    @NotNull(message = "[Planned Date]不允许为空!")
    private Timestamp datePlanned;

    /**
     * 属性 [PROCUREMENT_GROUP_ID]
     *
     */
    @JSONField(name = "procurement_group_id")
    @JsonProperty("procurement_group_id")
    private Integer procurementGroupId;

    /**
     * 属性 [DATE_EXECUTION]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_execution" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_execution")
    @NotNull(message = "[Execution Date]不允许为空!")
    private Timestamp dateExecution;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [PARTS_LINES]
     *
     */
    @JSONField(name = "parts_lines")
    @JsonProperty("parts_lines")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String partsLines;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 64, message = "内容长度必须小于等于[64]")
    private String name;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [TOOLS_DESCRIPTION]
     *
     */
    @JSONField(name = "tools_description")
    @JsonProperty("tools_description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String toolsDescription;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [WO_ID_TEXT]
     *
     */
    @JSONField(name = "wo_id_text")
    @JsonProperty("wo_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String woIdText;

    /**
     * 属性 [ASSET_ID_TEXT]
     *
     */
    @JSONField(name = "asset_id_text")
    @JsonProperty("asset_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String assetIdText;

    /**
     * 属性 [TASK_ID_TEXT]
     *
     */
    @JSONField(name = "task_id_text")
    @JsonProperty("task_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String taskIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [REQUEST_ID_TEXT]
     *
     */
    @JSONField(name = "request_id_text")
    @JsonProperty("request_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String requestIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [REQUEST_ID]
     *
     */
    @JSONField(name = "request_id")
    @JsonProperty("request_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long requestId;

    /**
     * 属性 [TASK_ID]
     *
     */
    @JSONField(name = "task_id")
    @JsonProperty("task_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long taskId;

    /**
     * 属性 [WO_ID]
     *
     */
    @JSONField(name = "wo_id")
    @JsonProperty("wo_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long woId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[公司]不允许为空!")
    private Long companyId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [ASSET_ID]
     *
     */
    @JSONField(name = "asset_id")
    @JsonProperty("asset_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[Asset]不允许为空!")
    private Long assetId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [MAINTENANCE_TYPE]
     */
    public void setMaintenanceType(String  maintenanceType){
        this.maintenanceType = maintenanceType ;
        this.modify("maintenance_type",maintenanceType);
    }

    /**
     * 设置 [DOCUMENTATION_DESCRIPTION]
     */
    public void setDocumentationDescription(String  documentationDescription){
        this.documentationDescription = documentationDescription ;
        this.modify("documentation_description",documentationDescription);
    }

    /**
     * 设置 [DATE_SCHEDULED]
     */
    public void setDateScheduled(Timestamp  dateScheduled){
        this.dateScheduled = dateScheduled ;
        this.modify("date_scheduled",dateScheduled);
    }

    /**
     * 设置 [LABOR_DESCRIPTION]
     */
    public void setLaborDescription(String  laborDescription){
        this.laborDescription = laborDescription ;
        this.modify("labor_description",laborDescription);
    }

    /**
     * 设置 [ORIGIN]
     */
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [OPERATIONS_DESCRIPTION]
     */
    public void setOperationsDescription(String  operationsDescription){
        this.operationsDescription = operationsDescription ;
        this.modify("operations_description",operationsDescription);
    }

    /**
     * 设置 [PROBLEM_DESCRIPTION]
     */
    public void setProblemDescription(String  problemDescription){
        this.problemDescription = problemDescription ;
        this.modify("problem_description",problemDescription);
    }

    /**
     * 设置 [DATE_PLANNED]
     */
    public void setDatePlanned(Timestamp  datePlanned){
        this.datePlanned = datePlanned ;
        this.modify("date_planned",datePlanned);
    }

    /**
     * 设置 [PROCUREMENT_GROUP_ID]
     */
    public void setProcurementGroupId(Integer  procurementGroupId){
        this.procurementGroupId = procurementGroupId ;
        this.modify("procurement_group_id",procurementGroupId);
    }

    /**
     * 设置 [DATE_EXECUTION]
     */
    public void setDateExecution(Timestamp  dateExecution){
        this.dateExecution = dateExecution ;
        this.modify("date_execution",dateExecution);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [TOOLS_DESCRIPTION]
     */
    public void setToolsDescription(String  toolsDescription){
        this.toolsDescription = toolsDescription ;
        this.modify("tools_description",toolsDescription);
    }

    /**
     * 设置 [REQUEST_ID]
     */
    public void setRequestId(Long  requestId){
        this.requestId = requestId ;
        this.modify("request_id",requestId);
    }

    /**
     * 设置 [TASK_ID]
     */
    public void setTaskId(Long  taskId){
        this.taskId = taskId ;
        this.modify("task_id",taskId);
    }

    /**
     * 设置 [WO_ID]
     */
    public void setWoId(Long  woId){
        this.woId = woId ;
        this.modify("wo_id",woId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [ASSET_ID]
     */
    public void setAssetId(Long  assetId){
        this.assetId = assetId ;
        this.modify("asset_id",assetId);
    }


}


