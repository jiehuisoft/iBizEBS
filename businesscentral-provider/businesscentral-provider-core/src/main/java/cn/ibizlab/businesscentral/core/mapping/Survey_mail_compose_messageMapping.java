package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_mail_compose_message;
import cn.ibizlab.businesscentral.core.dto.Survey_mail_compose_messageDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreSurvey_mail_compose_messageMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Survey_mail_compose_messageMapping extends MappingBase<Survey_mail_compose_messageDTO, Survey_mail_compose_message> {


}

