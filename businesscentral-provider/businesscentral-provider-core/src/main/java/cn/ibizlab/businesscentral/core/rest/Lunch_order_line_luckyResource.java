package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_order_line_lucky;
import cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_order_line_luckyService;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_order_line_luckySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"幸运工作餐订单明细行" })
@RestController("Core-lunch_order_line_lucky")
@RequestMapping("")
public class Lunch_order_line_luckyResource {

    @Autowired
    public ILunch_order_line_luckyService lunch_order_line_luckyService;

    @Autowired
    @Lazy
    public Lunch_order_line_luckyMapping lunch_order_line_luckyMapping;

    @PreAuthorize("hasPermission(this.lunch_order_line_luckyMapping.toDomain(#lunch_order_line_luckydto),'iBizBusinessCentral-Lunch_order_line_lucky-Create')")
    @ApiOperation(value = "新建幸运工作餐订单明细行", tags = {"幸运工作餐订单明细行" },  notes = "新建幸运工作餐订单明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_line_luckies")
    public ResponseEntity<Lunch_order_line_luckyDTO> create(@Validated @RequestBody Lunch_order_line_luckyDTO lunch_order_line_luckydto) {
        Lunch_order_line_lucky domain = lunch_order_line_luckyMapping.toDomain(lunch_order_line_luckydto);
		lunch_order_line_luckyService.create(domain);
        Lunch_order_line_luckyDTO dto = lunch_order_line_luckyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_order_line_luckyMapping.toDomain(#lunch_order_line_luckydtos),'iBizBusinessCentral-Lunch_order_line_lucky-Create')")
    @ApiOperation(value = "批量新建幸运工作餐订单明细行", tags = {"幸运工作餐订单明细行" },  notes = "批量新建幸运工作餐订单明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_line_luckies/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_order_line_luckyDTO> lunch_order_line_luckydtos) {
        lunch_order_line_luckyService.createBatch(lunch_order_line_luckyMapping.toDomain(lunch_order_line_luckydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "lunch_order_line_lucky" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.lunch_order_line_luckyService.get(#lunch_order_line_lucky_id),'iBizBusinessCentral-Lunch_order_line_lucky-Update')")
    @ApiOperation(value = "更新幸运工作餐订单明细行", tags = {"幸运工作餐订单明细行" },  notes = "更新幸运工作餐订单明细行")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_order_line_luckies/{lunch_order_line_lucky_id}")
    public ResponseEntity<Lunch_order_line_luckyDTO> update(@PathVariable("lunch_order_line_lucky_id") Long lunch_order_line_lucky_id, @RequestBody Lunch_order_line_luckyDTO lunch_order_line_luckydto) {
		Lunch_order_line_lucky domain  = lunch_order_line_luckyMapping.toDomain(lunch_order_line_luckydto);
        domain .setId(lunch_order_line_lucky_id);
		lunch_order_line_luckyService.update(domain );
		Lunch_order_line_luckyDTO dto = lunch_order_line_luckyMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_order_line_luckyService.getLunchOrderLineLuckyByEntities(this.lunch_order_line_luckyMapping.toDomain(#lunch_order_line_luckydtos)),'iBizBusinessCentral-Lunch_order_line_lucky-Update')")
    @ApiOperation(value = "批量更新幸运工作餐订单明细行", tags = {"幸运工作餐订单明细行" },  notes = "批量更新幸运工作餐订单明细行")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_order_line_luckies/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_order_line_luckyDTO> lunch_order_line_luckydtos) {
        lunch_order_line_luckyService.updateBatch(lunch_order_line_luckyMapping.toDomain(lunch_order_line_luckydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.lunch_order_line_luckyService.get(#lunch_order_line_lucky_id),'iBizBusinessCentral-Lunch_order_line_lucky-Remove')")
    @ApiOperation(value = "删除幸运工作餐订单明细行", tags = {"幸运工作餐订单明细行" },  notes = "删除幸运工作餐订单明细行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_order_line_luckies/{lunch_order_line_lucky_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("lunch_order_line_lucky_id") Long lunch_order_line_lucky_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_order_line_luckyService.remove(lunch_order_line_lucky_id));
    }

    @PreAuthorize("hasPermission(this.lunch_order_line_luckyService.getLunchOrderLineLuckyByIds(#ids),'iBizBusinessCentral-Lunch_order_line_lucky-Remove')")
    @ApiOperation(value = "批量删除幸运工作餐订单明细行", tags = {"幸运工作餐订单明细行" },  notes = "批量删除幸运工作餐订单明细行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_order_line_luckies/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        lunch_order_line_luckyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.lunch_order_line_luckyMapping.toDomain(returnObject.body),'iBizBusinessCentral-Lunch_order_line_lucky-Get')")
    @ApiOperation(value = "获取幸运工作餐订单明细行", tags = {"幸运工作餐订单明细行" },  notes = "获取幸运工作餐订单明细行")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_order_line_luckies/{lunch_order_line_lucky_id}")
    public ResponseEntity<Lunch_order_line_luckyDTO> get(@PathVariable("lunch_order_line_lucky_id") Long lunch_order_line_lucky_id) {
        Lunch_order_line_lucky domain = lunch_order_line_luckyService.get(lunch_order_line_lucky_id);
        Lunch_order_line_luckyDTO dto = lunch_order_line_luckyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取幸运工作餐订单明细行草稿", tags = {"幸运工作餐订单明细行" },  notes = "获取幸运工作餐订单明细行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_order_line_luckies/getdraft")
    public ResponseEntity<Lunch_order_line_luckyDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_order_line_luckyMapping.toDto(lunch_order_line_luckyService.getDraft(new Lunch_order_line_lucky())));
    }

    @ApiOperation(value = "检查幸运工作餐订单明细行", tags = {"幸运工作餐订单明细行" },  notes = "检查幸运工作餐订单明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_line_luckies/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Lunch_order_line_luckyDTO lunch_order_line_luckydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(lunch_order_line_luckyService.checkKey(lunch_order_line_luckyMapping.toDomain(lunch_order_line_luckydto)));
    }

    @PreAuthorize("hasPermission(this.lunch_order_line_luckyMapping.toDomain(#lunch_order_line_luckydto),'iBizBusinessCentral-Lunch_order_line_lucky-Save')")
    @ApiOperation(value = "保存幸运工作餐订单明细行", tags = {"幸运工作餐订单明细行" },  notes = "保存幸运工作餐订单明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_line_luckies/save")
    public ResponseEntity<Boolean> save(@RequestBody Lunch_order_line_luckyDTO lunch_order_line_luckydto) {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_order_line_luckyService.save(lunch_order_line_luckyMapping.toDomain(lunch_order_line_luckydto)));
    }

    @PreAuthorize("hasPermission(this.lunch_order_line_luckyMapping.toDomain(#lunch_order_line_luckydtos),'iBizBusinessCentral-Lunch_order_line_lucky-Save')")
    @ApiOperation(value = "批量保存幸运工作餐订单明细行", tags = {"幸运工作餐订单明细行" },  notes = "批量保存幸运工作餐订单明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_line_luckies/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Lunch_order_line_luckyDTO> lunch_order_line_luckydtos) {
        lunch_order_line_luckyService.saveBatch(lunch_order_line_luckyMapping.toDomain(lunch_order_line_luckydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_order_line_lucky-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_order_line_lucky-Get')")
	@ApiOperation(value = "获取数据集", tags = {"幸运工作餐订单明细行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_order_line_luckies/fetchdefault")
	public ResponseEntity<List<Lunch_order_line_luckyDTO>> fetchDefault(Lunch_order_line_luckySearchContext context) {
        Page<Lunch_order_line_lucky> domains = lunch_order_line_luckyService.searchDefault(context) ;
        List<Lunch_order_line_luckyDTO> list = lunch_order_line_luckyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_order_line_lucky-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_order_line_lucky-Get')")
	@ApiOperation(value = "查询数据集", tags = {"幸运工作餐订单明细行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/lunch_order_line_luckies/searchdefault")
	public ResponseEntity<Page<Lunch_order_line_luckyDTO>> searchDefault(@RequestBody Lunch_order_line_luckySearchContext context) {
        Page<Lunch_order_line_lucky> domains = lunch_order_line_luckyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_order_line_luckyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

