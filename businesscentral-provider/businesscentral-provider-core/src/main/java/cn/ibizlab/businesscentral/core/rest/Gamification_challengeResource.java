package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge;
import cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_challengeService;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_challengeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"游戏化挑战" })
@RestController("Core-gamification_challenge")
@RequestMapping("")
public class Gamification_challengeResource {

    @Autowired
    public IGamification_challengeService gamification_challengeService;

    @Autowired
    @Lazy
    public Gamification_challengeMapping gamification_challengeMapping;

    @PreAuthorize("hasPermission(this.gamification_challengeMapping.toDomain(#gamification_challengedto),'iBizBusinessCentral-Gamification_challenge-Create')")
    @ApiOperation(value = "新建游戏化挑战", tags = {"游戏化挑战" },  notes = "新建游戏化挑战")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenges")
    public ResponseEntity<Gamification_challengeDTO> create(@Validated @RequestBody Gamification_challengeDTO gamification_challengedto) {
        Gamification_challenge domain = gamification_challengeMapping.toDomain(gamification_challengedto);
		gamification_challengeService.create(domain);
        Gamification_challengeDTO dto = gamification_challengeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_challengeMapping.toDomain(#gamification_challengedtos),'iBizBusinessCentral-Gamification_challenge-Create')")
    @ApiOperation(value = "批量新建游戏化挑战", tags = {"游戏化挑战" },  notes = "批量新建游戏化挑战")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenges/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_challengeDTO> gamification_challengedtos) {
        gamification_challengeService.createBatch(gamification_challengeMapping.toDomain(gamification_challengedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "gamification_challenge" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.gamification_challengeService.get(#gamification_challenge_id),'iBizBusinessCentral-Gamification_challenge-Update')")
    @ApiOperation(value = "更新游戏化挑战", tags = {"游戏化挑战" },  notes = "更新游戏化挑战")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_challenges/{gamification_challenge_id}")
    public ResponseEntity<Gamification_challengeDTO> update(@PathVariable("gamification_challenge_id") Long gamification_challenge_id, @RequestBody Gamification_challengeDTO gamification_challengedto) {
		Gamification_challenge domain  = gamification_challengeMapping.toDomain(gamification_challengedto);
        domain .setId(gamification_challenge_id);
		gamification_challengeService.update(domain );
		Gamification_challengeDTO dto = gamification_challengeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_challengeService.getGamificationChallengeByEntities(this.gamification_challengeMapping.toDomain(#gamification_challengedtos)),'iBizBusinessCentral-Gamification_challenge-Update')")
    @ApiOperation(value = "批量更新游戏化挑战", tags = {"游戏化挑战" },  notes = "批量更新游戏化挑战")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_challenges/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_challengeDTO> gamification_challengedtos) {
        gamification_challengeService.updateBatch(gamification_challengeMapping.toDomain(gamification_challengedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.gamification_challengeService.get(#gamification_challenge_id),'iBizBusinessCentral-Gamification_challenge-Remove')")
    @ApiOperation(value = "删除游戏化挑战", tags = {"游戏化挑战" },  notes = "删除游戏化挑战")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_challenges/{gamification_challenge_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("gamification_challenge_id") Long gamification_challenge_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_challengeService.remove(gamification_challenge_id));
    }

    @PreAuthorize("hasPermission(this.gamification_challengeService.getGamificationChallengeByIds(#ids),'iBizBusinessCentral-Gamification_challenge-Remove')")
    @ApiOperation(value = "批量删除游戏化挑战", tags = {"游戏化挑战" },  notes = "批量删除游戏化挑战")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_challenges/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        gamification_challengeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.gamification_challengeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Gamification_challenge-Get')")
    @ApiOperation(value = "获取游戏化挑战", tags = {"游戏化挑战" },  notes = "获取游戏化挑战")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_challenges/{gamification_challenge_id}")
    public ResponseEntity<Gamification_challengeDTO> get(@PathVariable("gamification_challenge_id") Long gamification_challenge_id) {
        Gamification_challenge domain = gamification_challengeService.get(gamification_challenge_id);
        Gamification_challengeDTO dto = gamification_challengeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取游戏化挑战草稿", tags = {"游戏化挑战" },  notes = "获取游戏化挑战草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_challenges/getdraft")
    public ResponseEntity<Gamification_challengeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_challengeMapping.toDto(gamification_challengeService.getDraft(new Gamification_challenge())));
    }

    @ApiOperation(value = "检查游戏化挑战", tags = {"游戏化挑战" },  notes = "检查游戏化挑战")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenges/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Gamification_challengeDTO gamification_challengedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(gamification_challengeService.checkKey(gamification_challengeMapping.toDomain(gamification_challengedto)));
    }

    @PreAuthorize("hasPermission(this.gamification_challengeMapping.toDomain(#gamification_challengedto),'iBizBusinessCentral-Gamification_challenge-Save')")
    @ApiOperation(value = "保存游戏化挑战", tags = {"游戏化挑战" },  notes = "保存游戏化挑战")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenges/save")
    public ResponseEntity<Boolean> save(@RequestBody Gamification_challengeDTO gamification_challengedto) {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_challengeService.save(gamification_challengeMapping.toDomain(gamification_challengedto)));
    }

    @PreAuthorize("hasPermission(this.gamification_challengeMapping.toDomain(#gamification_challengedtos),'iBizBusinessCentral-Gamification_challenge-Save')")
    @ApiOperation(value = "批量保存游戏化挑战", tags = {"游戏化挑战" },  notes = "批量保存游戏化挑战")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenges/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Gamification_challengeDTO> gamification_challengedtos) {
        gamification_challengeService.saveBatch(gamification_challengeMapping.toDomain(gamification_challengedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_challenge-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_challenge-Get')")
	@ApiOperation(value = "获取数据集", tags = {"游戏化挑战" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_challenges/fetchdefault")
	public ResponseEntity<List<Gamification_challengeDTO>> fetchDefault(Gamification_challengeSearchContext context) {
        Page<Gamification_challenge> domains = gamification_challengeService.searchDefault(context) ;
        List<Gamification_challengeDTO> list = gamification_challengeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_challenge-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_challenge-Get')")
	@ApiOperation(value = "查询数据集", tags = {"游戏化挑战" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/gamification_challenges/searchdefault")
	public ResponseEntity<Page<Gamification_challengeDTO>> searchDefault(@RequestBody Gamification_challengeSearchContext context) {
        Page<Gamification_challenge> domains = gamification_challengeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_challengeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

