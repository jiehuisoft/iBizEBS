package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_event_type;
import cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_event_typeService;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_event_typeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"事件会议类型" })
@RestController("Core-calendar_event_type")
@RequestMapping("")
public class Calendar_event_typeResource {

    @Autowired
    public ICalendar_event_typeService calendar_event_typeService;

    @Autowired
    @Lazy
    public Calendar_event_typeMapping calendar_event_typeMapping;

    @PreAuthorize("hasPermission(this.calendar_event_typeMapping.toDomain(#calendar_event_typedto),'iBizBusinessCentral-Calendar_event_type-Create')")
    @ApiOperation(value = "新建事件会议类型", tags = {"事件会议类型" },  notes = "新建事件会议类型")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_event_types")
    public ResponseEntity<Calendar_event_typeDTO> create(@Validated @RequestBody Calendar_event_typeDTO calendar_event_typedto) {
        Calendar_event_type domain = calendar_event_typeMapping.toDomain(calendar_event_typedto);
		calendar_event_typeService.create(domain);
        Calendar_event_typeDTO dto = calendar_event_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.calendar_event_typeMapping.toDomain(#calendar_event_typedtos),'iBizBusinessCentral-Calendar_event_type-Create')")
    @ApiOperation(value = "批量新建事件会议类型", tags = {"事件会议类型" },  notes = "批量新建事件会议类型")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_event_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Calendar_event_typeDTO> calendar_event_typedtos) {
        calendar_event_typeService.createBatch(calendar_event_typeMapping.toDomain(calendar_event_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "calendar_event_type" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.calendar_event_typeService.get(#calendar_event_type_id),'iBizBusinessCentral-Calendar_event_type-Update')")
    @ApiOperation(value = "更新事件会议类型", tags = {"事件会议类型" },  notes = "更新事件会议类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_event_types/{calendar_event_type_id}")
    public ResponseEntity<Calendar_event_typeDTO> update(@PathVariable("calendar_event_type_id") Long calendar_event_type_id, @RequestBody Calendar_event_typeDTO calendar_event_typedto) {
		Calendar_event_type domain  = calendar_event_typeMapping.toDomain(calendar_event_typedto);
        domain .setId(calendar_event_type_id);
		calendar_event_typeService.update(domain );
		Calendar_event_typeDTO dto = calendar_event_typeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.calendar_event_typeService.getCalendarEventTypeByEntities(this.calendar_event_typeMapping.toDomain(#calendar_event_typedtos)),'iBizBusinessCentral-Calendar_event_type-Update')")
    @ApiOperation(value = "批量更新事件会议类型", tags = {"事件会议类型" },  notes = "批量更新事件会议类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_event_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_event_typeDTO> calendar_event_typedtos) {
        calendar_event_typeService.updateBatch(calendar_event_typeMapping.toDomain(calendar_event_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.calendar_event_typeService.get(#calendar_event_type_id),'iBizBusinessCentral-Calendar_event_type-Remove')")
    @ApiOperation(value = "删除事件会议类型", tags = {"事件会议类型" },  notes = "删除事件会议类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_event_types/{calendar_event_type_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("calendar_event_type_id") Long calendar_event_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(calendar_event_typeService.remove(calendar_event_type_id));
    }

    @PreAuthorize("hasPermission(this.calendar_event_typeService.getCalendarEventTypeByIds(#ids),'iBizBusinessCentral-Calendar_event_type-Remove')")
    @ApiOperation(value = "批量删除事件会议类型", tags = {"事件会议类型" },  notes = "批量删除事件会议类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_event_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        calendar_event_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.calendar_event_typeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Calendar_event_type-Get')")
    @ApiOperation(value = "获取事件会议类型", tags = {"事件会议类型" },  notes = "获取事件会议类型")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_event_types/{calendar_event_type_id}")
    public ResponseEntity<Calendar_event_typeDTO> get(@PathVariable("calendar_event_type_id") Long calendar_event_type_id) {
        Calendar_event_type domain = calendar_event_typeService.get(calendar_event_type_id);
        Calendar_event_typeDTO dto = calendar_event_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取事件会议类型草稿", tags = {"事件会议类型" },  notes = "获取事件会议类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_event_types/getdraft")
    public ResponseEntity<Calendar_event_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(calendar_event_typeMapping.toDto(calendar_event_typeService.getDraft(new Calendar_event_type())));
    }

    @ApiOperation(value = "检查事件会议类型", tags = {"事件会议类型" },  notes = "检查事件会议类型")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_event_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Calendar_event_typeDTO calendar_event_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(calendar_event_typeService.checkKey(calendar_event_typeMapping.toDomain(calendar_event_typedto)));
    }

    @PreAuthorize("hasPermission(this.calendar_event_typeMapping.toDomain(#calendar_event_typedto),'iBizBusinessCentral-Calendar_event_type-Save')")
    @ApiOperation(value = "保存事件会议类型", tags = {"事件会议类型" },  notes = "保存事件会议类型")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_event_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Calendar_event_typeDTO calendar_event_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(calendar_event_typeService.save(calendar_event_typeMapping.toDomain(calendar_event_typedto)));
    }

    @PreAuthorize("hasPermission(this.calendar_event_typeMapping.toDomain(#calendar_event_typedtos),'iBizBusinessCentral-Calendar_event_type-Save')")
    @ApiOperation(value = "批量保存事件会议类型", tags = {"事件会议类型" },  notes = "批量保存事件会议类型")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_event_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Calendar_event_typeDTO> calendar_event_typedtos) {
        calendar_event_typeService.saveBatch(calendar_event_typeMapping.toDomain(calendar_event_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_event_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Calendar_event_type-Get')")
	@ApiOperation(value = "获取数据集", tags = {"事件会议类型" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_event_types/fetchdefault")
	public ResponseEntity<List<Calendar_event_typeDTO>> fetchDefault(Calendar_event_typeSearchContext context) {
        Page<Calendar_event_type> domains = calendar_event_typeService.searchDefault(context) ;
        List<Calendar_event_typeDTO> list = calendar_event_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_event_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Calendar_event_type-Get')")
	@ApiOperation(value = "查询数据集", tags = {"事件会议类型" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/calendar_event_types/searchdefault")
	public ResponseEntity<Page<Calendar_event_typeDTO>> searchDefault(@RequestBody Calendar_event_typeSearchContext context) {
        Page<Calendar_event_type> domains = calendar_event_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(calendar_event_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

