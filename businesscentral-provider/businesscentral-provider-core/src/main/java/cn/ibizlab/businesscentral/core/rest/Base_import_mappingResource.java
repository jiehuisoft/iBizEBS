package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_mapping;
import cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_mappingService;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_mappingSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"基础导入对照" })
@RestController("Core-base_import_mapping")
@RequestMapping("")
public class Base_import_mappingResource {

    @Autowired
    public IBase_import_mappingService base_import_mappingService;

    @Autowired
    @Lazy
    public Base_import_mappingMapping base_import_mappingMapping;

    @PreAuthorize("hasPermission(this.base_import_mappingMapping.toDomain(#base_import_mappingdto),'iBizBusinessCentral-Base_import_mapping-Create')")
    @ApiOperation(value = "新建基础导入对照", tags = {"基础导入对照" },  notes = "新建基础导入对照")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings")
    public ResponseEntity<Base_import_mappingDTO> create(@Validated @RequestBody Base_import_mappingDTO base_import_mappingdto) {
        Base_import_mapping domain = base_import_mappingMapping.toDomain(base_import_mappingdto);
		base_import_mappingService.create(domain);
        Base_import_mappingDTO dto = base_import_mappingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_mappingMapping.toDomain(#base_import_mappingdtos),'iBizBusinessCentral-Base_import_mapping-Create')")
    @ApiOperation(value = "批量新建基础导入对照", tags = {"基础导入对照" },  notes = "批量新建基础导入对照")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_mappingDTO> base_import_mappingdtos) {
        base_import_mappingService.createBatch(base_import_mappingMapping.toDomain(base_import_mappingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_import_mapping" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_import_mappingService.get(#base_import_mapping_id),'iBizBusinessCentral-Base_import_mapping-Update')")
    @ApiOperation(value = "更新基础导入对照", tags = {"基础导入对照" },  notes = "更新基础导入对照")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_mappings/{base_import_mapping_id}")
    public ResponseEntity<Base_import_mappingDTO> update(@PathVariable("base_import_mapping_id") Long base_import_mapping_id, @RequestBody Base_import_mappingDTO base_import_mappingdto) {
		Base_import_mapping domain  = base_import_mappingMapping.toDomain(base_import_mappingdto);
        domain .setId(base_import_mapping_id);
		base_import_mappingService.update(domain );
		Base_import_mappingDTO dto = base_import_mappingMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_mappingService.getBaseImportMappingByEntities(this.base_import_mappingMapping.toDomain(#base_import_mappingdtos)),'iBizBusinessCentral-Base_import_mapping-Update')")
    @ApiOperation(value = "批量更新基础导入对照", tags = {"基础导入对照" },  notes = "批量更新基础导入对照")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_mappings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_mappingDTO> base_import_mappingdtos) {
        base_import_mappingService.updateBatch(base_import_mappingMapping.toDomain(base_import_mappingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_import_mappingService.get(#base_import_mapping_id),'iBizBusinessCentral-Base_import_mapping-Remove')")
    @ApiOperation(value = "删除基础导入对照", tags = {"基础导入对照" },  notes = "删除基础导入对照")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_mappings/{base_import_mapping_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_import_mapping_id") Long base_import_mapping_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_mappingService.remove(base_import_mapping_id));
    }

    @PreAuthorize("hasPermission(this.base_import_mappingService.getBaseImportMappingByIds(#ids),'iBizBusinessCentral-Base_import_mapping-Remove')")
    @ApiOperation(value = "批量删除基础导入对照", tags = {"基础导入对照" },  notes = "批量删除基础导入对照")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_mappings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_import_mappingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_import_mappingMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_import_mapping-Get')")
    @ApiOperation(value = "获取基础导入对照", tags = {"基础导入对照" },  notes = "获取基础导入对照")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_mappings/{base_import_mapping_id}")
    public ResponseEntity<Base_import_mappingDTO> get(@PathVariable("base_import_mapping_id") Long base_import_mapping_id) {
        Base_import_mapping domain = base_import_mappingService.get(base_import_mapping_id);
        Base_import_mappingDTO dto = base_import_mappingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取基础导入对照草稿", tags = {"基础导入对照" },  notes = "获取基础导入对照草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_mappings/getdraft")
    public ResponseEntity<Base_import_mappingDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_mappingMapping.toDto(base_import_mappingService.getDraft(new Base_import_mapping())));
    }

    @ApiOperation(value = "检查基础导入对照", tags = {"基础导入对照" },  notes = "检查基础导入对照")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_import_mappingDTO base_import_mappingdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_import_mappingService.checkKey(base_import_mappingMapping.toDomain(base_import_mappingdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_mappingMapping.toDomain(#base_import_mappingdto),'iBizBusinessCentral-Base_import_mapping-Save')")
    @ApiOperation(value = "保存基础导入对照", tags = {"基础导入对照" },  notes = "保存基础导入对照")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_import_mappingDTO base_import_mappingdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_mappingService.save(base_import_mappingMapping.toDomain(base_import_mappingdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_mappingMapping.toDomain(#base_import_mappingdtos),'iBizBusinessCentral-Base_import_mapping-Save')")
    @ApiOperation(value = "批量保存基础导入对照", tags = {"基础导入对照" },  notes = "批量保存基础导入对照")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_import_mappingDTO> base_import_mappingdtos) {
        base_import_mappingService.saveBatch(base_import_mappingMapping.toDomain(base_import_mappingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_mapping-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_mapping-Get')")
	@ApiOperation(value = "获取数据集", tags = {"基础导入对照" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_mappings/fetchdefault")
	public ResponseEntity<List<Base_import_mappingDTO>> fetchDefault(Base_import_mappingSearchContext context) {
        Page<Base_import_mapping> domains = base_import_mappingService.searchDefault(context) ;
        List<Base_import_mappingDTO> list = base_import_mappingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_mapping-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_mapping-Get')")
	@ApiOperation(value = "查询数据集", tags = {"基础导入对照" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_import_mappings/searchdefault")
	public ResponseEntity<Page<Base_import_mappingDTO>> searchDefault(@RequestBody Base_import_mappingSearchContext context) {
        Page<Base_import_mapping> domains = base_import_mappingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_mappingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

