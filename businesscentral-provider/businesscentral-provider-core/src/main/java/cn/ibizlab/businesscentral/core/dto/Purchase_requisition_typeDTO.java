package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Purchase_requisition_typeDTO]
 */
@Data
public class Purchase_requisition_typeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[申请类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [EXCLUSIVE]
     *
     */
    @JSONField(name = "exclusive")
    @JsonProperty("exclusive")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String exclusive;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [QUANTITY_COPY]
     *
     */
    @JSONField(name = "quantity_copy")
    @JsonProperty("quantity_copy")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String quantityCopy;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [LINE_COPY]
     *
     */
    @JSONField(name = "line_copy")
    @JsonProperty("line_copy")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String lineCopy;

    /**
     * 属性 [CREATE_UNAME]
     *
     */
    @JSONField(name = "create_uname")
    @JsonProperty("create_uname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String createUname;

    /**
     * 属性 [WRITE_UNAME]
     *
     */
    @JSONField(name = "write_uname")
    @JsonProperty("write_uname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String writeUname;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [EXCLUSIVE]
     */
    public void setExclusive(String  exclusive){
        this.exclusive = exclusive ;
        this.modify("exclusive",exclusive);
    }

    /**
     * 设置 [QUANTITY_COPY]
     */
    public void setQuantityCopy(String  quantityCopy){
        this.quantityCopy = quantityCopy ;
        this.modify("quantity_copy",quantityCopy);
    }

    /**
     * 设置 [WRITE_DATE]
     */
    public void setWriteDate(Timestamp  writeDate){
        this.writeDate = writeDate ;
        this.modify("write_date",writeDate);
    }

    /**
     * 设置 [CREATE_DATE]
     */
    public void setCreateDate(Timestamp  createDate){
        this.createDate = createDate ;
        this.modify("create_date",createDate);
    }

    /**
     * 设置 [LINE_COPY]
     */
    public void setLineCopy(String  lineCopy){
        this.lineCopy = lineCopy ;
        this.modify("line_copy",lineCopy);
    }

    /**
     * 设置 [WRITE_UID]
     */
    public void setWriteUid(Long  writeUid){
        this.writeUid = writeUid ;
        this.modify("write_uid",writeUid);
    }

    /**
     * 设置 [CREATE_UID]
     */
    public void setCreateUid(Long  createUid){
        this.createUid = createUid ;
        this.modify("create_uid",createUid);
    }


}


