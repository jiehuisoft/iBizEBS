package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_import;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_importService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statement_importSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"导入银行对账单" })
@RestController("Core-account_bank_statement_import")
@RequestMapping("")
public class Account_bank_statement_importResource {

    @Autowired
    public IAccount_bank_statement_importService account_bank_statement_importService;

    @Autowired
    @Lazy
    public Account_bank_statement_importMapping account_bank_statement_importMapping;

    @PreAuthorize("hasPermission(this.account_bank_statement_importMapping.toDomain(#account_bank_statement_importdto),'iBizBusinessCentral-Account_bank_statement_import-Create')")
    @ApiOperation(value = "新建导入银行对账单", tags = {"导入银行对账单" },  notes = "新建导入银行对账单")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_imports")
    public ResponseEntity<Account_bank_statement_importDTO> create(@Validated @RequestBody Account_bank_statement_importDTO account_bank_statement_importdto) {
        Account_bank_statement_import domain = account_bank_statement_importMapping.toDomain(account_bank_statement_importdto);
		account_bank_statement_importService.create(domain);
        Account_bank_statement_importDTO dto = account_bank_statement_importMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_importMapping.toDomain(#account_bank_statement_importdtos),'iBizBusinessCentral-Account_bank_statement_import-Create')")
    @ApiOperation(value = "批量新建导入银行对账单", tags = {"导入银行对账单" },  notes = "批量新建导入银行对账单")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_imports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_bank_statement_importDTO> account_bank_statement_importdtos) {
        account_bank_statement_importService.createBatch(account_bank_statement_importMapping.toDomain(account_bank_statement_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_bank_statement_import" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_bank_statement_importService.get(#account_bank_statement_import_id),'iBizBusinessCentral-Account_bank_statement_import-Update')")
    @ApiOperation(value = "更新导入银行对账单", tags = {"导入银行对账单" },  notes = "更新导入银行对账单")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_imports/{account_bank_statement_import_id}")
    public ResponseEntity<Account_bank_statement_importDTO> update(@PathVariable("account_bank_statement_import_id") Long account_bank_statement_import_id, @RequestBody Account_bank_statement_importDTO account_bank_statement_importdto) {
		Account_bank_statement_import domain  = account_bank_statement_importMapping.toDomain(account_bank_statement_importdto);
        domain .setId(account_bank_statement_import_id);
		account_bank_statement_importService.update(domain );
		Account_bank_statement_importDTO dto = account_bank_statement_importMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_importService.getAccountBankStatementImportByEntities(this.account_bank_statement_importMapping.toDomain(#account_bank_statement_importdtos)),'iBizBusinessCentral-Account_bank_statement_import-Update')")
    @ApiOperation(value = "批量更新导入银行对账单", tags = {"导入银行对账单" },  notes = "批量更新导入银行对账单")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_imports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statement_importDTO> account_bank_statement_importdtos) {
        account_bank_statement_importService.updateBatch(account_bank_statement_importMapping.toDomain(account_bank_statement_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_importService.get(#account_bank_statement_import_id),'iBizBusinessCentral-Account_bank_statement_import-Remove')")
    @ApiOperation(value = "删除导入银行对账单", tags = {"导入银行对账单" },  notes = "删除导入银行对账单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_imports/{account_bank_statement_import_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_import_id") Long account_bank_statement_import_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_importService.remove(account_bank_statement_import_id));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_importService.getAccountBankStatementImportByIds(#ids),'iBizBusinessCentral-Account_bank_statement_import-Remove')")
    @ApiOperation(value = "批量删除导入银行对账单", tags = {"导入银行对账单" },  notes = "批量删除导入银行对账单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_imports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_bank_statement_importService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_bank_statement_importMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_bank_statement_import-Get')")
    @ApiOperation(value = "获取导入银行对账单", tags = {"导入银行对账单" },  notes = "获取导入银行对账单")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_imports/{account_bank_statement_import_id}")
    public ResponseEntity<Account_bank_statement_importDTO> get(@PathVariable("account_bank_statement_import_id") Long account_bank_statement_import_id) {
        Account_bank_statement_import domain = account_bank_statement_importService.get(account_bank_statement_import_id);
        Account_bank_statement_importDTO dto = account_bank_statement_importMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取导入银行对账单草稿", tags = {"导入银行对账单" },  notes = "获取导入银行对账单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_imports/getdraft")
    public ResponseEntity<Account_bank_statement_importDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_importMapping.toDto(account_bank_statement_importService.getDraft(new Account_bank_statement_import())));
    }

    @ApiOperation(value = "检查导入银行对账单", tags = {"导入银行对账单" },  notes = "检查导入银行对账单")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_imports/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_bank_statement_importDTO account_bank_statement_importdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_importService.checkKey(account_bank_statement_importMapping.toDomain(account_bank_statement_importdto)));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_importMapping.toDomain(#account_bank_statement_importdto),'iBizBusinessCentral-Account_bank_statement_import-Save')")
    @ApiOperation(value = "保存导入银行对账单", tags = {"导入银行对账单" },  notes = "保存导入银行对账单")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_imports/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_bank_statement_importDTO account_bank_statement_importdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_importService.save(account_bank_statement_importMapping.toDomain(account_bank_statement_importdto)));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_importMapping.toDomain(#account_bank_statement_importdtos),'iBizBusinessCentral-Account_bank_statement_import-Save')")
    @ApiOperation(value = "批量保存导入银行对账单", tags = {"导入银行对账单" },  notes = "批量保存导入银行对账单")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_imports/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_bank_statement_importDTO> account_bank_statement_importdtos) {
        account_bank_statement_importService.saveBatch(account_bank_statement_importMapping.toDomain(account_bank_statement_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_bank_statement_import-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_bank_statement_import-Get')")
	@ApiOperation(value = "获取数据集", tags = {"导入银行对账单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statement_imports/fetchdefault")
	public ResponseEntity<List<Account_bank_statement_importDTO>> fetchDefault(Account_bank_statement_importSearchContext context) {
        Page<Account_bank_statement_import> domains = account_bank_statement_importService.searchDefault(context) ;
        List<Account_bank_statement_importDTO> list = account_bank_statement_importMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_bank_statement_import-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_bank_statement_import-Get')")
	@ApiOperation(value = "查询数据集", tags = {"导入银行对账单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_bank_statement_imports/searchdefault")
	public ResponseEntity<Page<Account_bank_statement_importDTO>> searchDefault(@RequestBody Account_bank_statement_importSearchContext context) {
        Page<Account_bank_statement_import> domains = account_bank_statement_importService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_bank_statement_importMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

