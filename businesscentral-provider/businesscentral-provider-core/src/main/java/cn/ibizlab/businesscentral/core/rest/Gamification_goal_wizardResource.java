package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal_wizard;
import cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_goal_wizardService;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_goal_wizardSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"游戏化目标向导" })
@RestController("Core-gamification_goal_wizard")
@RequestMapping("")
public class Gamification_goal_wizardResource {

    @Autowired
    public IGamification_goal_wizardService gamification_goal_wizardService;

    @Autowired
    @Lazy
    public Gamification_goal_wizardMapping gamification_goal_wizardMapping;

    @PreAuthorize("hasPermission(this.gamification_goal_wizardMapping.toDomain(#gamification_goal_wizarddto),'iBizBusinessCentral-Gamification_goal_wizard-Create')")
    @ApiOperation(value = "新建游戏化目标向导", tags = {"游戏化目标向导" },  notes = "新建游戏化目标向导")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_wizards")
    public ResponseEntity<Gamification_goal_wizardDTO> create(@Validated @RequestBody Gamification_goal_wizardDTO gamification_goal_wizarddto) {
        Gamification_goal_wizard domain = gamification_goal_wizardMapping.toDomain(gamification_goal_wizarddto);
		gamification_goal_wizardService.create(domain);
        Gamification_goal_wizardDTO dto = gamification_goal_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_goal_wizardMapping.toDomain(#gamification_goal_wizarddtos),'iBizBusinessCentral-Gamification_goal_wizard-Create')")
    @ApiOperation(value = "批量新建游戏化目标向导", tags = {"游戏化目标向导" },  notes = "批量新建游戏化目标向导")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_goal_wizardDTO> gamification_goal_wizarddtos) {
        gamification_goal_wizardService.createBatch(gamification_goal_wizardMapping.toDomain(gamification_goal_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "gamification_goal_wizard" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.gamification_goal_wizardService.get(#gamification_goal_wizard_id),'iBizBusinessCentral-Gamification_goal_wizard-Update')")
    @ApiOperation(value = "更新游戏化目标向导", tags = {"游戏化目标向导" },  notes = "更新游戏化目标向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_goal_wizards/{gamification_goal_wizard_id}")
    public ResponseEntity<Gamification_goal_wizardDTO> update(@PathVariable("gamification_goal_wizard_id") Long gamification_goal_wizard_id, @RequestBody Gamification_goal_wizardDTO gamification_goal_wizarddto) {
		Gamification_goal_wizard domain  = gamification_goal_wizardMapping.toDomain(gamification_goal_wizarddto);
        domain .setId(gamification_goal_wizard_id);
		gamification_goal_wizardService.update(domain );
		Gamification_goal_wizardDTO dto = gamification_goal_wizardMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_goal_wizardService.getGamificationGoalWizardByEntities(this.gamification_goal_wizardMapping.toDomain(#gamification_goal_wizarddtos)),'iBizBusinessCentral-Gamification_goal_wizard-Update')")
    @ApiOperation(value = "批量更新游戏化目标向导", tags = {"游戏化目标向导" },  notes = "批量更新游戏化目标向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_goal_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_goal_wizardDTO> gamification_goal_wizarddtos) {
        gamification_goal_wizardService.updateBatch(gamification_goal_wizardMapping.toDomain(gamification_goal_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.gamification_goal_wizardService.get(#gamification_goal_wizard_id),'iBizBusinessCentral-Gamification_goal_wizard-Remove')")
    @ApiOperation(value = "删除游戏化目标向导", tags = {"游戏化目标向导" },  notes = "删除游戏化目标向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goal_wizards/{gamification_goal_wizard_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("gamification_goal_wizard_id") Long gamification_goal_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_goal_wizardService.remove(gamification_goal_wizard_id));
    }

    @PreAuthorize("hasPermission(this.gamification_goal_wizardService.getGamificationGoalWizardByIds(#ids),'iBizBusinessCentral-Gamification_goal_wizard-Remove')")
    @ApiOperation(value = "批量删除游戏化目标向导", tags = {"游戏化目标向导" },  notes = "批量删除游戏化目标向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goal_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        gamification_goal_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.gamification_goal_wizardMapping.toDomain(returnObject.body),'iBizBusinessCentral-Gamification_goal_wizard-Get')")
    @ApiOperation(value = "获取游戏化目标向导", tags = {"游戏化目标向导" },  notes = "获取游戏化目标向导")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_wizards/{gamification_goal_wizard_id}")
    public ResponseEntity<Gamification_goal_wizardDTO> get(@PathVariable("gamification_goal_wizard_id") Long gamification_goal_wizard_id) {
        Gamification_goal_wizard domain = gamification_goal_wizardService.get(gamification_goal_wizard_id);
        Gamification_goal_wizardDTO dto = gamification_goal_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取游戏化目标向导草稿", tags = {"游戏化目标向导" },  notes = "获取游戏化目标向导草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_wizards/getdraft")
    public ResponseEntity<Gamification_goal_wizardDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_goal_wizardMapping.toDto(gamification_goal_wizardService.getDraft(new Gamification_goal_wizard())));
    }

    @ApiOperation(value = "检查游戏化目标向导", tags = {"游戏化目标向导" },  notes = "检查游戏化目标向导")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_wizards/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Gamification_goal_wizardDTO gamification_goal_wizarddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(gamification_goal_wizardService.checkKey(gamification_goal_wizardMapping.toDomain(gamification_goal_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.gamification_goal_wizardMapping.toDomain(#gamification_goal_wizarddto),'iBizBusinessCentral-Gamification_goal_wizard-Save')")
    @ApiOperation(value = "保存游戏化目标向导", tags = {"游戏化目标向导" },  notes = "保存游戏化目标向导")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_wizards/save")
    public ResponseEntity<Boolean> save(@RequestBody Gamification_goal_wizardDTO gamification_goal_wizarddto) {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_goal_wizardService.save(gamification_goal_wizardMapping.toDomain(gamification_goal_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.gamification_goal_wizardMapping.toDomain(#gamification_goal_wizarddtos),'iBizBusinessCentral-Gamification_goal_wizard-Save')")
    @ApiOperation(value = "批量保存游戏化目标向导", tags = {"游戏化目标向导" },  notes = "批量保存游戏化目标向导")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_wizards/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Gamification_goal_wizardDTO> gamification_goal_wizarddtos) {
        gamification_goal_wizardService.saveBatch(gamification_goal_wizardMapping.toDomain(gamification_goal_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_goal_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_goal_wizard-Get')")
	@ApiOperation(value = "获取数据集", tags = {"游戏化目标向导" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_goal_wizards/fetchdefault")
	public ResponseEntity<List<Gamification_goal_wizardDTO>> fetchDefault(Gamification_goal_wizardSearchContext context) {
        Page<Gamification_goal_wizard> domains = gamification_goal_wizardService.searchDefault(context) ;
        List<Gamification_goal_wizardDTO> list = gamification_goal_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_goal_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_goal_wizard-Get')")
	@ApiOperation(value = "查询数据集", tags = {"游戏化目标向导" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/gamification_goal_wizards/searchdefault")
	public ResponseEntity<Page<Gamification_goal_wizardDTO>> searchDefault(@RequestBody Gamification_goal_wizardSearchContext context) {
        Page<Gamification_goal_wizard> domains = gamification_goal_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_goal_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

