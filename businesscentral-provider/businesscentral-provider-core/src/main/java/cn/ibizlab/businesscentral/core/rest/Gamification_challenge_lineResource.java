package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge_line;
import cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_challenge_lineService;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_challenge_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"游戏化挑战的一般目标" })
@RestController("Core-gamification_challenge_line")
@RequestMapping("")
public class Gamification_challenge_lineResource {

    @Autowired
    public IGamification_challenge_lineService gamification_challenge_lineService;

    @Autowired
    @Lazy
    public Gamification_challenge_lineMapping gamification_challenge_lineMapping;

    @PreAuthorize("hasPermission(this.gamification_challenge_lineMapping.toDomain(#gamification_challenge_linedto),'iBizBusinessCentral-Gamification_challenge_line-Create')")
    @ApiOperation(value = "新建游戏化挑战的一般目标", tags = {"游戏化挑战的一般目标" },  notes = "新建游戏化挑战的一般目标")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenge_lines")
    public ResponseEntity<Gamification_challenge_lineDTO> create(@Validated @RequestBody Gamification_challenge_lineDTO gamification_challenge_linedto) {
        Gamification_challenge_line domain = gamification_challenge_lineMapping.toDomain(gamification_challenge_linedto);
		gamification_challenge_lineService.create(domain);
        Gamification_challenge_lineDTO dto = gamification_challenge_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_challenge_lineMapping.toDomain(#gamification_challenge_linedtos),'iBizBusinessCentral-Gamification_challenge_line-Create')")
    @ApiOperation(value = "批量新建游戏化挑战的一般目标", tags = {"游戏化挑战的一般目标" },  notes = "批量新建游戏化挑战的一般目标")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenge_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_challenge_lineDTO> gamification_challenge_linedtos) {
        gamification_challenge_lineService.createBatch(gamification_challenge_lineMapping.toDomain(gamification_challenge_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "gamification_challenge_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.gamification_challenge_lineService.get(#gamification_challenge_line_id),'iBizBusinessCentral-Gamification_challenge_line-Update')")
    @ApiOperation(value = "更新游戏化挑战的一般目标", tags = {"游戏化挑战的一般目标" },  notes = "更新游戏化挑战的一般目标")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_challenge_lines/{gamification_challenge_line_id}")
    public ResponseEntity<Gamification_challenge_lineDTO> update(@PathVariable("gamification_challenge_line_id") Long gamification_challenge_line_id, @RequestBody Gamification_challenge_lineDTO gamification_challenge_linedto) {
		Gamification_challenge_line domain  = gamification_challenge_lineMapping.toDomain(gamification_challenge_linedto);
        domain .setId(gamification_challenge_line_id);
		gamification_challenge_lineService.update(domain );
		Gamification_challenge_lineDTO dto = gamification_challenge_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_challenge_lineService.getGamificationChallengeLineByEntities(this.gamification_challenge_lineMapping.toDomain(#gamification_challenge_linedtos)),'iBizBusinessCentral-Gamification_challenge_line-Update')")
    @ApiOperation(value = "批量更新游戏化挑战的一般目标", tags = {"游戏化挑战的一般目标" },  notes = "批量更新游戏化挑战的一般目标")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_challenge_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_challenge_lineDTO> gamification_challenge_linedtos) {
        gamification_challenge_lineService.updateBatch(gamification_challenge_lineMapping.toDomain(gamification_challenge_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.gamification_challenge_lineService.get(#gamification_challenge_line_id),'iBizBusinessCentral-Gamification_challenge_line-Remove')")
    @ApiOperation(value = "删除游戏化挑战的一般目标", tags = {"游戏化挑战的一般目标" },  notes = "删除游戏化挑战的一般目标")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_challenge_lines/{gamification_challenge_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("gamification_challenge_line_id") Long gamification_challenge_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_challenge_lineService.remove(gamification_challenge_line_id));
    }

    @PreAuthorize("hasPermission(this.gamification_challenge_lineService.getGamificationChallengeLineByIds(#ids),'iBizBusinessCentral-Gamification_challenge_line-Remove')")
    @ApiOperation(value = "批量删除游戏化挑战的一般目标", tags = {"游戏化挑战的一般目标" },  notes = "批量删除游戏化挑战的一般目标")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_challenge_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        gamification_challenge_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.gamification_challenge_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Gamification_challenge_line-Get')")
    @ApiOperation(value = "获取游戏化挑战的一般目标", tags = {"游戏化挑战的一般目标" },  notes = "获取游戏化挑战的一般目标")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_challenge_lines/{gamification_challenge_line_id}")
    public ResponseEntity<Gamification_challenge_lineDTO> get(@PathVariable("gamification_challenge_line_id") Long gamification_challenge_line_id) {
        Gamification_challenge_line domain = gamification_challenge_lineService.get(gamification_challenge_line_id);
        Gamification_challenge_lineDTO dto = gamification_challenge_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取游戏化挑战的一般目标草稿", tags = {"游戏化挑战的一般目标" },  notes = "获取游戏化挑战的一般目标草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_challenge_lines/getdraft")
    public ResponseEntity<Gamification_challenge_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_challenge_lineMapping.toDto(gamification_challenge_lineService.getDraft(new Gamification_challenge_line())));
    }

    @ApiOperation(value = "检查游戏化挑战的一般目标", tags = {"游戏化挑战的一般目标" },  notes = "检查游戏化挑战的一般目标")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenge_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Gamification_challenge_lineDTO gamification_challenge_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(gamification_challenge_lineService.checkKey(gamification_challenge_lineMapping.toDomain(gamification_challenge_linedto)));
    }

    @PreAuthorize("hasPermission(this.gamification_challenge_lineMapping.toDomain(#gamification_challenge_linedto),'iBizBusinessCentral-Gamification_challenge_line-Save')")
    @ApiOperation(value = "保存游戏化挑战的一般目标", tags = {"游戏化挑战的一般目标" },  notes = "保存游戏化挑战的一般目标")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenge_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Gamification_challenge_lineDTO gamification_challenge_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_challenge_lineService.save(gamification_challenge_lineMapping.toDomain(gamification_challenge_linedto)));
    }

    @PreAuthorize("hasPermission(this.gamification_challenge_lineMapping.toDomain(#gamification_challenge_linedtos),'iBizBusinessCentral-Gamification_challenge_line-Save')")
    @ApiOperation(value = "批量保存游戏化挑战的一般目标", tags = {"游戏化挑战的一般目标" },  notes = "批量保存游戏化挑战的一般目标")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenge_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Gamification_challenge_lineDTO> gamification_challenge_linedtos) {
        gamification_challenge_lineService.saveBatch(gamification_challenge_lineMapping.toDomain(gamification_challenge_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_challenge_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_challenge_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"游戏化挑战的一般目标" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_challenge_lines/fetchdefault")
	public ResponseEntity<List<Gamification_challenge_lineDTO>> fetchDefault(Gamification_challenge_lineSearchContext context) {
        Page<Gamification_challenge_line> domains = gamification_challenge_lineService.searchDefault(context) ;
        List<Gamification_challenge_lineDTO> list = gamification_challenge_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_challenge_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_challenge_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"游戏化挑战的一般目标" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/gamification_challenge_lines/searchdefault")
	public ResponseEntity<Page<Gamification_challenge_lineDTO>> searchDefault(@RequestBody Gamification_challenge_lineSearchContext context) {
        Page<Gamification_challenge_line> domains = gamification_challenge_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_challenge_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

