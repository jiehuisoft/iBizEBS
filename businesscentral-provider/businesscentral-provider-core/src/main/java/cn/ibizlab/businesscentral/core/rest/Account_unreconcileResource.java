package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_unreconcile;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_unreconcileService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_unreconcileSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"会计取消核销" })
@RestController("Core-account_unreconcile")
@RequestMapping("")
public class Account_unreconcileResource {

    @Autowired
    public IAccount_unreconcileService account_unreconcileService;

    @Autowired
    @Lazy
    public Account_unreconcileMapping account_unreconcileMapping;

    @PreAuthorize("hasPermission(this.account_unreconcileMapping.toDomain(#account_unreconciledto),'iBizBusinessCentral-Account_unreconcile-Create')")
    @ApiOperation(value = "新建会计取消核销", tags = {"会计取消核销" },  notes = "新建会计取消核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_unreconciles")
    public ResponseEntity<Account_unreconcileDTO> create(@Validated @RequestBody Account_unreconcileDTO account_unreconciledto) {
        Account_unreconcile domain = account_unreconcileMapping.toDomain(account_unreconciledto);
		account_unreconcileService.create(domain);
        Account_unreconcileDTO dto = account_unreconcileMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_unreconcileMapping.toDomain(#account_unreconciledtos),'iBizBusinessCentral-Account_unreconcile-Create')")
    @ApiOperation(value = "批量新建会计取消核销", tags = {"会计取消核销" },  notes = "批量新建会计取消核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_unreconciles/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_unreconcileDTO> account_unreconciledtos) {
        account_unreconcileService.createBatch(account_unreconcileMapping.toDomain(account_unreconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_unreconcile" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_unreconcileService.get(#account_unreconcile_id),'iBizBusinessCentral-Account_unreconcile-Update')")
    @ApiOperation(value = "更新会计取消核销", tags = {"会计取消核销" },  notes = "更新会计取消核销")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_unreconciles/{account_unreconcile_id}")
    public ResponseEntity<Account_unreconcileDTO> update(@PathVariable("account_unreconcile_id") Long account_unreconcile_id, @RequestBody Account_unreconcileDTO account_unreconciledto) {
		Account_unreconcile domain  = account_unreconcileMapping.toDomain(account_unreconciledto);
        domain .setId(account_unreconcile_id);
		account_unreconcileService.update(domain );
		Account_unreconcileDTO dto = account_unreconcileMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_unreconcileService.getAccountUnreconcileByEntities(this.account_unreconcileMapping.toDomain(#account_unreconciledtos)),'iBizBusinessCentral-Account_unreconcile-Update')")
    @ApiOperation(value = "批量更新会计取消核销", tags = {"会计取消核销" },  notes = "批量更新会计取消核销")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_unreconciles/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_unreconcileDTO> account_unreconciledtos) {
        account_unreconcileService.updateBatch(account_unreconcileMapping.toDomain(account_unreconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_unreconcileService.get(#account_unreconcile_id),'iBizBusinessCentral-Account_unreconcile-Remove')")
    @ApiOperation(value = "删除会计取消核销", tags = {"会计取消核销" },  notes = "删除会计取消核销")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_unreconciles/{account_unreconcile_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_unreconcile_id") Long account_unreconcile_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_unreconcileService.remove(account_unreconcile_id));
    }

    @PreAuthorize("hasPermission(this.account_unreconcileService.getAccountUnreconcileByIds(#ids),'iBizBusinessCentral-Account_unreconcile-Remove')")
    @ApiOperation(value = "批量删除会计取消核销", tags = {"会计取消核销" },  notes = "批量删除会计取消核销")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_unreconciles/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_unreconcileService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_unreconcileMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_unreconcile-Get')")
    @ApiOperation(value = "获取会计取消核销", tags = {"会计取消核销" },  notes = "获取会计取消核销")
	@RequestMapping(method = RequestMethod.GET, value = "/account_unreconciles/{account_unreconcile_id}")
    public ResponseEntity<Account_unreconcileDTO> get(@PathVariable("account_unreconcile_id") Long account_unreconcile_id) {
        Account_unreconcile domain = account_unreconcileService.get(account_unreconcile_id);
        Account_unreconcileDTO dto = account_unreconcileMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取会计取消核销草稿", tags = {"会计取消核销" },  notes = "获取会计取消核销草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_unreconciles/getdraft")
    public ResponseEntity<Account_unreconcileDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_unreconcileMapping.toDto(account_unreconcileService.getDraft(new Account_unreconcile())));
    }

    @ApiOperation(value = "检查会计取消核销", tags = {"会计取消核销" },  notes = "检查会计取消核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_unreconciles/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_unreconcileDTO account_unreconciledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_unreconcileService.checkKey(account_unreconcileMapping.toDomain(account_unreconciledto)));
    }

    @PreAuthorize("hasPermission(this.account_unreconcileMapping.toDomain(#account_unreconciledto),'iBizBusinessCentral-Account_unreconcile-Save')")
    @ApiOperation(value = "保存会计取消核销", tags = {"会计取消核销" },  notes = "保存会计取消核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_unreconciles/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_unreconcileDTO account_unreconciledto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_unreconcileService.save(account_unreconcileMapping.toDomain(account_unreconciledto)));
    }

    @PreAuthorize("hasPermission(this.account_unreconcileMapping.toDomain(#account_unreconciledtos),'iBizBusinessCentral-Account_unreconcile-Save')")
    @ApiOperation(value = "批量保存会计取消核销", tags = {"会计取消核销" },  notes = "批量保存会计取消核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_unreconciles/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_unreconcileDTO> account_unreconciledtos) {
        account_unreconcileService.saveBatch(account_unreconcileMapping.toDomain(account_unreconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_unreconcile-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_unreconcile-Get')")
	@ApiOperation(value = "获取数据集", tags = {"会计取消核销" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_unreconciles/fetchdefault")
	public ResponseEntity<List<Account_unreconcileDTO>> fetchDefault(Account_unreconcileSearchContext context) {
        Page<Account_unreconcile> domains = account_unreconcileService.searchDefault(context) ;
        List<Account_unreconcileDTO> list = account_unreconcileMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_unreconcile-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_unreconcile-Get')")
	@ApiOperation(value = "查询数据集", tags = {"会计取消核销" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_unreconciles/searchdefault")
	public ResponseEntity<Page<Account_unreconcileDTO>> searchDefault(@RequestBody Account_unreconcileSearchContext context) {
        Page<Account_unreconcile> domains = account_unreconcileService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_unreconcileMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

