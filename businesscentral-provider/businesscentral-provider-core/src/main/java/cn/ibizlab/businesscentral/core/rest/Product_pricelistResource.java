package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelistService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_pricelistSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"价格表" })
@RestController("Core-product_pricelist")
@RequestMapping("")
public class Product_pricelistResource {

    @Autowired
    public IProduct_pricelistService product_pricelistService;

    @Autowired
    @Lazy
    public Product_pricelistMapping product_pricelistMapping;

    @PreAuthorize("hasPermission(this.product_pricelistMapping.toDomain(#product_pricelistdto),'iBizBusinessCentral-Product_pricelist-Create')")
    @ApiOperation(value = "新建价格表", tags = {"价格表" },  notes = "新建价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists")
    public ResponseEntity<Product_pricelistDTO> create(@Validated @RequestBody Product_pricelistDTO product_pricelistdto) {
        Product_pricelist domain = product_pricelistMapping.toDomain(product_pricelistdto);
		product_pricelistService.create(domain);
        Product_pricelistDTO dto = product_pricelistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_pricelistMapping.toDomain(#product_pricelistdtos),'iBizBusinessCentral-Product_pricelist-Create')")
    @ApiOperation(value = "批量新建价格表", tags = {"价格表" },  notes = "批量新建价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_pricelistDTO> product_pricelistdtos) {
        product_pricelistService.createBatch(product_pricelistMapping.toDomain(product_pricelistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_pricelist" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_pricelistService.get(#product_pricelist_id),'iBizBusinessCentral-Product_pricelist-Update')")
    @ApiOperation(value = "更新价格表", tags = {"价格表" },  notes = "更新价格表")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_pricelists/{product_pricelist_id}")
    public ResponseEntity<Product_pricelistDTO> update(@PathVariable("product_pricelist_id") Long product_pricelist_id, @RequestBody Product_pricelistDTO product_pricelistdto) {
		Product_pricelist domain  = product_pricelistMapping.toDomain(product_pricelistdto);
        domain .setId(product_pricelist_id);
		product_pricelistService.update(domain );
		Product_pricelistDTO dto = product_pricelistMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_pricelistService.getProductPricelistByEntities(this.product_pricelistMapping.toDomain(#product_pricelistdtos)),'iBizBusinessCentral-Product_pricelist-Update')")
    @ApiOperation(value = "批量更新价格表", tags = {"价格表" },  notes = "批量更新价格表")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_pricelists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_pricelistDTO> product_pricelistdtos) {
        product_pricelistService.updateBatch(product_pricelistMapping.toDomain(product_pricelistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_pricelistService.get(#product_pricelist_id),'iBizBusinessCentral-Product_pricelist-Remove')")
    @ApiOperation(value = "删除价格表", tags = {"价格表" },  notes = "删除价格表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelists/{product_pricelist_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_pricelist_id") Long product_pricelist_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_pricelistService.remove(product_pricelist_id));
    }

    @PreAuthorize("hasPermission(this.product_pricelistService.getProductPricelistByIds(#ids),'iBizBusinessCentral-Product_pricelist-Remove')")
    @ApiOperation(value = "批量删除价格表", tags = {"价格表" },  notes = "批量删除价格表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_pricelists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_pricelistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_pricelistMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_pricelist-Get')")
    @ApiOperation(value = "获取价格表", tags = {"价格表" },  notes = "获取价格表")
	@RequestMapping(method = RequestMethod.GET, value = "/product_pricelists/{product_pricelist_id}")
    public ResponseEntity<Product_pricelistDTO> get(@PathVariable("product_pricelist_id") Long product_pricelist_id) {
        Product_pricelist domain = product_pricelistService.get(product_pricelist_id);
        Product_pricelistDTO dto = product_pricelistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取价格表草稿", tags = {"价格表" },  notes = "获取价格表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_pricelists/getdraft")
    public ResponseEntity<Product_pricelistDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelistMapping.toDto(product_pricelistService.getDraft(new Product_pricelist())));
    }

    @ApiOperation(value = "检查价格表", tags = {"价格表" },  notes = "检查价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_pricelistDTO product_pricelistdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_pricelistService.checkKey(product_pricelistMapping.toDomain(product_pricelistdto)));
    }

    @PreAuthorize("hasPermission(this.product_pricelistMapping.toDomain(#product_pricelistdto),'iBizBusinessCentral-Product_pricelist-Save')")
    @ApiOperation(value = "保存价格表", tags = {"价格表" },  notes = "保存价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_pricelistDTO product_pricelistdto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_pricelistService.save(product_pricelistMapping.toDomain(product_pricelistdto)));
    }

    @PreAuthorize("hasPermission(this.product_pricelistMapping.toDomain(#product_pricelistdtos),'iBizBusinessCentral-Product_pricelist-Save')")
    @ApiOperation(value = "批量保存价格表", tags = {"价格表" },  notes = "批量保存价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_pricelists/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_pricelistDTO> product_pricelistdtos) {
        product_pricelistService.saveBatch(product_pricelistMapping.toDomain(product_pricelistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_pricelist-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_pricelist-Get')")
	@ApiOperation(value = "获取数据集", tags = {"价格表" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_pricelists/fetchdefault")
	public ResponseEntity<List<Product_pricelistDTO>> fetchDefault(Product_pricelistSearchContext context) {
        Page<Product_pricelist> domains = product_pricelistService.searchDefault(context) ;
        List<Product_pricelistDTO> list = product_pricelistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_pricelist-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_pricelist-Get')")
	@ApiOperation(value = "查询数据集", tags = {"价格表" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_pricelists/searchdefault")
	public ResponseEntity<Page<Product_pricelistDTO>> searchDefault(@RequestBody Product_pricelistSearchContext context) {
        Page<Product_pricelist> domains = product_pricelistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_pricelistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

