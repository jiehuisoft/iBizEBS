package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity_type;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_activity_typeService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_activity_typeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"活动类型" })
@RestController("Core-mail_activity_type")
@RequestMapping("")
public class Mail_activity_typeResource {

    @Autowired
    public IMail_activity_typeService mail_activity_typeService;

    @Autowired
    @Lazy
    public Mail_activity_typeMapping mail_activity_typeMapping;

    @PreAuthorize("hasPermission(this.mail_activity_typeMapping.toDomain(#mail_activity_typedto),'iBizBusinessCentral-Mail_activity_type-Create')")
    @ApiOperation(value = "新建活动类型", tags = {"活动类型" },  notes = "新建活动类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types")
    public ResponseEntity<Mail_activity_typeDTO> create(@Validated @RequestBody Mail_activity_typeDTO mail_activity_typedto) {
        Mail_activity_type domain = mail_activity_typeMapping.toDomain(mail_activity_typedto);
		mail_activity_typeService.create(domain);
        Mail_activity_typeDTO dto = mail_activity_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_activity_typeMapping.toDomain(#mail_activity_typedtos),'iBizBusinessCentral-Mail_activity_type-Create')")
    @ApiOperation(value = "批量新建活动类型", tags = {"活动类型" },  notes = "批量新建活动类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_activity_typeDTO> mail_activity_typedtos) {
        mail_activity_typeService.createBatch(mail_activity_typeMapping.toDomain(mail_activity_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_activity_type" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_activity_typeService.get(#mail_activity_type_id),'iBizBusinessCentral-Mail_activity_type-Update')")
    @ApiOperation(value = "更新活动类型", tags = {"活动类型" },  notes = "更新活动类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_types/{mail_activity_type_id}")
    public ResponseEntity<Mail_activity_typeDTO> update(@PathVariable("mail_activity_type_id") Long mail_activity_type_id, @RequestBody Mail_activity_typeDTO mail_activity_typedto) {
		Mail_activity_type domain  = mail_activity_typeMapping.toDomain(mail_activity_typedto);
        domain .setId(mail_activity_type_id);
		mail_activity_typeService.update(domain );
		Mail_activity_typeDTO dto = mail_activity_typeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_activity_typeService.getMailActivityTypeByEntities(this.mail_activity_typeMapping.toDomain(#mail_activity_typedtos)),'iBizBusinessCentral-Mail_activity_type-Update')")
    @ApiOperation(value = "批量更新活动类型", tags = {"活动类型" },  notes = "批量更新活动类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_activity_typeDTO> mail_activity_typedtos) {
        mail_activity_typeService.updateBatch(mail_activity_typeMapping.toDomain(mail_activity_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_activity_typeService.get(#mail_activity_type_id),'iBizBusinessCentral-Mail_activity_type-Remove')")
    @ApiOperation(value = "删除活动类型", tags = {"活动类型" },  notes = "删除活动类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_types/{mail_activity_type_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_activity_type_id") Long mail_activity_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_activity_typeService.remove(mail_activity_type_id));
    }

    @PreAuthorize("hasPermission(this.mail_activity_typeService.getMailActivityTypeByIds(#ids),'iBizBusinessCentral-Mail_activity_type-Remove')")
    @ApiOperation(value = "批量删除活动类型", tags = {"活动类型" },  notes = "批量删除活动类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_activity_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_activity_typeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_activity_type-Get')")
    @ApiOperation(value = "获取活动类型", tags = {"活动类型" },  notes = "获取活动类型")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activity_types/{mail_activity_type_id}")
    public ResponseEntity<Mail_activity_typeDTO> get(@PathVariable("mail_activity_type_id") Long mail_activity_type_id) {
        Mail_activity_type domain = mail_activity_typeService.get(mail_activity_type_id);
        Mail_activity_typeDTO dto = mail_activity_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取活动类型草稿", tags = {"活动类型" },  notes = "获取活动类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activity_types/getdraft")
    public ResponseEntity<Mail_activity_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_typeMapping.toDto(mail_activity_typeService.getDraft(new Mail_activity_type())));
    }

    @ApiOperation(value = "检查活动类型", tags = {"活动类型" },  notes = "检查活动类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_activity_typeDTO mail_activity_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_activity_typeService.checkKey(mail_activity_typeMapping.toDomain(mail_activity_typedto)));
    }

    @PreAuthorize("hasPermission(this.mail_activity_typeMapping.toDomain(#mail_activity_typedto),'iBizBusinessCentral-Mail_activity_type-Save')")
    @ApiOperation(value = "保存活动类型", tags = {"活动类型" },  notes = "保存活动类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_activity_typeDTO mail_activity_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_activity_typeService.save(mail_activity_typeMapping.toDomain(mail_activity_typedto)));
    }

    @PreAuthorize("hasPermission(this.mail_activity_typeMapping.toDomain(#mail_activity_typedtos),'iBizBusinessCentral-Mail_activity_type-Save')")
    @ApiOperation(value = "批量保存活动类型", tags = {"活动类型" },  notes = "批量保存活动类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_activity_typeDTO> mail_activity_typedtos) {
        mail_activity_typeService.saveBatch(mail_activity_typeMapping.toDomain(mail_activity_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_activity_type-Get')")
	@ApiOperation(value = "获取数据集", tags = {"活动类型" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_activity_types/fetchdefault")
	public ResponseEntity<List<Mail_activity_typeDTO>> fetchDefault(Mail_activity_typeSearchContext context) {
        Page<Mail_activity_type> domains = mail_activity_typeService.searchDefault(context) ;
        List<Mail_activity_typeDTO> list = mail_activity_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_activity_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_activity_type-Get')")
	@ApiOperation(value = "查询数据集", tags = {"活动类型" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_activity_types/searchdefault")
	public ResponseEntity<Page<Mail_activity_typeDTO>> searchDefault(@RequestBody Mail_activity_typeSearchContext context) {
        Page<Mail_activity_type> domains = mail_activity_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_activity_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

