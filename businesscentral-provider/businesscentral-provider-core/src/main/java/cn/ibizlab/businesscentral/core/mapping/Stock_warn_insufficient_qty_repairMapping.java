package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty_repair;
import cn.ibizlab.businesscentral.core.dto.Stock_warn_insufficient_qty_repairDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreStock_warn_insufficient_qty_repairMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_warn_insufficient_qty_repairMapping extends MappingBase<Stock_warn_insufficient_qty_repairDTO, Stock_warn_insufficient_qty_repair> {


}

