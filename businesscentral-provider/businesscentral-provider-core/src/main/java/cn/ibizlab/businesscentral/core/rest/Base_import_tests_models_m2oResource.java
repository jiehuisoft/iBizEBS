package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_m2o;
import cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_m2oService;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_m2oSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"测试:基本导入模型，多对一" })
@RestController("Core-base_import_tests_models_m2o")
@RequestMapping("")
public class Base_import_tests_models_m2oResource {

    @Autowired
    public IBase_import_tests_models_m2oService base_import_tests_models_m2oService;

    @Autowired
    @Lazy
    public Base_import_tests_models_m2oMapping base_import_tests_models_m2oMapping;

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2oMapping.toDomain(#base_import_tests_models_m2odto),'iBizBusinessCentral-Base_import_tests_models_m2o-Create')")
    @ApiOperation(value = "新建测试:基本导入模型，多对一", tags = {"测试:基本导入模型，多对一" },  notes = "新建测试:基本导入模型，多对一")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2os")
    public ResponseEntity<Base_import_tests_models_m2oDTO> create(@Validated @RequestBody Base_import_tests_models_m2oDTO base_import_tests_models_m2odto) {
        Base_import_tests_models_m2o domain = base_import_tests_models_m2oMapping.toDomain(base_import_tests_models_m2odto);
		base_import_tests_models_m2oService.create(domain);
        Base_import_tests_models_m2oDTO dto = base_import_tests_models_m2oMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2oMapping.toDomain(#base_import_tests_models_m2odtos),'iBizBusinessCentral-Base_import_tests_models_m2o-Create')")
    @ApiOperation(value = "批量新建测试:基本导入模型，多对一", tags = {"测试:基本导入模型，多对一" },  notes = "批量新建测试:基本导入模型，多对一")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2os/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_m2oDTO> base_import_tests_models_m2odtos) {
        base_import_tests_models_m2oService.createBatch(base_import_tests_models_m2oMapping.toDomain(base_import_tests_models_m2odtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_import_tests_models_m2o" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_import_tests_models_m2oService.get(#base_import_tests_models_m2o_id),'iBizBusinessCentral-Base_import_tests_models_m2o-Update')")
    @ApiOperation(value = "更新测试:基本导入模型，多对一", tags = {"测试:基本导入模型，多对一" },  notes = "更新测试:基本导入模型，多对一")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_m2os/{base_import_tests_models_m2o_id}")
    public ResponseEntity<Base_import_tests_models_m2oDTO> update(@PathVariable("base_import_tests_models_m2o_id") Long base_import_tests_models_m2o_id, @RequestBody Base_import_tests_models_m2oDTO base_import_tests_models_m2odto) {
		Base_import_tests_models_m2o domain  = base_import_tests_models_m2oMapping.toDomain(base_import_tests_models_m2odto);
        domain .setId(base_import_tests_models_m2o_id);
		base_import_tests_models_m2oService.update(domain );
		Base_import_tests_models_m2oDTO dto = base_import_tests_models_m2oMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2oService.getBaseImportTestsModelsM2oByEntities(this.base_import_tests_models_m2oMapping.toDomain(#base_import_tests_models_m2odtos)),'iBizBusinessCentral-Base_import_tests_models_m2o-Update')")
    @ApiOperation(value = "批量更新测试:基本导入模型，多对一", tags = {"测试:基本导入模型，多对一" },  notes = "批量更新测试:基本导入模型，多对一")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_m2os/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_m2oDTO> base_import_tests_models_m2odtos) {
        base_import_tests_models_m2oService.updateBatch(base_import_tests_models_m2oMapping.toDomain(base_import_tests_models_m2odtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2oService.get(#base_import_tests_models_m2o_id),'iBizBusinessCentral-Base_import_tests_models_m2o-Remove')")
    @ApiOperation(value = "删除测试:基本导入模型，多对一", tags = {"测试:基本导入模型，多对一" },  notes = "删除测试:基本导入模型，多对一")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_m2os/{base_import_tests_models_m2o_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_m2o_id") Long base_import_tests_models_m2o_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_m2oService.remove(base_import_tests_models_m2o_id));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2oService.getBaseImportTestsModelsM2oByIds(#ids),'iBizBusinessCentral-Base_import_tests_models_m2o-Remove')")
    @ApiOperation(value = "批量删除测试:基本导入模型，多对一", tags = {"测试:基本导入模型，多对一" },  notes = "批量删除测试:基本导入模型，多对一")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_m2os/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_import_tests_models_m2oService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_import_tests_models_m2oMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_import_tests_models_m2o-Get')")
    @ApiOperation(value = "获取测试:基本导入模型，多对一", tags = {"测试:基本导入模型，多对一" },  notes = "获取测试:基本导入模型，多对一")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2os/{base_import_tests_models_m2o_id}")
    public ResponseEntity<Base_import_tests_models_m2oDTO> get(@PathVariable("base_import_tests_models_m2o_id") Long base_import_tests_models_m2o_id) {
        Base_import_tests_models_m2o domain = base_import_tests_models_m2oService.get(base_import_tests_models_m2o_id);
        Base_import_tests_models_m2oDTO dto = base_import_tests_models_m2oMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取测试:基本导入模型，多对一草稿", tags = {"测试:基本导入模型，多对一" },  notes = "获取测试:基本导入模型，多对一草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2os/getdraft")
    public ResponseEntity<Base_import_tests_models_m2oDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_m2oMapping.toDto(base_import_tests_models_m2oService.getDraft(new Base_import_tests_models_m2o())));
    }

    @ApiOperation(value = "检查测试:基本导入模型，多对一", tags = {"测试:基本导入模型，多对一" },  notes = "检查测试:基本导入模型，多对一")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2os/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_import_tests_models_m2oDTO base_import_tests_models_m2odto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_m2oService.checkKey(base_import_tests_models_m2oMapping.toDomain(base_import_tests_models_m2odto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2oMapping.toDomain(#base_import_tests_models_m2odto),'iBizBusinessCentral-Base_import_tests_models_m2o-Save')")
    @ApiOperation(value = "保存测试:基本导入模型，多对一", tags = {"测试:基本导入模型，多对一" },  notes = "保存测试:基本导入模型，多对一")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2os/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_import_tests_models_m2oDTO base_import_tests_models_m2odto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_m2oService.save(base_import_tests_models_m2oMapping.toDomain(base_import_tests_models_m2odto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2oMapping.toDomain(#base_import_tests_models_m2odtos),'iBizBusinessCentral-Base_import_tests_models_m2o-Save')")
    @ApiOperation(value = "批量保存测试:基本导入模型，多对一", tags = {"测试:基本导入模型，多对一" },  notes = "批量保存测试:基本导入模型，多对一")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2os/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_import_tests_models_m2oDTO> base_import_tests_models_m2odtos) {
        base_import_tests_models_m2oService.saveBatch(base_import_tests_models_m2oMapping.toDomain(base_import_tests_models_m2odtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_m2o-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_m2o-Get')")
	@ApiOperation(value = "获取数据集", tags = {"测试:基本导入模型，多对一" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_m2os/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_m2oDTO>> fetchDefault(Base_import_tests_models_m2oSearchContext context) {
        Page<Base_import_tests_models_m2o> domains = base_import_tests_models_m2oService.searchDefault(context) ;
        List<Base_import_tests_models_m2oDTO> list = base_import_tests_models_m2oMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_m2o-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_m2o-Get')")
	@ApiOperation(value = "查询数据集", tags = {"测试:基本导入模型，多对一" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_import_tests_models_m2os/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_m2oDTO>> searchDefault(@RequestBody Base_import_tests_models_m2oSearchContext context) {
        Page<Base_import_tests_models_m2o> domains = base_import_tests_models_m2oService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_m2oMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

