package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type_mail;
import cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_type_mailService;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_type_mailSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"基于活动分类的邮件调度" })
@RestController("Core-event_type_mail")
@RequestMapping("")
public class Event_type_mailResource {

    @Autowired
    public IEvent_type_mailService event_type_mailService;

    @Autowired
    @Lazy
    public Event_type_mailMapping event_type_mailMapping;

    @PreAuthorize("hasPermission(this.event_type_mailMapping.toDomain(#event_type_maildto),'iBizBusinessCentral-Event_type_mail-Create')")
    @ApiOperation(value = "新建基于活动分类的邮件调度", tags = {"基于活动分类的邮件调度" },  notes = "新建基于活动分类的邮件调度")
	@RequestMapping(method = RequestMethod.POST, value = "/event_type_mails")
    public ResponseEntity<Event_type_mailDTO> create(@Validated @RequestBody Event_type_mailDTO event_type_maildto) {
        Event_type_mail domain = event_type_mailMapping.toDomain(event_type_maildto);
		event_type_mailService.create(domain);
        Event_type_mailDTO dto = event_type_mailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_type_mailMapping.toDomain(#event_type_maildtos),'iBizBusinessCentral-Event_type_mail-Create')")
    @ApiOperation(value = "批量新建基于活动分类的邮件调度", tags = {"基于活动分类的邮件调度" },  notes = "批量新建基于活动分类的邮件调度")
	@RequestMapping(method = RequestMethod.POST, value = "/event_type_mails/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_type_mailDTO> event_type_maildtos) {
        event_type_mailService.createBatch(event_type_mailMapping.toDomain(event_type_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "event_type_mail" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.event_type_mailService.get(#event_type_mail_id),'iBizBusinessCentral-Event_type_mail-Update')")
    @ApiOperation(value = "更新基于活动分类的邮件调度", tags = {"基于活动分类的邮件调度" },  notes = "更新基于活动分类的邮件调度")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_type_mails/{event_type_mail_id}")
    public ResponseEntity<Event_type_mailDTO> update(@PathVariable("event_type_mail_id") Long event_type_mail_id, @RequestBody Event_type_mailDTO event_type_maildto) {
		Event_type_mail domain  = event_type_mailMapping.toDomain(event_type_maildto);
        domain .setId(event_type_mail_id);
		event_type_mailService.update(domain );
		Event_type_mailDTO dto = event_type_mailMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_type_mailService.getEventTypeMailByEntities(this.event_type_mailMapping.toDomain(#event_type_maildtos)),'iBizBusinessCentral-Event_type_mail-Update')")
    @ApiOperation(value = "批量更新基于活动分类的邮件调度", tags = {"基于活动分类的邮件调度" },  notes = "批量更新基于活动分类的邮件调度")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_type_mails/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_type_mailDTO> event_type_maildtos) {
        event_type_mailService.updateBatch(event_type_mailMapping.toDomain(event_type_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.event_type_mailService.get(#event_type_mail_id),'iBizBusinessCentral-Event_type_mail-Remove')")
    @ApiOperation(value = "删除基于活动分类的邮件调度", tags = {"基于活动分类的邮件调度" },  notes = "删除基于活动分类的邮件调度")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_type_mails/{event_type_mail_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("event_type_mail_id") Long event_type_mail_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_type_mailService.remove(event_type_mail_id));
    }

    @PreAuthorize("hasPermission(this.event_type_mailService.getEventTypeMailByIds(#ids),'iBizBusinessCentral-Event_type_mail-Remove')")
    @ApiOperation(value = "批量删除基于活动分类的邮件调度", tags = {"基于活动分类的邮件调度" },  notes = "批量删除基于活动分类的邮件调度")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_type_mails/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        event_type_mailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.event_type_mailMapping.toDomain(returnObject.body),'iBizBusinessCentral-Event_type_mail-Get')")
    @ApiOperation(value = "获取基于活动分类的邮件调度", tags = {"基于活动分类的邮件调度" },  notes = "获取基于活动分类的邮件调度")
	@RequestMapping(method = RequestMethod.GET, value = "/event_type_mails/{event_type_mail_id}")
    public ResponseEntity<Event_type_mailDTO> get(@PathVariable("event_type_mail_id") Long event_type_mail_id) {
        Event_type_mail domain = event_type_mailService.get(event_type_mail_id);
        Event_type_mailDTO dto = event_type_mailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取基于活动分类的邮件调度草稿", tags = {"基于活动分类的邮件调度" },  notes = "获取基于活动分类的邮件调度草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/event_type_mails/getdraft")
    public ResponseEntity<Event_type_mailDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(event_type_mailMapping.toDto(event_type_mailService.getDraft(new Event_type_mail())));
    }

    @ApiOperation(value = "检查基于活动分类的邮件调度", tags = {"基于活动分类的邮件调度" },  notes = "检查基于活动分类的邮件调度")
	@RequestMapping(method = RequestMethod.POST, value = "/event_type_mails/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Event_type_mailDTO event_type_maildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(event_type_mailService.checkKey(event_type_mailMapping.toDomain(event_type_maildto)));
    }

    @PreAuthorize("hasPermission(this.event_type_mailMapping.toDomain(#event_type_maildto),'iBizBusinessCentral-Event_type_mail-Save')")
    @ApiOperation(value = "保存基于活动分类的邮件调度", tags = {"基于活动分类的邮件调度" },  notes = "保存基于活动分类的邮件调度")
	@RequestMapping(method = RequestMethod.POST, value = "/event_type_mails/save")
    public ResponseEntity<Boolean> save(@RequestBody Event_type_mailDTO event_type_maildto) {
        return ResponseEntity.status(HttpStatus.OK).body(event_type_mailService.save(event_type_mailMapping.toDomain(event_type_maildto)));
    }

    @PreAuthorize("hasPermission(this.event_type_mailMapping.toDomain(#event_type_maildtos),'iBizBusinessCentral-Event_type_mail-Save')")
    @ApiOperation(value = "批量保存基于活动分类的邮件调度", tags = {"基于活动分类的邮件调度" },  notes = "批量保存基于活动分类的邮件调度")
	@RequestMapping(method = RequestMethod.POST, value = "/event_type_mails/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Event_type_mailDTO> event_type_maildtos) {
        event_type_mailService.saveBatch(event_type_mailMapping.toDomain(event_type_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_type_mail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_type_mail-Get')")
	@ApiOperation(value = "获取数据集", tags = {"基于活动分类的邮件调度" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/event_type_mails/fetchdefault")
	public ResponseEntity<List<Event_type_mailDTO>> fetchDefault(Event_type_mailSearchContext context) {
        Page<Event_type_mail> domains = event_type_mailService.searchDefault(context) ;
        List<Event_type_mailDTO> list = event_type_mailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_type_mail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_type_mail-Get')")
	@ApiOperation(value = "查询数据集", tags = {"基于活动分类的邮件调度" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/event_type_mails/searchdefault")
	public ResponseEntity<Page<Event_type_mailDTO>> searchDefault(@RequestBody Event_type_mailSearchContext context) {
        Page<Event_type_mail> domains = event_type_mailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_type_mailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

