package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_autocomplete_sync;
import cn.ibizlab.businesscentral.core.dto.Res_partner_autocomplete_syncDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreRes_partner_autocomplete_syncMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Res_partner_autocomplete_syncMapping extends MappingBase<Res_partner_autocomplete_syncDTO, Res_partner_autocomplete_sync> {


}

