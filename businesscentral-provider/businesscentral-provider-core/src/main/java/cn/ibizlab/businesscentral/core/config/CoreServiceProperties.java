package cn.ibizlab.businesscentral.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.core")
@Data
public class CoreServiceProperties {

	private boolean enabled;

	private boolean auth;


}