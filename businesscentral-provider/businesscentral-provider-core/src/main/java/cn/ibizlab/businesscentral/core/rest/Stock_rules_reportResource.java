package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rules_report;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_rules_reportService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_rules_reportSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库存规则报告" })
@RestController("Core-stock_rules_report")
@RequestMapping("")
public class Stock_rules_reportResource {

    @Autowired
    public IStock_rules_reportService stock_rules_reportService;

    @Autowired
    @Lazy
    public Stock_rules_reportMapping stock_rules_reportMapping;

    @PreAuthorize("hasPermission(this.stock_rules_reportMapping.toDomain(#stock_rules_reportdto),'iBizBusinessCentral-Stock_rules_report-Create')")
    @ApiOperation(value = "新建库存规则报告", tags = {"库存规则报告" },  notes = "新建库存规则报告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports")
    public ResponseEntity<Stock_rules_reportDTO> create(@Validated @RequestBody Stock_rules_reportDTO stock_rules_reportdto) {
        Stock_rules_report domain = stock_rules_reportMapping.toDomain(stock_rules_reportdto);
		stock_rules_reportService.create(domain);
        Stock_rules_reportDTO dto = stock_rules_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_rules_reportMapping.toDomain(#stock_rules_reportdtos),'iBizBusinessCentral-Stock_rules_report-Create')")
    @ApiOperation(value = "批量新建库存规则报告", tags = {"库存规则报告" },  notes = "批量新建库存规则报告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_rules_reportDTO> stock_rules_reportdtos) {
        stock_rules_reportService.createBatch(stock_rules_reportMapping.toDomain(stock_rules_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_rules_report" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_rules_reportService.get(#stock_rules_report_id),'iBizBusinessCentral-Stock_rules_report-Update')")
    @ApiOperation(value = "更新库存规则报告", tags = {"库存规则报告" },  notes = "更新库存规则报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_rules_reports/{stock_rules_report_id}")
    public ResponseEntity<Stock_rules_reportDTO> update(@PathVariable("stock_rules_report_id") Long stock_rules_report_id, @RequestBody Stock_rules_reportDTO stock_rules_reportdto) {
		Stock_rules_report domain  = stock_rules_reportMapping.toDomain(stock_rules_reportdto);
        domain .setId(stock_rules_report_id);
		stock_rules_reportService.update(domain );
		Stock_rules_reportDTO dto = stock_rules_reportMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_rules_reportService.getStockRulesReportByEntities(this.stock_rules_reportMapping.toDomain(#stock_rules_reportdtos)),'iBizBusinessCentral-Stock_rules_report-Update')")
    @ApiOperation(value = "批量更新库存规则报告", tags = {"库存规则报告" },  notes = "批量更新库存规则报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_rules_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_rules_reportDTO> stock_rules_reportdtos) {
        stock_rules_reportService.updateBatch(stock_rules_reportMapping.toDomain(stock_rules_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_rules_reportService.get(#stock_rules_report_id),'iBizBusinessCentral-Stock_rules_report-Remove')")
    @ApiOperation(value = "删除库存规则报告", tags = {"库存规则报告" },  notes = "删除库存规则报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_rules_reports/{stock_rules_report_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_rules_report_id") Long stock_rules_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_rules_reportService.remove(stock_rules_report_id));
    }

    @PreAuthorize("hasPermission(this.stock_rules_reportService.getStockRulesReportByIds(#ids),'iBizBusinessCentral-Stock_rules_report-Remove')")
    @ApiOperation(value = "批量删除库存规则报告", tags = {"库存规则报告" },  notes = "批量删除库存规则报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_rules_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_rules_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_rules_reportMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_rules_report-Get')")
    @ApiOperation(value = "获取库存规则报告", tags = {"库存规则报告" },  notes = "获取库存规则报告")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_rules_reports/{stock_rules_report_id}")
    public ResponseEntity<Stock_rules_reportDTO> get(@PathVariable("stock_rules_report_id") Long stock_rules_report_id) {
        Stock_rules_report domain = stock_rules_reportService.get(stock_rules_report_id);
        Stock_rules_reportDTO dto = stock_rules_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库存规则报告草稿", tags = {"库存规则报告" },  notes = "获取库存规则报告草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_rules_reports/getdraft")
    public ResponseEntity<Stock_rules_reportDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_rules_reportMapping.toDto(stock_rules_reportService.getDraft(new Stock_rules_report())));
    }

    @ApiOperation(value = "检查库存规则报告", tags = {"库存规则报告" },  notes = "检查库存规则报告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_rules_reportDTO stock_rules_reportdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_rules_reportService.checkKey(stock_rules_reportMapping.toDomain(stock_rules_reportdto)));
    }

    @PreAuthorize("hasPermission(this.stock_rules_reportMapping.toDomain(#stock_rules_reportdto),'iBizBusinessCentral-Stock_rules_report-Save')")
    @ApiOperation(value = "保存库存规则报告", tags = {"库存规则报告" },  notes = "保存库存规则报告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_rules_reportDTO stock_rules_reportdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_rules_reportService.save(stock_rules_reportMapping.toDomain(stock_rules_reportdto)));
    }

    @PreAuthorize("hasPermission(this.stock_rules_reportMapping.toDomain(#stock_rules_reportdtos),'iBizBusinessCentral-Stock_rules_report-Save')")
    @ApiOperation(value = "批量保存库存规则报告", tags = {"库存规则报告" },  notes = "批量保存库存规则报告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_rules_reportDTO> stock_rules_reportdtos) {
        stock_rules_reportService.saveBatch(stock_rules_reportMapping.toDomain(stock_rules_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_rules_report-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_rules_report-Get')")
	@ApiOperation(value = "获取数据集", tags = {"库存规则报告" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_rules_reports/fetchdefault")
	public ResponseEntity<List<Stock_rules_reportDTO>> fetchDefault(Stock_rules_reportSearchContext context) {
        Page<Stock_rules_report> domains = stock_rules_reportService.searchDefault(context) ;
        List<Stock_rules_reportDTO> list = stock_rules_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_rules_report-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_rules_report-Get')")
	@ApiOperation(value = "查询数据集", tags = {"库存规则报告" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_rules_reports/searchdefault")
	public ResponseEntity<Page<Stock_rules_reportDTO>> searchDefault(@RequestBody Stock_rules_reportSearchContext context) {
        Page<Stock_rules_report> domains = stock_rules_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_rules_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

