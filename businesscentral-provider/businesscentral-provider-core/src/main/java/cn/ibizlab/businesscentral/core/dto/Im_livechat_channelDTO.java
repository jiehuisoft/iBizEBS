package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Im_livechat_channelDTO]
 */
@Data
public class Im_livechat_channelDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [USER_IDS]
     *
     */
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String userIds;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String websiteUrl;

    /**
     * 属性 [RULE_IDS]
     *
     */
    @JSONField(name = "rule_ids")
    @JsonProperty("rule_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String ruleIds;

    /**
     * 属性 [IMAGE]
     *
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 属性 [WEB_PAGE]
     *
     */
    @JSONField(name = "web_page")
    @JsonProperty("web_page")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String webPage;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private Boolean isPublished;

    /**
     * 属性 [SCRIPT_EXTERNAL]
     *
     */
    @JSONField(name = "script_external")
    @JsonProperty("script_external")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String scriptExternal;

    /**
     * 属性 [NBR_CHANNEL]
     *
     */
    @JSONField(name = "nbr_channel")
    @JsonProperty("nbr_channel")
    private Integer nbrChannel;

    /**
     * 属性 [BUTTON_TEXT]
     *
     */
    @JSONField(name = "button_text")
    @JsonProperty("button_text")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String buttonText;

    /**
     * 属性 [WEBSITE_DESCRIPTION]
     *
     */
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteDescription;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String channelIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 属性 [RATING_PERCENTAGE_SATISFACTION]
     *
     */
    @JSONField(name = "rating_percentage_satisfaction")
    @JsonProperty("rating_percentage_satisfaction")
    private Integer ratingPercentageSatisfaction;

    /**
     * 属性 [INPUT_PLACEHOLDER]
     *
     */
    @JSONField(name = "input_placeholder")
    @JsonProperty("input_placeholder")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String inputPlaceholder;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 属性 [ARE_YOU_INSIDE]
     *
     */
    @JSONField(name = "are_you_inside")
    @JsonProperty("are_you_inside")
    private Boolean areYouInside;

    /**
     * 属性 [DEFAULT_MESSAGE]
     *
     */
    @JSONField(name = "default_message")
    @JsonProperty("default_message")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String defaultMessage;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [IS_PUBLISHED]
     */
    public void setIsPublished(Boolean  isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }

    /**
     * 设置 [BUTTON_TEXT]
     */
    public void setButtonText(String  buttonText){
        this.buttonText = buttonText ;
        this.modify("button_text",buttonText);
    }

    /**
     * 设置 [WEBSITE_DESCRIPTION]
     */
    public void setWebsiteDescription(String  websiteDescription){
        this.websiteDescription = websiteDescription ;
        this.modify("website_description",websiteDescription);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [INPUT_PLACEHOLDER]
     */
    public void setInputPlaceholder(String  inputPlaceholder){
        this.inputPlaceholder = inputPlaceholder ;
        this.modify("input_placeholder",inputPlaceholder);
    }

    /**
     * 设置 [DEFAULT_MESSAGE]
     */
    public void setDefaultMessage(String  defaultMessage){
        this.defaultMessage = defaultMessage ;
        this.modify("default_message",defaultMessage);
    }


}


