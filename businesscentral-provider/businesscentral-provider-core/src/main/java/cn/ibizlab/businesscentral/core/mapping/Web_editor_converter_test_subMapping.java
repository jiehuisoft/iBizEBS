package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test_sub;
import cn.ibizlab.businesscentral.core.dto.Web_editor_converter_test_subDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreWeb_editor_converter_test_subMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Web_editor_converter_test_subMapping extends MappingBase<Web_editor_converter_test_subDTO, Web_editor_converter_test_sub> {


}

