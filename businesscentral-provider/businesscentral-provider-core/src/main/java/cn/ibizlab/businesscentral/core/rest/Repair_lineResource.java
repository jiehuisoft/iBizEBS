package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_line;
import cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_lineService;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"修理明细行(零件)" })
@RestController("Core-repair_line")
@RequestMapping("")
public class Repair_lineResource {

    @Autowired
    public IRepair_lineService repair_lineService;

    @Autowired
    @Lazy
    public Repair_lineMapping repair_lineMapping;

    @PreAuthorize("hasPermission(this.repair_lineMapping.toDomain(#repair_linedto),'iBizBusinessCentral-Repair_line-Create')")
    @ApiOperation(value = "新建修理明细行(零件)", tags = {"修理明细行(零件)" },  notes = "新建修理明细行(零件)")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_lines")
    public ResponseEntity<Repair_lineDTO> create(@Validated @RequestBody Repair_lineDTO repair_linedto) {
        Repair_line domain = repair_lineMapping.toDomain(repair_linedto);
		repair_lineService.create(domain);
        Repair_lineDTO dto = repair_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.repair_lineMapping.toDomain(#repair_linedtos),'iBizBusinessCentral-Repair_line-Create')")
    @ApiOperation(value = "批量新建修理明细行(零件)", tags = {"修理明细行(零件)" },  notes = "批量新建修理明细行(零件)")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Repair_lineDTO> repair_linedtos) {
        repair_lineService.createBatch(repair_lineMapping.toDomain(repair_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "repair_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.repair_lineService.get(#repair_line_id),'iBizBusinessCentral-Repair_line-Update')")
    @ApiOperation(value = "更新修理明细行(零件)", tags = {"修理明细行(零件)" },  notes = "更新修理明细行(零件)")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_lines/{repair_line_id}")
    public ResponseEntity<Repair_lineDTO> update(@PathVariable("repair_line_id") Long repair_line_id, @RequestBody Repair_lineDTO repair_linedto) {
		Repair_line domain  = repair_lineMapping.toDomain(repair_linedto);
        domain .setId(repair_line_id);
		repair_lineService.update(domain );
		Repair_lineDTO dto = repair_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.repair_lineService.getRepairLineByEntities(this.repair_lineMapping.toDomain(#repair_linedtos)),'iBizBusinessCentral-Repair_line-Update')")
    @ApiOperation(value = "批量更新修理明细行(零件)", tags = {"修理明细行(零件)" },  notes = "批量更新修理明细行(零件)")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_lineDTO> repair_linedtos) {
        repair_lineService.updateBatch(repair_lineMapping.toDomain(repair_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.repair_lineService.get(#repair_line_id),'iBizBusinessCentral-Repair_line-Remove')")
    @ApiOperation(value = "删除修理明细行(零件)", tags = {"修理明细行(零件)" },  notes = "删除修理明细行(零件)")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_lines/{repair_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("repair_line_id") Long repair_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(repair_lineService.remove(repair_line_id));
    }

    @PreAuthorize("hasPermission(this.repair_lineService.getRepairLineByIds(#ids),'iBizBusinessCentral-Repair_line-Remove')")
    @ApiOperation(value = "批量删除修理明细行(零件)", tags = {"修理明细行(零件)" },  notes = "批量删除修理明细行(零件)")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        repair_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.repair_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Repair_line-Get')")
    @ApiOperation(value = "获取修理明细行(零件)", tags = {"修理明细行(零件)" },  notes = "获取修理明细行(零件)")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_lines/{repair_line_id}")
    public ResponseEntity<Repair_lineDTO> get(@PathVariable("repair_line_id") Long repair_line_id) {
        Repair_line domain = repair_lineService.get(repair_line_id);
        Repair_lineDTO dto = repair_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取修理明细行(零件)草稿", tags = {"修理明细行(零件)" },  notes = "获取修理明细行(零件)草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_lines/getdraft")
    public ResponseEntity<Repair_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(repair_lineMapping.toDto(repair_lineService.getDraft(new Repair_line())));
    }

    @ApiOperation(value = "检查修理明细行(零件)", tags = {"修理明细行(零件)" },  notes = "检查修理明细行(零件)")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Repair_lineDTO repair_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(repair_lineService.checkKey(repair_lineMapping.toDomain(repair_linedto)));
    }

    @PreAuthorize("hasPermission(this.repair_lineMapping.toDomain(#repair_linedto),'iBizBusinessCentral-Repair_line-Save')")
    @ApiOperation(value = "保存修理明细行(零件)", tags = {"修理明细行(零件)" },  notes = "保存修理明细行(零件)")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Repair_lineDTO repair_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(repair_lineService.save(repair_lineMapping.toDomain(repair_linedto)));
    }

    @PreAuthorize("hasPermission(this.repair_lineMapping.toDomain(#repair_linedtos),'iBizBusinessCentral-Repair_line-Save')")
    @ApiOperation(value = "批量保存修理明细行(零件)", tags = {"修理明细行(零件)" },  notes = "批量保存修理明细行(零件)")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Repair_lineDTO> repair_linedtos) {
        repair_lineService.saveBatch(repair_lineMapping.toDomain(repair_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Repair_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Repair_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"修理明细行(零件)" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/repair_lines/fetchdefault")
	public ResponseEntity<List<Repair_lineDTO>> fetchDefault(Repair_lineSearchContext context) {
        Page<Repair_line> domains = repair_lineService.searchDefault(context) ;
        List<Repair_lineDTO> list = repair_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Repair_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Repair_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"修理明细行(零件)" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/repair_lines/searchdefault")
	public ResponseEntity<Page<Repair_lineDTO>> searchDefault(@RequestBody Repair_lineSearchContext context) {
        Page<Repair_line> domains = repair_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(repair_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

