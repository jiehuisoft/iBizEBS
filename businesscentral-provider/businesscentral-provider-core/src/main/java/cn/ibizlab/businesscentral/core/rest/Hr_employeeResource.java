package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_employeeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"员工" })
@RestController("Core-hr_employee")
@RequestMapping("")
public class Hr_employeeResource {

    @Autowired
    public IHr_employeeService hr_employeeService;

    @Autowired
    @Lazy
    public Hr_employeeMapping hr_employeeMapping;

    @PreAuthorize("hasPermission(this.hr_employeeMapping.toDomain(#hr_employeedto),'iBizBusinessCentral-Hr_employee-Create')")
    @ApiOperation(value = "新建员工", tags = {"员工" },  notes = "新建员工")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees")
    public ResponseEntity<Hr_employeeDTO> create(@Validated @RequestBody Hr_employeeDTO hr_employeedto) {
        Hr_employee domain = hr_employeeMapping.toDomain(hr_employeedto);
		hr_employeeService.create(domain);
        Hr_employeeDTO dto = hr_employeeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_employeeMapping.toDomain(#hr_employeedtos),'iBizBusinessCentral-Hr_employee-Create')")
    @ApiOperation(value = "批量新建员工", tags = {"员工" },  notes = "批量新建员工")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_employeeDTO> hr_employeedtos) {
        hr_employeeService.createBatch(hr_employeeMapping.toDomain(hr_employeedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_employee" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_employeeService.get(#hr_employee_id),'iBizBusinessCentral-Hr_employee-Update')")
    @ApiOperation(value = "更新员工", tags = {"员工" },  notes = "更新员工")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/{hr_employee_id}")
    public ResponseEntity<Hr_employeeDTO> update(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_employeeDTO hr_employeedto) {
		Hr_employee domain  = hr_employeeMapping.toDomain(hr_employeedto);
        domain .setId(hr_employee_id);
		hr_employeeService.update(domain );
		Hr_employeeDTO dto = hr_employeeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_employeeService.getHrEmployeeByEntities(this.hr_employeeMapping.toDomain(#hr_employeedtos)),'iBizBusinessCentral-Hr_employee-Update')")
    @ApiOperation(value = "批量更新员工", tags = {"员工" },  notes = "批量更新员工")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_employeeDTO> hr_employeedtos) {
        hr_employeeService.updateBatch(hr_employeeMapping.toDomain(hr_employeedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_employeeService.get(#hr_employee_id),'iBizBusinessCentral-Hr_employee-Remove')")
    @ApiOperation(value = "删除员工", tags = {"员工" },  notes = "删除员工")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/{hr_employee_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_employee_id") Long hr_employee_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_employeeService.remove(hr_employee_id));
    }

    @PreAuthorize("hasPermission(this.hr_employeeService.getHrEmployeeByIds(#ids),'iBizBusinessCentral-Hr_employee-Remove')")
    @ApiOperation(value = "批量删除员工", tags = {"员工" },  notes = "批量删除员工")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_employeeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_employeeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_employee-Get')")
    @ApiOperation(value = "获取员工", tags = {"员工" },  notes = "获取员工")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_employees/{hr_employee_id}")
    public ResponseEntity<Hr_employeeDTO> get(@PathVariable("hr_employee_id") Long hr_employee_id) {
        Hr_employee domain = hr_employeeService.get(hr_employee_id);
        Hr_employeeDTO dto = hr_employeeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取员工草稿", tags = {"员工" },  notes = "获取员工草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_employees/getdraft")
    public ResponseEntity<Hr_employeeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_employeeMapping.toDto(hr_employeeService.getDraft(new Hr_employee())));
    }

    @ApiOperation(value = "检查员工", tags = {"员工" },  notes = "检查员工")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_employeeDTO hr_employeedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_employeeService.checkKey(hr_employeeMapping.toDomain(hr_employeedto)));
    }

    @PreAuthorize("hasPermission(this.hr_employeeMapping.toDomain(#hr_employeedto),'iBizBusinessCentral-Hr_employee-Save')")
    @ApiOperation(value = "保存员工", tags = {"员工" },  notes = "保存员工")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_employeeDTO hr_employeedto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_employeeService.save(hr_employeeMapping.toDomain(hr_employeedto)));
    }

    @PreAuthorize("hasPermission(this.hr_employeeMapping.toDomain(#hr_employeedtos),'iBizBusinessCentral-Hr_employee-Save')")
    @ApiOperation(value = "批量保存员工", tags = {"员工" },  notes = "批量保存员工")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_employeeDTO> hr_employeedtos) {
        hr_employeeService.saveBatch(hr_employeeMapping.toDomain(hr_employeedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_employee-Get')")
	@ApiOperation(value = "获取数据集", tags = {"员工" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_employees/fetchdefault")
	public ResponseEntity<List<Hr_employeeDTO>> fetchDefault(Hr_employeeSearchContext context) {
        Page<Hr_employee> domains = hr_employeeService.searchDefault(context) ;
        List<Hr_employeeDTO> list = hr_employeeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_employee-Get')")
	@ApiOperation(value = "查询数据集", tags = {"员工" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_employees/searchdefault")
	public ResponseEntity<Page<Hr_employeeDTO>> searchDefault(@RequestBody Hr_employeeSearchContext context) {
        Page<Hr_employee> domains = hr_employeeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_employeeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Hr_employee-Get')")
	@ApiOperation(value = "获取首选表格", tags = {"员工" } ,notes = "获取首选表格")
    @RequestMapping(method= RequestMethod.GET , value="/hr_employees/fetchmaster")
	public ResponseEntity<List<Hr_employeeDTO>> fetchMaster(Hr_employeeSearchContext context) {
        Page<Hr_employee> domains = hr_employeeService.searchMaster(context) ;
        List<Hr_employeeDTO> list = hr_employeeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_employee-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Hr_employee-Get')")
	@ApiOperation(value = "查询首选表格", tags = {"员工" } ,notes = "查询首选表格")
    @RequestMapping(method= RequestMethod.POST , value="/hr_employees/searchmaster")
	public ResponseEntity<Page<Hr_employeeDTO>> searchMaster(@RequestBody Hr_employeeSearchContext context) {
        Page<Hr_employee> domains = hr_employeeService.searchMaster(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_employeeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

