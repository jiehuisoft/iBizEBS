package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_financial_year_op;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_financial_year_opService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_financial_year_opSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"上个财政年的期初" })
@RestController("Core-account_financial_year_op")
@RequestMapping("")
public class Account_financial_year_opResource {

    @Autowired
    public IAccount_financial_year_opService account_financial_year_opService;

    @Autowired
    @Lazy
    public Account_financial_year_opMapping account_financial_year_opMapping;

    @PreAuthorize("hasPermission(this.account_financial_year_opMapping.toDomain(#account_financial_year_opdto),'iBizBusinessCentral-Account_financial_year_op-Create')")
    @ApiOperation(value = "新建上个财政年的期初", tags = {"上个财政年的期初" },  notes = "新建上个财政年的期初")
	@RequestMapping(method = RequestMethod.POST, value = "/account_financial_year_ops")
    public ResponseEntity<Account_financial_year_opDTO> create(@Validated @RequestBody Account_financial_year_opDTO account_financial_year_opdto) {
        Account_financial_year_op domain = account_financial_year_opMapping.toDomain(account_financial_year_opdto);
		account_financial_year_opService.create(domain);
        Account_financial_year_opDTO dto = account_financial_year_opMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_financial_year_opMapping.toDomain(#account_financial_year_opdtos),'iBizBusinessCentral-Account_financial_year_op-Create')")
    @ApiOperation(value = "批量新建上个财政年的期初", tags = {"上个财政年的期初" },  notes = "批量新建上个财政年的期初")
	@RequestMapping(method = RequestMethod.POST, value = "/account_financial_year_ops/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_financial_year_opDTO> account_financial_year_opdtos) {
        account_financial_year_opService.createBatch(account_financial_year_opMapping.toDomain(account_financial_year_opdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_financial_year_op" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_financial_year_opService.get(#account_financial_year_op_id),'iBizBusinessCentral-Account_financial_year_op-Update')")
    @ApiOperation(value = "更新上个财政年的期初", tags = {"上个财政年的期初" },  notes = "更新上个财政年的期初")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_financial_year_ops/{account_financial_year_op_id}")
    public ResponseEntity<Account_financial_year_opDTO> update(@PathVariable("account_financial_year_op_id") Long account_financial_year_op_id, @RequestBody Account_financial_year_opDTO account_financial_year_opdto) {
		Account_financial_year_op domain  = account_financial_year_opMapping.toDomain(account_financial_year_opdto);
        domain .setId(account_financial_year_op_id);
		account_financial_year_opService.update(domain );
		Account_financial_year_opDTO dto = account_financial_year_opMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_financial_year_opService.getAccountFinancialYearOpByEntities(this.account_financial_year_opMapping.toDomain(#account_financial_year_opdtos)),'iBizBusinessCentral-Account_financial_year_op-Update')")
    @ApiOperation(value = "批量更新上个财政年的期初", tags = {"上个财政年的期初" },  notes = "批量更新上个财政年的期初")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_financial_year_ops/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_financial_year_opDTO> account_financial_year_opdtos) {
        account_financial_year_opService.updateBatch(account_financial_year_opMapping.toDomain(account_financial_year_opdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_financial_year_opService.get(#account_financial_year_op_id),'iBizBusinessCentral-Account_financial_year_op-Remove')")
    @ApiOperation(value = "删除上个财政年的期初", tags = {"上个财政年的期初" },  notes = "删除上个财政年的期初")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_financial_year_ops/{account_financial_year_op_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_financial_year_op_id") Long account_financial_year_op_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_financial_year_opService.remove(account_financial_year_op_id));
    }

    @PreAuthorize("hasPermission(this.account_financial_year_opService.getAccountFinancialYearOpByIds(#ids),'iBizBusinessCentral-Account_financial_year_op-Remove')")
    @ApiOperation(value = "批量删除上个财政年的期初", tags = {"上个财政年的期初" },  notes = "批量删除上个财政年的期初")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_financial_year_ops/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_financial_year_opService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_financial_year_opMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_financial_year_op-Get')")
    @ApiOperation(value = "获取上个财政年的期初", tags = {"上个财政年的期初" },  notes = "获取上个财政年的期初")
	@RequestMapping(method = RequestMethod.GET, value = "/account_financial_year_ops/{account_financial_year_op_id}")
    public ResponseEntity<Account_financial_year_opDTO> get(@PathVariable("account_financial_year_op_id") Long account_financial_year_op_id) {
        Account_financial_year_op domain = account_financial_year_opService.get(account_financial_year_op_id);
        Account_financial_year_opDTO dto = account_financial_year_opMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取上个财政年的期初草稿", tags = {"上个财政年的期初" },  notes = "获取上个财政年的期初草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_financial_year_ops/getdraft")
    public ResponseEntity<Account_financial_year_opDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_financial_year_opMapping.toDto(account_financial_year_opService.getDraft(new Account_financial_year_op())));
    }

    @ApiOperation(value = "检查上个财政年的期初", tags = {"上个财政年的期初" },  notes = "检查上个财政年的期初")
	@RequestMapping(method = RequestMethod.POST, value = "/account_financial_year_ops/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_financial_year_opDTO account_financial_year_opdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_financial_year_opService.checkKey(account_financial_year_opMapping.toDomain(account_financial_year_opdto)));
    }

    @PreAuthorize("hasPermission(this.account_financial_year_opMapping.toDomain(#account_financial_year_opdto),'iBizBusinessCentral-Account_financial_year_op-Save')")
    @ApiOperation(value = "保存上个财政年的期初", tags = {"上个财政年的期初" },  notes = "保存上个财政年的期初")
	@RequestMapping(method = RequestMethod.POST, value = "/account_financial_year_ops/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_financial_year_opDTO account_financial_year_opdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_financial_year_opService.save(account_financial_year_opMapping.toDomain(account_financial_year_opdto)));
    }

    @PreAuthorize("hasPermission(this.account_financial_year_opMapping.toDomain(#account_financial_year_opdtos),'iBizBusinessCentral-Account_financial_year_op-Save')")
    @ApiOperation(value = "批量保存上个财政年的期初", tags = {"上个财政年的期初" },  notes = "批量保存上个财政年的期初")
	@RequestMapping(method = RequestMethod.POST, value = "/account_financial_year_ops/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_financial_year_opDTO> account_financial_year_opdtos) {
        account_financial_year_opService.saveBatch(account_financial_year_opMapping.toDomain(account_financial_year_opdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_financial_year_op-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_financial_year_op-Get')")
	@ApiOperation(value = "获取数据集", tags = {"上个财政年的期初" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_financial_year_ops/fetchdefault")
	public ResponseEntity<List<Account_financial_year_opDTO>> fetchDefault(Account_financial_year_opSearchContext context) {
        Page<Account_financial_year_op> domains = account_financial_year_opService.searchDefault(context) ;
        List<Account_financial_year_opDTO> list = account_financial_year_opMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_financial_year_op-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_financial_year_op-Get')")
	@ApiOperation(value = "查询数据集", tags = {"上个财政年的期初" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_financial_year_ops/searchdefault")
	public ResponseEntity<Page<Account_financial_year_opDTO>> searchDefault(@RequestBody Account_financial_year_opSearchContext context) {
        Page<Account_financial_year_op> domains = account_financial_year_opService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_financial_year_opMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

