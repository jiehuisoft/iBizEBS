package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_log_fuel;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_fuelService;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_log_fuelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"车辆燃油记录" })
@RestController("Core-fleet_vehicle_log_fuel")
@RequestMapping("")
public class Fleet_vehicle_log_fuelResource {

    @Autowired
    public IFleet_vehicle_log_fuelService fleet_vehicle_log_fuelService;

    @Autowired
    @Lazy
    public Fleet_vehicle_log_fuelMapping fleet_vehicle_log_fuelMapping;

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_fuelMapping.toDomain(#fleet_vehicle_log_fueldto),'iBizBusinessCentral-Fleet_vehicle_log_fuel-Create')")
    @ApiOperation(value = "新建车辆燃油记录", tags = {"车辆燃油记录" },  notes = "新建车辆燃油记录")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_fuels")
    public ResponseEntity<Fleet_vehicle_log_fuelDTO> create(@Validated @RequestBody Fleet_vehicle_log_fuelDTO fleet_vehicle_log_fueldto) {
        Fleet_vehicle_log_fuel domain = fleet_vehicle_log_fuelMapping.toDomain(fleet_vehicle_log_fueldto);
		fleet_vehicle_log_fuelService.create(domain);
        Fleet_vehicle_log_fuelDTO dto = fleet_vehicle_log_fuelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_fuelMapping.toDomain(#fleet_vehicle_log_fueldtos),'iBizBusinessCentral-Fleet_vehicle_log_fuel-Create')")
    @ApiOperation(value = "批量新建车辆燃油记录", tags = {"车辆燃油记录" },  notes = "批量新建车辆燃油记录")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_fuels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_log_fuelDTO> fleet_vehicle_log_fueldtos) {
        fleet_vehicle_log_fuelService.createBatch(fleet_vehicle_log_fuelMapping.toDomain(fleet_vehicle_log_fueldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fleet_vehicle_log_fuel" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.fleet_vehicle_log_fuelService.get(#fleet_vehicle_log_fuel_id),'iBizBusinessCentral-Fleet_vehicle_log_fuel-Update')")
    @ApiOperation(value = "更新车辆燃油记录", tags = {"车辆燃油记录" },  notes = "更新车辆燃油记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_fuels/{fleet_vehicle_log_fuel_id}")
    public ResponseEntity<Fleet_vehicle_log_fuelDTO> update(@PathVariable("fleet_vehicle_log_fuel_id") Long fleet_vehicle_log_fuel_id, @RequestBody Fleet_vehicle_log_fuelDTO fleet_vehicle_log_fueldto) {
		Fleet_vehicle_log_fuel domain  = fleet_vehicle_log_fuelMapping.toDomain(fleet_vehicle_log_fueldto);
        domain .setId(fleet_vehicle_log_fuel_id);
		fleet_vehicle_log_fuelService.update(domain );
		Fleet_vehicle_log_fuelDTO dto = fleet_vehicle_log_fuelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_fuelService.getFleetVehicleLogFuelByEntities(this.fleet_vehicle_log_fuelMapping.toDomain(#fleet_vehicle_log_fueldtos)),'iBizBusinessCentral-Fleet_vehicle_log_fuel-Update')")
    @ApiOperation(value = "批量更新车辆燃油记录", tags = {"车辆燃油记录" },  notes = "批量更新车辆燃油记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_fuels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_log_fuelDTO> fleet_vehicle_log_fueldtos) {
        fleet_vehicle_log_fuelService.updateBatch(fleet_vehicle_log_fuelMapping.toDomain(fleet_vehicle_log_fueldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_fuelService.get(#fleet_vehicle_log_fuel_id),'iBizBusinessCentral-Fleet_vehicle_log_fuel-Remove')")
    @ApiOperation(value = "删除车辆燃油记录", tags = {"车辆燃油记录" },  notes = "删除车辆燃油记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_fuels/{fleet_vehicle_log_fuel_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_log_fuel_id") Long fleet_vehicle_log_fuel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_fuelService.remove(fleet_vehicle_log_fuel_id));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_fuelService.getFleetVehicleLogFuelByIds(#ids),'iBizBusinessCentral-Fleet_vehicle_log_fuel-Remove')")
    @ApiOperation(value = "批量删除车辆燃油记录", tags = {"车辆燃油记录" },  notes = "批量删除车辆燃油记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_fuels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        fleet_vehicle_log_fuelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.fleet_vehicle_log_fuelMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fleet_vehicle_log_fuel-Get')")
    @ApiOperation(value = "获取车辆燃油记录", tags = {"车辆燃油记录" },  notes = "获取车辆燃油记录")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_fuels/{fleet_vehicle_log_fuel_id}")
    public ResponseEntity<Fleet_vehicle_log_fuelDTO> get(@PathVariable("fleet_vehicle_log_fuel_id") Long fleet_vehicle_log_fuel_id) {
        Fleet_vehicle_log_fuel domain = fleet_vehicle_log_fuelService.get(fleet_vehicle_log_fuel_id);
        Fleet_vehicle_log_fuelDTO dto = fleet_vehicle_log_fuelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取车辆燃油记录草稿", tags = {"车辆燃油记录" },  notes = "获取车辆燃油记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_fuels/getdraft")
    public ResponseEntity<Fleet_vehicle_log_fuelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_fuelMapping.toDto(fleet_vehicle_log_fuelService.getDraft(new Fleet_vehicle_log_fuel())));
    }

    @ApiOperation(value = "检查车辆燃油记录", tags = {"车辆燃油记录" },  notes = "检查车辆燃油记录")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_fuels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Fleet_vehicle_log_fuelDTO fleet_vehicle_log_fueldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_fuelService.checkKey(fleet_vehicle_log_fuelMapping.toDomain(fleet_vehicle_log_fueldto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_fuelMapping.toDomain(#fleet_vehicle_log_fueldto),'iBizBusinessCentral-Fleet_vehicle_log_fuel-Save')")
    @ApiOperation(value = "保存车辆燃油记录", tags = {"车辆燃油记录" },  notes = "保存车辆燃油记录")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_fuels/save")
    public ResponseEntity<Boolean> save(@RequestBody Fleet_vehicle_log_fuelDTO fleet_vehicle_log_fueldto) {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_fuelService.save(fleet_vehicle_log_fuelMapping.toDomain(fleet_vehicle_log_fueldto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_fuelMapping.toDomain(#fleet_vehicle_log_fueldtos),'iBizBusinessCentral-Fleet_vehicle_log_fuel-Save')")
    @ApiOperation(value = "批量保存车辆燃油记录", tags = {"车辆燃油记录" },  notes = "批量保存车辆燃油记录")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_fuels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Fleet_vehicle_log_fuelDTO> fleet_vehicle_log_fueldtos) {
        fleet_vehicle_log_fuelService.saveBatch(fleet_vehicle_log_fuelMapping.toDomain(fleet_vehicle_log_fueldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_log_fuel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_log_fuel-Get')")
	@ApiOperation(value = "获取数据集", tags = {"车辆燃油记录" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_log_fuels/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_log_fuelDTO>> fetchDefault(Fleet_vehicle_log_fuelSearchContext context) {
        Page<Fleet_vehicle_log_fuel> domains = fleet_vehicle_log_fuelService.searchDefault(context) ;
        List<Fleet_vehicle_log_fuelDTO> list = fleet_vehicle_log_fuelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_log_fuel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_log_fuel-Get')")
	@ApiOperation(value = "查询数据集", tags = {"车辆燃油记录" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/fleet_vehicle_log_fuels/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_log_fuelDTO>> searchDefault(@RequestBody Fleet_vehicle_log_fuelSearchContext context) {
        Page<Fleet_vehicle_log_fuel> domains = fleet_vehicle_log_fuelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_log_fuelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

