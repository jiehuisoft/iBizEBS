package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Product_categoryDTO]
 */
@Data
public class Product_categoryDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     *
     */
    @JSONField(name = "property_stock_valuation_account_id")
    @JsonProperty("property_stock_valuation_account_id")
    private Integer propertyStockValuationAccountId;

    /**
     * 属性 [CHILD_ID]
     *
     */
    @JSONField(name = "child_id")
    @JsonProperty("child_id")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String childId;

    /**
     * 属性 [PRODUCT_COUNT]
     *
     */
    @JSONField(name = "product_count")
    @JsonProperty("product_count")
    private Integer productCount;

    /**
     * 属性 [ROUTE_IDS]
     *
     */
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String routeIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [PROPERTY_VALUATION]
     *
     */
    @JSONField(name = "property_valuation")
    @JsonProperty("property_valuation")
    @NotBlank(message = "[库存计价]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propertyValuation;

    /**
     * 属性 [PARENT_PATH]
     *
     */
    @JSONField(name = "parent_path")
    @JsonProperty("parent_path")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String parentPath;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [COMPLETE_NAME]
     *
     */
    @JSONField(name = "complete_name")
    @JsonProperty("complete_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String completeName;

    /**
     * 属性 [PROPERTY_ACCOUNT_CREDITOR_PRICE_DIFFERENCE_CATEG]
     *
     */
    @JSONField(name = "property_account_creditor_price_difference_categ")
    @JsonProperty("property_account_creditor_price_difference_categ")
    private Integer propertyAccountCreditorPriceDifferenceCateg;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     *
     */
    @JSONField(name = "property_stock_account_output_categ_id")
    @JsonProperty("property_stock_account_output_categ_id")
    private Integer propertyStockAccountOutputCategId;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [TOTAL_ROUTE_IDS]
     *
     */
    @JSONField(name = "total_route_ids")
    @JsonProperty("total_route_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String totalRouteIds;

    /**
     * 属性 [PROPERTY_COST_METHOD]
     *
     */
    @JSONField(name = "property_cost_method")
    @JsonProperty("property_cost_method")
    @NotBlank(message = "[成本方法]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propertyCostMethod;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_CATEG_ID]
     *
     */
    @JSONField(name = "property_account_income_categ_id")
    @JsonProperty("property_account_income_categ_id")
    private Integer propertyAccountIncomeCategId;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID]
     *
     */
    @JSONField(name = "property_account_expense_categ_id")
    @JsonProperty("property_account_expense_categ_id")
    private Integer propertyAccountExpenseCategId;

    /**
     * 属性 [PROPERTY_STOCK_JOURNAL]
     *
     */
    @JSONField(name = "property_stock_journal")
    @JsonProperty("property_stock_journal")
    private Integer propertyStockJournal;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     *
     */
    @JSONField(name = "property_stock_account_input_categ_id")
    @JsonProperty("property_stock_account_input_categ_id")
    private Integer propertyStockAccountInputCategId;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String parentIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [REMOVAL_STRATEGY_ID_TEXT]
     *
     */
    @JSONField(name = "removal_strategy_id_text")
    @JsonProperty("removal_strategy_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String removalStrategyIdText;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [REMOVAL_STRATEGY_ID]
     *
     */
    @JSONField(name = "removal_strategy_id")
    @JsonProperty("removal_strategy_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long removalStrategyId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PARENT_PATH]
     */
    public void setParentPath(String  parentPath){
        this.parentPath = parentPath ;
        this.modify("parent_path",parentPath);
    }

    /**
     * 设置 [COMPLETE_NAME]
     */
    public void setCompleteName(String  completeName){
        this.completeName = completeName ;
        this.modify("complete_name",completeName);
    }

    /**
     * 设置 [PARENT_ID]
     */
    public void setParentId(Long  parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [REMOVAL_STRATEGY_ID]
     */
    public void setRemovalStrategyId(Long  removalStrategyId){
        this.removalStrategyId = removalStrategyId ;
        this.modify("removal_strategy_id",removalStrategyId);
    }


}


