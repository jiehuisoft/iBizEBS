package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel_partner;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_channel_partnerService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_channel_partnerSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"渠道指南" })
@RestController("Core-mail_channel_partner")
@RequestMapping("")
public class Mail_channel_partnerResource {

    @Autowired
    public IMail_channel_partnerService mail_channel_partnerService;

    @Autowired
    @Lazy
    public Mail_channel_partnerMapping mail_channel_partnerMapping;

    @PreAuthorize("hasPermission(this.mail_channel_partnerMapping.toDomain(#mail_channel_partnerdto),'iBizBusinessCentral-Mail_channel_partner-Create')")
    @ApiOperation(value = "新建渠道指南", tags = {"渠道指南" },  notes = "新建渠道指南")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channel_partners")
    public ResponseEntity<Mail_channel_partnerDTO> create(@Validated @RequestBody Mail_channel_partnerDTO mail_channel_partnerdto) {
        Mail_channel_partner domain = mail_channel_partnerMapping.toDomain(mail_channel_partnerdto);
		mail_channel_partnerService.create(domain);
        Mail_channel_partnerDTO dto = mail_channel_partnerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_channel_partnerMapping.toDomain(#mail_channel_partnerdtos),'iBizBusinessCentral-Mail_channel_partner-Create')")
    @ApiOperation(value = "批量新建渠道指南", tags = {"渠道指南" },  notes = "批量新建渠道指南")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channel_partners/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_channel_partnerDTO> mail_channel_partnerdtos) {
        mail_channel_partnerService.createBatch(mail_channel_partnerMapping.toDomain(mail_channel_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_channel_partner" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_channel_partnerService.get(#mail_channel_partner_id),'iBizBusinessCentral-Mail_channel_partner-Update')")
    @ApiOperation(value = "更新渠道指南", tags = {"渠道指南" },  notes = "更新渠道指南")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_channel_partners/{mail_channel_partner_id}")
    public ResponseEntity<Mail_channel_partnerDTO> update(@PathVariable("mail_channel_partner_id") Long mail_channel_partner_id, @RequestBody Mail_channel_partnerDTO mail_channel_partnerdto) {
		Mail_channel_partner domain  = mail_channel_partnerMapping.toDomain(mail_channel_partnerdto);
        domain .setId(mail_channel_partner_id);
		mail_channel_partnerService.update(domain );
		Mail_channel_partnerDTO dto = mail_channel_partnerMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_channel_partnerService.getMailChannelPartnerByEntities(this.mail_channel_partnerMapping.toDomain(#mail_channel_partnerdtos)),'iBizBusinessCentral-Mail_channel_partner-Update')")
    @ApiOperation(value = "批量更新渠道指南", tags = {"渠道指南" },  notes = "批量更新渠道指南")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_channel_partners/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_channel_partnerDTO> mail_channel_partnerdtos) {
        mail_channel_partnerService.updateBatch(mail_channel_partnerMapping.toDomain(mail_channel_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_channel_partnerService.get(#mail_channel_partner_id),'iBizBusinessCentral-Mail_channel_partner-Remove')")
    @ApiOperation(value = "删除渠道指南", tags = {"渠道指南" },  notes = "删除渠道指南")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_channel_partners/{mail_channel_partner_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_channel_partner_id") Long mail_channel_partner_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_channel_partnerService.remove(mail_channel_partner_id));
    }

    @PreAuthorize("hasPermission(this.mail_channel_partnerService.getMailChannelPartnerByIds(#ids),'iBizBusinessCentral-Mail_channel_partner-Remove')")
    @ApiOperation(value = "批量删除渠道指南", tags = {"渠道指南" },  notes = "批量删除渠道指南")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_channel_partners/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_channel_partnerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_channel_partnerMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_channel_partner-Get')")
    @ApiOperation(value = "获取渠道指南", tags = {"渠道指南" },  notes = "获取渠道指南")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_channel_partners/{mail_channel_partner_id}")
    public ResponseEntity<Mail_channel_partnerDTO> get(@PathVariable("mail_channel_partner_id") Long mail_channel_partner_id) {
        Mail_channel_partner domain = mail_channel_partnerService.get(mail_channel_partner_id);
        Mail_channel_partnerDTO dto = mail_channel_partnerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取渠道指南草稿", tags = {"渠道指南" },  notes = "获取渠道指南草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_channel_partners/getdraft")
    public ResponseEntity<Mail_channel_partnerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_channel_partnerMapping.toDto(mail_channel_partnerService.getDraft(new Mail_channel_partner())));
    }

    @ApiOperation(value = "检查渠道指南", tags = {"渠道指南" },  notes = "检查渠道指南")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channel_partners/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_channel_partnerDTO mail_channel_partnerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_channel_partnerService.checkKey(mail_channel_partnerMapping.toDomain(mail_channel_partnerdto)));
    }

    @PreAuthorize("hasPermission(this.mail_channel_partnerMapping.toDomain(#mail_channel_partnerdto),'iBizBusinessCentral-Mail_channel_partner-Save')")
    @ApiOperation(value = "保存渠道指南", tags = {"渠道指南" },  notes = "保存渠道指南")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channel_partners/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_channel_partnerDTO mail_channel_partnerdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_channel_partnerService.save(mail_channel_partnerMapping.toDomain(mail_channel_partnerdto)));
    }

    @PreAuthorize("hasPermission(this.mail_channel_partnerMapping.toDomain(#mail_channel_partnerdtos),'iBizBusinessCentral-Mail_channel_partner-Save')")
    @ApiOperation(value = "批量保存渠道指南", tags = {"渠道指南" },  notes = "批量保存渠道指南")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channel_partners/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_channel_partnerDTO> mail_channel_partnerdtos) {
        mail_channel_partnerService.saveBatch(mail_channel_partnerMapping.toDomain(mail_channel_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_channel_partner-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_channel_partner-Get')")
	@ApiOperation(value = "获取数据集", tags = {"渠道指南" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_channel_partners/fetchdefault")
	public ResponseEntity<List<Mail_channel_partnerDTO>> fetchDefault(Mail_channel_partnerSearchContext context) {
        Page<Mail_channel_partner> domains = mail_channel_partnerService.searchDefault(context) ;
        List<Mail_channel_partnerDTO> list = mail_channel_partnerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_channel_partner-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_channel_partner-Get')")
	@ApiOperation(value = "查询数据集", tags = {"渠道指南" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_channel_partners/searchdefault")
	public ResponseEntity<Page<Mail_channel_partnerDTO>> searchDefault(@RequestBody Mail_channel_partnerSearchContext context) {
        Page<Mail_channel_partner> domains = mail_channel_partnerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_channel_partnerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

