package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_bank;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_bankService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_bankSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"银行账户" })
@RestController("Core-res_partner_bank")
@RequestMapping("")
public class Res_partner_bankResource {

    @Autowired
    public IRes_partner_bankService res_partner_bankService;

    @Autowired
    @Lazy
    public Res_partner_bankMapping res_partner_bankMapping;

    @PreAuthorize("hasPermission(this.res_partner_bankMapping.toDomain(#res_partner_bankdto),'iBizBusinessCentral-Res_partner_bank-Create')")
    @ApiOperation(value = "新建银行账户", tags = {"银行账户" },  notes = "新建银行账户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_banks")
    public ResponseEntity<Res_partner_bankDTO> create(@Validated @RequestBody Res_partner_bankDTO res_partner_bankdto) {
        Res_partner_bank domain = res_partner_bankMapping.toDomain(res_partner_bankdto);
		res_partner_bankService.create(domain);
        Res_partner_bankDTO dto = res_partner_bankMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partner_bankMapping.toDomain(#res_partner_bankdtos),'iBizBusinessCentral-Res_partner_bank-Create')")
    @ApiOperation(value = "批量新建银行账户", tags = {"银行账户" },  notes = "批量新建银行账户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_banks/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_partner_bankDTO> res_partner_bankdtos) {
        res_partner_bankService.createBatch(res_partner_bankMapping.toDomain(res_partner_bankdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_partner_bank" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_partner_bankService.get(#res_partner_bank_id),'iBizBusinessCentral-Res_partner_bank-Update')")
    @ApiOperation(value = "更新银行账户", tags = {"银行账户" },  notes = "更新银行账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_banks/{res_partner_bank_id}")
    public ResponseEntity<Res_partner_bankDTO> update(@PathVariable("res_partner_bank_id") Long res_partner_bank_id, @RequestBody Res_partner_bankDTO res_partner_bankdto) {
		Res_partner_bank domain  = res_partner_bankMapping.toDomain(res_partner_bankdto);
        domain .setId(res_partner_bank_id);
		res_partner_bankService.update(domain );
		Res_partner_bankDTO dto = res_partner_bankMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partner_bankService.getResPartnerBankByEntities(this.res_partner_bankMapping.toDomain(#res_partner_bankdtos)),'iBizBusinessCentral-Res_partner_bank-Update')")
    @ApiOperation(value = "批量更新银行账户", tags = {"银行账户" },  notes = "批量更新银行账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_banks/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_bankDTO> res_partner_bankdtos) {
        res_partner_bankService.updateBatch(res_partner_bankMapping.toDomain(res_partner_bankdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_partner_bankService.get(#res_partner_bank_id),'iBizBusinessCentral-Res_partner_bank-Remove')")
    @ApiOperation(value = "删除银行账户", tags = {"银行账户" },  notes = "删除银行账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_banks/{res_partner_bank_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_bank_id") Long res_partner_bank_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_partner_bankService.remove(res_partner_bank_id));
    }

    @PreAuthorize("hasPermission(this.res_partner_bankService.getResPartnerBankByIds(#ids),'iBizBusinessCentral-Res_partner_bank-Remove')")
    @ApiOperation(value = "批量删除银行账户", tags = {"银行账户" },  notes = "批量删除银行账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_banks/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_partner_bankService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_partner_bankMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_partner_bank-Get')")
    @ApiOperation(value = "获取银行账户", tags = {"银行账户" },  notes = "获取银行账户")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_banks/{res_partner_bank_id}")
    public ResponseEntity<Res_partner_bankDTO> get(@PathVariable("res_partner_bank_id") Long res_partner_bank_id) {
        Res_partner_bank domain = res_partner_bankService.get(res_partner_bank_id);
        Res_partner_bankDTO dto = res_partner_bankMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取银行账户草稿", tags = {"银行账户" },  notes = "获取银行账户草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_banks/getdraft")
    public ResponseEntity<Res_partner_bankDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_bankMapping.toDto(res_partner_bankService.getDraft(new Res_partner_bank())));
    }

    @ApiOperation(value = "检查银行账户", tags = {"银行账户" },  notes = "检查银行账户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_banks/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_partner_bankDTO res_partner_bankdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_partner_bankService.checkKey(res_partner_bankMapping.toDomain(res_partner_bankdto)));
    }

    @PreAuthorize("hasPermission(this.res_partner_bankMapping.toDomain(#res_partner_bankdto),'iBizBusinessCentral-Res_partner_bank-Save')")
    @ApiOperation(value = "保存银行账户", tags = {"银行账户" },  notes = "保存银行账户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_banks/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_partner_bankDTO res_partner_bankdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_bankService.save(res_partner_bankMapping.toDomain(res_partner_bankdto)));
    }

    @PreAuthorize("hasPermission(this.res_partner_bankMapping.toDomain(#res_partner_bankdtos),'iBizBusinessCentral-Res_partner_bank-Save')")
    @ApiOperation(value = "批量保存银行账户", tags = {"银行账户" },  notes = "批量保存银行账户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_banks/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_partner_bankDTO> res_partner_bankdtos) {
        res_partner_bankService.saveBatch(res_partner_bankMapping.toDomain(res_partner_bankdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner_bank-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner_bank-Get')")
	@ApiOperation(value = "获取数据集", tags = {"银行账户" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_banks/fetchdefault")
	public ResponseEntity<List<Res_partner_bankDTO>> fetchDefault(Res_partner_bankSearchContext context) {
        Page<Res_partner_bank> domains = res_partner_bankService.searchDefault(context) ;
        List<Res_partner_bankDTO> list = res_partner_bankMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner_bank-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner_bank-Get')")
	@ApiOperation(value = "查询数据集", tags = {"银行账户" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_partner_banks/searchdefault")
	public ResponseEntity<Page<Res_partner_bankDTO>> searchDefault(@RequestBody Res_partner_bankSearchContext context) {
        Page<Res_partner_bank> domains = res_partner_bankService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_bankMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasPermission(this.res_partner_bankMapping.toDomain(#res_partner_bankdto),'iBizBusinessCentral-Res_partner_bank-Create')")
    @ApiOperation(value = "根据供应商建立银行账户", tags = {"银行账户" },  notes = "根据供应商建立银行账户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/res_partner_banks")
    public ResponseEntity<Res_partner_bankDTO> createByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Res_partner_bankDTO res_partner_bankdto) {
        Res_partner_bank domain = res_partner_bankMapping.toDomain(res_partner_bankdto);
        domain.setPartnerId(res_supplier_id);
		res_partner_bankService.create(domain);
        Res_partner_bankDTO dto = res_partner_bankMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partner_bankMapping.toDomain(#res_partner_bankdtos),'iBizBusinessCentral-Res_partner_bank-Create')")
    @ApiOperation(value = "根据供应商批量建立银行账户", tags = {"银行账户" },  notes = "根据供应商批量建立银行账户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/res_partner_banks/batch")
    public ResponseEntity<Boolean> createBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Res_partner_bankDTO> res_partner_bankdtos) {
        List<Res_partner_bank> domainlist=res_partner_bankMapping.toDomain(res_partner_bankdtos);
        for(Res_partner_bank domain:domainlist){
            domain.setPartnerId(res_supplier_id);
        }
        res_partner_bankService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_partner_bank" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_partner_bankService.get(#res_partner_bank_id),'iBizBusinessCentral-Res_partner_bank-Update')")
    @ApiOperation(value = "根据供应商更新银行账户", tags = {"银行账户" },  notes = "根据供应商更新银行账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/res_partner_banks/{res_partner_bank_id}")
    public ResponseEntity<Res_partner_bankDTO> updateByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("res_partner_bank_id") Long res_partner_bank_id, @RequestBody Res_partner_bankDTO res_partner_bankdto) {
        Res_partner_bank domain = res_partner_bankMapping.toDomain(res_partner_bankdto);
        domain.setPartnerId(res_supplier_id);
        domain.setId(res_partner_bank_id);
		res_partner_bankService.update(domain);
        Res_partner_bankDTO dto = res_partner_bankMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partner_bankService.getResPartnerBankByEntities(this.res_partner_bankMapping.toDomain(#res_partner_bankdtos)),'iBizBusinessCentral-Res_partner_bank-Update')")
    @ApiOperation(value = "根据供应商批量更新银行账户", tags = {"银行账户" },  notes = "根据供应商批量更新银行账户")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/res_partner_banks/batch")
    public ResponseEntity<Boolean> updateBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Res_partner_bankDTO> res_partner_bankdtos) {
        List<Res_partner_bank> domainlist=res_partner_bankMapping.toDomain(res_partner_bankdtos);
        for(Res_partner_bank domain:domainlist){
            domain.setPartnerId(res_supplier_id);
        }
        res_partner_bankService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_partner_bankService.get(#res_partner_bank_id),'iBizBusinessCentral-Res_partner_bank-Remove')")
    @ApiOperation(value = "根据供应商删除银行账户", tags = {"银行账户" },  notes = "根据供应商删除银行账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/res_partner_banks/{res_partner_bank_id}")
    public ResponseEntity<Boolean> removeByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("res_partner_bank_id") Long res_partner_bank_id) {
		return ResponseEntity.status(HttpStatus.OK).body(res_partner_bankService.remove(res_partner_bank_id));
    }

    @PreAuthorize("hasPermission(this.res_partner_bankService.getResPartnerBankByIds(#ids),'iBizBusinessCentral-Res_partner_bank-Remove')")
    @ApiOperation(value = "根据供应商批量删除银行账户", tags = {"银行账户" },  notes = "根据供应商批量删除银行账户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/res_partner_banks/batch")
    public ResponseEntity<Boolean> removeBatchByRes_supplier(@RequestBody List<Long> ids) {
        res_partner_bankService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_partner_bankMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_partner_bank-Get')")
    @ApiOperation(value = "根据供应商获取银行账户", tags = {"银行账户" },  notes = "根据供应商获取银行账户")
	@RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/res_partner_banks/{res_partner_bank_id}")
    public ResponseEntity<Res_partner_bankDTO> getByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("res_partner_bank_id") Long res_partner_bank_id) {
        Res_partner_bank domain = res_partner_bankService.get(res_partner_bank_id);
        Res_partner_bankDTO dto = res_partner_bankMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据供应商获取银行账户草稿", tags = {"银行账户" },  notes = "根据供应商获取银行账户草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/res_partner_banks/getdraft")
    public ResponseEntity<Res_partner_bankDTO> getDraftByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id) {
        Res_partner_bank domain = new Res_partner_bank();
        domain.setPartnerId(res_supplier_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_bankMapping.toDto(res_partner_bankService.getDraft(domain)));
    }

    @ApiOperation(value = "根据供应商检查银行账户", tags = {"银行账户" },  notes = "根据供应商检查银行账户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/res_partner_banks/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Res_partner_bankDTO res_partner_bankdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_partner_bankService.checkKey(res_partner_bankMapping.toDomain(res_partner_bankdto)));
    }

    @PreAuthorize("hasPermission(this.res_partner_bankMapping.toDomain(#res_partner_bankdto),'iBizBusinessCentral-Res_partner_bank-Save')")
    @ApiOperation(value = "根据供应商保存银行账户", tags = {"银行账户" },  notes = "根据供应商保存银行账户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/res_partner_banks/save")
    public ResponseEntity<Boolean> saveByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Res_partner_bankDTO res_partner_bankdto) {
        Res_partner_bank domain = res_partner_bankMapping.toDomain(res_partner_bankdto);
        domain.setPartnerId(res_supplier_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_bankService.save(domain));
    }

    @PreAuthorize("hasPermission(this.res_partner_bankMapping.toDomain(#res_partner_bankdtos),'iBizBusinessCentral-Res_partner_bank-Save')")
    @ApiOperation(value = "根据供应商批量保存银行账户", tags = {"银行账户" },  notes = "根据供应商批量保存银行账户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/res_partner_banks/savebatch")
    public ResponseEntity<Boolean> saveBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Res_partner_bankDTO> res_partner_bankdtos) {
        List<Res_partner_bank> domainlist=res_partner_bankMapping.toDomain(res_partner_bankdtos);
        for(Res_partner_bank domain:domainlist){
             domain.setPartnerId(res_supplier_id);
        }
        res_partner_bankService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner_bank-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner_bank-Get')")
	@ApiOperation(value = "根据供应商获取数据集", tags = {"银行账户" } ,notes = "根据供应商获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/res_partner_banks/fetchdefault")
	public ResponseEntity<List<Res_partner_bankDTO>> fetchRes_partner_bankDefaultByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id,Res_partner_bankSearchContext context) {
        context.setN_partner_id_eq(res_supplier_id);
        Page<Res_partner_bank> domains = res_partner_bankService.searchDefault(context) ;
        List<Res_partner_bankDTO> list = res_partner_bankMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner_bank-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner_bank-Get')")
	@ApiOperation(value = "根据供应商查询数据集", tags = {"银行账户" } ,notes = "根据供应商查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/res_partner_banks/searchdefault")
	public ResponseEntity<Page<Res_partner_bankDTO>> searchRes_partner_bankDefaultByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Res_partner_bankSearchContext context) {
        context.setN_partner_id_eq(res_supplier_id);
        Page<Res_partner_bank> domains = res_partner_bankService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_bankMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

