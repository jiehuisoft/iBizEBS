package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse_orderpoint;
import cn.ibizlab.businesscentral.core.dto.Stock_warehouse_orderpointDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreStock_warehouse_orderpointMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_warehouse_orderpointMapping extends MappingBase<Stock_warehouse_orderpointDTO, Stock_warehouse_orderpoint> {


}

