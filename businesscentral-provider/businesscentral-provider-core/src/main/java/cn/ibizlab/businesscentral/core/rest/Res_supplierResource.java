package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_supplierService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_supplierSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"供应商" })
@RestController("Core-res_supplier")
@RequestMapping("")
public class Res_supplierResource {

    @Autowired
    public IRes_supplierService res_supplierService;

    @Autowired
    @Lazy
    public Res_supplierMapping res_supplierMapping;

    @PreAuthorize("hasPermission(this.res_supplierMapping.toDomain(#res_supplierdto),'iBizBusinessCentral-Res_supplier-Create')")
    @ApiOperation(value = "新建供应商", tags = {"供应商" },  notes = "新建供应商")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers")
    public ResponseEntity<Res_supplierDTO> create(@Validated @RequestBody Res_supplierDTO res_supplierdto) {
        Res_supplier domain = res_supplierMapping.toDomain(res_supplierdto);
		res_supplierService.create(domain);
        Res_supplierDTO dto = res_supplierMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_supplierMapping.toDomain(#res_supplierdtos),'iBizBusinessCentral-Res_supplier-Create')")
    @ApiOperation(value = "批量新建供应商", tags = {"供应商" },  notes = "批量新建供应商")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_supplierDTO> res_supplierdtos) {
        res_supplierService.createBatch(res_supplierMapping.toDomain(res_supplierdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_supplierService.get(#res_supplier_id),'iBizBusinessCentral-Res_supplier-Update')")
    @ApiOperation(value = "更新供应商", tags = {"供应商" },  notes = "更新供应商")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}")
    public ResponseEntity<Res_supplierDTO> update(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Res_supplierDTO res_supplierdto) {
		Res_supplier domain  = res_supplierMapping.toDomain(res_supplierdto);
        domain .setId(res_supplier_id);
		res_supplierService.update(domain );
		Res_supplierDTO dto = res_supplierMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_supplierService.getResSupplierByEntities(this.res_supplierMapping.toDomain(#res_supplierdtos)),'iBizBusinessCentral-Res_supplier-Update')")
    @ApiOperation(value = "批量更新供应商", tags = {"供应商" },  notes = "批量更新供应商")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_supplierDTO> res_supplierdtos) {
        res_supplierService.updateBatch(res_supplierMapping.toDomain(res_supplierdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_supplierService.get(#res_supplier_id),'iBizBusinessCentral-Res_supplier-Remove')")
    @ApiOperation(value = "删除供应商", tags = {"供应商" },  notes = "删除供应商")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_supplier_id") Long res_supplier_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_supplierService.remove(res_supplier_id));
    }

    @PreAuthorize("hasPermission(this.res_supplierService.getResSupplierByIds(#ids),'iBizBusinessCentral-Res_supplier-Remove')")
    @ApiOperation(value = "批量删除供应商", tags = {"供应商" },  notes = "批量删除供应商")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_supplierService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_supplierMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_supplier-Get')")
    @ApiOperation(value = "获取供应商", tags = {"供应商" },  notes = "获取供应商")
	@RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}")
    public ResponseEntity<Res_supplierDTO> get(@PathVariable("res_supplier_id") Long res_supplier_id) {
        Res_supplier domain = res_supplierService.get(res_supplier_id);
        Res_supplierDTO dto = res_supplierMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取供应商草稿", tags = {"供应商" },  notes = "获取供应商草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/getdraft")
    public ResponseEntity<Res_supplierDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_supplierMapping.toDto(res_supplierService.getDraft(new Res_supplier())));
    }

    @ApiOperation(value = "检查供应商", tags = {"供应商" },  notes = "检查供应商")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_supplierDTO res_supplierdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_supplierService.checkKey(res_supplierMapping.toDomain(res_supplierdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_supplier-MasterTabCount-all')")
    @ApiOperation(value = "MasterTabCount", tags = {"供应商" },  notes = "MasterTabCount")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/mastertabcount")
    public ResponseEntity<Res_supplierDTO> masterTabCount(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Res_supplierDTO res_supplierdto) {
        Res_supplier domain = res_supplierMapping.toDomain(res_supplierdto);
        domain.setId(res_supplier_id);
        domain = res_supplierService.masterTabCount(domain);
        res_supplierdto = res_supplierMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(res_supplierdto);
    }

    @PreAuthorize("hasPermission(this.res_supplierMapping.toDomain(#res_supplierdto),'iBizBusinessCentral-Res_supplier-Save')")
    @ApiOperation(value = "保存供应商", tags = {"供应商" },  notes = "保存供应商")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_supplierDTO res_supplierdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_supplierService.save(res_supplierMapping.toDomain(res_supplierdto)));
    }

    @PreAuthorize("hasPermission(this.res_supplierMapping.toDomain(#res_supplierdtos),'iBizBusinessCentral-Res_supplier-Save')")
    @ApiOperation(value = "批量保存供应商", tags = {"供应商" },  notes = "批量保存供应商")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_supplierDTO> res_supplierdtos) {
        res_supplierService.saveBatch(res_supplierMapping.toDomain(res_supplierdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_supplier-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_supplier-Get')")
	@ApiOperation(value = "获取数据集", tags = {"供应商" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/fetchdefault")
	public ResponseEntity<List<Res_supplierDTO>> fetchDefault(Res_supplierSearchContext context) {
        Page<Res_supplier> domains = res_supplierService.searchDefault(context) ;
        List<Res_supplierDTO> list = res_supplierMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_supplier-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_supplier-Get')")
	@ApiOperation(value = "查询数据集", tags = {"供应商" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/searchdefault")
	public ResponseEntity<Page<Res_supplierDTO>> searchDefault(@RequestBody Res_supplierSearchContext context) {
        Page<Res_supplier> domains = res_supplierService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_supplierMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_supplier-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Res_supplier-Get')")
	@ApiOperation(value = "获取首选表格", tags = {"供应商" } ,notes = "获取首选表格")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/fetchmaster")
	public ResponseEntity<List<Res_supplierDTO>> fetchMaster(Res_supplierSearchContext context) {
        Page<Res_supplier> domains = res_supplierService.searchMaster(context) ;
        List<Res_supplierDTO> list = res_supplierMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_supplier-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Res_supplier-Get')")
	@ApiOperation(value = "查询首选表格", tags = {"供应商" } ,notes = "查询首选表格")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/searchmaster")
	public ResponseEntity<Page<Res_supplierDTO>> searchMaster(@RequestBody Res_supplierSearchContext context) {
        Page<Res_supplier> domains = res_supplierService.searchMaster(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_supplierMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

