package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Mail_statistics_reportDTO]
 */
@Data
public class Mail_statistics_reportDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CLICKED]
     *
     */
    @JSONField(name = "clicked")
    @JsonProperty("clicked")
    private Integer clicked;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [BOUNCED]
     *
     */
    @JSONField(name = "bounced")
    @JsonProperty("bounced")
    private Integer bounced;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [REPLIED]
     *
     */
    @JSONField(name = "replied")
    @JsonProperty("replied")
    private Integer replied;

    /**
     * 属性 [CAMPAIGN]
     *
     */
    @JSONField(name = "campaign")
    @JsonProperty("campaign")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String campaign;

    /**
     * 属性 [SCHEDULED_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduled_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduled_date")
    private Timestamp scheduledDate;

    /**
     * 属性 [SENT]
     *
     */
    @JSONField(name = "sent")
    @JsonProperty("sent")
    private Integer sent;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String emailFrom;

    /**
     * 属性 [DELIVERED]
     *
     */
    @JSONField(name = "delivered")
    @JsonProperty("delivered")
    private Integer delivered;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [OPENED]
     *
     */
    @JSONField(name = "opened")
    @JsonProperty("opened")
    private Integer opened;


    /**
     * 设置 [CLICKED]
     */
    public void setClicked(Integer  clicked){
        this.clicked = clicked ;
        this.modify("clicked",clicked);
    }

    /**
     * 设置 [BOUNCED]
     */
    public void setBounced(Integer  bounced){
        this.bounced = bounced ;
        this.modify("bounced",bounced);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [REPLIED]
     */
    public void setReplied(Integer  replied){
        this.replied = replied ;
        this.modify("replied",replied);
    }

    /**
     * 设置 [CAMPAIGN]
     */
    public void setCampaign(String  campaign){
        this.campaign = campaign ;
        this.modify("campaign",campaign);
    }

    /**
     * 设置 [SCHEDULED_DATE]
     */
    public void setScheduledDate(Timestamp  scheduledDate){
        this.scheduledDate = scheduledDate ;
        this.modify("scheduled_date",scheduledDate);
    }

    /**
     * 设置 [SENT]
     */
    public void setSent(Integer  sent){
        this.sent = sent ;
        this.modify("sent",sent);
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    public void setEmailFrom(String  emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }

    /**
     * 设置 [DELIVERED]
     */
    public void setDelivered(Integer  delivered){
        this.delivered = delivered ;
        this.modify("delivered",delivered);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [OPENED]
     */
    public void setOpened(Integer  opened){
        this.opened = opened ;
        this.modify("opened",opened);
    }


}


