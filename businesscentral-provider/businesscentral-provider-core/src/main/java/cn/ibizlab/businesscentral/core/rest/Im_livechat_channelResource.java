package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_channelService;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.filter.Im_livechat_channelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"即时聊天" })
@RestController("Core-im_livechat_channel")
@RequestMapping("")
public class Im_livechat_channelResource {

    @Autowired
    public IIm_livechat_channelService im_livechat_channelService;

    @Autowired
    @Lazy
    public Im_livechat_channelMapping im_livechat_channelMapping;

    @PreAuthorize("hasPermission(this.im_livechat_channelMapping.toDomain(#im_livechat_channeldto),'iBizBusinessCentral-Im_livechat_channel-Create')")
    @ApiOperation(value = "新建即时聊天", tags = {"即时聊天" },  notes = "新建即时聊天")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channels")
    public ResponseEntity<Im_livechat_channelDTO> create(@Validated @RequestBody Im_livechat_channelDTO im_livechat_channeldto) {
        Im_livechat_channel domain = im_livechat_channelMapping.toDomain(im_livechat_channeldto);
		im_livechat_channelService.create(domain);
        Im_livechat_channelDTO dto = im_livechat_channelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.im_livechat_channelMapping.toDomain(#im_livechat_channeldtos),'iBizBusinessCentral-Im_livechat_channel-Create')")
    @ApiOperation(value = "批量新建即时聊天", tags = {"即时聊天" },  notes = "批量新建即时聊天")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Im_livechat_channelDTO> im_livechat_channeldtos) {
        im_livechat_channelService.createBatch(im_livechat_channelMapping.toDomain(im_livechat_channeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "im_livechat_channel" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.im_livechat_channelService.get(#im_livechat_channel_id),'iBizBusinessCentral-Im_livechat_channel-Update')")
    @ApiOperation(value = "更新即时聊天", tags = {"即时聊天" },  notes = "更新即时聊天")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_channels/{im_livechat_channel_id}")
    public ResponseEntity<Im_livechat_channelDTO> update(@PathVariable("im_livechat_channel_id") Long im_livechat_channel_id, @RequestBody Im_livechat_channelDTO im_livechat_channeldto) {
		Im_livechat_channel domain  = im_livechat_channelMapping.toDomain(im_livechat_channeldto);
        domain .setId(im_livechat_channel_id);
		im_livechat_channelService.update(domain );
		Im_livechat_channelDTO dto = im_livechat_channelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.im_livechat_channelService.getImLivechatChannelByEntities(this.im_livechat_channelMapping.toDomain(#im_livechat_channeldtos)),'iBizBusinessCentral-Im_livechat_channel-Update')")
    @ApiOperation(value = "批量更新即时聊天", tags = {"即时聊天" },  notes = "批量更新即时聊天")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_channels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Im_livechat_channelDTO> im_livechat_channeldtos) {
        im_livechat_channelService.updateBatch(im_livechat_channelMapping.toDomain(im_livechat_channeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.im_livechat_channelService.get(#im_livechat_channel_id),'iBizBusinessCentral-Im_livechat_channel-Remove')")
    @ApiOperation(value = "删除即时聊天", tags = {"即时聊天" },  notes = "删除即时聊天")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_channels/{im_livechat_channel_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("im_livechat_channel_id") Long im_livechat_channel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(im_livechat_channelService.remove(im_livechat_channel_id));
    }

    @PreAuthorize("hasPermission(this.im_livechat_channelService.getImLivechatChannelByIds(#ids),'iBizBusinessCentral-Im_livechat_channel-Remove')")
    @ApiOperation(value = "批量删除即时聊天", tags = {"即时聊天" },  notes = "批量删除即时聊天")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_channels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        im_livechat_channelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.im_livechat_channelMapping.toDomain(returnObject.body),'iBizBusinessCentral-Im_livechat_channel-Get')")
    @ApiOperation(value = "获取即时聊天", tags = {"即时聊天" },  notes = "获取即时聊天")
	@RequestMapping(method = RequestMethod.GET, value = "/im_livechat_channels/{im_livechat_channel_id}")
    public ResponseEntity<Im_livechat_channelDTO> get(@PathVariable("im_livechat_channel_id") Long im_livechat_channel_id) {
        Im_livechat_channel domain = im_livechat_channelService.get(im_livechat_channel_id);
        Im_livechat_channelDTO dto = im_livechat_channelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取即时聊天草稿", tags = {"即时聊天" },  notes = "获取即时聊天草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/im_livechat_channels/getdraft")
    public ResponseEntity<Im_livechat_channelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(im_livechat_channelMapping.toDto(im_livechat_channelService.getDraft(new Im_livechat_channel())));
    }

    @ApiOperation(value = "检查即时聊天", tags = {"即时聊天" },  notes = "检查即时聊天")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Im_livechat_channelDTO im_livechat_channeldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(im_livechat_channelService.checkKey(im_livechat_channelMapping.toDomain(im_livechat_channeldto)));
    }

    @PreAuthorize("hasPermission(this.im_livechat_channelMapping.toDomain(#im_livechat_channeldto),'iBizBusinessCentral-Im_livechat_channel-Save')")
    @ApiOperation(value = "保存即时聊天", tags = {"即时聊天" },  notes = "保存即时聊天")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channels/save")
    public ResponseEntity<Boolean> save(@RequestBody Im_livechat_channelDTO im_livechat_channeldto) {
        return ResponseEntity.status(HttpStatus.OK).body(im_livechat_channelService.save(im_livechat_channelMapping.toDomain(im_livechat_channeldto)));
    }

    @PreAuthorize("hasPermission(this.im_livechat_channelMapping.toDomain(#im_livechat_channeldtos),'iBizBusinessCentral-Im_livechat_channel-Save')")
    @ApiOperation(value = "批量保存即时聊天", tags = {"即时聊天" },  notes = "批量保存即时聊天")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Im_livechat_channelDTO> im_livechat_channeldtos) {
        im_livechat_channelService.saveBatch(im_livechat_channelMapping.toDomain(im_livechat_channeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_channel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Im_livechat_channel-Get')")
	@ApiOperation(value = "获取数据集", tags = {"即时聊天" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/im_livechat_channels/fetchdefault")
	public ResponseEntity<List<Im_livechat_channelDTO>> fetchDefault(Im_livechat_channelSearchContext context) {
        Page<Im_livechat_channel> domains = im_livechat_channelService.searchDefault(context) ;
        List<Im_livechat_channelDTO> list = im_livechat_channelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Im_livechat_channel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Im_livechat_channel-Get')")
	@ApiOperation(value = "查询数据集", tags = {"即时聊天" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/im_livechat_channels/searchdefault")
	public ResponseEntity<Page<Im_livechat_channelDTO>> searchDefault(@RequestBody Im_livechat_channelSearchContext context) {
        Page<Im_livechat_channel> domains = im_livechat_channelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(im_livechat_channelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

