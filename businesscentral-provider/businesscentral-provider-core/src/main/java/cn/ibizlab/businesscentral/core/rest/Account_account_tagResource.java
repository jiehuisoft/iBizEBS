package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_tag;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_account_tagService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_account_tagSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"账户标签" })
@RestController("Core-account_account_tag")
@RequestMapping("")
public class Account_account_tagResource {

    @Autowired
    public IAccount_account_tagService account_account_tagService;

    @Autowired
    @Lazy
    public Account_account_tagMapping account_account_tagMapping;

    @PreAuthorize("hasPermission(this.account_account_tagMapping.toDomain(#account_account_tagdto),'iBizBusinessCentral-Account_account_tag-Create')")
    @ApiOperation(value = "新建账户标签", tags = {"账户标签" },  notes = "新建账户标签")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_tags")
    public ResponseEntity<Account_account_tagDTO> create(@Validated @RequestBody Account_account_tagDTO account_account_tagdto) {
        Account_account_tag domain = account_account_tagMapping.toDomain(account_account_tagdto);
		account_account_tagService.create(domain);
        Account_account_tagDTO dto = account_account_tagMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_account_tagMapping.toDomain(#account_account_tagdtos),'iBizBusinessCentral-Account_account_tag-Create')")
    @ApiOperation(value = "批量新建账户标签", tags = {"账户标签" },  notes = "批量新建账户标签")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_tags/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_account_tagDTO> account_account_tagdtos) {
        account_account_tagService.createBatch(account_account_tagMapping.toDomain(account_account_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_account_tag" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_account_tagService.get(#account_account_tag_id),'iBizBusinessCentral-Account_account_tag-Update')")
    @ApiOperation(value = "更新账户标签", tags = {"账户标签" },  notes = "更新账户标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_account_tags/{account_account_tag_id}")
    public ResponseEntity<Account_account_tagDTO> update(@PathVariable("account_account_tag_id") Long account_account_tag_id, @RequestBody Account_account_tagDTO account_account_tagdto) {
		Account_account_tag domain  = account_account_tagMapping.toDomain(account_account_tagdto);
        domain .setId(account_account_tag_id);
		account_account_tagService.update(domain );
		Account_account_tagDTO dto = account_account_tagMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_account_tagService.getAccountAccountTagByEntities(this.account_account_tagMapping.toDomain(#account_account_tagdtos)),'iBizBusinessCentral-Account_account_tag-Update')")
    @ApiOperation(value = "批量更新账户标签", tags = {"账户标签" },  notes = "批量更新账户标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_account_tags/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_account_tagDTO> account_account_tagdtos) {
        account_account_tagService.updateBatch(account_account_tagMapping.toDomain(account_account_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_account_tagService.get(#account_account_tag_id),'iBizBusinessCentral-Account_account_tag-Remove')")
    @ApiOperation(value = "删除账户标签", tags = {"账户标签" },  notes = "删除账户标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_account_tags/{account_account_tag_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_account_tag_id") Long account_account_tag_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_account_tagService.remove(account_account_tag_id));
    }

    @PreAuthorize("hasPermission(this.account_account_tagService.getAccountAccountTagByIds(#ids),'iBizBusinessCentral-Account_account_tag-Remove')")
    @ApiOperation(value = "批量删除账户标签", tags = {"账户标签" },  notes = "批量删除账户标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_account_tags/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_account_tagService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_account_tagMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_account_tag-Get')")
    @ApiOperation(value = "获取账户标签", tags = {"账户标签" },  notes = "获取账户标签")
	@RequestMapping(method = RequestMethod.GET, value = "/account_account_tags/{account_account_tag_id}")
    public ResponseEntity<Account_account_tagDTO> get(@PathVariable("account_account_tag_id") Long account_account_tag_id) {
        Account_account_tag domain = account_account_tagService.get(account_account_tag_id);
        Account_account_tagDTO dto = account_account_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取账户标签草稿", tags = {"账户标签" },  notes = "获取账户标签草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_account_tags/getdraft")
    public ResponseEntity<Account_account_tagDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_account_tagMapping.toDto(account_account_tagService.getDraft(new Account_account_tag())));
    }

    @ApiOperation(value = "检查账户标签", tags = {"账户标签" },  notes = "检查账户标签")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_tags/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_account_tagDTO account_account_tagdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_account_tagService.checkKey(account_account_tagMapping.toDomain(account_account_tagdto)));
    }

    @PreAuthorize("hasPermission(this.account_account_tagMapping.toDomain(#account_account_tagdto),'iBizBusinessCentral-Account_account_tag-Save')")
    @ApiOperation(value = "保存账户标签", tags = {"账户标签" },  notes = "保存账户标签")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_tags/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_account_tagDTO account_account_tagdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_account_tagService.save(account_account_tagMapping.toDomain(account_account_tagdto)));
    }

    @PreAuthorize("hasPermission(this.account_account_tagMapping.toDomain(#account_account_tagdtos),'iBizBusinessCentral-Account_account_tag-Save')")
    @ApiOperation(value = "批量保存账户标签", tags = {"账户标签" },  notes = "批量保存账户标签")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_tags/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_account_tagDTO> account_account_tagdtos) {
        account_account_tagService.saveBatch(account_account_tagMapping.toDomain(account_account_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_account_tag-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_account_tag-Get')")
	@ApiOperation(value = "获取数据集", tags = {"账户标签" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_account_tags/fetchdefault")
	public ResponseEntity<List<Account_account_tagDTO>> fetchDefault(Account_account_tagSearchContext context) {
        Page<Account_account_tag> domains = account_account_tagService.searchDefault(context) ;
        List<Account_account_tagDTO> list = account_account_tagMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_account_tag-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_account_tag-Get')")
	@ApiOperation(value = "查询数据集", tags = {"账户标签" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_account_tags/searchdefault")
	public ResponseEntity<Page<Account_account_tagDTO>> searchDefault(@RequestBody Account_account_tagSearchContext context) {
        Page<Account_account_tag> domains = account_account_tagService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_account_tagMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

