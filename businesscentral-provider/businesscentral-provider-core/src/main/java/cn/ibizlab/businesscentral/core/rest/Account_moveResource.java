package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_moveService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_moveSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"凭证录入" })
@RestController("Core-account_move")
@RequestMapping("")
public class Account_moveResource {

    @Autowired
    public IAccount_moveService account_moveService;

    @Autowired
    @Lazy
    public Account_moveMapping account_moveMapping;

    @PreAuthorize("hasPermission(this.account_moveMapping.toDomain(#account_movedto),'iBizBusinessCentral-Account_move-Create')")
    @ApiOperation(value = "新建凭证录入", tags = {"凭证录入" },  notes = "新建凭证录入")
	@RequestMapping(method = RequestMethod.POST, value = "/account_moves")
    public ResponseEntity<Account_moveDTO> create(@Validated @RequestBody Account_moveDTO account_movedto) {
        Account_move domain = account_moveMapping.toDomain(account_movedto);
		account_moveService.create(domain);
        Account_moveDTO dto = account_moveMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_moveMapping.toDomain(#account_movedtos),'iBizBusinessCentral-Account_move-Create')")
    @ApiOperation(value = "批量新建凭证录入", tags = {"凭证录入" },  notes = "批量新建凭证录入")
	@RequestMapping(method = RequestMethod.POST, value = "/account_moves/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_moveDTO> account_movedtos) {
        account_moveService.createBatch(account_moveMapping.toDomain(account_movedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_move" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_moveService.get(#account_move_id),'iBizBusinessCentral-Account_move-Update')")
    @ApiOperation(value = "更新凭证录入", tags = {"凭证录入" },  notes = "更新凭证录入")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_moves/{account_move_id}")
    public ResponseEntity<Account_moveDTO> update(@PathVariable("account_move_id") Long account_move_id, @RequestBody Account_moveDTO account_movedto) {
		Account_move domain  = account_moveMapping.toDomain(account_movedto);
        domain .setId(account_move_id);
		account_moveService.update(domain );
		Account_moveDTO dto = account_moveMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_moveService.getAccountMoveByEntities(this.account_moveMapping.toDomain(#account_movedtos)),'iBizBusinessCentral-Account_move-Update')")
    @ApiOperation(value = "批量更新凭证录入", tags = {"凭证录入" },  notes = "批量更新凭证录入")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_moves/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_moveDTO> account_movedtos) {
        account_moveService.updateBatch(account_moveMapping.toDomain(account_movedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_moveService.get(#account_move_id),'iBizBusinessCentral-Account_move-Remove')")
    @ApiOperation(value = "删除凭证录入", tags = {"凭证录入" },  notes = "删除凭证录入")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_moves/{account_move_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_move_id") Long account_move_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_moveService.remove(account_move_id));
    }

    @PreAuthorize("hasPermission(this.account_moveService.getAccountMoveByIds(#ids),'iBizBusinessCentral-Account_move-Remove')")
    @ApiOperation(value = "批量删除凭证录入", tags = {"凭证录入" },  notes = "批量删除凭证录入")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_moves/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_moveService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_moveMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_move-Get')")
    @ApiOperation(value = "获取凭证录入", tags = {"凭证录入" },  notes = "获取凭证录入")
	@RequestMapping(method = RequestMethod.GET, value = "/account_moves/{account_move_id}")
    public ResponseEntity<Account_moveDTO> get(@PathVariable("account_move_id") Long account_move_id) {
        Account_move domain = account_moveService.get(account_move_id);
        Account_moveDTO dto = account_moveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取凭证录入草稿", tags = {"凭证录入" },  notes = "获取凭证录入草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_moves/getdraft")
    public ResponseEntity<Account_moveDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_moveMapping.toDto(account_moveService.getDraft(new Account_move())));
    }

    @ApiOperation(value = "检查凭证录入", tags = {"凭证录入" },  notes = "检查凭证录入")
	@RequestMapping(method = RequestMethod.POST, value = "/account_moves/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_moveDTO account_movedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_moveService.checkKey(account_moveMapping.toDomain(account_movedto)));
    }

    @PreAuthorize("hasPermission(this.account_moveMapping.toDomain(#account_movedto),'iBizBusinessCentral-Account_move-Save')")
    @ApiOperation(value = "保存凭证录入", tags = {"凭证录入" },  notes = "保存凭证录入")
	@RequestMapping(method = RequestMethod.POST, value = "/account_moves/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_moveDTO account_movedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_moveService.save(account_moveMapping.toDomain(account_movedto)));
    }

    @PreAuthorize("hasPermission(this.account_moveMapping.toDomain(#account_movedtos),'iBizBusinessCentral-Account_move-Save')")
    @ApiOperation(value = "批量保存凭证录入", tags = {"凭证录入" },  notes = "批量保存凭证录入")
	@RequestMapping(method = RequestMethod.POST, value = "/account_moves/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_moveDTO> account_movedtos) {
        account_moveService.saveBatch(account_moveMapping.toDomain(account_movedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_move-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_move-Get')")
	@ApiOperation(value = "获取数据集", tags = {"凭证录入" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_moves/fetchdefault")
	public ResponseEntity<List<Account_moveDTO>> fetchDefault(Account_moveSearchContext context) {
        Page<Account_move> domains = account_moveService.searchDefault(context) ;
        List<Account_moveDTO> list = account_moveMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_move-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_move-Get')")
	@ApiOperation(value = "查询数据集", tags = {"凭证录入" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_moves/searchdefault")
	public ResponseEntity<Page<Account_moveDTO>> searchDefault(@RequestBody Account_moveSearchContext context) {
        Page<Account_move> domains = account_moveService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_moveMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

