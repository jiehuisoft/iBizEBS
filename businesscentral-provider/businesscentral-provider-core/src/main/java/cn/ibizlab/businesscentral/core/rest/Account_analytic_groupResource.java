package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_group;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_groupService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_groupSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"分析类别" })
@RestController("Core-account_analytic_group")
@RequestMapping("")
public class Account_analytic_groupResource {

    @Autowired
    public IAccount_analytic_groupService account_analytic_groupService;

    @Autowired
    @Lazy
    public Account_analytic_groupMapping account_analytic_groupMapping;

    @PreAuthorize("hasPermission(this.account_analytic_groupMapping.toDomain(#account_analytic_groupdto),'iBizBusinessCentral-Account_analytic_group-Create')")
    @ApiOperation(value = "新建分析类别", tags = {"分析类别" },  notes = "新建分析类别")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_groups")
    public ResponseEntity<Account_analytic_groupDTO> create(@Validated @RequestBody Account_analytic_groupDTO account_analytic_groupdto) {
        Account_analytic_group domain = account_analytic_groupMapping.toDomain(account_analytic_groupdto);
		account_analytic_groupService.create(domain);
        Account_analytic_groupDTO dto = account_analytic_groupMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_analytic_groupMapping.toDomain(#account_analytic_groupdtos),'iBizBusinessCentral-Account_analytic_group-Create')")
    @ApiOperation(value = "批量新建分析类别", tags = {"分析类别" },  notes = "批量新建分析类别")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_groups/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_analytic_groupDTO> account_analytic_groupdtos) {
        account_analytic_groupService.createBatch(account_analytic_groupMapping.toDomain(account_analytic_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_analytic_group" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_analytic_groupService.get(#account_analytic_group_id),'iBizBusinessCentral-Account_analytic_group-Update')")
    @ApiOperation(value = "更新分析类别", tags = {"分析类别" },  notes = "更新分析类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_groups/{account_analytic_group_id}")
    public ResponseEntity<Account_analytic_groupDTO> update(@PathVariable("account_analytic_group_id") Long account_analytic_group_id, @RequestBody Account_analytic_groupDTO account_analytic_groupdto) {
		Account_analytic_group domain  = account_analytic_groupMapping.toDomain(account_analytic_groupdto);
        domain .setId(account_analytic_group_id);
		account_analytic_groupService.update(domain );
		Account_analytic_groupDTO dto = account_analytic_groupMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_analytic_groupService.getAccountAnalyticGroupByEntities(this.account_analytic_groupMapping.toDomain(#account_analytic_groupdtos)),'iBizBusinessCentral-Account_analytic_group-Update')")
    @ApiOperation(value = "批量更新分析类别", tags = {"分析类别" },  notes = "批量更新分析类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_groups/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_analytic_groupDTO> account_analytic_groupdtos) {
        account_analytic_groupService.updateBatch(account_analytic_groupMapping.toDomain(account_analytic_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_analytic_groupService.get(#account_analytic_group_id),'iBizBusinessCentral-Account_analytic_group-Remove')")
    @ApiOperation(value = "删除分析类别", tags = {"分析类别" },  notes = "删除分析类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_groups/{account_analytic_group_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_analytic_group_id") Long account_analytic_group_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_analytic_groupService.remove(account_analytic_group_id));
    }

    @PreAuthorize("hasPermission(this.account_analytic_groupService.getAccountAnalyticGroupByIds(#ids),'iBizBusinessCentral-Account_analytic_group-Remove')")
    @ApiOperation(value = "批量删除分析类别", tags = {"分析类别" },  notes = "批量删除分析类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_groups/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_analytic_groupService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_analytic_groupMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_analytic_group-Get')")
    @ApiOperation(value = "获取分析类别", tags = {"分析类别" },  notes = "获取分析类别")
	@RequestMapping(method = RequestMethod.GET, value = "/account_analytic_groups/{account_analytic_group_id}")
    public ResponseEntity<Account_analytic_groupDTO> get(@PathVariable("account_analytic_group_id") Long account_analytic_group_id) {
        Account_analytic_group domain = account_analytic_groupService.get(account_analytic_group_id);
        Account_analytic_groupDTO dto = account_analytic_groupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取分析类别草稿", tags = {"分析类别" },  notes = "获取分析类别草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_analytic_groups/getdraft")
    public ResponseEntity<Account_analytic_groupDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_analytic_groupMapping.toDto(account_analytic_groupService.getDraft(new Account_analytic_group())));
    }

    @ApiOperation(value = "检查分析类别", tags = {"分析类别" },  notes = "检查分析类别")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_groups/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_analytic_groupDTO account_analytic_groupdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_analytic_groupService.checkKey(account_analytic_groupMapping.toDomain(account_analytic_groupdto)));
    }

    @PreAuthorize("hasPermission(this.account_analytic_groupMapping.toDomain(#account_analytic_groupdto),'iBizBusinessCentral-Account_analytic_group-Save')")
    @ApiOperation(value = "保存分析类别", tags = {"分析类别" },  notes = "保存分析类别")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_groups/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_analytic_groupDTO account_analytic_groupdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_analytic_groupService.save(account_analytic_groupMapping.toDomain(account_analytic_groupdto)));
    }

    @PreAuthorize("hasPermission(this.account_analytic_groupMapping.toDomain(#account_analytic_groupdtos),'iBizBusinessCentral-Account_analytic_group-Save')")
    @ApiOperation(value = "批量保存分析类别", tags = {"分析类别" },  notes = "批量保存分析类别")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_groups/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_analytic_groupDTO> account_analytic_groupdtos) {
        account_analytic_groupService.saveBatch(account_analytic_groupMapping.toDomain(account_analytic_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_analytic_group-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_analytic_group-Get')")
	@ApiOperation(value = "获取数据集", tags = {"分析类别" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_analytic_groups/fetchdefault")
	public ResponseEntity<List<Account_analytic_groupDTO>> fetchDefault(Account_analytic_groupSearchContext context) {
        Page<Account_analytic_group> domains = account_analytic_groupService.searchDefault(context) ;
        List<Account_analytic_groupDTO> list = account_analytic_groupMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_analytic_group-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_analytic_group-Get')")
	@ApiOperation(value = "查询数据集", tags = {"分析类别" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_analytic_groups/searchdefault")
	public ResponseEntity<Page<Account_analytic_groupDTO>> searchDefault(@RequestBody Account_analytic_groupSearchContext context) {
        Page<Account_analytic_group> domains = account_analytic_groupService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_analytic_groupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

