package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_supplierinfo;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_supplierinfoService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_supplierinfoSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"供应商价格表" })
@RestController("Core-product_supplierinfo")
@RequestMapping("")
public class Product_supplierinfoResource {

    @Autowired
    public IProduct_supplierinfoService product_supplierinfoService;

    @Autowired
    @Lazy
    public Product_supplierinfoMapping product_supplierinfoMapping;

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodto),'iBizBusinessCentral-Product_supplierinfo-Create')")
    @ApiOperation(value = "新建供应商价格表", tags = {"供应商价格表" },  notes = "新建供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_supplierinfos")
    public ResponseEntity<Product_supplierinfoDTO> create(@Validated @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
		product_supplierinfoService.create(domain);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos),'iBizBusinessCentral-Product_supplierinfo-Create')")
    @ApiOperation(value = "批量新建供应商价格表", tags = {"供应商价格表" },  notes = "批量新建供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_supplierinfos/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        product_supplierinfoService.createBatch(product_supplierinfoMapping.toDomain(product_supplierinfodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_supplierinfo" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_supplierinfoService.get(#product_supplierinfo_id),'iBizBusinessCentral-Product_supplierinfo-Update')")
    @ApiOperation(value = "更新供应商价格表", tags = {"供应商价格表" },  notes = "更新供应商价格表")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Product_supplierinfoDTO> update(@PathVariable("product_supplierinfo_id") Long product_supplierinfo_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
		Product_supplierinfo domain  = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain .setId(product_supplierinfo_id);
		product_supplierinfoService.update(domain );
		Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.getProductSupplierinfoByEntities(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos)),'iBizBusinessCentral-Product_supplierinfo-Update')")
    @ApiOperation(value = "批量更新供应商价格表", tags = {"供应商价格表" },  notes = "批量更新供应商价格表")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_supplierinfos/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        product_supplierinfoService.updateBatch(product_supplierinfoMapping.toDomain(product_supplierinfodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.get(#product_supplierinfo_id),'iBizBusinessCentral-Product_supplierinfo-Remove')")
    @ApiOperation(value = "删除供应商价格表", tags = {"供应商价格表" },  notes = "删除供应商价格表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_supplierinfo_id") Long product_supplierinfo_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.remove(product_supplierinfo_id));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.getProductSupplierinfoByIds(#ids),'iBizBusinessCentral-Product_supplierinfo-Remove')")
    @ApiOperation(value = "批量删除供应商价格表", tags = {"供应商价格表" },  notes = "批量删除供应商价格表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_supplierinfos/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_supplierinfoService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_supplierinfo-Get')")
    @ApiOperation(value = "获取供应商价格表", tags = {"供应商价格表" },  notes = "获取供应商价格表")
	@RequestMapping(method = RequestMethod.GET, value = "/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Product_supplierinfoDTO> get(@PathVariable("product_supplierinfo_id") Long product_supplierinfo_id) {
        Product_supplierinfo domain = product_supplierinfoService.get(product_supplierinfo_id);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取供应商价格表草稿", tags = {"供应商价格表" },  notes = "获取供应商价格表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_supplierinfos/getdraft")
    public ResponseEntity<Product_supplierinfoDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoMapping.toDto(product_supplierinfoService.getDraft(new Product_supplierinfo())));
    }

    @ApiOperation(value = "检查供应商价格表", tags = {"供应商价格表" },  notes = "检查供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_supplierinfos/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.checkKey(product_supplierinfoMapping.toDomain(product_supplierinfodto)));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodto),'iBizBusinessCentral-Product_supplierinfo-Save')")
    @ApiOperation(value = "保存供应商价格表", tags = {"供应商价格表" },  notes = "保存供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_supplierinfos/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.save(product_supplierinfoMapping.toDomain(product_supplierinfodto)));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos),'iBizBusinessCentral-Product_supplierinfo-Save')")
    @ApiOperation(value = "批量保存供应商价格表", tags = {"供应商价格表" },  notes = "批量保存供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_supplierinfos/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        product_supplierinfoService.saveBatch(product_supplierinfoMapping.toDomain(product_supplierinfodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_supplierinfo-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_supplierinfo-Get')")
	@ApiOperation(value = "获取数据集", tags = {"供应商价格表" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_supplierinfos/fetchdefault")
	public ResponseEntity<List<Product_supplierinfoDTO>> fetchDefault(Product_supplierinfoSearchContext context) {
        Page<Product_supplierinfo> domains = product_supplierinfoService.searchDefault(context) ;
        List<Product_supplierinfoDTO> list = product_supplierinfoMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_supplierinfo-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_supplierinfo-Get')")
	@ApiOperation(value = "查询数据集", tags = {"供应商价格表" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_supplierinfos/searchdefault")
	public ResponseEntity<Page<Product_supplierinfoDTO>> searchDefault(@RequestBody Product_supplierinfoSearchContext context) {
        Page<Product_supplierinfo> domains = product_supplierinfoService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_supplierinfoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodto),'iBizBusinessCentral-Product_supplierinfo-Create')")
    @ApiOperation(value = "根据供应商建立供应商价格表", tags = {"供应商价格表" },  notes = "根据供应商建立供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/product_supplierinfos")
    public ResponseEntity<Product_supplierinfoDTO> createByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain.setName(res_supplier_id);
		product_supplierinfoService.create(domain);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos),'iBizBusinessCentral-Product_supplierinfo-Create')")
    @ApiOperation(value = "根据供应商批量建立供应商价格表", tags = {"供应商价格表" },  notes = "根据供应商批量建立供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/product_supplierinfos/batch")
    public ResponseEntity<Boolean> createBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        List<Product_supplierinfo> domainlist=product_supplierinfoMapping.toDomain(product_supplierinfodtos);
        for(Product_supplierinfo domain:domainlist){
            domain.setName(res_supplier_id);
        }
        product_supplierinfoService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_supplierinfo" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_supplierinfoService.get(#product_supplierinfo_id),'iBizBusinessCentral-Product_supplierinfo-Update')")
    @ApiOperation(value = "根据供应商更新供应商价格表", tags = {"供应商价格表" },  notes = "根据供应商更新供应商价格表")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Product_supplierinfoDTO> updateByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("product_supplierinfo_id") Long product_supplierinfo_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain.setName(res_supplier_id);
        domain.setId(product_supplierinfo_id);
		product_supplierinfoService.update(domain);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.getProductSupplierinfoByEntities(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos)),'iBizBusinessCentral-Product_supplierinfo-Update')")
    @ApiOperation(value = "根据供应商批量更新供应商价格表", tags = {"供应商价格表" },  notes = "根据供应商批量更新供应商价格表")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/product_supplierinfos/batch")
    public ResponseEntity<Boolean> updateBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        List<Product_supplierinfo> domainlist=product_supplierinfoMapping.toDomain(product_supplierinfodtos);
        for(Product_supplierinfo domain:domainlist){
            domain.setName(res_supplier_id);
        }
        product_supplierinfoService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.get(#product_supplierinfo_id),'iBizBusinessCentral-Product_supplierinfo-Remove')")
    @ApiOperation(value = "根据供应商删除供应商价格表", tags = {"供应商价格表" },  notes = "根据供应商删除供应商价格表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Boolean> removeByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("product_supplierinfo_id") Long product_supplierinfo_id) {
		return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.remove(product_supplierinfo_id));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.getProductSupplierinfoByIds(#ids),'iBizBusinessCentral-Product_supplierinfo-Remove')")
    @ApiOperation(value = "根据供应商批量删除供应商价格表", tags = {"供应商价格表" },  notes = "根据供应商批量删除供应商价格表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/product_supplierinfos/batch")
    public ResponseEntity<Boolean> removeBatchByRes_supplier(@RequestBody List<Long> ids) {
        product_supplierinfoService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_supplierinfo-Get')")
    @ApiOperation(value = "根据供应商获取供应商价格表", tags = {"供应商价格表" },  notes = "根据供应商获取供应商价格表")
	@RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Product_supplierinfoDTO> getByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("product_supplierinfo_id") Long product_supplierinfo_id) {
        Product_supplierinfo domain = product_supplierinfoService.get(product_supplierinfo_id);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据供应商获取供应商价格表草稿", tags = {"供应商价格表" },  notes = "根据供应商获取供应商价格表草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/product_supplierinfos/getdraft")
    public ResponseEntity<Product_supplierinfoDTO> getDraftByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id) {
        Product_supplierinfo domain = new Product_supplierinfo();
        domain.setName(res_supplier_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoMapping.toDto(product_supplierinfoService.getDraft(domain)));
    }

    @ApiOperation(value = "根据供应商检查供应商价格表", tags = {"供应商价格表" },  notes = "根据供应商检查供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/product_supplierinfos/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.checkKey(product_supplierinfoMapping.toDomain(product_supplierinfodto)));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodto),'iBizBusinessCentral-Product_supplierinfo-Save')")
    @ApiOperation(value = "根据供应商保存供应商价格表", tags = {"供应商价格表" },  notes = "根据供应商保存供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/product_supplierinfos/save")
    public ResponseEntity<Boolean> saveByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain.setName(res_supplier_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.save(domain));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos),'iBizBusinessCentral-Product_supplierinfo-Save')")
    @ApiOperation(value = "根据供应商批量保存供应商价格表", tags = {"供应商价格表" },  notes = "根据供应商批量保存供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/product_supplierinfos/savebatch")
    public ResponseEntity<Boolean> saveBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        List<Product_supplierinfo> domainlist=product_supplierinfoMapping.toDomain(product_supplierinfodtos);
        for(Product_supplierinfo domain:domainlist){
             domain.setName(res_supplier_id);
        }
        product_supplierinfoService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_supplierinfo-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_supplierinfo-Get')")
	@ApiOperation(value = "根据供应商获取数据集", tags = {"供应商价格表" } ,notes = "根据供应商获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/product_supplierinfos/fetchdefault")
	public ResponseEntity<List<Product_supplierinfoDTO>> fetchProduct_supplierinfoDefaultByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id,Product_supplierinfoSearchContext context) {
        context.setN_name_eq(res_supplier_id);
        Page<Product_supplierinfo> domains = product_supplierinfoService.searchDefault(context) ;
        List<Product_supplierinfoDTO> list = product_supplierinfoMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_supplierinfo-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_supplierinfo-Get')")
	@ApiOperation(value = "根据供应商查询数据集", tags = {"供应商价格表" } ,notes = "根据供应商查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/product_supplierinfos/searchdefault")
	public ResponseEntity<Page<Product_supplierinfoDTO>> searchProduct_supplierinfoDefaultByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Product_supplierinfoSearchContext context) {
        context.setN_name_eq(res_supplier_id);
        Page<Product_supplierinfo> domains = product_supplierinfoService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_supplierinfoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodto),'iBizBusinessCentral-Product_supplierinfo-Create')")
    @ApiOperation(value = "根据产品建立供应商价格表", tags = {"供应商价格表" },  notes = "根据产品建立供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/product_supplierinfos")
    public ResponseEntity<Product_supplierinfoDTO> createByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain.setProductId(product_product_id);
		product_supplierinfoService.create(domain);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos),'iBizBusinessCentral-Product_supplierinfo-Create')")
    @ApiOperation(value = "根据产品批量建立供应商价格表", tags = {"供应商价格表" },  notes = "根据产品批量建立供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/product_supplierinfos/batch")
    public ResponseEntity<Boolean> createBatchByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        List<Product_supplierinfo> domainlist=product_supplierinfoMapping.toDomain(product_supplierinfodtos);
        for(Product_supplierinfo domain:domainlist){
            domain.setProductId(product_product_id);
        }
        product_supplierinfoService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_supplierinfo" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_supplierinfoService.get(#product_supplierinfo_id),'iBizBusinessCentral-Product_supplierinfo-Update')")
    @ApiOperation(value = "根据产品更新供应商价格表", tags = {"供应商价格表" },  notes = "根据产品更新供应商价格表")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_products/{product_product_id}/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Product_supplierinfoDTO> updateByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("product_supplierinfo_id") Long product_supplierinfo_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain.setProductId(product_product_id);
        domain.setId(product_supplierinfo_id);
		product_supplierinfoService.update(domain);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.getProductSupplierinfoByEntities(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos)),'iBizBusinessCentral-Product_supplierinfo-Update')")
    @ApiOperation(value = "根据产品批量更新供应商价格表", tags = {"供应商价格表" },  notes = "根据产品批量更新供应商价格表")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_products/{product_product_id}/product_supplierinfos/batch")
    public ResponseEntity<Boolean> updateBatchByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        List<Product_supplierinfo> domainlist=product_supplierinfoMapping.toDomain(product_supplierinfodtos);
        for(Product_supplierinfo domain:domainlist){
            domain.setProductId(product_product_id);
        }
        product_supplierinfoService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.get(#product_supplierinfo_id),'iBizBusinessCentral-Product_supplierinfo-Remove')")
    @ApiOperation(value = "根据产品删除供应商价格表", tags = {"供应商价格表" },  notes = "根据产品删除供应商价格表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_products/{product_product_id}/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Boolean> removeByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("product_supplierinfo_id") Long product_supplierinfo_id) {
		return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.remove(product_supplierinfo_id));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.getProductSupplierinfoByIds(#ids),'iBizBusinessCentral-Product_supplierinfo-Remove')")
    @ApiOperation(value = "根据产品批量删除供应商价格表", tags = {"供应商价格表" },  notes = "根据产品批量删除供应商价格表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_products/{product_product_id}/product_supplierinfos/batch")
    public ResponseEntity<Boolean> removeBatchByProduct_product(@RequestBody List<Long> ids) {
        product_supplierinfoService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_supplierinfo-Get')")
    @ApiOperation(value = "根据产品获取供应商价格表", tags = {"供应商价格表" },  notes = "根据产品获取供应商价格表")
	@RequestMapping(method = RequestMethod.GET, value = "/product_products/{product_product_id}/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Product_supplierinfoDTO> getByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("product_supplierinfo_id") Long product_supplierinfo_id) {
        Product_supplierinfo domain = product_supplierinfoService.get(product_supplierinfo_id);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据产品获取供应商价格表草稿", tags = {"供应商价格表" },  notes = "根据产品获取供应商价格表草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/product_products/{product_product_id}/product_supplierinfos/getdraft")
    public ResponseEntity<Product_supplierinfoDTO> getDraftByProduct_product(@PathVariable("product_product_id") Long product_product_id) {
        Product_supplierinfo domain = new Product_supplierinfo();
        domain.setProductId(product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoMapping.toDto(product_supplierinfoService.getDraft(domain)));
    }

    @ApiOperation(value = "根据产品检查供应商价格表", tags = {"供应商价格表" },  notes = "根据产品检查供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/product_supplierinfos/checkkey")
    public ResponseEntity<Boolean> checkKeyByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.checkKey(product_supplierinfoMapping.toDomain(product_supplierinfodto)));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodto),'iBizBusinessCentral-Product_supplierinfo-Save')")
    @ApiOperation(value = "根据产品保存供应商价格表", tags = {"供应商价格表" },  notes = "根据产品保存供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/product_supplierinfos/save")
    public ResponseEntity<Boolean> saveByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain.setProductId(product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.save(domain));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos),'iBizBusinessCentral-Product_supplierinfo-Save')")
    @ApiOperation(value = "根据产品批量保存供应商价格表", tags = {"供应商价格表" },  notes = "根据产品批量保存供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/product_supplierinfos/savebatch")
    public ResponseEntity<Boolean> saveBatchByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        List<Product_supplierinfo> domainlist=product_supplierinfoMapping.toDomain(product_supplierinfodtos);
        for(Product_supplierinfo domain:domainlist){
             domain.setProductId(product_product_id);
        }
        product_supplierinfoService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_supplierinfo-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_supplierinfo-Get')")
	@ApiOperation(value = "根据产品获取数据集", tags = {"供应商价格表" } ,notes = "根据产品获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_products/{product_product_id}/product_supplierinfos/fetchdefault")
	public ResponseEntity<List<Product_supplierinfoDTO>> fetchProduct_supplierinfoDefaultByProduct_product(@PathVariable("product_product_id") Long product_product_id,Product_supplierinfoSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<Product_supplierinfo> domains = product_supplierinfoService.searchDefault(context) ;
        List<Product_supplierinfoDTO> list = product_supplierinfoMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_supplierinfo-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_supplierinfo-Get')")
	@ApiOperation(value = "根据产品查询数据集", tags = {"供应商价格表" } ,notes = "根据产品查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_products/{product_product_id}/product_supplierinfos/searchdefault")
	public ResponseEntity<Page<Product_supplierinfoDTO>> searchProduct_supplierinfoDefaultByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody Product_supplierinfoSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<Product_supplierinfo> domains = product_supplierinfoService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_supplierinfoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodto),'iBizBusinessCentral-Product_supplierinfo-Create')")
    @ApiOperation(value = "根据产品模板建立供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板建立供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_supplierinfos")
    public ResponseEntity<Product_supplierinfoDTO> createByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain.setProductTmplId(product_template_id);
		product_supplierinfoService.create(domain);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos),'iBizBusinessCentral-Product_supplierinfo-Create')")
    @ApiOperation(value = "根据产品模板批量建立供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板批量建立供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_supplierinfos/batch")
    public ResponseEntity<Boolean> createBatchByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        List<Product_supplierinfo> domainlist=product_supplierinfoMapping.toDomain(product_supplierinfodtos);
        for(Product_supplierinfo domain:domainlist){
            domain.setProductTmplId(product_template_id);
        }
        product_supplierinfoService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_supplierinfo" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_supplierinfoService.get(#product_supplierinfo_id),'iBizBusinessCentral-Product_supplierinfo-Update')")
    @ApiOperation(value = "根据产品模板更新供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板更新供应商价格表")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Product_supplierinfoDTO> updateByProduct_template(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_supplierinfo_id") Long product_supplierinfo_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain.setProductTmplId(product_template_id);
        domain.setId(product_supplierinfo_id);
		product_supplierinfoService.update(domain);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.getProductSupplierinfoByEntities(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos)),'iBizBusinessCentral-Product_supplierinfo-Update')")
    @ApiOperation(value = "根据产品模板批量更新供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板批量更新供应商价格表")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}/product_supplierinfos/batch")
    public ResponseEntity<Boolean> updateBatchByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        List<Product_supplierinfo> domainlist=product_supplierinfoMapping.toDomain(product_supplierinfodtos);
        for(Product_supplierinfo domain:domainlist){
            domain.setProductTmplId(product_template_id);
        }
        product_supplierinfoService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.get(#product_supplierinfo_id),'iBizBusinessCentral-Product_supplierinfo-Remove')")
    @ApiOperation(value = "根据产品模板删除供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板删除供应商价格表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Boolean> removeByProduct_template(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_supplierinfo_id") Long product_supplierinfo_id) {
		return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.remove(product_supplierinfo_id));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.getProductSupplierinfoByIds(#ids),'iBizBusinessCentral-Product_supplierinfo-Remove')")
    @ApiOperation(value = "根据产品模板批量删除供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板批量删除供应商价格表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}/product_supplierinfos/batch")
    public ResponseEntity<Boolean> removeBatchByProduct_template(@RequestBody List<Long> ids) {
        product_supplierinfoService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_supplierinfo-Get')")
    @ApiOperation(value = "根据产品模板获取供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板获取供应商价格表")
	@RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Product_supplierinfoDTO> getByProduct_template(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_supplierinfo_id") Long product_supplierinfo_id) {
        Product_supplierinfo domain = product_supplierinfoService.get(product_supplierinfo_id);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据产品模板获取供应商价格表草稿", tags = {"供应商价格表" },  notes = "根据产品模板获取供应商价格表草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}/product_supplierinfos/getdraft")
    public ResponseEntity<Product_supplierinfoDTO> getDraftByProduct_template(@PathVariable("product_template_id") Long product_template_id) {
        Product_supplierinfo domain = new Product_supplierinfo();
        domain.setProductTmplId(product_template_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoMapping.toDto(product_supplierinfoService.getDraft(domain)));
    }

    @ApiOperation(value = "根据产品模板检查供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板检查供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_supplierinfos/checkkey")
    public ResponseEntity<Boolean> checkKeyByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.checkKey(product_supplierinfoMapping.toDomain(product_supplierinfodto)));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodto),'iBizBusinessCentral-Product_supplierinfo-Save')")
    @ApiOperation(value = "根据产品模板保存供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板保存供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_supplierinfos/save")
    public ResponseEntity<Boolean> saveByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain.setProductTmplId(product_template_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.save(domain));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos),'iBizBusinessCentral-Product_supplierinfo-Save')")
    @ApiOperation(value = "根据产品模板批量保存供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板批量保存供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_supplierinfos/savebatch")
    public ResponseEntity<Boolean> saveBatchByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        List<Product_supplierinfo> domainlist=product_supplierinfoMapping.toDomain(product_supplierinfodtos);
        for(Product_supplierinfo domain:domainlist){
             domain.setProductTmplId(product_template_id);
        }
        product_supplierinfoService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_supplierinfo-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_supplierinfo-Get')")
	@ApiOperation(value = "根据产品模板获取数据集", tags = {"供应商价格表" } ,notes = "根据产品模板获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_templates/{product_template_id}/product_supplierinfos/fetchdefault")
	public ResponseEntity<List<Product_supplierinfoDTO>> fetchProduct_supplierinfoDefaultByProduct_template(@PathVariable("product_template_id") Long product_template_id,Product_supplierinfoSearchContext context) {
        context.setN_product_tmpl_id_eq(product_template_id);
        Page<Product_supplierinfo> domains = product_supplierinfoService.searchDefault(context) ;
        List<Product_supplierinfoDTO> list = product_supplierinfoMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_supplierinfo-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_supplierinfo-Get')")
	@ApiOperation(value = "根据产品模板查询数据集", tags = {"供应商价格表" } ,notes = "根据产品模板查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_templates/{product_template_id}/product_supplierinfos/searchdefault")
	public ResponseEntity<Page<Product_supplierinfoDTO>> searchProduct_supplierinfoDefaultByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_supplierinfoSearchContext context) {
        context.setN_product_tmpl_id_eq(product_template_id);
        Page<Product_supplierinfo> domains = product_supplierinfoService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_supplierinfoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodto),'iBizBusinessCentral-Product_supplierinfo-Create')")
    @ApiOperation(value = "根据产品模板产品建立供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板产品建立供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/product_supplierinfos")
    public ResponseEntity<Product_supplierinfoDTO> createByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain.setProductId(product_product_id);
		product_supplierinfoService.create(domain);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos),'iBizBusinessCentral-Product_supplierinfo-Create')")
    @ApiOperation(value = "根据产品模板产品批量建立供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板产品批量建立供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/product_supplierinfos/batch")
    public ResponseEntity<Boolean> createBatchByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        List<Product_supplierinfo> domainlist=product_supplierinfoMapping.toDomain(product_supplierinfodtos);
        for(Product_supplierinfo domain:domainlist){
            domain.setProductId(product_product_id);
        }
        product_supplierinfoService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_supplierinfo" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_supplierinfoService.get(#product_supplierinfo_id),'iBizBusinessCentral-Product_supplierinfo-Update')")
    @ApiOperation(value = "根据产品模板产品更新供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板产品更新供应商价格表")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Product_supplierinfoDTO> updateByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("product_supplierinfo_id") Long product_supplierinfo_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain.setProductId(product_product_id);
        domain.setId(product_supplierinfo_id);
		product_supplierinfoService.update(domain);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.getProductSupplierinfoByEntities(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos)),'iBizBusinessCentral-Product_supplierinfo-Update')")
    @ApiOperation(value = "根据产品模板产品批量更新供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板产品批量更新供应商价格表")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/product_supplierinfos/batch")
    public ResponseEntity<Boolean> updateBatchByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        List<Product_supplierinfo> domainlist=product_supplierinfoMapping.toDomain(product_supplierinfodtos);
        for(Product_supplierinfo domain:domainlist){
            domain.setProductId(product_product_id);
        }
        product_supplierinfoService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.get(#product_supplierinfo_id),'iBizBusinessCentral-Product_supplierinfo-Remove')")
    @ApiOperation(value = "根据产品模板产品删除供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板产品删除供应商价格表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Boolean> removeByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("product_supplierinfo_id") Long product_supplierinfo_id) {
		return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.remove(product_supplierinfo_id));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoService.getProductSupplierinfoByIds(#ids),'iBizBusinessCentral-Product_supplierinfo-Remove')")
    @ApiOperation(value = "根据产品模板产品批量删除供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板产品批量删除供应商价格表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/product_supplierinfos/batch")
    public ResponseEntity<Boolean> removeBatchByProduct_templateProduct_product(@RequestBody List<Long> ids) {
        product_supplierinfoService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_supplierinfo-Get')")
    @ApiOperation(value = "根据产品模板产品获取供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板产品获取供应商价格表")
	@RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Product_supplierinfoDTO> getByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("product_supplierinfo_id") Long product_supplierinfo_id) {
        Product_supplierinfo domain = product_supplierinfoService.get(product_supplierinfo_id);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据产品模板产品获取供应商价格表草稿", tags = {"供应商价格表" },  notes = "根据产品模板产品获取供应商价格表草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/product_supplierinfos/getdraft")
    public ResponseEntity<Product_supplierinfoDTO> getDraftByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id) {
        Product_supplierinfo domain = new Product_supplierinfo();
        domain.setProductId(product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoMapping.toDto(product_supplierinfoService.getDraft(domain)));
    }

    @ApiOperation(value = "根据产品模板产品检查供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板产品检查供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/product_supplierinfos/checkkey")
    public ResponseEntity<Boolean> checkKeyByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.checkKey(product_supplierinfoMapping.toDomain(product_supplierinfodto)));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodto),'iBizBusinessCentral-Product_supplierinfo-Save')")
    @ApiOperation(value = "根据产品模板产品保存供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板产品保存供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/product_supplierinfos/save")
    public ResponseEntity<Boolean> saveByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain.setProductId(product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.save(domain));
    }

    @PreAuthorize("hasPermission(this.product_supplierinfoMapping.toDomain(#product_supplierinfodtos),'iBizBusinessCentral-Product_supplierinfo-Save')")
    @ApiOperation(value = "根据产品模板产品批量保存供应商价格表", tags = {"供应商价格表" },  notes = "根据产品模板产品批量保存供应商价格表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/product_supplierinfos/savebatch")
    public ResponseEntity<Boolean> saveBatchByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        List<Product_supplierinfo> domainlist=product_supplierinfoMapping.toDomain(product_supplierinfodtos);
        for(Product_supplierinfo domain:domainlist){
             domain.setProductId(product_product_id);
        }
        product_supplierinfoService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_supplierinfo-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_supplierinfo-Get')")
	@ApiOperation(value = "根据产品模板产品获取数据集", tags = {"供应商价格表" } ,notes = "根据产品模板产品获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_templates/{product_template_id}/product_products/{product_product_id}/product_supplierinfos/fetchdefault")
	public ResponseEntity<List<Product_supplierinfoDTO>> fetchProduct_supplierinfoDefaultByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id,Product_supplierinfoSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<Product_supplierinfo> domains = product_supplierinfoService.searchDefault(context) ;
        List<Product_supplierinfoDTO> list = product_supplierinfoMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_supplierinfo-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_supplierinfo-Get')")
	@ApiOperation(value = "根据产品模板产品查询数据集", tags = {"供应商价格表" } ,notes = "根据产品模板产品查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_templates/{product_template_id}/product_products/{product_product_id}/product_supplierinfos/searchdefault")
	public ResponseEntity<Page<Product_supplierinfoDTO>> searchProduct_supplierinfoDefaultByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Product_supplierinfoSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<Product_supplierinfo> domains = product_supplierinfoService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_supplierinfoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

