package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_immediate_transfer;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_immediate_transferService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_immediate_transferSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"立即调拨" })
@RestController("Core-stock_immediate_transfer")
@RequestMapping("")
public class Stock_immediate_transferResource {

    @Autowired
    public IStock_immediate_transferService stock_immediate_transferService;

    @Autowired
    @Lazy
    public Stock_immediate_transferMapping stock_immediate_transferMapping;

    @PreAuthorize("hasPermission(this.stock_immediate_transferMapping.toDomain(#stock_immediate_transferdto),'iBizBusinessCentral-Stock_immediate_transfer-Create')")
    @ApiOperation(value = "新建立即调拨", tags = {"立即调拨" },  notes = "新建立即调拨")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_immediate_transfers")
    public ResponseEntity<Stock_immediate_transferDTO> create(@Validated @RequestBody Stock_immediate_transferDTO stock_immediate_transferdto) {
        Stock_immediate_transfer domain = stock_immediate_transferMapping.toDomain(stock_immediate_transferdto);
		stock_immediate_transferService.create(domain);
        Stock_immediate_transferDTO dto = stock_immediate_transferMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_immediate_transferMapping.toDomain(#stock_immediate_transferdtos),'iBizBusinessCentral-Stock_immediate_transfer-Create')")
    @ApiOperation(value = "批量新建立即调拨", tags = {"立即调拨" },  notes = "批量新建立即调拨")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_immediate_transfers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_immediate_transferDTO> stock_immediate_transferdtos) {
        stock_immediate_transferService.createBatch(stock_immediate_transferMapping.toDomain(stock_immediate_transferdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_immediate_transfer" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_immediate_transferService.get(#stock_immediate_transfer_id),'iBizBusinessCentral-Stock_immediate_transfer-Update')")
    @ApiOperation(value = "更新立即调拨", tags = {"立即调拨" },  notes = "更新立即调拨")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_immediate_transfers/{stock_immediate_transfer_id}")
    public ResponseEntity<Stock_immediate_transferDTO> update(@PathVariable("stock_immediate_transfer_id") Long stock_immediate_transfer_id, @RequestBody Stock_immediate_transferDTO stock_immediate_transferdto) {
		Stock_immediate_transfer domain  = stock_immediate_transferMapping.toDomain(stock_immediate_transferdto);
        domain .setId(stock_immediate_transfer_id);
		stock_immediate_transferService.update(domain );
		Stock_immediate_transferDTO dto = stock_immediate_transferMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_immediate_transferService.getStockImmediateTransferByEntities(this.stock_immediate_transferMapping.toDomain(#stock_immediate_transferdtos)),'iBizBusinessCentral-Stock_immediate_transfer-Update')")
    @ApiOperation(value = "批量更新立即调拨", tags = {"立即调拨" },  notes = "批量更新立即调拨")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_immediate_transfers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_immediate_transferDTO> stock_immediate_transferdtos) {
        stock_immediate_transferService.updateBatch(stock_immediate_transferMapping.toDomain(stock_immediate_transferdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_immediate_transferService.get(#stock_immediate_transfer_id),'iBizBusinessCentral-Stock_immediate_transfer-Remove')")
    @ApiOperation(value = "删除立即调拨", tags = {"立即调拨" },  notes = "删除立即调拨")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_immediate_transfers/{stock_immediate_transfer_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_immediate_transfer_id") Long stock_immediate_transfer_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_immediate_transferService.remove(stock_immediate_transfer_id));
    }

    @PreAuthorize("hasPermission(this.stock_immediate_transferService.getStockImmediateTransferByIds(#ids),'iBizBusinessCentral-Stock_immediate_transfer-Remove')")
    @ApiOperation(value = "批量删除立即调拨", tags = {"立即调拨" },  notes = "批量删除立即调拨")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_immediate_transfers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_immediate_transferService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_immediate_transferMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_immediate_transfer-Get')")
    @ApiOperation(value = "获取立即调拨", tags = {"立即调拨" },  notes = "获取立即调拨")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_immediate_transfers/{stock_immediate_transfer_id}")
    public ResponseEntity<Stock_immediate_transferDTO> get(@PathVariable("stock_immediate_transfer_id") Long stock_immediate_transfer_id) {
        Stock_immediate_transfer domain = stock_immediate_transferService.get(stock_immediate_transfer_id);
        Stock_immediate_transferDTO dto = stock_immediate_transferMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取立即调拨草稿", tags = {"立即调拨" },  notes = "获取立即调拨草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_immediate_transfers/getdraft")
    public ResponseEntity<Stock_immediate_transferDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_immediate_transferMapping.toDto(stock_immediate_transferService.getDraft(new Stock_immediate_transfer())));
    }

    @ApiOperation(value = "检查立即调拨", tags = {"立即调拨" },  notes = "检查立即调拨")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_immediate_transfers/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_immediate_transferDTO stock_immediate_transferdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_immediate_transferService.checkKey(stock_immediate_transferMapping.toDomain(stock_immediate_transferdto)));
    }

    @PreAuthorize("hasPermission(this.stock_immediate_transferMapping.toDomain(#stock_immediate_transferdto),'iBizBusinessCentral-Stock_immediate_transfer-Save')")
    @ApiOperation(value = "保存立即调拨", tags = {"立即调拨" },  notes = "保存立即调拨")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_immediate_transfers/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_immediate_transferDTO stock_immediate_transferdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_immediate_transferService.save(stock_immediate_transferMapping.toDomain(stock_immediate_transferdto)));
    }

    @PreAuthorize("hasPermission(this.stock_immediate_transferMapping.toDomain(#stock_immediate_transferdtos),'iBizBusinessCentral-Stock_immediate_transfer-Save')")
    @ApiOperation(value = "批量保存立即调拨", tags = {"立即调拨" },  notes = "批量保存立即调拨")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_immediate_transfers/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_immediate_transferDTO> stock_immediate_transferdtos) {
        stock_immediate_transferService.saveBatch(stock_immediate_transferMapping.toDomain(stock_immediate_transferdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_immediate_transfer-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_immediate_transfer-Get')")
	@ApiOperation(value = "获取数据集", tags = {"立即调拨" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_immediate_transfers/fetchdefault")
	public ResponseEntity<List<Stock_immediate_transferDTO>> fetchDefault(Stock_immediate_transferSearchContext context) {
        Page<Stock_immediate_transfer> domains = stock_immediate_transferService.searchDefault(context) ;
        List<Stock_immediate_transferDTO> list = stock_immediate_transferMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_immediate_transfer-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_immediate_transfer-Get')")
	@ApiOperation(value = "查询数据集", tags = {"立即调拨" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_immediate_transfers/searchdefault")
	public ResponseEntity<Page<Stock_immediate_transferDTO>> searchDefault(@RequestBody Stock_immediate_transferSearchContext context) {
        Page<Stock_immediate_transfer> domains = stock_immediate_transferService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_immediate_transferMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

