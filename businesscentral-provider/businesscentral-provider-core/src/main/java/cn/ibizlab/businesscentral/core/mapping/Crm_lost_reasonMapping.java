package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lost_reason;
import cn.ibizlab.businesscentral.core.dto.Crm_lost_reasonDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreCrm_lost_reasonMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Crm_lost_reasonMapping extends MappingBase<Crm_lost_reasonDTO, Crm_lost_reason> {


}

