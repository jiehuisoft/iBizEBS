package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_partner_merge_automatic_wizard;
import cn.ibizlab.businesscentral.core.dto.Base_partner_merge_automatic_wizardDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreBase_partner_merge_automatic_wizardMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Base_partner_merge_automatic_wizardMapping extends MappingBase<Base_partner_merge_automatic_wizardDTO, Base_partner_merge_automatic_wizard> {


}

