package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_fetchmail.domain.Fetchmail_server;
import cn.ibizlab.businesscentral.core.odoo_fetchmail.service.IFetchmail_serverService;
import cn.ibizlab.businesscentral.core.odoo_fetchmail.filter.Fetchmail_serverSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Incoming Mail Server" })
@RestController("Core-fetchmail_server")
@RequestMapping("")
public class Fetchmail_serverResource {

    @Autowired
    public IFetchmail_serverService fetchmail_serverService;

    @Autowired
    @Lazy
    public Fetchmail_serverMapping fetchmail_serverMapping;

    @PreAuthorize("hasPermission(this.fetchmail_serverMapping.toDomain(#fetchmail_serverdto),'iBizBusinessCentral-Fetchmail_server-Create')")
    @ApiOperation(value = "新建Incoming Mail Server", tags = {"Incoming Mail Server" },  notes = "新建Incoming Mail Server")
	@RequestMapping(method = RequestMethod.POST, value = "/fetchmail_servers")
    public ResponseEntity<Fetchmail_serverDTO> create(@Validated @RequestBody Fetchmail_serverDTO fetchmail_serverdto) {
        Fetchmail_server domain = fetchmail_serverMapping.toDomain(fetchmail_serverdto);
		fetchmail_serverService.create(domain);
        Fetchmail_serverDTO dto = fetchmail_serverMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fetchmail_serverMapping.toDomain(#fetchmail_serverdtos),'iBizBusinessCentral-Fetchmail_server-Create')")
    @ApiOperation(value = "批量新建Incoming Mail Server", tags = {"Incoming Mail Server" },  notes = "批量新建Incoming Mail Server")
	@RequestMapping(method = RequestMethod.POST, value = "/fetchmail_servers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fetchmail_serverDTO> fetchmail_serverdtos) {
        fetchmail_serverService.createBatch(fetchmail_serverMapping.toDomain(fetchmail_serverdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fetchmail_server" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.fetchmail_serverService.get(#fetchmail_server_id),'iBizBusinessCentral-Fetchmail_server-Update')")
    @ApiOperation(value = "更新Incoming Mail Server", tags = {"Incoming Mail Server" },  notes = "更新Incoming Mail Server")
	@RequestMapping(method = RequestMethod.PUT, value = "/fetchmail_servers/{fetchmail_server_id}")
    public ResponseEntity<Fetchmail_serverDTO> update(@PathVariable("fetchmail_server_id") Long fetchmail_server_id, @RequestBody Fetchmail_serverDTO fetchmail_serverdto) {
		Fetchmail_server domain  = fetchmail_serverMapping.toDomain(fetchmail_serverdto);
        domain .setId(fetchmail_server_id);
		fetchmail_serverService.update(domain );
		Fetchmail_serverDTO dto = fetchmail_serverMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fetchmail_serverService.getFetchmailServerByEntities(this.fetchmail_serverMapping.toDomain(#fetchmail_serverdtos)),'iBizBusinessCentral-Fetchmail_server-Update')")
    @ApiOperation(value = "批量更新Incoming Mail Server", tags = {"Incoming Mail Server" },  notes = "批量更新Incoming Mail Server")
	@RequestMapping(method = RequestMethod.PUT, value = "/fetchmail_servers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fetchmail_serverDTO> fetchmail_serverdtos) {
        fetchmail_serverService.updateBatch(fetchmail_serverMapping.toDomain(fetchmail_serverdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.fetchmail_serverService.get(#fetchmail_server_id),'iBizBusinessCentral-Fetchmail_server-Remove')")
    @ApiOperation(value = "删除Incoming Mail Server", tags = {"Incoming Mail Server" },  notes = "删除Incoming Mail Server")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fetchmail_servers/{fetchmail_server_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fetchmail_server_id") Long fetchmail_server_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fetchmail_serverService.remove(fetchmail_server_id));
    }

    @PreAuthorize("hasPermission(this.fetchmail_serverService.getFetchmailServerByIds(#ids),'iBizBusinessCentral-Fetchmail_server-Remove')")
    @ApiOperation(value = "批量删除Incoming Mail Server", tags = {"Incoming Mail Server" },  notes = "批量删除Incoming Mail Server")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fetchmail_servers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        fetchmail_serverService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.fetchmail_serverMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fetchmail_server-Get')")
    @ApiOperation(value = "获取Incoming Mail Server", tags = {"Incoming Mail Server" },  notes = "获取Incoming Mail Server")
	@RequestMapping(method = RequestMethod.GET, value = "/fetchmail_servers/{fetchmail_server_id}")
    public ResponseEntity<Fetchmail_serverDTO> get(@PathVariable("fetchmail_server_id") Long fetchmail_server_id) {
        Fetchmail_server domain = fetchmail_serverService.get(fetchmail_server_id);
        Fetchmail_serverDTO dto = fetchmail_serverMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Incoming Mail Server草稿", tags = {"Incoming Mail Server" },  notes = "获取Incoming Mail Server草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/fetchmail_servers/getdraft")
    public ResponseEntity<Fetchmail_serverDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(fetchmail_serverMapping.toDto(fetchmail_serverService.getDraft(new Fetchmail_server())));
    }

    @ApiOperation(value = "检查Incoming Mail Server", tags = {"Incoming Mail Server" },  notes = "检查Incoming Mail Server")
	@RequestMapping(method = RequestMethod.POST, value = "/fetchmail_servers/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Fetchmail_serverDTO fetchmail_serverdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(fetchmail_serverService.checkKey(fetchmail_serverMapping.toDomain(fetchmail_serverdto)));
    }

    @PreAuthorize("hasPermission(this.fetchmail_serverMapping.toDomain(#fetchmail_serverdto),'iBizBusinessCentral-Fetchmail_server-Save')")
    @ApiOperation(value = "保存Incoming Mail Server", tags = {"Incoming Mail Server" },  notes = "保存Incoming Mail Server")
	@RequestMapping(method = RequestMethod.POST, value = "/fetchmail_servers/save")
    public ResponseEntity<Boolean> save(@RequestBody Fetchmail_serverDTO fetchmail_serverdto) {
        return ResponseEntity.status(HttpStatus.OK).body(fetchmail_serverService.save(fetchmail_serverMapping.toDomain(fetchmail_serverdto)));
    }

    @PreAuthorize("hasPermission(this.fetchmail_serverMapping.toDomain(#fetchmail_serverdtos),'iBizBusinessCentral-Fetchmail_server-Save')")
    @ApiOperation(value = "批量保存Incoming Mail Server", tags = {"Incoming Mail Server" },  notes = "批量保存Incoming Mail Server")
	@RequestMapping(method = RequestMethod.POST, value = "/fetchmail_servers/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Fetchmail_serverDTO> fetchmail_serverdtos) {
        fetchmail_serverService.saveBatch(fetchmail_serverMapping.toDomain(fetchmail_serverdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fetchmail_server-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fetchmail_server-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Incoming Mail Server" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/fetchmail_servers/fetchdefault")
	public ResponseEntity<List<Fetchmail_serverDTO>> fetchDefault(Fetchmail_serverSearchContext context) {
        Page<Fetchmail_server> domains = fetchmail_serverService.searchDefault(context) ;
        List<Fetchmail_serverDTO> list = fetchmail_serverMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fetchmail_server-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fetchmail_server-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Incoming Mail Server" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/fetchmail_servers/searchdefault")
	public ResponseEntity<Page<Fetchmail_serverDTO>> searchDefault(@RequestBody Fetchmail_serverSearchContext context) {
        Page<Fetchmail_server> domains = fetchmail_serverService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fetchmail_serverMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

