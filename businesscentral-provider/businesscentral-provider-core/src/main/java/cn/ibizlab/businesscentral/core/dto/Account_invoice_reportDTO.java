package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Account_invoice_reportDTO]
 */
@Data
public class Account_invoice_reportDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NUMBER]
     *
     */
    @JSONField(name = "number")
    @JsonProperty("number")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String number;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;

    /**
     * 属性 [USER_CURRENCY_PRICE_TOTAL]
     *
     */
    @JSONField(name = "user_currency_price_total")
    @JsonProperty("user_currency_price_total")
    private Double userCurrencyPriceTotal;

    /**
     * 属性 [RESIDUAL]
     *
     */
    @JSONField(name = "residual")
    @JsonProperty("residual")
    private Double residual;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [DATE_DUE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_due" , format="yyyy-MM-dd")
    @JsonProperty("date_due")
    private Timestamp dateDue;

    /**
     * 属性 [NBR]
     *
     */
    @JSONField(name = "nbr")
    @JsonProperty("nbr")
    private Integer nbr;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [AMOUNT_TOTAL]
     *
     */
    @JSONField(name = "amount_total")
    @JsonProperty("amount_total")
    private Double amountTotal;

    /**
     * 属性 [UOM_NAME]
     *
     */
    @JSONField(name = "uom_name")
    @JsonProperty("uom_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String uomName;

    /**
     * 属性 [PRICE_AVERAGE]
     *
     */
    @JSONField(name = "price_average")
    @JsonProperty("price_average")
    private Double priceAverage;

    /**
     * 属性 [CURRENCY_RATE]
     *
     */
    @JSONField(name = "currency_rate")
    @JsonProperty("currency_rate")
    private Double currencyRate;

    /**
     * 属性 [PRICE_TOTAL]
     *
     */
    @JSONField(name = "price_total")
    @JsonProperty("price_total")
    private Double priceTotal;

    /**
     * 属性 [USER_CURRENCY_PRICE_AVERAGE]
     *
     */
    @JSONField(name = "user_currency_price_average")
    @JsonProperty("user_currency_price_average")
    private Double userCurrencyPriceAverage;

    /**
     * 属性 [USER_CURRENCY_RESIDUAL]
     *
     */
    @JSONField(name = "user_currency_residual")
    @JsonProperty("user_currency_residual")
    private Double userCurrencyResidual;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String type;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String teamIdText;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID_TEXT]
     *
     */
    @JSONField(name = "account_analytic_id_text")
    @JsonProperty("account_analytic_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accountAnalyticIdText;

    /**
     * 属性 [CATEG_ID_TEXT]
     *
     */
    @JSONField(name = "categ_id_text")
    @JsonProperty("categ_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String categIdText;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String journalIdText;

    /**
     * 属性 [FISCAL_POSITION_ID_TEXT]
     *
     */
    @JSONField(name = "fiscal_position_id_text")
    @JsonProperty("fiscal_position_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String fiscalPositionIdText;

    /**
     * 属性 [INVOICE_ID_TEXT]
     *
     */
    @JSONField(name = "invoice_id_text")
    @JsonProperty("invoice_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String invoiceIdText;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String countryIdText;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currencyIdText;

    /**
     * 属性 [PAYMENT_TERM_ID_TEXT]
     *
     */
    @JSONField(name = "payment_term_id_text")
    @JsonProperty("payment_term_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String paymentTermIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [ACCOUNT_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "account_line_id_text")
    @JsonProperty("account_line_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accountLineIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "commercial_partner_id_text")
    @JsonProperty("commercial_partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String commercialPartnerIdText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accountIdText;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long countryId;

    /**
     * 属性 [PARTNER_BANK_ID]
     *
     */
    @JSONField(name = "partner_bank_id")
    @JsonProperty("partner_bank_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerBankId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [CATEG_ID]
     *
     */
    @JSONField(name = "categ_id")
    @JsonProperty("categ_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long categId;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID]
     *
     */
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long commercialPartnerId;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long accountId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [PAYMENT_TERM_ID]
     *
     */
    @JSONField(name = "payment_term_id")
    @JsonProperty("payment_term_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long paymentTermId;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID]
     *
     */
    @JSONField(name = "account_analytic_id")
    @JsonProperty("account_analytic_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long accountAnalyticId;

    /**
     * 属性 [FISCAL_POSITION_ID]
     *
     */
    @JSONField(name = "fiscal_position_id")
    @JsonProperty("fiscal_position_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long fiscalPositionId;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long journalId;

    /**
     * 属性 [ACCOUNT_LINE_ID]
     *
     */
    @JSONField(name = "account_line_id")
    @JsonProperty("account_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long accountLineId;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long teamId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long currencyId;

    /**
     * 属性 [INVOICE_ID]
     *
     */
    @JSONField(name = "invoice_id")
    @JsonProperty("invoice_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long invoiceId;


    /**
     * 设置 [NUMBER]
     */
    public void setNumber(String  number){
        this.number = number ;
        this.modify("number",number);
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    public void setProductQty(Double  productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [RESIDUAL]
     */
    public void setResidual(Double  residual){
        this.residual = residual ;
        this.modify("residual",residual);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [DATE_DUE]
     */
    public void setDateDue(Timestamp  dateDue){
        this.dateDue = dateDue ;
        this.modify("date_due",dateDue);
    }

    /**
     * 设置 [NBR]
     */
    public void setNbr(Integer  nbr){
        this.nbr = nbr ;
        this.modify("nbr",nbr);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [AMOUNT_TOTAL]
     */
    public void setAmountTotal(Double  amountTotal){
        this.amountTotal = amountTotal ;
        this.modify("amount_total",amountTotal);
    }

    /**
     * 设置 [UOM_NAME]
     */
    public void setUomName(String  uomName){
        this.uomName = uomName ;
        this.modify("uom_name",uomName);
    }

    /**
     * 设置 [PRICE_AVERAGE]
     */
    public void setPriceAverage(Double  priceAverage){
        this.priceAverage = priceAverage ;
        this.modify("price_average",priceAverage);
    }

    /**
     * 设置 [CURRENCY_RATE]
     */
    public void setCurrencyRate(Double  currencyRate){
        this.currencyRate = currencyRate ;
        this.modify("currency_rate",currencyRate);
    }

    /**
     * 设置 [PRICE_TOTAL]
     */
    public void setPriceTotal(Double  priceTotal){
        this.priceTotal = priceTotal ;
        this.modify("price_total",priceTotal);
    }

    /**
     * 设置 [TYPE]
     */
    public void setType(String  type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    public void setCountryId(Long  countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

    /**
     * 设置 [PARTNER_BANK_ID]
     */
    public void setPartnerBankId(Long  partnerBankId){
        this.partnerBankId = partnerBankId ;
        this.modify("partner_bank_id",partnerBankId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [CATEG_ID]
     */
    public void setCategId(Long  categId){
        this.categId = categId ;
        this.modify("categ_id",categId);
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID]
     */
    public void setCommercialPartnerId(Long  commercialPartnerId){
        this.commercialPartnerId = commercialPartnerId ;
        this.modify("commercial_partner_id",commercialPartnerId);
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    public void setAccountId(Long  accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [PAYMENT_TERM_ID]
     */
    public void setPaymentTermId(Long  paymentTermId){
        this.paymentTermId = paymentTermId ;
        this.modify("payment_term_id",paymentTermId);
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID]
     */
    public void setAccountAnalyticId(Long  accountAnalyticId){
        this.accountAnalyticId = accountAnalyticId ;
        this.modify("account_analytic_id",accountAnalyticId);
    }

    /**
     * 设置 [FISCAL_POSITION_ID]
     */
    public void setFiscalPositionId(Long  fiscalPositionId){
        this.fiscalPositionId = fiscalPositionId ;
        this.modify("fiscal_position_id",fiscalPositionId);
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    public void setJournalId(Long  journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [ACCOUNT_LINE_ID]
     */
    public void setAccountLineId(Long  accountLineId){
        this.accountLineId = accountLineId ;
        this.modify("account_line_id",accountLineId);
    }

    /**
     * 设置 [TEAM_ID]
     */
    public void setTeamId(Long  teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [INVOICE_ID]
     */
    public void setInvoiceId(Long  invoiceId){
        this.invoiceId = invoiceId ;
        this.modify("invoice_id",invoiceId);
    }


}


