package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_resume_line;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_resume_lineService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_resume_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"员工简历行" })
@RestController("Core-hr_resume_line")
@RequestMapping("")
public class Hr_resume_lineResource {

    @Autowired
    public IHr_resume_lineService hr_resume_lineService;

    @Autowired
    @Lazy
    public Hr_resume_lineMapping hr_resume_lineMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Create-all')")
    @ApiOperation(value = "新建员工简历行", tags = {"员工简历行" },  notes = "新建员工简历行")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_resume_lines")
    public ResponseEntity<Hr_resume_lineDTO> create(@Validated @RequestBody Hr_resume_lineDTO hr_resume_linedto) {
        Hr_resume_line domain = hr_resume_lineMapping.toDomain(hr_resume_linedto);
		hr_resume_lineService.create(domain);
        Hr_resume_lineDTO dto = hr_resume_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Create-all')")
    @ApiOperation(value = "批量新建员工简历行", tags = {"员工简历行" },  notes = "批量新建员工简历行")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_resume_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_resume_lineDTO> hr_resume_linedtos) {
        hr_resume_lineService.createBatch(hr_resume_lineMapping.toDomain(hr_resume_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Update-all')")
    @ApiOperation(value = "更新员工简历行", tags = {"员工简历行" },  notes = "更新员工简历行")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_resume_lines/{hr_resume_line_id}")
    public ResponseEntity<Hr_resume_lineDTO> update(@PathVariable("hr_resume_line_id") Long hr_resume_line_id, @RequestBody Hr_resume_lineDTO hr_resume_linedto) {
		Hr_resume_line domain  = hr_resume_lineMapping.toDomain(hr_resume_linedto);
        domain .setId(hr_resume_line_id);
		hr_resume_lineService.update(domain );
		Hr_resume_lineDTO dto = hr_resume_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Update-all')")
    @ApiOperation(value = "批量更新员工简历行", tags = {"员工简历行" },  notes = "批量更新员工简历行")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_resume_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_resume_lineDTO> hr_resume_linedtos) {
        hr_resume_lineService.updateBatch(hr_resume_lineMapping.toDomain(hr_resume_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Remove-all')")
    @ApiOperation(value = "删除员工简历行", tags = {"员工简历行" },  notes = "删除员工简历行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_resume_lines/{hr_resume_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_resume_line_id") Long hr_resume_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_resume_lineService.remove(hr_resume_line_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Remove-all')")
    @ApiOperation(value = "批量删除员工简历行", tags = {"员工简历行" },  notes = "批量删除员工简历行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_resume_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_resume_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Get-all')")
    @ApiOperation(value = "获取员工简历行", tags = {"员工简历行" },  notes = "获取员工简历行")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_resume_lines/{hr_resume_line_id}")
    public ResponseEntity<Hr_resume_lineDTO> get(@PathVariable("hr_resume_line_id") Long hr_resume_line_id) {
        Hr_resume_line domain = hr_resume_lineService.get(hr_resume_line_id);
        Hr_resume_lineDTO dto = hr_resume_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取员工简历行草稿", tags = {"员工简历行" },  notes = "获取员工简历行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_resume_lines/getdraft")
    public ResponseEntity<Hr_resume_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_resume_lineMapping.toDto(hr_resume_lineService.getDraft(new Hr_resume_line())));
    }

    @ApiOperation(value = "检查员工简历行", tags = {"员工简历行" },  notes = "检查员工简历行")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_resume_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_resume_lineDTO hr_resume_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_resume_lineService.checkKey(hr_resume_lineMapping.toDomain(hr_resume_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Save-all')")
    @ApiOperation(value = "保存员工简历行", tags = {"员工简历行" },  notes = "保存员工简历行")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_resume_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_resume_lineDTO hr_resume_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_resume_lineService.save(hr_resume_lineMapping.toDomain(hr_resume_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Save-all')")
    @ApiOperation(value = "批量保存员工简历行", tags = {"员工简历行" },  notes = "批量保存员工简历行")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_resume_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_resume_lineDTO> hr_resume_linedtos) {
        hr_resume_lineService.saveBatch(hr_resume_lineMapping.toDomain(hr_resume_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"员工简历行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_resume_lines/fetchdefault")
	public ResponseEntity<List<Hr_resume_lineDTO>> fetchDefault(Hr_resume_lineSearchContext context) {
        Page<Hr_resume_line> domains = hr_resume_lineService.searchDefault(context) ;
        List<Hr_resume_lineDTO> list = hr_resume_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"员工简历行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_resume_lines/searchdefault")
	public ResponseEntity<Page<Hr_resume_lineDTO>> searchDefault(@RequestBody Hr_resume_lineSearchContext context) {
        Page<Hr_resume_line> domains = hr_resume_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_resume_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Create-all')")
    @ApiOperation(value = "根据员工建立员工简历行", tags = {"员工简历行" },  notes = "根据员工建立员工简历行")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_resume_lines")
    public ResponseEntity<Hr_resume_lineDTO> createByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_resume_lineDTO hr_resume_linedto) {
        Hr_resume_line domain = hr_resume_lineMapping.toDomain(hr_resume_linedto);
        domain.setEmployeeId(hr_employee_id);
		hr_resume_lineService.create(domain);
        Hr_resume_lineDTO dto = hr_resume_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Create-all')")
    @ApiOperation(value = "根据员工批量建立员工简历行", tags = {"员工简历行" },  notes = "根据员工批量建立员工简历行")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_resume_lines/batch")
    public ResponseEntity<Boolean> createBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Hr_resume_lineDTO> hr_resume_linedtos) {
        List<Hr_resume_line> domainlist=hr_resume_lineMapping.toDomain(hr_resume_linedtos);
        for(Hr_resume_line domain:domainlist){
            domain.setEmployeeId(hr_employee_id);
        }
        hr_resume_lineService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Update-all')")
    @ApiOperation(value = "根据员工更新员工简历行", tags = {"员工简历行" },  notes = "根据员工更新员工简历行")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/{hr_employee_id}/hr_resume_lines/{hr_resume_line_id}")
    public ResponseEntity<Hr_resume_lineDTO> updateByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("hr_resume_line_id") Long hr_resume_line_id, @RequestBody Hr_resume_lineDTO hr_resume_linedto) {
        Hr_resume_line domain = hr_resume_lineMapping.toDomain(hr_resume_linedto);
        domain.setEmployeeId(hr_employee_id);
        domain.setId(hr_resume_line_id);
		hr_resume_lineService.update(domain);
        Hr_resume_lineDTO dto = hr_resume_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Update-all')")
    @ApiOperation(value = "根据员工批量更新员工简历行", tags = {"员工简历行" },  notes = "根据员工批量更新员工简历行")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/{hr_employee_id}/hr_resume_lines/batch")
    public ResponseEntity<Boolean> updateBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Hr_resume_lineDTO> hr_resume_linedtos) {
        List<Hr_resume_line> domainlist=hr_resume_lineMapping.toDomain(hr_resume_linedtos);
        for(Hr_resume_line domain:domainlist){
            domain.setEmployeeId(hr_employee_id);
        }
        hr_resume_lineService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Remove-all')")
    @ApiOperation(value = "根据员工删除员工简历行", tags = {"员工简历行" },  notes = "根据员工删除员工简历行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/{hr_employee_id}/hr_resume_lines/{hr_resume_line_id}")
    public ResponseEntity<Boolean> removeByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("hr_resume_line_id") Long hr_resume_line_id) {
		return ResponseEntity.status(HttpStatus.OK).body(hr_resume_lineService.remove(hr_resume_line_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Remove-all')")
    @ApiOperation(value = "根据员工批量删除员工简历行", tags = {"员工简历行" },  notes = "根据员工批量删除员工简历行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/{hr_employee_id}/hr_resume_lines/batch")
    public ResponseEntity<Boolean> removeBatchByHr_employee(@RequestBody List<Long> ids) {
        hr_resume_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Get-all')")
    @ApiOperation(value = "根据员工获取员工简历行", tags = {"员工简历行" },  notes = "根据员工获取员工简历行")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_employees/{hr_employee_id}/hr_resume_lines/{hr_resume_line_id}")
    public ResponseEntity<Hr_resume_lineDTO> getByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("hr_resume_line_id") Long hr_resume_line_id) {
        Hr_resume_line domain = hr_resume_lineService.get(hr_resume_line_id);
        Hr_resume_lineDTO dto = hr_resume_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据员工获取员工简历行草稿", tags = {"员工简历行" },  notes = "根据员工获取员工简历行草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/hr_employees/{hr_employee_id}/hr_resume_lines/getdraft")
    public ResponseEntity<Hr_resume_lineDTO> getDraftByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id) {
        Hr_resume_line domain = new Hr_resume_line();
        domain.setEmployeeId(hr_employee_id);
        return ResponseEntity.status(HttpStatus.OK).body(hr_resume_lineMapping.toDto(hr_resume_lineService.getDraft(domain)));
    }

    @ApiOperation(value = "根据员工检查员工简历行", tags = {"员工简历行" },  notes = "根据员工检查员工简历行")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_resume_lines/checkkey")
    public ResponseEntity<Boolean> checkKeyByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_resume_lineDTO hr_resume_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_resume_lineService.checkKey(hr_resume_lineMapping.toDomain(hr_resume_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Save-all')")
    @ApiOperation(value = "根据员工保存员工简历行", tags = {"员工简历行" },  notes = "根据员工保存员工简历行")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_resume_lines/save")
    public ResponseEntity<Boolean> saveByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_resume_lineDTO hr_resume_linedto) {
        Hr_resume_line domain = hr_resume_lineMapping.toDomain(hr_resume_linedto);
        domain.setEmployeeId(hr_employee_id);
        return ResponseEntity.status(HttpStatus.OK).body(hr_resume_lineService.save(domain));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-Save-all')")
    @ApiOperation(value = "根据员工批量保存员工简历行", tags = {"员工简历行" },  notes = "根据员工批量保存员工简历行")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_resume_lines/savebatch")
    public ResponseEntity<Boolean> saveBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Hr_resume_lineDTO> hr_resume_linedtos) {
        List<Hr_resume_line> domainlist=hr_resume_lineMapping.toDomain(hr_resume_linedtos);
        for(Hr_resume_line domain:domainlist){
             domain.setEmployeeId(hr_employee_id);
        }
        hr_resume_lineService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-searchDefault-all')")
	@ApiOperation(value = "根据员工获取数据集", tags = {"员工简历行" } ,notes = "根据员工获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_employees/{hr_employee_id}/hr_resume_lines/fetchdefault")
	public ResponseEntity<List<Hr_resume_lineDTO>> fetchHr_resume_lineDefaultByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id,Hr_resume_lineSearchContext context) {
        context.setN_employee_id_eq(hr_employee_id);
        Page<Hr_resume_line> domains = hr_resume_lineService.searchDefault(context) ;
        List<Hr_resume_lineDTO> list = hr_resume_lineMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line-searchDefault-all')")
	@ApiOperation(value = "根据员工查询数据集", tags = {"员工简历行" } ,notes = "根据员工查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_employees/{hr_employee_id}/hr_resume_lines/searchdefault")
	public ResponseEntity<Page<Hr_resume_lineDTO>> searchHr_resume_lineDefaultByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_resume_lineSearchContext context) {
        context.setN_employee_id_eq(hr_employee_id);
        Page<Hr_resume_line> domains = hr_resume_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_resume_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

