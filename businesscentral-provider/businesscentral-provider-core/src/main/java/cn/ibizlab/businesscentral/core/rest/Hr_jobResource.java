package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_jobService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_jobSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工作岗位" })
@RestController("Core-hr_job")
@RequestMapping("")
public class Hr_jobResource {

    @Autowired
    public IHr_jobService hr_jobService;

    @Autowired
    @Lazy
    public Hr_jobMapping hr_jobMapping;

    @PreAuthorize("hasPermission(this.hr_jobMapping.toDomain(#hr_jobdto),'iBizBusinessCentral-Hr_job-Create')")
    @ApiOperation(value = "新建工作岗位", tags = {"工作岗位" },  notes = "新建工作岗位")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_jobs")
    public ResponseEntity<Hr_jobDTO> create(@Validated @RequestBody Hr_jobDTO hr_jobdto) {
        Hr_job domain = hr_jobMapping.toDomain(hr_jobdto);
		hr_jobService.create(domain);
        Hr_jobDTO dto = hr_jobMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_jobMapping.toDomain(#hr_jobdtos),'iBizBusinessCentral-Hr_job-Create')")
    @ApiOperation(value = "批量新建工作岗位", tags = {"工作岗位" },  notes = "批量新建工作岗位")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_jobs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_jobDTO> hr_jobdtos) {
        hr_jobService.createBatch(hr_jobMapping.toDomain(hr_jobdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_job" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_jobService.get(#hr_job_id),'iBizBusinessCentral-Hr_job-Update')")
    @ApiOperation(value = "更新工作岗位", tags = {"工作岗位" },  notes = "更新工作岗位")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_jobs/{hr_job_id}")
    public ResponseEntity<Hr_jobDTO> update(@PathVariable("hr_job_id") Long hr_job_id, @RequestBody Hr_jobDTO hr_jobdto) {
		Hr_job domain  = hr_jobMapping.toDomain(hr_jobdto);
        domain .setId(hr_job_id);
		hr_jobService.update(domain );
		Hr_jobDTO dto = hr_jobMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_jobService.getHrJobByEntities(this.hr_jobMapping.toDomain(#hr_jobdtos)),'iBizBusinessCentral-Hr_job-Update')")
    @ApiOperation(value = "批量更新工作岗位", tags = {"工作岗位" },  notes = "批量更新工作岗位")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_jobs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_jobDTO> hr_jobdtos) {
        hr_jobService.updateBatch(hr_jobMapping.toDomain(hr_jobdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_jobService.get(#hr_job_id),'iBizBusinessCentral-Hr_job-Remove')")
    @ApiOperation(value = "删除工作岗位", tags = {"工作岗位" },  notes = "删除工作岗位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_jobs/{hr_job_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_job_id") Long hr_job_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_jobService.remove(hr_job_id));
    }

    @PreAuthorize("hasPermission(this.hr_jobService.getHrJobByIds(#ids),'iBizBusinessCentral-Hr_job-Remove')")
    @ApiOperation(value = "批量删除工作岗位", tags = {"工作岗位" },  notes = "批量删除工作岗位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_jobs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_jobService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_jobMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_job-Get')")
    @ApiOperation(value = "获取工作岗位", tags = {"工作岗位" },  notes = "获取工作岗位")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_jobs/{hr_job_id}")
    public ResponseEntity<Hr_jobDTO> get(@PathVariable("hr_job_id") Long hr_job_id) {
        Hr_job domain = hr_jobService.get(hr_job_id);
        Hr_jobDTO dto = hr_jobMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工作岗位草稿", tags = {"工作岗位" },  notes = "获取工作岗位草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_jobs/getdraft")
    public ResponseEntity<Hr_jobDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_jobMapping.toDto(hr_jobService.getDraft(new Hr_job())));
    }

    @ApiOperation(value = "检查工作岗位", tags = {"工作岗位" },  notes = "检查工作岗位")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_jobs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_jobDTO hr_jobdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_jobService.checkKey(hr_jobMapping.toDomain(hr_jobdto)));
    }

    @PreAuthorize("hasPermission(this.hr_jobMapping.toDomain(#hr_jobdto),'iBizBusinessCentral-Hr_job-Save')")
    @ApiOperation(value = "保存工作岗位", tags = {"工作岗位" },  notes = "保存工作岗位")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_jobs/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_jobDTO hr_jobdto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_jobService.save(hr_jobMapping.toDomain(hr_jobdto)));
    }

    @PreAuthorize("hasPermission(this.hr_jobMapping.toDomain(#hr_jobdtos),'iBizBusinessCentral-Hr_job-Save')")
    @ApiOperation(value = "批量保存工作岗位", tags = {"工作岗位" },  notes = "批量保存工作岗位")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_jobs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_jobDTO> hr_jobdtos) {
        hr_jobService.saveBatch(hr_jobMapping.toDomain(hr_jobdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_job-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_job-Get')")
	@ApiOperation(value = "获取数据集", tags = {"工作岗位" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_jobs/fetchdefault")
	public ResponseEntity<List<Hr_jobDTO>> fetchDefault(Hr_jobSearchContext context) {
        Page<Hr_job> domains = hr_jobService.searchDefault(context) ;
        List<Hr_jobDTO> list = hr_jobMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_job-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_job-Get')")
	@ApiOperation(value = "查询数据集", tags = {"工作岗位" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_jobs/searchdefault")
	public ResponseEntity<Page<Hr_jobDTO>> searchDefault(@RequestBody Hr_jobSearchContext context) {
        Page<Hr_job> domains = hr_jobService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_jobMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_job-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Hr_job-Get')")
	@ApiOperation(value = "获取首选表格", tags = {"工作岗位" } ,notes = "获取首选表格")
    @RequestMapping(method= RequestMethod.GET , value="/hr_jobs/fetchmaster")
	public ResponseEntity<List<Hr_jobDTO>> fetchMaster(Hr_jobSearchContext context) {
        Page<Hr_job> domains = hr_jobService.searchMaster(context) ;
        List<Hr_jobDTO> list = hr_jobMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_job-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Hr_job-Get')")
	@ApiOperation(value = "查询首选表格", tags = {"工作岗位" } ,notes = "查询首选表格")
    @RequestMapping(method= RequestMethod.POST , value="/hr_jobs/searchmaster")
	public ResponseEntity<Page<Hr_jobDTO>> searchMaster(@RequestBody Hr_jobSearchContext context) {
        Page<Hr_job> domains = hr_jobService.searchMaster(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_jobMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

