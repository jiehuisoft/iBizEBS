package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_templateService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_templateSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"EMail模板" })
@RestController("Core-mail_template")
@RequestMapping("")
public class Mail_templateResource {

    @Autowired
    public IMail_templateService mail_templateService;

    @Autowired
    @Lazy
    public Mail_templateMapping mail_templateMapping;

    @PreAuthorize("hasPermission(this.mail_templateMapping.toDomain(#mail_templatedto),'iBizBusinessCentral-Mail_template-Create')")
    @ApiOperation(value = "新建EMail模板", tags = {"EMail模板" },  notes = "新建EMail模板")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_templates")
    public ResponseEntity<Mail_templateDTO> create(@Validated @RequestBody Mail_templateDTO mail_templatedto) {
        Mail_template domain = mail_templateMapping.toDomain(mail_templatedto);
		mail_templateService.create(domain);
        Mail_templateDTO dto = mail_templateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_templateMapping.toDomain(#mail_templatedtos),'iBizBusinessCentral-Mail_template-Create')")
    @ApiOperation(value = "批量新建EMail模板", tags = {"EMail模板" },  notes = "批量新建EMail模板")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_templates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_templateDTO> mail_templatedtos) {
        mail_templateService.createBatch(mail_templateMapping.toDomain(mail_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_template" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_templateService.get(#mail_template_id),'iBizBusinessCentral-Mail_template-Update')")
    @ApiOperation(value = "更新EMail模板", tags = {"EMail模板" },  notes = "更新EMail模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_templates/{mail_template_id}")
    public ResponseEntity<Mail_templateDTO> update(@PathVariable("mail_template_id") Long mail_template_id, @RequestBody Mail_templateDTO mail_templatedto) {
		Mail_template domain  = mail_templateMapping.toDomain(mail_templatedto);
        domain .setId(mail_template_id);
		mail_templateService.update(domain );
		Mail_templateDTO dto = mail_templateMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_templateService.getMailTemplateByEntities(this.mail_templateMapping.toDomain(#mail_templatedtos)),'iBizBusinessCentral-Mail_template-Update')")
    @ApiOperation(value = "批量更新EMail模板", tags = {"EMail模板" },  notes = "批量更新EMail模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_templates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_templateDTO> mail_templatedtos) {
        mail_templateService.updateBatch(mail_templateMapping.toDomain(mail_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_templateService.get(#mail_template_id),'iBizBusinessCentral-Mail_template-Remove')")
    @ApiOperation(value = "删除EMail模板", tags = {"EMail模板" },  notes = "删除EMail模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_templates/{mail_template_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_template_id") Long mail_template_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_templateService.remove(mail_template_id));
    }

    @PreAuthorize("hasPermission(this.mail_templateService.getMailTemplateByIds(#ids),'iBizBusinessCentral-Mail_template-Remove')")
    @ApiOperation(value = "批量删除EMail模板", tags = {"EMail模板" },  notes = "批量删除EMail模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_templates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_templateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_templateMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_template-Get')")
    @ApiOperation(value = "获取EMail模板", tags = {"EMail模板" },  notes = "获取EMail模板")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_templates/{mail_template_id}")
    public ResponseEntity<Mail_templateDTO> get(@PathVariable("mail_template_id") Long mail_template_id) {
        Mail_template domain = mail_templateService.get(mail_template_id);
        Mail_templateDTO dto = mail_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取EMail模板草稿", tags = {"EMail模板" },  notes = "获取EMail模板草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_templates/getdraft")
    public ResponseEntity<Mail_templateDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_templateMapping.toDto(mail_templateService.getDraft(new Mail_template())));
    }

    @ApiOperation(value = "检查EMail模板", tags = {"EMail模板" },  notes = "检查EMail模板")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_templates/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_templateDTO mail_templatedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_templateService.checkKey(mail_templateMapping.toDomain(mail_templatedto)));
    }

    @PreAuthorize("hasPermission(this.mail_templateMapping.toDomain(#mail_templatedto),'iBizBusinessCentral-Mail_template-Save')")
    @ApiOperation(value = "保存EMail模板", tags = {"EMail模板" },  notes = "保存EMail模板")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_templates/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_templateDTO mail_templatedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_templateService.save(mail_templateMapping.toDomain(mail_templatedto)));
    }

    @PreAuthorize("hasPermission(this.mail_templateMapping.toDomain(#mail_templatedtos),'iBizBusinessCentral-Mail_template-Save')")
    @ApiOperation(value = "批量保存EMail模板", tags = {"EMail模板" },  notes = "批量保存EMail模板")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_templates/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_templateDTO> mail_templatedtos) {
        mail_templateService.saveBatch(mail_templateMapping.toDomain(mail_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_template-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_template-Get')")
	@ApiOperation(value = "获取数据集", tags = {"EMail模板" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_templates/fetchdefault")
	public ResponseEntity<List<Mail_templateDTO>> fetchDefault(Mail_templateSearchContext context) {
        Page<Mail_template> domains = mail_templateService.searchDefault(context) ;
        List<Mail_templateDTO> list = mail_templateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_template-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_template-Get')")
	@ApiOperation(value = "查询数据集", tags = {"EMail模板" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_templates/searchdefault")
	public ResponseEntity<Page<Mail_templateDTO>> searchDefault(@RequestBody Mail_templateSearchContext context) {
        Page<Mail_template> domains = mail_templateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_templateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

