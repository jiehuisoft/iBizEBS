package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_event_type;
import cn.ibizlab.businesscentral.core.dto.Calendar_event_typeDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreCalendar_event_typeMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Calendar_event_typeMapping extends MappingBase<Calendar_event_typeDTO, Calendar_event_type> {


}

