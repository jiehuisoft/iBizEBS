package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Stock_return_pickingDTO]
 */
@Data
public class Stock_return_pickingDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [PRODUCT_RETURN_MOVES]
     *
     */
    @JSONField(name = "product_return_moves")
    @JsonProperty("product_return_moves")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String productReturnMoves;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [MOVE_DEST_EXISTS]
     *
     */
    @JSONField(name = "move_dest_exists")
    @JsonProperty("move_dest_exists")
    private Boolean moveDestExists;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ORIGINAL_LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "original_location_id_text")
    @JsonProperty("original_location_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String originalLocationIdText;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String locationIdText;

    /**
     * 属性 [PARENT_LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "parent_location_id_text")
    @JsonProperty("parent_location_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String parentLocationIdText;

    /**
     * 属性 [PICKING_ID_TEXT]
     *
     */
    @JSONField(name = "picking_id_text")
    @JsonProperty("picking_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pickingIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [PARENT_LOCATION_ID]
     *
     */
    @JSONField(name = "parent_location_id")
    @JsonProperty("parent_location_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentLocationId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [ORIGINAL_LOCATION_ID]
     *
     */
    @JSONField(name = "original_location_id")
    @JsonProperty("original_location_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long originalLocationId;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long locationId;

    /**
     * 属性 [PICKING_ID]
     *
     */
    @JSONField(name = "picking_id")
    @JsonProperty("picking_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pickingId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [MOVE_DEST_EXISTS]
     */
    public void setMoveDestExists(Boolean  moveDestExists){
        this.moveDestExists = moveDestExists ;
        this.modify("move_dest_exists",moveDestExists);
    }

    /**
     * 设置 [PARENT_LOCATION_ID]
     */
    public void setParentLocationId(Long  parentLocationId){
        this.parentLocationId = parentLocationId ;
        this.modify("parent_location_id",parentLocationId);
    }

    /**
     * 设置 [ORIGINAL_LOCATION_ID]
     */
    public void setOriginalLocationId(Long  originalLocationId){
        this.originalLocationId = originalLocationId ;
        this.modify("original_location_id",originalLocationId);
    }

    /**
     * 设置 [LOCATION_ID]
     */
    public void setLocationId(Long  locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }

    /**
     * 设置 [PICKING_ID]
     */
    public void setPickingId(Long  pickingId){
        this.pickingId = pickingId ;
        this.modify("picking_id",pickingId);
    }


}


