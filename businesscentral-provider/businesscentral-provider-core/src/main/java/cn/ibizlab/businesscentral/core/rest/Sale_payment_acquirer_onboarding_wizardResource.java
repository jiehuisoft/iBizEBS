package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_payment_acquirer_onboarding_wizardService;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_payment_acquirer_onboarding_wizardSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"销售付款获得在线向导" })
@RestController("Core-sale_payment_acquirer_onboarding_wizard")
@RequestMapping("")
public class Sale_payment_acquirer_onboarding_wizardResource {

    @Autowired
    public ISale_payment_acquirer_onboarding_wizardService sale_payment_acquirer_onboarding_wizardService;

    @Autowired
    @Lazy
    public Sale_payment_acquirer_onboarding_wizardMapping sale_payment_acquirer_onboarding_wizardMapping;

    @PreAuthorize("hasPermission(this.sale_payment_acquirer_onboarding_wizardMapping.toDomain(#sale_payment_acquirer_onboarding_wizarddto),'iBizBusinessCentral-Sale_payment_acquirer_onboarding_wizard-Create')")
    @ApiOperation(value = "新建销售付款获得在线向导", tags = {"销售付款获得在线向导" },  notes = "新建销售付款获得在线向导")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_payment_acquirer_onboarding_wizards")
    public ResponseEntity<Sale_payment_acquirer_onboarding_wizardDTO> create(@Validated @RequestBody Sale_payment_acquirer_onboarding_wizardDTO sale_payment_acquirer_onboarding_wizarddto) {
        Sale_payment_acquirer_onboarding_wizard domain = sale_payment_acquirer_onboarding_wizardMapping.toDomain(sale_payment_acquirer_onboarding_wizarddto);
		sale_payment_acquirer_onboarding_wizardService.create(domain);
        Sale_payment_acquirer_onboarding_wizardDTO dto = sale_payment_acquirer_onboarding_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_payment_acquirer_onboarding_wizardMapping.toDomain(#sale_payment_acquirer_onboarding_wizarddtos),'iBizBusinessCentral-Sale_payment_acquirer_onboarding_wizard-Create')")
    @ApiOperation(value = "批量新建销售付款获得在线向导", tags = {"销售付款获得在线向导" },  notes = "批量新建销售付款获得在线向导")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sale_payment_acquirer_onboarding_wizardDTO> sale_payment_acquirer_onboarding_wizarddtos) {
        sale_payment_acquirer_onboarding_wizardService.createBatch(sale_payment_acquirer_onboarding_wizardMapping.toDomain(sale_payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "sale_payment_acquirer_onboarding_wizard" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.sale_payment_acquirer_onboarding_wizardService.get(#sale_payment_acquirer_onboarding_wizard_id),'iBizBusinessCentral-Sale_payment_acquirer_onboarding_wizard-Update')")
    @ApiOperation(value = "更新销售付款获得在线向导", tags = {"销售付款获得在线向导" },  notes = "更新销售付款获得在线向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_payment_acquirer_onboarding_wizards/{sale_payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Sale_payment_acquirer_onboarding_wizardDTO> update(@PathVariable("sale_payment_acquirer_onboarding_wizard_id") Long sale_payment_acquirer_onboarding_wizard_id, @RequestBody Sale_payment_acquirer_onboarding_wizardDTO sale_payment_acquirer_onboarding_wizarddto) {
		Sale_payment_acquirer_onboarding_wizard domain  = sale_payment_acquirer_onboarding_wizardMapping.toDomain(sale_payment_acquirer_onboarding_wizarddto);
        domain .setId(sale_payment_acquirer_onboarding_wizard_id);
		sale_payment_acquirer_onboarding_wizardService.update(domain );
		Sale_payment_acquirer_onboarding_wizardDTO dto = sale_payment_acquirer_onboarding_wizardMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_payment_acquirer_onboarding_wizardService.getSalePaymentAcquirerOnboardingWizardByEntities(this.sale_payment_acquirer_onboarding_wizardMapping.toDomain(#sale_payment_acquirer_onboarding_wizarddtos)),'iBizBusinessCentral-Sale_payment_acquirer_onboarding_wizard-Update')")
    @ApiOperation(value = "批量更新销售付款获得在线向导", tags = {"销售付款获得在线向导" },  notes = "批量更新销售付款获得在线向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_payment_acquirer_onboarding_wizardDTO> sale_payment_acquirer_onboarding_wizarddtos) {
        sale_payment_acquirer_onboarding_wizardService.updateBatch(sale_payment_acquirer_onboarding_wizardMapping.toDomain(sale_payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.sale_payment_acquirer_onboarding_wizardService.get(#sale_payment_acquirer_onboarding_wizard_id),'iBizBusinessCentral-Sale_payment_acquirer_onboarding_wizard-Remove')")
    @ApiOperation(value = "删除销售付款获得在线向导", tags = {"销售付款获得在线向导" },  notes = "删除销售付款获得在线向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_payment_acquirer_onboarding_wizards/{sale_payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("sale_payment_acquirer_onboarding_wizard_id") Long sale_payment_acquirer_onboarding_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sale_payment_acquirer_onboarding_wizardService.remove(sale_payment_acquirer_onboarding_wizard_id));
    }

    @PreAuthorize("hasPermission(this.sale_payment_acquirer_onboarding_wizardService.getSalePaymentAcquirerOnboardingWizardByIds(#ids),'iBizBusinessCentral-Sale_payment_acquirer_onboarding_wizard-Remove')")
    @ApiOperation(value = "批量删除销售付款获得在线向导", tags = {"销售付款获得在线向导" },  notes = "批量删除销售付款获得在线向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        sale_payment_acquirer_onboarding_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.sale_payment_acquirer_onboarding_wizardMapping.toDomain(returnObject.body),'iBizBusinessCentral-Sale_payment_acquirer_onboarding_wizard-Get')")
    @ApiOperation(value = "获取销售付款获得在线向导", tags = {"销售付款获得在线向导" },  notes = "获取销售付款获得在线向导")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_payment_acquirer_onboarding_wizards/{sale_payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Sale_payment_acquirer_onboarding_wizardDTO> get(@PathVariable("sale_payment_acquirer_onboarding_wizard_id") Long sale_payment_acquirer_onboarding_wizard_id) {
        Sale_payment_acquirer_onboarding_wizard domain = sale_payment_acquirer_onboarding_wizardService.get(sale_payment_acquirer_onboarding_wizard_id);
        Sale_payment_acquirer_onboarding_wizardDTO dto = sale_payment_acquirer_onboarding_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取销售付款获得在线向导草稿", tags = {"销售付款获得在线向导" },  notes = "获取销售付款获得在线向导草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_payment_acquirer_onboarding_wizards/getdraft")
    public ResponseEntity<Sale_payment_acquirer_onboarding_wizardDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(sale_payment_acquirer_onboarding_wizardMapping.toDto(sale_payment_acquirer_onboarding_wizardService.getDraft(new Sale_payment_acquirer_onboarding_wizard())));
    }

    @ApiOperation(value = "检查销售付款获得在线向导", tags = {"销售付款获得在线向导" },  notes = "检查销售付款获得在线向导")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_payment_acquirer_onboarding_wizards/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sale_payment_acquirer_onboarding_wizardDTO sale_payment_acquirer_onboarding_wizarddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(sale_payment_acquirer_onboarding_wizardService.checkKey(sale_payment_acquirer_onboarding_wizardMapping.toDomain(sale_payment_acquirer_onboarding_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.sale_payment_acquirer_onboarding_wizardMapping.toDomain(#sale_payment_acquirer_onboarding_wizarddto),'iBizBusinessCentral-Sale_payment_acquirer_onboarding_wizard-Save')")
    @ApiOperation(value = "保存销售付款获得在线向导", tags = {"销售付款获得在线向导" },  notes = "保存销售付款获得在线向导")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_payment_acquirer_onboarding_wizards/save")
    public ResponseEntity<Boolean> save(@RequestBody Sale_payment_acquirer_onboarding_wizardDTO sale_payment_acquirer_onboarding_wizarddto) {
        return ResponseEntity.status(HttpStatus.OK).body(sale_payment_acquirer_onboarding_wizardService.save(sale_payment_acquirer_onboarding_wizardMapping.toDomain(sale_payment_acquirer_onboarding_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.sale_payment_acquirer_onboarding_wizardMapping.toDomain(#sale_payment_acquirer_onboarding_wizarddtos),'iBizBusinessCentral-Sale_payment_acquirer_onboarding_wizard-Save')")
    @ApiOperation(value = "批量保存销售付款获得在线向导", tags = {"销售付款获得在线向导" },  notes = "批量保存销售付款获得在线向导")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_payment_acquirer_onboarding_wizards/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Sale_payment_acquirer_onboarding_wizardDTO> sale_payment_acquirer_onboarding_wizarddtos) {
        sale_payment_acquirer_onboarding_wizardService.saveBatch(sale_payment_acquirer_onboarding_wizardMapping.toDomain(sale_payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_payment_acquirer_onboarding_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_payment_acquirer_onboarding_wizard-Get')")
	@ApiOperation(value = "获取数据集", tags = {"销售付款获得在线向导" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/sale_payment_acquirer_onboarding_wizards/fetchdefault")
	public ResponseEntity<List<Sale_payment_acquirer_onboarding_wizardDTO>> fetchDefault(Sale_payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Sale_payment_acquirer_onboarding_wizard> domains = sale_payment_acquirer_onboarding_wizardService.searchDefault(context) ;
        List<Sale_payment_acquirer_onboarding_wizardDTO> list = sale_payment_acquirer_onboarding_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_payment_acquirer_onboarding_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_payment_acquirer_onboarding_wizard-Get')")
	@ApiOperation(value = "查询数据集", tags = {"销售付款获得在线向导" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/sale_payment_acquirer_onboarding_wizards/searchdefault")
	public ResponseEntity<Page<Sale_payment_acquirer_onboarding_wizardDTO>> searchDefault(@RequestBody Sale_payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Sale_payment_acquirer_onboarding_wizard> domains = sale_payment_acquirer_onboarding_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sale_payment_acquirer_onboarding_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

