package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_attendee;
import cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_attendeeService;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_attendeeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"日历出席者信息" })
@RestController("Core-calendar_attendee")
@RequestMapping("")
public class Calendar_attendeeResource {

    @Autowired
    public ICalendar_attendeeService calendar_attendeeService;

    @Autowired
    @Lazy
    public Calendar_attendeeMapping calendar_attendeeMapping;

    @PreAuthorize("hasPermission(this.calendar_attendeeMapping.toDomain(#calendar_attendeedto),'iBizBusinessCentral-Calendar_attendee-Create')")
    @ApiOperation(value = "新建日历出席者信息", tags = {"日历出席者信息" },  notes = "新建日历出席者信息")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_attendees")
    public ResponseEntity<Calendar_attendeeDTO> create(@Validated @RequestBody Calendar_attendeeDTO calendar_attendeedto) {
        Calendar_attendee domain = calendar_attendeeMapping.toDomain(calendar_attendeedto);
		calendar_attendeeService.create(domain);
        Calendar_attendeeDTO dto = calendar_attendeeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.calendar_attendeeMapping.toDomain(#calendar_attendeedtos),'iBizBusinessCentral-Calendar_attendee-Create')")
    @ApiOperation(value = "批量新建日历出席者信息", tags = {"日历出席者信息" },  notes = "批量新建日历出席者信息")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_attendees/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Calendar_attendeeDTO> calendar_attendeedtos) {
        calendar_attendeeService.createBatch(calendar_attendeeMapping.toDomain(calendar_attendeedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "calendar_attendee" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.calendar_attendeeService.get(#calendar_attendee_id),'iBizBusinessCentral-Calendar_attendee-Update')")
    @ApiOperation(value = "更新日历出席者信息", tags = {"日历出席者信息" },  notes = "更新日历出席者信息")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_attendees/{calendar_attendee_id}")
    public ResponseEntity<Calendar_attendeeDTO> update(@PathVariable("calendar_attendee_id") Long calendar_attendee_id, @RequestBody Calendar_attendeeDTO calendar_attendeedto) {
		Calendar_attendee domain  = calendar_attendeeMapping.toDomain(calendar_attendeedto);
        domain .setId(calendar_attendee_id);
		calendar_attendeeService.update(domain );
		Calendar_attendeeDTO dto = calendar_attendeeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.calendar_attendeeService.getCalendarAttendeeByEntities(this.calendar_attendeeMapping.toDomain(#calendar_attendeedtos)),'iBizBusinessCentral-Calendar_attendee-Update')")
    @ApiOperation(value = "批量更新日历出席者信息", tags = {"日历出席者信息" },  notes = "批量更新日历出席者信息")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_attendees/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_attendeeDTO> calendar_attendeedtos) {
        calendar_attendeeService.updateBatch(calendar_attendeeMapping.toDomain(calendar_attendeedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.calendar_attendeeService.get(#calendar_attendee_id),'iBizBusinessCentral-Calendar_attendee-Remove')")
    @ApiOperation(value = "删除日历出席者信息", tags = {"日历出席者信息" },  notes = "删除日历出席者信息")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_attendees/{calendar_attendee_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("calendar_attendee_id") Long calendar_attendee_id) {
         return ResponseEntity.status(HttpStatus.OK).body(calendar_attendeeService.remove(calendar_attendee_id));
    }

    @PreAuthorize("hasPermission(this.calendar_attendeeService.getCalendarAttendeeByIds(#ids),'iBizBusinessCentral-Calendar_attendee-Remove')")
    @ApiOperation(value = "批量删除日历出席者信息", tags = {"日历出席者信息" },  notes = "批量删除日历出席者信息")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_attendees/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        calendar_attendeeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.calendar_attendeeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Calendar_attendee-Get')")
    @ApiOperation(value = "获取日历出席者信息", tags = {"日历出席者信息" },  notes = "获取日历出席者信息")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_attendees/{calendar_attendee_id}")
    public ResponseEntity<Calendar_attendeeDTO> get(@PathVariable("calendar_attendee_id") Long calendar_attendee_id) {
        Calendar_attendee domain = calendar_attendeeService.get(calendar_attendee_id);
        Calendar_attendeeDTO dto = calendar_attendeeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取日历出席者信息草稿", tags = {"日历出席者信息" },  notes = "获取日历出席者信息草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_attendees/getdraft")
    public ResponseEntity<Calendar_attendeeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(calendar_attendeeMapping.toDto(calendar_attendeeService.getDraft(new Calendar_attendee())));
    }

    @ApiOperation(value = "检查日历出席者信息", tags = {"日历出席者信息" },  notes = "检查日历出席者信息")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_attendees/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Calendar_attendeeDTO calendar_attendeedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(calendar_attendeeService.checkKey(calendar_attendeeMapping.toDomain(calendar_attendeedto)));
    }

    @PreAuthorize("hasPermission(this.calendar_attendeeMapping.toDomain(#calendar_attendeedto),'iBizBusinessCentral-Calendar_attendee-Save')")
    @ApiOperation(value = "保存日历出席者信息", tags = {"日历出席者信息" },  notes = "保存日历出席者信息")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_attendees/save")
    public ResponseEntity<Boolean> save(@RequestBody Calendar_attendeeDTO calendar_attendeedto) {
        return ResponseEntity.status(HttpStatus.OK).body(calendar_attendeeService.save(calendar_attendeeMapping.toDomain(calendar_attendeedto)));
    }

    @PreAuthorize("hasPermission(this.calendar_attendeeMapping.toDomain(#calendar_attendeedtos),'iBizBusinessCentral-Calendar_attendee-Save')")
    @ApiOperation(value = "批量保存日历出席者信息", tags = {"日历出席者信息" },  notes = "批量保存日历出席者信息")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_attendees/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Calendar_attendeeDTO> calendar_attendeedtos) {
        calendar_attendeeService.saveBatch(calendar_attendeeMapping.toDomain(calendar_attendeedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_attendee-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Calendar_attendee-Get')")
	@ApiOperation(value = "获取数据集", tags = {"日历出席者信息" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_attendees/fetchdefault")
	public ResponseEntity<List<Calendar_attendeeDTO>> fetchDefault(Calendar_attendeeSearchContext context) {
        Page<Calendar_attendee> domains = calendar_attendeeService.searchDefault(context) ;
        List<Calendar_attendeeDTO> list = calendar_attendeeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_attendee-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Calendar_attendee-Get')")
	@ApiOperation(value = "查询数据集", tags = {"日历出席者信息" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/calendar_attendees/searchdefault")
	public ResponseEntity<Page<Calendar_attendeeDTO>> searchDefault(@RequestBody Calendar_attendeeSearchContext context) {
        Page<Calendar_attendee> domains = calendar_attendeeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(calendar_attendeeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

