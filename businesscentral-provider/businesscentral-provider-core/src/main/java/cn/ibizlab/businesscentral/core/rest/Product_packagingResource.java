package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_packaging;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_packagingService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_packagingSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品包装" })
@RestController("Core-product_packaging")
@RequestMapping("")
public class Product_packagingResource {

    @Autowired
    public IProduct_packagingService product_packagingService;

    @Autowired
    @Lazy
    public Product_packagingMapping product_packagingMapping;

    @PreAuthorize("hasPermission(this.product_packagingMapping.toDomain(#product_packagingdto),'iBizBusinessCentral-Product_packaging-Create')")
    @ApiOperation(value = "新建产品包装", tags = {"产品包装" },  notes = "新建产品包装")
	@RequestMapping(method = RequestMethod.POST, value = "/product_packagings")
    public ResponseEntity<Product_packagingDTO> create(@Validated @RequestBody Product_packagingDTO product_packagingdto) {
        Product_packaging domain = product_packagingMapping.toDomain(product_packagingdto);
		product_packagingService.create(domain);
        Product_packagingDTO dto = product_packagingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_packagingMapping.toDomain(#product_packagingdtos),'iBizBusinessCentral-Product_packaging-Create')")
    @ApiOperation(value = "批量新建产品包装", tags = {"产品包装" },  notes = "批量新建产品包装")
	@RequestMapping(method = RequestMethod.POST, value = "/product_packagings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_packagingDTO> product_packagingdtos) {
        product_packagingService.createBatch(product_packagingMapping.toDomain(product_packagingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_packaging" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_packagingService.get(#product_packaging_id),'iBizBusinessCentral-Product_packaging-Update')")
    @ApiOperation(value = "更新产品包装", tags = {"产品包装" },  notes = "更新产品包装")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_packagings/{product_packaging_id}")
    public ResponseEntity<Product_packagingDTO> update(@PathVariable("product_packaging_id") Long product_packaging_id, @RequestBody Product_packagingDTO product_packagingdto) {
		Product_packaging domain  = product_packagingMapping.toDomain(product_packagingdto);
        domain .setId(product_packaging_id);
		product_packagingService.update(domain );
		Product_packagingDTO dto = product_packagingMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_packagingService.getProductPackagingByEntities(this.product_packagingMapping.toDomain(#product_packagingdtos)),'iBizBusinessCentral-Product_packaging-Update')")
    @ApiOperation(value = "批量更新产品包装", tags = {"产品包装" },  notes = "批量更新产品包装")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_packagings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_packagingDTO> product_packagingdtos) {
        product_packagingService.updateBatch(product_packagingMapping.toDomain(product_packagingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_packagingService.get(#product_packaging_id),'iBizBusinessCentral-Product_packaging-Remove')")
    @ApiOperation(value = "删除产品包装", tags = {"产品包装" },  notes = "删除产品包装")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_packagings/{product_packaging_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_packaging_id") Long product_packaging_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_packagingService.remove(product_packaging_id));
    }

    @PreAuthorize("hasPermission(this.product_packagingService.getProductPackagingByIds(#ids),'iBizBusinessCentral-Product_packaging-Remove')")
    @ApiOperation(value = "批量删除产品包装", tags = {"产品包装" },  notes = "批量删除产品包装")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_packagings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_packagingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_packagingMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_packaging-Get')")
    @ApiOperation(value = "获取产品包装", tags = {"产品包装" },  notes = "获取产品包装")
	@RequestMapping(method = RequestMethod.GET, value = "/product_packagings/{product_packaging_id}")
    public ResponseEntity<Product_packagingDTO> get(@PathVariable("product_packaging_id") Long product_packaging_id) {
        Product_packaging domain = product_packagingService.get(product_packaging_id);
        Product_packagingDTO dto = product_packagingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品包装草稿", tags = {"产品包装" },  notes = "获取产品包装草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_packagings/getdraft")
    public ResponseEntity<Product_packagingDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_packagingMapping.toDto(product_packagingService.getDraft(new Product_packaging())));
    }

    @ApiOperation(value = "检查产品包装", tags = {"产品包装" },  notes = "检查产品包装")
	@RequestMapping(method = RequestMethod.POST, value = "/product_packagings/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_packagingDTO product_packagingdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_packagingService.checkKey(product_packagingMapping.toDomain(product_packagingdto)));
    }

    @PreAuthorize("hasPermission(this.product_packagingMapping.toDomain(#product_packagingdto),'iBizBusinessCentral-Product_packaging-Save')")
    @ApiOperation(value = "保存产品包装", tags = {"产品包装" },  notes = "保存产品包装")
	@RequestMapping(method = RequestMethod.POST, value = "/product_packagings/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_packagingDTO product_packagingdto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_packagingService.save(product_packagingMapping.toDomain(product_packagingdto)));
    }

    @PreAuthorize("hasPermission(this.product_packagingMapping.toDomain(#product_packagingdtos),'iBizBusinessCentral-Product_packaging-Save')")
    @ApiOperation(value = "批量保存产品包装", tags = {"产品包装" },  notes = "批量保存产品包装")
	@RequestMapping(method = RequestMethod.POST, value = "/product_packagings/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_packagingDTO> product_packagingdtos) {
        product_packagingService.saveBatch(product_packagingMapping.toDomain(product_packagingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_packaging-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_packaging-Get')")
	@ApiOperation(value = "获取数据集", tags = {"产品包装" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_packagings/fetchdefault")
	public ResponseEntity<List<Product_packagingDTO>> fetchDefault(Product_packagingSearchContext context) {
        Page<Product_packaging> domains = product_packagingService.searchDefault(context) ;
        List<Product_packagingDTO> list = product_packagingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_packaging-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_packaging-Get')")
	@ApiOperation(value = "查询数据集", tags = {"产品包装" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_packagings/searchdefault")
	public ResponseEntity<Page<Product_packagingDTO>> searchDefault(@RequestBody Product_packagingSearchContext context) {
        Page<Product_packaging> domains = product_packagingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_packagingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

