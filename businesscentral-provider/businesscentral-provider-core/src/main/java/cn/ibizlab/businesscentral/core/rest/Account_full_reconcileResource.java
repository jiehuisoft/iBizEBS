package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_full_reconcile;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_full_reconcileService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_full_reconcileSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"完全核销" })
@RestController("Core-account_full_reconcile")
@RequestMapping("")
public class Account_full_reconcileResource {

    @Autowired
    public IAccount_full_reconcileService account_full_reconcileService;

    @Autowired
    @Lazy
    public Account_full_reconcileMapping account_full_reconcileMapping;

    @PreAuthorize("hasPermission(this.account_full_reconcileMapping.toDomain(#account_full_reconciledto),'iBizBusinessCentral-Account_full_reconcile-Create')")
    @ApiOperation(value = "新建完全核销", tags = {"完全核销" },  notes = "新建完全核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles")
    public ResponseEntity<Account_full_reconcileDTO> create(@Validated @RequestBody Account_full_reconcileDTO account_full_reconciledto) {
        Account_full_reconcile domain = account_full_reconcileMapping.toDomain(account_full_reconciledto);
		account_full_reconcileService.create(domain);
        Account_full_reconcileDTO dto = account_full_reconcileMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_full_reconcileMapping.toDomain(#account_full_reconciledtos),'iBizBusinessCentral-Account_full_reconcile-Create')")
    @ApiOperation(value = "批量新建完全核销", tags = {"完全核销" },  notes = "批量新建完全核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_full_reconcileDTO> account_full_reconciledtos) {
        account_full_reconcileService.createBatch(account_full_reconcileMapping.toDomain(account_full_reconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_full_reconcile" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_full_reconcileService.get(#account_full_reconcile_id),'iBizBusinessCentral-Account_full_reconcile-Update')")
    @ApiOperation(value = "更新完全核销", tags = {"完全核销" },  notes = "更新完全核销")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_full_reconciles/{account_full_reconcile_id}")
    public ResponseEntity<Account_full_reconcileDTO> update(@PathVariable("account_full_reconcile_id") Long account_full_reconcile_id, @RequestBody Account_full_reconcileDTO account_full_reconciledto) {
		Account_full_reconcile domain  = account_full_reconcileMapping.toDomain(account_full_reconciledto);
        domain .setId(account_full_reconcile_id);
		account_full_reconcileService.update(domain );
		Account_full_reconcileDTO dto = account_full_reconcileMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_full_reconcileService.getAccountFullReconcileByEntities(this.account_full_reconcileMapping.toDomain(#account_full_reconciledtos)),'iBizBusinessCentral-Account_full_reconcile-Update')")
    @ApiOperation(value = "批量更新完全核销", tags = {"完全核销" },  notes = "批量更新完全核销")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_full_reconciles/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_full_reconcileDTO> account_full_reconciledtos) {
        account_full_reconcileService.updateBatch(account_full_reconcileMapping.toDomain(account_full_reconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_full_reconcileService.get(#account_full_reconcile_id),'iBizBusinessCentral-Account_full_reconcile-Remove')")
    @ApiOperation(value = "删除完全核销", tags = {"完全核销" },  notes = "删除完全核销")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_full_reconciles/{account_full_reconcile_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_full_reconcile_id") Long account_full_reconcile_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_full_reconcileService.remove(account_full_reconcile_id));
    }

    @PreAuthorize("hasPermission(this.account_full_reconcileService.getAccountFullReconcileByIds(#ids),'iBizBusinessCentral-Account_full_reconcile-Remove')")
    @ApiOperation(value = "批量删除完全核销", tags = {"完全核销" },  notes = "批量删除完全核销")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_full_reconciles/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_full_reconcileService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_full_reconcileMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_full_reconcile-Get')")
    @ApiOperation(value = "获取完全核销", tags = {"完全核销" },  notes = "获取完全核销")
	@RequestMapping(method = RequestMethod.GET, value = "/account_full_reconciles/{account_full_reconcile_id}")
    public ResponseEntity<Account_full_reconcileDTO> get(@PathVariable("account_full_reconcile_id") Long account_full_reconcile_id) {
        Account_full_reconcile domain = account_full_reconcileService.get(account_full_reconcile_id);
        Account_full_reconcileDTO dto = account_full_reconcileMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取完全核销草稿", tags = {"完全核销" },  notes = "获取完全核销草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_full_reconciles/getdraft")
    public ResponseEntity<Account_full_reconcileDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_full_reconcileMapping.toDto(account_full_reconcileService.getDraft(new Account_full_reconcile())));
    }

    @ApiOperation(value = "检查完全核销", tags = {"完全核销" },  notes = "检查完全核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_full_reconcileDTO account_full_reconciledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_full_reconcileService.checkKey(account_full_reconcileMapping.toDomain(account_full_reconciledto)));
    }

    @PreAuthorize("hasPermission(this.account_full_reconcileMapping.toDomain(#account_full_reconciledto),'iBizBusinessCentral-Account_full_reconcile-Save')")
    @ApiOperation(value = "保存完全核销", tags = {"完全核销" },  notes = "保存完全核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_full_reconcileDTO account_full_reconciledto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_full_reconcileService.save(account_full_reconcileMapping.toDomain(account_full_reconciledto)));
    }

    @PreAuthorize("hasPermission(this.account_full_reconcileMapping.toDomain(#account_full_reconciledtos),'iBizBusinessCentral-Account_full_reconcile-Save')")
    @ApiOperation(value = "批量保存完全核销", tags = {"完全核销" },  notes = "批量保存完全核销")
	@RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_full_reconcileDTO> account_full_reconciledtos) {
        account_full_reconcileService.saveBatch(account_full_reconcileMapping.toDomain(account_full_reconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_full_reconcile-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_full_reconcile-Get')")
	@ApiOperation(value = "获取数据集", tags = {"完全核销" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_full_reconciles/fetchdefault")
	public ResponseEntity<List<Account_full_reconcileDTO>> fetchDefault(Account_full_reconcileSearchContext context) {
        Page<Account_full_reconcile> domains = account_full_reconcileService.searchDefault(context) ;
        List<Account_full_reconcileDTO> list = account_full_reconcileMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_full_reconcile-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_full_reconcile-Get')")
	@ApiOperation(value = "查询数据集", tags = {"完全核销" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_full_reconciles/searchdefault")
	public ResponseEntity<Page<Account_full_reconcileDTO>> searchDefault(@RequestBody Account_full_reconcileSearchContext context) {
        Page<Account_full_reconcile> domains = account_full_reconcileService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_full_reconcileMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

