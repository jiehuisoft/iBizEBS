package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position_tax;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_position_taxService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_fiscal_position_taxSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"税率的科目调整" })
@RestController("Core-account_fiscal_position_tax")
@RequestMapping("")
public class Account_fiscal_position_taxResource {

    @Autowired
    public IAccount_fiscal_position_taxService account_fiscal_position_taxService;

    @Autowired
    @Lazy
    public Account_fiscal_position_taxMapping account_fiscal_position_taxMapping;

    @PreAuthorize("hasPermission(this.account_fiscal_position_taxMapping.toDomain(#account_fiscal_position_taxdto),'iBizBusinessCentral-Account_fiscal_position_tax-Create')")
    @ApiOperation(value = "新建税率的科目调整", tags = {"税率的科目调整" },  notes = "新建税率的科目调整")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes")
    public ResponseEntity<Account_fiscal_position_taxDTO> create(@Validated @RequestBody Account_fiscal_position_taxDTO account_fiscal_position_taxdto) {
        Account_fiscal_position_tax domain = account_fiscal_position_taxMapping.toDomain(account_fiscal_position_taxdto);
		account_fiscal_position_taxService.create(domain);
        Account_fiscal_position_taxDTO dto = account_fiscal_position_taxMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_fiscal_position_taxMapping.toDomain(#account_fiscal_position_taxdtos),'iBizBusinessCentral-Account_fiscal_position_tax-Create')")
    @ApiOperation(value = "批量新建税率的科目调整", tags = {"税率的科目调整" },  notes = "批量新建税率的科目调整")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_fiscal_position_taxDTO> account_fiscal_position_taxdtos) {
        account_fiscal_position_taxService.createBatch(account_fiscal_position_taxMapping.toDomain(account_fiscal_position_taxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_fiscal_position_tax" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_fiscal_position_taxService.get(#account_fiscal_position_tax_id),'iBizBusinessCentral-Account_fiscal_position_tax-Update')")
    @ApiOperation(value = "更新税率的科目调整", tags = {"税率的科目调整" },  notes = "更新税率的科目调整")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_position_taxes/{account_fiscal_position_tax_id}")
    public ResponseEntity<Account_fiscal_position_taxDTO> update(@PathVariable("account_fiscal_position_tax_id") Long account_fiscal_position_tax_id, @RequestBody Account_fiscal_position_taxDTO account_fiscal_position_taxdto) {
		Account_fiscal_position_tax domain  = account_fiscal_position_taxMapping.toDomain(account_fiscal_position_taxdto);
        domain .setId(account_fiscal_position_tax_id);
		account_fiscal_position_taxService.update(domain );
		Account_fiscal_position_taxDTO dto = account_fiscal_position_taxMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_fiscal_position_taxService.getAccountFiscalPositionTaxByEntities(this.account_fiscal_position_taxMapping.toDomain(#account_fiscal_position_taxdtos)),'iBizBusinessCentral-Account_fiscal_position_tax-Update')")
    @ApiOperation(value = "批量更新税率的科目调整", tags = {"税率的科目调整" },  notes = "批量更新税率的科目调整")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_position_taxes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_fiscal_position_taxDTO> account_fiscal_position_taxdtos) {
        account_fiscal_position_taxService.updateBatch(account_fiscal_position_taxMapping.toDomain(account_fiscal_position_taxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_fiscal_position_taxService.get(#account_fiscal_position_tax_id),'iBizBusinessCentral-Account_fiscal_position_tax-Remove')")
    @ApiOperation(value = "删除税率的科目调整", tags = {"税率的科目调整" },  notes = "删除税率的科目调整")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_position_taxes/{account_fiscal_position_tax_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_fiscal_position_tax_id") Long account_fiscal_position_tax_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_position_taxService.remove(account_fiscal_position_tax_id));
    }

    @PreAuthorize("hasPermission(this.account_fiscal_position_taxService.getAccountFiscalPositionTaxByIds(#ids),'iBizBusinessCentral-Account_fiscal_position_tax-Remove')")
    @ApiOperation(value = "批量删除税率的科目调整", tags = {"税率的科目调整" },  notes = "批量删除税率的科目调整")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_position_taxes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_fiscal_position_taxService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_fiscal_position_taxMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_fiscal_position_tax-Get')")
    @ApiOperation(value = "获取税率的科目调整", tags = {"税率的科目调整" },  notes = "获取税率的科目调整")
	@RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_taxes/{account_fiscal_position_tax_id}")
    public ResponseEntity<Account_fiscal_position_taxDTO> get(@PathVariable("account_fiscal_position_tax_id") Long account_fiscal_position_tax_id) {
        Account_fiscal_position_tax domain = account_fiscal_position_taxService.get(account_fiscal_position_tax_id);
        Account_fiscal_position_taxDTO dto = account_fiscal_position_taxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取税率的科目调整草稿", tags = {"税率的科目调整" },  notes = "获取税率的科目调整草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_taxes/getdraft")
    public ResponseEntity<Account_fiscal_position_taxDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_position_taxMapping.toDto(account_fiscal_position_taxService.getDraft(new Account_fiscal_position_tax())));
    }

    @ApiOperation(value = "检查税率的科目调整", tags = {"税率的科目调整" },  notes = "检查税率的科目调整")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_fiscal_position_taxDTO account_fiscal_position_taxdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_fiscal_position_taxService.checkKey(account_fiscal_position_taxMapping.toDomain(account_fiscal_position_taxdto)));
    }

    @PreAuthorize("hasPermission(this.account_fiscal_position_taxMapping.toDomain(#account_fiscal_position_taxdto),'iBizBusinessCentral-Account_fiscal_position_tax-Save')")
    @ApiOperation(value = "保存税率的科目调整", tags = {"税率的科目调整" },  notes = "保存税率的科目调整")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_fiscal_position_taxDTO account_fiscal_position_taxdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_position_taxService.save(account_fiscal_position_taxMapping.toDomain(account_fiscal_position_taxdto)));
    }

    @PreAuthorize("hasPermission(this.account_fiscal_position_taxMapping.toDomain(#account_fiscal_position_taxdtos),'iBizBusinessCentral-Account_fiscal_position_tax-Save')")
    @ApiOperation(value = "批量保存税率的科目调整", tags = {"税率的科目调整" },  notes = "批量保存税率的科目调整")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_fiscal_position_taxDTO> account_fiscal_position_taxdtos) {
        account_fiscal_position_taxService.saveBatch(account_fiscal_position_taxMapping.toDomain(account_fiscal_position_taxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_fiscal_position_tax-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_fiscal_position_tax-Get')")
	@ApiOperation(value = "获取数据集", tags = {"税率的科目调整" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_fiscal_position_taxes/fetchdefault")
	public ResponseEntity<List<Account_fiscal_position_taxDTO>> fetchDefault(Account_fiscal_position_taxSearchContext context) {
        Page<Account_fiscal_position_tax> domains = account_fiscal_position_taxService.searchDefault(context) ;
        List<Account_fiscal_position_taxDTO> list = account_fiscal_position_taxMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_fiscal_position_tax-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_fiscal_position_tax-Get')")
	@ApiOperation(value = "查询数据集", tags = {"税率的科目调整" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_fiscal_position_taxes/searchdefault")
	public ResponseEntity<Page<Account_fiscal_position_taxDTO>> searchDefault(@RequestBody Account_fiscal_position_taxSearchContext context) {
        Page<Account_fiscal_position_tax> domains = account_fiscal_position_taxService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_fiscal_position_taxMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

