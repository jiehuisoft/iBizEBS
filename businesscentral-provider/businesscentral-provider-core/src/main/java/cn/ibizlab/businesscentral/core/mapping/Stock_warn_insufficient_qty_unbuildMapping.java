package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild;
import cn.ibizlab.businesscentral.core.dto.Stock_warn_insufficient_qty_unbuildDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreStock_warn_insufficient_qty_unbuildMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_warn_insufficient_qty_unbuildMapping extends MappingBase<Stock_warn_insufficient_qty_unbuildDTO, Stock_warn_insufficient_qty_unbuild> {


}

