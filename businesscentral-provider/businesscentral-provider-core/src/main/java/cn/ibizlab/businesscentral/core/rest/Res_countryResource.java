package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_countryService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_countrySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"国家/地区" })
@RestController("Core-res_country")
@RequestMapping("")
public class Res_countryResource {

    @Autowired
    public IRes_countryService res_countryService;

    @Autowired
    @Lazy
    public Res_countryMapping res_countryMapping;

    @PreAuthorize("hasPermission(this.res_countryMapping.toDomain(#res_countrydto),'iBizBusinessCentral-Res_country-Create')")
    @ApiOperation(value = "新建国家/地区", tags = {"国家/地区" },  notes = "新建国家/地区")
	@RequestMapping(method = RequestMethod.POST, value = "/res_countries")
    public ResponseEntity<Res_countryDTO> create(@Validated @RequestBody Res_countryDTO res_countrydto) {
        Res_country domain = res_countryMapping.toDomain(res_countrydto);
		res_countryService.create(domain);
        Res_countryDTO dto = res_countryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_countryMapping.toDomain(#res_countrydtos),'iBizBusinessCentral-Res_country-Create')")
    @ApiOperation(value = "批量新建国家/地区", tags = {"国家/地区" },  notes = "批量新建国家/地区")
	@RequestMapping(method = RequestMethod.POST, value = "/res_countries/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_countryDTO> res_countrydtos) {
        res_countryService.createBatch(res_countryMapping.toDomain(res_countrydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_country" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_countryService.get(#res_country_id),'iBizBusinessCentral-Res_country-Update')")
    @ApiOperation(value = "更新国家/地区", tags = {"国家/地区" },  notes = "更新国家/地区")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_countries/{res_country_id}")
    public ResponseEntity<Res_countryDTO> update(@PathVariable("res_country_id") Long res_country_id, @RequestBody Res_countryDTO res_countrydto) {
		Res_country domain  = res_countryMapping.toDomain(res_countrydto);
        domain .setId(res_country_id);
		res_countryService.update(domain );
		Res_countryDTO dto = res_countryMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_countryService.getResCountryByEntities(this.res_countryMapping.toDomain(#res_countrydtos)),'iBizBusinessCentral-Res_country-Update')")
    @ApiOperation(value = "批量更新国家/地区", tags = {"国家/地区" },  notes = "批量更新国家/地区")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_countries/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_countryDTO> res_countrydtos) {
        res_countryService.updateBatch(res_countryMapping.toDomain(res_countrydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_countryService.get(#res_country_id),'iBizBusinessCentral-Res_country-Remove')")
    @ApiOperation(value = "删除国家/地区", tags = {"国家/地区" },  notes = "删除国家/地区")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_countries/{res_country_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_country_id") Long res_country_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_countryService.remove(res_country_id));
    }

    @PreAuthorize("hasPermission(this.res_countryService.getResCountryByIds(#ids),'iBizBusinessCentral-Res_country-Remove')")
    @ApiOperation(value = "批量删除国家/地区", tags = {"国家/地区" },  notes = "批量删除国家/地区")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_countries/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_countryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_countryMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_country-Get')")
    @ApiOperation(value = "获取国家/地区", tags = {"国家/地区" },  notes = "获取国家/地区")
	@RequestMapping(method = RequestMethod.GET, value = "/res_countries/{res_country_id}")
    public ResponseEntity<Res_countryDTO> get(@PathVariable("res_country_id") Long res_country_id) {
        Res_country domain = res_countryService.get(res_country_id);
        Res_countryDTO dto = res_countryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取国家/地区草稿", tags = {"国家/地区" },  notes = "获取国家/地区草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_countries/getdraft")
    public ResponseEntity<Res_countryDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_countryMapping.toDto(res_countryService.getDraft(new Res_country())));
    }

    @ApiOperation(value = "检查国家/地区", tags = {"国家/地区" },  notes = "检查国家/地区")
	@RequestMapping(method = RequestMethod.POST, value = "/res_countries/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_countryDTO res_countrydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_countryService.checkKey(res_countryMapping.toDomain(res_countrydto)));
    }

    @PreAuthorize("hasPermission(this.res_countryMapping.toDomain(#res_countrydto),'iBizBusinessCentral-Res_country-Save')")
    @ApiOperation(value = "保存国家/地区", tags = {"国家/地区" },  notes = "保存国家/地区")
	@RequestMapping(method = RequestMethod.POST, value = "/res_countries/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_countryDTO res_countrydto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_countryService.save(res_countryMapping.toDomain(res_countrydto)));
    }

    @PreAuthorize("hasPermission(this.res_countryMapping.toDomain(#res_countrydtos),'iBizBusinessCentral-Res_country-Save')")
    @ApiOperation(value = "批量保存国家/地区", tags = {"国家/地区" },  notes = "批量保存国家/地区")
	@RequestMapping(method = RequestMethod.POST, value = "/res_countries/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_countryDTO> res_countrydtos) {
        res_countryService.saveBatch(res_countryMapping.toDomain(res_countrydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_country-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_country-Get')")
	@ApiOperation(value = "获取数据集", tags = {"国家/地区" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_countries/fetchdefault")
	public ResponseEntity<List<Res_countryDTO>> fetchDefault(Res_countrySearchContext context) {
        Page<Res_country> domains = res_countryService.searchDefault(context) ;
        List<Res_countryDTO> list = res_countryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_country-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_country-Get')")
	@ApiOperation(value = "查询数据集", tags = {"国家/地区" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_countries/searchdefault")
	public ResponseEntity<Page<Res_countryDTO>> searchDefault(@RequestBody Res_countrySearchContext context) {
        Page<Res_country> domains = res_countryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_countryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

