package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_wizard_invite;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_wizard_inviteService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_wizard_inviteSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"邀请向导" })
@RestController("Core-mail_wizard_invite")
@RequestMapping("")
public class Mail_wizard_inviteResource {

    @Autowired
    public IMail_wizard_inviteService mail_wizard_inviteService;

    @Autowired
    @Lazy
    public Mail_wizard_inviteMapping mail_wizard_inviteMapping;

    @PreAuthorize("hasPermission(this.mail_wizard_inviteMapping.toDomain(#mail_wizard_invitedto),'iBizBusinessCentral-Mail_wizard_invite-Create')")
    @ApiOperation(value = "新建邀请向导", tags = {"邀请向导" },  notes = "新建邀请向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_wizard_invites")
    public ResponseEntity<Mail_wizard_inviteDTO> create(@Validated @RequestBody Mail_wizard_inviteDTO mail_wizard_invitedto) {
        Mail_wizard_invite domain = mail_wizard_inviteMapping.toDomain(mail_wizard_invitedto);
		mail_wizard_inviteService.create(domain);
        Mail_wizard_inviteDTO dto = mail_wizard_inviteMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_wizard_inviteMapping.toDomain(#mail_wizard_invitedtos),'iBizBusinessCentral-Mail_wizard_invite-Create')")
    @ApiOperation(value = "批量新建邀请向导", tags = {"邀请向导" },  notes = "批量新建邀请向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_wizard_invites/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_wizard_inviteDTO> mail_wizard_invitedtos) {
        mail_wizard_inviteService.createBatch(mail_wizard_inviteMapping.toDomain(mail_wizard_invitedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_wizard_invite" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_wizard_inviteService.get(#mail_wizard_invite_id),'iBizBusinessCentral-Mail_wizard_invite-Update')")
    @ApiOperation(value = "更新邀请向导", tags = {"邀请向导" },  notes = "更新邀请向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_wizard_invites/{mail_wizard_invite_id}")
    public ResponseEntity<Mail_wizard_inviteDTO> update(@PathVariable("mail_wizard_invite_id") Long mail_wizard_invite_id, @RequestBody Mail_wizard_inviteDTO mail_wizard_invitedto) {
		Mail_wizard_invite domain  = mail_wizard_inviteMapping.toDomain(mail_wizard_invitedto);
        domain .setId(mail_wizard_invite_id);
		mail_wizard_inviteService.update(domain );
		Mail_wizard_inviteDTO dto = mail_wizard_inviteMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_wizard_inviteService.getMailWizardInviteByEntities(this.mail_wizard_inviteMapping.toDomain(#mail_wizard_invitedtos)),'iBizBusinessCentral-Mail_wizard_invite-Update')")
    @ApiOperation(value = "批量更新邀请向导", tags = {"邀请向导" },  notes = "批量更新邀请向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_wizard_invites/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_wizard_inviteDTO> mail_wizard_invitedtos) {
        mail_wizard_inviteService.updateBatch(mail_wizard_inviteMapping.toDomain(mail_wizard_invitedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_wizard_inviteService.get(#mail_wizard_invite_id),'iBizBusinessCentral-Mail_wizard_invite-Remove')")
    @ApiOperation(value = "删除邀请向导", tags = {"邀请向导" },  notes = "删除邀请向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_wizard_invites/{mail_wizard_invite_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_wizard_invite_id") Long mail_wizard_invite_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_wizard_inviteService.remove(mail_wizard_invite_id));
    }

    @PreAuthorize("hasPermission(this.mail_wizard_inviteService.getMailWizardInviteByIds(#ids),'iBizBusinessCentral-Mail_wizard_invite-Remove')")
    @ApiOperation(value = "批量删除邀请向导", tags = {"邀请向导" },  notes = "批量删除邀请向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_wizard_invites/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_wizard_inviteService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_wizard_inviteMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_wizard_invite-Get')")
    @ApiOperation(value = "获取邀请向导", tags = {"邀请向导" },  notes = "获取邀请向导")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_wizard_invites/{mail_wizard_invite_id}")
    public ResponseEntity<Mail_wizard_inviteDTO> get(@PathVariable("mail_wizard_invite_id") Long mail_wizard_invite_id) {
        Mail_wizard_invite domain = mail_wizard_inviteService.get(mail_wizard_invite_id);
        Mail_wizard_inviteDTO dto = mail_wizard_inviteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取邀请向导草稿", tags = {"邀请向导" },  notes = "获取邀请向导草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_wizard_invites/getdraft")
    public ResponseEntity<Mail_wizard_inviteDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_wizard_inviteMapping.toDto(mail_wizard_inviteService.getDraft(new Mail_wizard_invite())));
    }

    @ApiOperation(value = "检查邀请向导", tags = {"邀请向导" },  notes = "检查邀请向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_wizard_invites/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_wizard_inviteDTO mail_wizard_invitedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_wizard_inviteService.checkKey(mail_wizard_inviteMapping.toDomain(mail_wizard_invitedto)));
    }

    @PreAuthorize("hasPermission(this.mail_wizard_inviteMapping.toDomain(#mail_wizard_invitedto),'iBizBusinessCentral-Mail_wizard_invite-Save')")
    @ApiOperation(value = "保存邀请向导", tags = {"邀请向导" },  notes = "保存邀请向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_wizard_invites/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_wizard_inviteDTO mail_wizard_invitedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_wizard_inviteService.save(mail_wizard_inviteMapping.toDomain(mail_wizard_invitedto)));
    }

    @PreAuthorize("hasPermission(this.mail_wizard_inviteMapping.toDomain(#mail_wizard_invitedtos),'iBizBusinessCentral-Mail_wizard_invite-Save')")
    @ApiOperation(value = "批量保存邀请向导", tags = {"邀请向导" },  notes = "批量保存邀请向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_wizard_invites/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_wizard_inviteDTO> mail_wizard_invitedtos) {
        mail_wizard_inviteService.saveBatch(mail_wizard_inviteMapping.toDomain(mail_wizard_invitedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_wizard_invite-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_wizard_invite-Get')")
	@ApiOperation(value = "获取数据集", tags = {"邀请向导" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_wizard_invites/fetchdefault")
	public ResponseEntity<List<Mail_wizard_inviteDTO>> fetchDefault(Mail_wizard_inviteSearchContext context) {
        Page<Mail_wizard_invite> domains = mail_wizard_inviteService.searchDefault(context) ;
        List<Mail_wizard_inviteDTO> list = mail_wizard_inviteMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_wizard_invite-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_wizard_invite-Get')")
	@ApiOperation(value = "查询数据集", tags = {"邀请向导" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_wizard_invites/searchdefault")
	public ResponseEntity<Page<Mail_wizard_inviteDTO>> searchDefault(@RequestBody Mail_wizard_inviteSearchContext context) {
        Page<Mail_wizard_invite> domains = mail_wizard_inviteService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_wizard_inviteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

