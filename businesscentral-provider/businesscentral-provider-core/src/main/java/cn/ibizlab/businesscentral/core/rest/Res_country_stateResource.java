package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_state;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_country_stateService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_country_stateSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"国家/地区州/省" })
@RestController("Core-res_country_state")
@RequestMapping("")
public class Res_country_stateResource {

    @Autowired
    public IRes_country_stateService res_country_stateService;

    @Autowired
    @Lazy
    public Res_country_stateMapping res_country_stateMapping;

    @PreAuthorize("hasPermission(this.res_country_stateMapping.toDomain(#res_country_statedto),'iBizBusinessCentral-Res_country_state-Create')")
    @ApiOperation(value = "新建国家/地区州/省", tags = {"国家/地区州/省" },  notes = "新建国家/地区州/省")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_states")
    public ResponseEntity<Res_country_stateDTO> create(@Validated @RequestBody Res_country_stateDTO res_country_statedto) {
        Res_country_state domain = res_country_stateMapping.toDomain(res_country_statedto);
		res_country_stateService.create(domain);
        Res_country_stateDTO dto = res_country_stateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_country_stateMapping.toDomain(#res_country_statedtos),'iBizBusinessCentral-Res_country_state-Create')")
    @ApiOperation(value = "批量新建国家/地区州/省", tags = {"国家/地区州/省" },  notes = "批量新建国家/地区州/省")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_states/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_country_stateDTO> res_country_statedtos) {
        res_country_stateService.createBatch(res_country_stateMapping.toDomain(res_country_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_country_state" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_country_stateService.get(#res_country_state_id),'iBizBusinessCentral-Res_country_state-Update')")
    @ApiOperation(value = "更新国家/地区州/省", tags = {"国家/地区州/省" },  notes = "更新国家/地区州/省")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_country_states/{res_country_state_id}")
    public ResponseEntity<Res_country_stateDTO> update(@PathVariable("res_country_state_id") Long res_country_state_id, @RequestBody Res_country_stateDTO res_country_statedto) {
		Res_country_state domain  = res_country_stateMapping.toDomain(res_country_statedto);
        domain .setId(res_country_state_id);
		res_country_stateService.update(domain );
		Res_country_stateDTO dto = res_country_stateMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_country_stateService.getResCountryStateByEntities(this.res_country_stateMapping.toDomain(#res_country_statedtos)),'iBizBusinessCentral-Res_country_state-Update')")
    @ApiOperation(value = "批量更新国家/地区州/省", tags = {"国家/地区州/省" },  notes = "批量更新国家/地区州/省")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_country_states/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_country_stateDTO> res_country_statedtos) {
        res_country_stateService.updateBatch(res_country_stateMapping.toDomain(res_country_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_country_stateService.get(#res_country_state_id),'iBizBusinessCentral-Res_country_state-Remove')")
    @ApiOperation(value = "删除国家/地区州/省", tags = {"国家/地区州/省" },  notes = "删除国家/地区州/省")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_country_states/{res_country_state_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_country_state_id") Long res_country_state_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_country_stateService.remove(res_country_state_id));
    }

    @PreAuthorize("hasPermission(this.res_country_stateService.getResCountryStateByIds(#ids),'iBizBusinessCentral-Res_country_state-Remove')")
    @ApiOperation(value = "批量删除国家/地区州/省", tags = {"国家/地区州/省" },  notes = "批量删除国家/地区州/省")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_country_states/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_country_stateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_country_stateMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_country_state-Get')")
    @ApiOperation(value = "获取国家/地区州/省", tags = {"国家/地区州/省" },  notes = "获取国家/地区州/省")
	@RequestMapping(method = RequestMethod.GET, value = "/res_country_states/{res_country_state_id}")
    public ResponseEntity<Res_country_stateDTO> get(@PathVariable("res_country_state_id") Long res_country_state_id) {
        Res_country_state domain = res_country_stateService.get(res_country_state_id);
        Res_country_stateDTO dto = res_country_stateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取国家/地区州/省草稿", tags = {"国家/地区州/省" },  notes = "获取国家/地区州/省草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_country_states/getdraft")
    public ResponseEntity<Res_country_stateDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_country_stateMapping.toDto(res_country_stateService.getDraft(new Res_country_state())));
    }

    @ApiOperation(value = "检查国家/地区州/省", tags = {"国家/地区州/省" },  notes = "检查国家/地区州/省")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_states/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_country_stateDTO res_country_statedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_country_stateService.checkKey(res_country_stateMapping.toDomain(res_country_statedto)));
    }

    @PreAuthorize("hasPermission(this.res_country_stateMapping.toDomain(#res_country_statedto),'iBizBusinessCentral-Res_country_state-Save')")
    @ApiOperation(value = "保存国家/地区州/省", tags = {"国家/地区州/省" },  notes = "保存国家/地区州/省")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_states/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_country_stateDTO res_country_statedto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_country_stateService.save(res_country_stateMapping.toDomain(res_country_statedto)));
    }

    @PreAuthorize("hasPermission(this.res_country_stateMapping.toDomain(#res_country_statedtos),'iBizBusinessCentral-Res_country_state-Save')")
    @ApiOperation(value = "批量保存国家/地区州/省", tags = {"国家/地区州/省" },  notes = "批量保存国家/地区州/省")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_states/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_country_stateDTO> res_country_statedtos) {
        res_country_stateService.saveBatch(res_country_stateMapping.toDomain(res_country_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_country_state-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_country_state-Get')")
	@ApiOperation(value = "获取数据集", tags = {"国家/地区州/省" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_country_states/fetchdefault")
	public ResponseEntity<List<Res_country_stateDTO>> fetchDefault(Res_country_stateSearchContext context) {
        Page<Res_country_state> domains = res_country_stateService.searchDefault(context) ;
        List<Res_country_stateDTO> list = res_country_stateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_country_state-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_country_state-Get')")
	@ApiOperation(value = "查询数据集", tags = {"国家/地区州/省" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_country_states/searchdefault")
	public ResponseEntity<Page<Res_country_stateDTO>> searchDefault(@RequestBody Res_country_stateSearchContext context) {
        Page<Res_country_state> domains = res_country_stateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_country_stateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

