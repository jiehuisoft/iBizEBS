package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_char;
import cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_charService;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_charSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"测试:基本导入模型，字符" })
@RestController("Core-base_import_tests_models_char")
@RequestMapping("")
public class Base_import_tests_models_charResource {

    @Autowired
    public IBase_import_tests_models_charService base_import_tests_models_charService;

    @Autowired
    @Lazy
    public Base_import_tests_models_charMapping base_import_tests_models_charMapping;

    @PreAuthorize("hasPermission(this.base_import_tests_models_charMapping.toDomain(#base_import_tests_models_chardto),'iBizBusinessCentral-Base_import_tests_models_char-Create')")
    @ApiOperation(value = "新建测试:基本导入模型，字符", tags = {"测试:基本导入模型，字符" },  notes = "新建测试:基本导入模型，字符")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_chars")
    public ResponseEntity<Base_import_tests_models_charDTO> create(@Validated @RequestBody Base_import_tests_models_charDTO base_import_tests_models_chardto) {
        Base_import_tests_models_char domain = base_import_tests_models_charMapping.toDomain(base_import_tests_models_chardto);
		base_import_tests_models_charService.create(domain);
        Base_import_tests_models_charDTO dto = base_import_tests_models_charMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_charMapping.toDomain(#base_import_tests_models_chardtos),'iBizBusinessCentral-Base_import_tests_models_char-Create')")
    @ApiOperation(value = "批量新建测试:基本导入模型，字符", tags = {"测试:基本导入模型，字符" },  notes = "批量新建测试:基本导入模型，字符")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_chars/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_charDTO> base_import_tests_models_chardtos) {
        base_import_tests_models_charService.createBatch(base_import_tests_models_charMapping.toDomain(base_import_tests_models_chardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_import_tests_models_char" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_import_tests_models_charService.get(#base_import_tests_models_char_id),'iBizBusinessCentral-Base_import_tests_models_char-Update')")
    @ApiOperation(value = "更新测试:基本导入模型，字符", tags = {"测试:基本导入模型，字符" },  notes = "更新测试:基本导入模型，字符")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_chars/{base_import_tests_models_char_id}")
    public ResponseEntity<Base_import_tests_models_charDTO> update(@PathVariable("base_import_tests_models_char_id") Long base_import_tests_models_char_id, @RequestBody Base_import_tests_models_charDTO base_import_tests_models_chardto) {
		Base_import_tests_models_char domain  = base_import_tests_models_charMapping.toDomain(base_import_tests_models_chardto);
        domain .setId(base_import_tests_models_char_id);
		base_import_tests_models_charService.update(domain );
		Base_import_tests_models_charDTO dto = base_import_tests_models_charMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_charService.getBaseImportTestsModelsCharByEntities(this.base_import_tests_models_charMapping.toDomain(#base_import_tests_models_chardtos)),'iBizBusinessCentral-Base_import_tests_models_char-Update')")
    @ApiOperation(value = "批量更新测试:基本导入模型，字符", tags = {"测试:基本导入模型，字符" },  notes = "批量更新测试:基本导入模型，字符")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_chars/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_charDTO> base_import_tests_models_chardtos) {
        base_import_tests_models_charService.updateBatch(base_import_tests_models_charMapping.toDomain(base_import_tests_models_chardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_charService.get(#base_import_tests_models_char_id),'iBizBusinessCentral-Base_import_tests_models_char-Remove')")
    @ApiOperation(value = "删除测试:基本导入模型，字符", tags = {"测试:基本导入模型，字符" },  notes = "删除测试:基本导入模型，字符")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_chars/{base_import_tests_models_char_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_char_id") Long base_import_tests_models_char_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_charService.remove(base_import_tests_models_char_id));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_charService.getBaseImportTestsModelsCharByIds(#ids),'iBizBusinessCentral-Base_import_tests_models_char-Remove')")
    @ApiOperation(value = "批量删除测试:基本导入模型，字符", tags = {"测试:基本导入模型，字符" },  notes = "批量删除测试:基本导入模型，字符")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_chars/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_import_tests_models_charService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_import_tests_models_charMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_import_tests_models_char-Get')")
    @ApiOperation(value = "获取测试:基本导入模型，字符", tags = {"测试:基本导入模型，字符" },  notes = "获取测试:基本导入模型，字符")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_chars/{base_import_tests_models_char_id}")
    public ResponseEntity<Base_import_tests_models_charDTO> get(@PathVariable("base_import_tests_models_char_id") Long base_import_tests_models_char_id) {
        Base_import_tests_models_char domain = base_import_tests_models_charService.get(base_import_tests_models_char_id);
        Base_import_tests_models_charDTO dto = base_import_tests_models_charMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取测试:基本导入模型，字符草稿", tags = {"测试:基本导入模型，字符" },  notes = "获取测试:基本导入模型，字符草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_chars/getdraft")
    public ResponseEntity<Base_import_tests_models_charDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_charMapping.toDto(base_import_tests_models_charService.getDraft(new Base_import_tests_models_char())));
    }

    @ApiOperation(value = "检查测试:基本导入模型，字符", tags = {"测试:基本导入模型，字符" },  notes = "检查测试:基本导入模型，字符")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_chars/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_import_tests_models_charDTO base_import_tests_models_chardto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_charService.checkKey(base_import_tests_models_charMapping.toDomain(base_import_tests_models_chardto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_charMapping.toDomain(#base_import_tests_models_chardto),'iBizBusinessCentral-Base_import_tests_models_char-Save')")
    @ApiOperation(value = "保存测试:基本导入模型，字符", tags = {"测试:基本导入模型，字符" },  notes = "保存测试:基本导入模型，字符")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_chars/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_import_tests_models_charDTO base_import_tests_models_chardto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_charService.save(base_import_tests_models_charMapping.toDomain(base_import_tests_models_chardto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_charMapping.toDomain(#base_import_tests_models_chardtos),'iBizBusinessCentral-Base_import_tests_models_char-Save')")
    @ApiOperation(value = "批量保存测试:基本导入模型，字符", tags = {"测试:基本导入模型，字符" },  notes = "批量保存测试:基本导入模型，字符")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_chars/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_import_tests_models_charDTO> base_import_tests_models_chardtos) {
        base_import_tests_models_charService.saveBatch(base_import_tests_models_charMapping.toDomain(base_import_tests_models_chardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_char-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_char-Get')")
	@ApiOperation(value = "获取数据集", tags = {"测试:基本导入模型，字符" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_chars/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_charDTO>> fetchDefault(Base_import_tests_models_charSearchContext context) {
        Page<Base_import_tests_models_char> domains = base_import_tests_models_charService.searchDefault(context) ;
        List<Base_import_tests_models_charDTO> list = base_import_tests_models_charMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_char-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_char-Get')")
	@ApiOperation(value = "查询数据集", tags = {"测试:基本导入模型，字符" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_import_tests_models_chars/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_charDTO>> searchDefault(@RequestBody Base_import_tests_models_charSearchContext context) {
        Page<Base_import_tests_models_char> domains = base_import_tests_models_charService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_charMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

