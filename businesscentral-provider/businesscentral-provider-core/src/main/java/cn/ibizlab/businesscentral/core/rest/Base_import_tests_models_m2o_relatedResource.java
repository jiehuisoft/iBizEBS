package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_m2o_related;
import cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_m2o_relatedService;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_m2o_relatedSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"测试:基本导入模型，多对一关系" })
@RestController("Core-base_import_tests_models_m2o_related")
@RequestMapping("")
public class Base_import_tests_models_m2o_relatedResource {

    @Autowired
    public IBase_import_tests_models_m2o_relatedService base_import_tests_models_m2o_relatedService;

    @Autowired
    @Lazy
    public Base_import_tests_models_m2o_relatedMapping base_import_tests_models_m2o_relatedMapping;

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2o_relatedMapping.toDomain(#base_import_tests_models_m2o_relateddto),'iBizBusinessCentral-Base_import_tests_models_m2o_related-Create')")
    @ApiOperation(value = "新建测试:基本导入模型，多对一关系", tags = {"测试:基本导入模型，多对一关系" },  notes = "新建测试:基本导入模型，多对一关系")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2o_relateds")
    public ResponseEntity<Base_import_tests_models_m2o_relatedDTO> create(@Validated @RequestBody Base_import_tests_models_m2o_relatedDTO base_import_tests_models_m2o_relateddto) {
        Base_import_tests_models_m2o_related domain = base_import_tests_models_m2o_relatedMapping.toDomain(base_import_tests_models_m2o_relateddto);
		base_import_tests_models_m2o_relatedService.create(domain);
        Base_import_tests_models_m2o_relatedDTO dto = base_import_tests_models_m2o_relatedMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2o_relatedMapping.toDomain(#base_import_tests_models_m2o_relateddtos),'iBizBusinessCentral-Base_import_tests_models_m2o_related-Create')")
    @ApiOperation(value = "批量新建测试:基本导入模型，多对一关系", tags = {"测试:基本导入模型，多对一关系" },  notes = "批量新建测试:基本导入模型，多对一关系")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2o_relateds/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_m2o_relatedDTO> base_import_tests_models_m2o_relateddtos) {
        base_import_tests_models_m2o_relatedService.createBatch(base_import_tests_models_m2o_relatedMapping.toDomain(base_import_tests_models_m2o_relateddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_import_tests_models_m2o_related" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_import_tests_models_m2o_relatedService.get(#base_import_tests_models_m2o_related_id),'iBizBusinessCentral-Base_import_tests_models_m2o_related-Update')")
    @ApiOperation(value = "更新测试:基本导入模型，多对一关系", tags = {"测试:基本导入模型，多对一关系" },  notes = "更新测试:基本导入模型，多对一关系")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_m2o_relateds/{base_import_tests_models_m2o_related_id}")
    public ResponseEntity<Base_import_tests_models_m2o_relatedDTO> update(@PathVariable("base_import_tests_models_m2o_related_id") Long base_import_tests_models_m2o_related_id, @RequestBody Base_import_tests_models_m2o_relatedDTO base_import_tests_models_m2o_relateddto) {
		Base_import_tests_models_m2o_related domain  = base_import_tests_models_m2o_relatedMapping.toDomain(base_import_tests_models_m2o_relateddto);
        domain .setId(base_import_tests_models_m2o_related_id);
		base_import_tests_models_m2o_relatedService.update(domain );
		Base_import_tests_models_m2o_relatedDTO dto = base_import_tests_models_m2o_relatedMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2o_relatedService.getBaseImportTestsModelsM2oRelatedByEntities(this.base_import_tests_models_m2o_relatedMapping.toDomain(#base_import_tests_models_m2o_relateddtos)),'iBizBusinessCentral-Base_import_tests_models_m2o_related-Update')")
    @ApiOperation(value = "批量更新测试:基本导入模型，多对一关系", tags = {"测试:基本导入模型，多对一关系" },  notes = "批量更新测试:基本导入模型，多对一关系")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_m2o_relateds/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_m2o_relatedDTO> base_import_tests_models_m2o_relateddtos) {
        base_import_tests_models_m2o_relatedService.updateBatch(base_import_tests_models_m2o_relatedMapping.toDomain(base_import_tests_models_m2o_relateddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2o_relatedService.get(#base_import_tests_models_m2o_related_id),'iBizBusinessCentral-Base_import_tests_models_m2o_related-Remove')")
    @ApiOperation(value = "删除测试:基本导入模型，多对一关系", tags = {"测试:基本导入模型，多对一关系" },  notes = "删除测试:基本导入模型，多对一关系")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_m2o_relateds/{base_import_tests_models_m2o_related_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_m2o_related_id") Long base_import_tests_models_m2o_related_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_m2o_relatedService.remove(base_import_tests_models_m2o_related_id));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2o_relatedService.getBaseImportTestsModelsM2oRelatedByIds(#ids),'iBizBusinessCentral-Base_import_tests_models_m2o_related-Remove')")
    @ApiOperation(value = "批量删除测试:基本导入模型，多对一关系", tags = {"测试:基本导入模型，多对一关系" },  notes = "批量删除测试:基本导入模型，多对一关系")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_m2o_relateds/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_import_tests_models_m2o_relatedService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_import_tests_models_m2o_relatedMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_import_tests_models_m2o_related-Get')")
    @ApiOperation(value = "获取测试:基本导入模型，多对一关系", tags = {"测试:基本导入模型，多对一关系" },  notes = "获取测试:基本导入模型，多对一关系")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2o_relateds/{base_import_tests_models_m2o_related_id}")
    public ResponseEntity<Base_import_tests_models_m2o_relatedDTO> get(@PathVariable("base_import_tests_models_m2o_related_id") Long base_import_tests_models_m2o_related_id) {
        Base_import_tests_models_m2o_related domain = base_import_tests_models_m2o_relatedService.get(base_import_tests_models_m2o_related_id);
        Base_import_tests_models_m2o_relatedDTO dto = base_import_tests_models_m2o_relatedMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取测试:基本导入模型，多对一关系草稿", tags = {"测试:基本导入模型，多对一关系" },  notes = "获取测试:基本导入模型，多对一关系草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2o_relateds/getdraft")
    public ResponseEntity<Base_import_tests_models_m2o_relatedDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_m2o_relatedMapping.toDto(base_import_tests_models_m2o_relatedService.getDraft(new Base_import_tests_models_m2o_related())));
    }

    @ApiOperation(value = "检查测试:基本导入模型，多对一关系", tags = {"测试:基本导入模型，多对一关系" },  notes = "检查测试:基本导入模型，多对一关系")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2o_relateds/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_import_tests_models_m2o_relatedDTO base_import_tests_models_m2o_relateddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_m2o_relatedService.checkKey(base_import_tests_models_m2o_relatedMapping.toDomain(base_import_tests_models_m2o_relateddto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2o_relatedMapping.toDomain(#base_import_tests_models_m2o_relateddto),'iBizBusinessCentral-Base_import_tests_models_m2o_related-Save')")
    @ApiOperation(value = "保存测试:基本导入模型，多对一关系", tags = {"测试:基本导入模型，多对一关系" },  notes = "保存测试:基本导入模型，多对一关系")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2o_relateds/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_import_tests_models_m2o_relatedDTO base_import_tests_models_m2o_relateddto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_m2o_relatedService.save(base_import_tests_models_m2o_relatedMapping.toDomain(base_import_tests_models_m2o_relateddto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_m2o_relatedMapping.toDomain(#base_import_tests_models_m2o_relateddtos),'iBizBusinessCentral-Base_import_tests_models_m2o_related-Save')")
    @ApiOperation(value = "批量保存测试:基本导入模型，多对一关系", tags = {"测试:基本导入模型，多对一关系" },  notes = "批量保存测试:基本导入模型，多对一关系")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2o_relateds/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_import_tests_models_m2o_relatedDTO> base_import_tests_models_m2o_relateddtos) {
        base_import_tests_models_m2o_relatedService.saveBatch(base_import_tests_models_m2o_relatedMapping.toDomain(base_import_tests_models_m2o_relateddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_m2o_related-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_m2o_related-Get')")
	@ApiOperation(value = "获取数据集", tags = {"测试:基本导入模型，多对一关系" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_m2o_relateds/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_m2o_relatedDTO>> fetchDefault(Base_import_tests_models_m2o_relatedSearchContext context) {
        Page<Base_import_tests_models_m2o_related> domains = base_import_tests_models_m2o_relatedService.searchDefault(context) ;
        List<Base_import_tests_models_m2o_relatedDTO> list = base_import_tests_models_m2o_relatedMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_m2o_related-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_m2o_related-Get')")
	@ApiOperation(value = "查询数据集", tags = {"测试:基本导入模型，多对一关系" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_import_tests_models_m2o_relateds/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_m2o_relatedDTO>> searchDefault(@RequestBody Base_import_tests_models_m2o_relatedSearchContext context) {
        Page<Base_import_tests_models_m2o_related> domains = base_import_tests_models_m2o_relatedService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_m2o_relatedMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

