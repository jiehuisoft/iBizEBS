package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type;
import cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_typeService;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_typeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"活动类别" })
@RestController("Core-event_type")
@RequestMapping("")
public class Event_typeResource {

    @Autowired
    public IEvent_typeService event_typeService;

    @Autowired
    @Lazy
    public Event_typeMapping event_typeMapping;

    @PreAuthorize("hasPermission(this.event_typeMapping.toDomain(#event_typedto),'iBizBusinessCentral-Event_type-Create')")
    @ApiOperation(value = "新建活动类别", tags = {"活动类别" },  notes = "新建活动类别")
	@RequestMapping(method = RequestMethod.POST, value = "/event_types")
    public ResponseEntity<Event_typeDTO> create(@Validated @RequestBody Event_typeDTO event_typedto) {
        Event_type domain = event_typeMapping.toDomain(event_typedto);
		event_typeService.create(domain);
        Event_typeDTO dto = event_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_typeMapping.toDomain(#event_typedtos),'iBizBusinessCentral-Event_type-Create')")
    @ApiOperation(value = "批量新建活动类别", tags = {"活动类别" },  notes = "批量新建活动类别")
	@RequestMapping(method = RequestMethod.POST, value = "/event_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_typeDTO> event_typedtos) {
        event_typeService.createBatch(event_typeMapping.toDomain(event_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "event_type" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.event_typeService.get(#event_type_id),'iBizBusinessCentral-Event_type-Update')")
    @ApiOperation(value = "更新活动类别", tags = {"活动类别" },  notes = "更新活动类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_types/{event_type_id}")
    public ResponseEntity<Event_typeDTO> update(@PathVariable("event_type_id") Long event_type_id, @RequestBody Event_typeDTO event_typedto) {
		Event_type domain  = event_typeMapping.toDomain(event_typedto);
        domain .setId(event_type_id);
		event_typeService.update(domain );
		Event_typeDTO dto = event_typeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_typeService.getEventTypeByEntities(this.event_typeMapping.toDomain(#event_typedtos)),'iBizBusinessCentral-Event_type-Update')")
    @ApiOperation(value = "批量更新活动类别", tags = {"活动类别" },  notes = "批量更新活动类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_typeDTO> event_typedtos) {
        event_typeService.updateBatch(event_typeMapping.toDomain(event_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.event_typeService.get(#event_type_id),'iBizBusinessCentral-Event_type-Remove')")
    @ApiOperation(value = "删除活动类别", tags = {"活动类别" },  notes = "删除活动类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_types/{event_type_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("event_type_id") Long event_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_typeService.remove(event_type_id));
    }

    @PreAuthorize("hasPermission(this.event_typeService.getEventTypeByIds(#ids),'iBizBusinessCentral-Event_type-Remove')")
    @ApiOperation(value = "批量删除活动类别", tags = {"活动类别" },  notes = "批量删除活动类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        event_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.event_typeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Event_type-Get')")
    @ApiOperation(value = "获取活动类别", tags = {"活动类别" },  notes = "获取活动类别")
	@RequestMapping(method = RequestMethod.GET, value = "/event_types/{event_type_id}")
    public ResponseEntity<Event_typeDTO> get(@PathVariable("event_type_id") Long event_type_id) {
        Event_type domain = event_typeService.get(event_type_id);
        Event_typeDTO dto = event_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取活动类别草稿", tags = {"活动类别" },  notes = "获取活动类别草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/event_types/getdraft")
    public ResponseEntity<Event_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(event_typeMapping.toDto(event_typeService.getDraft(new Event_type())));
    }

    @ApiOperation(value = "检查活动类别", tags = {"活动类别" },  notes = "检查活动类别")
	@RequestMapping(method = RequestMethod.POST, value = "/event_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Event_typeDTO event_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(event_typeService.checkKey(event_typeMapping.toDomain(event_typedto)));
    }

    @PreAuthorize("hasPermission(this.event_typeMapping.toDomain(#event_typedto),'iBizBusinessCentral-Event_type-Save')")
    @ApiOperation(value = "保存活动类别", tags = {"活动类别" },  notes = "保存活动类别")
	@RequestMapping(method = RequestMethod.POST, value = "/event_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Event_typeDTO event_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(event_typeService.save(event_typeMapping.toDomain(event_typedto)));
    }

    @PreAuthorize("hasPermission(this.event_typeMapping.toDomain(#event_typedtos),'iBizBusinessCentral-Event_type-Save')")
    @ApiOperation(value = "批量保存活动类别", tags = {"活动类别" },  notes = "批量保存活动类别")
	@RequestMapping(method = RequestMethod.POST, value = "/event_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Event_typeDTO> event_typedtos) {
        event_typeService.saveBatch(event_typeMapping.toDomain(event_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_type-Get')")
	@ApiOperation(value = "获取数据集", tags = {"活动类别" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/event_types/fetchdefault")
	public ResponseEntity<List<Event_typeDTO>> fetchDefault(Event_typeSearchContext context) {
        Page<Event_type> domains = event_typeService.searchDefault(context) ;
        List<Event_typeDTO> list = event_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_type-Get')")
	@ApiOperation(value = "查询数据集", tags = {"活动类别" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/event_types/searchdefault")
	public ResponseEntity<Page<Event_typeDTO>> searchDefault(@RequestBody Event_typeSearchContext context) {
        Page<Event_type> domains = event_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

