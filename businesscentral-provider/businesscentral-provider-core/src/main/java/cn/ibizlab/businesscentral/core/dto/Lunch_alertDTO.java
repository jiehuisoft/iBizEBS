package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Lunch_alertDTO]
 */
@Data
public class Lunch_alertDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SPECIFIC_DAY]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "specific_day" , format="yyyy-MM-dd")
    @JsonProperty("specific_day")
    private Timestamp specificDay;

    /**
     * 属性 [DISPLAY]
     *
     */
    @JSONField(name = "display")
    @JsonProperty("display")
    private Boolean display;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [THURSDAY]
     *
     */
    @JSONField(name = "thursday")
    @JsonProperty("thursday")
    private Boolean thursday;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [WEDNESDAY]
     *
     */
    @JSONField(name = "wednesday")
    @JsonProperty("wednesday")
    private Boolean wednesday;

    /**
     * 属性 [TUESDAY]
     *
     */
    @JSONField(name = "tuesday")
    @JsonProperty("tuesday")
    private Boolean tuesday;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MESSAGE]
     *
     */
    @JSONField(name = "message")
    @JsonProperty("message")
    @NotBlank(message = "[消息]不允许为空!")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String message;

    /**
     * 属性 [ALERT_TYPE]
     *
     */
    @JSONField(name = "alert_type")
    @JsonProperty("alert_type")
    @NotBlank(message = "[重新提起]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String alertType;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [SATURDAY]
     *
     */
    @JSONField(name = "saturday")
    @JsonProperty("saturday")
    private Boolean saturday;

    /**
     * 属性 [START_HOUR]
     *
     */
    @JSONField(name = "start_hour")
    @JsonProperty("start_hour")
    @NotNull(message = "[介于]不允许为空!")
    private Double startHour;

    /**
     * 属性 [MONDAY]
     *
     */
    @JSONField(name = "monday")
    @JsonProperty("monday")
    private Boolean monday;

    /**
     * 属性 [FRIDAY]
     *
     */
    @JSONField(name = "friday")
    @JsonProperty("friday")
    private Boolean friday;

    /**
     * 属性 [END_HOUR]
     *
     */
    @JSONField(name = "end_hour")
    @JsonProperty("end_hour")
    @NotNull(message = "[和]不允许为空!")
    private Double endHour;

    /**
     * 属性 [SUNDAY]
     *
     */
    @JSONField(name = "sunday")
    @JsonProperty("sunday")
    private Boolean sunday;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [SPECIFIC_DAY]
     */
    public void setSpecificDay(Timestamp  specificDay){
        this.specificDay = specificDay ;
        this.modify("specific_day",specificDay);
    }

    /**
     * 设置 [THURSDAY]
     */
    public void setThursday(Boolean  thursday){
        this.thursday = thursday ;
        this.modify("thursday",thursday);
    }

    /**
     * 设置 [WEDNESDAY]
     */
    public void setWednesday(Boolean  wednesday){
        this.wednesday = wednesday ;
        this.modify("wednesday",wednesday);
    }

    /**
     * 设置 [TUESDAY]
     */
    public void setTuesday(Boolean  tuesday){
        this.tuesday = tuesday ;
        this.modify("tuesday",tuesday);
    }

    /**
     * 设置 [MESSAGE]
     */
    public void setMessage(String  message){
        this.message = message ;
        this.modify("message",message);
    }

    /**
     * 设置 [ALERT_TYPE]
     */
    public void setAlertType(String  alertType){
        this.alertType = alertType ;
        this.modify("alert_type",alertType);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [SATURDAY]
     */
    public void setSaturday(Boolean  saturday){
        this.saturday = saturday ;
        this.modify("saturday",saturday);
    }

    /**
     * 设置 [START_HOUR]
     */
    public void setStartHour(Double  startHour){
        this.startHour = startHour ;
        this.modify("start_hour",startHour);
    }

    /**
     * 设置 [MONDAY]
     */
    public void setMonday(Boolean  monday){
        this.monday = monday ;
        this.modify("monday",monday);
    }

    /**
     * 设置 [FRIDAY]
     */
    public void setFriday(Boolean  friday){
        this.friday = friday ;
        this.modify("friday",friday);
    }

    /**
     * 设置 [END_HOUR]
     */
    public void setEndHour(Double  endHour){
        this.endHour = endHour ;
        this.modify("end_hour",endHour);
    }

    /**
     * 设置 [SUNDAY]
     */
    public void setSunday(Boolean  sunday){
        this.sunday = sunday ;
        this.modify("sunday",sunday);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }


}


