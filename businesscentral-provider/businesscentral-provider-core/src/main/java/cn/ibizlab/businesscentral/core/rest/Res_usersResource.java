package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_usersSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"用户" })
@RestController("Core-res_users")
@RequestMapping("")
public class Res_usersResource {

    @Autowired
    public IRes_usersService res_usersService;

    @Autowired
    @Lazy
    public Res_usersMapping res_usersMapping;

    @PreAuthorize("hasPermission(this.res_usersMapping.toDomain(#res_usersdto),'iBizBusinessCentral-Res_users-Create')")
    @ApiOperation(value = "新建用户", tags = {"用户" },  notes = "新建用户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users")
    public ResponseEntity<Res_usersDTO> create(@Validated @RequestBody Res_usersDTO res_usersdto) {
        Res_users domain = res_usersMapping.toDomain(res_usersdto);
		res_usersService.create(domain);
        Res_usersDTO dto = res_usersMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_usersMapping.toDomain(#res_usersdtos),'iBizBusinessCentral-Res_users-Create')")
    @ApiOperation(value = "批量新建用户", tags = {"用户" },  notes = "批量新建用户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_usersDTO> res_usersdtos) {
        res_usersService.createBatch(res_usersMapping.toDomain(res_usersdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_users" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_usersService.get(#res_users_id),'iBizBusinessCentral-Res_users-Update')")
    @ApiOperation(value = "更新用户", tags = {"用户" },  notes = "更新用户")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_users/{res_users_id}")
    public ResponseEntity<Res_usersDTO> update(@PathVariable("res_users_id") Long res_users_id, @RequestBody Res_usersDTO res_usersdto) {
		Res_users domain  = res_usersMapping.toDomain(res_usersdto);
        domain .setId(res_users_id);
		res_usersService.update(domain );
		Res_usersDTO dto = res_usersMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_usersService.getResUsersByEntities(this.res_usersMapping.toDomain(#res_usersdtos)),'iBizBusinessCentral-Res_users-Update')")
    @ApiOperation(value = "批量更新用户", tags = {"用户" },  notes = "批量更新用户")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_users/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_usersDTO> res_usersdtos) {
        res_usersService.updateBatch(res_usersMapping.toDomain(res_usersdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_usersService.get(#res_users_id),'iBizBusinessCentral-Res_users-Remove')")
    @ApiOperation(value = "删除用户", tags = {"用户" },  notes = "删除用户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_users/{res_users_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_users_id") Long res_users_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_usersService.remove(res_users_id));
    }

    @PreAuthorize("hasPermission(this.res_usersService.getResUsersByIds(#ids),'iBizBusinessCentral-Res_users-Remove')")
    @ApiOperation(value = "批量删除用户", tags = {"用户" },  notes = "批量删除用户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_users/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_usersService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_usersMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_users-Get')")
    @ApiOperation(value = "获取用户", tags = {"用户" },  notes = "获取用户")
	@RequestMapping(method = RequestMethod.GET, value = "/res_users/{res_users_id}")
    public ResponseEntity<Res_usersDTO> get(@PathVariable("res_users_id") Long res_users_id) {
        Res_users domain = res_usersService.get(res_users_id);
        Res_usersDTO dto = res_usersMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取用户草稿", tags = {"用户" },  notes = "获取用户草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_users/getdraft")
    public ResponseEntity<Res_usersDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_usersMapping.toDto(res_usersService.getDraft(new Res_users())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_users-A-all')")
    @ApiOperation(value = "A", tags = {"用户" },  notes = "A")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users/{res_users_id}/a")
    public ResponseEntity<Res_usersDTO> a(@PathVariable("res_users_id") Long res_users_id, @RequestBody Res_usersDTO res_usersdto) {
        Res_users domain = res_usersMapping.toDomain(res_usersdto);
        domain.setId(res_users_id);
        domain = res_usersService.a(domain);
        res_usersdto = res_usersMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(res_usersdto);
    }

    @ApiOperation(value = "检查用户", tags = {"用户" },  notes = "检查用户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_usersDTO res_usersdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_usersService.checkKey(res_usersMapping.toDomain(res_usersdto)));
    }

    @PreAuthorize("hasPermission(this.res_usersMapping.toDomain(#res_usersdto),'iBizBusinessCentral-Res_users-Save')")
    @ApiOperation(value = "保存用户", tags = {"用户" },  notes = "保存用户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_usersDTO res_usersdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_usersService.save(res_usersMapping.toDomain(res_usersdto)));
    }

    @PreAuthorize("hasPermission(this.res_usersMapping.toDomain(#res_usersdtos),'iBizBusinessCentral-Res_users-Save')")
    @ApiOperation(value = "批量保存用户", tags = {"用户" },  notes = "批量保存用户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_usersDTO> res_usersdtos) {
        res_usersService.saveBatch(res_usersMapping.toDomain(res_usersdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_users-searchActive-all') and hasPermission(#context,'iBizBusinessCentral-Res_users-Get')")
	@ApiOperation(value = "获取有效用户", tags = {"用户" } ,notes = "获取有效用户")
    @RequestMapping(method= RequestMethod.GET , value="/res_users/fetchactive")
	public ResponseEntity<List<Res_usersDTO>> fetchActive(Res_usersSearchContext context) {
        Page<Res_users> domains = res_usersService.searchActive(context) ;
        List<Res_usersDTO> list = res_usersMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_users-searchActive-all') and hasPermission(#context,'iBizBusinessCentral-Res_users-Get')")
	@ApiOperation(value = "查询有效用户", tags = {"用户" } ,notes = "查询有效用户")
    @RequestMapping(method= RequestMethod.POST , value="/res_users/searchactive")
	public ResponseEntity<Page<Res_usersDTO>> searchActive(@RequestBody Res_usersSearchContext context) {
        Page<Res_users> domains = res_usersService.searchActive(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_usersMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_users-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_users-Get')")
	@ApiOperation(value = "获取数据集", tags = {"用户" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_users/fetchdefault")
	public ResponseEntity<List<Res_usersDTO>> fetchDefault(Res_usersSearchContext context) {
        Page<Res_users> domains = res_usersService.searchDefault(context) ;
        List<Res_usersDTO> list = res_usersMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_users-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_users-Get')")
	@ApiOperation(value = "查询数据集", tags = {"用户" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_users/searchdefault")
	public ResponseEntity<Page<Res_usersDTO>> searchDefault(@RequestBody Res_usersSearchContext context) {
        Page<Res_users> domains = res_usersService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_usersMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

