package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_stage;
import cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_stageService;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_stageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"保养阶段" })
@RestController("Core-maintenance_stage")
@RequestMapping("")
public class Maintenance_stageResource {

    @Autowired
    public IMaintenance_stageService maintenance_stageService;

    @Autowired
    @Lazy
    public Maintenance_stageMapping maintenance_stageMapping;

    @PreAuthorize("hasPermission(this.maintenance_stageMapping.toDomain(#maintenance_stagedto),'iBizBusinessCentral-Maintenance_stage-Create')")
    @ApiOperation(value = "新建保养阶段", tags = {"保养阶段" },  notes = "新建保养阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_stages")
    public ResponseEntity<Maintenance_stageDTO> create(@Validated @RequestBody Maintenance_stageDTO maintenance_stagedto) {
        Maintenance_stage domain = maintenance_stageMapping.toDomain(maintenance_stagedto);
		maintenance_stageService.create(domain);
        Maintenance_stageDTO dto = maintenance_stageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.maintenance_stageMapping.toDomain(#maintenance_stagedtos),'iBizBusinessCentral-Maintenance_stage-Create')")
    @ApiOperation(value = "批量新建保养阶段", tags = {"保养阶段" },  notes = "批量新建保养阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_stages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Maintenance_stageDTO> maintenance_stagedtos) {
        maintenance_stageService.createBatch(maintenance_stageMapping.toDomain(maintenance_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "maintenance_stage" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.maintenance_stageService.get(#maintenance_stage_id),'iBizBusinessCentral-Maintenance_stage-Update')")
    @ApiOperation(value = "更新保养阶段", tags = {"保养阶段" },  notes = "更新保养阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_stages/{maintenance_stage_id}")
    public ResponseEntity<Maintenance_stageDTO> update(@PathVariable("maintenance_stage_id") Long maintenance_stage_id, @RequestBody Maintenance_stageDTO maintenance_stagedto) {
		Maintenance_stage domain  = maintenance_stageMapping.toDomain(maintenance_stagedto);
        domain .setId(maintenance_stage_id);
		maintenance_stageService.update(domain );
		Maintenance_stageDTO dto = maintenance_stageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.maintenance_stageService.getMaintenanceStageByEntities(this.maintenance_stageMapping.toDomain(#maintenance_stagedtos)),'iBizBusinessCentral-Maintenance_stage-Update')")
    @ApiOperation(value = "批量更新保养阶段", tags = {"保养阶段" },  notes = "批量更新保养阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_stages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_stageDTO> maintenance_stagedtos) {
        maintenance_stageService.updateBatch(maintenance_stageMapping.toDomain(maintenance_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.maintenance_stageService.get(#maintenance_stage_id),'iBizBusinessCentral-Maintenance_stage-Remove')")
    @ApiOperation(value = "删除保养阶段", tags = {"保养阶段" },  notes = "删除保养阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_stages/{maintenance_stage_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_stage_id") Long maintenance_stage_id) {
         return ResponseEntity.status(HttpStatus.OK).body(maintenance_stageService.remove(maintenance_stage_id));
    }

    @PreAuthorize("hasPermission(this.maintenance_stageService.getMaintenanceStageByIds(#ids),'iBizBusinessCentral-Maintenance_stage-Remove')")
    @ApiOperation(value = "批量删除保养阶段", tags = {"保养阶段" },  notes = "批量删除保养阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_stages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        maintenance_stageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.maintenance_stageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Maintenance_stage-Get')")
    @ApiOperation(value = "获取保养阶段", tags = {"保养阶段" },  notes = "获取保养阶段")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_stages/{maintenance_stage_id}")
    public ResponseEntity<Maintenance_stageDTO> get(@PathVariable("maintenance_stage_id") Long maintenance_stage_id) {
        Maintenance_stage domain = maintenance_stageService.get(maintenance_stage_id);
        Maintenance_stageDTO dto = maintenance_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取保养阶段草稿", tags = {"保养阶段" },  notes = "获取保养阶段草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_stages/getdraft")
    public ResponseEntity<Maintenance_stageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(maintenance_stageMapping.toDto(maintenance_stageService.getDraft(new Maintenance_stage())));
    }

    @ApiOperation(value = "检查保养阶段", tags = {"保养阶段" },  notes = "检查保养阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_stages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Maintenance_stageDTO maintenance_stagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(maintenance_stageService.checkKey(maintenance_stageMapping.toDomain(maintenance_stagedto)));
    }

    @PreAuthorize("hasPermission(this.maintenance_stageMapping.toDomain(#maintenance_stagedto),'iBizBusinessCentral-Maintenance_stage-Save')")
    @ApiOperation(value = "保存保养阶段", tags = {"保养阶段" },  notes = "保存保养阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_stages/save")
    public ResponseEntity<Boolean> save(@RequestBody Maintenance_stageDTO maintenance_stagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(maintenance_stageService.save(maintenance_stageMapping.toDomain(maintenance_stagedto)));
    }

    @PreAuthorize("hasPermission(this.maintenance_stageMapping.toDomain(#maintenance_stagedtos),'iBizBusinessCentral-Maintenance_stage-Save')")
    @ApiOperation(value = "批量保存保养阶段", tags = {"保养阶段" },  notes = "批量保存保养阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_stages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Maintenance_stageDTO> maintenance_stagedtos) {
        maintenance_stageService.saveBatch(maintenance_stageMapping.toDomain(maintenance_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Maintenance_stage-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Maintenance_stage-Get')")
	@ApiOperation(value = "获取数据集", tags = {"保养阶段" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_stages/fetchdefault")
	public ResponseEntity<List<Maintenance_stageDTO>> fetchDefault(Maintenance_stageSearchContext context) {
        Page<Maintenance_stage> domains = maintenance_stageService.searchDefault(context) ;
        List<Maintenance_stageDTO> list = maintenance_stageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Maintenance_stage-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Maintenance_stage-Get')")
	@ApiOperation(value = "查询数据集", tags = {"保养阶段" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/maintenance_stages/searchdefault")
	public ResponseEntity<Page<Maintenance_stageDTO>> searchDefault(@RequestBody Maintenance_stageSearchContext context) {
        Page<Maintenance_stage> domains = maintenance_stageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(maintenance_stageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

