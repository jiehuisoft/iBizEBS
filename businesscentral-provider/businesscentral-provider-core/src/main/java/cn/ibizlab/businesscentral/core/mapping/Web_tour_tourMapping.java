package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_web_tour.domain.Web_tour_tour;
import cn.ibizlab.businesscentral.core.dto.Web_tour_tourDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreWeb_tour_tourMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Web_tour_tourMapping extends MappingBase<Web_tour_tourDTO, Web_tour_tour> {


}

