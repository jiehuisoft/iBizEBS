package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment_category;
import cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipment_categoryService;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_equipment_categorySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"维护设备类别" })
@RestController("Core-maintenance_equipment_category")
@RequestMapping("")
public class Maintenance_equipment_categoryResource {

    @Autowired
    public IMaintenance_equipment_categoryService maintenance_equipment_categoryService;

    @Autowired
    @Lazy
    public Maintenance_equipment_categoryMapping maintenance_equipment_categoryMapping;

    @PreAuthorize("hasPermission(this.maintenance_equipment_categoryMapping.toDomain(#maintenance_equipment_categorydto),'iBizBusinessCentral-Maintenance_equipment_category-Create')")
    @ApiOperation(value = "新建维护设备类别", tags = {"维护设备类别" },  notes = "新建维护设备类别")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipment_categories")
    public ResponseEntity<Maintenance_equipment_categoryDTO> create(@Validated @RequestBody Maintenance_equipment_categoryDTO maintenance_equipment_categorydto) {
        Maintenance_equipment_category domain = maintenance_equipment_categoryMapping.toDomain(maintenance_equipment_categorydto);
		maintenance_equipment_categoryService.create(domain);
        Maintenance_equipment_categoryDTO dto = maintenance_equipment_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.maintenance_equipment_categoryMapping.toDomain(#maintenance_equipment_categorydtos),'iBizBusinessCentral-Maintenance_equipment_category-Create')")
    @ApiOperation(value = "批量新建维护设备类别", tags = {"维护设备类别" },  notes = "批量新建维护设备类别")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipment_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Maintenance_equipment_categoryDTO> maintenance_equipment_categorydtos) {
        maintenance_equipment_categoryService.createBatch(maintenance_equipment_categoryMapping.toDomain(maintenance_equipment_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "maintenance_equipment_category" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.maintenance_equipment_categoryService.get(#maintenance_equipment_category_id),'iBizBusinessCentral-Maintenance_equipment_category-Update')")
    @ApiOperation(value = "更新维护设备类别", tags = {"维护设备类别" },  notes = "更新维护设备类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_equipment_categories/{maintenance_equipment_category_id}")
    public ResponseEntity<Maintenance_equipment_categoryDTO> update(@PathVariable("maintenance_equipment_category_id") Long maintenance_equipment_category_id, @RequestBody Maintenance_equipment_categoryDTO maintenance_equipment_categorydto) {
		Maintenance_equipment_category domain  = maintenance_equipment_categoryMapping.toDomain(maintenance_equipment_categorydto);
        domain .setId(maintenance_equipment_category_id);
		maintenance_equipment_categoryService.update(domain );
		Maintenance_equipment_categoryDTO dto = maintenance_equipment_categoryMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.maintenance_equipment_categoryService.getMaintenanceEquipmentCategoryByEntities(this.maintenance_equipment_categoryMapping.toDomain(#maintenance_equipment_categorydtos)),'iBizBusinessCentral-Maintenance_equipment_category-Update')")
    @ApiOperation(value = "批量更新维护设备类别", tags = {"维护设备类别" },  notes = "批量更新维护设备类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_equipment_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_equipment_categoryDTO> maintenance_equipment_categorydtos) {
        maintenance_equipment_categoryService.updateBatch(maintenance_equipment_categoryMapping.toDomain(maintenance_equipment_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.maintenance_equipment_categoryService.get(#maintenance_equipment_category_id),'iBizBusinessCentral-Maintenance_equipment_category-Remove')")
    @ApiOperation(value = "删除维护设备类别", tags = {"维护设备类别" },  notes = "删除维护设备类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_equipment_categories/{maintenance_equipment_category_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_equipment_category_id") Long maintenance_equipment_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(maintenance_equipment_categoryService.remove(maintenance_equipment_category_id));
    }

    @PreAuthorize("hasPermission(this.maintenance_equipment_categoryService.getMaintenanceEquipmentCategoryByIds(#ids),'iBizBusinessCentral-Maintenance_equipment_category-Remove')")
    @ApiOperation(value = "批量删除维护设备类别", tags = {"维护设备类别" },  notes = "批量删除维护设备类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_equipment_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        maintenance_equipment_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.maintenance_equipment_categoryMapping.toDomain(returnObject.body),'iBizBusinessCentral-Maintenance_equipment_category-Get')")
    @ApiOperation(value = "获取维护设备类别", tags = {"维护设备类别" },  notes = "获取维护设备类别")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_equipment_categories/{maintenance_equipment_category_id}")
    public ResponseEntity<Maintenance_equipment_categoryDTO> get(@PathVariable("maintenance_equipment_category_id") Long maintenance_equipment_category_id) {
        Maintenance_equipment_category domain = maintenance_equipment_categoryService.get(maintenance_equipment_category_id);
        Maintenance_equipment_categoryDTO dto = maintenance_equipment_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取维护设备类别草稿", tags = {"维护设备类别" },  notes = "获取维护设备类别草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_equipment_categories/getdraft")
    public ResponseEntity<Maintenance_equipment_categoryDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(maintenance_equipment_categoryMapping.toDto(maintenance_equipment_categoryService.getDraft(new Maintenance_equipment_category())));
    }

    @ApiOperation(value = "检查维护设备类别", tags = {"维护设备类别" },  notes = "检查维护设备类别")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipment_categories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Maintenance_equipment_categoryDTO maintenance_equipment_categorydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(maintenance_equipment_categoryService.checkKey(maintenance_equipment_categoryMapping.toDomain(maintenance_equipment_categorydto)));
    }

    @PreAuthorize("hasPermission(this.maintenance_equipment_categoryMapping.toDomain(#maintenance_equipment_categorydto),'iBizBusinessCentral-Maintenance_equipment_category-Save')")
    @ApiOperation(value = "保存维护设备类别", tags = {"维护设备类别" },  notes = "保存维护设备类别")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipment_categories/save")
    public ResponseEntity<Boolean> save(@RequestBody Maintenance_equipment_categoryDTO maintenance_equipment_categorydto) {
        return ResponseEntity.status(HttpStatus.OK).body(maintenance_equipment_categoryService.save(maintenance_equipment_categoryMapping.toDomain(maintenance_equipment_categorydto)));
    }

    @PreAuthorize("hasPermission(this.maintenance_equipment_categoryMapping.toDomain(#maintenance_equipment_categorydtos),'iBizBusinessCentral-Maintenance_equipment_category-Save')")
    @ApiOperation(value = "批量保存维护设备类别", tags = {"维护设备类别" },  notes = "批量保存维护设备类别")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipment_categories/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Maintenance_equipment_categoryDTO> maintenance_equipment_categorydtos) {
        maintenance_equipment_categoryService.saveBatch(maintenance_equipment_categoryMapping.toDomain(maintenance_equipment_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Maintenance_equipment_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Maintenance_equipment_category-Get')")
	@ApiOperation(value = "获取数据集", tags = {"维护设备类别" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_equipment_categories/fetchdefault")
	public ResponseEntity<List<Maintenance_equipment_categoryDTO>> fetchDefault(Maintenance_equipment_categorySearchContext context) {
        Page<Maintenance_equipment_category> domains = maintenance_equipment_categoryService.searchDefault(context) ;
        List<Maintenance_equipment_categoryDTO> list = maintenance_equipment_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Maintenance_equipment_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Maintenance_equipment_category-Get')")
	@ApiOperation(value = "查询数据集", tags = {"维护设备类别" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/maintenance_equipment_categories/searchdefault")
	public ResponseEntity<Page<Maintenance_equipment_categoryDTO>> searchDefault(@RequestBody Maintenance_equipment_categorySearchContext context) {
        Page<Maintenance_equipment_category> domains = maintenance_equipment_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(maintenance_equipment_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

