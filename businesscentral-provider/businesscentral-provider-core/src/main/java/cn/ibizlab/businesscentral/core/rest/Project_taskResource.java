package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_task;
import cn.ibizlab.businesscentral.core.odoo_project.service.IProject_taskService;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_taskSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"任务" })
@RestController("Core-project_task")
@RequestMapping("")
public class Project_taskResource {

    @Autowired
    public IProject_taskService project_taskService;

    @Autowired
    @Lazy
    public Project_taskMapping project_taskMapping;

    @PreAuthorize("hasPermission(this.project_taskMapping.toDomain(#project_taskdto),'iBizBusinessCentral-Project_task-Create')")
    @ApiOperation(value = "新建任务", tags = {"任务" },  notes = "新建任务")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tasks")
    public ResponseEntity<Project_taskDTO> create(@Validated @RequestBody Project_taskDTO project_taskdto) {
        Project_task domain = project_taskMapping.toDomain(project_taskdto);
		project_taskService.create(domain);
        Project_taskDTO dto = project_taskMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.project_taskMapping.toDomain(#project_taskdtos),'iBizBusinessCentral-Project_task-Create')")
    @ApiOperation(value = "批量新建任务", tags = {"任务" },  notes = "批量新建任务")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tasks/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Project_taskDTO> project_taskdtos) {
        project_taskService.createBatch(project_taskMapping.toDomain(project_taskdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "project_task" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.project_taskService.get(#project_task_id),'iBizBusinessCentral-Project_task-Update')")
    @ApiOperation(value = "更新任务", tags = {"任务" },  notes = "更新任务")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_tasks/{project_task_id}")
    public ResponseEntity<Project_taskDTO> update(@PathVariable("project_task_id") Long project_task_id, @RequestBody Project_taskDTO project_taskdto) {
		Project_task domain  = project_taskMapping.toDomain(project_taskdto);
        domain .setId(project_task_id);
		project_taskService.update(domain );
		Project_taskDTO dto = project_taskMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.project_taskService.getProjectTaskByEntities(this.project_taskMapping.toDomain(#project_taskdtos)),'iBizBusinessCentral-Project_task-Update')")
    @ApiOperation(value = "批量更新任务", tags = {"任务" },  notes = "批量更新任务")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_tasks/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Project_taskDTO> project_taskdtos) {
        project_taskService.updateBatch(project_taskMapping.toDomain(project_taskdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.project_taskService.get(#project_task_id),'iBizBusinessCentral-Project_task-Remove')")
    @ApiOperation(value = "删除任务", tags = {"任务" },  notes = "删除任务")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_tasks/{project_task_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("project_task_id") Long project_task_id) {
         return ResponseEntity.status(HttpStatus.OK).body(project_taskService.remove(project_task_id));
    }

    @PreAuthorize("hasPermission(this.project_taskService.getProjectTaskByIds(#ids),'iBizBusinessCentral-Project_task-Remove')")
    @ApiOperation(value = "批量删除任务", tags = {"任务" },  notes = "批量删除任务")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_tasks/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        project_taskService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.project_taskMapping.toDomain(returnObject.body),'iBizBusinessCentral-Project_task-Get')")
    @ApiOperation(value = "获取任务", tags = {"任务" },  notes = "获取任务")
	@RequestMapping(method = RequestMethod.GET, value = "/project_tasks/{project_task_id}")
    public ResponseEntity<Project_taskDTO> get(@PathVariable("project_task_id") Long project_task_id) {
        Project_task domain = project_taskService.get(project_task_id);
        Project_taskDTO dto = project_taskMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取任务草稿", tags = {"任务" },  notes = "获取任务草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/project_tasks/getdraft")
    public ResponseEntity<Project_taskDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(project_taskMapping.toDto(project_taskService.getDraft(new Project_task())));
    }

    @ApiOperation(value = "检查任务", tags = {"任务" },  notes = "检查任务")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tasks/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Project_taskDTO project_taskdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(project_taskService.checkKey(project_taskMapping.toDomain(project_taskdto)));
    }

    @PreAuthorize("hasPermission(this.project_taskMapping.toDomain(#project_taskdto),'iBizBusinessCentral-Project_task-Save')")
    @ApiOperation(value = "保存任务", tags = {"任务" },  notes = "保存任务")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tasks/save")
    public ResponseEntity<Boolean> save(@RequestBody Project_taskDTO project_taskdto) {
        return ResponseEntity.status(HttpStatus.OK).body(project_taskService.save(project_taskMapping.toDomain(project_taskdto)));
    }

    @PreAuthorize("hasPermission(this.project_taskMapping.toDomain(#project_taskdtos),'iBizBusinessCentral-Project_task-Save')")
    @ApiOperation(value = "批量保存任务", tags = {"任务" },  notes = "批量保存任务")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tasks/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Project_taskDTO> project_taskdtos) {
        project_taskService.saveBatch(project_taskMapping.toDomain(project_taskdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Project_task-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Project_task-Get')")
	@ApiOperation(value = "获取数据集", tags = {"任务" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/project_tasks/fetchdefault")
	public ResponseEntity<List<Project_taskDTO>> fetchDefault(Project_taskSearchContext context) {
        Page<Project_task> domains = project_taskService.searchDefault(context) ;
        List<Project_taskDTO> list = project_taskMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Project_task-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Project_task-Get')")
	@ApiOperation(value = "查询数据集", tags = {"任务" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/project_tasks/searchdefault")
	public ResponseEntity<Page<Project_taskDTO>> searchDefault(@RequestBody Project_taskSearchContext context) {
        Page<Project_task> domains = project_taskService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(project_taskMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

