package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_holidays_summary_employee;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_holidays_summary_employeeService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_holidays_summary_employeeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"按员工的休假摘要报告" })
@RestController("Core-hr_holidays_summary_employee")
@RequestMapping("")
public class Hr_holidays_summary_employeeResource {

    @Autowired
    public IHr_holidays_summary_employeeService hr_holidays_summary_employeeService;

    @Autowired
    @Lazy
    public Hr_holidays_summary_employeeMapping hr_holidays_summary_employeeMapping;

    @PreAuthorize("hasPermission(this.hr_holidays_summary_employeeMapping.toDomain(#hr_holidays_summary_employeedto),'iBizBusinessCentral-Hr_holidays_summary_employee-Create')")
    @ApiOperation(value = "新建按员工的休假摘要报告", tags = {"按员工的休假摘要报告" },  notes = "新建按员工的休假摘要报告")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_employees")
    public ResponseEntity<Hr_holidays_summary_employeeDTO> create(@Validated @RequestBody Hr_holidays_summary_employeeDTO hr_holidays_summary_employeedto) {
        Hr_holidays_summary_employee domain = hr_holidays_summary_employeeMapping.toDomain(hr_holidays_summary_employeedto);
		hr_holidays_summary_employeeService.create(domain);
        Hr_holidays_summary_employeeDTO dto = hr_holidays_summary_employeeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_holidays_summary_employeeMapping.toDomain(#hr_holidays_summary_employeedtos),'iBizBusinessCentral-Hr_holidays_summary_employee-Create')")
    @ApiOperation(value = "批量新建按员工的休假摘要报告", tags = {"按员工的休假摘要报告" },  notes = "批量新建按员工的休假摘要报告")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_employees/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_holidays_summary_employeeDTO> hr_holidays_summary_employeedtos) {
        hr_holidays_summary_employeeService.createBatch(hr_holidays_summary_employeeMapping.toDomain(hr_holidays_summary_employeedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_holidays_summary_employee" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_holidays_summary_employeeService.get(#hr_holidays_summary_employee_id),'iBizBusinessCentral-Hr_holidays_summary_employee-Update')")
    @ApiOperation(value = "更新按员工的休假摘要报告", tags = {"按员工的休假摘要报告" },  notes = "更新按员工的休假摘要报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_holidays_summary_employees/{hr_holidays_summary_employee_id}")
    public ResponseEntity<Hr_holidays_summary_employeeDTO> update(@PathVariable("hr_holidays_summary_employee_id") Long hr_holidays_summary_employee_id, @RequestBody Hr_holidays_summary_employeeDTO hr_holidays_summary_employeedto) {
		Hr_holidays_summary_employee domain  = hr_holidays_summary_employeeMapping.toDomain(hr_holidays_summary_employeedto);
        domain .setId(hr_holidays_summary_employee_id);
		hr_holidays_summary_employeeService.update(domain );
		Hr_holidays_summary_employeeDTO dto = hr_holidays_summary_employeeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_holidays_summary_employeeService.getHrHolidaysSummaryEmployeeByEntities(this.hr_holidays_summary_employeeMapping.toDomain(#hr_holidays_summary_employeedtos)),'iBizBusinessCentral-Hr_holidays_summary_employee-Update')")
    @ApiOperation(value = "批量更新按员工的休假摘要报告", tags = {"按员工的休假摘要报告" },  notes = "批量更新按员工的休假摘要报告")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_holidays_summary_employees/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_holidays_summary_employeeDTO> hr_holidays_summary_employeedtos) {
        hr_holidays_summary_employeeService.updateBatch(hr_holidays_summary_employeeMapping.toDomain(hr_holidays_summary_employeedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_holidays_summary_employeeService.get(#hr_holidays_summary_employee_id),'iBizBusinessCentral-Hr_holidays_summary_employee-Remove')")
    @ApiOperation(value = "删除按员工的休假摘要报告", tags = {"按员工的休假摘要报告" },  notes = "删除按员工的休假摘要报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_holidays_summary_employees/{hr_holidays_summary_employee_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_holidays_summary_employee_id") Long hr_holidays_summary_employee_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_holidays_summary_employeeService.remove(hr_holidays_summary_employee_id));
    }

    @PreAuthorize("hasPermission(this.hr_holidays_summary_employeeService.getHrHolidaysSummaryEmployeeByIds(#ids),'iBizBusinessCentral-Hr_holidays_summary_employee-Remove')")
    @ApiOperation(value = "批量删除按员工的休假摘要报告", tags = {"按员工的休假摘要报告" },  notes = "批量删除按员工的休假摘要报告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_holidays_summary_employees/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_holidays_summary_employeeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_holidays_summary_employeeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_holidays_summary_employee-Get')")
    @ApiOperation(value = "获取按员工的休假摘要报告", tags = {"按员工的休假摘要报告" },  notes = "获取按员工的休假摘要报告")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_holidays_summary_employees/{hr_holidays_summary_employee_id}")
    public ResponseEntity<Hr_holidays_summary_employeeDTO> get(@PathVariable("hr_holidays_summary_employee_id") Long hr_holidays_summary_employee_id) {
        Hr_holidays_summary_employee domain = hr_holidays_summary_employeeService.get(hr_holidays_summary_employee_id);
        Hr_holidays_summary_employeeDTO dto = hr_holidays_summary_employeeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取按员工的休假摘要报告草稿", tags = {"按员工的休假摘要报告" },  notes = "获取按员工的休假摘要报告草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_holidays_summary_employees/getdraft")
    public ResponseEntity<Hr_holidays_summary_employeeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_holidays_summary_employeeMapping.toDto(hr_holidays_summary_employeeService.getDraft(new Hr_holidays_summary_employee())));
    }

    @ApiOperation(value = "检查按员工的休假摘要报告", tags = {"按员工的休假摘要报告" },  notes = "检查按员工的休假摘要报告")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_employees/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_holidays_summary_employeeDTO hr_holidays_summary_employeedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_holidays_summary_employeeService.checkKey(hr_holidays_summary_employeeMapping.toDomain(hr_holidays_summary_employeedto)));
    }

    @PreAuthorize("hasPermission(this.hr_holidays_summary_employeeMapping.toDomain(#hr_holidays_summary_employeedto),'iBizBusinessCentral-Hr_holidays_summary_employee-Save')")
    @ApiOperation(value = "保存按员工的休假摘要报告", tags = {"按员工的休假摘要报告" },  notes = "保存按员工的休假摘要报告")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_employees/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_holidays_summary_employeeDTO hr_holidays_summary_employeedto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_holidays_summary_employeeService.save(hr_holidays_summary_employeeMapping.toDomain(hr_holidays_summary_employeedto)));
    }

    @PreAuthorize("hasPermission(this.hr_holidays_summary_employeeMapping.toDomain(#hr_holidays_summary_employeedtos),'iBizBusinessCentral-Hr_holidays_summary_employee-Save')")
    @ApiOperation(value = "批量保存按员工的休假摘要报告", tags = {"按员工的休假摘要报告" },  notes = "批量保存按员工的休假摘要报告")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_employees/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_holidays_summary_employeeDTO> hr_holidays_summary_employeedtos) {
        hr_holidays_summary_employeeService.saveBatch(hr_holidays_summary_employeeMapping.toDomain(hr_holidays_summary_employeedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_holidays_summary_employee-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_holidays_summary_employee-Get')")
	@ApiOperation(value = "获取数据集", tags = {"按员工的休假摘要报告" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_holidays_summary_employees/fetchdefault")
	public ResponseEntity<List<Hr_holidays_summary_employeeDTO>> fetchDefault(Hr_holidays_summary_employeeSearchContext context) {
        Page<Hr_holidays_summary_employee> domains = hr_holidays_summary_employeeService.searchDefault(context) ;
        List<Hr_holidays_summary_employeeDTO> list = hr_holidays_summary_employeeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_holidays_summary_employee-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_holidays_summary_employee-Get')")
	@ApiOperation(value = "查询数据集", tags = {"按员工的休假摘要报告" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_holidays_summary_employees/searchdefault")
	public ResponseEntity<Page<Hr_holidays_summary_employeeDTO>> searchDefault(@RequestBody Hr_holidays_summary_employeeSearchContext context) {
        Page<Hr_holidays_summary_employee> domains = hr_holidays_summary_employeeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_holidays_summary_employeeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

