package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge;
import cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_badgeService;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_badgeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"游戏化徽章" })
@RestController("Core-gamification_badge")
@RequestMapping("")
public class Gamification_badgeResource {

    @Autowired
    public IGamification_badgeService gamification_badgeService;

    @Autowired
    @Lazy
    public Gamification_badgeMapping gamification_badgeMapping;

    @PreAuthorize("hasPermission(this.gamification_badgeMapping.toDomain(#gamification_badgedto),'iBizBusinessCentral-Gamification_badge-Create')")
    @ApiOperation(value = "新建游戏化徽章", tags = {"游戏化徽章" },  notes = "新建游戏化徽章")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badges")
    public ResponseEntity<Gamification_badgeDTO> create(@Validated @RequestBody Gamification_badgeDTO gamification_badgedto) {
        Gamification_badge domain = gamification_badgeMapping.toDomain(gamification_badgedto);
		gamification_badgeService.create(domain);
        Gamification_badgeDTO dto = gamification_badgeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_badgeMapping.toDomain(#gamification_badgedtos),'iBizBusinessCentral-Gamification_badge-Create')")
    @ApiOperation(value = "批量新建游戏化徽章", tags = {"游戏化徽章" },  notes = "批量新建游戏化徽章")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badges/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_badgeDTO> gamification_badgedtos) {
        gamification_badgeService.createBatch(gamification_badgeMapping.toDomain(gamification_badgedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "gamification_badge" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.gamification_badgeService.get(#gamification_badge_id),'iBizBusinessCentral-Gamification_badge-Update')")
    @ApiOperation(value = "更新游戏化徽章", tags = {"游戏化徽章" },  notes = "更新游戏化徽章")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_badges/{gamification_badge_id}")
    public ResponseEntity<Gamification_badgeDTO> update(@PathVariable("gamification_badge_id") Long gamification_badge_id, @RequestBody Gamification_badgeDTO gamification_badgedto) {
		Gamification_badge domain  = gamification_badgeMapping.toDomain(gamification_badgedto);
        domain .setId(gamification_badge_id);
		gamification_badgeService.update(domain );
		Gamification_badgeDTO dto = gamification_badgeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_badgeService.getGamificationBadgeByEntities(this.gamification_badgeMapping.toDomain(#gamification_badgedtos)),'iBizBusinessCentral-Gamification_badge-Update')")
    @ApiOperation(value = "批量更新游戏化徽章", tags = {"游戏化徽章" },  notes = "批量更新游戏化徽章")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_badges/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_badgeDTO> gamification_badgedtos) {
        gamification_badgeService.updateBatch(gamification_badgeMapping.toDomain(gamification_badgedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.gamification_badgeService.get(#gamification_badge_id),'iBizBusinessCentral-Gamification_badge-Remove')")
    @ApiOperation(value = "删除游戏化徽章", tags = {"游戏化徽章" },  notes = "删除游戏化徽章")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badges/{gamification_badge_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("gamification_badge_id") Long gamification_badge_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_badgeService.remove(gamification_badge_id));
    }

    @PreAuthorize("hasPermission(this.gamification_badgeService.getGamificationBadgeByIds(#ids),'iBizBusinessCentral-Gamification_badge-Remove')")
    @ApiOperation(value = "批量删除游戏化徽章", tags = {"游戏化徽章" },  notes = "批量删除游戏化徽章")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badges/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        gamification_badgeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.gamification_badgeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Gamification_badge-Get')")
    @ApiOperation(value = "获取游戏化徽章", tags = {"游戏化徽章" },  notes = "获取游戏化徽章")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_badges/{gamification_badge_id}")
    public ResponseEntity<Gamification_badgeDTO> get(@PathVariable("gamification_badge_id") Long gamification_badge_id) {
        Gamification_badge domain = gamification_badgeService.get(gamification_badge_id);
        Gamification_badgeDTO dto = gamification_badgeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取游戏化徽章草稿", tags = {"游戏化徽章" },  notes = "获取游戏化徽章草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_badges/getdraft")
    public ResponseEntity<Gamification_badgeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_badgeMapping.toDto(gamification_badgeService.getDraft(new Gamification_badge())));
    }

    @ApiOperation(value = "检查游戏化徽章", tags = {"游戏化徽章" },  notes = "检查游戏化徽章")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badges/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Gamification_badgeDTO gamification_badgedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(gamification_badgeService.checkKey(gamification_badgeMapping.toDomain(gamification_badgedto)));
    }

    @PreAuthorize("hasPermission(this.gamification_badgeMapping.toDomain(#gamification_badgedto),'iBizBusinessCentral-Gamification_badge-Save')")
    @ApiOperation(value = "保存游戏化徽章", tags = {"游戏化徽章" },  notes = "保存游戏化徽章")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badges/save")
    public ResponseEntity<Boolean> save(@RequestBody Gamification_badgeDTO gamification_badgedto) {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_badgeService.save(gamification_badgeMapping.toDomain(gamification_badgedto)));
    }

    @PreAuthorize("hasPermission(this.gamification_badgeMapping.toDomain(#gamification_badgedtos),'iBizBusinessCentral-Gamification_badge-Save')")
    @ApiOperation(value = "批量保存游戏化徽章", tags = {"游戏化徽章" },  notes = "批量保存游戏化徽章")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badges/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Gamification_badgeDTO> gamification_badgedtos) {
        gamification_badgeService.saveBatch(gamification_badgeMapping.toDomain(gamification_badgedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_badge-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_badge-Get')")
	@ApiOperation(value = "获取数据集", tags = {"游戏化徽章" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_badges/fetchdefault")
	public ResponseEntity<List<Gamification_badgeDTO>> fetchDefault(Gamification_badgeSearchContext context) {
        Page<Gamification_badge> domains = gamification_badgeService.searchDefault(context) ;
        List<Gamification_badgeDTO> list = gamification_badgeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_badge-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_badge-Get')")
	@ApiOperation(value = "查询数据集", tags = {"游戏化徽章" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/gamification_badges/searchdefault")
	public ResponseEntity<Page<Gamification_badgeDTO>> searchDefault(@RequestBody Gamification_badgeSearchContext context) {
        Page<Gamification_badge> domains = gamification_badgeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_badgeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

