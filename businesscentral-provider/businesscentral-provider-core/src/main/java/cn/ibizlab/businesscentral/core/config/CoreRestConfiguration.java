package cn.ibizlab.businesscentral.core.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.businesscentral.core")
public class CoreRestConfiguration {

}
