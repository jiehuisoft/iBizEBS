package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_partner_merge_line;
import cn.ibizlab.businesscentral.core.odoo_base.service.IBase_partner_merge_lineService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_partner_merge_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"合并合作伙伴明细行" })
@RestController("Core-base_partner_merge_line")
@RequestMapping("")
public class Base_partner_merge_lineResource {

    @Autowired
    public IBase_partner_merge_lineService base_partner_merge_lineService;

    @Autowired
    @Lazy
    public Base_partner_merge_lineMapping base_partner_merge_lineMapping;

    @PreAuthorize("hasPermission(this.base_partner_merge_lineMapping.toDomain(#base_partner_merge_linedto),'iBizBusinessCentral-Base_partner_merge_line-Create')")
    @ApiOperation(value = "新建合并合作伙伴明细行", tags = {"合并合作伙伴明细行" },  notes = "新建合并合作伙伴明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_lines")
    public ResponseEntity<Base_partner_merge_lineDTO> create(@Validated @RequestBody Base_partner_merge_lineDTO base_partner_merge_linedto) {
        Base_partner_merge_line domain = base_partner_merge_lineMapping.toDomain(base_partner_merge_linedto);
		base_partner_merge_lineService.create(domain);
        Base_partner_merge_lineDTO dto = base_partner_merge_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_partner_merge_lineMapping.toDomain(#base_partner_merge_linedtos),'iBizBusinessCentral-Base_partner_merge_line-Create')")
    @ApiOperation(value = "批量新建合并合作伙伴明细行", tags = {"合并合作伙伴明细行" },  notes = "批量新建合并合作伙伴明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_partner_merge_lineDTO> base_partner_merge_linedtos) {
        base_partner_merge_lineService.createBatch(base_partner_merge_lineMapping.toDomain(base_partner_merge_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_partner_merge_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_partner_merge_lineService.get(#base_partner_merge_line_id),'iBizBusinessCentral-Base_partner_merge_line-Update')")
    @ApiOperation(value = "更新合并合作伙伴明细行", tags = {"合并合作伙伴明细行" },  notes = "更新合并合作伙伴明细行")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_partner_merge_lines/{base_partner_merge_line_id}")
    public ResponseEntity<Base_partner_merge_lineDTO> update(@PathVariable("base_partner_merge_line_id") Long base_partner_merge_line_id, @RequestBody Base_partner_merge_lineDTO base_partner_merge_linedto) {
		Base_partner_merge_line domain  = base_partner_merge_lineMapping.toDomain(base_partner_merge_linedto);
        domain .setId(base_partner_merge_line_id);
		base_partner_merge_lineService.update(domain );
		Base_partner_merge_lineDTO dto = base_partner_merge_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_partner_merge_lineService.getBasePartnerMergeLineByEntities(this.base_partner_merge_lineMapping.toDomain(#base_partner_merge_linedtos)),'iBizBusinessCentral-Base_partner_merge_line-Update')")
    @ApiOperation(value = "批量更新合并合作伙伴明细行", tags = {"合并合作伙伴明细行" },  notes = "批量更新合并合作伙伴明细行")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_partner_merge_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_partner_merge_lineDTO> base_partner_merge_linedtos) {
        base_partner_merge_lineService.updateBatch(base_partner_merge_lineMapping.toDomain(base_partner_merge_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_partner_merge_lineService.get(#base_partner_merge_line_id),'iBizBusinessCentral-Base_partner_merge_line-Remove')")
    @ApiOperation(value = "删除合并合作伙伴明细行", tags = {"合并合作伙伴明细行" },  notes = "删除合并合作伙伴明细行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_partner_merge_lines/{base_partner_merge_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_partner_merge_line_id") Long base_partner_merge_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_partner_merge_lineService.remove(base_partner_merge_line_id));
    }

    @PreAuthorize("hasPermission(this.base_partner_merge_lineService.getBasePartnerMergeLineByIds(#ids),'iBizBusinessCentral-Base_partner_merge_line-Remove')")
    @ApiOperation(value = "批量删除合并合作伙伴明细行", tags = {"合并合作伙伴明细行" },  notes = "批量删除合并合作伙伴明细行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_partner_merge_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_partner_merge_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_partner_merge_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_partner_merge_line-Get')")
    @ApiOperation(value = "获取合并合作伙伴明细行", tags = {"合并合作伙伴明细行" },  notes = "获取合并合作伙伴明细行")
	@RequestMapping(method = RequestMethod.GET, value = "/base_partner_merge_lines/{base_partner_merge_line_id}")
    public ResponseEntity<Base_partner_merge_lineDTO> get(@PathVariable("base_partner_merge_line_id") Long base_partner_merge_line_id) {
        Base_partner_merge_line domain = base_partner_merge_lineService.get(base_partner_merge_line_id);
        Base_partner_merge_lineDTO dto = base_partner_merge_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取合并合作伙伴明细行草稿", tags = {"合并合作伙伴明细行" },  notes = "获取合并合作伙伴明细行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_partner_merge_lines/getdraft")
    public ResponseEntity<Base_partner_merge_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_partner_merge_lineMapping.toDto(base_partner_merge_lineService.getDraft(new Base_partner_merge_line())));
    }

    @ApiOperation(value = "检查合并合作伙伴明细行", tags = {"合并合作伙伴明细行" },  notes = "检查合并合作伙伴明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_partner_merge_lineDTO base_partner_merge_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_partner_merge_lineService.checkKey(base_partner_merge_lineMapping.toDomain(base_partner_merge_linedto)));
    }

    @PreAuthorize("hasPermission(this.base_partner_merge_lineMapping.toDomain(#base_partner_merge_linedto),'iBizBusinessCentral-Base_partner_merge_line-Save')")
    @ApiOperation(value = "保存合并合作伙伴明细行", tags = {"合并合作伙伴明细行" },  notes = "保存合并合作伙伴明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_partner_merge_lineDTO base_partner_merge_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_partner_merge_lineService.save(base_partner_merge_lineMapping.toDomain(base_partner_merge_linedto)));
    }

    @PreAuthorize("hasPermission(this.base_partner_merge_lineMapping.toDomain(#base_partner_merge_linedtos),'iBizBusinessCentral-Base_partner_merge_line-Save')")
    @ApiOperation(value = "批量保存合并合作伙伴明细行", tags = {"合并合作伙伴明细行" },  notes = "批量保存合并合作伙伴明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_partner_merge_lineDTO> base_partner_merge_linedtos) {
        base_partner_merge_lineService.saveBatch(base_partner_merge_lineMapping.toDomain(base_partner_merge_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_partner_merge_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_partner_merge_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"合并合作伙伴明细行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_partner_merge_lines/fetchdefault")
	public ResponseEntity<List<Base_partner_merge_lineDTO>> fetchDefault(Base_partner_merge_lineSearchContext context) {
        Page<Base_partner_merge_line> domains = base_partner_merge_lineService.searchDefault(context) ;
        List<Base_partner_merge_lineDTO> list = base_partner_merge_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_partner_merge_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_partner_merge_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"合并合作伙伴明细行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_partner_merge_lines/searchdefault")
	public ResponseEntity<Page<Base_partner_merge_lineDTO>> searchDefault(@RequestBody Base_partner_merge_lineSearchContext context) {
        Page<Base_partner_merge_line> domains = base_partner_merge_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_partner_merge_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

