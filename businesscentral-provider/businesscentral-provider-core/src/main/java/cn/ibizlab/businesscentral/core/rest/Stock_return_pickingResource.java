package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_return_picking;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_return_pickingService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_return_pickingSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"退回拣货" })
@RestController("Core-stock_return_picking")
@RequestMapping("")
public class Stock_return_pickingResource {

    @Autowired
    public IStock_return_pickingService stock_return_pickingService;

    @Autowired
    @Lazy
    public Stock_return_pickingMapping stock_return_pickingMapping;

    @PreAuthorize("hasPermission(this.stock_return_pickingMapping.toDomain(#stock_return_pickingdto),'iBizBusinessCentral-Stock_return_picking-Create')")
    @ApiOperation(value = "新建退回拣货", tags = {"退回拣货" },  notes = "新建退回拣货")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_return_pickings")
    public ResponseEntity<Stock_return_pickingDTO> create(@Validated @RequestBody Stock_return_pickingDTO stock_return_pickingdto) {
        Stock_return_picking domain = stock_return_pickingMapping.toDomain(stock_return_pickingdto);
		stock_return_pickingService.create(domain);
        Stock_return_pickingDTO dto = stock_return_pickingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_return_pickingMapping.toDomain(#stock_return_pickingdtos),'iBizBusinessCentral-Stock_return_picking-Create')")
    @ApiOperation(value = "批量新建退回拣货", tags = {"退回拣货" },  notes = "批量新建退回拣货")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_return_pickings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_return_pickingDTO> stock_return_pickingdtos) {
        stock_return_pickingService.createBatch(stock_return_pickingMapping.toDomain(stock_return_pickingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_return_picking" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_return_pickingService.get(#stock_return_picking_id),'iBizBusinessCentral-Stock_return_picking-Update')")
    @ApiOperation(value = "更新退回拣货", tags = {"退回拣货" },  notes = "更新退回拣货")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_return_pickings/{stock_return_picking_id}")
    public ResponseEntity<Stock_return_pickingDTO> update(@PathVariable("stock_return_picking_id") Long stock_return_picking_id, @RequestBody Stock_return_pickingDTO stock_return_pickingdto) {
		Stock_return_picking domain  = stock_return_pickingMapping.toDomain(stock_return_pickingdto);
        domain .setId(stock_return_picking_id);
		stock_return_pickingService.update(domain );
		Stock_return_pickingDTO dto = stock_return_pickingMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_return_pickingService.getStockReturnPickingByEntities(this.stock_return_pickingMapping.toDomain(#stock_return_pickingdtos)),'iBizBusinessCentral-Stock_return_picking-Update')")
    @ApiOperation(value = "批量更新退回拣货", tags = {"退回拣货" },  notes = "批量更新退回拣货")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_return_pickings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_return_pickingDTO> stock_return_pickingdtos) {
        stock_return_pickingService.updateBatch(stock_return_pickingMapping.toDomain(stock_return_pickingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_return_pickingService.get(#stock_return_picking_id),'iBizBusinessCentral-Stock_return_picking-Remove')")
    @ApiOperation(value = "删除退回拣货", tags = {"退回拣货" },  notes = "删除退回拣货")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_return_pickings/{stock_return_picking_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_return_picking_id") Long stock_return_picking_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_return_pickingService.remove(stock_return_picking_id));
    }

    @PreAuthorize("hasPermission(this.stock_return_pickingService.getStockReturnPickingByIds(#ids),'iBizBusinessCentral-Stock_return_picking-Remove')")
    @ApiOperation(value = "批量删除退回拣货", tags = {"退回拣货" },  notes = "批量删除退回拣货")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_return_pickings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_return_pickingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_return_pickingMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_return_picking-Get')")
    @ApiOperation(value = "获取退回拣货", tags = {"退回拣货" },  notes = "获取退回拣货")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_return_pickings/{stock_return_picking_id}")
    public ResponseEntity<Stock_return_pickingDTO> get(@PathVariable("stock_return_picking_id") Long stock_return_picking_id) {
        Stock_return_picking domain = stock_return_pickingService.get(stock_return_picking_id);
        Stock_return_pickingDTO dto = stock_return_pickingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取退回拣货草稿", tags = {"退回拣货" },  notes = "获取退回拣货草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_return_pickings/getdraft")
    public ResponseEntity<Stock_return_pickingDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_return_pickingMapping.toDto(stock_return_pickingService.getDraft(new Stock_return_picking())));
    }

    @ApiOperation(value = "检查退回拣货", tags = {"退回拣货" },  notes = "检查退回拣货")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_return_pickings/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_return_pickingDTO stock_return_pickingdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_return_pickingService.checkKey(stock_return_pickingMapping.toDomain(stock_return_pickingdto)));
    }

    @PreAuthorize("hasPermission(this.stock_return_pickingMapping.toDomain(#stock_return_pickingdto),'iBizBusinessCentral-Stock_return_picking-Save')")
    @ApiOperation(value = "保存退回拣货", tags = {"退回拣货" },  notes = "保存退回拣货")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_return_pickings/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_return_pickingDTO stock_return_pickingdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_return_pickingService.save(stock_return_pickingMapping.toDomain(stock_return_pickingdto)));
    }

    @PreAuthorize("hasPermission(this.stock_return_pickingMapping.toDomain(#stock_return_pickingdtos),'iBizBusinessCentral-Stock_return_picking-Save')")
    @ApiOperation(value = "批量保存退回拣货", tags = {"退回拣货" },  notes = "批量保存退回拣货")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_return_pickings/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_return_pickingDTO> stock_return_pickingdtos) {
        stock_return_pickingService.saveBatch(stock_return_pickingMapping.toDomain(stock_return_pickingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_return_picking-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_return_picking-Get')")
	@ApiOperation(value = "获取数据集", tags = {"退回拣货" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_return_pickings/fetchdefault")
	public ResponseEntity<List<Stock_return_pickingDTO>> fetchDefault(Stock_return_pickingSearchContext context) {
        Page<Stock_return_picking> domains = stock_return_pickingService.searchDefault(context) ;
        List<Stock_return_pickingDTO> list = stock_return_pickingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_return_picking-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_return_picking-Get')")
	@ApiOperation(value = "查询数据集", tags = {"退回拣货" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_return_pickings/searchdefault")
	public ResponseEntity<Page<Stock_return_pickingDTO>> searchDefault(@RequestBody Stock_return_pickingSearchContext context) {
        Page<Stock_return_picking> domains = stock_return_pickingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_return_pickingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

