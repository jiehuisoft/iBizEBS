package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template_line;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_template_lineService;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_template_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"报价单模板行" })
@RestController("Core-sale_order_template_line")
@RequestMapping("")
public class Sale_order_template_lineResource {

    @Autowired
    public ISale_order_template_lineService sale_order_template_lineService;

    @Autowired
    @Lazy
    public Sale_order_template_lineMapping sale_order_template_lineMapping;

    @PreAuthorize("hasPermission(this.sale_order_template_lineMapping.toDomain(#sale_order_template_linedto),'iBizBusinessCentral-Sale_order_template_line-Create')")
    @ApiOperation(value = "新建报价单模板行", tags = {"报价单模板行" },  notes = "新建报价单模板行")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_lines")
    public ResponseEntity<Sale_order_template_lineDTO> create(@Validated @RequestBody Sale_order_template_lineDTO sale_order_template_linedto) {
        Sale_order_template_line domain = sale_order_template_lineMapping.toDomain(sale_order_template_linedto);
		sale_order_template_lineService.create(domain);
        Sale_order_template_lineDTO dto = sale_order_template_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_order_template_lineMapping.toDomain(#sale_order_template_linedtos),'iBizBusinessCentral-Sale_order_template_line-Create')")
    @ApiOperation(value = "批量新建报价单模板行", tags = {"报价单模板行" },  notes = "批量新建报价单模板行")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sale_order_template_lineDTO> sale_order_template_linedtos) {
        sale_order_template_lineService.createBatch(sale_order_template_lineMapping.toDomain(sale_order_template_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "sale_order_template_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.sale_order_template_lineService.get(#sale_order_template_line_id),'iBizBusinessCentral-Sale_order_template_line-Update')")
    @ApiOperation(value = "更新报价单模板行", tags = {"报价单模板行" },  notes = "更新报价单模板行")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_order_template_lines/{sale_order_template_line_id}")
    public ResponseEntity<Sale_order_template_lineDTO> update(@PathVariable("sale_order_template_line_id") Long sale_order_template_line_id, @RequestBody Sale_order_template_lineDTO sale_order_template_linedto) {
		Sale_order_template_line domain  = sale_order_template_lineMapping.toDomain(sale_order_template_linedto);
        domain .setId(sale_order_template_line_id);
		sale_order_template_lineService.update(domain );
		Sale_order_template_lineDTO dto = sale_order_template_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_order_template_lineService.getSaleOrderTemplateLineByEntities(this.sale_order_template_lineMapping.toDomain(#sale_order_template_linedtos)),'iBizBusinessCentral-Sale_order_template_line-Update')")
    @ApiOperation(value = "批量更新报价单模板行", tags = {"报价单模板行" },  notes = "批量更新报价单模板行")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_order_template_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_order_template_lineDTO> sale_order_template_linedtos) {
        sale_order_template_lineService.updateBatch(sale_order_template_lineMapping.toDomain(sale_order_template_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.sale_order_template_lineService.get(#sale_order_template_line_id),'iBizBusinessCentral-Sale_order_template_line-Remove')")
    @ApiOperation(value = "删除报价单模板行", tags = {"报价单模板行" },  notes = "删除报价单模板行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_template_lines/{sale_order_template_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("sale_order_template_line_id") Long sale_order_template_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sale_order_template_lineService.remove(sale_order_template_line_id));
    }

    @PreAuthorize("hasPermission(this.sale_order_template_lineService.getSaleOrderTemplateLineByIds(#ids),'iBizBusinessCentral-Sale_order_template_line-Remove')")
    @ApiOperation(value = "批量删除报价单模板行", tags = {"报价单模板行" },  notes = "批量删除报价单模板行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_template_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        sale_order_template_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.sale_order_template_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Sale_order_template_line-Get')")
    @ApiOperation(value = "获取报价单模板行", tags = {"报价单模板行" },  notes = "获取报价单模板行")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_order_template_lines/{sale_order_template_line_id}")
    public ResponseEntity<Sale_order_template_lineDTO> get(@PathVariable("sale_order_template_line_id") Long sale_order_template_line_id) {
        Sale_order_template_line domain = sale_order_template_lineService.get(sale_order_template_line_id);
        Sale_order_template_lineDTO dto = sale_order_template_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取报价单模板行草稿", tags = {"报价单模板行" },  notes = "获取报价单模板行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_order_template_lines/getdraft")
    public ResponseEntity<Sale_order_template_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(sale_order_template_lineMapping.toDto(sale_order_template_lineService.getDraft(new Sale_order_template_line())));
    }

    @ApiOperation(value = "检查报价单模板行", tags = {"报价单模板行" },  notes = "检查报价单模板行")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sale_order_template_lineDTO sale_order_template_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(sale_order_template_lineService.checkKey(sale_order_template_lineMapping.toDomain(sale_order_template_linedto)));
    }

    @PreAuthorize("hasPermission(this.sale_order_template_lineMapping.toDomain(#sale_order_template_linedto),'iBizBusinessCentral-Sale_order_template_line-Save')")
    @ApiOperation(value = "保存报价单模板行", tags = {"报价单模板行" },  notes = "保存报价单模板行")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Sale_order_template_lineDTO sale_order_template_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(sale_order_template_lineService.save(sale_order_template_lineMapping.toDomain(sale_order_template_linedto)));
    }

    @PreAuthorize("hasPermission(this.sale_order_template_lineMapping.toDomain(#sale_order_template_linedtos),'iBizBusinessCentral-Sale_order_template_line-Save')")
    @ApiOperation(value = "批量保存报价单模板行", tags = {"报价单模板行" },  notes = "批量保存报价单模板行")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Sale_order_template_lineDTO> sale_order_template_linedtos) {
        sale_order_template_lineService.saveBatch(sale_order_template_lineMapping.toDomain(sale_order_template_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_order_template_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_order_template_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"报价单模板行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/sale_order_template_lines/fetchdefault")
	public ResponseEntity<List<Sale_order_template_lineDTO>> fetchDefault(Sale_order_template_lineSearchContext context) {
        Page<Sale_order_template_line> domains = sale_order_template_lineService.searchDefault(context) ;
        List<Sale_order_template_lineDTO> list = sale_order_template_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_order_template_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_order_template_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"报价单模板行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/sale_order_template_lines/searchdefault")
	public ResponseEntity<Page<Sale_order_template_lineDTO>> searchDefault(@RequestBody Sale_order_template_lineSearchContext context) {
        Page<Sale_order_template_line> domains = sale_order_template_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sale_order_template_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

