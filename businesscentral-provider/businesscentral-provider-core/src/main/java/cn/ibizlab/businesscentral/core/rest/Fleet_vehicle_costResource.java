package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_costService;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_costSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"车辆相关的费用" })
@RestController("Core-fleet_vehicle_cost")
@RequestMapping("")
public class Fleet_vehicle_costResource {

    @Autowired
    public IFleet_vehicle_costService fleet_vehicle_costService;

    @Autowired
    @Lazy
    public Fleet_vehicle_costMapping fleet_vehicle_costMapping;

    @PreAuthorize("hasPermission(this.fleet_vehicle_costMapping.toDomain(#fleet_vehicle_costdto),'iBizBusinessCentral-Fleet_vehicle_cost-Create')")
    @ApiOperation(value = "新建车辆相关的费用", tags = {"车辆相关的费用" },  notes = "新建车辆相关的费用")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs")
    public ResponseEntity<Fleet_vehicle_costDTO> create(@Validated @RequestBody Fleet_vehicle_costDTO fleet_vehicle_costdto) {
        Fleet_vehicle_cost domain = fleet_vehicle_costMapping.toDomain(fleet_vehicle_costdto);
		fleet_vehicle_costService.create(domain);
        Fleet_vehicle_costDTO dto = fleet_vehicle_costMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_costMapping.toDomain(#fleet_vehicle_costdtos),'iBizBusinessCentral-Fleet_vehicle_cost-Create')")
    @ApiOperation(value = "批量新建车辆相关的费用", tags = {"车辆相关的费用" },  notes = "批量新建车辆相关的费用")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_costDTO> fleet_vehicle_costdtos) {
        fleet_vehicle_costService.createBatch(fleet_vehicle_costMapping.toDomain(fleet_vehicle_costdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fleet_vehicle_cost" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.fleet_vehicle_costService.get(#fleet_vehicle_cost_id),'iBizBusinessCentral-Fleet_vehicle_cost-Update')")
    @ApiOperation(value = "更新车辆相关的费用", tags = {"车辆相关的费用" },  notes = "更新车辆相关的费用")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_costs/{fleet_vehicle_cost_id}")
    public ResponseEntity<Fleet_vehicle_costDTO> update(@PathVariable("fleet_vehicle_cost_id") Long fleet_vehicle_cost_id, @RequestBody Fleet_vehicle_costDTO fleet_vehicle_costdto) {
		Fleet_vehicle_cost domain  = fleet_vehicle_costMapping.toDomain(fleet_vehicle_costdto);
        domain .setId(fleet_vehicle_cost_id);
		fleet_vehicle_costService.update(domain );
		Fleet_vehicle_costDTO dto = fleet_vehicle_costMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_costService.getFleetVehicleCostByEntities(this.fleet_vehicle_costMapping.toDomain(#fleet_vehicle_costdtos)),'iBizBusinessCentral-Fleet_vehicle_cost-Update')")
    @ApiOperation(value = "批量更新车辆相关的费用", tags = {"车辆相关的费用" },  notes = "批量更新车辆相关的费用")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_costs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_costDTO> fleet_vehicle_costdtos) {
        fleet_vehicle_costService.updateBatch(fleet_vehicle_costMapping.toDomain(fleet_vehicle_costdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_costService.get(#fleet_vehicle_cost_id),'iBizBusinessCentral-Fleet_vehicle_cost-Remove')")
    @ApiOperation(value = "删除车辆相关的费用", tags = {"车辆相关的费用" },  notes = "删除车辆相关的费用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_costs/{fleet_vehicle_cost_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_cost_id") Long fleet_vehicle_cost_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_costService.remove(fleet_vehicle_cost_id));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_costService.getFleetVehicleCostByIds(#ids),'iBizBusinessCentral-Fleet_vehicle_cost-Remove')")
    @ApiOperation(value = "批量删除车辆相关的费用", tags = {"车辆相关的费用" },  notes = "批量删除车辆相关的费用")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_costs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        fleet_vehicle_costService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.fleet_vehicle_costMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fleet_vehicle_cost-Get')")
    @ApiOperation(value = "获取车辆相关的费用", tags = {"车辆相关的费用" },  notes = "获取车辆相关的费用")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_costs/{fleet_vehicle_cost_id}")
    public ResponseEntity<Fleet_vehicle_costDTO> get(@PathVariable("fleet_vehicle_cost_id") Long fleet_vehicle_cost_id) {
        Fleet_vehicle_cost domain = fleet_vehicle_costService.get(fleet_vehicle_cost_id);
        Fleet_vehicle_costDTO dto = fleet_vehicle_costMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取车辆相关的费用草稿", tags = {"车辆相关的费用" },  notes = "获取车辆相关的费用草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_costs/getdraft")
    public ResponseEntity<Fleet_vehicle_costDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_costMapping.toDto(fleet_vehicle_costService.getDraft(new Fleet_vehicle_cost())));
    }

    @ApiOperation(value = "检查车辆相关的费用", tags = {"车辆相关的费用" },  notes = "检查车辆相关的费用")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Fleet_vehicle_costDTO fleet_vehicle_costdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_costService.checkKey(fleet_vehicle_costMapping.toDomain(fleet_vehicle_costdto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_costMapping.toDomain(#fleet_vehicle_costdto),'iBizBusinessCentral-Fleet_vehicle_cost-Save')")
    @ApiOperation(value = "保存车辆相关的费用", tags = {"车辆相关的费用" },  notes = "保存车辆相关的费用")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs/save")
    public ResponseEntity<Boolean> save(@RequestBody Fleet_vehicle_costDTO fleet_vehicle_costdto) {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_costService.save(fleet_vehicle_costMapping.toDomain(fleet_vehicle_costdto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_costMapping.toDomain(#fleet_vehicle_costdtos),'iBizBusinessCentral-Fleet_vehicle_cost-Save')")
    @ApiOperation(value = "批量保存车辆相关的费用", tags = {"车辆相关的费用" },  notes = "批量保存车辆相关的费用")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Fleet_vehicle_costDTO> fleet_vehicle_costdtos) {
        fleet_vehicle_costService.saveBatch(fleet_vehicle_costMapping.toDomain(fleet_vehicle_costdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_cost-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_cost-Get')")
	@ApiOperation(value = "获取数据集", tags = {"车辆相关的费用" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_costs/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_costDTO>> fetchDefault(Fleet_vehicle_costSearchContext context) {
        Page<Fleet_vehicle_cost> domains = fleet_vehicle_costService.searchDefault(context) ;
        List<Fleet_vehicle_costDTO> list = fleet_vehicle_costMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_cost-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_cost-Get')")
	@ApiOperation(value = "查询数据集", tags = {"车辆相关的费用" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/fleet_vehicle_costs/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_costDTO>> searchDefault(@RequestBody Fleet_vehicle_costSearchContext context) {
        Page<Fleet_vehicle_cost> domains = fleet_vehicle_costService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_costMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

