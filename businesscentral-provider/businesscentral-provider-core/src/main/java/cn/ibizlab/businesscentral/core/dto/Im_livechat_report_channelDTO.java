package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Im_livechat_report_channelDTO]
 */
@Data
public class Im_livechat_report_channelDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DURATION]
     *
     */
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Double duration;

    /**
     * 属性 [START_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("start_date")
    private Timestamp startDate;

    /**
     * 属性 [START_DATE_HOUR]
     *
     */
    @JSONField(name = "start_date_hour")
    @JsonProperty("start_date_hour")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String startDateHour;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CHANNEL_NAME]
     *
     */
    @JSONField(name = "channel_name")
    @JsonProperty("channel_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String channelName;

    /**
     * 属性 [NBR_MESSAGE]
     *
     */
    @JSONField(name = "nbr_message")
    @JsonProperty("nbr_message")
    private Integer nbrMessage;

    /**
     * 属性 [UUID]
     *
     */
    @JSONField(name = "uuid")
    @JsonProperty("uuid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String uuid;

    /**
     * 属性 [TECHNICAL_NAME]
     *
     */
    @JSONField(name = "technical_name")
    @JsonProperty("technical_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String technicalName;

    /**
     * 属性 [NBR_SPEAKER]
     *
     */
    @JSONField(name = "nbr_speaker")
    @JsonProperty("nbr_speaker")
    private Integer nbrSpeaker;

    /**
     * 属性 [LIVECHAT_CHANNEL_ID_TEXT]
     *
     */
    @JSONField(name = "livechat_channel_id_text")
    @JsonProperty("livechat_channel_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String livechatChannelIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [CHANNEL_ID_TEXT]
     *
     */
    @JSONField(name = "channel_id_text")
    @JsonProperty("channel_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String channelIdText;

    /**
     * 属性 [CHANNEL_ID]
     *
     */
    @JSONField(name = "channel_id")
    @JsonProperty("channel_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long channelId;

    /**
     * 属性 [LIVECHAT_CHANNEL_ID]
     *
     */
    @JSONField(name = "livechat_channel_id")
    @JsonProperty("livechat_channel_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long livechatChannelId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;


    /**
     * 设置 [DURATION]
     */
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }

    /**
     * 设置 [START_DATE]
     */
    public void setStartDate(Timestamp  startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 设置 [START_DATE_HOUR]
     */
    public void setStartDateHour(String  startDateHour){
        this.startDateHour = startDateHour ;
        this.modify("start_date_hour",startDateHour);
    }

    /**
     * 设置 [CHANNEL_NAME]
     */
    public void setChannelName(String  channelName){
        this.channelName = channelName ;
        this.modify("channel_name",channelName);
    }

    /**
     * 设置 [NBR_MESSAGE]
     */
    public void setNbrMessage(Integer  nbrMessage){
        this.nbrMessage = nbrMessage ;
        this.modify("nbr_message",nbrMessage);
    }

    /**
     * 设置 [UUID]
     */
    public void setUuid(String  uuid){
        this.uuid = uuid ;
        this.modify("uuid",uuid);
    }

    /**
     * 设置 [TECHNICAL_NAME]
     */
    public void setTechnicalName(String  technicalName){
        this.technicalName = technicalName ;
        this.modify("technical_name",technicalName);
    }

    /**
     * 设置 [NBR_SPEAKER]
     */
    public void setNbrSpeaker(Integer  nbrSpeaker){
        this.nbrSpeaker = nbrSpeaker ;
        this.modify("nbr_speaker",nbrSpeaker);
    }

    /**
     * 设置 [CHANNEL_ID]
     */
    public void setChannelId(Long  channelId){
        this.channelId = channelId ;
        this.modify("channel_id",channelId);
    }

    /**
     * 设置 [LIVECHAT_CHANNEL_ID]
     */
    public void setLivechatChannelId(Long  livechatChannelId){
        this.livechatChannelId = livechatChannelId ;
        this.modify("livechat_channel_id",livechatChannelId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }


}


