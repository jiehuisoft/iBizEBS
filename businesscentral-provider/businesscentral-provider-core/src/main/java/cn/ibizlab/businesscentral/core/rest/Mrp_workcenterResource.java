package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenterService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenterSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工作中心" })
@RestController("Core-mrp_workcenter")
@RequestMapping("")
public class Mrp_workcenterResource {

    @Autowired
    public IMrp_workcenterService mrp_workcenterService;

    @Autowired
    @Lazy
    public Mrp_workcenterMapping mrp_workcenterMapping;

    @PreAuthorize("hasPermission(this.mrp_workcenterMapping.toDomain(#mrp_workcenterdto),'iBizBusinessCentral-Mrp_workcenter-Create')")
    @ApiOperation(value = "新建工作中心", tags = {"工作中心" },  notes = "新建工作中心")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters")
    public ResponseEntity<Mrp_workcenterDTO> create(@Validated @RequestBody Mrp_workcenterDTO mrp_workcenterdto) {
        Mrp_workcenter domain = mrp_workcenterMapping.toDomain(mrp_workcenterdto);
		mrp_workcenterService.create(domain);
        Mrp_workcenterDTO dto = mrp_workcenterMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_workcenterMapping.toDomain(#mrp_workcenterdtos),'iBizBusinessCentral-Mrp_workcenter-Create')")
    @ApiOperation(value = "批量新建工作中心", tags = {"工作中心" },  notes = "批量新建工作中心")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_workcenterDTO> mrp_workcenterdtos) {
        mrp_workcenterService.createBatch(mrp_workcenterMapping.toDomain(mrp_workcenterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_workcenter" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_workcenterService.get(#mrp_workcenter_id),'iBizBusinessCentral-Mrp_workcenter-Update')")
    @ApiOperation(value = "更新工作中心", tags = {"工作中心" },  notes = "更新工作中心")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenters/{mrp_workcenter_id}")
    public ResponseEntity<Mrp_workcenterDTO> update(@PathVariable("mrp_workcenter_id") Long mrp_workcenter_id, @RequestBody Mrp_workcenterDTO mrp_workcenterdto) {
		Mrp_workcenter domain  = mrp_workcenterMapping.toDomain(mrp_workcenterdto);
        domain .setId(mrp_workcenter_id);
		mrp_workcenterService.update(domain );
		Mrp_workcenterDTO dto = mrp_workcenterMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_workcenterService.getMrpWorkcenterByEntities(this.mrp_workcenterMapping.toDomain(#mrp_workcenterdtos)),'iBizBusinessCentral-Mrp_workcenter-Update')")
    @ApiOperation(value = "批量更新工作中心", tags = {"工作中心" },  notes = "批量更新工作中心")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenters/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workcenterDTO> mrp_workcenterdtos) {
        mrp_workcenterService.updateBatch(mrp_workcenterMapping.toDomain(mrp_workcenterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_workcenterService.get(#mrp_workcenter_id),'iBizBusinessCentral-Mrp_workcenter-Remove')")
    @ApiOperation(value = "删除工作中心", tags = {"工作中心" },  notes = "删除工作中心")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenters/{mrp_workcenter_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workcenter_id") Long mrp_workcenter_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenterService.remove(mrp_workcenter_id));
    }

    @PreAuthorize("hasPermission(this.mrp_workcenterService.getMrpWorkcenterByIds(#ids),'iBizBusinessCentral-Mrp_workcenter-Remove')")
    @ApiOperation(value = "批量删除工作中心", tags = {"工作中心" },  notes = "批量删除工作中心")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenters/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_workcenterService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_workcenterMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_workcenter-Get')")
    @ApiOperation(value = "获取工作中心", tags = {"工作中心" },  notes = "获取工作中心")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenters/{mrp_workcenter_id}")
    public ResponseEntity<Mrp_workcenterDTO> get(@PathVariable("mrp_workcenter_id") Long mrp_workcenter_id) {
        Mrp_workcenter domain = mrp_workcenterService.get(mrp_workcenter_id);
        Mrp_workcenterDTO dto = mrp_workcenterMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工作中心草稿", tags = {"工作中心" },  notes = "获取工作中心草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenters/getdraft")
    public ResponseEntity<Mrp_workcenterDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenterMapping.toDto(mrp_workcenterService.getDraft(new Mrp_workcenter())));
    }

    @ApiOperation(value = "检查工作中心", tags = {"工作中心" },  notes = "检查工作中心")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_workcenterDTO mrp_workcenterdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_workcenterService.checkKey(mrp_workcenterMapping.toDomain(mrp_workcenterdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_workcenterMapping.toDomain(#mrp_workcenterdto),'iBizBusinessCentral-Mrp_workcenter-Save')")
    @ApiOperation(value = "保存工作中心", tags = {"工作中心" },  notes = "保存工作中心")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_workcenterDTO mrp_workcenterdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenterService.save(mrp_workcenterMapping.toDomain(mrp_workcenterdto)));
    }

    @PreAuthorize("hasPermission(this.mrp_workcenterMapping.toDomain(#mrp_workcenterdtos),'iBizBusinessCentral-Mrp_workcenter-Save')")
    @ApiOperation(value = "批量保存工作中心", tags = {"工作中心" },  notes = "批量保存工作中心")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_workcenterDTO> mrp_workcenterdtos) {
        mrp_workcenterService.saveBatch(mrp_workcenterMapping.toDomain(mrp_workcenterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_workcenter-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_workcenter-Get')")
	@ApiOperation(value = "获取数据集", tags = {"工作中心" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workcenters/fetchdefault")
	public ResponseEntity<List<Mrp_workcenterDTO>> fetchDefault(Mrp_workcenterSearchContext context) {
        Page<Mrp_workcenter> domains = mrp_workcenterService.searchDefault(context) ;
        List<Mrp_workcenterDTO> list = mrp_workcenterMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_workcenter-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_workcenter-Get')")
	@ApiOperation(value = "查询数据集", tags = {"工作中心" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_workcenters/searchdefault")
	public ResponseEntity<Page<Mrp_workcenterDTO>> searchDefault(@RequestBody Mrp_workcenterSearchContext context) {
        Page<Mrp_workcenter> domains = mrp_workcenterService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_workcenterMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

