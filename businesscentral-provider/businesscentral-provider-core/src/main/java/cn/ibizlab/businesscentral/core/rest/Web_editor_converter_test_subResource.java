package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test_sub;
import cn.ibizlab.businesscentral.core.odoo_web_editor.service.IWeb_editor_converter_test_subService;
import cn.ibizlab.businesscentral.core.odoo_web_editor.filter.Web_editor_converter_test_subSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Web编辑器转换器子测试" })
@RestController("Core-web_editor_converter_test_sub")
@RequestMapping("")
public class Web_editor_converter_test_subResource {

    @Autowired
    public IWeb_editor_converter_test_subService web_editor_converter_test_subService;

    @Autowired
    @Lazy
    public Web_editor_converter_test_subMapping web_editor_converter_test_subMapping;

    @PreAuthorize("hasPermission(this.web_editor_converter_test_subMapping.toDomain(#web_editor_converter_test_subdto),'iBizBusinessCentral-Web_editor_converter_test_sub-Create')")
    @ApiOperation(value = "新建Web编辑器转换器子测试", tags = {"Web编辑器转换器子测试" },  notes = "新建Web编辑器转换器子测试")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs")
    public ResponseEntity<Web_editor_converter_test_subDTO> create(@Validated @RequestBody Web_editor_converter_test_subDTO web_editor_converter_test_subdto) {
        Web_editor_converter_test_sub domain = web_editor_converter_test_subMapping.toDomain(web_editor_converter_test_subdto);
		web_editor_converter_test_subService.create(domain);
        Web_editor_converter_test_subDTO dto = web_editor_converter_test_subMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.web_editor_converter_test_subMapping.toDomain(#web_editor_converter_test_subdtos),'iBizBusinessCentral-Web_editor_converter_test_sub-Create')")
    @ApiOperation(value = "批量新建Web编辑器转换器子测试", tags = {"Web编辑器转换器子测试" },  notes = "批量新建Web编辑器转换器子测试")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Web_editor_converter_test_subDTO> web_editor_converter_test_subdtos) {
        web_editor_converter_test_subService.createBatch(web_editor_converter_test_subMapping.toDomain(web_editor_converter_test_subdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "web_editor_converter_test_sub" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.web_editor_converter_test_subService.get(#web_editor_converter_test_sub_id),'iBizBusinessCentral-Web_editor_converter_test_sub-Update')")
    @ApiOperation(value = "更新Web编辑器转换器子测试", tags = {"Web编辑器转换器子测试" },  notes = "更新Web编辑器转换器子测试")
	@RequestMapping(method = RequestMethod.PUT, value = "/web_editor_converter_test_subs/{web_editor_converter_test_sub_id}")
    public ResponseEntity<Web_editor_converter_test_subDTO> update(@PathVariable("web_editor_converter_test_sub_id") Long web_editor_converter_test_sub_id, @RequestBody Web_editor_converter_test_subDTO web_editor_converter_test_subdto) {
		Web_editor_converter_test_sub domain  = web_editor_converter_test_subMapping.toDomain(web_editor_converter_test_subdto);
        domain .setId(web_editor_converter_test_sub_id);
		web_editor_converter_test_subService.update(domain );
		Web_editor_converter_test_subDTO dto = web_editor_converter_test_subMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.web_editor_converter_test_subService.getWebEditorConverterTestSubByEntities(this.web_editor_converter_test_subMapping.toDomain(#web_editor_converter_test_subdtos)),'iBizBusinessCentral-Web_editor_converter_test_sub-Update')")
    @ApiOperation(value = "批量更新Web编辑器转换器子测试", tags = {"Web编辑器转换器子测试" },  notes = "批量更新Web编辑器转换器子测试")
	@RequestMapping(method = RequestMethod.PUT, value = "/web_editor_converter_test_subs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Web_editor_converter_test_subDTO> web_editor_converter_test_subdtos) {
        web_editor_converter_test_subService.updateBatch(web_editor_converter_test_subMapping.toDomain(web_editor_converter_test_subdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.web_editor_converter_test_subService.get(#web_editor_converter_test_sub_id),'iBizBusinessCentral-Web_editor_converter_test_sub-Remove')")
    @ApiOperation(value = "删除Web编辑器转换器子测试", tags = {"Web编辑器转换器子测试" },  notes = "删除Web编辑器转换器子测试")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web_editor_converter_test_subs/{web_editor_converter_test_sub_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("web_editor_converter_test_sub_id") Long web_editor_converter_test_sub_id) {
         return ResponseEntity.status(HttpStatus.OK).body(web_editor_converter_test_subService.remove(web_editor_converter_test_sub_id));
    }

    @PreAuthorize("hasPermission(this.web_editor_converter_test_subService.getWebEditorConverterTestSubByIds(#ids),'iBizBusinessCentral-Web_editor_converter_test_sub-Remove')")
    @ApiOperation(value = "批量删除Web编辑器转换器子测试", tags = {"Web编辑器转换器子测试" },  notes = "批量删除Web编辑器转换器子测试")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web_editor_converter_test_subs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        web_editor_converter_test_subService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.web_editor_converter_test_subMapping.toDomain(returnObject.body),'iBizBusinessCentral-Web_editor_converter_test_sub-Get')")
    @ApiOperation(value = "获取Web编辑器转换器子测试", tags = {"Web编辑器转换器子测试" },  notes = "获取Web编辑器转换器子测试")
	@RequestMapping(method = RequestMethod.GET, value = "/web_editor_converter_test_subs/{web_editor_converter_test_sub_id}")
    public ResponseEntity<Web_editor_converter_test_subDTO> get(@PathVariable("web_editor_converter_test_sub_id") Long web_editor_converter_test_sub_id) {
        Web_editor_converter_test_sub domain = web_editor_converter_test_subService.get(web_editor_converter_test_sub_id);
        Web_editor_converter_test_subDTO dto = web_editor_converter_test_subMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Web编辑器转换器子测试草稿", tags = {"Web编辑器转换器子测试" },  notes = "获取Web编辑器转换器子测试草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/web_editor_converter_test_subs/getdraft")
    public ResponseEntity<Web_editor_converter_test_subDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(web_editor_converter_test_subMapping.toDto(web_editor_converter_test_subService.getDraft(new Web_editor_converter_test_sub())));
    }

    @ApiOperation(value = "检查Web编辑器转换器子测试", tags = {"Web编辑器转换器子测试" },  notes = "检查Web编辑器转换器子测试")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Web_editor_converter_test_subDTO web_editor_converter_test_subdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(web_editor_converter_test_subService.checkKey(web_editor_converter_test_subMapping.toDomain(web_editor_converter_test_subdto)));
    }

    @PreAuthorize("hasPermission(this.web_editor_converter_test_subMapping.toDomain(#web_editor_converter_test_subdto),'iBizBusinessCentral-Web_editor_converter_test_sub-Save')")
    @ApiOperation(value = "保存Web编辑器转换器子测试", tags = {"Web编辑器转换器子测试" },  notes = "保存Web编辑器转换器子测试")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs/save")
    public ResponseEntity<Boolean> save(@RequestBody Web_editor_converter_test_subDTO web_editor_converter_test_subdto) {
        return ResponseEntity.status(HttpStatus.OK).body(web_editor_converter_test_subService.save(web_editor_converter_test_subMapping.toDomain(web_editor_converter_test_subdto)));
    }

    @PreAuthorize("hasPermission(this.web_editor_converter_test_subMapping.toDomain(#web_editor_converter_test_subdtos),'iBizBusinessCentral-Web_editor_converter_test_sub-Save')")
    @ApiOperation(value = "批量保存Web编辑器转换器子测试", tags = {"Web编辑器转换器子测试" },  notes = "批量保存Web编辑器转换器子测试")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Web_editor_converter_test_subDTO> web_editor_converter_test_subdtos) {
        web_editor_converter_test_subService.saveBatch(web_editor_converter_test_subMapping.toDomain(web_editor_converter_test_subdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_editor_converter_test_sub-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Web_editor_converter_test_sub-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Web编辑器转换器子测试" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/web_editor_converter_test_subs/fetchdefault")
	public ResponseEntity<List<Web_editor_converter_test_subDTO>> fetchDefault(Web_editor_converter_test_subSearchContext context) {
        Page<Web_editor_converter_test_sub> domains = web_editor_converter_test_subService.searchDefault(context) ;
        List<Web_editor_converter_test_subDTO> list = web_editor_converter_test_subMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Web_editor_converter_test_sub-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Web_editor_converter_test_sub-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Web编辑器转换器子测试" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/web_editor_converter_test_subs/searchdefault")
	public ResponseEntity<Page<Web_editor_converter_test_subDTO>> searchDefault(@RequestBody Web_editor_converter_test_subSearchContext context) {
        Page<Web_editor_converter_test_sub> domains = web_editor_converter_test_subService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(web_editor_converter_test_subMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

