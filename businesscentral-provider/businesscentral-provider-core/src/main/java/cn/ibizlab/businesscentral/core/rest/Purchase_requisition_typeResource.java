package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition_type;
import cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisition_typeService;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_requisition_typeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"采购申请类型" })
@RestController("Core-purchase_requisition_type")
@RequestMapping("")
public class Purchase_requisition_typeResource {

    @Autowired
    public IPurchase_requisition_typeService purchase_requisition_typeService;

    @Autowired
    @Lazy
    public Purchase_requisition_typeMapping purchase_requisition_typeMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_type-Create-all')")
    @ApiOperation(value = "新建采购申请类型", tags = {"采购申请类型" },  notes = "新建采购申请类型")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisition_types")
    public ResponseEntity<Purchase_requisition_typeDTO> create(@Validated @RequestBody Purchase_requisition_typeDTO purchase_requisition_typedto) {
        Purchase_requisition_type domain = purchase_requisition_typeMapping.toDomain(purchase_requisition_typedto);
		purchase_requisition_typeService.create(domain);
        Purchase_requisition_typeDTO dto = purchase_requisition_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_type-Create-all')")
    @ApiOperation(value = "批量新建采购申请类型", tags = {"采购申请类型" },  notes = "批量新建采购申请类型")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisition_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Purchase_requisition_typeDTO> purchase_requisition_typedtos) {
        purchase_requisition_typeService.createBatch(purchase_requisition_typeMapping.toDomain(purchase_requisition_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_type-Update-all')")
    @ApiOperation(value = "更新采购申请类型", tags = {"采购申请类型" },  notes = "更新采购申请类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_requisition_types/{purchase_requisition_type_id}")
    public ResponseEntity<Purchase_requisition_typeDTO> update(@PathVariable("purchase_requisition_type_id") Long purchase_requisition_type_id, @RequestBody Purchase_requisition_typeDTO purchase_requisition_typedto) {
		Purchase_requisition_type domain  = purchase_requisition_typeMapping.toDomain(purchase_requisition_typedto);
        domain .setId(purchase_requisition_type_id);
		purchase_requisition_typeService.update(domain );
		Purchase_requisition_typeDTO dto = purchase_requisition_typeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_type-Update-all')")
    @ApiOperation(value = "批量更新采购申请类型", tags = {"采购申请类型" },  notes = "批量更新采购申请类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_requisition_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_requisition_typeDTO> purchase_requisition_typedtos) {
        purchase_requisition_typeService.updateBatch(purchase_requisition_typeMapping.toDomain(purchase_requisition_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_type-Remove-all')")
    @ApiOperation(value = "删除采购申请类型", tags = {"采购申请类型" },  notes = "删除采购申请类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_requisition_types/{purchase_requisition_type_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("purchase_requisition_type_id") Long purchase_requisition_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_typeService.remove(purchase_requisition_type_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_type-Remove-all')")
    @ApiOperation(value = "批量删除采购申请类型", tags = {"采购申请类型" },  notes = "批量删除采购申请类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_requisition_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        purchase_requisition_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_type-Get-all')")
    @ApiOperation(value = "获取采购申请类型", tags = {"采购申请类型" },  notes = "获取采购申请类型")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_requisition_types/{purchase_requisition_type_id}")
    public ResponseEntity<Purchase_requisition_typeDTO> get(@PathVariable("purchase_requisition_type_id") Long purchase_requisition_type_id) {
        Purchase_requisition_type domain = purchase_requisition_typeService.get(purchase_requisition_type_id);
        Purchase_requisition_typeDTO dto = purchase_requisition_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取采购申请类型草稿", tags = {"采购申请类型" },  notes = "获取采购申请类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_requisition_types/getdraft")
    public ResponseEntity<Purchase_requisition_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_typeMapping.toDto(purchase_requisition_typeService.getDraft(new Purchase_requisition_type())));
    }

    @ApiOperation(value = "检查采购申请类型", tags = {"采购申请类型" },  notes = "检查采购申请类型")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisition_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Purchase_requisition_typeDTO purchase_requisition_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_typeService.checkKey(purchase_requisition_typeMapping.toDomain(purchase_requisition_typedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_type-Save-all')")
    @ApiOperation(value = "保存采购申请类型", tags = {"采购申请类型" },  notes = "保存采购申请类型")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisition_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Purchase_requisition_typeDTO purchase_requisition_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisition_typeService.save(purchase_requisition_typeMapping.toDomain(purchase_requisition_typedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_type-Save-all')")
    @ApiOperation(value = "批量保存采购申请类型", tags = {"采购申请类型" },  notes = "批量保存采购申请类型")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisition_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Purchase_requisition_typeDTO> purchase_requisition_typedtos) {
        purchase_requisition_typeService.saveBatch(purchase_requisition_typeMapping.toDomain(purchase_requisition_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_type-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"采购申请类型" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_requisition_types/fetchdefault")
	public ResponseEntity<List<Purchase_requisition_typeDTO>> fetchDefault(Purchase_requisition_typeSearchContext context) {
        Page<Purchase_requisition_type> domains = purchase_requisition_typeService.searchDefault(context) ;
        List<Purchase_requisition_typeDTO> list = purchase_requisition_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition_type-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"采购申请类型" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_requisition_types/searchdefault")
	public ResponseEntity<Page<Purchase_requisition_typeDTO>> searchDefault(@RequestBody Purchase_requisition_typeSearchContext context) {
        Page<Purchase_requisition_type> domains = purchase_requisition_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_requisition_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

