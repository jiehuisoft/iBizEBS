package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Mail_activityDTO]
 */
@Data
public class Mail_activityDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [MAIL_TEMPLATE_IDS]
     *
     */
    @JSONField(name = "mail_template_ids")
    @JsonProperty("mail_template_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String mailTemplateIds;

    /**
     * 属性 [AUTOMATED]
     *
     */
    @JSONField(name = "automated")
    @JsonProperty("automated")
    private Boolean automated;

    /**
     * 属性 [RES_NAME]
     *
     */
    @JSONField(name = "res_name")
    @JsonProperty("res_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String resName;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("date_deadline")
    @NotNull(message = "[到期时间]不允许为空!")
    private Timestamp dateDeadline;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [SUMMARY]
     *
     */
    @JSONField(name = "summary")
    @JsonProperty("summary")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String summary;

    /**
     * 属性 [HAS_RECOMMENDED_ACTIVITIES]
     *
     */
    @JSONField(name = "has_recommended_activities")
    @JsonProperty("has_recommended_activities")
    private Boolean hasRecommendedActivities;

    /**
     * 属性 [RES_ID]
     *
     */
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    @NotNull(message = "[相关文档编号]不允许为空!")
    private Integer resId;

    /**
     * 属性 [NOTE]
     *
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String note;

    /**
     * 属性 [RES_MODEL_ID]
     *
     */
    @JSONField(name = "res_model_id")
    @JsonProperty("res_model_id")
    @NotNull(message = "[文档模型]不允许为空!")
    private Integer resModelId;

    /**
     * 属性 [RES_MODEL]
     *
     */
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String resModel;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [ACTIVITY_CATEGORY]
     *
     */
    @JSONField(name = "activity_category")
    @JsonProperty("activity_category")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityCategory;

    /**
     * 属性 [ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "activity_type_id_text")
    @JsonProperty("activity_type_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityTypeIdText;

    /**
     * 属性 [PREVIOUS_ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "previous_activity_type_id_text")
    @JsonProperty("previous_activity_type_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String previousActivityTypeIdText;

    /**
     * 属性 [NOTE_ID_TEXT]
     *
     */
    @JSONField(name = "note_id_text")
    @JsonProperty("note_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String noteIdText;

    /**
     * 属性 [ICON]
     *
     */
    @JSONField(name = "icon")
    @JsonProperty("icon")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String icon;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [ACTIVITY_DECORATION]
     *
     */
    @JSONField(name = "activity_decoration")
    @JsonProperty("activity_decoration")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityDecoration;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [FORCE_NEXT]
     *
     */
    @JSONField(name = "force_next")
    @JsonProperty("force_next")
    private Boolean forceNext;

    /**
     * 属性 [RECOMMENDED_ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "recommended_activity_type_id_text")
    @JsonProperty("recommended_activity_type_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String recommendedActivityTypeIdText;

    /**
     * 属性 [CALENDAR_EVENT_ID_TEXT]
     *
     */
    @JSONField(name = "calendar_event_id_text")
    @JsonProperty("calendar_event_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String calendarEventIdText;

    /**
     * 属性 [RECOMMENDED_ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "recommended_activity_type_id")
    @JsonProperty("recommended_activity_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long recommendedActivityTypeId;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long activityTypeId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[分派给]不允许为空!")
    private Long userId;

    /**
     * 属性 [NOTE_ID]
     *
     */
    @JSONField(name = "note_id")
    @JsonProperty("note_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long noteId;

    /**
     * 属性 [PREVIOUS_ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "previous_activity_type_id")
    @JsonProperty("previous_activity_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long previousActivityTypeId;

    /**
     * 属性 [CALENDAR_EVENT_ID]
     *
     */
    @JSONField(name = "calendar_event_id")
    @JsonProperty("calendar_event_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long calendarEventId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [AUTOMATED]
     */
    public void setAutomated(Boolean  automated){
        this.automated = automated ;
        this.modify("automated",automated);
    }

    /**
     * 设置 [RES_NAME]
     */
    public void setResName(String  resName){
        this.resName = resName ;
        this.modify("res_name",resName);
    }

    /**
     * 设置 [DATE_DEADLINE]
     */
    public void setDateDeadline(Timestamp  dateDeadline){
        this.dateDeadline = dateDeadline ;
        this.modify("date_deadline",dateDeadline);
    }

    /**
     * 设置 [SUMMARY]
     */
    public void setSummary(String  summary){
        this.summary = summary ;
        this.modify("summary",summary);
    }

    /**
     * 设置 [RES_ID]
     */
    public void setResId(Integer  resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [NOTE]
     */
    public void setNote(String  note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [RES_MODEL_ID]
     */
    public void setResModelId(Integer  resModelId){
        this.resModelId = resModelId ;
        this.modify("res_model_id",resModelId);
    }

    /**
     * 设置 [RES_MODEL]
     */
    public void setResModel(String  resModel){
        this.resModel = resModel ;
        this.modify("res_model",resModel);
    }

    /**
     * 设置 [RECOMMENDED_ACTIVITY_TYPE_ID]
     */
    public void setRecommendedActivityTypeId(Long  recommendedActivityTypeId){
        this.recommendedActivityTypeId = recommendedActivityTypeId ;
        this.modify("recommended_activity_type_id",recommendedActivityTypeId);
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    public void setActivityTypeId(Long  activityTypeId){
        this.activityTypeId = activityTypeId ;
        this.modify("activity_type_id",activityTypeId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [NOTE_ID]
     */
    public void setNoteId(Long  noteId){
        this.noteId = noteId ;
        this.modify("note_id",noteId);
    }

    /**
     * 设置 [PREVIOUS_ACTIVITY_TYPE_ID]
     */
    public void setPreviousActivityTypeId(Long  previousActivityTypeId){
        this.previousActivityTypeId = previousActivityTypeId ;
        this.modify("previous_activity_type_id",previousActivityTypeId);
    }

    /**
     * 设置 [CALENDAR_EVENT_ID]
     */
    public void setCalendarEventId(Long  calendarEventId){
        this.calendarEventId = calendarEventId ;
        this.modify("calendar_event_id",calendarEventId);
    }


}


