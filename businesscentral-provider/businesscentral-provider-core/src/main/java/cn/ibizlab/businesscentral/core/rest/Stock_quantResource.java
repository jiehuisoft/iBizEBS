package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quantService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_quantSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"即时库存" })
@RestController("Core-stock_quant")
@RequestMapping("")
public class Stock_quantResource {

    @Autowired
    public IStock_quantService stock_quantService;

    @Autowired
    @Lazy
    public Stock_quantMapping stock_quantMapping;

    @PreAuthorize("hasPermission(this.stock_quantMapping.toDomain(#stock_quantdto),'iBizBusinessCentral-Stock_quant-Create')")
    @ApiOperation(value = "新建即时库存", tags = {"即时库存" },  notes = "新建即时库存")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quants")
    public ResponseEntity<Stock_quantDTO> create(@Validated @RequestBody Stock_quantDTO stock_quantdto) {
        Stock_quant domain = stock_quantMapping.toDomain(stock_quantdto);
		stock_quantService.create(domain);
        Stock_quantDTO dto = stock_quantMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_quantMapping.toDomain(#stock_quantdtos),'iBizBusinessCentral-Stock_quant-Create')")
    @ApiOperation(value = "批量新建即时库存", tags = {"即时库存" },  notes = "批量新建即时库存")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quants/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_quantDTO> stock_quantdtos) {
        stock_quantService.createBatch(stock_quantMapping.toDomain(stock_quantdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_quant" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_quantService.get(#stock_quant_id),'iBizBusinessCentral-Stock_quant-Update')")
    @ApiOperation(value = "更新即时库存", tags = {"即时库存" },  notes = "更新即时库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_quants/{stock_quant_id}")
    public ResponseEntity<Stock_quantDTO> update(@PathVariable("stock_quant_id") Long stock_quant_id, @RequestBody Stock_quantDTO stock_quantdto) {
		Stock_quant domain  = stock_quantMapping.toDomain(stock_quantdto);
        domain .setId(stock_quant_id);
		stock_quantService.update(domain );
		Stock_quantDTO dto = stock_quantMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_quantService.getStockQuantByEntities(this.stock_quantMapping.toDomain(#stock_quantdtos)),'iBizBusinessCentral-Stock_quant-Update')")
    @ApiOperation(value = "批量更新即时库存", tags = {"即时库存" },  notes = "批量更新即时库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_quants/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_quantDTO> stock_quantdtos) {
        stock_quantService.updateBatch(stock_quantMapping.toDomain(stock_quantdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_quantService.get(#stock_quant_id),'iBizBusinessCentral-Stock_quant-Remove')")
    @ApiOperation(value = "删除即时库存", tags = {"即时库存" },  notes = "删除即时库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_quants/{stock_quant_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_quant_id") Long stock_quant_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_quantService.remove(stock_quant_id));
    }

    @PreAuthorize("hasPermission(this.stock_quantService.getStockQuantByIds(#ids),'iBizBusinessCentral-Stock_quant-Remove')")
    @ApiOperation(value = "批量删除即时库存", tags = {"即时库存" },  notes = "批量删除即时库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_quants/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_quantService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_quantMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_quant-Get')")
    @ApiOperation(value = "获取即时库存", tags = {"即时库存" },  notes = "获取即时库存")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_quants/{stock_quant_id}")
    public ResponseEntity<Stock_quantDTO> get(@PathVariable("stock_quant_id") Long stock_quant_id) {
        Stock_quant domain = stock_quantService.get(stock_quant_id);
        Stock_quantDTO dto = stock_quantMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取即时库存草稿", tags = {"即时库存" },  notes = "获取即时库存草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_quants/getdraft")
    public ResponseEntity<Stock_quantDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_quantMapping.toDto(stock_quantService.getDraft(new Stock_quant())));
    }

    @ApiOperation(value = "检查即时库存", tags = {"即时库存" },  notes = "检查即时库存")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quants/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_quantDTO stock_quantdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_quantService.checkKey(stock_quantMapping.toDomain(stock_quantdto)));
    }

    @PreAuthorize("hasPermission(this.stock_quantMapping.toDomain(#stock_quantdto),'iBizBusinessCentral-Stock_quant-Save')")
    @ApiOperation(value = "保存即时库存", tags = {"即时库存" },  notes = "保存即时库存")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quants/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_quantDTO stock_quantdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_quantService.save(stock_quantMapping.toDomain(stock_quantdto)));
    }

    @PreAuthorize("hasPermission(this.stock_quantMapping.toDomain(#stock_quantdtos),'iBizBusinessCentral-Stock_quant-Save')")
    @ApiOperation(value = "批量保存即时库存", tags = {"即时库存" },  notes = "批量保存即时库存")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quants/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_quantDTO> stock_quantdtos) {
        stock_quantService.saveBatch(stock_quantMapping.toDomain(stock_quantdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_quant-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_quant-Get')")
	@ApiOperation(value = "获取数据集", tags = {"即时库存" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_quants/fetchdefault")
	public ResponseEntity<List<Stock_quantDTO>> fetchDefault(Stock_quantSearchContext context) {
        Page<Stock_quant> domains = stock_quantService.searchDefault(context) ;
        List<Stock_quantDTO> list = stock_quantMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_quant-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_quant-Get')")
	@ApiOperation(value = "查询数据集", tags = {"即时库存" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_quants/searchdefault")
	public ResponseEntity<Page<Stock_quantDTO>> searchDefault(@RequestBody Stock_quantSearchContext context) {
        Page<Stock_quant> domains = stock_quantService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_quantMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}

