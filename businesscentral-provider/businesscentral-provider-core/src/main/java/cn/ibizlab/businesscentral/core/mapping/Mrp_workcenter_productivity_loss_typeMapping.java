package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type;
import cn.ibizlab.businesscentral.core.dto.Mrp_workcenter_productivity_loss_typeDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMrp_workcenter_productivity_loss_typeMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mrp_workcenter_productivity_loss_typeMapping extends MappingBase<Mrp_workcenter_productivity_loss_typeDTO, Mrp_workcenter_productivity_loss_type> {


}

