package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_shortcode;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_shortcodeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_shortcode] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-shortcode", fallback = mail_shortcodeFallback.class)
public interface mail_shortcodeFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/mail_shortcodes/{id}")
    Mail_shortcode get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes")
    Mail_shortcode create(@RequestBody Mail_shortcode mail_shortcode);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes/batch")
    Boolean createBatch(@RequestBody List<Mail_shortcode> mail_shortcodes);



    @RequestMapping(method = RequestMethod.PUT, value = "/mail_shortcodes/{id}")
    Mail_shortcode update(@PathVariable("id") Long id,@RequestBody Mail_shortcode mail_shortcode);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_shortcodes/batch")
    Boolean updateBatch(@RequestBody List<Mail_shortcode> mail_shortcodes);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_shortcodes/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_shortcodes/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes/search")
    Page<Mail_shortcode> search(@RequestBody Mail_shortcodeSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_shortcodes/select")
    Page<Mail_shortcode> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_shortcodes/getdraft")
    Mail_shortcode getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes/checkkey")
    Boolean checkKey(@RequestBody Mail_shortcode mail_shortcode);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes/save")
    Boolean save(@RequestBody Mail_shortcode mail_shortcode);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_shortcode> mail_shortcodes);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes/searchdefault")
    Page<Mail_shortcode> searchDefault(@RequestBody Mail_shortcodeSearchContext context);


}
