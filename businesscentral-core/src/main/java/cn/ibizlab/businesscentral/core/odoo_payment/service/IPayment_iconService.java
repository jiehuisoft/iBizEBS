package cn.ibizlab.businesscentral.core.odoo_payment.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_icon;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_iconSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Payment_icon] 服务对象接口
 */
public interface IPayment_iconService extends IService<Payment_icon>{

    boolean create(Payment_icon et) ;
    void createBatch(List<Payment_icon> list) ;
    boolean update(Payment_icon et) ;
    void updateBatch(List<Payment_icon> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Payment_icon get(Long key) ;
    Payment_icon getDraft(Payment_icon et) ;
    boolean checkKey(Payment_icon et) ;
    boolean save(Payment_icon et) ;
    void saveBatch(List<Payment_icon> list) ;
    Page<Payment_icon> searchDefault(Payment_iconSearchContext context) ;
    List<Payment_icon> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Payment_icon> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Payment_icon> getPaymentIconByIds(List<Long> ids) ;
    List<Payment_icon> getPaymentIconByEntities(List<Payment_icon> entities) ;
}


