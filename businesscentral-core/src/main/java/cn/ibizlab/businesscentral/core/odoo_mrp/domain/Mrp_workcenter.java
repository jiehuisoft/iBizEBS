package cn.ibizlab.businesscentral.core.odoo_mrp.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[工作中心]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MRP_WORKCENTER",resultMap = "Mrp_workcenterResultMap")
public class Mrp_workcenter extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 每小时成本
     */
    @DEField(name = "costs_hour")
    @TableField(value = "costs_hour")
    @JSONField(name = "costs_hour")
    @JsonProperty("costs_hour")
    private Double costsHour;
    /**
     * 工作中心负载
     */
    @TableField(exist = false)
    @JSONField(name = "workcenter_load")
    @JsonProperty("workcenter_load")
    private Double workcenterLoad;
    /**
     * 工作中心状态
     */
    @DEField(name = "working_state")
    @TableField(value = "working_state")
    @JSONField(name = "working_state")
    @JsonProperty("working_state")
    private String workingState;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 延迟的订单合计
     */
    @TableField(exist = false)
    @JSONField(name = "workorder_late_count")
    @JsonProperty("workorder_late_count")
    private Integer workorderLateCount;
    /**
     * 效能
     */
    @TableField(exist = false)
    @JSONField(name = "performance")
    @JsonProperty("performance")
    private Integer performance;
    /**
     * OEE 目标
     */
    @DEField(name = "oee_target")
    @TableField(value = "oee_target")
    @JSONField(name = "oee_target")
    @JsonProperty("oee_target")
    private Double oeeTarget;
    /**
     * 容量
     */
    @TableField(value = "capacity")
    @JSONField(name = "capacity")
    @JsonProperty("capacity")
    private Double capacity;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * # 工单
     */
    @TableField(exist = false)
    @JSONField(name = "workorder_count")
    @JsonProperty("workorder_count")
    private Integer workorderCount;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 时间日志
     */
    @TableField(exist = false)
    @JSONField(name = "time_ids")
    @JsonProperty("time_ids")
    private String timeIds;
    /**
     * 生产性时间
     */
    @TableField(exist = false)
    @JSONField(name = "productive_time")
    @JsonProperty("productive_time")
    private Double productiveTime;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 工艺行
     */
    @TableField(exist = false)
    @JSONField(name = "routing_line_ids")
    @JsonProperty("routing_line_ids")
    private String routingLineIds;
    /**
     * OEE
     */
    @TableField(exist = false)
    @JSONField(name = "oee")
    @JsonProperty("oee")
    private Double oee;
    /**
     * 说明
     */
    @TableField(value = "note")
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;
    /**
     * 代码
     */
    @TableField(value = "code")
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;
    /**
     * 颜色
     */
    @TableField(value = "color")
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;
    /**
     * 生产后时间
     */
    @DEField(name = "time_stop")
    @TableField(value = "time_stop")
    @JSONField(name = "time_stop")
    @JsonProperty("time_stop")
    private Double timeStop;
    /**
     * 生产前时间
     */
    @DEField(name = "time_start")
    @TableField(value = "time_start")
    @JSONField(name = "time_start")
    @JsonProperty("time_start")
    private Double timeStart;
    /**
     * 待定的订单合计
     */
    @TableField(exist = false)
    @JSONField(name = "workorder_pending_count")
    @JsonProperty("workorder_pending_count")
    private Integer workorderPendingCount;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 阻塞时间
     */
    @TableField(exist = false)
    @JSONField(name = "blocked_time")
    @JsonProperty("blocked_time")
    private Double blockedTime;
    /**
     * # 读取工单
     */
    @TableField(exist = false)
    @JSONField(name = "workorder_ready_count")
    @JsonProperty("workorder_ready_count")
    private Integer workorderReadyCount;
    /**
     * 运行的订单合计
     */
    @TableField(exist = false)
    @JSONField(name = "workorder_progress_count")
    @JsonProperty("workorder_progress_count")
    private Integer workorderProgressCount;
    /**
     * 订单
     */
    @TableField(exist = false)
    @JSONField(name = "order_ids")
    @JsonProperty("order_ids")
    private String orderIds;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 时区
     */
    @TableField(exist = false)
    @JSONField(name = "tz")
    @JsonProperty("tz")
    private String tz;
    /**
     * 时间效率
     */
    @TableField(exist = false)
    @JSONField(name = "time_efficiency")
    @JsonProperty("time_efficiency")
    private Double timeEfficiency;
    /**
     * 有效
     */
    @TableField(exist = false)
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 工作时间
     */
    @TableField(exist = false)
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    private String resourceCalendarIdText;
    /**
     * 工作中心
     */
    @TableField(exist = false)
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 工作时间
     */
    @DEField(name = "resource_calendar_id")
    @TableField(value = "resource_calendar_id")
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Long resourceCalendarId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 资源
     */
    @DEField(name = "resource_id")
    @TableField(value = "resource_id")
    @JSONField(name = "resource_id")
    @JsonProperty("resource_id")
    private Long resourceId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar odooResourceCalendar;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_resource odooResource;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [每小时成本]
     */
    public void setCostsHour(Double costsHour){
        this.costsHour = costsHour ;
        this.modify("costs_hour",costsHour);
    }

    /**
     * 设置 [工作中心状态]
     */
    public void setWorkingState(String workingState){
        this.workingState = workingState ;
        this.modify("working_state",workingState);
    }

    /**
     * 设置 [OEE 目标]
     */
    public void setOeeTarget(Double oeeTarget){
        this.oeeTarget = oeeTarget ;
        this.modify("oee_target",oeeTarget);
    }

    /**
     * 设置 [容量]
     */
    public void setCapacity(Double capacity){
        this.capacity = capacity ;
        this.modify("capacity",capacity);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [说明]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [代码]
     */
    public void setCode(String code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [颜色]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [生产后时间]
     */
    public void setTimeStop(Double timeStop){
        this.timeStop = timeStop ;
        this.modify("time_stop",timeStop);
    }

    /**
     * 设置 [生产前时间]
     */
    public void setTimeStart(Double timeStart){
        this.timeStart = timeStart ;
        this.modify("time_start",timeStart);
    }

    /**
     * 设置 [工作时间]
     */
    public void setResourceCalendarId(Long resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [资源]
     */
    public void setResourceId(Long resourceId){
        this.resourceId = resourceId ;
        this.modify("resource_id",resourceId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


