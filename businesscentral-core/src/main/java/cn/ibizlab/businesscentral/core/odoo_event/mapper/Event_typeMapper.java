package cn.ibizlab.businesscentral.core.odoo_event.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_typeSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Event_typeMapper extends BaseMapper<Event_type>{

    Page<Event_type> searchDefault(IPage page, @Param("srf") Event_typeSearchContext context, @Param("ew") Wrapper<Event_type> wrapper) ;
    @Override
    Event_type selectById(Serializable id);
    @Override
    int insert(Event_type entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Event_type entity);
    @Override
    int update(@Param(Constants.ENTITY) Event_type entity, @Param("ew") Wrapper<Event_type> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Event_type> selectByCreateUid(@Param("id") Serializable id) ;

    List<Event_type> selectByWriteUid(@Param("id") Serializable id) ;


}
