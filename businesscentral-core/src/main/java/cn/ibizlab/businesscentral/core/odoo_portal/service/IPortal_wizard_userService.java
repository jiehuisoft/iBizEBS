package cn.ibizlab.businesscentral.core.odoo_portal.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_wizard_user;
import cn.ibizlab.businesscentral.core.odoo_portal.filter.Portal_wizard_userSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Portal_wizard_user] 服务对象接口
 */
public interface IPortal_wizard_userService extends IService<Portal_wizard_user>{

    boolean create(Portal_wizard_user et) ;
    void createBatch(List<Portal_wizard_user> list) ;
    boolean update(Portal_wizard_user et) ;
    void updateBatch(List<Portal_wizard_user> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Portal_wizard_user get(Long key) ;
    Portal_wizard_user getDraft(Portal_wizard_user et) ;
    boolean checkKey(Portal_wizard_user et) ;
    boolean save(Portal_wizard_user et) ;
    void saveBatch(List<Portal_wizard_user> list) ;
    Page<Portal_wizard_user> searchDefault(Portal_wizard_userSearchContext context) ;
    List<Portal_wizard_user> selectByWizardId(Long id);
    void removeByWizardId(Collection<Long> ids);
    void removeByWizardId(Long id);
    List<Portal_wizard_user> selectByPartnerId(Long id);
    void removeByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Portal_wizard_user> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Portal_wizard_user> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Portal_wizard_user> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Portal_wizard_user> getPortalWizardUserByIds(List<Long> ids) ;
    List<Portal_wizard_user> getPortalWizardUserByEntities(List<Portal_wizard_user> entities) ;
}


