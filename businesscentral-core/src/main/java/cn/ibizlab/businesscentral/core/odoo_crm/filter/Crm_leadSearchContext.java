package cn.ibizlab.businesscentral.core.odoo_crm.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead;
/**
 * 关系型数据实体[Crm_lead] 查询条件对象
 */
@Slf4j
@Data
public class Crm_leadSearchContext extends QueryWrapperContext<Crm_lead> {

	private String n_name_like;//[商机]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_type_eq;//[类型]
	public void setN_type_eq(String n_type_eq) {
        this.n_type_eq = n_type_eq;
        if(!ObjectUtils.isEmpty(this.n_type_eq)){
            this.getSearchCond().eq("type", n_type_eq);
        }
    }
	private String n_kanban_state_eq;//[看板状态]
	public void setN_kanban_state_eq(String n_kanban_state_eq) {
        this.n_kanban_state_eq = n_kanban_state_eq;
        if(!ObjectUtils.isEmpty(this.n_kanban_state_eq)){
            this.getSearchCond().eq("kanban_state", n_kanban_state_eq);
        }
    }
	private String n_priority_eq;//[优先级]
	public void setN_priority_eq(String n_priority_eq) {
        this.n_priority_eq = n_priority_eq;
        if(!ObjectUtils.isEmpty(this.n_priority_eq)){
            this.getSearchCond().eq("priority", n_priority_eq);
        }
    }
	private String n_user_id_text_eq;//[销售员]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[销售员]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_source_id_text_eq;//[来源]
	public void setN_source_id_text_eq(String n_source_id_text_eq) {
        this.n_source_id_text_eq = n_source_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_source_id_text_eq)){
            this.getSearchCond().eq("source_id_text", n_source_id_text_eq);
        }
    }
	private String n_source_id_text_like;//[来源]
	public void setN_source_id_text_like(String n_source_id_text_like) {
        this.n_source_id_text_like = n_source_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_source_id_text_like)){
            this.getSearchCond().like("source_id_text", n_source_id_text_like);
        }
    }
	private String n_partner_address_name_eq;//[业务伙伴联系姓名]
	public void setN_partner_address_name_eq(String n_partner_address_name_eq) {
        this.n_partner_address_name_eq = n_partner_address_name_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_address_name_eq)){
            this.getSearchCond().eq("partner_address_name", n_partner_address_name_eq);
        }
    }
	private String n_partner_address_name_like;//[业务伙伴联系姓名]
	public void setN_partner_address_name_like(String n_partner_address_name_like) {
        this.n_partner_address_name_like = n_partner_address_name_like;
        if(!ObjectUtils.isEmpty(this.n_partner_address_name_like)){
            this.getSearchCond().like("partner_address_name", n_partner_address_name_like);
        }
    }
	private String n_medium_id_text_eq;//[媒介]
	public void setN_medium_id_text_eq(String n_medium_id_text_eq) {
        this.n_medium_id_text_eq = n_medium_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_medium_id_text_eq)){
            this.getSearchCond().eq("medium_id_text", n_medium_id_text_eq);
        }
    }
	private String n_medium_id_text_like;//[媒介]
	public void setN_medium_id_text_like(String n_medium_id_text_like) {
        this.n_medium_id_text_like = n_medium_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_medium_id_text_like)){
            this.getSearchCond().like("medium_id_text", n_medium_id_text_like);
        }
    }
	private String n_team_id_text_eq;//[销售团队]
	public void setN_team_id_text_eq(String n_team_id_text_eq) {
        this.n_team_id_text_eq = n_team_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_eq)){
            this.getSearchCond().eq("team_id_text", n_team_id_text_eq);
        }
    }
	private String n_team_id_text_like;//[销售团队]
	public void setN_team_id_text_like(String n_team_id_text_like) {
        this.n_team_id_text_like = n_team_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_like)){
            this.getSearchCond().like("team_id_text", n_team_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_state_id_text_eq;//[省份]
	public void setN_state_id_text_eq(String n_state_id_text_eq) {
        this.n_state_id_text_eq = n_state_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_state_id_text_eq)){
            this.getSearchCond().eq("state_id_text", n_state_id_text_eq);
        }
    }
	private String n_state_id_text_like;//[省份]
	public void setN_state_id_text_like(String n_state_id_text_like) {
        this.n_state_id_text_like = n_state_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_state_id_text_like)){
            this.getSearchCond().like("state_id_text", n_state_id_text_like);
        }
    }
	private String n_campaign_id_text_eq;//[营销]
	public void setN_campaign_id_text_eq(String n_campaign_id_text_eq) {
        this.n_campaign_id_text_eq = n_campaign_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_text_eq)){
            this.getSearchCond().eq("campaign_id_text", n_campaign_id_text_eq);
        }
    }
	private String n_campaign_id_text_like;//[营销]
	public void setN_campaign_id_text_like(String n_campaign_id_text_like) {
        this.n_campaign_id_text_like = n_campaign_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_text_like)){
            this.getSearchCond().like("campaign_id_text", n_campaign_id_text_like);
        }
    }
	private String n_stage_id_text_eq;//[阶段]
	public void setN_stage_id_text_eq(String n_stage_id_text_eq) {
        this.n_stage_id_text_eq = n_stage_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_stage_id_text_eq)){
            this.getSearchCond().eq("stage_id_text", n_stage_id_text_eq);
        }
    }
	private String n_stage_id_text_like;//[阶段]
	public void setN_stage_id_text_like(String n_stage_id_text_like) {
        this.n_stage_id_text_like = n_stage_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_stage_id_text_like)){
            this.getSearchCond().like("stage_id_text", n_stage_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_country_id_text_eq;//[国家]
	public void setN_country_id_text_eq(String n_country_id_text_eq) {
        this.n_country_id_text_eq = n_country_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_eq)){
            this.getSearchCond().eq("country_id_text", n_country_id_text_eq);
        }
    }
	private String n_country_id_text_like;//[国家]
	public void setN_country_id_text_like(String n_country_id_text_like) {
        this.n_country_id_text_like = n_country_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_like)){
            this.getSearchCond().like("country_id_text", n_country_id_text_like);
        }
    }
	private String n_title_text_eq;//[称谓]
	public void setN_title_text_eq(String n_title_text_eq) {
        this.n_title_text_eq = n_title_text_eq;
        if(!ObjectUtils.isEmpty(this.n_title_text_eq)){
            this.getSearchCond().eq("title_text", n_title_text_eq);
        }
    }
	private String n_title_text_like;//[称谓]
	public void setN_title_text_like(String n_title_text_like) {
        this.n_title_text_like = n_title_text_like;
        if(!ObjectUtils.isEmpty(this.n_title_text_like)){
            this.getSearchCond().like("title_text", n_title_text_like);
        }
    }
	private String n_lost_reason_text_eq;//[失去原因]
	public void setN_lost_reason_text_eq(String n_lost_reason_text_eq) {
        this.n_lost_reason_text_eq = n_lost_reason_text_eq;
        if(!ObjectUtils.isEmpty(this.n_lost_reason_text_eq)){
            this.getSearchCond().eq("lost_reason_text", n_lost_reason_text_eq);
        }
    }
	private String n_lost_reason_text_like;//[失去原因]
	public void setN_lost_reason_text_like(String n_lost_reason_text_like) {
        this.n_lost_reason_text_like = n_lost_reason_text_like;
        if(!ObjectUtils.isEmpty(this.n_lost_reason_text_like)){
            this.getSearchCond().like("lost_reason_text", n_lost_reason_text_like);
        }
    }
	private Long n_lost_reason_eq;//[失去原因]
	public void setN_lost_reason_eq(Long n_lost_reason_eq) {
        this.n_lost_reason_eq = n_lost_reason_eq;
        if(!ObjectUtils.isEmpty(this.n_lost_reason_eq)){
            this.getSearchCond().eq("lost_reason", n_lost_reason_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_user_id_eq;//[销售员]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_state_id_eq;//[省份]
	public void setN_state_id_eq(Long n_state_id_eq) {
        this.n_state_id_eq = n_state_id_eq;
        if(!ObjectUtils.isEmpty(this.n_state_id_eq)){
            this.getSearchCond().eq("state_id", n_state_id_eq);
        }
    }
	private Long n_medium_id_eq;//[媒介]
	public void setN_medium_id_eq(Long n_medium_id_eq) {
        this.n_medium_id_eq = n_medium_id_eq;
        if(!ObjectUtils.isEmpty(this.n_medium_id_eq)){
            this.getSearchCond().eq("medium_id", n_medium_id_eq);
        }
    }
	private Long n_stage_id_eq;//[阶段]
	public void setN_stage_id_eq(Long n_stage_id_eq) {
        this.n_stage_id_eq = n_stage_id_eq;
        if(!ObjectUtils.isEmpty(this.n_stage_id_eq)){
            this.getSearchCond().eq("stage_id", n_stage_id_eq);
        }
    }
	private Long n_source_id_eq;//[来源]
	public void setN_source_id_eq(Long n_source_id_eq) {
        this.n_source_id_eq = n_source_id_eq;
        if(!ObjectUtils.isEmpty(this.n_source_id_eq)){
            this.getSearchCond().eq("source_id", n_source_id_eq);
        }
    }
	private Long n_country_id_eq;//[国家]
	public void setN_country_id_eq(Long n_country_id_eq) {
        this.n_country_id_eq = n_country_id_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_eq)){
            this.getSearchCond().eq("country_id", n_country_id_eq);
        }
    }
	private Long n_campaign_id_eq;//[营销]
	public void setN_campaign_id_eq(Long n_campaign_id_eq) {
        this.n_campaign_id_eq = n_campaign_id_eq;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_eq)){
            this.getSearchCond().eq("campaign_id", n_campaign_id_eq);
        }
    }
	private Long n_partner_id_eq;//[客户]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_team_id_eq;//[销售团队]
	public void setN_team_id_eq(Long n_team_id_eq) {
        this.n_team_id_eq = n_team_id_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_eq)){
            this.getSearchCond().eq("team_id", n_team_id_eq);
        }
    }
	private Long n_title_eq;//[称谓]
	public void setN_title_eq(Long n_title_eq) {
        this.n_title_eq = n_title_eq;
        if(!ObjectUtils.isEmpty(this.n_title_eq)){
            this.getSearchCond().eq("title", n_title_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



