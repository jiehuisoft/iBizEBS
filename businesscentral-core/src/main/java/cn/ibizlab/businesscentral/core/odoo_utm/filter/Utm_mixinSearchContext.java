package cn.ibizlab.businesscentral.core.odoo_utm.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_mixin;
/**
 * 关系型数据实体[Utm_mixin] 查询条件对象
 */
@Slf4j
@Data
public class Utm_mixinSearchContext extends QueryWrapperContext<Utm_mixin> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_medium_id_text_eq;//[媒体]
	public void setN_medium_id_text_eq(String n_medium_id_text_eq) {
        this.n_medium_id_text_eq = n_medium_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_medium_id_text_eq)){
            this.getSearchCond().eq("medium_id_text", n_medium_id_text_eq);
        }
    }
	private String n_medium_id_text_like;//[媒体]
	public void setN_medium_id_text_like(String n_medium_id_text_like) {
        this.n_medium_id_text_like = n_medium_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_medium_id_text_like)){
            this.getSearchCond().like("medium_id_text", n_medium_id_text_like);
        }
    }
	private String n_source_id_text_eq;//[来源]
	public void setN_source_id_text_eq(String n_source_id_text_eq) {
        this.n_source_id_text_eq = n_source_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_source_id_text_eq)){
            this.getSearchCond().eq("source_id_text", n_source_id_text_eq);
        }
    }
	private String n_source_id_text_like;//[来源]
	public void setN_source_id_text_like(String n_source_id_text_like) {
        this.n_source_id_text_like = n_source_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_source_id_text_like)){
            this.getSearchCond().like("source_id_text", n_source_id_text_like);
        }
    }
	private String n_campaign_id_text_eq;//[营销]
	public void setN_campaign_id_text_eq(String n_campaign_id_text_eq) {
        this.n_campaign_id_text_eq = n_campaign_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_text_eq)){
            this.getSearchCond().eq("campaign_id_text", n_campaign_id_text_eq);
        }
    }
	private String n_campaign_id_text_like;//[营销]
	public void setN_campaign_id_text_like(String n_campaign_id_text_like) {
        this.n_campaign_id_text_like = n_campaign_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_text_like)){
            this.getSearchCond().like("campaign_id_text", n_campaign_id_text_like);
        }
    }
	private Long n_medium_id_eq;//[媒体]
	public void setN_medium_id_eq(Long n_medium_id_eq) {
        this.n_medium_id_eq = n_medium_id_eq;
        if(!ObjectUtils.isEmpty(this.n_medium_id_eq)){
            this.getSearchCond().eq("medium_id", n_medium_id_eq);
        }
    }
	private Long n_source_id_eq;//[来源]
	public void setN_source_id_eq(Long n_source_id_eq) {
        this.n_source_id_eq = n_source_id_eq;
        if(!ObjectUtils.isEmpty(this.n_source_id_eq)){
            this.getSearchCond().eq("source_id", n_source_id_eq);
        }
    }
	private Long n_campaign_id_eq;//[营销]
	public void setN_campaign_id_eq(Long n_campaign_id_eq) {
        this.n_campaign_id_eq = n_campaign_id_eq;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_eq)){
            this.getSearchCond().eq("campaign_id", n_campaign_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



