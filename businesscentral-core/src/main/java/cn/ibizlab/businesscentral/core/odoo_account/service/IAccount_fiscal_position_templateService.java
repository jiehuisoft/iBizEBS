package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_fiscal_position_templateSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_fiscal_position_template] 服务对象接口
 */
public interface IAccount_fiscal_position_templateService extends IService<Account_fiscal_position_template>{

    boolean create(Account_fiscal_position_template et) ;
    void createBatch(List<Account_fiscal_position_template> list) ;
    boolean update(Account_fiscal_position_template et) ;
    void updateBatch(List<Account_fiscal_position_template> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_fiscal_position_template get(Long key) ;
    Account_fiscal_position_template getDraft(Account_fiscal_position_template et) ;
    boolean checkKey(Account_fiscal_position_template et) ;
    boolean save(Account_fiscal_position_template et) ;
    void saveBatch(List<Account_fiscal_position_template> list) ;
    Page<Account_fiscal_position_template> searchDefault(Account_fiscal_position_templateSearchContext context) ;
    List<Account_fiscal_position_template> selectByChartTemplateId(Long id);
    void resetByChartTemplateId(Long id);
    void resetByChartTemplateId(Collection<Long> ids);
    void removeByChartTemplateId(Long id);
    List<Account_fiscal_position_template> selectByCountryGroupId(Long id);
    void resetByCountryGroupId(Long id);
    void resetByCountryGroupId(Collection<Long> ids);
    void removeByCountryGroupId(Long id);
    List<Account_fiscal_position_template> selectByCountryId(Long id);
    void resetByCountryId(Long id);
    void resetByCountryId(Collection<Long> ids);
    void removeByCountryId(Long id);
    List<Account_fiscal_position_template> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_fiscal_position_template> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_fiscal_position_template> getAccountFiscalPositionTemplateByIds(List<Long> ids) ;
    List<Account_fiscal_position_template> getAccountFiscalPositionTemplateByEntities(List<Account_fiscal_position_template> entities) ;
}


