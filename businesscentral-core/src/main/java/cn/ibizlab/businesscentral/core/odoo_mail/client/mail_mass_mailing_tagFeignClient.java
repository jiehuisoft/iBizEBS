package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_tag;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_tagSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_mass_mailing_tag] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-mass-mailing-tag", fallback = mail_mass_mailing_tagFallback.class)
public interface mail_mass_mailing_tagFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags/search")
    Page<Mail_mass_mailing_tag> search(@RequestBody Mail_mass_mailing_tagSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_tags/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_tags/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags")
    Mail_mass_mailing_tag create(@RequestBody Mail_mass_mailing_tag mail_mass_mailing_tag);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags/batch")
    Boolean createBatch(@RequestBody List<Mail_mass_mailing_tag> mail_mass_mailing_tags);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_tags/{id}")
    Mail_mass_mailing_tag update(@PathVariable("id") Long id,@RequestBody Mail_mass_mailing_tag mail_mass_mailing_tag);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_tags/batch")
    Boolean updateBatch(@RequestBody List<Mail_mass_mailing_tag> mail_mass_mailing_tags);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_tags/{id}")
    Mail_mass_mailing_tag get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_tags/select")
    Page<Mail_mass_mailing_tag> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_tags/getdraft")
    Mail_mass_mailing_tag getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags/checkkey")
    Boolean checkKey(@RequestBody Mail_mass_mailing_tag mail_mass_mailing_tag);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags/save")
    Boolean save(@RequestBody Mail_mass_mailing_tag mail_mass_mailing_tag);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_mass_mailing_tag> mail_mass_mailing_tags);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags/searchdefault")
    Page<Mail_mass_mailing_tag> searchDefault(@RequestBody Mail_mass_mailing_tagSearchContext context);


}
