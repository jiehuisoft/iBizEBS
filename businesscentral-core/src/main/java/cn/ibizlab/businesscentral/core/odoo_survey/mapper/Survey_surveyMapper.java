package cn.ibizlab.businesscentral.core.odoo_survey.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_survey;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_surveySearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Survey_surveyMapper extends BaseMapper<Survey_survey>{

    Page<Survey_survey> searchDefault(IPage page, @Param("srf") Survey_surveySearchContext context, @Param("ew") Wrapper<Survey_survey> wrapper) ;
    @Override
    Survey_survey selectById(Serializable id);
    @Override
    int insert(Survey_survey entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Survey_survey entity);
    @Override
    int update(@Param(Constants.ENTITY) Survey_survey entity, @Param("ew") Wrapper<Survey_survey> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Survey_survey> selectByEmailTemplateId(@Param("id") Serializable id) ;

    List<Survey_survey> selectByCreateUid(@Param("id") Serializable id) ;

    List<Survey_survey> selectByWriteUid(@Param("id") Serializable id) ;

    List<Survey_survey> selectByStageId(@Param("id") Serializable id) ;


}
