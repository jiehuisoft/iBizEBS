package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_thread;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_threadSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_thread] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-thread", fallback = mail_threadFallback.class)
public interface mail_threadFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/mail_threads/search")
    Page<Mail_thread> search(@RequestBody Mail_threadSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_threads/{id}")
    Mail_thread update(@PathVariable("id") Long id,@RequestBody Mail_thread mail_thread);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_threads/batch")
    Boolean updateBatch(@RequestBody List<Mail_thread> mail_threads);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_threads/{id}")
    Mail_thread get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_threads/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_threads/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_threads")
    Mail_thread create(@RequestBody Mail_thread mail_thread);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_threads/batch")
    Boolean createBatch(@RequestBody List<Mail_thread> mail_threads);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_threads/select")
    Page<Mail_thread> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_threads/getdraft")
    Mail_thread getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_threads/checkkey")
    Boolean checkKey(@RequestBody Mail_thread mail_thread);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_threads/save")
    Boolean save(@RequestBody Mail_thread mail_thread);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_threads/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_thread> mail_threads);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_threads/searchdefault")
    Page<Mail_thread> searchDefault(@RequestBody Mail_threadSearchContext context);


}
