package cn.ibizlab.businesscentral.core.odoo_uom.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom;
import cn.ibizlab.businesscentral.core.odoo_uom.filter.Uom_uomSearchContext;
import cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_uom.mapper.Uom_uomMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[产品计量单位] 服务对象接口实现
 */
@Slf4j
@Service("Uom_uomServiceImpl")
public class Uom_uomServiceImpl extends EBSServiceImpl<Uom_uomMapper, Uom_uom> implements IUom_uomService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_lineService accountAnalyticLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_lineService accountInvoiceLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_lineService accountMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expenseService hrExpenseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_order_parts_lineService mroOrderPartsLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_parameterService mroPmParameterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_task_parts_lineService mroTaskPartsLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bom_lineService mrpBomLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bomService mrpBomService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_productionService mrpProductionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_product_produce_lineService mrpProductProduceLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_product_produceService mrpProductProduceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_unbuildService mrpUnbuildService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_replenishService productReplenishService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_templateService productTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService purchaseOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_reportService purchaseReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisition_lineService purchaseRequisitionLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_feeService repairFeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_lineService repairLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_orderService repairOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService saleOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_optionService saleOrderOptionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_template_lineService saleOrderTemplateLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_template_optionService saleOrderTemplateOptionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_reportService saleReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventory_lineService stockInventoryLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_move_lineService stockMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_production_lotService stockProductionLotService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_scrapService stockScrapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_categoryService uomCategoryService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "uom.uom" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Uom_uom et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IUom_uomService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Uom_uom> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Uom_uom et) {
        Uom_uom old = new Uom_uom() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IUom_uomService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IUom_uomService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Uom_uom> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountAnalyticLineService.resetByProductUomId(key);
        accountInvoiceLineService.resetByUomId(key);
        accountMoveLineService.resetByProductUomId(key);
        hrExpenseService.resetByProductUomId(key);
        mroOrderPartsLineService.resetByPartsUom(key);
        mroPmParameterService.resetByParameterUom(key);
        mroTaskPartsLineService.resetByPartsUom(key);
        mrpBomLineService.resetByProductUomId(key);
        mrpBomService.resetByProductUomId(key);
        mrpProductionService.resetByProductUomId(key);
        mrpProductProduceLineService.resetByProductUomId(key);
        mrpProductProduceService.resetByProductUomId(key);
        mrpUnbuildService.resetByProductUomId(key);
        productReplenishService.resetByProductUomId(key);
        productTemplateService.resetByUomId(key);
        productTemplateService.resetByUomPoId(key);
        purchaseOrderLineService.resetByProductUom(key);
        purchaseReportService.resetByProductUom(key);
        repairFeeService.resetByProductUom(key);
        repairLineService.resetByProductUom(key);
        repairOrderService.resetByProductUom(key);
        saleOrderLineService.resetByProductUom(key);
        saleOrderOptionService.resetByUomId(key);
        saleOrderTemplateLineService.resetByProductUomId(key);
        saleOrderTemplateOptionService.resetByUomId(key);
        saleReportService.resetByProductUom(key);
        stockInventoryLineService.resetByProductUomId(key);
        stockMoveLineService.resetByProductUomId(key);
        stockMoveService.resetByProductUom(key);
        stockProductionLotService.resetByProductUomId(key);
        stockScrapService.resetByProductUomId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountAnalyticLineService.resetByProductUomId(idList);
        accountInvoiceLineService.resetByUomId(idList);
        accountMoveLineService.resetByProductUomId(idList);
        hrExpenseService.resetByProductUomId(idList);
        mroOrderPartsLineService.resetByPartsUom(idList);
        mroPmParameterService.resetByParameterUom(idList);
        mroTaskPartsLineService.resetByPartsUom(idList);
        mrpBomLineService.resetByProductUomId(idList);
        mrpBomService.resetByProductUomId(idList);
        mrpProductionService.resetByProductUomId(idList);
        mrpProductProduceLineService.resetByProductUomId(idList);
        mrpProductProduceService.resetByProductUomId(idList);
        mrpUnbuildService.resetByProductUomId(idList);
        productReplenishService.resetByProductUomId(idList);
        productTemplateService.resetByUomId(idList);
        productTemplateService.resetByUomPoId(idList);
        purchaseOrderLineService.resetByProductUom(idList);
        purchaseReportService.resetByProductUom(idList);
        repairFeeService.resetByProductUom(idList);
        repairLineService.resetByProductUom(idList);
        repairOrderService.resetByProductUom(idList);
        saleOrderLineService.resetByProductUom(idList);
        saleOrderOptionService.resetByUomId(idList);
        saleOrderTemplateLineService.resetByProductUomId(idList);
        saleOrderTemplateOptionService.resetByUomId(idList);
        saleReportService.resetByProductUom(idList);
        stockInventoryLineService.resetByProductUomId(idList);
        stockMoveLineService.resetByProductUomId(idList);
        stockMoveService.resetByProductUom(idList);
        stockProductionLotService.resetByProductUomId(idList);
        stockScrapService.resetByProductUomId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Uom_uom get(Long key) {
        Uom_uom et = getById(key);
        if(et==null){
            et=new Uom_uom();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Uom_uom getDraft(Uom_uom et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Uom_uom et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Uom_uom et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Uom_uom et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Uom_uom> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Uom_uom> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Uom_uom> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Uom_uom>().eq("create_uid",id));
    }

	@Override
    public List<Uom_uom> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Uom_uom>().eq("write_uid",id));
    }

	@Override
    public List<Uom_uom> selectByCategoryId(Long id) {
        return baseMapper.selectByCategoryId(id);
    }
    @Override
    public void removeByCategoryId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Uom_uom>().in("category_id",ids));
    }

    @Override
    public void removeByCategoryId(Long id) {
        this.remove(new QueryWrapper<Uom_uom>().eq("category_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Uom_uom> searchDefault(Uom_uomSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Uom_uom> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Uom_uom>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Uom_uom et){
        //实体关系[DER1N_UOM_UOM__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_UOM_UOM__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_UOM_UOM__UOM_CATEGORY__CATEGORY_ID]
        if(!ObjectUtils.isEmpty(et.getCategoryId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_category odooCategory=et.getOdooCategory();
            if(ObjectUtils.isEmpty(odooCategory)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_category majorEntity=uomCategoryService.get(et.getCategoryId());
                et.setOdooCategory(majorEntity);
                odooCategory=majorEntity;
            }
            et.setCategoryIdText(odooCategory.getName());
            et.setIsPosGroupable(odooCategory.getIsPosGroupable());
            et.setMeasureType(odooCategory.getMeasureType());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Uom_uom> getUomUomByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Uom_uom> getUomUomByEntities(List<Uom_uom> entities) {
        List ids =new ArrayList();
        for(Uom_uom entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



