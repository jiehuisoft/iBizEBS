package cn.ibizlab.businesscentral.core.odoo_crm.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[线索/商机]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "CRM_LEAD",resultMap = "Crm_leadResultMap")
public class Crm_lead extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * #会议
     */
    @TableField(exist = false)
    @JSONField(name = "meeting_count")
    @JsonProperty("meeting_count")
    private Integer meetingCount;
    /**
     * 退回
     */
    @DEField(name = "message_bounce")
    @TableField(value = "message_bounce")
    @JSONField(name = "message_bounce")
    @JsonProperty("message_bounce")
    private Integer messageBounce;
    /**
     * 城市
     */
    @TableField(value = "city")
    @JSONField(name = "city")
    @JsonProperty("city")
    private String city;
    /**
     * 关闭日期
     */
    @DEField(name = "day_close")
    @TableField(value = "day_close")
    @JSONField(name = "day_close")
    @JsonProperty("day_close")
    private Double dayClose;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 按比例分摊收入
     */
    @DEField(name = "expected_revenue")
    @TableField(value = "expected_revenue")
    @JSONField(name = "expected_revenue")
    @JsonProperty("expected_revenue")
    private BigDecimal expectedRevenue;
    /**
     * 关闭日期
     */
    @DEField(name = "date_closed")
    @TableField(value = "date_closed")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_closed" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_closed")
    private Timestamp dateClosed;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 全局抄送
     */
    @DEField(name = "email_cc")
    @TableField(value = "email_cc")
    @JSONField(name = "email_cc")
    @JsonProperty("email_cc")
    private String emailCc;
    /**
     * 联系人姓名
     */
    @DEField(name = "contact_name")
    @TableField(value = "contact_name")
    @JSONField(name = "contact_name")
    @JsonProperty("contact_name")
    private String contactName;
    /**
     * 最后阶段更新
     */
    @DEField(name = "date_last_stage_update")
    @TableField(value = "date_last_stage_update")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_last_stage_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_last_stage_update")
    private Timestamp dateLastStageUpdate;
    /**
     * 预期收益
     */
    @DEField(name = "planned_revenue")
    @TableField(value = "planned_revenue")
    @JSONField(name = "planned_revenue")
    @JsonProperty("planned_revenue")
    private BigDecimal plannedRevenue;
    /**
     * 网站信息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 预期结束
     */
    @DEField(name = "date_deadline")
    @TableField(value = "date_deadline")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("date_deadline")
    private Timestamp dateDeadline;
    /**
     * 邮政编码
     */
    @TableField(value = "zip")
    @JSONField(name = "zip")
    @JsonProperty("zip")
    private String zip;
    /**
     * 商机
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 便签
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 手机
     */
    @TableField(value = "mobile")
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    private String mobile;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 类型
     */
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 网站
     */
    @TableField(value = "website")
    @JSONField(name = "website")
    @JsonProperty("website")
    private String website;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * EMail
     */
    @DEField(name = "email_from")
    @TableField(value = "email_from")
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;
    /**
     * 转换日期
     */
    @DEField(name = "date_conversion")
    @TableField(value = "date_conversion")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_conversion" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_conversion")
    private Timestamp dateConversion;
    /**
     * 客户名称
     */
    @DEField(name = "partner_name")
    @TableField(value = "partner_name")
    @JSONField(name = "partner_name")
    @JsonProperty("partner_name")
    private String partnerName;
    /**
     * 销售订单的总数
     */
    @TableField(exist = false)
    @JSONField(name = "sale_amount_total")
    @JsonProperty("sale_amount_total")
    private BigDecimal saleAmountTotal;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 需要激活
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 看板状态
     */
    @TableField(exist = false)
    @JSONField(name = "kanban_state")
    @JsonProperty("kanban_state")
    private String kanbanState;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 引荐于
     */
    @TableField(value = "referred")
    @JSONField(name = "referred")
    @JsonProperty("referred")
    private String referred;
    /**
     * 概率
     */
    @TableField(value = "probability")
    @JSONField(name = "probability")
    @JsonProperty("probability")
    private Double probability;
    /**
     * 最近行动
     */
    @DEField(name = "date_action_last")
    @TableField(value = "date_action_last")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_action_last" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_action_last")
    private Timestamp dateActionLast;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 分配日期
     */
    @DEField(name = "date_open")
    @TableField(value = "date_open")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_open" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_open")
    private Timestamp dateOpen;
    /**
     * 电话
     */
    @TableField(value = "phone")
    @JSONField(name = "phone")
    @JsonProperty("phone")
    private String phone;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 分配天数
     */
    @DEField(name = "day_open")
    @TableField(value = "day_open")
    @JSONField(name = "day_open")
    @JsonProperty("day_open")
    private Double dayOpen;
    /**
     * 下一活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 报价单的数量
     */
    @TableField(exist = false)
    @JSONField(name = "sale_number")
    @JsonProperty("sale_number")
    private Integer saleNumber;
    /**
     * 工作岗位
     */
    @TableField(value = "function")
    @JSONField(name = "function")
    @JsonProperty("function")
    private String function;
    /**
     * 街道 2
     */
    @TableField(value = "street2")
    @JSONField(name = "street2")
    @JsonProperty("street2")
    private String street2;
    /**
     * 订单
     */
    @TableField(exist = false)
    @JSONField(name = "order_ids")
    @JsonProperty("order_ids")
    private String orderIds;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 街道
     */
    @TableField(value = "street")
    @JSONField(name = "street")
    @JsonProperty("street")
    private String street;
    /**
     * 颜色索引
     */
    @TableField(value = "color")
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 标签
     */
    @TableField(exist = false)
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    private String tagIds;
    /**
     * 黑名单
     */
    @TableField(exist = false)
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private Boolean isBlacklisted;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 优先级
     */
    @TableField(value = "priority")
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;
    /**
     * 销售员
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 来源
     */
    @TableField(exist = false)
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    private String sourceIdText;
    /**
     * 业务伙伴联系姓名
     */
    @TableField(exist = false)
    @JSONField(name = "partner_address_name")
    @JsonProperty("partner_address_name")
    private String partnerAddressName;
    /**
     * 媒介
     */
    @TableField(exist = false)
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "company_currency")
    @JsonProperty("company_currency")
    private Long companyCurrency;
    /**
     * 销售团队
     */
    @TableField(exist = false)
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    private String teamIdText;
    /**
     * Partner Contact Mobile
     */
    @TableField(exist = false)
    @JSONField(name = "partner_address_mobile")
    @JsonProperty("partner_address_mobile")
    private String partnerAddressMobile;
    /**
     * 最后更新
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 用户EMail
     */
    @TableField(exist = false)
    @JSONField(name = "user_email")
    @JsonProperty("user_email")
    private String userEmail;
    /**
     * 用户 登录
     */
    @TableField(exist = false)
    @JSONField(name = "user_login")
    @JsonProperty("user_login")
    private String userLogin;
    /**
     * 合作伙伴联系电话
     */
    @TableField(exist = false)
    @JSONField(name = "partner_address_phone")
    @JsonProperty("partner_address_phone")
    private String partnerAddressPhone;
    /**
     * 省份
     */
    @TableField(exist = false)
    @JSONField(name = "state_id_text")
    @JsonProperty("state_id_text")
    private String stateIdText;
    /**
     * 营销
     */
    @TableField(exist = false)
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    private String campaignIdText;
    /**
     * 合作伙伴黑名单
     */
    @TableField(exist = false)
    @JSONField(name = "partner_is_blacklisted")
    @JsonProperty("partner_is_blacklisted")
    private Boolean partnerIsBlacklisted;
    /**
     * 业务伙伴联系EMail
     */
    @TableField(exist = false)
    @JSONField(name = "partner_address_email")
    @JsonProperty("partner_address_email")
    private String partnerAddressEmail;
    /**
     * 阶段
     */
    @TableField(exist = false)
    @JSONField(name = "stage_id_text")
    @JsonProperty("stage_id_text")
    private String stageIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 国家
     */
    @TableField(exist = false)
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;
    /**
     * 称谓
     */
    @TableField(exist = false)
    @JSONField(name = "title_text")
    @JsonProperty("title_text")
    private String titleText;
    /**
     * 失去原因
     */
    @TableField(exist = false)
    @JSONField(name = "lost_reason_text")
    @JsonProperty("lost_reason_text")
    private String lostReasonText;
    /**
     * 失去原因
     */
    @DEField(name = "lost_reason")
    @TableField(value = "lost_reason")
    @JSONField(name = "lost_reason")
    @JsonProperty("lost_reason")
    private Long lostReason;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 最后更新
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 销售员
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 省份
     */
    @DEField(name = "state_id")
    @TableField(value = "state_id")
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    private Long stateId;
    /**
     * 媒介
     */
    @DEField(name = "medium_id")
    @TableField(value = "medium_id")
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Long mediumId;
    /**
     * 阶段
     */
    @DEField(name = "stage_id")
    @TableField(value = "stage_id")
    @JSONField(name = "stage_id")
    @JsonProperty("stage_id")
    private Long stageId;
    /**
     * 来源
     */
    @DEField(name = "source_id")
    @TableField(value = "source_id")
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Long sourceId;
    /**
     * 国家
     */
    @DEField(name = "country_id")
    @TableField(value = "country_id")
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Long countryId;
    /**
     * 营销
     */
    @DEField(name = "campaign_id")
    @TableField(value = "campaign_id")
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Long campaignId;
    /**
     * 客户
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 销售团队
     */
    @DEField(name = "team_id")
    @TableField(value = "team_id")
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Long teamId;
    /**
     * 称谓
     */
    @TableField(value = "title")
    @JSONField(name = "title")
    @JsonProperty("title")
    private Long title;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lost_reason odooLost;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_stage odooStage;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team odooTeam;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_state odooState;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountry;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_title odooTitle;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign odooCampaign;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium odooMedium;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource;



    /**
     * 设置 [退回]
     */
    public void setMessageBounce(Integer messageBounce){
        this.messageBounce = messageBounce ;
        this.modify("message_bounce",messageBounce);
    }

    /**
     * 设置 [城市]
     */
    public void setCity(String city){
        this.city = city ;
        this.modify("city",city);
    }

    /**
     * 设置 [关闭日期]
     */
    public void setDayClose(Double dayClose){
        this.dayClose = dayClose ;
        this.modify("day_close",dayClose);
    }

    /**
     * 设置 [按比例分摊收入]
     */
    public void setExpectedRevenue(BigDecimal expectedRevenue){
        this.expectedRevenue = expectedRevenue ;
        this.modify("expected_revenue",expectedRevenue);
    }

    /**
     * 设置 [关闭日期]
     */
    public void setDateClosed(Timestamp dateClosed){
        this.dateClosed = dateClosed ;
        this.modify("date_closed",dateClosed);
    }

    /**
     * 格式化日期 [关闭日期]
     */
    public String formatDateClosed(){
        if (this.dateClosed == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateClosed);
    }
    /**
     * 设置 [全局抄送]
     */
    public void setEmailCc(String emailCc){
        this.emailCc = emailCc ;
        this.modify("email_cc",emailCc);
    }

    /**
     * 设置 [联系人姓名]
     */
    public void setContactName(String contactName){
        this.contactName = contactName ;
        this.modify("contact_name",contactName);
    }

    /**
     * 设置 [最后阶段更新]
     */
    public void setDateLastStageUpdate(Timestamp dateLastStageUpdate){
        this.dateLastStageUpdate = dateLastStageUpdate ;
        this.modify("date_last_stage_update",dateLastStageUpdate);
    }

    /**
     * 格式化日期 [最后阶段更新]
     */
    public String formatDateLastStageUpdate(){
        if (this.dateLastStageUpdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateLastStageUpdate);
    }
    /**
     * 设置 [预期收益]
     */
    public void setPlannedRevenue(BigDecimal plannedRevenue){
        this.plannedRevenue = plannedRevenue ;
        this.modify("planned_revenue",plannedRevenue);
    }

    /**
     * 设置 [预期结束]
     */
    public void setDateDeadline(Timestamp dateDeadline){
        this.dateDeadline = dateDeadline ;
        this.modify("date_deadline",dateDeadline);
    }

    /**
     * 格式化日期 [预期结束]
     */
    public String formatDateDeadline(){
        if (this.dateDeadline == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dateDeadline);
    }
    /**
     * 设置 [邮政编码]
     */
    public void setZip(String zip){
        this.zip = zip ;
        this.modify("zip",zip);
    }

    /**
     * 设置 [商机]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [便签]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [手机]
     */
    public void setMobile(String mobile){
        this.mobile = mobile ;
        this.modify("mobile",mobile);
    }

    /**
     * 设置 [类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [网站]
     */
    public void setWebsite(String website){
        this.website = website ;
        this.modify("website",website);
    }

    /**
     * 设置 [EMail]
     */
    public void setEmailFrom(String emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }

    /**
     * 设置 [转换日期]
     */
    public void setDateConversion(Timestamp dateConversion){
        this.dateConversion = dateConversion ;
        this.modify("date_conversion",dateConversion);
    }

    /**
     * 格式化日期 [转换日期]
     */
    public String formatDateConversion(){
        if (this.dateConversion == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateConversion);
    }
    /**
     * 设置 [客户名称]
     */
    public void setPartnerName(String partnerName){
        this.partnerName = partnerName ;
        this.modify("partner_name",partnerName);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [引荐于]
     */
    public void setReferred(String referred){
        this.referred = referred ;
        this.modify("referred",referred);
    }

    /**
     * 设置 [概率]
     */
    public void setProbability(Double probability){
        this.probability = probability ;
        this.modify("probability",probability);
    }

    /**
     * 设置 [最近行动]
     */
    public void setDateActionLast(Timestamp dateActionLast){
        this.dateActionLast = dateActionLast ;
        this.modify("date_action_last",dateActionLast);
    }

    /**
     * 格式化日期 [最近行动]
     */
    public String formatDateActionLast(){
        if (this.dateActionLast == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateActionLast);
    }
    /**
     * 设置 [分配日期]
     */
    public void setDateOpen(Timestamp dateOpen){
        this.dateOpen = dateOpen ;
        this.modify("date_open",dateOpen);
    }

    /**
     * 格式化日期 [分配日期]
     */
    public String formatDateOpen(){
        if (this.dateOpen == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateOpen);
    }
    /**
     * 设置 [电话]
     */
    public void setPhone(String phone){
        this.phone = phone ;
        this.modify("phone",phone);
    }

    /**
     * 设置 [分配天数]
     */
    public void setDayOpen(Double dayOpen){
        this.dayOpen = dayOpen ;
        this.modify("day_open",dayOpen);
    }

    /**
     * 设置 [工作岗位]
     */
    public void setFunction(String function){
        this.function = function ;
        this.modify("function",function);
    }

    /**
     * 设置 [街道 2]
     */
    public void setStreet2(String street2){
        this.street2 = street2 ;
        this.modify("street2",street2);
    }

    /**
     * 设置 [街道]
     */
    public void setStreet(String street){
        this.street = street ;
        this.modify("street",street);
    }

    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [优先级]
     */
    public void setPriority(String priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [失去原因]
     */
    public void setLostReason(Long lostReason){
        this.lostReason = lostReason ;
        this.modify("lost_reason",lostReason);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [销售员]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [省份]
     */
    public void setStateId(Long stateId){
        this.stateId = stateId ;
        this.modify("state_id",stateId);
    }

    /**
     * 设置 [媒介]
     */
    public void setMediumId(Long mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }

    /**
     * 设置 [阶段]
     */
    public void setStageId(Long stageId){
        this.stageId = stageId ;
        this.modify("stage_id",stageId);
    }

    /**
     * 设置 [来源]
     */
    public void setSourceId(Long sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }

    /**
     * 设置 [国家]
     */
    public void setCountryId(Long countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

    /**
     * 设置 [营销]
     */
    public void setCampaignId(Long campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }

    /**
     * 设置 [客户]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [销售团队]
     */
    public void setTeamId(Long teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }

    /**
     * 设置 [称谓]
     */
    public void setTitle(Long title){
        this.title = title ;
        this.modify("title",title);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


