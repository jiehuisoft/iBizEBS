package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity_mixin;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_activity_mixinSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_activity_mixin] 服务对象接口
 */
public interface IMail_activity_mixinService extends IService<Mail_activity_mixin>{

    boolean create(Mail_activity_mixin et) ;
    void createBatch(List<Mail_activity_mixin> list) ;
    boolean update(Mail_activity_mixin et) ;
    void updateBatch(List<Mail_activity_mixin> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_activity_mixin get(Long key) ;
    Mail_activity_mixin getDraft(Mail_activity_mixin et) ;
    boolean checkKey(Mail_activity_mixin et) ;
    boolean save(Mail_activity_mixin et) ;
    void saveBatch(List<Mail_activity_mixin> list) ;
    Page<Mail_activity_mixin> searchDefault(Mail_activity_mixinSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


