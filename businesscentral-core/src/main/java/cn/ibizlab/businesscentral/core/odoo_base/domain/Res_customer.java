package cn.ibizlab.businesscentral.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[客户]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "RES_PARTNER",resultMap = "Res_customerResultMap")
public class Res_customer extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 标签
     */
    @TableField(exist = false)
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private String categoryId;
    /**
     * 工作岗位
     */
    @TableField(value = "function")
    @JSONField(name = "function")
    @JsonProperty("function")
    private String function;
    /**
     * 网站网址
     */
    @TableField(exist = false)
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;
    /**
     * 电话
     */
    @TableField(value = "phone")
    @JSONField(name = "phone")
    @JsonProperty("phone")
    private String phone;
    /**
     * 手机
     */
    @TableField(value = "mobile")
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    private String mobile;
    /**
     * 公司类别
     */
    @TableField(exist = false)
    @JSONField(name = "company_type")
    @JsonProperty("company_type")
    private String companyType;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 地址类型
     */
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * EMail
     */
    @TableField(value = "email")
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;
    /**
     * 税号
     */
    @TableField(value = "vat")
    @JSONField(name = "vat")
    @JsonProperty("vat")
    private String vat;



    /**
     * 设置 [工作岗位]
     */
    public void setFunction(String function){
        this.function = function ;
        this.modify("function",function);
    }

    /**
     * 设置 [电话]
     */
    public void setPhone(String phone){
        this.phone = phone ;
        this.modify("phone",phone);
    }

    /**
     * 设置 [手机]
     */
    public void setMobile(String mobile){
        this.mobile = mobile ;
        this.modify("mobile",mobile);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [地址类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [EMail]
     */
    public void setEmail(String email){
        this.email = email ;
        this.modify("email",email);
    }

    /**
     * 设置 [税号]
     */
    public void setVat(String vat){
        this.vat = vat ;
        this.modify("vat",vat);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


