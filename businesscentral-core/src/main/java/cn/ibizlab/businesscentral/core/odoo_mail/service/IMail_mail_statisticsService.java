package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mail_statistics;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mail_statisticsSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_mail_statistics] 服务对象接口
 */
public interface IMail_mail_statisticsService extends IService<Mail_mail_statistics>{

    boolean create(Mail_mail_statistics et) ;
    void createBatch(List<Mail_mail_statistics> list) ;
    boolean update(Mail_mail_statistics et) ;
    void updateBatch(List<Mail_mail_statistics> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_mail_statistics get(Long key) ;
    Mail_mail_statistics getDraft(Mail_mail_statistics et) ;
    boolean checkKey(Mail_mail_statistics et) ;
    boolean save(Mail_mail_statistics et) ;
    void saveBatch(List<Mail_mail_statistics> list) ;
    Page<Mail_mail_statistics> searchDefault(Mail_mail_statisticsSearchContext context) ;
    List<Mail_mail_statistics> selectByMailMailId(Long id);
    void resetByMailMailId(Long id);
    void resetByMailMailId(Collection<Long> ids);
    void removeByMailMailId(Long id);
    List<Mail_mail_statistics> selectByMassMailingCampaignId(Long id);
    void resetByMassMailingCampaignId(Long id);
    void resetByMassMailingCampaignId(Collection<Long> ids);
    void removeByMassMailingCampaignId(Long id);
    List<Mail_mail_statistics> selectByMassMailingId(Long id);
    void resetByMassMailingId(Long id);
    void resetByMassMailingId(Collection<Long> ids);
    void removeByMassMailingId(Long id);
    List<Mail_mail_statistics> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_mail_statistics> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_mail_statistics> getMailMailStatisticsByIds(List<Long> ids) ;
    List<Mail_mail_statistics> getMailMailStatisticsByEntities(List<Mail_mail_statistics> entities) ;
}


