package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_closebalance;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statement_closebalanceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_bank_statement_closebalance] 服务对象接口
 */
public interface IAccount_bank_statement_closebalanceService extends IService<Account_bank_statement_closebalance>{

    boolean create(Account_bank_statement_closebalance et) ;
    void createBatch(List<Account_bank_statement_closebalance> list) ;
    boolean update(Account_bank_statement_closebalance et) ;
    void updateBatch(List<Account_bank_statement_closebalance> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_bank_statement_closebalance get(Long key) ;
    Account_bank_statement_closebalance getDraft(Account_bank_statement_closebalance et) ;
    boolean checkKey(Account_bank_statement_closebalance et) ;
    boolean save(Account_bank_statement_closebalance et) ;
    void saveBatch(List<Account_bank_statement_closebalance> list) ;
    Page<Account_bank_statement_closebalance> searchDefault(Account_bank_statement_closebalanceSearchContext context) ;
    List<Account_bank_statement_closebalance> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_bank_statement_closebalance> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_bank_statement_closebalance> getAccountBankStatementClosebalanceByIds(List<Long> ids) ;
    List<Account_bank_statement_closebalance> getAccountBankStatementClosebalanceByEntities(List<Account_bank_statement_closebalance> entities) ;
}


