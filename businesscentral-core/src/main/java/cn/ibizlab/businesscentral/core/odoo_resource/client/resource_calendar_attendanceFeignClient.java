package cn.ibizlab.businesscentral.core.odoo_resource.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar_attendance;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_calendar_attendanceSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[resource_calendar_attendance] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-resource:odoo-resource}", contextId = "resource-calendar-attendance", fallback = resource_calendar_attendanceFallback.class)
public interface resource_calendar_attendanceFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/resource_calendar_attendances/{id}")
    Resource_calendar_attendance update(@PathVariable("id") Long id,@RequestBody Resource_calendar_attendance resource_calendar_attendance);

    @RequestMapping(method = RequestMethod.PUT, value = "/resource_calendar_attendances/batch")
    Boolean updateBatch(@RequestBody List<Resource_calendar_attendance> resource_calendar_attendances);


    @RequestMapping(method = RequestMethod.GET, value = "/resource_calendar_attendances/{id}")
    Resource_calendar_attendance get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendar_attendances/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendar_attendances/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances")
    Resource_calendar_attendance create(@RequestBody Resource_calendar_attendance resource_calendar_attendance);

    @RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances/batch")
    Boolean createBatch(@RequestBody List<Resource_calendar_attendance> resource_calendar_attendances);



    @RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances/search")
    Page<Resource_calendar_attendance> search(@RequestBody Resource_calendar_attendanceSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/resource_calendar_attendances/select")
    Page<Resource_calendar_attendance> select();


    @RequestMapping(method = RequestMethod.GET, value = "/resource_calendar_attendances/getdraft")
    Resource_calendar_attendance getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances/checkkey")
    Boolean checkKey(@RequestBody Resource_calendar_attendance resource_calendar_attendance);


    @RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances/save")
    Boolean save(@RequestBody Resource_calendar_attendance resource_calendar_attendance);

    @RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances/savebatch")
    Boolean saveBatch(@RequestBody List<Resource_calendar_attendance> resource_calendar_attendances);



    @RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances/searchdefault")
    Page<Resource_calendar_attendance> searchDefault(@RequestBody Resource_calendar_attendanceSearchContext context);


}
