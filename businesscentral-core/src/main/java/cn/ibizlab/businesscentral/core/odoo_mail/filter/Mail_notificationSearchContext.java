package cn.ibizlab.businesscentral.core.odoo_mail.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_notification;
/**
 * 关系型数据实体[Mail_notification] 查询条件对象
 */
@Slf4j
@Data
public class Mail_notificationSearchContext extends QueryWrapperContext<Mail_notification> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_email_status_eq;//[EMail状态]
	public void setN_email_status_eq(String n_email_status_eq) {
        this.n_email_status_eq = n_email_status_eq;
        if(!ObjectUtils.isEmpty(this.n_email_status_eq)){
            this.getSearchCond().eq("email_status", n_email_status_eq);
        }
    }
	private String n_failure_type_eq;//[失败类型]
	public void setN_failure_type_eq(String n_failure_type_eq) {
        this.n_failure_type_eq = n_failure_type_eq;
        if(!ObjectUtils.isEmpty(this.n_failure_type_eq)){
            this.getSearchCond().eq("failure_type", n_failure_type_eq);
        }
    }
	private String n_res_partner_id_text_eq;//[需收件人]
	public void setN_res_partner_id_text_eq(String n_res_partner_id_text_eq) {
        this.n_res_partner_id_text_eq = n_res_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_res_partner_id_text_eq)){
            this.getSearchCond().eq("res_partner_id_text", n_res_partner_id_text_eq);
        }
    }
	private String n_res_partner_id_text_like;//[需收件人]
	public void setN_res_partner_id_text_like(String n_res_partner_id_text_like) {
        this.n_res_partner_id_text_like = n_res_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_res_partner_id_text_like)){
            this.getSearchCond().like("res_partner_id_text", n_res_partner_id_text_like);
        }
    }
	private Long n_mail_id_eq;//[邮件]
	public void setN_mail_id_eq(Long n_mail_id_eq) {
        this.n_mail_id_eq = n_mail_id_eq;
        if(!ObjectUtils.isEmpty(this.n_mail_id_eq)){
            this.getSearchCond().eq("mail_id", n_mail_id_eq);
        }
    }
	private Long n_mail_message_id_eq;//[消息]
	public void setN_mail_message_id_eq(Long n_mail_message_id_eq) {
        this.n_mail_message_id_eq = n_mail_message_id_eq;
        if(!ObjectUtils.isEmpty(this.n_mail_message_id_eq)){
            this.getSearchCond().eq("mail_message_id", n_mail_message_id_eq);
        }
    }
	private Long n_res_partner_id_eq;//[需收件人]
	public void setN_res_partner_id_eq(Long n_res_partner_id_eq) {
        this.n_res_partner_id_eq = n_res_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_res_partner_id_eq)){
            this.getSearchCond().eq("res_partner_id", n_res_partner_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



