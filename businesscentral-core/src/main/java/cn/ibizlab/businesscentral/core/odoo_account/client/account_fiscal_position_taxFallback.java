package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position_tax;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_fiscal_position_taxSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_fiscal_position_tax] 服务对象接口
 */
@Component
public class account_fiscal_position_taxFallback implements account_fiscal_position_taxFeignClient{

    public Page<Account_fiscal_position_tax> search(Account_fiscal_position_taxSearchContext context){
            return null;
     }




    public Account_fiscal_position_tax get(Long id){
            return null;
     }


    public Account_fiscal_position_tax update(Long id, Account_fiscal_position_tax account_fiscal_position_tax){
            return null;
     }
    public Boolean updateBatch(List<Account_fiscal_position_tax> account_fiscal_position_taxes){
            return false;
     }



    public Account_fiscal_position_tax create(Account_fiscal_position_tax account_fiscal_position_tax){
            return null;
     }
    public Boolean createBatch(List<Account_fiscal_position_tax> account_fiscal_position_taxes){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Account_fiscal_position_tax> select(){
            return null;
     }

    public Account_fiscal_position_tax getDraft(){
            return null;
    }



    public Boolean checkKey(Account_fiscal_position_tax account_fiscal_position_tax){
            return false;
     }


    public Boolean save(Account_fiscal_position_tax account_fiscal_position_tax){
            return false;
     }
    public Boolean saveBatch(List<Account_fiscal_position_tax> account_fiscal_position_taxes){
            return false;
     }

    public Page<Account_fiscal_position_tax> searchDefault(Account_fiscal_position_taxSearchContext context){
            return null;
     }


}
