package cn.ibizlab.businesscentral.core.odoo_resource.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_test;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_testSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Resource_testMapper extends BaseMapper<Resource_test>{

    Page<Resource_test> searchDefault(IPage page, @Param("srf") Resource_testSearchContext context, @Param("ew") Wrapper<Resource_test> wrapper) ;
    @Override
    Resource_test selectById(Serializable id);
    @Override
    int insert(Resource_test entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Resource_test entity);
    @Override
    int update(@Param(Constants.ENTITY) Resource_test entity, @Param("ew") Wrapper<Resource_test> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Resource_test> selectByResourceCalendarId(@Param("id") Serializable id) ;

    List<Resource_test> selectByResourceId(@Param("id") Serializable id) ;

    List<Resource_test> selectByCompanyId(@Param("id") Serializable id) ;

    List<Resource_test> selectByCreateUid(@Param("id") Serializable id) ;

    List<Resource_test> selectByWriteUid(@Param("id") Serializable id) ;


}
