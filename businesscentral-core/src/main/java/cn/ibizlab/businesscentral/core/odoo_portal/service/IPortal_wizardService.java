package cn.ibizlab.businesscentral.core.odoo_portal.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_wizard;
import cn.ibizlab.businesscentral.core.odoo_portal.filter.Portal_wizardSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Portal_wizard] 服务对象接口
 */
public interface IPortal_wizardService extends IService<Portal_wizard>{

    boolean create(Portal_wizard et) ;
    void createBatch(List<Portal_wizard> list) ;
    boolean update(Portal_wizard et) ;
    void updateBatch(List<Portal_wizard> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Portal_wizard get(Long key) ;
    Portal_wizard getDraft(Portal_wizard et) ;
    boolean checkKey(Portal_wizard et) ;
    boolean save(Portal_wizard et) ;
    void saveBatch(List<Portal_wizard> list) ;
    Page<Portal_wizard> searchDefault(Portal_wizardSearchContext context) ;
    List<Portal_wizard> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Portal_wizard> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Portal_wizard> getPortalWizardByIds(List<Long> ids) ;
    List<Portal_wizard> getPortalWizardByEntities(List<Portal_wizard> entities) ;
}


