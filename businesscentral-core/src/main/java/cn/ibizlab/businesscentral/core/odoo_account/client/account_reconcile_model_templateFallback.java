package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconcile_model_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_reconcile_model_templateSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_reconcile_model_template] 服务对象接口
 */
@Component
public class account_reconcile_model_templateFallback implements account_reconcile_model_templateFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Account_reconcile_model_template create(Account_reconcile_model_template account_reconcile_model_template){
            return null;
     }
    public Boolean createBatch(List<Account_reconcile_model_template> account_reconcile_model_templates){
            return false;
     }

    public Page<Account_reconcile_model_template> search(Account_reconcile_model_templateSearchContext context){
            return null;
     }


    public Account_reconcile_model_template update(Long id, Account_reconcile_model_template account_reconcile_model_template){
            return null;
     }
    public Boolean updateBatch(List<Account_reconcile_model_template> account_reconcile_model_templates){
            return false;
     }



    public Account_reconcile_model_template get(Long id){
            return null;
     }




    public Page<Account_reconcile_model_template> select(){
            return null;
     }

    public Account_reconcile_model_template getDraft(){
            return null;
    }



    public Boolean checkKey(Account_reconcile_model_template account_reconcile_model_template){
            return false;
     }


    public Boolean save(Account_reconcile_model_template account_reconcile_model_template){
            return false;
     }
    public Boolean saveBatch(List<Account_reconcile_model_template> account_reconcile_model_templates){
            return false;
     }

    public Page<Account_reconcile_model_template> searchDefault(Account_reconcile_model_templateSearchContext context){
            return null;
     }


}
