package cn.ibizlab.businesscentral.core.odoo_stock.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_package_level;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_package_levelSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Stock_package_levelMapper extends BaseMapper<Stock_package_level>{

    Page<Stock_package_level> searchDefault(IPage page, @Param("srf") Stock_package_levelSearchContext context, @Param("ew") Wrapper<Stock_package_level> wrapper) ;
    @Override
    Stock_package_level selectById(Serializable id);
    @Override
    int insert(Stock_package_level entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Stock_package_level entity);
    @Override
    int update(@Param(Constants.ENTITY) Stock_package_level entity, @Param("ew") Wrapper<Stock_package_level> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Stock_package_level> selectByCreateUid(@Param("id") Serializable id) ;

    List<Stock_package_level> selectByWriteUid(@Param("id") Serializable id) ;

    List<Stock_package_level> selectByLocationDestId(@Param("id") Serializable id) ;

    List<Stock_package_level> selectByPickingId(@Param("id") Serializable id) ;

    List<Stock_package_level> selectByPackageId(@Param("id") Serializable id) ;


}
