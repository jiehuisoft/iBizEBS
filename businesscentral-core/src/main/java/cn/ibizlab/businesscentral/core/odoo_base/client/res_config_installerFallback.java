package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_config_installer;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_config_installerSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_config_installer] 服务对象接口
 */
@Component
public class res_config_installerFallback implements res_config_installerFeignClient{


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Res_config_installer create(Res_config_installer res_config_installer){
            return null;
     }
    public Boolean createBatch(List<Res_config_installer> res_config_installers){
            return false;
     }


    public Page<Res_config_installer> search(Res_config_installerSearchContext context){
            return null;
     }


    public Res_config_installer update(Long id, Res_config_installer res_config_installer){
            return null;
     }
    public Boolean updateBatch(List<Res_config_installer> res_config_installers){
            return false;
     }


    public Res_config_installer get(Long id){
            return null;
     }



    public Page<Res_config_installer> select(){
            return null;
     }

    public Res_config_installer getDraft(){
            return null;
    }



    public Boolean checkKey(Res_config_installer res_config_installer){
            return false;
     }


    public Boolean save(Res_config_installer res_config_installer){
            return false;
     }
    public Boolean saveBatch(List<Res_config_installer> res_config_installers){
            return false;
     }

    public Page<Res_config_installer> searchDefault(Res_config_installerSearchContext context){
            return null;
     }


}
