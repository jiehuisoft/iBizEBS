package cn.ibizlab.businesscentral.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_confirm;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_confirmSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Event_confirm] 服务对象接口
 */
public interface IEvent_confirmService extends IService<Event_confirm>{

    boolean create(Event_confirm et) ;
    void createBatch(List<Event_confirm> list) ;
    boolean update(Event_confirm et) ;
    void updateBatch(List<Event_confirm> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Event_confirm get(Long key) ;
    Event_confirm getDraft(Event_confirm et) ;
    boolean checkKey(Event_confirm et) ;
    boolean save(Event_confirm et) ;
    void saveBatch(List<Event_confirm> list) ;
    Page<Event_confirm> searchDefault(Event_confirmSearchContext context) ;
    List<Event_confirm> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Event_confirm> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Event_confirm> getEventConfirmByIds(List<Long> ids) ;
    List<Event_confirm> getEventConfirmByEntities(List<Event_confirm> entities) ;
}


