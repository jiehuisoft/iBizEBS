package cn.ibizlab.businesscentral.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_product_configurator;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_product_configuratorSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[sale_product_configurator] 服务对象接口
 */
@Component
public class sale_product_configuratorFallback implements sale_product_configuratorFeignClient{

    public Sale_product_configurator create(Sale_product_configurator sale_product_configurator){
            return null;
     }
    public Boolean createBatch(List<Sale_product_configurator> sale_product_configurators){
            return false;
     }



    public Page<Sale_product_configurator> search(Sale_product_configuratorSearchContext context){
            return null;
     }


    public Sale_product_configurator get(Long id){
            return null;
     }


    public Sale_product_configurator update(Long id, Sale_product_configurator sale_product_configurator){
            return null;
     }
    public Boolean updateBatch(List<Sale_product_configurator> sale_product_configurators){
            return false;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Sale_product_configurator> select(){
            return null;
     }

    public Sale_product_configurator getDraft(){
            return null;
    }



    public Boolean checkKey(Sale_product_configurator sale_product_configurator){
            return false;
     }


    public Boolean save(Sale_product_configurator sale_product_configurator){
            return false;
     }
    public Boolean saveBatch(List<Sale_product_configurator> sale_product_configurators){
            return false;
     }

    public Page<Sale_product_configurator> searchDefault(Sale_product_configuratorSearchContext context){
            return null;
     }


}
