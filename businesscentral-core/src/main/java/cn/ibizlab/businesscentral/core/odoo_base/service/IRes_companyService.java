package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_companySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_company] 服务对象接口
 */
public interface IRes_companyService extends IService<Res_company>{

    boolean create(Res_company et) ;
    void createBatch(List<Res_company> list) ;
    boolean update(Res_company et) ;
    void updateBatch(List<Res_company> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_company get(Long key) ;
    Res_company getDraft(Res_company et) ;
    boolean checkKey(Res_company et) ;
    boolean save(Res_company et) ;
    void saveBatch(List<Res_company> list) ;
    Page<Res_company> searchDefault(Res_companySearchContext context) ;
    Page<Res_company> searchROOT(Res_companySearchContext context) ;
    List<Res_company> selectByPropertyStockAccountInputCategId(Long id);
    void resetByPropertyStockAccountInputCategId(Long id);
    void resetByPropertyStockAccountInputCategId(Collection<Long> ids);
    void removeByPropertyStockAccountInputCategId(Long id);
    List<Res_company> selectByPropertyStockAccountOutputCategId(Long id);
    void resetByPropertyStockAccountOutputCategId(Long id);
    void resetByPropertyStockAccountOutputCategId(Collection<Long> ids);
    void removeByPropertyStockAccountOutputCategId(Long id);
    List<Res_company> selectByPropertyStockValuationAccountId(Long id);
    void resetByPropertyStockValuationAccountId(Long id);
    void resetByPropertyStockValuationAccountId(Collection<Long> ids);
    void removeByPropertyStockValuationAccountId(Long id);
    List<Res_company> selectByTransferAccountId(Long id);
    void resetByTransferAccountId(Long id);
    void resetByTransferAccountId(Collection<Long> ids);
    void removeByTransferAccountId(Long id);
    List<Res_company> selectByChartTemplateId(Long id);
    void resetByChartTemplateId(Long id);
    void resetByChartTemplateId(Collection<Long> ids);
    void removeByChartTemplateId(Long id);
    List<Res_company> selectByIncotermId(Long id);
    void resetByIncotermId(Long id);
    void resetByIncotermId(Collection<Long> ids);
    void removeByIncotermId(Long id);
    List<Res_company> selectByCurrencyExchangeJournalId(Long id);
    void resetByCurrencyExchangeJournalId(Long id);
    void resetByCurrencyExchangeJournalId(Collection<Long> ids);
    void removeByCurrencyExchangeJournalId(Long id);
    List<Res_company> selectByTaxCashBasisJournalId(Long id);
    void resetByTaxCashBasisJournalId(Long id);
    void resetByTaxCashBasisJournalId(Collection<Long> ids);
    void removeByTaxCashBasisJournalId(Long id);
    List<Res_company> selectByAccountOpeningMoveId(Long id);
    void resetByAccountOpeningMoveId(Long id);
    void resetByAccountOpeningMoveId(Collection<Long> ids);
    void removeByAccountOpeningMoveId(Long id);
    List<Res_company> selectByAccountPurchaseTaxId(Long id);
    void resetByAccountPurchaseTaxId(Long id);
    void resetByAccountPurchaseTaxId(Collection<Long> ids);
    void removeByAccountPurchaseTaxId(Long id);
    List<Res_company> selectByAccountSaleTaxId(Long id);
    void resetByAccountSaleTaxId(Long id);
    void resetByAccountSaleTaxId(Collection<Long> ids);
    void removeByAccountSaleTaxId(Long id);
    List<Res_company> selectByResourceCalendarId(Long id);
    List<Res_company> selectByResourceCalendarId(Collection<Long> ids);
    void removeByResourceCalendarId(Long id);
    List<Res_company> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Res_company> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Res_company> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Res_company> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_company> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Res_company> selectByInternalTransitLocationId(Long id);
    void resetByInternalTransitLocationId(Long id);
    void resetByInternalTransitLocationId(Collection<Long> ids);
    void removeByInternalTransitLocationId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_company> getResCompanyByIds(List<Long> ids) ;
    List<Res_company> getResCompanyByEntities(List<Res_company> entities) ;
}


