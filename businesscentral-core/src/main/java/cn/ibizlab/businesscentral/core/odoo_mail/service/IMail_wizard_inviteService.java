package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_wizard_invite;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_wizard_inviteSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_wizard_invite] 服务对象接口
 */
public interface IMail_wizard_inviteService extends IService<Mail_wizard_invite>{

    boolean create(Mail_wizard_invite et) ;
    void createBatch(List<Mail_wizard_invite> list) ;
    boolean update(Mail_wizard_invite et) ;
    void updateBatch(List<Mail_wizard_invite> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_wizard_invite get(Long key) ;
    Mail_wizard_invite getDraft(Mail_wizard_invite et) ;
    boolean checkKey(Mail_wizard_invite et) ;
    boolean save(Mail_wizard_invite et) ;
    void saveBatch(List<Mail_wizard_invite> list) ;
    Page<Mail_wizard_invite> searchDefault(Mail_wizard_inviteSearchContext context) ;
    List<Mail_wizard_invite> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_wizard_invite> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_wizard_invite> getMailWizardInviteByIds(List<Long> ids) ;
    List<Mail_wizard_invite> getMailWizardInviteByEntities(List<Mail_wizard_invite> entities) ;
}


