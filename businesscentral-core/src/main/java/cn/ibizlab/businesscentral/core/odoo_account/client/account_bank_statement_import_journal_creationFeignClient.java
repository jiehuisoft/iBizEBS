package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_import_journal_creation;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statement_import_journal_creationSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_bank_statement_import_journal_creation] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-bank-statement-import-journal-creation", fallback = account_bank_statement_import_journal_creationFallback.class)
public interface account_bank_statement_import_journal_creationFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations")
    Account_bank_statement_import_journal_creation create(@RequestBody Account_bank_statement_import_journal_creation account_bank_statement_import_journal_creation);

    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations/batch")
    Boolean createBatch(@RequestBody List<Account_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations);


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_import_journal_creations/{id}")
    Account_bank_statement_import_journal_creation get(@PathVariable("id") Long id);





    @RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_import_journal_creations/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_import_journal_creations/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_import_journal_creations/{id}")
    Account_bank_statement_import_journal_creation update(@PathVariable("id") Long id,@RequestBody Account_bank_statement_import_journal_creation account_bank_statement_import_journal_creation);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_import_journal_creations/batch")
    Boolean updateBatch(@RequestBody List<Account_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations);



    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations/search")
    Page<Account_bank_statement_import_journal_creation> search(@RequestBody Account_bank_statement_import_journal_creationSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_import_journal_creations/select")
    Page<Account_bank_statement_import_journal_creation> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_import_journal_creations/getdraft")
    Account_bank_statement_import_journal_creation getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations/checkkey")
    Boolean checkKey(@RequestBody Account_bank_statement_import_journal_creation account_bank_statement_import_journal_creation);


    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations/save")
    Boolean save(@RequestBody Account_bank_statement_import_journal_creation account_bank_statement_import_journal_creation);

    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations/savebatch")
    Boolean saveBatch(@RequestBody List<Account_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations);



    @RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations/searchdefault")
    Page<Account_bank_statement_import_journal_creation> searchDefault(@RequestBody Account_bank_statement_import_journal_creationSearchContext context);


}
