package cn.ibizlab.businesscentral.core.odoo_repair.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_line;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[repair_line] 服务对象接口
 */
@Component
public class repair_lineFallback implements repair_lineFeignClient{



    public Repair_line get(Long id){
            return null;
     }


    public Repair_line update(Long id, Repair_line repair_line){
            return null;
     }
    public Boolean updateBatch(List<Repair_line> repair_lines){
            return false;
     }


    public Page<Repair_line> search(Repair_lineSearchContext context){
            return null;
     }



    public Repair_line create(Repair_line repair_line){
            return null;
     }
    public Boolean createBatch(List<Repair_line> repair_lines){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Repair_line> select(){
            return null;
     }

    public Repair_line getDraft(){
            return null;
    }



    public Boolean checkKey(Repair_line repair_line){
            return false;
     }


    public Boolean save(Repair_line repair_line){
            return false;
     }
    public Boolean saveBatch(List<Repair_line> repair_lines){
            return false;
     }

    public Page<Repair_line> searchDefault(Repair_lineSearchContext context){
            return null;
     }


}
