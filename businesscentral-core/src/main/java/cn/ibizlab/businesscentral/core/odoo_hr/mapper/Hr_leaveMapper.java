package cn.ibizlab.businesscentral.core.odoo_hr.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leaveSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Hr_leaveMapper extends BaseMapper<Hr_leave>{

    Page<Hr_leave> searchDefault(IPage page, @Param("srf") Hr_leaveSearchContext context, @Param("ew") Wrapper<Hr_leave> wrapper) ;
    @Override
    Hr_leave selectById(Serializable id);
    @Override
    int insert(Hr_leave entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Hr_leave entity);
    @Override
    int update(@Param(Constants.ENTITY) Hr_leave entity, @Param("ew") Wrapper<Hr_leave> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Hr_leave> selectByMeetingId(@Param("id") Serializable id) ;

    List<Hr_leave> selectByDepartmentId(@Param("id") Serializable id) ;

    List<Hr_leave> selectByCategoryId(@Param("id") Serializable id) ;

    List<Hr_leave> selectByEmployeeId(@Param("id") Serializable id) ;

    List<Hr_leave> selectByFirstApproverId(@Param("id") Serializable id) ;

    List<Hr_leave> selectByManagerId(@Param("id") Serializable id) ;

    List<Hr_leave> selectBySecondApproverId(@Param("id") Serializable id) ;

    List<Hr_leave> selectByHolidayStatusId(@Param("id") Serializable id) ;

    List<Hr_leave> selectByParentId(@Param("id") Serializable id) ;

    List<Hr_leave> selectByModeCompanyId(@Param("id") Serializable id) ;

    List<Hr_leave> selectByCreateUid(@Param("id") Serializable id) ;

    List<Hr_leave> selectByUserId(@Param("id") Serializable id) ;

    List<Hr_leave> selectByWriteUid(@Param("id") Serializable id) ;


}
