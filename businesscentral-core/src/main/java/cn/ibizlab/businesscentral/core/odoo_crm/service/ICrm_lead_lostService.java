package cn.ibizlab.businesscentral.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead_lost;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_lead_lostSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Crm_lead_lost] 服务对象接口
 */
public interface ICrm_lead_lostService extends IService<Crm_lead_lost>{

    boolean create(Crm_lead_lost et) ;
    void createBatch(List<Crm_lead_lost> list) ;
    boolean update(Crm_lead_lost et) ;
    void updateBatch(List<Crm_lead_lost> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Crm_lead_lost get(Long key) ;
    Crm_lead_lost getDraft(Crm_lead_lost et) ;
    boolean checkKey(Crm_lead_lost et) ;
    boolean save(Crm_lead_lost et) ;
    void saveBatch(List<Crm_lead_lost> list) ;
    Page<Crm_lead_lost> searchDefault(Crm_lead_lostSearchContext context) ;
    List<Crm_lead_lost> selectByLostReasonId(Long id);
    void resetByLostReasonId(Long id);
    void resetByLostReasonId(Collection<Long> ids);
    void removeByLostReasonId(Long id);
    List<Crm_lead_lost> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Crm_lead_lost> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Crm_lead_lost> getCrmLeadLostByIds(List<Long> ids) ;
    List<Crm_lead_lost> getCrmLeadLostByEntities(List<Crm_lead_lost> entities) ;
}


