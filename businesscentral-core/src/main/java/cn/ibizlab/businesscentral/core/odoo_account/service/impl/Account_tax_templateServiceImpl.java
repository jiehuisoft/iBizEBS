package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_tax_templateSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_tax_templateService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_tax_templateMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[税率模板] 服务对象接口实现
 */
@Slf4j
@Service("Account_tax_templateServiceImpl")
public class Account_tax_templateServiceImpl extends EBSServiceImpl<Account_tax_templateMapper, Account_tax_template> implements IAccount_tax_templateService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_position_tax_templateService accountFiscalPositionTaxTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_reconcile_model_templateService accountReconcileModelTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_account_templateService accountAccountTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_chart_templateService accountChartTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_tax_groupService accountTaxGroupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.tax.template" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_tax_template et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_tax_templateService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_tax_template> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_tax_template et) {
        Account_tax_template old = new Account_tax_template() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_tax_templateService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_tax_templateService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_tax_template> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountFiscalPositionTaxTemplateService.resetByTaxDestId(key);
        accountFiscalPositionTaxTemplateService.resetByTaxSrcId(key);
        if(!ObjectUtils.isEmpty(accountReconcileModelTemplateService.selectBySecondTaxId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_reconcile_model_template]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(accountReconcileModelTemplateService.selectByTaxId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_reconcile_model_template]数据，无法删除!","","");
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountFiscalPositionTaxTemplateService.resetByTaxDestId(idList);
        accountFiscalPositionTaxTemplateService.resetByTaxSrcId(idList);
        if(!ObjectUtils.isEmpty(accountReconcileModelTemplateService.selectBySecondTaxId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_reconcile_model_template]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(accountReconcileModelTemplateService.selectByTaxId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_reconcile_model_template]数据，无法删除!","","");
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_tax_template get(Long key) {
        Account_tax_template et = getById(key);
        if(et==null){
            et=new Account_tax_template();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_tax_template getDraft(Account_tax_template et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_tax_template et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_tax_template et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_tax_template et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_tax_template> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_tax_template> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_tax_template> selectByAccountId(Long id) {
        return baseMapper.selectByAccountId(id);
    }
    @Override
    public List<Account_tax_template> selectByAccountId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Account_tax_template>().in("id",ids));
    }

    @Override
    public void removeByAccountId(Long id) {
        this.remove(new QueryWrapper<Account_tax_template>().eq("account_id",id));
    }

	@Override
    public List<Account_tax_template> selectByCashBasisAccountId(Long id) {
        return baseMapper.selectByCashBasisAccountId(id);
    }
    @Override
    public void resetByCashBasisAccountId(Long id) {
        this.update(new UpdateWrapper<Account_tax_template>().set("cash_basis_account_id",null).eq("cash_basis_account_id",id));
    }

    @Override
    public void resetByCashBasisAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_tax_template>().set("cash_basis_account_id",null).in("cash_basis_account_id",ids));
    }

    @Override
    public void removeByCashBasisAccountId(Long id) {
        this.remove(new QueryWrapper<Account_tax_template>().eq("cash_basis_account_id",id));
    }

	@Override
    public List<Account_tax_template> selectByCashBasisBaseAccountId(Long id) {
        return baseMapper.selectByCashBasisBaseAccountId(id);
    }
    @Override
    public void resetByCashBasisBaseAccountId(Long id) {
        this.update(new UpdateWrapper<Account_tax_template>().set("cash_basis_base_account_id",null).eq("cash_basis_base_account_id",id));
    }

    @Override
    public void resetByCashBasisBaseAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_tax_template>().set("cash_basis_base_account_id",null).in("cash_basis_base_account_id",ids));
    }

    @Override
    public void removeByCashBasisBaseAccountId(Long id) {
        this.remove(new QueryWrapper<Account_tax_template>().eq("cash_basis_base_account_id",id));
    }

	@Override
    public List<Account_tax_template> selectByRefundAccountId(Long id) {
        return baseMapper.selectByRefundAccountId(id);
    }
    @Override
    public List<Account_tax_template> selectByRefundAccountId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Account_tax_template>().in("id",ids));
    }

    @Override
    public void removeByRefundAccountId(Long id) {
        this.remove(new QueryWrapper<Account_tax_template>().eq("refund_account_id",id));
    }

	@Override
    public List<Account_tax_template> selectByChartTemplateId(Long id) {
        return baseMapper.selectByChartTemplateId(id);
    }
    @Override
    public void resetByChartTemplateId(Long id) {
        this.update(new UpdateWrapper<Account_tax_template>().set("chart_template_id",null).eq("chart_template_id",id));
    }

    @Override
    public void resetByChartTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_tax_template>().set("chart_template_id",null).in("chart_template_id",ids));
    }

    @Override
    public void removeByChartTemplateId(Long id) {
        this.remove(new QueryWrapper<Account_tax_template>().eq("chart_template_id",id));
    }

	@Override
    public List<Account_tax_template> selectByTaxGroupId(Long id) {
        return baseMapper.selectByTaxGroupId(id);
    }
    @Override
    public void resetByTaxGroupId(Long id) {
        this.update(new UpdateWrapper<Account_tax_template>().set("tax_group_id",null).eq("tax_group_id",id));
    }

    @Override
    public void resetByTaxGroupId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_tax_template>().set("tax_group_id",null).in("tax_group_id",ids));
    }

    @Override
    public void removeByTaxGroupId(Long id) {
        this.remove(new QueryWrapper<Account_tax_template>().eq("tax_group_id",id));
    }

	@Override
    public List<Account_tax_template> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_tax_template>().eq("create_uid",id));
    }

	@Override
    public List<Account_tax_template> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_tax_template>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_tax_template> searchDefault(Account_tax_templateSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_tax_template> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_tax_template>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_tax_template et){
        //实体关系[DER1N_ACCOUNT_TAX_TEMPLATE__ACCOUNT_ACCOUNT_TEMPLATE__ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooAccount=et.getOdooAccount();
            if(ObjectUtils.isEmpty(odooAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template majorEntity=accountAccountTemplateService.get(et.getAccountId());
                et.setOdooAccount(majorEntity);
                odooAccount=majorEntity;
            }
            et.setAccountIdText(odooAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_TAX_TEMPLATE__ACCOUNT_ACCOUNT_TEMPLATE__CASH_BASIS_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getCashBasisAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooCashBasisAccount=et.getOdooCashBasisAccount();
            if(ObjectUtils.isEmpty(odooCashBasisAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template majorEntity=accountAccountTemplateService.get(et.getCashBasisAccountId());
                et.setOdooCashBasisAccount(majorEntity);
                odooCashBasisAccount=majorEntity;
            }
            et.setCashBasisAccountIdText(odooCashBasisAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_TAX_TEMPLATE__ACCOUNT_ACCOUNT_TEMPLATE__CASH_BASIS_BASE_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getCashBasisBaseAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooCashBasisBaseAccount=et.getOdooCashBasisBaseAccount();
            if(ObjectUtils.isEmpty(odooCashBasisBaseAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template majorEntity=accountAccountTemplateService.get(et.getCashBasisBaseAccountId());
                et.setOdooCashBasisBaseAccount(majorEntity);
                odooCashBasisBaseAccount=majorEntity;
            }
            et.setCashBasisBaseAccountIdText(odooCashBasisBaseAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_TAX_TEMPLATE__ACCOUNT_ACCOUNT_TEMPLATE__REFUND_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getRefundAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooRefundAccount=et.getOdooRefundAccount();
            if(ObjectUtils.isEmpty(odooRefundAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template majorEntity=accountAccountTemplateService.get(et.getRefundAccountId());
                et.setOdooRefundAccount(majorEntity);
                odooRefundAccount=majorEntity;
            }
            et.setRefundAccountIdText(odooRefundAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_TAX_TEMPLATE__ACCOUNT_CHART_TEMPLATE__CHART_TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getChartTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template odooChartTemplate=et.getOdooChartTemplate();
            if(ObjectUtils.isEmpty(odooChartTemplate)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template majorEntity=accountChartTemplateService.get(et.getChartTemplateId());
                et.setOdooChartTemplate(majorEntity);
                odooChartTemplate=majorEntity;
            }
            et.setChartTemplateIdText(odooChartTemplate.getName());
        }
        //实体关系[DER1N_ACCOUNT_TAX_TEMPLATE__ACCOUNT_TAX_GROUP__TAX_GROUP_ID]
        if(!ObjectUtils.isEmpty(et.getTaxGroupId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_group odooTaxGroup=et.getOdooTaxGroup();
            if(ObjectUtils.isEmpty(odooTaxGroup)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_group majorEntity=accountTaxGroupService.get(et.getTaxGroupId());
                et.setOdooTaxGroup(majorEntity);
                odooTaxGroup=majorEntity;
            }
            et.setTaxGroupIdText(odooTaxGroup.getName());
        }
        //实体关系[DER1N_ACCOUNT_TAX_TEMPLATE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_TAX_TEMPLATE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_tax_template> getAccountTaxTemplateByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_tax_template> getAccountTaxTemplateByEntities(List<Account_tax_template> entities) {
        List ids =new ArrayList();
        for(Account_tax_template entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



