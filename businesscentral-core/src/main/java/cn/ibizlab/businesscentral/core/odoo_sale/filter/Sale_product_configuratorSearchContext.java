package cn.ibizlab.businesscentral.core.odoo_sale.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_product_configurator;
/**
 * 关系型数据实体[Sale_product_configurator] 查询条件对象
 */
@Slf4j
@Data
public class Sale_product_configuratorSearchContext extends QueryWrapperContext<Sale_product_configurator> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_product_template_id_text_eq;//[产品]
	public void setN_product_template_id_text_eq(String n_product_template_id_text_eq) {
        this.n_product_template_id_text_eq = n_product_template_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_template_id_text_eq)){
            this.getSearchCond().eq("product_template_id_text", n_product_template_id_text_eq);
        }
    }
	private String n_product_template_id_text_like;//[产品]
	public void setN_product_template_id_text_like(String n_product_template_id_text_like) {
        this.n_product_template_id_text_like = n_product_template_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_template_id_text_like)){
            this.getSearchCond().like("product_template_id_text", n_product_template_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_pricelist_id_text_eq;//[价格表]
	public void setN_pricelist_id_text_eq(String n_pricelist_id_text_eq) {
        this.n_pricelist_id_text_eq = n_pricelist_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_text_eq)){
            this.getSearchCond().eq("pricelist_id_text", n_pricelist_id_text_eq);
        }
    }
	private String n_pricelist_id_text_like;//[价格表]
	public void setN_pricelist_id_text_like(String n_pricelist_id_text_like) {
        this.n_pricelist_id_text_like = n_pricelist_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_text_like)){
            this.getSearchCond().like("pricelist_id_text", n_pricelist_id_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_pricelist_id_eq;//[价格表]
	public void setN_pricelist_id_eq(Long n_pricelist_id_eq) {
        this.n_pricelist_id_eq = n_pricelist_id_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_eq)){
            this.getSearchCond().eq("pricelist_id", n_pricelist_id_eq);
        }
    }
	private Long n_product_template_id_eq;//[产品]
	public void setN_product_template_id_eq(Long n_product_template_id_eq) {
        this.n_product_template_id_eq = n_product_template_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_template_id_eq)){
            this.getSearchCond().eq("product_template_id", n_product_template_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



