package cn.ibizlab.businesscentral.core.odoo_calendar.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[活动]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "CALENDAR_EVENT",resultMap = "Calendar_eventResultMap")
public class Calendar_event extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 地点
     */
    @TableField(value = "location")
    @JSONField(name = "location")
    @JsonProperty("location")
    private String location;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 循环ID日期
     */
    @DEField(name = "recurrent_id_date")
    @TableField(value = "recurrent_id_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "recurrent_id_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("recurrent_id_date")
    private Timestamp recurrentIdDate;
    /**
     * 周六
     */
    @TableField(value = "sa")
    @JSONField(name = "sa")
    @JsonProperty("sa")
    private Boolean sa;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 循环
     */
    @TableField(value = "recurrency")
    @JSONField(name = "recurrency")
    @JsonProperty("recurrency")
    private Boolean recurrency;
    /**
     * 重复
     */
    @TableField(value = "count")
    @JSONField(name = "count")
    @JsonProperty("count")
    private Integer count;
    /**
     * 周五
     */
    @TableField(value = "fr")
    @JSONField(name = "fr")
    @JsonProperty("fr")
    private Boolean fr;
    /**
     * 循环规则
     */
    @TableField(value = "rrule")
    @JSONField(name = "rrule")
    @JsonProperty("rrule")
    private String rrule;
    /**
     * 选项
     */
    @DEField(name = "month_by")
    @TableField(value = "month_by")
    @JSONField(name = "month_by")
    @JsonProperty("month_by")
    private String monthBy;
    /**
     * 工作日
     */
    @DEField(name = "week_list")
    @TableField(value = "week_list")
    @JSONField(name = "week_list")
    @JsonProperty("week_list")
    private String weekList;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 停止
     */
    @TableField(value = "stop")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "stop" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("stop")
    private Timestamp stop;
    /**
     * 文档ID
     */
    @DEField(name = "res_id")
    @TableField(value = "res_id")
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 循环ID
     */
    @DEField(name = "recurrent_id")
    @TableField(value = "recurrent_id")
    @JSONField(name = "recurrent_id")
    @JsonProperty("recurrent_id")
    private Integer recurrentId;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 重复终止
     */
    @DEField(name = "end_type")
    @TableField(value = "end_type")
    @JSONField(name = "end_type")
    @JsonProperty("end_type")
    private String endType;
    /**
     * 参与者
     */
    @TableField(exist = false)
    @JSONField(name = "attendee_ids")
    @JsonProperty("attendee_ids")
    private String attendeeIds;
    /**
     * 出席者状态
     */
    @TableField(exist = false)
    @JSONField(name = "attendee_status")
    @JsonProperty("attendee_status")
    private String attendeeStatus;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 重新提起
     */
    @DEField(name = "rrule_type")
    @TableField(value = "rrule_type")
    @JSONField(name = "rrule_type")
    @JsonProperty("rrule_type")
    private String rruleType;
    /**
     * 重复
     */
    @TableField(value = "interval")
    @JSONField(name = "interval")
    @JsonProperty("interval")
    private Integer interval;
    /**
     * 隐私
     */
    @TableField(value = "privacy")
    @JSONField(name = "privacy")
    @JsonProperty("privacy")
    private String privacy;
    /**
     * 持续时间
     */
    @TableField(value = "duration")
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Double duration;
    /**
     * 开始时间
     */
    @DEField(name = "start_datetime")
    @TableField(value = "start_datetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("start_datetime")
    private Timestamp startDatetime;
    /**
     * 出席者
     */
    @TableField(exist = false)
    @JSONField(name = "is_attendee")
    @JsonProperty("is_attendee")
    private Boolean isAttendee;
    /**
     * 开始日期
     */
    @DEField(name = "start_date")
    @TableField(value = "start_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd")
    @JsonProperty("start_date")
    private Timestamp startDate;
    /**
     * 周一
     */
    @TableField(value = "mo")
    @JSONField(name = "mo")
    @JsonProperty("mo")
    private Boolean mo;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 周三
     */
    @TableField(value = "we")
    @JSONField(name = "we")
    @JsonProperty("we")
    private Boolean we;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 活动时间
     */
    @TableField(exist = false)
    @JSONField(name = "display_time")
    @JsonProperty("display_time")
    private String displayTime;
    /**
     * 日期
     */
    @DEField(name = "display_start")
    @TableField(value = "display_start")
    @JSONField(name = "display_start")
    @JsonProperty("display_start")
    private String displayStart;
    /**
     * 结束日期
     */
    @DEField(name = "stop_date")
    @TableField(value = "stop_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "stop_date" , format="yyyy-MM-dd")
    @JsonProperty("stop_date")
    private Timestamp stopDate;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 重复直到
     */
    @DEField(name = "final_date")
    @TableField(value = "final_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "final_date" , format="yyyy-MM-dd")
    @JsonProperty("final_date")
    private Timestamp finalDate;
    /**
     * 文档模型
     */
    @DEField(name = "res_model_id")
    @TableField(value = "res_model_id")
    @JSONField(name = "res_model_id")
    @JsonProperty("res_model_id")
    private Integer resModelId;
    /**
     * 事件是否突出显示
     */
    @TableField(exist = false)
    @JSONField(name = "is_highlighted")
    @JsonProperty("is_highlighted")
    private Boolean isHighlighted;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 结束日期时间
     */
    @DEField(name = "stop_datetime")
    @TableField(value = "stop_datetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "stop_datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("stop_datetime")
    private Timestamp stopDatetime;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 文档模型名称
     */
    @DEField(name = "res_model")
    @TableField(value = "res_model")
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    private String resModel;
    /**
     * 周二
     */
    @TableField(value = "tu")
    @JSONField(name = "tu")
    @JsonProperty("tu")
    private Boolean tu;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 显示时间为
     */
    @DEField(name = "show_as")
    @TableField(value = "show_as")
    @JSONField(name = "show_as")
    @JsonProperty("show_as")
    private String showAs;
    /**
     * 标签
     */
    @TableField(exist = false)
    @JSONField(name = "categ_ids")
    @JsonProperty("categ_ids")
    private String categIds;
    /**
     * 提醒
     */
    @TableField(exist = false)
    @JSONField(name = "alarm_ids")
    @JsonProperty("alarm_ids")
    private String alarmIds;
    /**
     * 周四
     */
    @TableField(value = "th")
    @JSONField(name = "th")
    @JsonProperty("th")
    private Boolean th;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 按 天
     */
    @TableField(value = "byday")
    @JSONField(name = "byday")
    @JsonProperty("byday")
    private String byday;
    /**
     * 日期
     */
    @TableField(value = "day")
    @JSONField(name = "day")
    @JsonProperty("day")
    private Integer day;
    /**
     * 开始
     */
    @TableField(value = "start")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("start")
    private Timestamp start;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 与会者
     */
    @TableField(exist = false)
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    private String partnerIds;
    /**
     * 全天
     */
    @TableField(value = "allday")
    @JSONField(name = "allday")
    @JsonProperty("allday")
    private Boolean allday;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 周日
     */
    @TableField(value = "su")
    @JSONField(name = "su")
    @JsonProperty("su")
    private Boolean su;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 会议主题
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 商机
     */
    @TableField(exist = false)
    @JSONField(name = "opportunity_id_text")
    @JsonProperty("opportunity_id_text")
    private String opportunityIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 所有者
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 负责人
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 申请人
     */
    @TableField(exist = false)
    @JSONField(name = "applicant_id_text")
    @JsonProperty("applicant_id_text")
    private String applicantIdText;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 申请人
     */
    @DEField(name = "applicant_id")
    @TableField(value = "applicant_id")
    @JSONField(name = "applicant_id")
    @JsonProperty("applicant_id")
    private Long applicantId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 所有者
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 商机
     */
    @DEField(name = "opportunity_id")
    @TableField(value = "opportunity_id")
    @JSONField(name = "opportunity_id")
    @JsonProperty("opportunity_id")
    private Long opportunityId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead odooOpportunity;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_applicant odooApplicant;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [地点]
     */
    public void setLocation(String location){
        this.location = location ;
        this.modify("location",location);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [循环ID日期]
     */
    public void setRecurrentIdDate(Timestamp recurrentIdDate){
        this.recurrentIdDate = recurrentIdDate ;
        this.modify("recurrent_id_date",recurrentIdDate);
    }

    /**
     * 格式化日期 [循环ID日期]
     */
    public String formatRecurrentIdDate(){
        if (this.recurrentIdDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(recurrentIdDate);
    }
    /**
     * 设置 [周六]
     */
    public void setSa(Boolean sa){
        this.sa = sa ;
        this.modify("sa",sa);
    }

    /**
     * 设置 [循环]
     */
    public void setRecurrency(Boolean recurrency){
        this.recurrency = recurrency ;
        this.modify("recurrency",recurrency);
    }

    /**
     * 设置 [重复]
     */
    public void setCount(Integer count){
        this.count = count ;
        this.modify("count",count);
    }

    /**
     * 设置 [周五]
     */
    public void setFr(Boolean fr){
        this.fr = fr ;
        this.modify("fr",fr);
    }

    /**
     * 设置 [循环规则]
     */
    public void setRrule(String rrule){
        this.rrule = rrule ;
        this.modify("rrule",rrule);
    }

    /**
     * 设置 [选项]
     */
    public void setMonthBy(String monthBy){
        this.monthBy = monthBy ;
        this.modify("month_by",monthBy);
    }

    /**
     * 设置 [工作日]
     */
    public void setWeekList(String weekList){
        this.weekList = weekList ;
        this.modify("week_list",weekList);
    }

    /**
     * 设置 [停止]
     */
    public void setStop(Timestamp stop){
        this.stop = stop ;
        this.modify("stop",stop);
    }

    /**
     * 格式化日期 [停止]
     */
    public String formatStop(){
        if (this.stop == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(stop);
    }
    /**
     * 设置 [文档ID]
     */
    public void setResId(Integer resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [循环ID]
     */
    public void setRecurrentId(Integer recurrentId){
        this.recurrentId = recurrentId ;
        this.modify("recurrent_id",recurrentId);
    }

    /**
     * 设置 [重复终止]
     */
    public void setEndType(String endType){
        this.endType = endType ;
        this.modify("end_type",endType);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [重新提起]
     */
    public void setRruleType(String rruleType){
        this.rruleType = rruleType ;
        this.modify("rrule_type",rruleType);
    }

    /**
     * 设置 [重复]
     */
    public void setInterval(Integer interval){
        this.interval = interval ;
        this.modify("interval",interval);
    }

    /**
     * 设置 [隐私]
     */
    public void setPrivacy(String privacy){
        this.privacy = privacy ;
        this.modify("privacy",privacy);
    }

    /**
     * 设置 [持续时间]
     */
    public void setDuration(Double duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }

    /**
     * 设置 [开始时间]
     */
    public void setStartDatetime(Timestamp startDatetime){
        this.startDatetime = startDatetime ;
        this.modify("start_datetime",startDatetime);
    }

    /**
     * 格式化日期 [开始时间]
     */
    public String formatStartDatetime(){
        if (this.startDatetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(startDatetime);
    }
    /**
     * 设置 [开始日期]
     */
    public void setStartDate(Timestamp startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatStartDate(){
        if (this.startDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(startDate);
    }
    /**
     * 设置 [周一]
     */
    public void setMo(Boolean mo){
        this.mo = mo ;
        this.modify("mo",mo);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [周三]
     */
    public void setWe(Boolean we){
        this.we = we ;
        this.modify("we",we);
    }

    /**
     * 设置 [日期]
     */
    public void setDisplayStart(String displayStart){
        this.displayStart = displayStart ;
        this.modify("display_start",displayStart);
    }

    /**
     * 设置 [结束日期]
     */
    public void setStopDate(Timestamp stopDate){
        this.stopDate = stopDate ;
        this.modify("stop_date",stopDate);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatStopDate(){
        if (this.stopDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(stopDate);
    }
    /**
     * 设置 [重复直到]
     */
    public void setFinalDate(Timestamp finalDate){
        this.finalDate = finalDate ;
        this.modify("final_date",finalDate);
    }

    /**
     * 格式化日期 [重复直到]
     */
    public String formatFinalDate(){
        if (this.finalDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(finalDate);
    }
    /**
     * 设置 [文档模型]
     */
    public void setResModelId(Integer resModelId){
        this.resModelId = resModelId ;
        this.modify("res_model_id",resModelId);
    }

    /**
     * 设置 [结束日期时间]
     */
    public void setStopDatetime(Timestamp stopDatetime){
        this.stopDatetime = stopDatetime ;
        this.modify("stop_datetime",stopDatetime);
    }

    /**
     * 格式化日期 [结束日期时间]
     */
    public String formatStopDatetime(){
        if (this.stopDatetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(stopDatetime);
    }
    /**
     * 设置 [文档模型名称]
     */
    public void setResModel(String resModel){
        this.resModel = resModel ;
        this.modify("res_model",resModel);
    }

    /**
     * 设置 [周二]
     */
    public void setTu(Boolean tu){
        this.tu = tu ;
        this.modify("tu",tu);
    }

    /**
     * 设置 [显示时间为]
     */
    public void setShowAs(String showAs){
        this.showAs = showAs ;
        this.modify("show_as",showAs);
    }

    /**
     * 设置 [周四]
     */
    public void setTh(Boolean th){
        this.th = th ;
        this.modify("th",th);
    }

    /**
     * 设置 [按 天]
     */
    public void setByday(String byday){
        this.byday = byday ;
        this.modify("byday",byday);
    }

    /**
     * 设置 [日期]
     */
    public void setDay(Integer day){
        this.day = day ;
        this.modify("day",day);
    }

    /**
     * 设置 [开始]
     */
    public void setStart(Timestamp start){
        this.start = start ;
        this.modify("start",start);
    }

    /**
     * 格式化日期 [开始]
     */
    public String formatStart(){
        if (this.start == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(start);
    }
    /**
     * 设置 [全天]
     */
    public void setAllday(Boolean allday){
        this.allday = allday ;
        this.modify("allday",allday);
    }

    /**
     * 设置 [周日]
     */
    public void setSu(Boolean su){
        this.su = su ;
        this.modify("su",su);
    }

    /**
     * 设置 [会议主题]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [申请人]
     */
    public void setApplicantId(Long applicantId){
        this.applicantId = applicantId ;
        this.modify("applicant_id",applicantId);
    }

    /**
     * 设置 [所有者]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [商机]
     */
    public void setOpportunityId(Long opportunityId){
        this.opportunityId = opportunityId ;
        this.modify("opportunity_id",opportunityId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


