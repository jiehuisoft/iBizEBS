package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mail;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mailSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_mail] 服务对象接口
 */
public interface IMail_mailService extends IService<Mail_mail>{

    boolean create(Mail_mail et) ;
    void createBatch(List<Mail_mail> list) ;
    boolean update(Mail_mail et) ;
    void updateBatch(List<Mail_mail> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_mail get(Long key) ;
    Mail_mail getDraft(Mail_mail et) ;
    boolean checkKey(Mail_mail et) ;
    boolean save(Mail_mail et) ;
    void saveBatch(List<Mail_mail> list) ;
    Page<Mail_mail> searchDefault(Mail_mailSearchContext context) ;
    List<Mail_mail> selectByFetchmailServerId(Long id);
    void resetByFetchmailServerId(Long id);
    void resetByFetchmailServerId(Collection<Long> ids);
    void removeByFetchmailServerId(Long id);
    List<Mail_mail> selectByMailingId(Long id);
    void resetByMailingId(Long id);
    void resetByMailingId(Collection<Long> ids);
    void removeByMailingId(Long id);
    List<Mail_mail> selectByMailMessageId(Long id);
    void removeByMailMessageId(Collection<Long> ids);
    void removeByMailMessageId(Long id);
    List<Mail_mail> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_mail> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_mail> getMailMailByIds(List<Long> ids) ;
    List<Mail_mail> getMailMailByEntities(List<Mail_mail> entities) ;
}


