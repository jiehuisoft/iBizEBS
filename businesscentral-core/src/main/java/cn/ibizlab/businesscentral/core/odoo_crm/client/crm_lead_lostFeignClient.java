package cn.ibizlab.businesscentral.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead_lost;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_lead_lostSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[crm_lead_lost] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-crm:odoo-crm}", contextId = "crm-lead-lost", fallback = crm_lead_lostFallback.class)
public interface crm_lead_lostFeignClient {



    @RequestMapping(method = RequestMethod.GET, value = "/crm_lead_losts/{id}")
    Crm_lead_lost get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_losts/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead_losts/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_losts/{id}")
    Crm_lead_lost update(@PathVariable("id") Long id,@RequestBody Crm_lead_lost crm_lead_lost);

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_lead_losts/batch")
    Boolean updateBatch(@RequestBody List<Crm_lead_lost> crm_lead_losts);




    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts/search")
    Page<Crm_lead_lost> search(@RequestBody Crm_lead_lostSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts")
    Crm_lead_lost create(@RequestBody Crm_lead_lost crm_lead_lost);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts/batch")
    Boolean createBatch(@RequestBody List<Crm_lead_lost> crm_lead_losts);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_lead_losts/select")
    Page<Crm_lead_lost> select();


    @RequestMapping(method = RequestMethod.GET, value = "/crm_lead_losts/getdraft")
    Crm_lead_lost getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts/checkkey")
    Boolean checkKey(@RequestBody Crm_lead_lost crm_lead_lost);


    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts/save")
    Boolean save(@RequestBody Crm_lead_lost crm_lead_lost);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts/savebatch")
    Boolean saveBatch(@RequestBody List<Crm_lead_lost> crm_lead_losts);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_lead_losts/searchdefault")
    Page<Crm_lead_lost> searchDefault(@RequestBody Crm_lead_lostSearchContext context);


}
