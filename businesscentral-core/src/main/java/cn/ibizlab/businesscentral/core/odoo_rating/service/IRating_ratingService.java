package cn.ibizlab.businesscentral.core.odoo_rating.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_rating.domain.Rating_rating;
import cn.ibizlab.businesscentral.core.odoo_rating.filter.Rating_ratingSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Rating_rating] 服务对象接口
 */
public interface IRating_ratingService extends IService<Rating_rating>{

    boolean create(Rating_rating et) ;
    void createBatch(List<Rating_rating> list) ;
    boolean update(Rating_rating et) ;
    void updateBatch(List<Rating_rating> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Rating_rating get(Long key) ;
    Rating_rating getDraft(Rating_rating et) ;
    boolean checkKey(Rating_rating et) ;
    boolean save(Rating_rating et) ;
    void saveBatch(List<Rating_rating> list) ;
    Page<Rating_rating> searchDefault(Rating_ratingSearchContext context) ;
    List<Rating_rating> selectByMessageId(Long id);
    void resetByMessageId(Long id);
    void resetByMessageId(Collection<Long> ids);
    void removeByMessageId(Long id);
    List<Rating_rating> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Rating_rating> selectByRatedPartnerId(Long id);
    void resetByRatedPartnerId(Long id);
    void resetByRatedPartnerId(Collection<Long> ids);
    void removeByRatedPartnerId(Long id);
    List<Rating_rating> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Rating_rating> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Rating_rating> getRatingRatingByIds(List<Long> ids) ;
    List<Rating_rating> getRatingRatingByEntities(List<Rating_rating> entities) ;
}


