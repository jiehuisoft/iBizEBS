package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_cash_rounding;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_cash_roundingSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_cash_rounding] 服务对象接口
 */
@Component
public class account_cash_roundingFallback implements account_cash_roundingFeignClient{

    public Account_cash_rounding get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Account_cash_rounding create(Account_cash_rounding account_cash_rounding){
            return null;
     }
    public Boolean createBatch(List<Account_cash_rounding> account_cash_roundings){
            return false;
     }

    public Account_cash_rounding update(Long id, Account_cash_rounding account_cash_rounding){
            return null;
     }
    public Boolean updateBatch(List<Account_cash_rounding> account_cash_roundings){
            return false;
     }


    public Page<Account_cash_rounding> search(Account_cash_roundingSearchContext context){
            return null;
     }



    public Page<Account_cash_rounding> select(){
            return null;
     }

    public Account_cash_rounding getDraft(){
            return null;
    }



    public Boolean checkKey(Account_cash_rounding account_cash_rounding){
            return false;
     }


    public Boolean save(Account_cash_rounding account_cash_rounding){
            return false;
     }
    public Boolean saveBatch(List<Account_cash_rounding> account_cash_roundings){
            return false;
     }

    public Page<Account_cash_rounding> searchDefault(Account_cash_roundingSearchContext context){
            return null;
     }


}
