package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_account_templateSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_account_templateService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_account_templateMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[科目模板] 服务对象接口实现
 */
@Slf4j
@Service("Account_account_templateServiceImpl")
public class Account_account_templateServiceImpl extends EBSServiceImpl<Account_account_templateMapper, Account_account_template> implements IAccount_account_templateService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_chart_templateService accountChartTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_position_account_templateService accountFiscalPositionAccountTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_reconcile_model_templateService accountReconcileModelTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_tax_templateService accountTaxTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_account_typeService accountAccountTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_groupService accountGroupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.account.template" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_account_template et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_account_templateService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_account_template> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_account_template et) {
        Account_account_template old = new Account_account_template() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_account_templateService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_account_templateService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_account_template> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountChartTemplateService.resetByExpenseCurrencyExchangeAccountId(key);
        accountChartTemplateService.resetByIncomeCurrencyExchangeAccountId(key);
        accountChartTemplateService.resetByPropertyAccountExpenseCategId(key);
        accountChartTemplateService.resetByPropertyAccountExpenseId(key);
        accountChartTemplateService.resetByPropertyAccountIncomeCategId(key);
        accountChartTemplateService.resetByPropertyAccountIncomeId(key);
        accountChartTemplateService.resetByPropertyAccountPayableId(key);
        accountChartTemplateService.resetByPropertyAccountReceivableId(key);
        accountChartTemplateService.resetByPropertyStockAccountInputCategId(key);
        accountChartTemplateService.resetByPropertyStockAccountOutputCategId(key);
        accountChartTemplateService.resetByPropertyStockValuationAccountId(key);
        accountFiscalPositionAccountTemplateService.resetByAccountDestId(key);
        accountFiscalPositionAccountTemplateService.resetByAccountSrcId(key);
        accountReconcileModelTemplateService.removeByAccountId(key);
        accountReconcileModelTemplateService.removeBySecondAccountId(key);
        if(!ObjectUtils.isEmpty(accountTaxTemplateService.selectByAccountId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_tax_template]数据，无法删除!","","");
        accountTaxTemplateService.resetByCashBasisAccountId(key);
        accountTaxTemplateService.resetByCashBasisBaseAccountId(key);
        if(!ObjectUtils.isEmpty(accountTaxTemplateService.selectByRefundAccountId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_tax_template]数据，无法删除!","","");
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountChartTemplateService.resetByExpenseCurrencyExchangeAccountId(idList);
        accountChartTemplateService.resetByIncomeCurrencyExchangeAccountId(idList);
        accountChartTemplateService.resetByPropertyAccountExpenseCategId(idList);
        accountChartTemplateService.resetByPropertyAccountExpenseId(idList);
        accountChartTemplateService.resetByPropertyAccountIncomeCategId(idList);
        accountChartTemplateService.resetByPropertyAccountIncomeId(idList);
        accountChartTemplateService.resetByPropertyAccountPayableId(idList);
        accountChartTemplateService.resetByPropertyAccountReceivableId(idList);
        accountChartTemplateService.resetByPropertyStockAccountInputCategId(idList);
        accountChartTemplateService.resetByPropertyStockAccountOutputCategId(idList);
        accountChartTemplateService.resetByPropertyStockValuationAccountId(idList);
        accountFiscalPositionAccountTemplateService.resetByAccountDestId(idList);
        accountFiscalPositionAccountTemplateService.resetByAccountSrcId(idList);
        accountReconcileModelTemplateService.removeByAccountId(idList);
        accountReconcileModelTemplateService.removeBySecondAccountId(idList);
        if(!ObjectUtils.isEmpty(accountTaxTemplateService.selectByAccountId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_tax_template]数据，无法删除!","","");
        accountTaxTemplateService.resetByCashBasisAccountId(idList);
        accountTaxTemplateService.resetByCashBasisBaseAccountId(idList);
        if(!ObjectUtils.isEmpty(accountTaxTemplateService.selectByRefundAccountId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_tax_template]数据，无法删除!","","");
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_account_template get(Long key) {
        Account_account_template et = getById(key);
        if(et==null){
            et=new Account_account_template();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_account_template getDraft(Account_account_template et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_account_template et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_account_template et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_account_template et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_account_template> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_account_template> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_account_template> selectByUserTypeId(Long id) {
        return baseMapper.selectByUserTypeId(id);
    }
    @Override
    public void resetByUserTypeId(Long id) {
        this.update(new UpdateWrapper<Account_account_template>().set("user_type_id",null).eq("user_type_id",id));
    }

    @Override
    public void resetByUserTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_account_template>().set("user_type_id",null).in("user_type_id",ids));
    }

    @Override
    public void removeByUserTypeId(Long id) {
        this.remove(new QueryWrapper<Account_account_template>().eq("user_type_id",id));
    }

	@Override
    public List<Account_account_template> selectByChartTemplateId(Long id) {
        return baseMapper.selectByChartTemplateId(id);
    }
    @Override
    public void resetByChartTemplateId(Long id) {
        this.update(new UpdateWrapper<Account_account_template>().set("chart_template_id",null).eq("chart_template_id",id));
    }

    @Override
    public void resetByChartTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_account_template>().set("chart_template_id",null).in("chart_template_id",ids));
    }

    @Override
    public void removeByChartTemplateId(Long id) {
        this.remove(new QueryWrapper<Account_account_template>().eq("chart_template_id",id));
    }

	@Override
    public List<Account_account_template> selectByGroupId(Long id) {
        return baseMapper.selectByGroupId(id);
    }
    @Override
    public void resetByGroupId(Long id) {
        this.update(new UpdateWrapper<Account_account_template>().set("group_id",null).eq("group_id",id));
    }

    @Override
    public void resetByGroupId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_account_template>().set("group_id",null).in("group_id",ids));
    }

    @Override
    public void removeByGroupId(Long id) {
        this.remove(new QueryWrapper<Account_account_template>().eq("group_id",id));
    }

	@Override
    public List<Account_account_template> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Account_account_template>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_account_template>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Account_account_template>().eq("currency_id",id));
    }

	@Override
    public List<Account_account_template> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_account_template>().eq("create_uid",id));
    }

	@Override
    public List<Account_account_template> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_account_template>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_account_template> searchDefault(Account_account_templateSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_account_template> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_account_template>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_account_template et){
        //实体关系[DER1N_ACCOUNT_ACCOUNT_TEMPLATE__ACCOUNT_ACCOUNT_TYPE__USER_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getUserTypeId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_type odooUserType=et.getOdooUserType();
            if(ObjectUtils.isEmpty(odooUserType)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_type majorEntity=accountAccountTypeService.get(et.getUserTypeId());
                et.setOdooUserType(majorEntity);
                odooUserType=majorEntity;
            }
            et.setUserTypeIdText(odooUserType.getName());
        }
        //实体关系[DER1N_ACCOUNT_ACCOUNT_TEMPLATE__ACCOUNT_CHART_TEMPLATE__CHART_TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getChartTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template odooChartTemplate=et.getOdooChartTemplate();
            if(ObjectUtils.isEmpty(odooChartTemplate)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template majorEntity=accountChartTemplateService.get(et.getChartTemplateId());
                et.setOdooChartTemplate(majorEntity);
                odooChartTemplate=majorEntity;
            }
            et.setChartTemplateIdText(odooChartTemplate.getName());
        }
        //实体关系[DER1N_ACCOUNT_ACCOUNT_TEMPLATE__ACCOUNT_GROUP__GROUP_ID]
        if(!ObjectUtils.isEmpty(et.getGroupId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_group odooGroup=et.getOdooGroup();
            if(ObjectUtils.isEmpty(odooGroup)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_group majorEntity=accountGroupService.get(et.getGroupId());
                et.setOdooGroup(majorEntity);
                odooGroup=majorEntity;
            }
            et.setGroupIdText(odooGroup.getName());
        }
        //实体关系[DER1N_ACCOUNT_ACCOUNT_TEMPLATE__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_ACCOUNT_ACCOUNT_TEMPLATE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_ACCOUNT_TEMPLATE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_account_template> getAccountAccountTemplateByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_account_template> getAccountAccountTemplateByEntities(List<Account_account_template> entities) {
        List ids =new ArrayList();
        for(Account_account_template entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



