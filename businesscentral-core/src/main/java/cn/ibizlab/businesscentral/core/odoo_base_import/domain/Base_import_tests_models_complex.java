package cn.ibizlab.businesscentral.core.odoo_base_import.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[测试:基本导入模型复合体]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "BASE_IMPORT_TESTS_MODELS_COMPLEX",resultMap = "Base_import_tests_models_complexResultMap")
public class Base_import_tests_models_complex extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * Dt
     */
    @TableField(value = "dt")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "dt" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("dt")
    private Timestamp dt;
    /**
     * C
     */
    @TableField(value = "c")
    @JSONField(name = "c")
    @JsonProperty("c")
    private String c;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * F
     */
    @TableField(value = "f")
    @JSONField(name = "f")
    @JsonProperty("f")
    private Double f;
    /**
     * M
     */
    @TableField(value = "m")
    @JSONField(name = "m")
    @JsonProperty("m")
    private BigDecimal m;
    /**
     * D
     */
    @TableField(value = "d")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "d" , format="yyyy-MM-dd")
    @JsonProperty("d")
    private Timestamp d;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @TableField(value = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [Dt]
     */
    public void setDt(Timestamp dt){
        this.dt = dt ;
        this.modify("dt",dt);
    }

    /**
     * 格式化日期 [Dt]
     */
    public String formatDt(){
        if (this.dt == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dt);
    }
    /**
     * 设置 [C]
     */
    public void setC(String c){
        this.c = c ;
        this.modify("c",c);
    }

    /**
     * 设置 [F]
     */
    public void setF(Double f){
        this.f = f ;
        this.modify("f",f);
    }

    /**
     * 设置 [M]
     */
    public void setM(BigDecimal m){
        this.m = m ;
        this.modify("m",m);
    }

    /**
     * 设置 [D]
     */
    public void setD(Timestamp d){
        this.d = d ;
        this.modify("d",d);
    }

    /**
     * 格式化日期 [D]
     */
    public String formatD(){
        if (this.d == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(d);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Long currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


