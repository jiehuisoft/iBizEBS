package cn.ibizlab.businesscentral.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_page;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_pageSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Survey_page] 服务对象接口
 */
public interface ISurvey_pageService extends IService<Survey_page>{

    boolean create(Survey_page et) ;
    void createBatch(List<Survey_page> list) ;
    boolean update(Survey_page et) ;
    void updateBatch(List<Survey_page> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Survey_page get(Long key) ;
    Survey_page getDraft(Survey_page et) ;
    boolean checkKey(Survey_page et) ;
    boolean save(Survey_page et) ;
    void saveBatch(List<Survey_page> list) ;
    Page<Survey_page> searchDefault(Survey_pageSearchContext context) ;
    List<Survey_page> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Survey_page> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Survey_page> selectBySurveyId(Long id);
    void removeBySurveyId(Collection<Long> ids);
    void removeBySurveyId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Survey_page> getSurveyPageByIds(List<Long> ids) ;
    List<Survey_page> getSurveyPageByEntities(List<Survey_page> entities) ;
}


