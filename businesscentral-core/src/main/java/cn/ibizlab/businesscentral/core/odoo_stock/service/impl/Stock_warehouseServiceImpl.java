package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warehouseSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_warehouseMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[仓库] 服务对象接口实现
 */
@Slf4j
@Service("Stock_warehouseServiceImpl")
public class Stock_warehouseServiceImpl extends EBSServiceImpl<Stock_warehouseMapper, Stock_warehouse> implements IStock_warehouseService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_replenishService productReplenishService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_reportService purchaseReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisitionService purchaseRequisitionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_orderService saleOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_reportService saleReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_location_routeService stockLocationRouteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_picking_typeService stockPickingTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_ruleService stockRuleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouse_orderpointService stockWarehouseOrderpointService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.warehouse" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_warehouse et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_warehouseService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_warehouse> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_warehouse et) {
        Stock_warehouse old = new Stock_warehouse() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_warehouseService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_warehouseService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_warehouse> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        productReplenishService.resetByWarehouseId(key);
        purchaseReportService.resetByPickingTypeId(key);
        saleOrderService.resetByWarehouseId(key);
        saleReportService.resetByWarehouseId(key);
        stockLocationRouteService.resetBySuppliedWhId(key);
        stockLocationRouteService.resetBySupplierWhId(key);
        stockMoveService.resetByWarehouseId(key);
        stockPickingTypeService.removeByWarehouseId(key);
        stockRuleService.resetByPropagateWarehouseId(key);
        stockRuleService.resetByWarehouseId(key);
        stockWarehouseOrderpointService.removeByWarehouseId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        productReplenishService.resetByWarehouseId(idList);
        purchaseReportService.resetByPickingTypeId(idList);
        saleOrderService.resetByWarehouseId(idList);
        saleReportService.resetByWarehouseId(idList);
        stockLocationRouteService.resetBySuppliedWhId(idList);
        stockLocationRouteService.resetBySupplierWhId(idList);
        stockMoveService.resetByWarehouseId(idList);
        stockPickingTypeService.removeByWarehouseId(idList);
        stockRuleService.resetByPropagateWarehouseId(idList);
        stockRuleService.resetByWarehouseId(idList);
        stockWarehouseOrderpointService.removeByWarehouseId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_warehouse get(Long key) {
        Stock_warehouse et = getById(key);
        if(et==null){
            et=new Stock_warehouse();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_warehouse getDraft(Stock_warehouse et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_warehouse et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_warehouse et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_warehouse et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_warehouse> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_warehouse> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_warehouse> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("company_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("partner_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("create_uid",id));
    }

	@Override
    public List<Stock_warehouse> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("write_uid",id));
    }

	@Override
    public List<Stock_warehouse> selectByCrossdockRouteId(Long id) {
        return baseMapper.selectByCrossdockRouteId(id);
    }
    @Override
    public List<Stock_warehouse> selectByCrossdockRouteId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Stock_warehouse>().in("id",ids));
    }

    @Override
    public void removeByCrossdockRouteId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("crossdock_route_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByDeliveryRouteId(Long id) {
        return baseMapper.selectByDeliveryRouteId(id);
    }
    @Override
    public List<Stock_warehouse> selectByDeliveryRouteId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Stock_warehouse>().in("id",ids));
    }

    @Override
    public void removeByDeliveryRouteId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("delivery_route_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByPbmRouteId(Long id) {
        return baseMapper.selectByPbmRouteId(id);
    }
    @Override
    public List<Stock_warehouse> selectByPbmRouteId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Stock_warehouse>().in("id",ids));
    }

    @Override
    public void removeByPbmRouteId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("pbm_route_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByReceptionRouteId(Long id) {
        return baseMapper.selectByReceptionRouteId(id);
    }
    @Override
    public List<Stock_warehouse> selectByReceptionRouteId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Stock_warehouse>().in("id",ids));
    }

    @Override
    public void removeByReceptionRouteId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("reception_route_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByLotStockId(Long id) {
        return baseMapper.selectByLotStockId(id);
    }
    @Override
    public void resetByLotStockId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("lot_stock_id",null).eq("lot_stock_id",id));
    }

    @Override
    public void resetByLotStockId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("lot_stock_id",null).in("lot_stock_id",ids));
    }

    @Override
    public void removeByLotStockId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("lot_stock_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByPbmLocId(Long id) {
        return baseMapper.selectByPbmLocId(id);
    }
    @Override
    public void resetByPbmLocId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("pbm_loc_id",null).eq("pbm_loc_id",id));
    }

    @Override
    public void resetByPbmLocId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("pbm_loc_id",null).in("pbm_loc_id",ids));
    }

    @Override
    public void removeByPbmLocId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("pbm_loc_id",id));
    }

	@Override
    public List<Stock_warehouse> selectBySamLocId(Long id) {
        return baseMapper.selectBySamLocId(id);
    }
    @Override
    public void resetBySamLocId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("sam_loc_id",null).eq("sam_loc_id",id));
    }

    @Override
    public void resetBySamLocId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("sam_loc_id",null).in("sam_loc_id",ids));
    }

    @Override
    public void removeBySamLocId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("sam_loc_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByViewLocationId(Long id) {
        return baseMapper.selectByViewLocationId(id);
    }
    @Override
    public void resetByViewLocationId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("view_location_id",null).eq("view_location_id",id));
    }

    @Override
    public void resetByViewLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("view_location_id",null).in("view_location_id",ids));
    }

    @Override
    public void removeByViewLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("view_location_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByWhInputStockLocId(Long id) {
        return baseMapper.selectByWhInputStockLocId(id);
    }
    @Override
    public void resetByWhInputStockLocId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("wh_input_stock_loc_id",null).eq("wh_input_stock_loc_id",id));
    }

    @Override
    public void resetByWhInputStockLocId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("wh_input_stock_loc_id",null).in("wh_input_stock_loc_id",ids));
    }

    @Override
    public void removeByWhInputStockLocId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("wh_input_stock_loc_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByWhOutputStockLocId(Long id) {
        return baseMapper.selectByWhOutputStockLocId(id);
    }
    @Override
    public void resetByWhOutputStockLocId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("wh_output_stock_loc_id",null).eq("wh_output_stock_loc_id",id));
    }

    @Override
    public void resetByWhOutputStockLocId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("wh_output_stock_loc_id",null).in("wh_output_stock_loc_id",ids));
    }

    @Override
    public void removeByWhOutputStockLocId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("wh_output_stock_loc_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByWhPackStockLocId(Long id) {
        return baseMapper.selectByWhPackStockLocId(id);
    }
    @Override
    public void resetByWhPackStockLocId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("wh_pack_stock_loc_id",null).eq("wh_pack_stock_loc_id",id));
    }

    @Override
    public void resetByWhPackStockLocId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("wh_pack_stock_loc_id",null).in("wh_pack_stock_loc_id",ids));
    }

    @Override
    public void removeByWhPackStockLocId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("wh_pack_stock_loc_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByWhQcStockLocId(Long id) {
        return baseMapper.selectByWhQcStockLocId(id);
    }
    @Override
    public void resetByWhQcStockLocId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("wh_qc_stock_loc_id",null).eq("wh_qc_stock_loc_id",id));
    }

    @Override
    public void resetByWhQcStockLocId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("wh_qc_stock_loc_id",null).in("wh_qc_stock_loc_id",ids));
    }

    @Override
    public void removeByWhQcStockLocId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("wh_qc_stock_loc_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByIntTypeId(Long id) {
        return baseMapper.selectByIntTypeId(id);
    }
    @Override
    public void resetByIntTypeId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("int_type_id",null).eq("int_type_id",id));
    }

    @Override
    public void resetByIntTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("int_type_id",null).in("int_type_id",ids));
    }

    @Override
    public void removeByIntTypeId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("int_type_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByInTypeId(Long id) {
        return baseMapper.selectByInTypeId(id);
    }
    @Override
    public void resetByInTypeId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("in_type_id",null).eq("in_type_id",id));
    }

    @Override
    public void resetByInTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("in_type_id",null).in("in_type_id",ids));
    }

    @Override
    public void removeByInTypeId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("in_type_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByManuTypeId(Long id) {
        return baseMapper.selectByManuTypeId(id);
    }
    @Override
    public void resetByManuTypeId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("manu_type_id",null).eq("manu_type_id",id));
    }

    @Override
    public void resetByManuTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("manu_type_id",null).in("manu_type_id",ids));
    }

    @Override
    public void removeByManuTypeId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("manu_type_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByOutTypeId(Long id) {
        return baseMapper.selectByOutTypeId(id);
    }
    @Override
    public void resetByOutTypeId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("out_type_id",null).eq("out_type_id",id));
    }

    @Override
    public void resetByOutTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("out_type_id",null).in("out_type_id",ids));
    }

    @Override
    public void removeByOutTypeId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("out_type_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByPackTypeId(Long id) {
        return baseMapper.selectByPackTypeId(id);
    }
    @Override
    public void resetByPackTypeId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("pack_type_id",null).eq("pack_type_id",id));
    }

    @Override
    public void resetByPackTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("pack_type_id",null).in("pack_type_id",ids));
    }

    @Override
    public void removeByPackTypeId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("pack_type_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByPbmTypeId(Long id) {
        return baseMapper.selectByPbmTypeId(id);
    }
    @Override
    public void resetByPbmTypeId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("pbm_type_id",null).eq("pbm_type_id",id));
    }

    @Override
    public void resetByPbmTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("pbm_type_id",null).in("pbm_type_id",ids));
    }

    @Override
    public void removeByPbmTypeId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("pbm_type_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByPickTypeId(Long id) {
        return baseMapper.selectByPickTypeId(id);
    }
    @Override
    public void resetByPickTypeId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("pick_type_id",null).eq("pick_type_id",id));
    }

    @Override
    public void resetByPickTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("pick_type_id",null).in("pick_type_id",ids));
    }

    @Override
    public void removeByPickTypeId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("pick_type_id",id));
    }

	@Override
    public List<Stock_warehouse> selectBySamTypeId(Long id) {
        return baseMapper.selectBySamTypeId(id);
    }
    @Override
    public void resetBySamTypeId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("sam_type_id",null).eq("sam_type_id",id));
    }

    @Override
    public void resetBySamTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("sam_type_id",null).in("sam_type_id",ids));
    }

    @Override
    public void removeBySamTypeId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("sam_type_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByBuyPullId(Long id) {
        return baseMapper.selectByBuyPullId(id);
    }
    @Override
    public void resetByBuyPullId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("buy_pull_id",null).eq("buy_pull_id",id));
    }

    @Override
    public void resetByBuyPullId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("buy_pull_id",null).in("buy_pull_id",ids));
    }

    @Override
    public void removeByBuyPullId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("buy_pull_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByManufacturePullId(Long id) {
        return baseMapper.selectByManufacturePullId(id);
    }
    @Override
    public void resetByManufacturePullId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("manufacture_pull_id",null).eq("manufacture_pull_id",id));
    }

    @Override
    public void resetByManufacturePullId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("manufacture_pull_id",null).in("manufacture_pull_id",ids));
    }

    @Override
    public void removeByManufacturePullId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("manufacture_pull_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByMtoPullId(Long id) {
        return baseMapper.selectByMtoPullId(id);
    }
    @Override
    public void resetByMtoPullId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("mto_pull_id",null).eq("mto_pull_id",id));
    }

    @Override
    public void resetByMtoPullId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("mto_pull_id",null).in("mto_pull_id",ids));
    }

    @Override
    public void removeByMtoPullId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("mto_pull_id",id));
    }

	@Override
    public List<Stock_warehouse> selectByPbmMtoPullId(Long id) {
        return baseMapper.selectByPbmMtoPullId(id);
    }
    @Override
    public void resetByPbmMtoPullId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("pbm_mto_pull_id",null).eq("pbm_mto_pull_id",id));
    }

    @Override
    public void resetByPbmMtoPullId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("pbm_mto_pull_id",null).in("pbm_mto_pull_id",ids));
    }

    @Override
    public void removeByPbmMtoPullId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("pbm_mto_pull_id",id));
    }

	@Override
    public List<Stock_warehouse> selectBySamRuleId(Long id) {
        return baseMapper.selectBySamRuleId(id);
    }
    @Override
    public void resetBySamRuleId(Long id) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("sam_rule_id",null).eq("sam_rule_id",id));
    }

    @Override
    public void resetBySamRuleId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_warehouse>().set("sam_rule_id",null).in("sam_rule_id",ids));
    }

    @Override
    public void removeBySamRuleId(Long id) {
        this.remove(new QueryWrapper<Stock_warehouse>().eq("sam_rule_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_warehouse> searchDefault(Stock_warehouseSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_warehouse> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_warehouse>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_warehouse et){
        //实体关系[DER1N_STOCK_WAREHOUSE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_LOCATION_ROUTE__CROSSDOCK_ROUTE_ID]
        if(!ObjectUtils.isEmpty(et.getCrossdockRouteId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route odooCrossdockRoute=et.getOdooCrossdockRoute();
            if(ObjectUtils.isEmpty(odooCrossdockRoute)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route majorEntity=stockLocationRouteService.get(et.getCrossdockRouteId());
                et.setOdooCrossdockRoute(majorEntity);
                odooCrossdockRoute=majorEntity;
            }
            et.setCrossdockRouteIdText(odooCrossdockRoute.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_LOCATION_ROUTE__DELIVERY_ROUTE_ID]
        if(!ObjectUtils.isEmpty(et.getDeliveryRouteId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route odooDeliveryRoute=et.getOdooDeliveryRoute();
            if(ObjectUtils.isEmpty(odooDeliveryRoute)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route majorEntity=stockLocationRouteService.get(et.getDeliveryRouteId());
                et.setOdooDeliveryRoute(majorEntity);
                odooDeliveryRoute=majorEntity;
            }
            et.setDeliveryRouteIdText(odooDeliveryRoute.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_LOCATION_ROUTE__PBM_ROUTE_ID]
        if(!ObjectUtils.isEmpty(et.getPbmRouteId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route odooPbmRoute=et.getOdooPbmRoute();
            if(ObjectUtils.isEmpty(odooPbmRoute)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route majorEntity=stockLocationRouteService.get(et.getPbmRouteId());
                et.setOdooPbmRoute(majorEntity);
                odooPbmRoute=majorEntity;
            }
            et.setPbmRouteIdText(odooPbmRoute.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_LOCATION_ROUTE__RECEPTION_ROUTE_ID]
        if(!ObjectUtils.isEmpty(et.getReceptionRouteId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route odooReceptionRoute=et.getOdooReceptionRoute();
            if(ObjectUtils.isEmpty(odooReceptionRoute)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route majorEntity=stockLocationRouteService.get(et.getReceptionRouteId());
                et.setOdooReceptionRoute(majorEntity);
                odooReceptionRoute=majorEntity;
            }
            et.setReceptionRouteIdText(odooReceptionRoute.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_LOCATION__LOT_STOCK_ID]
        if(!ObjectUtils.isEmpty(et.getLotStockId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLotStock=et.getOdooLotStock();
            if(ObjectUtils.isEmpty(odooLotStock)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLotStockId());
                et.setOdooLotStock(majorEntity);
                odooLotStock=majorEntity;
            }
            et.setLotStockIdText(odooLotStock.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_LOCATION__PBM_LOC_ID]
        if(!ObjectUtils.isEmpty(et.getPbmLocId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooPbmLoc=et.getOdooPbmLoc();
            if(ObjectUtils.isEmpty(odooPbmLoc)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getPbmLocId());
                et.setOdooPbmLoc(majorEntity);
                odooPbmLoc=majorEntity;
            }
            et.setPbmLocIdText(odooPbmLoc.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_LOCATION__SAM_LOC_ID]
        if(!ObjectUtils.isEmpty(et.getSamLocId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooSamLoc=et.getOdooSamLoc();
            if(ObjectUtils.isEmpty(odooSamLoc)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getSamLocId());
                et.setOdooSamLoc(majorEntity);
                odooSamLoc=majorEntity;
            }
            et.setSamLocIdText(odooSamLoc.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_LOCATION__VIEW_LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getViewLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooViewLocation=et.getOdooViewLocation();
            if(ObjectUtils.isEmpty(odooViewLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getViewLocationId());
                et.setOdooViewLocation(majorEntity);
                odooViewLocation=majorEntity;
            }
            et.setViewLocationIdText(odooViewLocation.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_LOCATION__WH_INPUT_STOCK_LOC_ID]
        if(!ObjectUtils.isEmpty(et.getWhInputStockLocId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooWhInputStockLoc=et.getOdooWhInputStockLoc();
            if(ObjectUtils.isEmpty(odooWhInputStockLoc)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getWhInputStockLocId());
                et.setOdooWhInputStockLoc(majorEntity);
                odooWhInputStockLoc=majorEntity;
            }
            et.setWhInputStockLocIdText(odooWhInputStockLoc.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_LOCATION__WH_OUTPUT_STOCK_LOC_ID]
        if(!ObjectUtils.isEmpty(et.getWhOutputStockLocId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooWhOutputStockLoc=et.getOdooWhOutputStockLoc();
            if(ObjectUtils.isEmpty(odooWhOutputStockLoc)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getWhOutputStockLocId());
                et.setOdooWhOutputStockLoc(majorEntity);
                odooWhOutputStockLoc=majorEntity;
            }
            et.setWhOutputStockLocIdText(odooWhOutputStockLoc.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_LOCATION__WH_PACK_STOCK_LOC_ID]
        if(!ObjectUtils.isEmpty(et.getWhPackStockLocId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooWhPackStockLoc=et.getOdooWhPackStockLoc();
            if(ObjectUtils.isEmpty(odooWhPackStockLoc)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getWhPackStockLocId());
                et.setOdooWhPackStockLoc(majorEntity);
                odooWhPackStockLoc=majorEntity;
            }
            et.setWhPackStockLocIdText(odooWhPackStockLoc.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_LOCATION__WH_QC_STOCK_LOC_ID]
        if(!ObjectUtils.isEmpty(et.getWhQcStockLocId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooWhQcStockLoc=et.getOdooWhQcStockLoc();
            if(ObjectUtils.isEmpty(odooWhQcStockLoc)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getWhQcStockLocId());
                et.setOdooWhQcStockLoc(majorEntity);
                odooWhQcStockLoc=majorEntity;
            }
            et.setWhQcStockLocIdText(odooWhQcStockLoc.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_PICKING_TYPE__INT_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getIntTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooIntType=et.getOdooIntType();
            if(ObjectUtils.isEmpty(odooIntType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getIntTypeId());
                et.setOdooIntType(majorEntity);
                odooIntType=majorEntity;
            }
            et.setIntTypeIdText(odooIntType.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_PICKING_TYPE__IN_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getInTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooInType=et.getOdooInType();
            if(ObjectUtils.isEmpty(odooInType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getInTypeId());
                et.setOdooInType(majorEntity);
                odooInType=majorEntity;
            }
            et.setInTypeIdText(odooInType.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_PICKING_TYPE__MANU_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getManuTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooManuType=et.getOdooManuType();
            if(ObjectUtils.isEmpty(odooManuType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getManuTypeId());
                et.setOdooManuType(majorEntity);
                odooManuType=majorEntity;
            }
            et.setManuTypeIdText(odooManuType.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_PICKING_TYPE__OUT_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getOutTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooOutType=et.getOdooOutType();
            if(ObjectUtils.isEmpty(odooOutType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getOutTypeId());
                et.setOdooOutType(majorEntity);
                odooOutType=majorEntity;
            }
            et.setOutTypeIdText(odooOutType.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_PICKING_TYPE__PACK_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getPackTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooPackType=et.getOdooPackType();
            if(ObjectUtils.isEmpty(odooPackType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getPackTypeId());
                et.setOdooPackType(majorEntity);
                odooPackType=majorEntity;
            }
            et.setPackTypeIdText(odooPackType.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_PICKING_TYPE__PBM_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getPbmTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooPbmType=et.getOdooPbmType();
            if(ObjectUtils.isEmpty(odooPbmType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getPbmTypeId());
                et.setOdooPbmType(majorEntity);
                odooPbmType=majorEntity;
            }
            et.setPbmTypeIdText(odooPbmType.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_PICKING_TYPE__PICK_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getPickTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooPickType=et.getOdooPickType();
            if(ObjectUtils.isEmpty(odooPickType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getPickTypeId());
                et.setOdooPickType(majorEntity);
                odooPickType=majorEntity;
            }
            et.setPickTypeIdText(odooPickType.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_PICKING_TYPE__SAM_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getSamTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooSamType=et.getOdooSamType();
            if(ObjectUtils.isEmpty(odooSamType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getSamTypeId());
                et.setOdooSamType(majorEntity);
                odooSamType=majorEntity;
            }
            et.setSamTypeIdText(odooSamType.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_RULE__BUY_PULL_ID]
        if(!ObjectUtils.isEmpty(et.getBuyPullId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule odooBuyPull=et.getOdooBuyPull();
            if(ObjectUtils.isEmpty(odooBuyPull)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule majorEntity=stockRuleService.get(et.getBuyPullId());
                et.setOdooBuyPull(majorEntity);
                odooBuyPull=majorEntity;
            }
            et.setBuyPullIdText(odooBuyPull.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_RULE__MANUFACTURE_PULL_ID]
        if(!ObjectUtils.isEmpty(et.getManufacturePullId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule odooManufacturePull=et.getOdooManufacturePull();
            if(ObjectUtils.isEmpty(odooManufacturePull)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule majorEntity=stockRuleService.get(et.getManufacturePullId());
                et.setOdooManufacturePull(majorEntity);
                odooManufacturePull=majorEntity;
            }
            et.setManufacturePullIdText(odooManufacturePull.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_RULE__MTO_PULL_ID]
        if(!ObjectUtils.isEmpty(et.getMtoPullId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule odooMtoPull=et.getOdooMtoPull();
            if(ObjectUtils.isEmpty(odooMtoPull)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule majorEntity=stockRuleService.get(et.getMtoPullId());
                et.setOdooMtoPull(majorEntity);
                odooMtoPull=majorEntity;
            }
            et.setMtoPullIdText(odooMtoPull.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_RULE__PBM_MTO_PULL_ID]
        if(!ObjectUtils.isEmpty(et.getPbmMtoPullId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule odooPbmMtoPull=et.getOdooPbmMtoPull();
            if(ObjectUtils.isEmpty(odooPbmMtoPull)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule majorEntity=stockRuleService.get(et.getPbmMtoPullId());
                et.setOdooPbmMtoPull(majorEntity);
                odooPbmMtoPull=majorEntity;
            }
            et.setPbmMtoPullIdText(odooPbmMtoPull.getName());
        }
        //实体关系[DER1N_STOCK_WAREHOUSE__STOCK_RULE__SAM_RULE_ID]
        if(!ObjectUtils.isEmpty(et.getSamRuleId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule odooSamRule=et.getOdooSamRule();
            if(ObjectUtils.isEmpty(odooSamRule)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule majorEntity=stockRuleService.get(et.getSamRuleId());
                et.setOdooSamRule(majorEntity);
                odooSamRule=majorEntity;
            }
            et.setSamRuleIdText(odooSamRule.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_warehouse> getStockWarehouseByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_warehouse> getStockWarehouseByEntities(List<Stock_warehouse> entities) {
        List ids =new ArrayList();
        for(Stock_warehouse entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



