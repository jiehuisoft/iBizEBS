package cn.ibizlab.businesscentral.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_resource;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_resourceSearchContext;
import cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_resourceService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_resource.mapper.Resource_resourceMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[资源] 服务对象接口实现
 */
@Slf4j
@Service("Resource_resourceServiceImpl")
public class Resource_resourceServiceImpl extends EBSServiceImpl<Resource_resourceMapper, Resource_resource> implements IResource_resourceService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenterService mrpWorkcenterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendar_leavesService resourceCalendarLeavesService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_mixinService resourceMixinService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_testService resourceTestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendarService resourceCalendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "resource.resource" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Resource_resource et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IResource_resourceService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Resource_resource> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Resource_resource et) {
        Resource_resource old = new Resource_resource() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IResource_resourceService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IResource_resourceService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Resource_resource> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        if(!ObjectUtils.isEmpty(hrEmployeeService.selectByResourceId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Hr_employee]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(mrpWorkcenterService.selectByResourceId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mrp_workcenter]数据，无法删除!","","");
        resourceCalendarLeavesService.resetByResourceId(key);
        if(!ObjectUtils.isEmpty(resourceMixinService.selectByResourceId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Resource_mixin]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(resourceTestService.selectByResourceId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Resource_test]数据，无法删除!","","");
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        if(!ObjectUtils.isEmpty(hrEmployeeService.selectByResourceId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Hr_employee]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(mrpWorkcenterService.selectByResourceId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mrp_workcenter]数据，无法删除!","","");
        resourceCalendarLeavesService.resetByResourceId(idList);
        if(!ObjectUtils.isEmpty(resourceMixinService.selectByResourceId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Resource_mixin]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(resourceTestService.selectByResourceId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Resource_test]数据，无法删除!","","");
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Resource_resource get(Long key) {
        Resource_resource et = getById(key);
        if(et==null){
            et=new Resource_resource();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Resource_resource getDraft(Resource_resource et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Resource_resource et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Resource_resource et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Resource_resource et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Resource_resource> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Resource_resource> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Resource_resource> selectByCalendarId(Long id) {
        return baseMapper.selectByCalendarId(id);
    }
    @Override
    public void resetByCalendarId(Long id) {
        this.update(new UpdateWrapper<Resource_resource>().set("calendar_id",null).eq("calendar_id",id));
    }

    @Override
    public void resetByCalendarId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Resource_resource>().set("calendar_id",null).in("calendar_id",ids));
    }

    @Override
    public void removeByCalendarId(Long id) {
        this.remove(new QueryWrapper<Resource_resource>().eq("calendar_id",id));
    }

	@Override
    public List<Resource_resource> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Resource_resource>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Resource_resource>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Resource_resource>().eq("company_id",id));
    }

	@Override
    public List<Resource_resource> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Resource_resource>().eq("create_uid",id));
    }

	@Override
    public List<Resource_resource> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Resource_resource>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Resource_resource>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Resource_resource>().eq("user_id",id));
    }

	@Override
    public List<Resource_resource> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Resource_resource>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Resource_resource> searchDefault(Resource_resourceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Resource_resource> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Resource_resource>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Resource_resource et){
        //实体关系[DER1N_RESOURCE_RESOURCE__RESOURCE_CALENDAR__CALENDAR_ID]
        if(!ObjectUtils.isEmpty(et.getCalendarId())){
            cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar odooCalendar=et.getOdooCalendar();
            if(ObjectUtils.isEmpty(odooCalendar)){
                cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar majorEntity=resourceCalendarService.get(et.getCalendarId());
                et.setOdooCalendar(majorEntity);
                odooCalendar=majorEntity;
            }
            et.setCalendarIdText(odooCalendar.getName());
        }
        //实体关系[DER1N_RESOURCE_RESOURCE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_RESOURCE_RESOURCE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_RESOURCE_RESOURCE__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_RESOURCE_RESOURCE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Resource_resource> getResourceResourceByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Resource_resource> getResourceResourceByEntities(List<Resource_resource> entities) {
        List ids =new ArrayList();
        for(Resource_resource entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



