package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_removal;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_removalSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_removal] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-product:odoo-product}", contextId = "product-removal", fallback = product_removalFallback.class)
public interface product_removalFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_removals/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_removals/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);





    @RequestMapping(method = RequestMethod.POST, value = "/product_removals/search")
    Page<Product_removal> search(@RequestBody Product_removalSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/product_removals")
    Product_removal create(@RequestBody Product_removal product_removal);

    @RequestMapping(method = RequestMethod.POST, value = "/product_removals/batch")
    Boolean createBatch(@RequestBody List<Product_removal> product_removals);



    @RequestMapping(method = RequestMethod.GET, value = "/product_removals/{id}")
    Product_removal get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_removals/{id}")
    Product_removal update(@PathVariable("id") Long id,@RequestBody Product_removal product_removal);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_removals/batch")
    Boolean updateBatch(@RequestBody List<Product_removal> product_removals);


    @RequestMapping(method = RequestMethod.GET, value = "/product_removals/select")
    Page<Product_removal> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_removals/getdraft")
    Product_removal getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/product_removals/checkkey")
    Boolean checkKey(@RequestBody Product_removal product_removal);


    @RequestMapping(method = RequestMethod.POST, value = "/product_removals/save")
    Boolean save(@RequestBody Product_removal product_removal);

    @RequestMapping(method = RequestMethod.POST, value = "/product_removals/savebatch")
    Boolean saveBatch(@RequestBody List<Product_removal> product_removals);



    @RequestMapping(method = RequestMethod.POST, value = "/product_removals/searchdefault")
    Page<Product_removal> searchDefault(@RequestBody Product_removalSearchContext context);


}
