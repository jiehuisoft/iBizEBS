package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_moveSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_move] 服务对象接口
 */
public interface IStock_moveService extends IService<Stock_move>{

    boolean create(Stock_move et) ;
    void createBatch(List<Stock_move> list) ;
    boolean update(Stock_move et) ;
    void updateBatch(List<Stock_move> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_move get(Long key) ;
    Stock_move getDraft(Stock_move et) ;
    boolean checkKey(Stock_move et) ;
    boolean save(Stock_move et) ;
    void saveBatch(List<Stock_move> list) ;
    Page<Stock_move> searchDefault(Stock_moveSearchContext context) ;
    List<Stock_move> selectByBomLineId(Long id);
    void resetByBomLineId(Long id);
    void resetByBomLineId(Collection<Long> ids);
    void removeByBomLineId(Long id);
    List<Stock_move> selectByCreatedProductionId(Long id);
    void resetByCreatedProductionId(Long id);
    void resetByCreatedProductionId(Collection<Long> ids);
    void removeByCreatedProductionId(Long id);
    List<Stock_move> selectByProductionId(Long id);
    void resetByProductionId(Long id);
    void resetByProductionId(Collection<Long> ids);
    void removeByProductionId(Long id);
    List<Stock_move> selectByRawMaterialProductionId(Long id);
    void resetByRawMaterialProductionId(Long id);
    void resetByRawMaterialProductionId(Collection<Long> ids);
    void removeByRawMaterialProductionId(Long id);
    List<Stock_move> selectByOperationId(Long id);
    void resetByOperationId(Long id);
    void resetByOperationId(Collection<Long> ids);
    void removeByOperationId(Long id);
    List<Stock_move> selectByConsumeUnbuildId(Long id);
    void resetByConsumeUnbuildId(Long id);
    void resetByConsumeUnbuildId(Collection<Long> ids);
    void removeByConsumeUnbuildId(Long id);
    List<Stock_move> selectByUnbuildId(Long id);
    void resetByUnbuildId(Long id);
    void resetByUnbuildId(Collection<Long> ids);
    void removeByUnbuildId(Long id);
    List<Stock_move> selectByWorkorderId(Long id);
    void resetByWorkorderId(Long id);
    void resetByWorkorderId(Collection<Long> ids);
    void removeByWorkorderId(Long id);
    List<Stock_move> selectByProductPackaging(Long id);
    void resetByProductPackaging(Long id);
    void resetByProductPackaging(Collection<Long> ids);
    void removeByProductPackaging(Long id);
    List<Stock_move> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Stock_move> selectByCreatedPurchaseLineId(Long id);
    void resetByCreatedPurchaseLineId(Long id);
    void resetByCreatedPurchaseLineId(Collection<Long> ids);
    void removeByCreatedPurchaseLineId(Long id);
    List<Stock_move> selectByPurchaseLineId(Long id);
    void resetByPurchaseLineId(Long id);
    void resetByPurchaseLineId(Collection<Long> ids);
    void removeByPurchaseLineId(Long id);
    List<Stock_move> selectByRepairId(Long id);
    void resetByRepairId(Long id);
    void resetByRepairId(Collection<Long> ids);
    void removeByRepairId(Long id);
    List<Stock_move> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Stock_move> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Stock_move> selectByRestrictPartnerId(Long id);
    void resetByRestrictPartnerId(Long id);
    void resetByRestrictPartnerId(Collection<Long> ids);
    void removeByRestrictPartnerId(Long id);
    List<Stock_move> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_move> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_move> selectBySaleLineId(Long id);
    void resetBySaleLineId(Long id);
    void resetBySaleLineId(Collection<Long> ids);
    void removeBySaleLineId(Long id);
    List<Stock_move> selectByInventoryId(Long id);
    void resetByInventoryId(Long id);
    void resetByInventoryId(Collection<Long> ids);
    void removeByInventoryId(Long id);
    List<Stock_move> selectByLocationDestId(Long id);
    void resetByLocationDestId(Long id);
    void resetByLocationDestId(Collection<Long> ids);
    void removeByLocationDestId(Long id);
    List<Stock_move> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    List<Stock_move> selectByOriginReturnedMoveId(Long id);
    void resetByOriginReturnedMoveId(Long id);
    void resetByOriginReturnedMoveId(Collection<Long> ids);
    void removeByOriginReturnedMoveId(Long id);
    List<Stock_move> selectByPackageLevelId(Long id);
    void resetByPackageLevelId(Long id);
    void resetByPackageLevelId(Collection<Long> ids);
    void removeByPackageLevelId(Long id);
    List<Stock_move> selectByPickingTypeId(Long id);
    void resetByPickingTypeId(Long id);
    void resetByPickingTypeId(Collection<Long> ids);
    void removeByPickingTypeId(Long id);
    List<Stock_move> selectByPickingId(Long id);
    void resetByPickingId(Long id);
    void resetByPickingId(Collection<Long> ids);
    void removeByPickingId(Long id);
    List<Stock_move> selectByRuleId(Long id);
    List<Stock_move> selectByRuleId(Collection<Long> ids);
    void removeByRuleId(Long id);
    List<Stock_move> selectByWarehouseId(Long id);
    void resetByWarehouseId(Long id);
    void resetByWarehouseId(Collection<Long> ids);
    void removeByWarehouseId(Long id);
    List<Stock_move> selectByProductUom(Long id);
    void resetByProductUom(Long id);
    void resetByProductUom(Collection<Long> ids);
    void removeByProductUom(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_move> getStockMoveByIds(List<Long> ids) ;
    List<Stock_move> getStockMoveByEntities(List<Stock_move> entities) ;
}


