package cn.ibizlab.businesscentral.core.odoo_calendar.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_contacts;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_contactsSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Calendar_contactsMapper extends BaseMapper<Calendar_contacts>{

    Page<Calendar_contacts> searchDefault(IPage page, @Param("srf") Calendar_contactsSearchContext context, @Param("ew") Wrapper<Calendar_contacts> wrapper) ;
    @Override
    Calendar_contacts selectById(Serializable id);
    @Override
    int insert(Calendar_contacts entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Calendar_contacts entity);
    @Override
    int update(@Param(Constants.ENTITY) Calendar_contacts entity, @Param("ew") Wrapper<Calendar_contacts> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Calendar_contacts> selectByPartnerId(@Param("id") Serializable id) ;

    List<Calendar_contacts> selectByCreateUid(@Param("id") Serializable id) ;

    List<Calendar_contacts> selectByUserId(@Param("id") Serializable id) ;

    List<Calendar_contacts> selectByWriteUid(@Param("id") Serializable id) ;


}
