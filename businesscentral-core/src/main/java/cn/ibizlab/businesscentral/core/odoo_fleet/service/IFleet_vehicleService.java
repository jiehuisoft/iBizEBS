package cn.ibizlab.businesscentral.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicleSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Fleet_vehicle] 服务对象接口
 */
public interface IFleet_vehicleService extends IService<Fleet_vehicle>{

    boolean create(Fleet_vehicle et) ;
    void createBatch(List<Fleet_vehicle> list) ;
    boolean update(Fleet_vehicle et) ;
    void updateBatch(List<Fleet_vehicle> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Fleet_vehicle get(Long key) ;
    Fleet_vehicle getDraft(Fleet_vehicle et) ;
    boolean checkKey(Fleet_vehicle et) ;
    boolean save(Fleet_vehicle et) ;
    void saveBatch(List<Fleet_vehicle> list) ;
    Page<Fleet_vehicle> searchDefault(Fleet_vehicleSearchContext context) ;
    List<Fleet_vehicle> selectByBrandId(Long id);
    void resetByBrandId(Long id);
    void resetByBrandId(Collection<Long> ids);
    void removeByBrandId(Long id);
    List<Fleet_vehicle> selectByModelId(Long id);
    void resetByModelId(Long id);
    void resetByModelId(Collection<Long> ids);
    void removeByModelId(Long id);
    List<Fleet_vehicle> selectByStateId(Long id);
    void resetByStateId(Long id);
    void resetByStateId(Collection<Long> ids);
    void removeByStateId(Long id);
    List<Fleet_vehicle> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Fleet_vehicle> selectByDriverId(Long id);
    void resetByDriverId(Long id);
    void resetByDriverId(Collection<Long> ids);
    void removeByDriverId(Long id);
    List<Fleet_vehicle> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Fleet_vehicle> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Fleet_vehicle> getFleetVehicleByIds(List<Long> ids) ;
    List<Fleet_vehicle> getFleetVehicleByEntities(List<Fleet_vehicle> entities) ;
}


