package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_automation_lead_test;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_automation_lead_testSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Base_automation_lead_test] 服务对象接口
 */
public interface IBase_automation_lead_testService extends IService<Base_automation_lead_test>{

    boolean create(Base_automation_lead_test et) ;
    void createBatch(List<Base_automation_lead_test> list) ;
    boolean update(Base_automation_lead_test et) ;
    void updateBatch(List<Base_automation_lead_test> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Base_automation_lead_test get(Long key) ;
    Base_automation_lead_test getDraft(Base_automation_lead_test et) ;
    boolean checkKey(Base_automation_lead_test et) ;
    boolean save(Base_automation_lead_test et) ;
    void saveBatch(List<Base_automation_lead_test> list) ;
    Page<Base_automation_lead_test> searchDefault(Base_automation_lead_testSearchContext context) ;
    List<Base_automation_lead_test> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Base_automation_lead_test> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Base_automation_lead_test> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Base_automation_lead_test> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Base_automation_lead_test> getBaseAutomationLeadTestByIds(List<Long> ids) ;
    List<Base_automation_lead_test> getBaseAutomationLeadTestByEntities(List<Base_automation_lead_test> entities) ;
}


