package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_list_contact_relSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_mass_mailing_list_contact_rel] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-mass-mailing-list-contact-rel", fallback = mail_mass_mailing_list_contact_relFallback.class)
public interface mail_mass_mailing_list_contact_relFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_list_contact_rels/search")
    Page<Mail_mass_mailing_list_contact_rel> search(@RequestBody Mail_mass_mailing_list_contact_relSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_list_contact_rels")
    Mail_mass_mailing_list_contact_rel create(@RequestBody Mail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_list_contact_rels/batch")
    Boolean createBatch(@RequestBody List<Mail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_list_contact_rels/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_list_contact_rels/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_list_contact_rels/{id}")
    Mail_mass_mailing_list_contact_rel update(@PathVariable("id") Long id,@RequestBody Mail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_list_contact_rels/batch")
    Boolean updateBatch(@RequestBody List<Mail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_list_contact_rels/{id}")
    Mail_mass_mailing_list_contact_rel get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_list_contact_rels/select")
    Page<Mail_mass_mailing_list_contact_rel> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_list_contact_rels/getdraft")
    Mail_mass_mailing_list_contact_rel getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_list_contact_rels/checkkey")
    Boolean checkKey(@RequestBody Mail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_list_contact_rels/save")
    Boolean save(@RequestBody Mail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_list_contact_rels/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_list_contact_rels/searchdefault")
    Page<Mail_mass_mailing_list_contact_rel> searchDefault(@RequestBody Mail_mass_mailing_list_contact_relSearchContext context);


}
