package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_statistics_report;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_statistics_reportSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_statistics_report] 服务对象接口
 */
public interface IMail_statistics_reportService extends IService<Mail_statistics_report>{

    boolean create(Mail_statistics_report et) ;
    void createBatch(List<Mail_statistics_report> list) ;
    boolean update(Mail_statistics_report et) ;
    void updateBatch(List<Mail_statistics_report> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_statistics_report get(Long key) ;
    Mail_statistics_report getDraft(Mail_statistics_report et) ;
    boolean checkKey(Mail_statistics_report et) ;
    boolean save(Mail_statistics_report et) ;
    void saveBatch(List<Mail_statistics_report> list) ;
    Page<Mail_statistics_report> searchDefault(Mail_statistics_reportSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


