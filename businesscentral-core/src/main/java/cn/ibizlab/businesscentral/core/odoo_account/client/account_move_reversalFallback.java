package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move_reversal;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_move_reversalSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_move_reversal] 服务对象接口
 */
@Component
public class account_move_reversalFallback implements account_move_reversalFeignClient{


    public Account_move_reversal update(Long id, Account_move_reversal account_move_reversal){
            return null;
     }
    public Boolean updateBatch(List<Account_move_reversal> account_move_reversals){
            return false;
     }



    public Account_move_reversal create(Account_move_reversal account_move_reversal){
            return null;
     }
    public Boolean createBatch(List<Account_move_reversal> account_move_reversals){
            return false;
     }


    public Account_move_reversal get(Long id){
            return null;
     }


    public Page<Account_move_reversal> search(Account_move_reversalSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Account_move_reversal> select(){
            return null;
     }

    public Account_move_reversal getDraft(){
            return null;
    }



    public Boolean checkKey(Account_move_reversal account_move_reversal){
            return false;
     }


    public Boolean save(Account_move_reversal account_move_reversal){
            return false;
     }
    public Boolean saveBatch(List<Account_move_reversal> account_move_reversals){
            return false;
     }

    public Page<Account_move_reversal> searchDefault(Account_move_reversalSearchContext context){
            return null;
     }


}
