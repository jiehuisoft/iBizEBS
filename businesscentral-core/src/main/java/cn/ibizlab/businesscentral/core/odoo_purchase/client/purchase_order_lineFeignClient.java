package cn.ibizlab.businesscentral.core.odoo_purchase.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_order_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[purchase_order_line] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-purchase:odoo-purchase}", contextId = "purchase-order-line", fallback = purchase_order_lineFallback.class)
public interface purchase_order_lineFeignClient {



    @RequestMapping(method = RequestMethod.DELETE, value = "/purchase_order_lines/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/purchase_order_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/purchase_order_lines/{id}")
    Purchase_order_line get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines")
    Purchase_order_line create(@RequestBody Purchase_order_line purchase_order_line);

    @RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/batch")
    Boolean createBatch(@RequestBody List<Purchase_order_line> purchase_order_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/search")
    Page<Purchase_order_line> search(@RequestBody Purchase_order_lineSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/purchase_order_lines/{id}")
    Purchase_order_line update(@PathVariable("id") Long id,@RequestBody Purchase_order_line purchase_order_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/purchase_order_lines/batch")
    Boolean updateBatch(@RequestBody List<Purchase_order_line> purchase_order_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/purchase_order_lines/select")
    Page<Purchase_order_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/purchase_order_lines/getdraft")
    Purchase_order_line getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/{id}/calc_amount")
    Purchase_order_line calc_amount(@PathVariable("id") Long id,@RequestBody Purchase_order_line purchase_order_line);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/{id}/calc_price")
    Purchase_order_line calc_price(@PathVariable("id") Long id,@RequestBody Purchase_order_line purchase_order_line);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/checkkey")
    Boolean checkKey(@RequestBody Purchase_order_line purchase_order_line);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/{id}/product_change")
    Purchase_order_line product_change(@PathVariable("id") Long id,@RequestBody Purchase_order_line purchase_order_line);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/save")
    Boolean save(@RequestBody Purchase_order_line purchase_order_line);

    @RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/savebatch")
    Boolean saveBatch(@RequestBody List<Purchase_order_line> purchase_order_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/searchcalc_order_amount")
    Page<Purchase_order_line> searchCalc_order_amount(@RequestBody Purchase_order_lineSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/searchdefault")
    Page<Purchase_order_line> searchDefault(@RequestBody Purchase_order_lineSearchContext context);


}
