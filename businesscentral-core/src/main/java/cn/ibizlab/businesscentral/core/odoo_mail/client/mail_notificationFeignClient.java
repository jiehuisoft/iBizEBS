package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_notification;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_notificationSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_notification] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-notification", fallback = mail_notificationFallback.class)
public interface mail_notificationFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/mail_notifications")
    Mail_notification create(@RequestBody Mail_notification mail_notification);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_notifications/batch")
    Boolean createBatch(@RequestBody List<Mail_notification> mail_notifications);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_notifications/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_notifications/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_notifications/search")
    Page<Mail_notification> search(@RequestBody Mail_notificationSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_notifications/{id}")
    Mail_notification get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_notifications/{id}")
    Mail_notification update(@PathVariable("id") Long id,@RequestBody Mail_notification mail_notification);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_notifications/batch")
    Boolean updateBatch(@RequestBody List<Mail_notification> mail_notifications);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_notifications/select")
    Page<Mail_notification> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_notifications/getdraft")
    Mail_notification getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_notifications/checkkey")
    Boolean checkKey(@RequestBody Mail_notification mail_notification);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_notifications/save")
    Boolean save(@RequestBody Mail_notification mail_notification);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_notifications/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_notification> mail_notifications);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_notifications/searchdefault")
    Page<Mail_notification> searchDefault(@RequestBody Mail_notificationSearchContext context);


}
