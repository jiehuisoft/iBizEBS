package cn.ibizlab.businesscentral.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_eventSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[event_event] 服务对象接口
 */
@Component
public class event_eventFallback implements event_eventFeignClient{

    public Page<Event_event> search(Event_eventSearchContext context){
            return null;
     }


    public Event_event create(Event_event event_event){
            return null;
     }
    public Boolean createBatch(List<Event_event> event_events){
            return false;
     }

    public Event_event get(Long id){
            return null;
     }




    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Event_event update(Long id, Event_event event_event){
            return null;
     }
    public Boolean updateBatch(List<Event_event> event_events){
            return false;
     }



    public Page<Event_event> select(){
            return null;
     }

    public Event_event getDraft(){
            return null;
    }



    public Boolean checkKey(Event_event event_event){
            return false;
     }


    public Boolean save(Event_event event_event){
            return false;
     }
    public Boolean saveBatch(List<Event_event> event_events){
            return false;
     }

    public Page<Event_event> searchDefault(Event_eventSearchContext context){
            return null;
     }


}
