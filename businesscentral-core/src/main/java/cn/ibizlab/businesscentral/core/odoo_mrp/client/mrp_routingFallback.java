package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_routingSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_routing] 服务对象接口
 */
@Component
public class mrp_routingFallback implements mrp_routingFeignClient{

    public Mrp_routing update(Long id, Mrp_routing mrp_routing){
            return null;
     }
    public Boolean updateBatch(List<Mrp_routing> mrp_routings){
            return false;
     }


    public Mrp_routing get(Long id){
            return null;
     }


    public Mrp_routing create(Mrp_routing mrp_routing){
            return null;
     }
    public Boolean createBatch(List<Mrp_routing> mrp_routings){
            return false;
     }


    public Page<Mrp_routing> search(Mrp_routingSearchContext context){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Mrp_routing> select(){
            return null;
     }

    public Mrp_routing getDraft(){
            return null;
    }



    public Boolean checkKey(Mrp_routing mrp_routing){
            return false;
     }


    public Boolean save(Mrp_routing mrp_routing){
            return false;
     }
    public Boolean saveBatch(List<Mrp_routing> mrp_routings){
            return false;
     }

    public Page<Mrp_routing> searchDefault(Mrp_routingSearchContext context){
            return null;
     }


}
