package cn.ibizlab.businesscentral.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_source;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_recruitment_sourceSearchContext;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_recruitment_sourceService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_hr.mapper.Hr_recruitment_sourceMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[应聘者来源] 服务对象接口实现
 */
@Slf4j
@Service("Hr_recruitment_sourceServiceImpl")
public class Hr_recruitment_sourceServiceImpl extends EBSServiceImpl<Hr_recruitment_sourceMapper, Hr_recruitment_source> implements IHr_recruitment_sourceService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_jobService hrJobService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_aliasService mailAliasService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_sourceService utmSourceService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "hr.recruitment.source" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Hr_recruitment_source et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_recruitment_sourceService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Hr_recruitment_source> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Hr_recruitment_source et) {
        Hr_recruitment_source old = new Hr_recruitment_source() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_recruitment_sourceService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_recruitment_sourceService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Hr_recruitment_source> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Hr_recruitment_source get(Long key) {
        Hr_recruitment_source et = getById(key);
        if(et==null){
            et=new Hr_recruitment_source();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Hr_recruitment_source getDraft(Hr_recruitment_source et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Hr_recruitment_source et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Hr_recruitment_source et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Hr_recruitment_source et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Hr_recruitment_source> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Hr_recruitment_source> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Hr_recruitment_source> selectByJobId(Long id) {
        return baseMapper.selectByJobId(id);
    }
    @Override
    public void resetByJobId(Long id) {
        this.update(new UpdateWrapper<Hr_recruitment_source>().set("job_id",null).eq("job_id",id));
    }

    @Override
    public void resetByJobId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_recruitment_source>().set("job_id",null).in("job_id",ids));
    }

    @Override
    public void removeByJobId(Long id) {
        this.remove(new QueryWrapper<Hr_recruitment_source>().eq("job_id",id));
    }

	@Override
    public List<Hr_recruitment_source> selectByAliasId(Long id) {
        return baseMapper.selectByAliasId(id);
    }
    @Override
    public void resetByAliasId(Long id) {
        this.update(new UpdateWrapper<Hr_recruitment_source>().set("alias_id",null).eq("alias_id",id));
    }

    @Override
    public void resetByAliasId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_recruitment_source>().set("alias_id",null).in("alias_id",ids));
    }

    @Override
    public void removeByAliasId(Long id) {
        this.remove(new QueryWrapper<Hr_recruitment_source>().eq("alias_id",id));
    }

	@Override
    public List<Hr_recruitment_source> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Hr_recruitment_source>().eq("create_uid",id));
    }

	@Override
    public List<Hr_recruitment_source> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Hr_recruitment_source>().eq("write_uid",id));
    }

	@Override
    public List<Hr_recruitment_source> selectBySourceId(Long id) {
        return baseMapper.selectBySourceId(id);
    }
    @Override
    public void removeBySourceId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Hr_recruitment_source>().in("source_id",ids));
    }

    @Override
    public void removeBySourceId(Long id) {
        this.remove(new QueryWrapper<Hr_recruitment_source>().eq("source_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Hr_recruitment_source> searchDefault(Hr_recruitment_sourceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Hr_recruitment_source> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Hr_recruitment_source>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Hr_recruitment_source et){
        //实体关系[DER1N_HR_RECRUITMENT_SOURCE__HR_JOB__JOB_ID]
        if(!ObjectUtils.isEmpty(et.getJobId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job odooJob=et.getOdooJob();
            if(ObjectUtils.isEmpty(odooJob)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job majorEntity=hrJobService.get(et.getJobId());
                et.setOdooJob(majorEntity);
                odooJob=majorEntity;
            }
            et.setJobIdText(odooJob.getName());
        }
        //实体关系[DER1N_HR_RECRUITMENT_SOURCE__MAIL_ALIAS__ALIAS_ID]
        if(!ObjectUtils.isEmpty(et.getAliasId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias odooAlias=et.getOdooAlias();
            if(ObjectUtils.isEmpty(odooAlias)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias majorEntity=mailAliasService.get(et.getAliasId());
                et.setOdooAlias(majorEntity);
                odooAlias=majorEntity;
            }
            et.setEmail(odooAlias.getDisplayName());
        }
        //实体关系[DER1N_HR_RECRUITMENT_SOURCE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_HR_RECRUITMENT_SOURCE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_HR_RECRUITMENT_SOURCE__UTM_SOURCE__SOURCE_ID]
        if(!ObjectUtils.isEmpty(et.getSourceId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource=et.getOdooSource();
            if(ObjectUtils.isEmpty(odooSource)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source majorEntity=utmSourceService.get(et.getSourceId());
                et.setOdooSource(majorEntity);
                odooSource=majorEntity;
            }
            et.setName(odooSource.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Hr_recruitment_source> getHrRecruitmentSourceByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Hr_recruitment_source> getHrRecruitmentSourceByEntities(List<Hr_recruitment_source> entities) {
        List ids =new ArrayList();
        for(Hr_recruitment_source entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



