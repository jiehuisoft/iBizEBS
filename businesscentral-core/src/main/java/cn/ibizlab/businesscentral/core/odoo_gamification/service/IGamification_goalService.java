package cn.ibizlab.businesscentral.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_goalSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Gamification_goal] 服务对象接口
 */
public interface IGamification_goalService extends IService<Gamification_goal>{

    boolean create(Gamification_goal et) ;
    void createBatch(List<Gamification_goal> list) ;
    boolean update(Gamification_goal et) ;
    void updateBatch(List<Gamification_goal> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Gamification_goal get(Long key) ;
    Gamification_goal getDraft(Gamification_goal et) ;
    boolean checkKey(Gamification_goal et) ;
    boolean save(Gamification_goal et) ;
    void saveBatch(List<Gamification_goal> list) ;
    Page<Gamification_goal> searchDefault(Gamification_goalSearchContext context) ;
    List<Gamification_goal> selectByLineId(Long id);
    void removeByLineId(Collection<Long> ids);
    void removeByLineId(Long id);
    List<Gamification_goal> selectByChallengeId(Long id);
    void resetByChallengeId(Long id);
    void resetByChallengeId(Collection<Long> ids);
    void removeByChallengeId(Long id);
    List<Gamification_goal> selectByDefinitionId(Long id);
    void removeByDefinitionId(Collection<Long> ids);
    void removeByDefinitionId(Long id);
    List<Gamification_goal> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Gamification_goal> selectByUserId(Long id);
    void removeByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Gamification_goal> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Gamification_goal> getGamificationGoalByIds(List<Long> ids) ;
    List<Gamification_goal> getGamificationGoalByEntities(List<Gamification_goal> entities) ;
}


