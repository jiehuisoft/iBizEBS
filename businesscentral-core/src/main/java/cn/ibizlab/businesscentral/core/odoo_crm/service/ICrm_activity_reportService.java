package cn.ibizlab.businesscentral.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_activity_reportSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Crm_activity_report] 服务对象接口
 */
public interface ICrm_activity_reportService extends IService<Crm_activity_report>{

    boolean create(Crm_activity_report et) ;
    void createBatch(List<Crm_activity_report> list) ;
    boolean update(Crm_activity_report et) ;
    void updateBatch(List<Crm_activity_report> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Crm_activity_report get(Long key) ;
    Crm_activity_report getDraft(Crm_activity_report et) ;
    boolean checkKey(Crm_activity_report et) ;
    boolean save(Crm_activity_report et) ;
    void saveBatch(List<Crm_activity_report> list) ;
    Page<Crm_activity_report> searchDefault(Crm_activity_reportSearchContext context) ;
    List<Crm_activity_report> selectByLeadId(Long id);
    void removeByLeadId(Long id);
    List<Crm_activity_report> selectByStageId(Long id);
    void removeByStageId(Long id);
    List<Crm_activity_report> selectByTeamId(Long id);
    void removeByTeamId(Long id);
    List<Crm_activity_report> selectByMailActivityTypeId(Long id);
    void removeByMailActivityTypeId(Long id);
    List<Crm_activity_report> selectBySubtypeId(Long id);
    void removeBySubtypeId(Long id);
    List<Crm_activity_report> selectByCompanyId(Long id);
    void removeByCompanyId(Long id);
    List<Crm_activity_report> selectByCountryId(Long id);
    void removeByCountryId(Long id);
    List<Crm_activity_report> selectByAuthorId(Long id);
    void removeByAuthorId(Long id);
    List<Crm_activity_report> selectByPartnerId(Long id);
    void removeByPartnerId(Long id);
    List<Crm_activity_report> selectByUserId(Long id);
    void removeByUserId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


