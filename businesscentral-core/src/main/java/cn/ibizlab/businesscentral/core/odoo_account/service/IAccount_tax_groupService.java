package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_group;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_tax_groupSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_tax_group] 服务对象接口
 */
public interface IAccount_tax_groupService extends IService<Account_tax_group>{

    boolean create(Account_tax_group et) ;
    void createBatch(List<Account_tax_group> list) ;
    boolean update(Account_tax_group et) ;
    void updateBatch(List<Account_tax_group> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_tax_group get(Long key) ;
    Account_tax_group getDraft(Account_tax_group et) ;
    boolean checkKey(Account_tax_group et) ;
    boolean save(Account_tax_group et) ;
    void saveBatch(List<Account_tax_group> list) ;
    Page<Account_tax_group> searchDefault(Account_tax_groupSearchContext context) ;
    List<Account_tax_group> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_tax_group> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_tax_group> getAccountTaxGroupByIds(List<Long> ids) ;
    List<Account_tax_group> getAccountTaxGroupByEntities(List<Account_tax_group> entities) ;
}


