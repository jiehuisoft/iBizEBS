package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenter_productivity_lossSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_workcenter_productivity_loss] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mrp:odoo-mrp}", contextId = "mrp-workcenter-productivity-loss", fallback = mrp_workcenter_productivity_lossFallback.class)
public interface mrp_workcenter_productivity_lossFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses/search")
    Page<Mrp_workcenter_productivity_loss> search(@RequestBody Mrp_workcenter_productivity_lossSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivity_losses/{id}")
    Mrp_workcenter_productivity_loss update(@PathVariable("id") Long id,@RequestBody Mrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivity_losses/batch")
    Boolean updateBatch(@RequestBody List<Mrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses")
    Mrp_workcenter_productivity_loss create(@RequestBody Mrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses/batch")
    Boolean createBatch(@RequestBody List<Mrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivity_losses/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivity_losses/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);





    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivity_losses/{id}")
    Mrp_workcenter_productivity_loss get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivity_losses/select")
    Page<Mrp_workcenter_productivity_loss> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivity_losses/getdraft")
    Mrp_workcenter_productivity_loss getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses/checkkey")
    Boolean checkKey(@RequestBody Mrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses/save")
    Boolean save(@RequestBody Mrp_workcenter_productivity_loss mrp_workcenter_productivity_loss);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses/savebatch")
    Boolean saveBatch(@RequestBody List<Mrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses/searchdefault")
    Page<Mrp_workcenter_productivity_loss> searchDefault(@RequestBody Mrp_workcenter_productivity_lossSearchContext context);


}
