package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_removal;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_removalSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_removal] 服务对象接口
 */
public interface IProduct_removalService extends IService<Product_removal>{

    boolean create(Product_removal et) ;
    void createBatch(List<Product_removal> list) ;
    boolean update(Product_removal et) ;
    void updateBatch(List<Product_removal> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_removal get(Long key) ;
    Product_removal getDraft(Product_removal et) ;
    boolean checkKey(Product_removal et) ;
    boolean save(Product_removal et) ;
    void saveBatch(List<Product_removal> list) ;
    Page<Product_removal> searchDefault(Product_removalSearchContext context) ;
    List<Product_removal> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_removal> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_removal> getProductRemovalByIds(List<Long> ids) ;
    List<Product_removal> getProductRemovalByEntities(List<Product_removal> entities) ;
}


