package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_cashbox;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statement_cashboxSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_bank_statement_cashbox] 服务对象接口
 */
@Component
public class account_bank_statement_cashboxFallback implements account_bank_statement_cashboxFeignClient{

    public Account_bank_statement_cashbox update(Long id, Account_bank_statement_cashbox account_bank_statement_cashbox){
            return null;
     }
    public Boolean updateBatch(List<Account_bank_statement_cashbox> account_bank_statement_cashboxes){
            return false;
     }


    public Account_bank_statement_cashbox get(Long id){
            return null;
     }



    public Page<Account_bank_statement_cashbox> search(Account_bank_statement_cashboxSearchContext context){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Account_bank_statement_cashbox create(Account_bank_statement_cashbox account_bank_statement_cashbox){
            return null;
     }
    public Boolean createBatch(List<Account_bank_statement_cashbox> account_bank_statement_cashboxes){
            return false;
     }

    public Page<Account_bank_statement_cashbox> select(){
            return null;
     }

    public Account_bank_statement_cashbox getDraft(){
            return null;
    }



    public Boolean checkKey(Account_bank_statement_cashbox account_bank_statement_cashbox){
            return false;
     }


    public Boolean save(Account_bank_statement_cashbox account_bank_statement_cashbox){
            return false;
     }
    public Boolean saveBatch(List<Account_bank_statement_cashbox> account_bank_statement_cashboxes){
            return false;
     }

    public Page<Account_bank_statement_cashbox> searchDefault(Account_bank_statement_cashboxSearchContext context){
            return null;
     }


}
