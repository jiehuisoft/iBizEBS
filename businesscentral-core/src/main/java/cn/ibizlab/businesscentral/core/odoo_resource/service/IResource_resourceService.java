package cn.ibizlab.businesscentral.core.odoo_resource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_resource;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_resourceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Resource_resource] 服务对象接口
 */
public interface IResource_resourceService extends IService<Resource_resource>{

    boolean create(Resource_resource et) ;
    void createBatch(List<Resource_resource> list) ;
    boolean update(Resource_resource et) ;
    void updateBatch(List<Resource_resource> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Resource_resource get(Long key) ;
    Resource_resource getDraft(Resource_resource et) ;
    boolean checkKey(Resource_resource et) ;
    boolean save(Resource_resource et) ;
    void saveBatch(List<Resource_resource> list) ;
    Page<Resource_resource> searchDefault(Resource_resourceSearchContext context) ;
    List<Resource_resource> selectByCalendarId(Long id);
    void resetByCalendarId(Long id);
    void resetByCalendarId(Collection<Long> ids);
    void removeByCalendarId(Long id);
    List<Resource_resource> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Resource_resource> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Resource_resource> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Resource_resource> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Resource_resource> getResourceResourceByIds(List<Long> ids) ;
    List<Resource_resource> getResourceResourceByEntities(List<Resource_resource> entities) ;
}


