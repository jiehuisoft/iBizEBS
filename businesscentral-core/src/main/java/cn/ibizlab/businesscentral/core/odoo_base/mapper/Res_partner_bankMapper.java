package cn.ibizlab.businesscentral.core.odoo_base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_bank;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_bankSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Res_partner_bankMapper extends BaseMapper<Res_partner_bank>{

    Page<Res_partner_bank> searchDefault(IPage page, @Param("srf") Res_partner_bankSearchContext context, @Param("ew") Wrapper<Res_partner_bank> wrapper) ;
    @Override
    Res_partner_bank selectById(Serializable id);
    @Override
    int insert(Res_partner_bank entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Res_partner_bank entity);
    @Override
    int update(@Param(Constants.ENTITY) Res_partner_bank entity, @Param("ew") Wrapper<Res_partner_bank> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Res_partner_bank> selectByBankId(@Param("id") Serializable id) ;

    List<Res_partner_bank> selectByCompanyId(@Param("id") Serializable id) ;

    List<Res_partner_bank> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Res_partner_bank> selectByPartnerId(@Param("id") Serializable id) ;

    List<Res_partner_bank> selectByCreateUid(@Param("id") Serializable id) ;

    List<Res_partner_bank> selectByWriteUid(@Param("id") Serializable id) ;


}
