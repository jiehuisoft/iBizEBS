package cn.ibizlab.businesscentral.core.odoo_payment.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_acquirerSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[payment_acquirer] 服务对象接口
 */
@Component
public class payment_acquirerFallback implements payment_acquirerFeignClient{

    public Payment_acquirer update(Long id, Payment_acquirer payment_acquirer){
            return null;
     }
    public Boolean updateBatch(List<Payment_acquirer> payment_acquirers){
            return false;
     }


    public Payment_acquirer create(Payment_acquirer payment_acquirer){
            return null;
     }
    public Boolean createBatch(List<Payment_acquirer> payment_acquirers){
            return false;
     }

    public Page<Payment_acquirer> search(Payment_acquirerSearchContext context){
            return null;
     }



    public Payment_acquirer get(Long id){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Payment_acquirer> select(){
            return null;
     }

    public Payment_acquirer getDraft(){
            return null;
    }



    public Boolean checkKey(Payment_acquirer payment_acquirer){
            return false;
     }


    public Boolean save(Payment_acquirer payment_acquirer){
            return false;
     }
    public Boolean saveBatch(List<Payment_acquirer> payment_acquirers){
            return false;
     }

    public Page<Payment_acquirer> searchDefault(Payment_acquirerSearchContext context){
            return null;
     }


}
