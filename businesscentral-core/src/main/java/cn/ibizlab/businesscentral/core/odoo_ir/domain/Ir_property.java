package cn.ibizlab.businesscentral.core.odoo_ir.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[动态存储]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "IR_PROPERTY",resultMap = "Ir_propertyResultMap")
public class Ir_property extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * type
     */
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * value_float
     */
    @DEField(name = "value_float")
    @TableField(value = "value_float")
    @JSONField(name = "value_float")
    @JsonProperty("value_float")
    private Double valueFloat;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * res_id
     */
    @DEField(name = "res_id")
    @TableField(value = "res_id")
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private String resId;
    /**
     * value_text
     */
    @DEField(name = "value_text")
    @TableField(value = "value_text")
    @JSONField(name = "value_text")
    @JsonProperty("value_text")
    private String valueText;
    /**
     * value_datetime
     */
    @DEField(name = "value_datetime")
    @TableField(value = "value_datetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "value_datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("value_datetime")
    private Timestamp valueDatetime;
    /**
     * value_reference
     */
    @DEField(name = "value_reference")
    @TableField(value = "value_reference")
    @JSONField(name = "value_reference")
    @JsonProperty("value_reference")
    private String valueReference;
    /**
     * value_integer
     */
    @DEField(name = "value_integer")
    @TableField(value = "value_integer")
    @JSONField(name = "value_integer")
    @JsonProperty("value_integer")
    private Integer valueInteger;
    /**
     * ID
     */
    @DEField(name = "fields_id")
    @TableField(value = "fields_id")
    @JSONField(name = "fields_id")
    @JsonProperty("fields_id")
    private Long fieldsId;
    /**
     * ID
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_model_fields odooFields;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;



    /**
     * 设置 [type]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [value_float]
     */
    public void setValueFloat(Double valueFloat){
        this.valueFloat = valueFloat ;
        this.modify("value_float",valueFloat);
    }

    /**
     * 设置 [res_id]
     */
    public void setResId(String resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [value_text]
     */
    public void setValueText(String valueText){
        this.valueText = valueText ;
        this.modify("value_text",valueText);
    }

    /**
     * 设置 [value_datetime]
     */
    public void setValueDatetime(Timestamp valueDatetime){
        this.valueDatetime = valueDatetime ;
        this.modify("value_datetime",valueDatetime);
    }

    /**
     * 格式化日期 [value_datetime]
     */
    public String formatValueDatetime(){
        if (this.valueDatetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(valueDatetime);
    }
    /**
     * 设置 [value_reference]
     */
    public void setValueReference(String valueReference){
        this.valueReference = valueReference ;
        this.modify("value_reference",valueReference);
    }

    /**
     * 设置 [value_integer]
     */
    public void setValueInteger(Integer valueInteger){
        this.valueInteger = valueInteger ;
        this.modify("value_integer",valueInteger);
    }

    /**
     * 设置 [ID]
     */
    public void setFieldsId(Long fieldsId){
        this.fieldsId = fieldsId ;
        this.modify("fields_id",fieldsId);
    }

    /**
     * 设置 [ID]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


