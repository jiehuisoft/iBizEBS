package cn.ibizlab.businesscentral.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_mail_compose_message;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_mail_compose_messageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[survey_mail_compose_message] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-survey:odoo-survey}", contextId = "survey-mail-compose-message", fallback = survey_mail_compose_messageFallback.class)
public interface survey_mail_compose_messageFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_mail_compose_messages/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/survey_mail_compose_messages/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/survey_mail_compose_messages/{id}")
    Survey_mail_compose_message update(@PathVariable("id") Long id,@RequestBody Survey_mail_compose_message survey_mail_compose_message);

    @RequestMapping(method = RequestMethod.PUT, value = "/survey_mail_compose_messages/batch")
    Boolean updateBatch(@RequestBody List<Survey_mail_compose_message> survey_mail_compose_messages);


    @RequestMapping(method = RequestMethod.GET, value = "/survey_mail_compose_messages/{id}")
    Survey_mail_compose_message get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages")
    Survey_mail_compose_message create(@RequestBody Survey_mail_compose_message survey_mail_compose_message);

    @RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages/batch")
    Boolean createBatch(@RequestBody List<Survey_mail_compose_message> survey_mail_compose_messages);



    @RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages/search")
    Page<Survey_mail_compose_message> search(@RequestBody Survey_mail_compose_messageSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/survey_mail_compose_messages/select")
    Page<Survey_mail_compose_message> select();


    @RequestMapping(method = RequestMethod.GET, value = "/survey_mail_compose_messages/getdraft")
    Survey_mail_compose_message getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages/checkkey")
    Boolean checkKey(@RequestBody Survey_mail_compose_message survey_mail_compose_message);


    @RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages/save")
    Boolean save(@RequestBody Survey_mail_compose_message survey_mail_compose_message);

    @RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages/savebatch")
    Boolean saveBatch(@RequestBody List<Survey_mail_compose_message> survey_mail_compose_messages);



    @RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages/searchdefault")
    Page<Survey_mail_compose_message> searchDefault(@RequestBody Survey_mail_compose_messageSearchContext context);


}
