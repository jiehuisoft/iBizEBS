package cn.ibizlab.businesscentral.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_mail;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_mailSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[event_mail] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-event:odoo-event}", contextId = "event-mail", fallback = event_mailFallback.class)
public interface event_mailFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/event_mails/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/event_mails/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/event_mails")
    Event_mail create(@RequestBody Event_mail event_mail);

    @RequestMapping(method = RequestMethod.POST, value = "/event_mails/batch")
    Boolean createBatch(@RequestBody List<Event_mail> event_mails);





    @RequestMapping(method = RequestMethod.POST, value = "/event_mails/search")
    Page<Event_mail> search(@RequestBody Event_mailSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/event_mails/{id}")
    Event_mail get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/event_mails/{id}")
    Event_mail update(@PathVariable("id") Long id,@RequestBody Event_mail event_mail);

    @RequestMapping(method = RequestMethod.PUT, value = "/event_mails/batch")
    Boolean updateBatch(@RequestBody List<Event_mail> event_mails);


    @RequestMapping(method = RequestMethod.GET, value = "/event_mails/select")
    Page<Event_mail> select();


    @RequestMapping(method = RequestMethod.GET, value = "/event_mails/getdraft")
    Event_mail getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/event_mails/checkkey")
    Boolean checkKey(@RequestBody Event_mail event_mail);


    @RequestMapping(method = RequestMethod.POST, value = "/event_mails/save")
    Boolean save(@RequestBody Event_mail event_mail);

    @RequestMapping(method = RequestMethod.POST, value = "/event_mails/savebatch")
    Boolean saveBatch(@RequestBody List<Event_mail> event_mails);



    @RequestMapping(method = RequestMethod.POST, value = "/event_mails/searchdefault")
    Page<Event_mail> searchDefault(@RequestBody Event_mailSearchContext context);


}
