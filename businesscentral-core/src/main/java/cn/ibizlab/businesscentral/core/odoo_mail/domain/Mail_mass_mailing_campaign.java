package cn.ibizlab.businesscentral.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[群发邮件营销]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAIL_MASS_MAILING_CAMPAIGN",resultMap = "Mail_mass_mailing_campaignResultMap")
public class Mail_mass_mailing_campaign extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 总计
     */
    @TableField(exist = false)
    @JSONField(name = "total")
    @JsonProperty("total")
    private Integer total;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 颜色索引
     */
    @TableField(value = "color")
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;
    /**
     * 群发邮件
     */
    @TableField(exist = false)
    @JSONField(name = "mass_mailing_ids")
    @JsonProperty("mass_mailing_ids")
    private String massMailingIds;
    /**
     * 打开比例
     */
    @TableField(exist = false)
    @JSONField(name = "opened_ratio")
    @JsonProperty("opened_ratio")
    private Integer openedRatio;
    /**
     * 失败的
     */
    @TableField(exist = false)
    @JSONField(name = "failed")
    @JsonProperty("failed")
    private Integer failed;
    /**
     * 安排
     */
    @TableField(exist = false)
    @JSONField(name = "scheduled")
    @JsonProperty("scheduled")
    private Integer scheduled;
    /**
     * 被退回
     */
    @TableField(exist = false)
    @JSONField(name = "bounced")
    @JsonProperty("bounced")
    private Integer bounced;
    /**
     * 点击数
     */
    @TableField(exist = false)
    @JSONField(name = "clicks_ratio")
    @JsonProperty("clicks_ratio")
    private Integer clicksRatio;
    /**
     * 发送邮件
     */
    @TableField(exist = false)
    @JSONField(name = "sent")
    @JsonProperty("sent")
    private Integer sent;
    /**
     * 已接收比例
     */
    @TableField(exist = false)
    @JSONField(name = "received_ratio")
    @JsonProperty("received_ratio")
    private Integer receivedRatio;
    /**
     * 忽略
     */
    @TableField(exist = false)
    @JSONField(name = "ignored")
    @JsonProperty("ignored")
    private Integer ignored;
    /**
     * 支持 A/B 测试
     */
    @DEField(name = "unique_ab_testing")
    @TableField(value = "unique_ab_testing")
    @JSONField(name = "unique_ab_testing")
    @JsonProperty("unique_ab_testing")
    private Boolean uniqueAbTesting;
    /**
     * 已回复
     */
    @TableField(exist = false)
    @JSONField(name = "replied")
    @JsonProperty("replied")
    private Integer replied;
    /**
     * 邮件
     */
    @TableField(exist = false)
    @JSONField(name = "total_mailings")
    @JsonProperty("total_mailings")
    private Integer totalMailings;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 被退回的比率
     */
    @TableField(exist = false)
    @JSONField(name = "bounced_ratio")
    @JsonProperty("bounced_ratio")
    private Integer bouncedRatio;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 标签
     */
    @TableField(exist = false)
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    private String tagIds;
    /**
     * 已开启
     */
    @TableField(exist = false)
    @JSONField(name = "opened")
    @JsonProperty("opened")
    private Integer opened;
    /**
     * 回复比例
     */
    @TableField(exist = false)
    @JSONField(name = "replied_ratio")
    @JsonProperty("replied_ratio")
    private Integer repliedRatio;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 已送货
     */
    @TableField(exist = false)
    @JSONField(name = "delivered")
    @JsonProperty("delivered")
    private Integer delivered;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 媒体
     */
    @TableField(exist = false)
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;
    /**
     * 阶段
     */
    @TableField(exist = false)
    @JSONField(name = "stage_id_text")
    @JsonProperty("stage_id_text")
    private String stageIdText;
    /**
     * 负责人
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 来源
     */
    @TableField(exist = false)
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    private String sourceIdText;
    /**
     * 营销名称
     */
    @TableField(exist = false)
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 负责人
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 运动_ ID
     */
    @DEField(name = "campaign_id")
    @TableField(value = "campaign_id")
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Long campaignId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 阶段
     */
    @DEField(name = "stage_id")
    @TableField(value = "stage_id")
    @JSONField(name = "stage_id")
    @JsonProperty("stage_id")
    private Long stageId;
    /**
     * 来源
     */
    @DEField(name = "source_id")
    @TableField(value = "source_id")
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Long sourceId;
    /**
     * 媒体
     */
    @DEField(name = "medium_id")
    @TableField(value = "medium_id")
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Long mediumId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_stage odooStage;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign odooCampaign;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium odooMedium;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource;



    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [支持 A/B 测试]
     */
    public void setUniqueAbTesting(Boolean uniqueAbTesting){
        this.uniqueAbTesting = uniqueAbTesting ;
        this.modify("unique_ab_testing",uniqueAbTesting);
    }

    /**
     * 设置 [负责人]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [运动_ ID]
     */
    public void setCampaignId(Long campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }

    /**
     * 设置 [阶段]
     */
    public void setStageId(Long stageId){
        this.stageId = stageId ;
        this.modify("stage_id",stageId);
    }

    /**
     * 设置 [来源]
     */
    public void setSourceId(Long sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }

    /**
     * 设置 [媒体]
     */
    public void setMediumId(Long mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


