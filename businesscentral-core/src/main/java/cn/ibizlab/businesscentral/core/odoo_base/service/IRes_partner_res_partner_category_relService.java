package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_res_partner_category_rel;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_res_partner_category_relSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_partner_res_partner_category_rel] 服务对象接口
 */
public interface IRes_partner_res_partner_category_relService extends IService<Res_partner_res_partner_category_rel>{

    boolean create(Res_partner_res_partner_category_rel et) ;
    void createBatch(List<Res_partner_res_partner_category_rel> list) ;
    boolean update(Res_partner_res_partner_category_rel et) ;
    void updateBatch(List<Res_partner_res_partner_category_rel> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Res_partner_res_partner_category_rel get(String key) ;
    Res_partner_res_partner_category_rel getDraft(Res_partner_res_partner_category_rel et) ;
    boolean checkKey(Res_partner_res_partner_category_rel et) ;
    boolean save(Res_partner_res_partner_category_rel et) ;
    void saveBatch(List<Res_partner_res_partner_category_rel> list) ;
    Page<Res_partner_res_partner_category_rel> searchDefault(Res_partner_res_partner_category_relSearchContext context) ;
    List<Res_partner_res_partner_category_rel> selectByCategoryId(Long id);
    void removeByCategoryId(Long id);
    List<Res_partner_res_partner_category_rel> selectByPartnerId(Long id);
    void removeByPartnerId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


