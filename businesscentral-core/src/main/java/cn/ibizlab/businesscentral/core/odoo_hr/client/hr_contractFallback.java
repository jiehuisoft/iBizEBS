package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_contract;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_contractSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_contract] 服务对象接口
 */
@Component
public class hr_contractFallback implements hr_contractFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Hr_contract get(Long id){
            return null;
     }



    public Page<Hr_contract> search(Hr_contractSearchContext context){
            return null;
     }



    public Hr_contract create(Hr_contract hr_contract){
            return null;
     }
    public Boolean createBatch(List<Hr_contract> hr_contracts){
            return false;
     }

    public Hr_contract update(Long id, Hr_contract hr_contract){
            return null;
     }
    public Boolean updateBatch(List<Hr_contract> hr_contracts){
            return false;
     }



    public Page<Hr_contract> select(){
            return null;
     }

    public Hr_contract getDraft(){
            return null;
    }



    public Boolean checkKey(Hr_contract hr_contract){
            return false;
     }


    public Boolean save(Hr_contract hr_contract){
            return false;
     }
    public Boolean saveBatch(List<Hr_contract> hr_contracts){
            return false;
     }

    public Page<Hr_contract> searchDefault(Hr_contractSearchContext context){
            return null;
     }


}
