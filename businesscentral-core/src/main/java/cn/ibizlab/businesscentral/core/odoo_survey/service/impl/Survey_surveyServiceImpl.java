package cn.ibizlab.businesscentral.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_survey;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_surveySearchContext;
import cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_surveyService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_survey.mapper.Survey_surveyMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[问卷] 服务对象接口实现
 */
@Slf4j
@Service("Survey_surveyServiceImpl")
public class Survey_surveyServiceImpl extends EBSServiceImpl<Survey_surveyMapper, Survey_survey> implements ISurvey_surveyService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_jobService hrJobService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_mail_compose_messageService surveyMailComposeMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_pageService surveyPageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_user_input_lineService surveyUserInputLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_user_inputService surveyUserInputService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_templateService mailTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_stageService surveyStageService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "survey.survey" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Survey_survey et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISurvey_surveyService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Survey_survey> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Survey_survey et) {
        Survey_survey old = new Survey_survey() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISurvey_surveyService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISurvey_surveyService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Survey_survey> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        surveyMailComposeMessageService.resetBySurveyId(key);
        surveyPageService.removeBySurveyId(key);
        surveyUserInputLineService.resetBySurveyId(key);
        if(!ObjectUtils.isEmpty(surveyUserInputService.selectBySurveyId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Survey_user_input]数据，无法删除!","","");
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        surveyMailComposeMessageService.resetBySurveyId(idList);
        surveyPageService.removeBySurveyId(idList);
        surveyUserInputLineService.resetBySurveyId(idList);
        if(!ObjectUtils.isEmpty(surveyUserInputService.selectBySurveyId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Survey_user_input]数据，无法删除!","","");
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Survey_survey get(Long key) {
        Survey_survey et = getById(key);
        if(et==null){
            et=new Survey_survey();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Survey_survey getDraft(Survey_survey et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Survey_survey et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Survey_survey et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Survey_survey et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Survey_survey> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Survey_survey> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Survey_survey> selectByEmailTemplateId(Long id) {
        return baseMapper.selectByEmailTemplateId(id);
    }
    @Override
    public void resetByEmailTemplateId(Long id) {
        this.update(new UpdateWrapper<Survey_survey>().set("email_template_id",null).eq("email_template_id",id));
    }

    @Override
    public void resetByEmailTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Survey_survey>().set("email_template_id",null).in("email_template_id",ids));
    }

    @Override
    public void removeByEmailTemplateId(Long id) {
        this.remove(new QueryWrapper<Survey_survey>().eq("email_template_id",id));
    }

	@Override
    public List<Survey_survey> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Survey_survey>().eq("create_uid",id));
    }

	@Override
    public List<Survey_survey> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Survey_survey>().eq("write_uid",id));
    }

	@Override
    public List<Survey_survey> selectByStageId(Long id) {
        return baseMapper.selectByStageId(id);
    }
    @Override
    public List<Survey_survey> selectByStageId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Survey_survey>().in("id",ids));
    }

    @Override
    public void removeByStageId(Long id) {
        this.remove(new QueryWrapper<Survey_survey>().eq("stage_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Survey_survey> searchDefault(Survey_surveySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Survey_survey> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Survey_survey>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Survey_survey et){
        //实体关系[DER1N_SURVEY_SURVEY__MAIL_TEMPLATE__EMAIL_TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getEmailTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooEmailTemplate=et.getOdooEmailTemplate();
            if(ObjectUtils.isEmpty(odooEmailTemplate)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template majorEntity=mailTemplateService.get(et.getEmailTemplateId());
                et.setOdooEmailTemplate(majorEntity);
                odooEmailTemplate=majorEntity;
            }
            et.setEmailTemplateIdText(odooEmailTemplate.getName());
        }
        //实体关系[DER1N_SURVEY_SURVEY__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_SURVEY_SURVEY__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_SURVEY_SURVEY__SURVEY_STAGE__STAGE_ID]
        if(!ObjectUtils.isEmpty(et.getStageId())){
            cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_stage odooStage=et.getOdooStage();
            if(ObjectUtils.isEmpty(odooStage)){
                cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_stage majorEntity=surveyStageService.get(et.getStageId());
                et.setOdooStage(majorEntity);
                odooStage=majorEntity;
            }
            et.setIsClosed(odooStage.getClosed());
            et.setStageIdText(odooStage.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Survey_survey> getSurveySurveyByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Survey_survey> getSurveySurveyByEntities(List<Survey_survey> entities) {
        List ids =new ArrayList();
        for(Survey_survey entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



