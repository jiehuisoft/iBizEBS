package cn.ibizlab.businesscentral.core.odoo_payment.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_token;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_tokenSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Payment_token] 服务对象接口
 */
public interface IPayment_tokenService extends IService<Payment_token>{

    boolean create(Payment_token et) ;
    void createBatch(List<Payment_token> list) ;
    boolean update(Payment_token et) ;
    void updateBatch(List<Payment_token> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Payment_token get(Long key) ;
    Payment_token getDraft(Payment_token et) ;
    boolean checkKey(Payment_token et) ;
    boolean save(Payment_token et) ;
    void saveBatch(List<Payment_token> list) ;
    Page<Payment_token> searchDefault(Payment_tokenSearchContext context) ;
    List<Payment_token> selectByAcquirerId(Long id);
    void resetByAcquirerId(Long id);
    void resetByAcquirerId(Collection<Long> ids);
    void removeByAcquirerId(Long id);
    List<Payment_token> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Payment_token> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Payment_token> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Payment_token> getPaymentTokenByIds(List<Long> ids) ;
    List<Payment_token> getPaymentTokenByEntities(List<Payment_token> entities) ;
}


