package cn.ibizlab.businesscentral.core.odoo_purchase.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_orderSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Purchase_orderMapper extends BaseMapper<Purchase_order>{

    Page<Purchase_order> searchDefault(IPage page, @Param("srf") Purchase_orderSearchContext context, @Param("ew") Wrapper<Purchase_order> wrapper) ;
    Page<Purchase_order> searchMaster(IPage page, @Param("srf") Purchase_orderSearchContext context, @Param("ew") Wrapper<Purchase_order> wrapper) ;
    Page<Purchase_order> searchOrder(IPage page, @Param("srf") Purchase_orderSearchContext context, @Param("ew") Wrapper<Purchase_order> wrapper) ;
    @Override
    Purchase_order selectById(Serializable id);
    @Override
    int insert(Purchase_order entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Purchase_order entity);
    @Override
    int update(@Param(Constants.ENTITY) Purchase_order entity, @Param("ew") Wrapper<Purchase_order> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Purchase_order> selectByRequisitionId(@Param("id") Serializable id) ;

    List<Purchase_order> selectByPartnerId(@Param("id") Serializable id) ;

    List<Purchase_order> selectByFiscalPositionId(@Param("id") Serializable id) ;

    List<Purchase_order> selectByIncotermId(@Param("id") Serializable id) ;

    List<Purchase_order> selectByPaymentTermId(@Param("id") Serializable id) ;

    List<Purchase_order> selectByCompanyId(@Param("id") Serializable id) ;

    List<Purchase_order> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Purchase_order> selectByDestAddressId(@Param("id") Serializable id) ;

    List<Purchase_order> selectByCreateUid(@Param("id") Serializable id) ;

    List<Purchase_order> selectByUserId(@Param("id") Serializable id) ;

    List<Purchase_order> selectByWriteUid(@Param("id") Serializable id) ;

    List<Purchase_order> selectByPickingTypeId(@Param("id") Serializable id) ;


}
