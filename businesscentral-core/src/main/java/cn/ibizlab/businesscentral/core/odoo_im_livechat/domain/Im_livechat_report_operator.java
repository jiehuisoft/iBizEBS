package cn.ibizlab.businesscentral.core.odoo_im_livechat.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[实时聊天支持操作员报告]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "IM_LIVECHAT_REPORT_OPERATOR",resultMap = "Im_livechat_report_operatorResultMap")
public class Im_livechat_report_operator extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 该回答了
     */
    @DEField(name = "time_to_answer")
    @TableField(value = "time_to_answer")
    @JSONField(name = "time_to_answer")
    @JsonProperty("time_to_answer")
    private Double timeToAnswer;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 会话的开始日期
     */
    @DEField(name = "start_date")
    @TableField(value = "start_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("start_date")
    private Timestamp startDate;
    /**
     * # 会话
     */
    @DEField(name = "nbr_channel")
    @TableField(value = "nbr_channel")
    @JSONField(name = "nbr_channel")
    @JsonProperty("nbr_channel")
    private Integer nbrChannel;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 平均时间
     */
    @TableField(value = "duration")
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Double duration;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 渠道
     */
    @TableField(exist = false)
    @JSONField(name = "livechat_channel_id_text")
    @JsonProperty("livechat_channel_id_text")
    private String livechatChannelIdText;
    /**
     * 对话
     */
    @TableField(exist = false)
    @JSONField(name = "channel_id_text")
    @JsonProperty("channel_id_text")
    private String channelIdText;
    /**
     * 运算符
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 对话
     */
    @DEField(name = "channel_id")
    @TableField(value = "channel_id")
    @JSONField(name = "channel_id")
    @JsonProperty("channel_id")
    private Long channelId;
    /**
     * 运算符
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 渠道
     */
    @DEField(name = "livechat_channel_id")
    @TableField(value = "livechat_channel_id")
    @JSONField(name = "livechat_channel_id")
    @JsonProperty("livechat_channel_id")
    private Long livechatChannelId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel odooLivechatChannel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel odooChannel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;



    /**
     * 设置 [该回答了]
     */
    public void setTimeToAnswer(Double timeToAnswer){
        this.timeToAnswer = timeToAnswer ;
        this.modify("time_to_answer",timeToAnswer);
    }

    /**
     * 设置 [会话的开始日期]
     */
    public void setStartDate(Timestamp startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 格式化日期 [会话的开始日期]
     */
    public String formatStartDate(){
        if (this.startDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(startDate);
    }
    /**
     * 设置 [# 会话]
     */
    public void setNbrChannel(Integer nbrChannel){
        this.nbrChannel = nbrChannel ;
        this.modify("nbr_channel",nbrChannel);
    }

    /**
     * 设置 [平均时间]
     */
    public void setDuration(Double duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }

    /**
     * 设置 [对话]
     */
    public void setChannelId(Long channelId){
        this.channelId = channelId ;
        this.modify("channel_id",channelId);
    }

    /**
     * 设置 [运算符]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [渠道]
     */
    public void setLivechatChannelId(Long livechatChannelId){
        this.livechatChannelId = livechatChannelId ;
        this.modify("livechat_channel_id",livechatChannelId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


