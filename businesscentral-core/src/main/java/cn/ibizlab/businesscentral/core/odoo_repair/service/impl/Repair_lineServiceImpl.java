package cn.ibizlab.businesscentral.core.odoo_repair.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_line;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_lineSearchContext;
import cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_lineService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_repair.mapper.Repair_lineMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[修理明细行(零件)] 服务对象接口实现
 */
@Slf4j
@Service("Repair_lineServiceImpl")
public class Repair_lineServiceImpl extends EBSServiceImpl<Repair_lineMapper, Repair_line> implements IRepair_lineService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_lineService accountInvoiceLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_orderService repairOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_production_lotService stockProductionLotService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "repair.line" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Repair_line et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRepair_lineService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Repair_line> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Repair_line et) {
        Repair_line old = new Repair_line() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRepair_lineService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRepair_lineService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Repair_line> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Repair_line get(Long key) {
        Repair_line et = getById(key);
        if(et==null){
            et=new Repair_line();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Repair_line getDraft(Repair_line et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Repair_line et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Repair_line et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Repair_line et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Repair_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Repair_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Repair_line> selectByInvoiceLineId(Long id) {
        return baseMapper.selectByInvoiceLineId(id);
    }
    @Override
    public void resetByInvoiceLineId(Long id) {
        this.update(new UpdateWrapper<Repair_line>().set("invoice_line_id",null).eq("invoice_line_id",id));
    }

    @Override
    public void resetByInvoiceLineId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_line>().set("invoice_line_id",null).in("invoice_line_id",ids));
    }

    @Override
    public void removeByInvoiceLineId(Long id) {
        this.remove(new QueryWrapper<Repair_line>().eq("invoice_line_id",id));
    }

	@Override
    public List<Repair_line> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Repair_line>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_line>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Repair_line>().eq("product_id",id));
    }

	@Override
    public List<Repair_line> selectByRepairId(Long id) {
        return baseMapper.selectByRepairId(id);
    }
    @Override
    public void removeByRepairId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Repair_line>().in("repair_id",ids));
    }

    @Override
    public void removeByRepairId(Long id) {
        this.remove(new QueryWrapper<Repair_line>().eq("repair_id",id));
    }

	@Override
    public List<Repair_line> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Repair_line>().eq("create_uid",id));
    }

	@Override
    public List<Repair_line> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Repair_line>().eq("write_uid",id));
    }

	@Override
    public List<Repair_line> selectByLocationDestId(Long id) {
        return baseMapper.selectByLocationDestId(id);
    }
    @Override
    public void resetByLocationDestId(Long id) {
        this.update(new UpdateWrapper<Repair_line>().set("location_dest_id",null).eq("location_dest_id",id));
    }

    @Override
    public void resetByLocationDestId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_line>().set("location_dest_id",null).in("location_dest_id",ids));
    }

    @Override
    public void removeByLocationDestId(Long id) {
        this.remove(new QueryWrapper<Repair_line>().eq("location_dest_id",id));
    }

	@Override
    public List<Repair_line> selectByLocationId(Long id) {
        return baseMapper.selectByLocationId(id);
    }
    @Override
    public void resetByLocationId(Long id) {
        this.update(new UpdateWrapper<Repair_line>().set("location_id",null).eq("location_id",id));
    }

    @Override
    public void resetByLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_line>().set("location_id",null).in("location_id",ids));
    }

    @Override
    public void removeByLocationId(Long id) {
        this.remove(new QueryWrapper<Repair_line>().eq("location_id",id));
    }

	@Override
    public List<Repair_line> selectByMoveId(Long id) {
        return baseMapper.selectByMoveId(id);
    }
    @Override
    public void resetByMoveId(Long id) {
        this.update(new UpdateWrapper<Repair_line>().set("move_id",null).eq("move_id",id));
    }

    @Override
    public void resetByMoveId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_line>().set("move_id",null).in("move_id",ids));
    }

    @Override
    public void removeByMoveId(Long id) {
        this.remove(new QueryWrapper<Repair_line>().eq("move_id",id));
    }

	@Override
    public List<Repair_line> selectByLotId(Long id) {
        return baseMapper.selectByLotId(id);
    }
    @Override
    public void resetByLotId(Long id) {
        this.update(new UpdateWrapper<Repair_line>().set("lot_id",null).eq("lot_id",id));
    }

    @Override
    public void resetByLotId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_line>().set("lot_id",null).in("lot_id",ids));
    }

    @Override
    public void removeByLotId(Long id) {
        this.remove(new QueryWrapper<Repair_line>().eq("lot_id",id));
    }

	@Override
    public List<Repair_line> selectByProductUom(Long id) {
        return baseMapper.selectByProductUom(id);
    }
    @Override
    public void resetByProductUom(Long id) {
        this.update(new UpdateWrapper<Repair_line>().set("product_uom",null).eq("product_uom",id));
    }

    @Override
    public void resetByProductUom(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_line>().set("product_uom",null).in("product_uom",ids));
    }

    @Override
    public void removeByProductUom(Long id) {
        this.remove(new QueryWrapper<Repair_line>().eq("product_uom",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Repair_line> searchDefault(Repair_lineSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Repair_line> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Repair_line>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Repair_line et){
        //实体关系[DER1N_REPAIR_LINE__ACCOUNT_INVOICE_LINE__INVOICE_LINE_ID]
        if(!ObjectUtils.isEmpty(et.getInvoiceLineId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_line odooInvoiceLine=et.getOdooInvoiceLine();
            if(ObjectUtils.isEmpty(odooInvoiceLine)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_line majorEntity=accountInvoiceLineService.get(et.getInvoiceLineId());
                et.setOdooInvoiceLine(majorEntity);
                odooInvoiceLine=majorEntity;
            }
            et.setInvoiceLineIdText(odooInvoiceLine.getName());
        }
        //实体关系[DER1N_REPAIR_LINE__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_REPAIR_LINE__REPAIR_ORDER__REPAIR_ID]
        if(!ObjectUtils.isEmpty(et.getRepairId())){
            cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order odooRepair=et.getOdooRepair();
            if(ObjectUtils.isEmpty(odooRepair)){
                cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order majorEntity=repairOrderService.get(et.getRepairId());
                et.setOdooRepair(majorEntity);
                odooRepair=majorEntity;
            }
            et.setRepairIdText(odooRepair.getName());
        }
        //实体关系[DER1N_REPAIR_LINE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_REPAIR_LINE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_REPAIR_LINE__STOCK_LOCATION__LOCATION_DEST_ID]
        if(!ObjectUtils.isEmpty(et.getLocationDestId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocationDest=et.getOdooLocationDest();
            if(ObjectUtils.isEmpty(odooLocationDest)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationDestId());
                et.setOdooLocationDest(majorEntity);
                odooLocationDest=majorEntity;
            }
            et.setLocationDestIdText(odooLocationDest.getName());
        }
        //实体关系[DER1N_REPAIR_LINE__STOCK_LOCATION__LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation=et.getOdooLocation();
            if(ObjectUtils.isEmpty(odooLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationId());
                et.setOdooLocation(majorEntity);
                odooLocation=majorEntity;
            }
            et.setLocationIdText(odooLocation.getName());
        }
        //实体关系[DER1N_REPAIR_LINE__STOCK_MOVE__MOVE_ID]
        if(!ObjectUtils.isEmpty(et.getMoveId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move odooMove=et.getOdooMove();
            if(ObjectUtils.isEmpty(odooMove)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move majorEntity=stockMoveService.get(et.getMoveId());
                et.setOdooMove(majorEntity);
                odooMove=majorEntity;
            }
            et.setMoveIdText(odooMove.getName());
        }
        //实体关系[DER1N_REPAIR_LINE__STOCK_PRODUCTION_LOT__LOT_ID]
        if(!ObjectUtils.isEmpty(et.getLotId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot odooLot=et.getOdooLot();
            if(ObjectUtils.isEmpty(odooLot)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot majorEntity=stockProductionLotService.get(et.getLotId());
                et.setOdooLot(majorEntity);
                odooLot=majorEntity;
            }
            et.setLotIdText(odooLot.getName());
        }
        //实体关系[DER1N_REPAIR_LINE__UOM_UOM__PRODUCT_UOM]
        if(!ObjectUtils.isEmpty(et.getProductUom())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUom());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Repair_line> getRepairLineByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Repair_line> getRepairLineByEntities(List<Repair_line> entities) {
        List ids =new ArrayList();
        for(Repair_line entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



