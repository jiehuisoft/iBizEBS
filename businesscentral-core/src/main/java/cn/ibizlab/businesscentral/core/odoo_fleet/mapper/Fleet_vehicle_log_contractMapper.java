package cn.ibizlab.businesscentral.core.odoo_fleet.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_log_contractSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Fleet_vehicle_log_contractMapper extends BaseMapper<Fleet_vehicle_log_contract>{

    Page<Fleet_vehicle_log_contract> searchDefault(IPage page, @Param("srf") Fleet_vehicle_log_contractSearchContext context, @Param("ew") Wrapper<Fleet_vehicle_log_contract> wrapper) ;
    @Override
    Fleet_vehicle_log_contract selectById(Serializable id);
    @Override
    int insert(Fleet_vehicle_log_contract entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Fleet_vehicle_log_contract entity);
    @Override
    int update(@Param(Constants.ENTITY) Fleet_vehicle_log_contract entity, @Param("ew") Wrapper<Fleet_vehicle_log_contract> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Fleet_vehicle_log_contract> selectByCostId(@Param("id") Serializable id) ;

    List<Fleet_vehicle_log_contract> selectByInsurerId(@Param("id") Serializable id) ;

    List<Fleet_vehicle_log_contract> selectByPurchaserId(@Param("id") Serializable id) ;

    List<Fleet_vehicle_log_contract> selectByCreateUid(@Param("id") Serializable id) ;

    List<Fleet_vehicle_log_contract> selectByUserId(@Param("id") Serializable id) ;

    List<Fleet_vehicle_log_contract> selectByWriteUid(@Param("id") Serializable id) ;


}
