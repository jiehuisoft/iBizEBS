package cn.ibizlab.businesscentral.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_jobSearchContext;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_jobService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_hr.mapper.Hr_jobMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[工作岗位] 服务对象接口实现
 */
@Slf4j
@Service("Hr_jobServiceImpl")
public class Hr_jobServiceImpl extends EBSServiceImpl<Hr_jobMapper, Hr_job> implements IHr_jobService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_applicantService hrApplicantService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_contractService hrContractService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_recruitment_sourceService hrRecruitmentSourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_recruitment_stageService hrRecruitmentStageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_surveyService surveySurveyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_departmentService hrDepartmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_aliasService mailAliasService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "hr.job" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Hr_job et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_jobService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Hr_job> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Hr_job et) {
        Hr_job old = new Hr_job() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_jobService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_jobService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Hr_job> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        hrApplicantService.resetByJobId(key);
        hrContractService.resetByJobId(key);
        hrEmployeeService.resetByJobId(key);
        hrRecruitmentSourceService.resetByJobId(key);
        hrRecruitmentStageService.removeByJobId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        hrApplicantService.resetByJobId(idList);
        hrContractService.resetByJobId(idList);
        hrEmployeeService.resetByJobId(idList);
        hrRecruitmentSourceService.resetByJobId(idList);
        hrRecruitmentStageService.removeByJobId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Hr_job get(Long key) {
        Hr_job et = getById(key);
        if(et==null){
            et=new Hr_job();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Hr_job getDraft(Hr_job et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Hr_job et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Hr_job et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Hr_job et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Hr_job> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Hr_job> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Hr_job> selectBySurveyId(Long id) {
        return baseMapper.selectBySurveyId(id);
    }
    @Override
    public void removeBySurveyId(Long id) {
        this.remove(new QueryWrapper<Hr_job>().eq("survey_id",id));
    }

	@Override
    public List<Hr_job> selectByDepartmentId(Long id) {
        return baseMapper.selectByDepartmentId(id);
    }
    @Override
    public void resetByDepartmentId(Long id) {
        this.update(new UpdateWrapper<Hr_job>().set("department_id",null).eq("department_id",id));
    }

    @Override
    public void resetByDepartmentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_job>().set("department_id",null).in("department_id",ids));
    }

    @Override
    public void removeByDepartmentId(Long id) {
        this.remove(new QueryWrapper<Hr_job>().eq("department_id",id));
    }

	@Override
    public List<Hr_job> selectByManagerId(Long id) {
        return baseMapper.selectByManagerId(id);
    }
    @Override
    public void resetByManagerId(Long id) {
        this.update(new UpdateWrapper<Hr_job>().set("manager_id",null).eq("manager_id",id));
    }

    @Override
    public void resetByManagerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_job>().set("manager_id",null).in("manager_id",ids));
    }

    @Override
    public void removeByManagerId(Long id) {
        this.remove(new QueryWrapper<Hr_job>().eq("manager_id",id));
    }

	@Override
    public List<Hr_job> selectByAliasId(Long id) {
        return baseMapper.selectByAliasId(id);
    }
    @Override
    public List<Hr_job> selectByAliasId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Hr_job>().in("id",ids));
    }

    @Override
    public void removeByAliasId(Long id) {
        this.remove(new QueryWrapper<Hr_job>().eq("alias_id",id));
    }

	@Override
    public List<Hr_job> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Hr_job>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_job>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Hr_job>().eq("company_id",id));
    }

	@Override
    public List<Hr_job> selectByAddressId(Long id) {
        return baseMapper.selectByAddressId(id);
    }
    @Override
    public void resetByAddressId(Long id) {
        this.update(new UpdateWrapper<Hr_job>().set("address_id",null).eq("address_id",id));
    }

    @Override
    public void resetByAddressId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_job>().set("address_id",null).in("address_id",ids));
    }

    @Override
    public void removeByAddressId(Long id) {
        this.remove(new QueryWrapper<Hr_job>().eq("address_id",id));
    }

	@Override
    public List<Hr_job> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Hr_job>().eq("create_uid",id));
    }

	@Override
    public List<Hr_job> selectByHrResponsibleId(Long id) {
        return baseMapper.selectByHrResponsibleId(id);
    }
    @Override
    public void resetByHrResponsibleId(Long id) {
        this.update(new UpdateWrapper<Hr_job>().set("hr_responsible_id",null).eq("hr_responsible_id",id));
    }

    @Override
    public void resetByHrResponsibleId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_job>().set("hr_responsible_id",null).in("hr_responsible_id",ids));
    }

    @Override
    public void removeByHrResponsibleId(Long id) {
        this.remove(new QueryWrapper<Hr_job>().eq("hr_responsible_id",id));
    }

	@Override
    public List<Hr_job> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Hr_job>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_job>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Hr_job>().eq("user_id",id));
    }

	@Override
    public List<Hr_job> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Hr_job>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Hr_job> searchDefault(Hr_jobSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Hr_job> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Hr_job>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 首选表格
     */
    @Override
    public Page<Hr_job> searchMaster(Hr_jobSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Hr_job> pages=baseMapper.searchMaster(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Hr_job>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Hr_job et){
        //实体关系[DER1N_HR_JOB_SURVEY_SURVEY_SURVEY_ID]
        if(!ObjectUtils.isEmpty(et.getSurveyId())){
            cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_survey odooSurvey=et.getOdooSurvey();
            if(ObjectUtils.isEmpty(odooSurvey)){
                cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_survey majorEntity=surveySurveyService.get(et.getSurveyId());
                et.setOdooSurvey(majorEntity);
                odooSurvey=majorEntity;
            }
            et.setSurveyTitle(odooSurvey.getTitle());
        }
        //实体关系[DER1N_HR_JOB__HR_DEPARTMENT__DEPARTMENT_ID]
        if(!ObjectUtils.isEmpty(et.getDepartmentId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department odooDepartment=et.getOdooDepartment();
            if(ObjectUtils.isEmpty(odooDepartment)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department majorEntity=hrDepartmentService.get(et.getDepartmentId());
                et.setOdooDepartment(majorEntity);
                odooDepartment=majorEntity;
            }
            et.setDepartmentIdText(odooDepartment.getName());
        }
        //实体关系[DER1N_HR_JOB__HR_EMPLOYEE__MANAGER_ID]
        if(!ObjectUtils.isEmpty(et.getManagerId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooManager=et.getOdooManager();
            if(ObjectUtils.isEmpty(odooManager)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getManagerId());
                et.setOdooManager(majorEntity);
                odooManager=majorEntity;
            }
            et.setManagerIdText(odooManager.getName());
        }
        //实体关系[DER1N_HR_JOB__MAIL_ALIAS__ALIAS_ID]
        if(!ObjectUtils.isEmpty(et.getAliasId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias odooAlias=et.getOdooAlias();
            if(ObjectUtils.isEmpty(odooAlias)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias majorEntity=mailAliasService.get(et.getAliasId());
                et.setOdooAlias(majorEntity);
                odooAlias=majorEntity;
            }
            et.setAliasDefaults(odooAlias.getAliasDefaults());
            et.setAliasName(odooAlias.getAliasName());
            et.setAliasForceThreadId(odooAlias.getAliasForceThreadId());
            et.setAliasDomain(odooAlias.getAliasDomain());
            et.setAliasParentModelId(odooAlias.getAliasParentModelId());
            et.setAliasParentThreadId(odooAlias.getAliasParentThreadId());
            et.setAliasModelId(odooAlias.getAliasModelId());
            et.setAliasUserId(odooAlias.getAliasUserId());
            et.setAliasContact(odooAlias.getAliasContact());
        }
        //实体关系[DER1N_HR_JOB__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_HR_JOB__RES_PARTNER__ADDRESS_ID]
        if(!ObjectUtils.isEmpty(et.getAddressId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooAddress=et.getOdooAddress();
            if(ObjectUtils.isEmpty(odooAddress)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getAddressId());
                et.setOdooAddress(majorEntity);
                odooAddress=majorEntity;
            }
            et.setAddressIdText(odooAddress.getName());
        }
        //实体关系[DER1N_HR_JOB__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_HR_JOB__RES_USERS__HR_RESPONSIBLE_ID]
        if(!ObjectUtils.isEmpty(et.getHrResponsibleId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooHrResponsible=et.getOdooHrResponsible();
            if(ObjectUtils.isEmpty(odooHrResponsible)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getHrResponsibleId());
                et.setOdooHrResponsible(majorEntity);
                odooHrResponsible=majorEntity;
            }
            et.setHrResponsibleIdText(odooHrResponsible.getName());
        }
        //实体关系[DER1N_HR_JOB__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_HR_JOB__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Hr_job> getHrJobByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Hr_job> getHrJobByEntities(List<Hr_job> entities) {
        List ids =new ArrayList();
        for(Hr_job entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



