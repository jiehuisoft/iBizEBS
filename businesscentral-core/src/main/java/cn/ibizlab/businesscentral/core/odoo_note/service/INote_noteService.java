package cn.ibizlab.businesscentral.core.odoo_note.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_note.domain.Note_note;
import cn.ibizlab.businesscentral.core.odoo_note.filter.Note_noteSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Note_note] 服务对象接口
 */
public interface INote_noteService extends IService<Note_note>{

    boolean create(Note_note et) ;
    void createBatch(List<Note_note> list) ;
    boolean update(Note_note et) ;
    void updateBatch(List<Note_note> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Note_note get(Long key) ;
    Note_note getDraft(Note_note et) ;
    boolean checkKey(Note_note et) ;
    boolean save(Note_note et) ;
    void saveBatch(List<Note_note> list) ;
    Page<Note_note> searchDefault(Note_noteSearchContext context) ;
    List<Note_note> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Note_note> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Note_note> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Note_note> getNoteNoteByIds(List<Long> ids) ;
    List<Note_note> getNoteNoteByEntities(List<Note_note> entities) ;
}


