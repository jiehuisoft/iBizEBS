package cn.ibizlab.businesscentral.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_rule;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_ruleSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mro_pm_rule] 服务对象接口
 */
public interface IMro_pm_ruleService extends IService<Mro_pm_rule>{

    boolean create(Mro_pm_rule et) ;
    void createBatch(List<Mro_pm_rule> list) ;
    boolean update(Mro_pm_rule et) ;
    void updateBatch(List<Mro_pm_rule> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mro_pm_rule get(Long key) ;
    Mro_pm_rule getDraft(Mro_pm_rule et) ;
    boolean checkKey(Mro_pm_rule et) ;
    boolean save(Mro_pm_rule et) ;
    void saveBatch(List<Mro_pm_rule> list) ;
    Page<Mro_pm_rule> searchDefault(Mro_pm_ruleSearchContext context) ;
    List<Mro_pm_rule> selectByCategoryId(Long id);
    List<Mro_pm_rule> selectByCategoryId(Collection<Long> ids);
    void removeByCategoryId(Long id);
    List<Mro_pm_rule> selectByParameterId(Long id);
    List<Mro_pm_rule> selectByParameterId(Collection<Long> ids);
    void removeByParameterId(Long id);
    List<Mro_pm_rule> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mro_pm_rule> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mro_pm_rule> getMroPmRuleByIds(List<Long> ids) ;
    List<Mro_pm_rule> getMroPmRuleByEntities(List<Mro_pm_rule> entities) ;
}


