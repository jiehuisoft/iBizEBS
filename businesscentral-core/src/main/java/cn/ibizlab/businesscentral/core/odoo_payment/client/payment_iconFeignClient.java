package cn.ibizlab.businesscentral.core.odoo_payment.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_icon;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_iconSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[payment_icon] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-payment:odoo-payment}", contextId = "payment-icon", fallback = payment_iconFallback.class)
public interface payment_iconFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/payment_icons")
    Payment_icon create(@RequestBody Payment_icon payment_icon);

    @RequestMapping(method = RequestMethod.POST, value = "/payment_icons/batch")
    Boolean createBatch(@RequestBody List<Payment_icon> payment_icons);



    @RequestMapping(method = RequestMethod.DELETE, value = "/payment_icons/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/payment_icons/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/payment_icons/search")
    Page<Payment_icon> search(@RequestBody Payment_iconSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/payment_icons/{id}")
    Payment_icon get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/payment_icons/{id}")
    Payment_icon update(@PathVariable("id") Long id,@RequestBody Payment_icon payment_icon);

    @RequestMapping(method = RequestMethod.PUT, value = "/payment_icons/batch")
    Boolean updateBatch(@RequestBody List<Payment_icon> payment_icons);


    @RequestMapping(method = RequestMethod.GET, value = "/payment_icons/select")
    Page<Payment_icon> select();


    @RequestMapping(method = RequestMethod.GET, value = "/payment_icons/getdraft")
    Payment_icon getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/payment_icons/checkkey")
    Boolean checkKey(@RequestBody Payment_icon payment_icon);


    @RequestMapping(method = RequestMethod.POST, value = "/payment_icons/save")
    Boolean save(@RequestBody Payment_icon payment_icon);

    @RequestMapping(method = RequestMethod.POST, value = "/payment_icons/savebatch")
    Boolean saveBatch(@RequestBody List<Payment_icon> payment_icons);



    @RequestMapping(method = RequestMethod.POST, value = "/payment_icons/searchdefault")
    Page<Payment_icon> searchDefault(@RequestBody Payment_iconSearchContext context);


}
