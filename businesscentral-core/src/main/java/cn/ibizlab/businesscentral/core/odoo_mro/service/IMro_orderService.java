package cn.ibizlab.businesscentral.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_order;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_orderSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mro_order] 服务对象接口
 */
public interface IMro_orderService extends IService<Mro_order>{

    boolean create(Mro_order et) ;
    void createBatch(List<Mro_order> list) ;
    boolean update(Mro_order et) ;
    void updateBatch(List<Mro_order> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mro_order get(Long key) ;
    Mro_order getDraft(Mro_order et) ;
    boolean checkKey(Mro_order et) ;
    boolean save(Mro_order et) ;
    void saveBatch(List<Mro_order> list) ;
    Page<Mro_order> searchDefault(Mro_orderSearchContext context) ;
    List<Mro_order> selectByAssetId(Long id);
    void resetByAssetId(Long id);
    void resetByAssetId(Collection<Long> ids);
    void removeByAssetId(Long id);
    List<Mro_order> selectByRequestId(Long id);
    void resetByRequestId(Long id);
    void resetByRequestId(Collection<Long> ids);
    void removeByRequestId(Long id);
    List<Mro_order> selectByTaskId(Long id);
    void resetByTaskId(Long id);
    void resetByTaskId(Collection<Long> ids);
    void removeByTaskId(Long id);
    List<Mro_order> selectByWoId(Long id);
    void removeByWoId(Collection<Long> ids);
    void removeByWoId(Long id);
    List<Mro_order> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Mro_order> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mro_order> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Mro_order> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mro_order> getMroOrderByIds(List<Long> ids) ;
    List<Mro_order> getMroOrderByEntities(List<Mro_order> entities) ;
}


