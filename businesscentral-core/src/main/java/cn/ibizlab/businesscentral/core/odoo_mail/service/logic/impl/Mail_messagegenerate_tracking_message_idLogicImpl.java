package cn.ibizlab.businesscentral.core.odoo_mail.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.odoo_mail.service.logic.IMail_messagegenerate_tracking_message_idLogic;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message;

/**
 * 关系型数据实体[generate_tracking_message_id] 对象
 */
@Slf4j
@Service
public class Mail_messagegenerate_tracking_message_idLogicImpl implements IMail_messagegenerate_tracking_message_idLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_messageService mail_messageservice;

    public cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_messageService getMail_messageService() {
        return this.mail_messageservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_messageService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_messageService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Mail_message et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("mail_messagegenerate_tracking_message_iddefault",et);
           kieSession.setGlobal("mail_messageservice",mail_messageservice);
           kieSession.setGlobal("iBzSysMail_messageDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.odoo_mail.service.logic.mail_messagegenerate_tracking_message_id");

        }catch(Exception e){
            throw new RuntimeException("执行[generate_tracking_message_id]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
