package cn.ibizlab.businesscentral.core.odoo_payment.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_transaction;
/**
 * 关系型数据实体[Payment_transaction] 查询条件对象
 */
@Slf4j
@Data
public class Payment_transactionSearchContext extends QueryWrapperContext<Payment_transaction> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_partner_lang_eq;//[语言]
	public void setN_partner_lang_eq(String n_partner_lang_eq) {
        this.n_partner_lang_eq = n_partner_lang_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_lang_eq)){
            this.getSearchCond().eq("partner_lang", n_partner_lang_eq);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_type_eq;//[类型]
	public void setN_type_eq(String n_type_eq) {
        this.n_type_eq = n_type_eq;
        if(!ObjectUtils.isEmpty(this.n_type_eq)){
            this.getSearchCond().eq("type", n_type_eq);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_payment_id_text_eq;//[付款]
	public void setN_payment_id_text_eq(String n_payment_id_text_eq) {
        this.n_payment_id_text_eq = n_payment_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_id_text_eq)){
            this.getSearchCond().eq("payment_id_text", n_payment_id_text_eq);
        }
    }
	private String n_payment_id_text_like;//[付款]
	public void setN_payment_id_text_like(String n_payment_id_text_like) {
        this.n_payment_id_text_like = n_payment_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_payment_id_text_like)){
            this.getSearchCond().like("payment_id_text", n_payment_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[客户]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[客户]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_acquirer_id_text_eq;//[收单方]
	public void setN_acquirer_id_text_eq(String n_acquirer_id_text_eq) {
        this.n_acquirer_id_text_eq = n_acquirer_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_acquirer_id_text_eq)){
            this.getSearchCond().eq("acquirer_id_text", n_acquirer_id_text_eq);
        }
    }
	private String n_acquirer_id_text_like;//[收单方]
	public void setN_acquirer_id_text_like(String n_acquirer_id_text_like) {
        this.n_acquirer_id_text_like = n_acquirer_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_acquirer_id_text_like)){
            this.getSearchCond().like("acquirer_id_text", n_acquirer_id_text_like);
        }
    }
	private String n_partner_country_id_text_eq;//[国家]
	public void setN_partner_country_id_text_eq(String n_partner_country_id_text_eq) {
        this.n_partner_country_id_text_eq = n_partner_country_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_country_id_text_eq)){
            this.getSearchCond().eq("partner_country_id_text", n_partner_country_id_text_eq);
        }
    }
	private String n_partner_country_id_text_like;//[国家]
	public void setN_partner_country_id_text_like(String n_partner_country_id_text_like) {
        this.n_partner_country_id_text_like = n_partner_country_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_country_id_text_like)){
            this.getSearchCond().like("partner_country_id_text", n_partner_country_id_text_like);
        }
    }
	private String n_payment_token_id_text_eq;//[付款令牌]
	public void setN_payment_token_id_text_eq(String n_payment_token_id_text_eq) {
        this.n_payment_token_id_text_eq = n_payment_token_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_token_id_text_eq)){
            this.getSearchCond().eq("payment_token_id_text", n_payment_token_id_text_eq);
        }
    }
	private String n_payment_token_id_text_like;//[付款令牌]
	public void setN_payment_token_id_text_like(String n_payment_token_id_text_like) {
        this.n_payment_token_id_text_like = n_payment_token_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_payment_token_id_text_like)){
            this.getSearchCond().like("payment_token_id_text", n_payment_token_id_text_like);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_partner_country_id_eq;//[国家]
	public void setN_partner_country_id_eq(Long n_partner_country_id_eq) {
        this.n_partner_country_id_eq = n_partner_country_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_country_id_eq)){
            this.getSearchCond().eq("partner_country_id", n_partner_country_id_eq);
        }
    }
	private Long n_payment_token_id_eq;//[付款令牌]
	public void setN_payment_token_id_eq(Long n_payment_token_id_eq) {
        this.n_payment_token_id_eq = n_payment_token_id_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_token_id_eq)){
            this.getSearchCond().eq("payment_token_id", n_payment_token_id_eq);
        }
    }
	private Long n_partner_id_eq;//[客户]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_payment_id_eq;//[付款]
	public void setN_payment_id_eq(Long n_payment_id_eq) {
        this.n_payment_id_eq = n_payment_id_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_id_eq)){
            this.getSearchCond().eq("payment_id", n_payment_id_eq);
        }
    }
	private Long n_acquirer_id_eq;//[收单方]
	public void setN_acquirer_id_eq(Long n_acquirer_id_eq) {
        this.n_acquirer_id_eq = n_acquirer_id_eq;
        if(!ObjectUtils.isEmpty(this.n_acquirer_id_eq)){
            this.getSearchCond().eq("acquirer_id", n_acquirer_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



