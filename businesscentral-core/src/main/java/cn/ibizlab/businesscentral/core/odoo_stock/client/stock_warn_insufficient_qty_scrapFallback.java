package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty_scrap;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warn_insufficient_qty_scrapSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_warn_insufficient_qty_scrap] 服务对象接口
 */
@Component
public class stock_warn_insufficient_qty_scrapFallback implements stock_warn_insufficient_qty_scrapFeignClient{

    public Page<Stock_warn_insufficient_qty_scrap> search(Stock_warn_insufficient_qty_scrapSearchContext context){
            return null;
     }



    public Stock_warn_insufficient_qty_scrap create(Stock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap){
            return null;
     }
    public Boolean createBatch(List<Stock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Stock_warn_insufficient_qty_scrap update(Long id, Stock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap){
            return null;
     }
    public Boolean updateBatch(List<Stock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps){
            return false;
     }


    public Stock_warn_insufficient_qty_scrap get(Long id){
            return null;
     }


    public Page<Stock_warn_insufficient_qty_scrap> select(){
            return null;
     }

    public Stock_warn_insufficient_qty_scrap getDraft(){
            return null;
    }



    public Boolean checkKey(Stock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap){
            return false;
     }


    public Boolean save(Stock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap){
            return false;
     }
    public Boolean saveBatch(List<Stock_warn_insufficient_qty_scrap> stock_warn_insufficient_qty_scraps){
            return false;
     }

    public Page<Stock_warn_insufficient_qty_scrap> searchDefault(Stock_warn_insufficient_qty_scrapSearchContext context){
            return null;
     }


}
