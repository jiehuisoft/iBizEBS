package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_attribute;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_attributeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_attribute] 服务对象接口
 */
@Component
public class product_attributeFallback implements product_attributeFeignClient{

    public Product_attribute get(Long id){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Product_attribute update(Long id, Product_attribute product_attribute){
            return null;
     }
    public Boolean updateBatch(List<Product_attribute> product_attributes){
            return false;
     }


    public Page<Product_attribute> search(Product_attributeSearchContext context){
            return null;
     }



    public Product_attribute create(Product_attribute product_attribute){
            return null;
     }
    public Boolean createBatch(List<Product_attribute> product_attributes){
            return false;
     }


    public Page<Product_attribute> select(){
            return null;
     }

    public Product_attribute getDraft(){
            return null;
    }



    public Boolean checkKey(Product_attribute product_attribute){
            return false;
     }


    public Boolean save(Product_attribute product_attribute){
            return false;
     }
    public Boolean saveBatch(List<Product_attribute> product_attributes){
            return false;
     }

    public Page<Product_attribute> searchDefault(Product_attributeSearchContext context){
            return null;
     }


}
