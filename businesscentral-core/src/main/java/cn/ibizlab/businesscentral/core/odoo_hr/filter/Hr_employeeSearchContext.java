package cn.ibizlab.businesscentral.core.odoo_hr.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee;
/**
 * 关系型数据实体[Hr_employee] 查询条件对象
 */
@Slf4j
@Data
public class Hr_employeeSearchContext extends QueryWrapperContext<Hr_employee> {

	private String n_marital_eq;//[婚姻状况]
	public void setN_marital_eq(String n_marital_eq) {
        this.n_marital_eq = n_marital_eq;
        if(!ObjectUtils.isEmpty(this.n_marital_eq)){
            this.getSearchCond().eq("marital", n_marital_eq);
        }
    }
	private String n_certificate_eq;//[证书等级]
	public void setN_certificate_eq(String n_certificate_eq) {
        this.n_certificate_eq = n_certificate_eq;
        if(!ObjectUtils.isEmpty(this.n_certificate_eq)){
            this.getSearchCond().eq("certificate", n_certificate_eq);
        }
    }
	private String n_attendance_state_eq;//[出勤状态]
	public void setN_attendance_state_eq(String n_attendance_state_eq) {
        this.n_attendance_state_eq = n_attendance_state_eq;
        if(!ObjectUtils.isEmpty(this.n_attendance_state_eq)){
            this.getSearchCond().eq("attendance_state", n_attendance_state_eq);
        }
    }
	private String n_gender_eq;//[性别]
	public void setN_gender_eq(String n_gender_eq) {
        this.n_gender_eq = n_gender_eq;
        if(!ObjectUtils.isEmpty(this.n_gender_eq)){
            this.getSearchCond().eq("gender", n_gender_eq);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_current_leave_state_eq;//[当前休假状态]
	public void setN_current_leave_state_eq(String n_current_leave_state_eq) {
        this.n_current_leave_state_eq = n_current_leave_state_eq;
        if(!ObjectUtils.isEmpty(this.n_current_leave_state_eq)){
            this.getSearchCond().eq("current_leave_state", n_current_leave_state_eq);
        }
    }
	private String n_parent_id_text_eq;//[经理]
	public void setN_parent_id_text_eq(String n_parent_id_text_eq) {
        this.n_parent_id_text_eq = n_parent_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_eq)){
            this.getSearchCond().eq("parent_id_text", n_parent_id_text_eq);
        }
    }
	private String n_parent_id_text_like;//[经理]
	public void setN_parent_id_text_like(String n_parent_id_text_like) {
        this.n_parent_id_text_like = n_parent_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_like)){
            this.getSearchCond().like("parent_id_text", n_parent_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_name_eq;//[名称]
	public void setN_name_eq(String n_name_eq) {
        this.n_name_eq = n_name_eq;
        if(!ObjectUtils.isEmpty(this.n_name_eq)){
            this.getSearchCond().eq("name", n_name_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_address_home_id_text_eq;//[家庭住址]
	public void setN_address_home_id_text_eq(String n_address_home_id_text_eq) {
        this.n_address_home_id_text_eq = n_address_home_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_address_home_id_text_eq)){
            this.getSearchCond().eq("address_home_id_text", n_address_home_id_text_eq);
        }
    }
	private String n_address_home_id_text_like;//[家庭住址]
	public void setN_address_home_id_text_like(String n_address_home_id_text_like) {
        this.n_address_home_id_text_like = n_address_home_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_address_home_id_text_like)){
            this.getSearchCond().like("address_home_id_text", n_address_home_id_text_like);
        }
    }
	private String n_resource_calendar_id_text_eq;//[工作时间]
	public void setN_resource_calendar_id_text_eq(String n_resource_calendar_id_text_eq) {
        this.n_resource_calendar_id_text_eq = n_resource_calendar_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_calendar_id_text_eq)){
            this.getSearchCond().eq("resource_calendar_id_text", n_resource_calendar_id_text_eq);
        }
    }
	private String n_resource_calendar_id_text_like;//[工作时间]
	public void setN_resource_calendar_id_text_like(String n_resource_calendar_id_text_like) {
        this.n_resource_calendar_id_text_like = n_resource_calendar_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_resource_calendar_id_text_like)){
            this.getSearchCond().like("resource_calendar_id_text", n_resource_calendar_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[用户]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[用户]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_department_id_text_eq;//[部门]
	public void setN_department_id_text_eq(String n_department_id_text_eq) {
        this.n_department_id_text_eq = n_department_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_department_id_text_eq)){
            this.getSearchCond().eq("department_id_text", n_department_id_text_eq);
        }
    }
	private String n_department_id_text_like;//[部门]
	public void setN_department_id_text_like(String n_department_id_text_like) {
        this.n_department_id_text_like = n_department_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_department_id_text_like)){
            this.getSearchCond().like("department_id_text", n_department_id_text_like);
        }
    }
	private String n_country_of_birth_text_eq;//[国籍]
	public void setN_country_of_birth_text_eq(String n_country_of_birth_text_eq) {
        this.n_country_of_birth_text_eq = n_country_of_birth_text_eq;
        if(!ObjectUtils.isEmpty(this.n_country_of_birth_text_eq)){
            this.getSearchCond().eq("country_of_birth_text", n_country_of_birth_text_eq);
        }
    }
	private String n_country_of_birth_text_like;//[国籍]
	public void setN_country_of_birth_text_like(String n_country_of_birth_text_like) {
        this.n_country_of_birth_text_like = n_country_of_birth_text_like;
        if(!ObjectUtils.isEmpty(this.n_country_of_birth_text_like)){
            this.getSearchCond().like("country_of_birth_text", n_country_of_birth_text_like);
        }
    }
	private String n_expense_manager_id_text_eq;//[负责人]
	public void setN_expense_manager_id_text_eq(String n_expense_manager_id_text_eq) {
        this.n_expense_manager_id_text_eq = n_expense_manager_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_expense_manager_id_text_eq)){
            this.getSearchCond().eq("expense_manager_id_text", n_expense_manager_id_text_eq);
        }
    }
	private String n_expense_manager_id_text_like;//[负责人]
	public void setN_expense_manager_id_text_like(String n_expense_manager_id_text_like) {
        this.n_expense_manager_id_text_like = n_expense_manager_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_expense_manager_id_text_like)){
            this.getSearchCond().like("expense_manager_id_text", n_expense_manager_id_text_like);
        }
    }
	private String n_job_id_text_eq;//[工作岗位]
	public void setN_job_id_text_eq(String n_job_id_text_eq) {
        this.n_job_id_text_eq = n_job_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_job_id_text_eq)){
            this.getSearchCond().eq("job_id_text", n_job_id_text_eq);
        }
    }
	private String n_job_id_text_like;//[工作岗位]
	public void setN_job_id_text_like(String n_job_id_text_like) {
        this.n_job_id_text_like = n_job_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_job_id_text_like)){
            this.getSearchCond().like("job_id_text", n_job_id_text_like);
        }
    }
	private String n_address_id_text_eq;//[工作地址]
	public void setN_address_id_text_eq(String n_address_id_text_eq) {
        this.n_address_id_text_eq = n_address_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_address_id_text_eq)){
            this.getSearchCond().eq("address_id_text", n_address_id_text_eq);
        }
    }
	private String n_address_id_text_like;//[工作地址]
	public void setN_address_id_text_like(String n_address_id_text_like) {
        this.n_address_id_text_like = n_address_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_address_id_text_like)){
            this.getSearchCond().like("address_id_text", n_address_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_coach_id_text_eq;//[教练]
	public void setN_coach_id_text_eq(String n_coach_id_text_eq) {
        this.n_coach_id_text_eq = n_coach_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_coach_id_text_eq)){
            this.getSearchCond().eq("coach_id_text", n_coach_id_text_eq);
        }
    }
	private String n_coach_id_text_like;//[教练]
	public void setN_coach_id_text_like(String n_coach_id_text_like) {
        this.n_coach_id_text_like = n_coach_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_coach_id_text_like)){
            this.getSearchCond().like("coach_id_text", n_coach_id_text_like);
        }
    }
	private String n_country_id_text_eq;//[国籍(国家)]
	public void setN_country_id_text_eq(String n_country_id_text_eq) {
        this.n_country_id_text_eq = n_country_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_eq)){
            this.getSearchCond().eq("country_id_text", n_country_id_text_eq);
        }
    }
	private String n_country_id_text_like;//[国籍(国家)]
	public void setN_country_id_text_like(String n_country_id_text_like) {
        this.n_country_id_text_like = n_country_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_like)){
            this.getSearchCond().like("country_id_text", n_country_id_text_like);
        }
    }
	private Long n_address_home_id_eq;//[家庭住址]
	public void setN_address_home_id_eq(Long n_address_home_id_eq) {
        this.n_address_home_id_eq = n_address_home_id_eq;
        if(!ObjectUtils.isEmpty(this.n_address_home_id_eq)){
            this.getSearchCond().eq("address_home_id", n_address_home_id_eq);
        }
    }
	private Long n_expense_manager_id_eq;//[负责人]
	public void setN_expense_manager_id_eq(Long n_expense_manager_id_eq) {
        this.n_expense_manager_id_eq = n_expense_manager_id_eq;
        if(!ObjectUtils.isEmpty(this.n_expense_manager_id_eq)){
            this.getSearchCond().eq("expense_manager_id", n_expense_manager_id_eq);
        }
    }
	private Long n_bank_account_id_eq;//[银行账户号码]
	public void setN_bank_account_id_eq(Long n_bank_account_id_eq) {
        this.n_bank_account_id_eq = n_bank_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_bank_account_id_eq)){
            this.getSearchCond().eq("bank_account_id", n_bank_account_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_country_id_eq;//[国籍(国家)]
	public void setN_country_id_eq(Long n_country_id_eq) {
        this.n_country_id_eq = n_country_id_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_eq)){
            this.getSearchCond().eq("country_id", n_country_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_job_id_eq;//[工作岗位]
	public void setN_job_id_eq(Long n_job_id_eq) {
        this.n_job_id_eq = n_job_id_eq;
        if(!ObjectUtils.isEmpty(this.n_job_id_eq)){
            this.getSearchCond().eq("job_id", n_job_id_eq);
        }
    }
	private Long n_resource_id_eq;//[资源]
	public void setN_resource_id_eq(Long n_resource_id_eq) {
        this.n_resource_id_eq = n_resource_id_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_id_eq)){
            this.getSearchCond().eq("resource_id", n_resource_id_eq);
        }
    }
	private Long n_user_id_eq;//[用户]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_department_id_eq;//[部门]
	public void setN_department_id_eq(Long n_department_id_eq) {
        this.n_department_id_eq = n_department_id_eq;
        if(!ObjectUtils.isEmpty(this.n_department_id_eq)){
            this.getSearchCond().eq("department_id", n_department_id_eq);
        }
    }
	private Long n_parent_id_eq;//[经理]
	public void setN_parent_id_eq(Long n_parent_id_eq) {
        this.n_parent_id_eq = n_parent_id_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_eq)){
            this.getSearchCond().eq("parent_id", n_parent_id_eq);
        }
    }
	private Long n_last_attendance_id_eq;//[上次出勤]
	public void setN_last_attendance_id_eq(Long n_last_attendance_id_eq) {
        this.n_last_attendance_id_eq = n_last_attendance_id_eq;
        if(!ObjectUtils.isEmpty(this.n_last_attendance_id_eq)){
            this.getSearchCond().eq("last_attendance_id", n_last_attendance_id_eq);
        }
    }
	private Long n_coach_id_eq;//[教练]
	public void setN_coach_id_eq(Long n_coach_id_eq) {
        this.n_coach_id_eq = n_coach_id_eq;
        if(!ObjectUtils.isEmpty(this.n_coach_id_eq)){
            this.getSearchCond().eq("coach_id", n_coach_id_eq);
        }
    }
	private Long n_address_id_eq;//[工作地址]
	public void setN_address_id_eq(Long n_address_id_eq) {
        this.n_address_id_eq = n_address_id_eq;
        if(!ObjectUtils.isEmpty(this.n_address_id_eq)){
            this.getSearchCond().eq("address_id", n_address_id_eq);
        }
    }
	private Long n_country_of_birth_eq;//[国籍]
	public void setN_country_of_birth_eq(Long n_country_of_birth_eq) {
        this.n_country_of_birth_eq = n_country_of_birth_eq;
        if(!ObjectUtils.isEmpty(this.n_country_of_birth_eq)){
            this.getSearchCond().eq("country_of_birth", n_country_of_birth_eq);
        }
    }
	private Long n_resource_calendar_id_eq;//[工作时间]
	public void setN_resource_calendar_id_eq(Long n_resource_calendar_id_eq) {
        this.n_resource_calendar_id_eq = n_resource_calendar_id_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_calendar_id_eq)){
            this.getSearchCond().eq("resource_calendar_id", n_resource_calendar_id_eq);
        }
    }
	private Long n_leave_manager_id_eq;//[ID]
	public void setN_leave_manager_id_eq(Long n_leave_manager_id_eq) {
        this.n_leave_manager_id_eq = n_leave_manager_id_eq;
        if(!ObjectUtils.isEmpty(this.n_leave_manager_id_eq)){
            this.getSearchCond().eq("leave_manager_id", n_leave_manager_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



