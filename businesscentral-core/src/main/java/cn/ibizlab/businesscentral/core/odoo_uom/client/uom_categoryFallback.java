package cn.ibizlab.businesscentral.core.odoo_uom.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_category;
import cn.ibizlab.businesscentral.core.odoo_uom.filter.Uom_categorySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[uom_category] 服务对象接口
 */
@Component
public class uom_categoryFallback implements uom_categoryFeignClient{


    public Uom_category get(Long id){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Uom_category create(Uom_category uom_category){
            return null;
     }
    public Boolean createBatch(List<Uom_category> uom_categories){
            return false;
     }

    public Page<Uom_category> search(Uom_categorySearchContext context){
            return null;
     }



    public Uom_category update(Long id, Uom_category uom_category){
            return null;
     }
    public Boolean updateBatch(List<Uom_category> uom_categories){
            return false;
     }


    public Page<Uom_category> select(){
            return null;
     }

    public Uom_category getDraft(){
            return null;
    }



    public Boolean checkKey(Uom_category uom_category){
            return false;
     }


    public Boolean save(Uom_category uom_category){
            return false;
     }
    public Boolean saveBatch(List<Uom_category> uom_categories){
            return false;
     }

    public Page<Uom_category> searchDefault(Uom_categorySearchContext context){
            return null;
     }


}
