package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenter_productivity_loss_typeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_workcenter_productivity_loss_type] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mrp:odoo-mrp}", contextId = "mrp-workcenter-productivity-loss-type", fallback = mrp_workcenter_productivity_loss_typeFallback.class)
public interface mrp_workcenter_productivity_loss_typeFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types")
    Mrp_workcenter_productivity_loss_type create(@RequestBody Mrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types/batch")
    Boolean createBatch(@RequestBody List<Mrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types);




    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivity_loss_types/{id}")
    Mrp_workcenter_productivity_loss_type get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivity_loss_types/{id}")
    Mrp_workcenter_productivity_loss_type update(@PathVariable("id") Long id,@RequestBody Mrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivity_loss_types/batch")
    Boolean updateBatch(@RequestBody List<Mrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivity_loss_types/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivity_loss_types/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types/search")
    Page<Mrp_workcenter_productivity_loss_type> search(@RequestBody Mrp_workcenter_productivity_loss_typeSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivity_loss_types/select")
    Page<Mrp_workcenter_productivity_loss_type> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivity_loss_types/getdraft")
    Mrp_workcenter_productivity_loss_type getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types/checkkey")
    Boolean checkKey(@RequestBody Mrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types/save")
    Boolean save(@RequestBody Mrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types/savebatch")
    Boolean saveBatch(@RequestBody List<Mrp_workcenter_productivity_loss_type> mrp_workcenter_productivity_loss_types);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types/searchdefault")
    Page<Mrp_workcenter_productivity_loss_type> searchDefault(@RequestBody Mrp_workcenter_productivity_loss_typeSearchContext context);


}
