package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_scrap;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_scrapSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_scrap] 服务对象接口
 */
public interface IStock_scrapService extends IService<Stock_scrap>{

    boolean create(Stock_scrap et) ;
    void createBatch(List<Stock_scrap> list) ;
    boolean update(Stock_scrap et) ;
    void updateBatch(List<Stock_scrap> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_scrap get(Long key) ;
    Stock_scrap getDraft(Stock_scrap et) ;
    boolean checkKey(Stock_scrap et) ;
    boolean save(Stock_scrap et) ;
    void saveBatch(List<Stock_scrap> list) ;
    Page<Stock_scrap> searchDefault(Stock_scrapSearchContext context) ;
    List<Stock_scrap> selectByProductionId(Long id);
    void resetByProductionId(Long id);
    void resetByProductionId(Collection<Long> ids);
    void removeByProductionId(Long id);
    List<Stock_scrap> selectByWorkorderId(Long id);
    void resetByWorkorderId(Long id);
    void resetByWorkorderId(Collection<Long> ids);
    void removeByWorkorderId(Long id);
    List<Stock_scrap> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Stock_scrap> selectByOwnerId(Long id);
    void resetByOwnerId(Long id);
    void resetByOwnerId(Collection<Long> ids);
    void removeByOwnerId(Long id);
    List<Stock_scrap> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_scrap> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_scrap> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    List<Stock_scrap> selectByScrapLocationId(Long id);
    void resetByScrapLocationId(Long id);
    void resetByScrapLocationId(Collection<Long> ids);
    void removeByScrapLocationId(Long id);
    List<Stock_scrap> selectByMoveId(Long id);
    void resetByMoveId(Long id);
    void resetByMoveId(Collection<Long> ids);
    void removeByMoveId(Long id);
    List<Stock_scrap> selectByPickingId(Long id);
    void resetByPickingId(Long id);
    void resetByPickingId(Collection<Long> ids);
    void removeByPickingId(Long id);
    List<Stock_scrap> selectByLotId(Long id);
    void resetByLotId(Long id);
    void resetByLotId(Collection<Long> ids);
    void removeByLotId(Long id);
    List<Stock_scrap> selectByPackageId(Long id);
    void resetByPackageId(Long id);
    void resetByPackageId(Collection<Long> ids);
    void removeByPackageId(Long id);
    List<Stock_scrap> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_scrap> getStockScrapByIds(List<Long> ids) ;
    List<Stock_scrap> getStockScrapByEntities(List<Stock_scrap> entities) ;
}


