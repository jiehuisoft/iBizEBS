package cn.ibizlab.businesscentral.core.odoo_digest.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[摘要]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "DIGEST_DIGEST",resultMap = "Digest_digestResultMap")
public class Digest_digest extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * Kpi网站销售总价值
     */
    @TableField(exist = false)
    @JSONField(name = "kpi_website_sale_total_value")
    @JsonProperty("kpi_website_sale_total_value")
    private BigDecimal kpiWebsiteSaleTotalValue;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * KPI CRM预期收益
     */
    @TableField(exist = false)
    @JSONField(name = "kpi_crm_lead_created_value")
    @JsonProperty("kpi_crm_lead_created_value")
    private Integer kpiCrmLeadCreatedValue;
    /**
     * 已签单商机
     */
    @DEField(name = "kpi_crm_opportunities_won")
    @TableField(value = "kpi_crm_opportunities_won")
    @JSONField(name = "kpi_crm_opportunities_won")
    @JsonProperty("kpi_crm_opportunities_won")
    private Boolean kpiCrmOpportunitiesWon;
    /**
     * 可用字段
     */
    @TableField(exist = false)
    @JSONField(name = "available_fields")
    @JsonProperty("available_fields")
    private String availableFields;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 收件人
     */
    @TableField(exist = false)
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;
    /**
     * KPI CRM签单金额
     */
    @TableField(exist = false)
    @JSONField(name = "kpi_crm_opportunities_won_value")
    @JsonProperty("kpi_crm_opportunities_won_value")
    private Integer kpiCrmOpportunitiesWonValue;
    /**
     * 员工
     */
    @DEField(name = "kpi_hr_recruitment_new_colleagues")
    @TableField(value = "kpi_hr_recruitment_new_colleagues")
    @JSONField(name = "kpi_hr_recruitment_new_colleagues")
    @JsonProperty("kpi_hr_recruitment_new_colleagues")
    private Boolean kpiHrRecruitmentNewColleagues;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * POS 销售
     */
    @DEField(name = "kpi_pos_total")
    @TableField(value = "kpi_pos_total")
    @JSONField(name = "kpi_pos_total")
    @JsonProperty("kpi_pos_total")
    private Boolean kpiPosTotal;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 终端销售总额的关键绩效指标
     */
    @TableField(exist = false)
    @JSONField(name = "kpi_pos_total_value")
    @JsonProperty("kpi_pos_total_value")
    private BigDecimal kpiPosTotalValue;
    /**
     * 电商销售
     */
    @DEField(name = "kpi_website_sale_total")
    @TableField(value = "kpi_website_sale_total")
    @JSONField(name = "kpi_website_sale_total")
    @JsonProperty("kpi_website_sale_total")
    private Boolean kpiWebsiteSaleTotal;
    /**
     * 所有销售
     */
    @DEField(name = "kpi_all_sale_total")
    @TableField(value = "kpi_all_sale_total")
    @JSONField(name = "kpi_all_sale_total")
    @JsonProperty("kpi_all_sale_total")
    private Boolean kpiAllSaleTotal;
    /**
     * 下一发送日期
     */
    @DEField(name = "next_run_date")
    @TableField(value = "next_run_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "next_run_date" , format="yyyy-MM-dd")
    @JsonProperty("next_run_date")
    private Timestamp nextRunDate;
    /**
     * 已连接用户
     */
    @DEField(name = "kpi_res_users_connected")
    @TableField(value = "kpi_res_users_connected")
    @JSONField(name = "kpi_res_users_connected")
    @JsonProperty("kpi_res_users_connected")
    private Boolean kpiResUsersConnected;
    /**
     * Kpi Res 用户连接值
     */
    @TableField(exist = false)
    @JSONField(name = "kpi_res_users_connected_value")
    @JsonProperty("kpi_res_users_connected_value")
    private Integer kpiResUsersConnectedValue;
    /**
     * 周期
     */
    @TableField(value = "periodicity")
    @JSONField(name = "periodicity")
    @JsonProperty("periodicity")
    private String periodicity;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 人力资源新聘员工KPI指标
     */
    @TableField(exist = false)
    @JSONField(name = "kpi_hr_recruitment_new_colleagues_value")
    @JsonProperty("kpi_hr_recruitment_new_colleagues_value")
    private Integer kpiHrRecruitmentNewColleaguesValue;
    /**
     * 收入
     */
    @DEField(name = "kpi_account_total_revenue")
    @TableField(value = "kpi_account_total_revenue")
    @JSONField(name = "kpi_account_total_revenue")
    @JsonProperty("kpi_account_total_revenue")
    private Boolean kpiAccountTotalRevenue;
    /**
     * 新的线索/商机
     */
    @DEField(name = "kpi_crm_lead_created")
    @TableField(value = "kpi_crm_lead_created")
    @JSONField(name = "kpi_crm_lead_created")
    @JsonProperty("kpi_crm_lead_created")
    private Boolean kpiCrmLeadCreated;
    /**
     * 开放任务
     */
    @DEField(name = "kpi_project_task_opened")
    @TableField(value = "kpi_project_task_opened")
    @JSONField(name = "kpi_project_task_opened")
    @JsonProperty("kpi_project_task_opened")
    private Boolean kpiProjectTaskOpened;
    /**
     * 所有销售总价值KPI
     */
    @TableField(exist = false)
    @JSONField(name = "kpi_all_sale_total_value")
    @JsonProperty("kpi_all_sale_total_value")
    private BigDecimal kpiAllSaleTotalValue;
    /**
     * 已订阅
     */
    @TableField(exist = false)
    @JSONField(name = "is_subscribed")
    @JsonProperty("is_subscribed")
    private Boolean isSubscribed;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 项目任务开放价值的关键绩效指标
     */
    @TableField(exist = false)
    @JSONField(name = "kpi_project_task_opened_value")
    @JsonProperty("kpi_project_task_opened_value")
    private Integer kpiProjectTaskOpenedValue;
    /**
     * KPI账户总收入
     */
    @TableField(exist = false)
    @JSONField(name = "kpi_account_total_revenue_value")
    @JsonProperty("kpi_account_total_revenue_value")
    private BigDecimal kpiAccountTotalRevenueValue;
    /**
     * Kpi 邮件信息总计
     */
    @TableField(exist = false)
    @JSONField(name = "kpi_mail_message_total_value")
    @JsonProperty("kpi_mail_message_total_value")
    private Integer kpiMailMessageTotalValue;
    /**
     * 消息
     */
    @DEField(name = "kpi_mail_message_total")
    @TableField(value = "kpi_mail_message_total")
    @JSONField(name = "kpi_mail_message_total")
    @JsonProperty("kpi_mail_message_total")
    private Boolean kpiMailMessageTotal;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * EMail模板
     */
    @TableField(exist = false)
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    private String templateIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * EMail模板
     */
    @DEField(name = "template_id")
    @TableField(value = "template_id")
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    private Long templateId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooTemplate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [已签单商机]
     */
    public void setKpiCrmOpportunitiesWon(Boolean kpiCrmOpportunitiesWon){
        this.kpiCrmOpportunitiesWon = kpiCrmOpportunitiesWon ;
        this.modify("kpi_crm_opportunities_won",kpiCrmOpportunitiesWon);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [员工]
     */
    public void setKpiHrRecruitmentNewColleagues(Boolean kpiHrRecruitmentNewColleagues){
        this.kpiHrRecruitmentNewColleagues = kpiHrRecruitmentNewColleagues ;
        this.modify("kpi_hr_recruitment_new_colleagues",kpiHrRecruitmentNewColleagues);
    }

    /**
     * 设置 [POS 销售]
     */
    public void setKpiPosTotal(Boolean kpiPosTotal){
        this.kpiPosTotal = kpiPosTotal ;
        this.modify("kpi_pos_total",kpiPosTotal);
    }

    /**
     * 设置 [电商销售]
     */
    public void setKpiWebsiteSaleTotal(Boolean kpiWebsiteSaleTotal){
        this.kpiWebsiteSaleTotal = kpiWebsiteSaleTotal ;
        this.modify("kpi_website_sale_total",kpiWebsiteSaleTotal);
    }

    /**
     * 设置 [所有销售]
     */
    public void setKpiAllSaleTotal(Boolean kpiAllSaleTotal){
        this.kpiAllSaleTotal = kpiAllSaleTotal ;
        this.modify("kpi_all_sale_total",kpiAllSaleTotal);
    }

    /**
     * 设置 [下一发送日期]
     */
    public void setNextRunDate(Timestamp nextRunDate){
        this.nextRunDate = nextRunDate ;
        this.modify("next_run_date",nextRunDate);
    }

    /**
     * 格式化日期 [下一发送日期]
     */
    public String formatNextRunDate(){
        if (this.nextRunDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(nextRunDate);
    }
    /**
     * 设置 [已连接用户]
     */
    public void setKpiResUsersConnected(Boolean kpiResUsersConnected){
        this.kpiResUsersConnected = kpiResUsersConnected ;
        this.modify("kpi_res_users_connected",kpiResUsersConnected);
    }

    /**
     * 设置 [周期]
     */
    public void setPeriodicity(String periodicity){
        this.periodicity = periodicity ;
        this.modify("periodicity",periodicity);
    }

    /**
     * 设置 [收入]
     */
    public void setKpiAccountTotalRevenue(Boolean kpiAccountTotalRevenue){
        this.kpiAccountTotalRevenue = kpiAccountTotalRevenue ;
        this.modify("kpi_account_total_revenue",kpiAccountTotalRevenue);
    }

    /**
     * 设置 [新的线索/商机]
     */
    public void setKpiCrmLeadCreated(Boolean kpiCrmLeadCreated){
        this.kpiCrmLeadCreated = kpiCrmLeadCreated ;
        this.modify("kpi_crm_lead_created",kpiCrmLeadCreated);
    }

    /**
     * 设置 [开放任务]
     */
    public void setKpiProjectTaskOpened(Boolean kpiProjectTaskOpened){
        this.kpiProjectTaskOpened = kpiProjectTaskOpened ;
        this.modify("kpi_project_task_opened",kpiProjectTaskOpened);
    }

    /**
     * 设置 [消息]
     */
    public void setKpiMailMessageTotal(Boolean kpiMailMessageTotal){
        this.kpiMailMessageTotal = kpiMailMessageTotal ;
        this.modify("kpi_mail_message_total",kpiMailMessageTotal);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [EMail模板]
     */
    public void setTemplateId(Long templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


