package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_followers_mail_message_subtype_rel;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_followers_mail_message_subtype_relSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_followers_mail_message_subtype_rel] 服务对象接口
 */
public interface IMail_followers_mail_message_subtype_relService extends IService<Mail_followers_mail_message_subtype_rel>{

    boolean create(Mail_followers_mail_message_subtype_rel et) ;
    void createBatch(List<Mail_followers_mail_message_subtype_rel> list) ;
    boolean update(Mail_followers_mail_message_subtype_rel et) ;
    void updateBatch(List<Mail_followers_mail_message_subtype_rel> list) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    Mail_followers_mail_message_subtype_rel get(String key) ;
    Mail_followers_mail_message_subtype_rel getDraft(Mail_followers_mail_message_subtype_rel et) ;
    boolean checkKey(Mail_followers_mail_message_subtype_rel et) ;
    boolean save(Mail_followers_mail_message_subtype_rel et) ;
    void saveBatch(List<Mail_followers_mail_message_subtype_rel> list) ;
    Page<Mail_followers_mail_message_subtype_rel> searchDefault(Mail_followers_mail_message_subtype_relSearchContext context) ;
    List<Mail_followers_mail_message_subtype_rel> selectByMailFollowersId(Long id);
    void removeByMailFollowersId(Long id);
    List<Mail_followers_mail_message_subtype_rel> selectByMailMessageSubtypeId(Long id);
    void removeByMailMessageSubtypeId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


