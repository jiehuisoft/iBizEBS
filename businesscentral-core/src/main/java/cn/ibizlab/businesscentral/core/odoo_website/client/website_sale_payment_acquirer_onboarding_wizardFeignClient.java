package cn.ibizlab.businesscentral.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_sale_payment_acquirer_onboarding_wizardSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[website_sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-website:odoo-website}", contextId = "website-sale-payment-acquirer-onboarding-wizard", fallback = website_sale_payment_acquirer_onboarding_wizardFallback.class)
public interface website_sale_payment_acquirer_onboarding_wizardFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards")
    Website_sale_payment_acquirer_onboarding_wizard create(@RequestBody Website_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards/batch")
    Boolean createBatch(@RequestBody List<Website_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards);




    @RequestMapping(method = RequestMethod.GET, value = "/website_sale_payment_acquirer_onboarding_wizards/{id}")
    Website_sale_payment_acquirer_onboarding_wizard get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/website_sale_payment_acquirer_onboarding_wizards/{id}")
    Website_sale_payment_acquirer_onboarding_wizard update(@PathVariable("id") Long id,@RequestBody Website_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

    @RequestMapping(method = RequestMethod.PUT, value = "/website_sale_payment_acquirer_onboarding_wizards/batch")
    Boolean updateBatch(@RequestBody List<Website_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards);


    @RequestMapping(method = RequestMethod.DELETE, value = "/website_sale_payment_acquirer_onboarding_wizards/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/website_sale_payment_acquirer_onboarding_wizards/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards/search")
    Page<Website_sale_payment_acquirer_onboarding_wizard> search(@RequestBody Website_sale_payment_acquirer_onboarding_wizardSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/website_sale_payment_acquirer_onboarding_wizards/select")
    Page<Website_sale_payment_acquirer_onboarding_wizard> select();


    @RequestMapping(method = RequestMethod.GET, value = "/website_sale_payment_acquirer_onboarding_wizards/getdraft")
    Website_sale_payment_acquirer_onboarding_wizard getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards/checkkey")
    Boolean checkKey(@RequestBody Website_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);


    @RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards/save")
    Boolean save(@RequestBody Website_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards/savebatch")
    Boolean saveBatch(@RequestBody List<Website_sale_payment_acquirer_onboarding_wizard> website_sale_payment_acquirer_onboarding_wizards);



    @RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards/searchdefault")
    Page<Website_sale_payment_acquirer_onboarding_wizard> searchDefault(@RequestBody Website_sale_payment_acquirer_onboarding_wizardSearchContext context);


}
