package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_tax;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_taxSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_invoice_tax] 服务对象接口
 */
@Component
public class account_invoice_taxFallback implements account_invoice_taxFeignClient{

    public Page<Account_invoice_tax> search(Account_invoice_taxSearchContext context){
            return null;
     }





    public Account_invoice_tax update(Long id, Account_invoice_tax account_invoice_tax){
            return null;
     }
    public Boolean updateBatch(List<Account_invoice_tax> account_invoice_taxes){
            return false;
     }


    public Account_invoice_tax get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Account_invoice_tax create(Account_invoice_tax account_invoice_tax){
            return null;
     }
    public Boolean createBatch(List<Account_invoice_tax> account_invoice_taxes){
            return false;
     }

    public Page<Account_invoice_tax> select(){
            return null;
     }

    public Account_invoice_tax getDraft(){
            return null;
    }



    public Boolean checkKey(Account_invoice_tax account_invoice_tax){
            return false;
     }


    public Boolean save(Account_invoice_tax account_invoice_tax){
            return false;
     }
    public Boolean saveBatch(List<Account_invoice_tax> account_invoice_taxes){
            return false;
     }

    public Page<Account_invoice_tax> searchDefault(Account_invoice_taxSearchContext context){
            return null;
     }


}
