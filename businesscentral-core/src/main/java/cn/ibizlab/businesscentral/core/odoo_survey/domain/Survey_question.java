package cn.ibizlab.businesscentral.core.odoo_survey.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[调查问题]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SURVEY_QUESTION",resultMap = "Survey_questionResultMap")
public class Survey_question extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表格类型
     */
    @DEField(name = "matrix_subtype")
    @TableField(value = "matrix_subtype")
    @JSONField(name = "matrix_subtype")
    @JsonProperty("matrix_subtype")
    private String matrixSubtype;
    /**
     * 信息：验证错误
     */
    @DEField(name = "validation_error_msg")
    @TableField(value = "validation_error_msg")
    @JSONField(name = "validation_error_msg")
    @JsonProperty("validation_error_msg")
    private String validationErrorMsg;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 问题类型
     */
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 显示评论字段
     */
    @DEField(name = "comments_allowed")
    @TableField(value = "comments_allowed")
    @JSONField(name = "comments_allowed")
    @JsonProperty("comments_allowed")
    private Boolean commentsAllowed;
    /**
     * 最大日期
     */
    @DEField(name = "validation_max_date")
    @TableField(value = "validation_max_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validation_max_date" , format="yyyy-MM-dd")
    @JsonProperty("validation_max_date")
    private Timestamp validationMaxDate;
    /**
     * 表格行数
     */
    @TableField(exist = false)
    @JSONField(name = "labels_ids_2")
    @JsonProperty("labels_ids_2")
    private String labelsIds2;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最大值
     */
    @DEField(name = "validation_max_float_value")
    @TableField(value = "validation_max_float_value")
    @JSONField(name = "validation_max_float_value")
    @JsonProperty("validation_max_float_value")
    private Double validationMaxFloatValue;
    /**
     * 问题名称
     */
    @TableField(value = "question")
    @JSONField(name = "question")
    @JsonProperty("question")
    private String question;
    /**
     * 答案类型
     */
    @TableField(exist = false)
    @JSONField(name = "labels_ids")
    @JsonProperty("labels_ids")
    private String labelsIds;
    /**
     * 最小值
     */
    @DEField(name = "validation_min_float_value")
    @TableField(value = "validation_min_float_value")
    @JSONField(name = "validation_min_float_value")
    @JsonProperty("validation_min_float_value")
    private Double validationMinFloatValue;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 必答问题
     */
    @DEField(name = "constr_mandatory")
    @TableField(value = "constr_mandatory")
    @JSONField(name = "constr_mandatory")
    @JsonProperty("constr_mandatory")
    private Boolean constrMandatory;
    /**
     * 答案
     */
    @TableField(exist = false)
    @JSONField(name = "user_input_line_ids")
    @JsonProperty("user_input_line_ids")
    private String userInputLineIds;
    /**
     * 评论消息
     */
    @DEField(name = "comments_message")
    @TableField(value = "comments_message")
    @JSONField(name = "comments_message")
    @JsonProperty("comments_message")
    private String commentsMessage;
    /**
     * 评论字段是答案选项
     */
    @DEField(name = "comment_count_as_answer")
    @TableField(value = "comment_count_as_answer")
    @JSONField(name = "comment_count_as_answer")
    @JsonProperty("comment_count_as_answer")
    private Boolean commentCountAsAnswer;
    /**
     * 验证文本
     */
    @DEField(name = "validation_required")
    @TableField(value = "validation_required")
    @JSONField(name = "validation_required")
    @JsonProperty("validation_required")
    private Boolean validationRequired;
    /**
     * 输入必须是EMail
     */
    @DEField(name = "validation_email")
    @TableField(value = "validation_email")
    @JSONField(name = "validation_email")
    @JsonProperty("validation_email")
    private Boolean validationEmail;
    /**
     * 最小日期
     */
    @DEField(name = "validation_min_date")
    @TableField(value = "validation_min_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validation_min_date" , format="yyyy-MM-dd")
    @JsonProperty("validation_min_date")
    private Timestamp validationMinDate;
    /**
     * 错误消息
     */
    @DEField(name = "constr_error_msg")
    @TableField(value = "constr_error_msg")
    @JSONField(name = "constr_error_msg")
    @JsonProperty("constr_error_msg")
    private String constrErrorMsg;
    /**
     * 最大文本长度
     */
    @DEField(name = "validation_length_max")
    @TableField(value = "validation_length_max")
    @JSONField(name = "validation_length_max")
    @JsonProperty("validation_length_max")
    private Integer validationLengthMax;
    /**
     * 栏位数
     */
    @DEField(name = "column_nb")
    @TableField(value = "column_nb")
    @JSONField(name = "column_nb")
    @JsonProperty("column_nb")
    private String columnNb;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 显示模式
     */
    @DEField(name = "display_mode")
    @TableField(value = "display_mode")
    @JSONField(name = "display_mode")
    @JsonProperty("display_mode")
    private String displayMode;
    /**
     * 最小文本长度
     */
    @DEField(name = "validation_length_min")
    @TableField(value = "validation_length_min")
    @JSONField(name = "validation_length_min")
    @JsonProperty("validation_length_min")
    private Integer validationLengthMin;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 问卷
     */
    @TableField(exist = false)
    @JSONField(name = "survey_id")
    @JsonProperty("survey_id")
    private Long surveyId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 调查页面
     */
    @DEField(name = "page_id")
    @TableField(value = "page_id")
    @JSONField(name = "page_id")
    @JsonProperty("page_id")
    private Long pageId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_page odooPage;



    /**
     * 设置 [表格类型]
     */
    public void setMatrixSubtype(String matrixSubtype){
        this.matrixSubtype = matrixSubtype ;
        this.modify("matrix_subtype",matrixSubtype);
    }

    /**
     * 设置 [信息：验证错误]
     */
    public void setValidationErrorMsg(String validationErrorMsg){
        this.validationErrorMsg = validationErrorMsg ;
        this.modify("validation_error_msg",validationErrorMsg);
    }

    /**
     * 设置 [问题类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [显示评论字段]
     */
    public void setCommentsAllowed(Boolean commentsAllowed){
        this.commentsAllowed = commentsAllowed ;
        this.modify("comments_allowed",commentsAllowed);
    }

    /**
     * 设置 [最大日期]
     */
    public void setValidationMaxDate(Timestamp validationMaxDate){
        this.validationMaxDate = validationMaxDate ;
        this.modify("validation_max_date",validationMaxDate);
    }

    /**
     * 格式化日期 [最大日期]
     */
    public String formatValidationMaxDate(){
        if (this.validationMaxDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(validationMaxDate);
    }
    /**
     * 设置 [最大值]
     */
    public void setValidationMaxFloatValue(Double validationMaxFloatValue){
        this.validationMaxFloatValue = validationMaxFloatValue ;
        this.modify("validation_max_float_value",validationMaxFloatValue);
    }

    /**
     * 设置 [问题名称]
     */
    public void setQuestion(String question){
        this.question = question ;
        this.modify("question",question);
    }

    /**
     * 设置 [最小值]
     */
    public void setValidationMinFloatValue(Double validationMinFloatValue){
        this.validationMinFloatValue = validationMinFloatValue ;
        this.modify("validation_min_float_value",validationMinFloatValue);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [必答问题]
     */
    public void setConstrMandatory(Boolean constrMandatory){
        this.constrMandatory = constrMandatory ;
        this.modify("constr_mandatory",constrMandatory);
    }

    /**
     * 设置 [评论消息]
     */
    public void setCommentsMessage(String commentsMessage){
        this.commentsMessage = commentsMessage ;
        this.modify("comments_message",commentsMessage);
    }

    /**
     * 设置 [评论字段是答案选项]
     */
    public void setCommentCountAsAnswer(Boolean commentCountAsAnswer){
        this.commentCountAsAnswer = commentCountAsAnswer ;
        this.modify("comment_count_as_answer",commentCountAsAnswer);
    }

    /**
     * 设置 [验证文本]
     */
    public void setValidationRequired(Boolean validationRequired){
        this.validationRequired = validationRequired ;
        this.modify("validation_required",validationRequired);
    }

    /**
     * 设置 [输入必须是EMail]
     */
    public void setValidationEmail(Boolean validationEmail){
        this.validationEmail = validationEmail ;
        this.modify("validation_email",validationEmail);
    }

    /**
     * 设置 [最小日期]
     */
    public void setValidationMinDate(Timestamp validationMinDate){
        this.validationMinDate = validationMinDate ;
        this.modify("validation_min_date",validationMinDate);
    }

    /**
     * 格式化日期 [最小日期]
     */
    public String formatValidationMinDate(){
        if (this.validationMinDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(validationMinDate);
    }
    /**
     * 设置 [错误消息]
     */
    public void setConstrErrorMsg(String constrErrorMsg){
        this.constrErrorMsg = constrErrorMsg ;
        this.modify("constr_error_msg",constrErrorMsg);
    }

    /**
     * 设置 [最大文本长度]
     */
    public void setValidationLengthMax(Integer validationLengthMax){
        this.validationLengthMax = validationLengthMax ;
        this.modify("validation_length_max",validationLengthMax);
    }

    /**
     * 设置 [栏位数]
     */
    public void setColumnNb(String columnNb){
        this.columnNb = columnNb ;
        this.modify("column_nb",columnNb);
    }

    /**
     * 设置 [显示模式]
     */
    public void setDisplayMode(String displayMode){
        this.displayMode = displayMode ;
        this.modify("display_mode",displayMode);
    }

    /**
     * 设置 [最小文本长度]
     */
    public void setValidationLengthMin(Integer validationLengthMin){
        this.validationLengthMin = validationLengthMin ;
        this.modify("validation_length_min",validationLengthMin);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [调查页面]
     */
    public void setPageId(Long pageId){
        this.pageId = pageId ;
        this.modify("page_id",pageId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


