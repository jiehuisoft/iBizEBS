package cn.ibizlab.businesscentral.core.odoo_stock.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warehouseSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Stock_warehouseMapper extends BaseMapper<Stock_warehouse>{

    Page<Stock_warehouse> searchDefault(IPage page, @Param("srf") Stock_warehouseSearchContext context, @Param("ew") Wrapper<Stock_warehouse> wrapper) ;
    @Override
    Stock_warehouse selectById(Serializable id);
    @Override
    int insert(Stock_warehouse entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Stock_warehouse entity);
    @Override
    int update(@Param(Constants.ENTITY) Stock_warehouse entity, @Param("ew") Wrapper<Stock_warehouse> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Stock_warehouse> selectByCompanyId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByPartnerId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByCreateUid(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByWriteUid(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByCrossdockRouteId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByDeliveryRouteId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByPbmRouteId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByReceptionRouteId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByLotStockId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByPbmLocId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectBySamLocId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByViewLocationId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByWhInputStockLocId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByWhOutputStockLocId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByWhPackStockLocId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByWhQcStockLocId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByIntTypeId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByInTypeId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByManuTypeId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByOutTypeId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByPackTypeId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByPbmTypeId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByPickTypeId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectBySamTypeId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByBuyPullId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByManufacturePullId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByMtoPullId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectByPbmMtoPullId(@Param("id") Serializable id) ;

    List<Stock_warehouse> selectBySamRuleId(@Param("id") Serializable id) ;


}
