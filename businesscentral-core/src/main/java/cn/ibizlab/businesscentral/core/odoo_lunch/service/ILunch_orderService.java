package cn.ibizlab.businesscentral.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_order;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_orderSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Lunch_order] 服务对象接口
 */
public interface ILunch_orderService extends IService<Lunch_order>{

    boolean create(Lunch_order et) ;
    void createBatch(List<Lunch_order> list) ;
    boolean update(Lunch_order et) ;
    void updateBatch(List<Lunch_order> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Lunch_order get(Long key) ;
    Lunch_order getDraft(Lunch_order et) ;
    boolean checkKey(Lunch_order et) ;
    boolean save(Lunch_order et) ;
    void saveBatch(List<Lunch_order> list) ;
    Page<Lunch_order> searchDefault(Lunch_orderSearchContext context) ;
    List<Lunch_order> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Lunch_order> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Lunch_order> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Lunch_order> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Lunch_order> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Lunch_order> getLunchOrderByIds(List<Long> ids) ;
    List<Lunch_order> getLunchOrderByEntities(List<Lunch_order> entities) ;
}


