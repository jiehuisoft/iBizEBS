package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term_line;
/**
 * 关系型数据实体[Account_payment_term_line] 查询条件对象
 */
@Slf4j
@Data
public class Account_payment_term_lineSearchContext extends QueryWrapperContext<Account_payment_term_line> {

	private String n_value_eq;//[类型]
	public void setN_value_eq(String n_value_eq) {
        this.n_value_eq = n_value_eq;
        if(!ObjectUtils.isEmpty(this.n_value_eq)){
            this.getSearchCond().eq("value", n_value_eq);
        }
    }
	private String n_option_eq;//[选项]
	public void setN_option_eq(String n_option_eq) {
        this.n_option_eq = n_option_eq;
        if(!ObjectUtils.isEmpty(this.n_option_eq)){
            this.getSearchCond().eq("option", n_option_eq);
        }
    }
	private String n_payment_id_text_eq;//[付款条款]
	public void setN_payment_id_text_eq(String n_payment_id_text_eq) {
        this.n_payment_id_text_eq = n_payment_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_id_text_eq)){
            this.getSearchCond().eq("payment_id_text", n_payment_id_text_eq);
        }
    }
	private String n_payment_id_text_like;//[付款条款]
	public void setN_payment_id_text_like(String n_payment_id_text_like) {
        this.n_payment_id_text_like = n_payment_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_payment_id_text_like)){
            this.getSearchCond().like("payment_id_text", n_payment_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_payment_id_eq;//[付款条款]
	public void setN_payment_id_eq(Long n_payment_id_eq) {
        this.n_payment_id_eq = n_payment_id_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_id_eq)){
            this.getSearchCond().eq("payment_id", n_payment_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



