package cn.ibizlab.businesscentral.core.odoo_sale.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[销售分析报告]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SALE_REPORT",resultMap = "Sale_reportResultMap")
public class Sale_report extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 不含税总计
     */
    @DEField(name = "price_subtotal")
    @TableField(value = "price_subtotal")
    @JSONField(name = "price_subtotal")
    @JsonProperty("price_subtotal")
    private Double priceSubtotal;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 折扣 %
     */
    @TableField(value = "discount")
    @JSONField(name = "discount")
    @JsonProperty("discount")
    private Double discount;
    /**
     * 折扣金额
     */
    @DEField(name = "discount_amount")
    @TableField(value = "discount_amount")
    @JSONField(name = "discount_amount")
    @JsonProperty("discount_amount")
    private Double discountAmount;
    /**
     * 体积
     */
    @TableField(value = "volume")
    @JSONField(name = "volume")
    @JsonProperty("volume")
    private Double volume;
    /**
     * 总计
     */
    @DEField(name = "price_total")
    @TableField(value = "price_total")
    @JSONField(name = "price_total")
    @JsonProperty("price_total")
    private Double priceTotal;
    /**
     * 确认日期
     */
    @DEField(name = "confirmation_date")
    @TableField(value = "confirmation_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "confirmation_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("confirmation_date")
    private Timestamp confirmationDate;
    /**
     * 单据日期
     */
    @TableField(value = "date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 订单关联
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 待开票数量
     */
    @DEField(name = "qty_to_invoice")
    @TableField(value = "qty_to_invoice")
    @JSONField(name = "qty_to_invoice")
    @JsonProperty("qty_to_invoice")
    private Double qtyToInvoice;
    /**
     * # 明细行
     */
    @TableField(value = "nbr")
    @JSONField(name = "nbr")
    @JsonProperty("nbr")
    private Integer nbr;
    /**
     * 毛重
     */
    @TableField(value = "weight")
    @JSONField(name = "weight")
    @JsonProperty("weight")
    private Double weight;
    /**
     * 订购数量
     */
    @DEField(name = "product_uom_qty")
    @TableField(value = "product_uom_qty")
    @JSONField(name = "product_uom_qty")
    @JsonProperty("product_uom_qty")
    private Double productUomQty;
    /**
     * 不含税待开票金额
     */
    @DEField(name = "untaxed_amount_to_invoice")
    @TableField(value = "untaxed_amount_to_invoice")
    @JSONField(name = "untaxed_amount_to_invoice")
    @JsonProperty("untaxed_amount_to_invoice")
    private Double untaxedAmountToInvoice;
    /**
     * 不含税已开票金额
     */
    @DEField(name = "untaxed_amount_invoiced")
    @TableField(value = "untaxed_amount_invoiced")
    @JSONField(name = "untaxed_amount_invoiced")
    @JsonProperty("untaxed_amount_invoiced")
    private Double untaxedAmountInvoiced;
    /**
     * 网站
     */
    @DEField(name = "website_id")
    @TableField(value = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;
    /**
     * 已送货数量
     */
    @DEField(name = "qty_delivered")
    @TableField(value = "qty_delivered")
    @JSONField(name = "qty_delivered")
    @JsonProperty("qty_delivered")
    private Double qtyDelivered;
    /**
     * 已开票数量
     */
    @DEField(name = "qty_invoiced")
    @TableField(value = "qty_invoiced")
    @JSONField(name = "qty_invoiced")
    @JsonProperty("qty_invoiced")
    private Double qtyInvoiced;
    /**
     * 产品
     */
    @TableField(exist = false)
    @JSONField(name = "product_tmpl_id_text")
    @JsonProperty("product_tmpl_id_text")
    private String productTmplIdText;
    /**
     * 仓库
     */
    @TableField(exist = false)
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    private String warehouseIdText;
    /**
     * 分析账户
     */
    @TableField(exist = false)
    @JSONField(name = "analytic_account_id_text")
    @JsonProperty("analytic_account_id_text")
    private String analyticAccountIdText;
    /**
     * 产品变体
     */
    @TableField(exist = false)
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;
    /**
     * 客户国家
     */
    @TableField(exist = false)
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;
    /**
     * 产品种类
     */
    @TableField(exist = false)
    @JSONField(name = "categ_id_text")
    @JsonProperty("categ_id_text")
    private String categIdText;
    /**
     * 来源
     */
    @TableField(exist = false)
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    private String sourceIdText;
    /**
     * 营销
     */
    @TableField(exist = false)
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    private String campaignIdText;
    /**
     * 订单 #
     */
    @TableField(exist = false)
    @JSONField(name = "order_id_text")
    @JsonProperty("order_id_text")
    private String orderIdText;
    /**
     * 销售员
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 客户实体
     */
    @TableField(exist = false)
    @JSONField(name = "commercial_partner_id_text")
    @JsonProperty("commercial_partner_id_text")
    private String commercialPartnerIdText;
    /**
     * 媒体
     */
    @TableField(exist = false)
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;
    /**
     * 计量单位
     */
    @TableField(exist = false)
    @JSONField(name = "product_uom_text")
    @JsonProperty("product_uom_text")
    private String productUomText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 销售团队
     */
    @TableField(exist = false)
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    private String teamIdText;
    /**
     * 价格表
     */
    @TableField(exist = false)
    @JSONField(name = "pricelist_id_text")
    @JsonProperty("pricelist_id_text")
    private String pricelistIdText;
    /**
     * 客户
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 销售团队
     */
    @DEField(name = "team_id")
    @TableField(value = "team_id")
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Long teamId;
    /**
     * 客户实体
     */
    @DEField(name = "commercial_partner_id")
    @TableField(value = "commercial_partner_id")
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    private Long commercialPartnerId;
    /**
     * 销售员
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 产品
     */
    @DEField(name = "product_tmpl_id")
    @TableField(value = "product_tmpl_id")
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    private Long productTmplId;
    /**
     * 分析账户
     */
    @DEField(name = "analytic_account_id")
    @TableField(value = "analytic_account_id")
    @JSONField(name = "analytic_account_id")
    @JsonProperty("analytic_account_id")
    private Long analyticAccountId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 媒体
     */
    @DEField(name = "medium_id")
    @TableField(value = "medium_id")
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Long mediumId;
    /**
     * 订单 #
     */
    @DEField(name = "order_id")
    @TableField(value = "order_id")
    @JSONField(name = "order_id")
    @JsonProperty("order_id")
    private Long orderId;
    /**
     * 客户
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 来源
     */
    @DEField(name = "source_id")
    @TableField(value = "source_id")
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Long sourceId;
    /**
     * 营销
     */
    @DEField(name = "campaign_id")
    @TableField(value = "campaign_id")
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Long campaignId;
    /**
     * 产品种类
     */
    @DEField(name = "categ_id")
    @TableField(value = "categ_id")
    @JSONField(name = "categ_id")
    @JsonProperty("categ_id")
    private Long categId;
    /**
     * 仓库
     */
    @DEField(name = "warehouse_id")
    @TableField(value = "warehouse_id")
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Long warehouseId;
    /**
     * 产品变体
     */
    @DEField(name = "product_id")
    @TableField(value = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Long productId;
    /**
     * 价格表
     */
    @DEField(name = "pricelist_id")
    @TableField(value = "pricelist_id")
    @JSONField(name = "pricelist_id")
    @JsonProperty("pricelist_id")
    private Long pricelistId;
    /**
     * 计量单位
     */
    @DEField(name = "product_uom")
    @TableField(value = "product_uom")
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    private Long productUom;
    /**
     * 客户国家
     */
    @DEField(name = "country_id")
    @TableField(value = "country_id")
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Long countryId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAnalyticAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team odooTeam;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category odooCateg;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist odooPricelist;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template odooProductTmpl;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountry;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooCommercialPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order odooOrder;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooWarehouse;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign odooCampaign;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium odooMedium;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource;



    /**
     * 设置 [不含税总计]
     */
    public void setPriceSubtotal(Double priceSubtotal){
        this.priceSubtotal = priceSubtotal ;
        this.modify("price_subtotal",priceSubtotal);
    }

    /**
     * 设置 [折扣 %]
     */
    public void setDiscount(Double discount){
        this.discount = discount ;
        this.modify("discount",discount);
    }

    /**
     * 设置 [折扣金额]
     */
    public void setDiscountAmount(Double discountAmount){
        this.discountAmount = discountAmount ;
        this.modify("discount_amount",discountAmount);
    }

    /**
     * 设置 [体积]
     */
    public void setVolume(Double volume){
        this.volume = volume ;
        this.modify("volume",volume);
    }

    /**
     * 设置 [总计]
     */
    public void setPriceTotal(Double priceTotal){
        this.priceTotal = priceTotal ;
        this.modify("price_total",priceTotal);
    }

    /**
     * 设置 [确认日期]
     */
    public void setConfirmationDate(Timestamp confirmationDate){
        this.confirmationDate = confirmationDate ;
        this.modify("confirmation_date",confirmationDate);
    }

    /**
     * 格式化日期 [确认日期]
     */
    public String formatConfirmationDate(){
        if (this.confirmationDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(confirmationDate);
    }
    /**
     * 设置 [单据日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 格式化日期 [单据日期]
     */
    public String formatDate(){
        if (this.date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
    /**
     * 设置 [订单关联]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [待开票数量]
     */
    public void setQtyToInvoice(Double qtyToInvoice){
        this.qtyToInvoice = qtyToInvoice ;
        this.modify("qty_to_invoice",qtyToInvoice);
    }

    /**
     * 设置 [# 明细行]
     */
    public void setNbr(Integer nbr){
        this.nbr = nbr ;
        this.modify("nbr",nbr);
    }

    /**
     * 设置 [毛重]
     */
    public void setWeight(Double weight){
        this.weight = weight ;
        this.modify("weight",weight);
    }

    /**
     * 设置 [订购数量]
     */
    public void setProductUomQty(Double productUomQty){
        this.productUomQty = productUomQty ;
        this.modify("product_uom_qty",productUomQty);
    }

    /**
     * 设置 [不含税待开票金额]
     */
    public void setUntaxedAmountToInvoice(Double untaxedAmountToInvoice){
        this.untaxedAmountToInvoice = untaxedAmountToInvoice ;
        this.modify("untaxed_amount_to_invoice",untaxedAmountToInvoice);
    }

    /**
     * 设置 [不含税已开票金额]
     */
    public void setUntaxedAmountInvoiced(Double untaxedAmountInvoiced){
        this.untaxedAmountInvoiced = untaxedAmountInvoiced ;
        this.modify("untaxed_amount_invoiced",untaxedAmountInvoiced);
    }

    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [已送货数量]
     */
    public void setQtyDelivered(Double qtyDelivered){
        this.qtyDelivered = qtyDelivered ;
        this.modify("qty_delivered",qtyDelivered);
    }

    /**
     * 设置 [已开票数量]
     */
    public void setQtyInvoiced(Double qtyInvoiced){
        this.qtyInvoiced = qtyInvoiced ;
        this.modify("qty_invoiced",qtyInvoiced);
    }

    /**
     * 设置 [销售团队]
     */
    public void setTeamId(Long teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }

    /**
     * 设置 [客户实体]
     */
    public void setCommercialPartnerId(Long commercialPartnerId){
        this.commercialPartnerId = commercialPartnerId ;
        this.modify("commercial_partner_id",commercialPartnerId);
    }

    /**
     * 设置 [销售员]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [产品]
     */
    public void setProductTmplId(Long productTmplId){
        this.productTmplId = productTmplId ;
        this.modify("product_tmpl_id",productTmplId);
    }

    /**
     * 设置 [分析账户]
     */
    public void setAnalyticAccountId(Long analyticAccountId){
        this.analyticAccountId = analyticAccountId ;
        this.modify("analytic_account_id",analyticAccountId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [媒体]
     */
    public void setMediumId(Long mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }

    /**
     * 设置 [订单 #]
     */
    public void setOrderId(Long orderId){
        this.orderId = orderId ;
        this.modify("order_id",orderId);
    }

    /**
     * 设置 [客户]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [来源]
     */
    public void setSourceId(Long sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }

    /**
     * 设置 [营销]
     */
    public void setCampaignId(Long campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }

    /**
     * 设置 [产品种类]
     */
    public void setCategId(Long categId){
        this.categId = categId ;
        this.modify("categ_id",categId);
    }

    /**
     * 设置 [仓库]
     */
    public void setWarehouseId(Long warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }

    /**
     * 设置 [产品变体]
     */
    public void setProductId(Long productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [价格表]
     */
    public void setPricelistId(Long pricelistId){
        this.pricelistId = pricelistId ;
        this.modify("pricelist_id",pricelistId);
    }

    /**
     * 设置 [计量单位]
     */
    public void setProductUom(Long productUom){
        this.productUom = productUom ;
        this.modify("product_uom",productUom);
    }

    /**
     * 设置 [客户国家]
     */
    public void setCountryId(Long countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


