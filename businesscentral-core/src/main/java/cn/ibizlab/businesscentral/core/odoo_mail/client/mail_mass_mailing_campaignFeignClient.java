package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_campaign;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_campaignSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_mass_mailing_campaign] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-mass-mailing-campaign", fallback = mail_mass_mailing_campaignFallback.class)
public interface mail_mass_mailing_campaignFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_campaigns/{id}")
    Mail_mass_mailing_campaign update(@PathVariable("id") Long id,@RequestBody Mail_mass_mailing_campaign mail_mass_mailing_campaign);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_campaigns/batch")
    Boolean updateBatch(@RequestBody List<Mail_mass_mailing_campaign> mail_mass_mailing_campaigns);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_campaigns/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_campaigns/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_campaigns/{id}")
    Mail_mass_mailing_campaign get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns/search")
    Page<Mail_mass_mailing_campaign> search(@RequestBody Mail_mass_mailing_campaignSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns")
    Mail_mass_mailing_campaign create(@RequestBody Mail_mass_mailing_campaign mail_mass_mailing_campaign);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns/batch")
    Boolean createBatch(@RequestBody List<Mail_mass_mailing_campaign> mail_mass_mailing_campaigns);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_campaigns/select")
    Page<Mail_mass_mailing_campaign> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_campaigns/getdraft")
    Mail_mass_mailing_campaign getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns/checkkey")
    Boolean checkKey(@RequestBody Mail_mass_mailing_campaign mail_mass_mailing_campaign);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns/save")
    Boolean save(@RequestBody Mail_mass_mailing_campaign mail_mass_mailing_campaign);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_mass_mailing_campaign> mail_mass_mailing_campaigns);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns/searchdefault")
    Page<Mail_mass_mailing_campaign> searchDefault(@RequestBody Mail_mass_mailing_campaignSearchContext context);


}
