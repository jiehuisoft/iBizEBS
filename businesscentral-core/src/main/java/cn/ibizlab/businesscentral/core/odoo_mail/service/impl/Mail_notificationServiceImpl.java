package cn.ibizlab.businesscentral.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_notification;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_notificationSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_notificationService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mail.mapper.Mail_notificationMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[消息通知] 服务对象接口实现
 */
@Slf4j
@Service("Mail_notificationServiceImpl")
public class Mail_notificationServiceImpl extends EBSServiceImpl<Mail_notificationMapper, Mail_notification> implements IMail_notificationService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mailService mailMailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_messageService mailMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mail.notification" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mail_notification et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_notificationService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mail_notification> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mail_notification et) {
        Mail_notification old = new Mail_notification() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_notificationService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_notificationService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mail_notification> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mail_notification get(Long key) {
        Mail_notification et = getById(key);
        if(et==null){
            et=new Mail_notification();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mail_notification getDraft(Mail_notification et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mail_notification et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mail_notification et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mail_notification et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mail_notification> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mail_notification> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mail_notification> selectByMailId(Long id) {
        return baseMapper.selectByMailId(id);
    }
    @Override
    public void resetByMailId(Long id) {
        this.update(new UpdateWrapper<Mail_notification>().set("mail_id",null).eq("mail_id",id));
    }

    @Override
    public void resetByMailId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_notification>().set("mail_id",null).in("mail_id",ids));
    }

    @Override
    public void removeByMailId(Long id) {
        this.remove(new QueryWrapper<Mail_notification>().eq("mail_id",id));
    }

	@Override
    public List<Mail_notification> selectByMailMessageId(Long id) {
        return baseMapper.selectByMailMessageId(id);
    }
    @Override
    public void removeByMailMessageId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Mail_notification>().in("mail_message_id",ids));
    }

    @Override
    public void removeByMailMessageId(Long id) {
        this.remove(new QueryWrapper<Mail_notification>().eq("mail_message_id",id));
    }

	@Override
    public List<Mail_notification> selectByResPartnerId(Long id) {
        return baseMapper.selectByResPartnerId(id);
    }
    @Override
    public void removeByResPartnerId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Mail_notification>().in("res_partner_id",ids));
    }

    @Override
    public void removeByResPartnerId(Long id) {
        this.remove(new QueryWrapper<Mail_notification>().eq("res_partner_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mail_notification> searchDefault(Mail_notificationSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mail_notification> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mail_notification>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mail_notification et){
        //实体关系[DER1N_MAIL_NOTIFICATION__RES_PARTNER__RES_PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getResPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooResPartner=et.getOdooResPartner();
            if(ObjectUtils.isEmpty(odooResPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getResPartnerId());
                et.setOdooResPartner(majorEntity);
                odooResPartner=majorEntity;
            }
            et.setResPartnerIdText(odooResPartner.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }




}



