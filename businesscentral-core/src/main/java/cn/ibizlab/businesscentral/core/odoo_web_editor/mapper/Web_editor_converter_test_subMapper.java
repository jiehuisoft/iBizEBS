package cn.ibizlab.businesscentral.core.odoo_web_editor.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test_sub;
import cn.ibizlab.businesscentral.core.odoo_web_editor.filter.Web_editor_converter_test_subSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Web_editor_converter_test_subMapper extends BaseMapper<Web_editor_converter_test_sub>{

    Page<Web_editor_converter_test_sub> searchDefault(IPage page, @Param("srf") Web_editor_converter_test_subSearchContext context, @Param("ew") Wrapper<Web_editor_converter_test_sub> wrapper) ;
    @Override
    Web_editor_converter_test_sub selectById(Serializable id);
    @Override
    int insert(Web_editor_converter_test_sub entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Web_editor_converter_test_sub entity);
    @Override
    int update(@Param(Constants.ENTITY) Web_editor_converter_test_sub entity, @Param("ew") Wrapper<Web_editor_converter_test_sub> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Web_editor_converter_test_sub> selectByCreateUid(@Param("id") Serializable id) ;

    List<Web_editor_converter_test_sub> selectByWriteUid(@Param("id") Serializable id) ;


}
