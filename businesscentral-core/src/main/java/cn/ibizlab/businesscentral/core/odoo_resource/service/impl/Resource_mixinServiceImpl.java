package cn.ibizlab.businesscentral.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_mixin;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_mixinSearchContext;
import cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_mixinService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_resource.mapper.Resource_mixinMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[资源装饰] 服务对象接口实现
 */
@Slf4j
@Service("Resource_mixinServiceImpl")
public class Resource_mixinServiceImpl extends EBSServiceImpl<Resource_mixinMapper, Resource_mixin> implements IResource_mixinService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendarService resourceCalendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_resourceService resourceResourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "resource.mixin" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Resource_mixin et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IResource_mixinService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Resource_mixin> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Resource_mixin et) {
        Resource_mixin old = new Resource_mixin() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IResource_mixinService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IResource_mixinService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Resource_mixin> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Resource_mixin get(Long key) {
        Resource_mixin et = getById(key);
        if(et==null){
            et=new Resource_mixin();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Resource_mixin getDraft(Resource_mixin et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Resource_mixin et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Resource_mixin et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Resource_mixin et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Resource_mixin> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Resource_mixin> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Resource_mixin> selectByResourceCalendarId(Long id) {
        return baseMapper.selectByResourceCalendarId(id);
    }
    @Override
    public void resetByResourceCalendarId(Long id) {
        this.update(new UpdateWrapper<Resource_mixin>().set("resource_calendar_id",null).eq("resource_calendar_id",id));
    }

    @Override
    public void resetByResourceCalendarId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Resource_mixin>().set("resource_calendar_id",null).in("resource_calendar_id",ids));
    }

    @Override
    public void removeByResourceCalendarId(Long id) {
        this.remove(new QueryWrapper<Resource_mixin>().eq("resource_calendar_id",id));
    }

	@Override
    public List<Resource_mixin> selectByResourceId(Long id) {
        return baseMapper.selectByResourceId(id);
    }
    @Override
    public List<Resource_mixin> selectByResourceId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Resource_mixin>().in("id",ids));
    }

    @Override
    public void removeByResourceId(Long id) {
        this.remove(new QueryWrapper<Resource_mixin>().eq("resource_id",id));
    }

	@Override
    public List<Resource_mixin> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Resource_mixin>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Resource_mixin>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Resource_mixin>().eq("company_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Resource_mixin> searchDefault(Resource_mixinSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Resource_mixin> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Resource_mixin>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Resource_mixin et){
        //实体关系[DER1N_RESOURCE_MIXIN__RESOURCE_CALENDAR__RESOURCE_CALENDAR_ID]
        if(!ObjectUtils.isEmpty(et.getResourceCalendarId())){
            cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar odooResourceCalendar=et.getOdooResourceCalendar();
            if(ObjectUtils.isEmpty(odooResourceCalendar)){
                cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar majorEntity=resourceCalendarService.get(et.getResourceCalendarId());
                et.setOdooResourceCalendar(majorEntity);
                odooResourceCalendar=majorEntity;
            }
            et.setResourceCalendarIdText(odooResourceCalendar.getName());
        }
        //实体关系[DER1N_RESOURCE_MIXIN__RESOURCE_RESOURCE__RESOURCE_ID]
        if(!ObjectUtils.isEmpty(et.getResourceId())){
            cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_resource odooResource=et.getOdooResource();
            if(ObjectUtils.isEmpty(odooResource)){
                cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_resource majorEntity=resourceResourceService.get(et.getResourceId());
                et.setOdooResource(majorEntity);
                odooResource=majorEntity;
            }
            et.setTz(odooResource.getTz());
            et.setResourceIdText(odooResource.getName());
        }
        //实体关系[DER1N_RESOURCE_MIXIN__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }




}



