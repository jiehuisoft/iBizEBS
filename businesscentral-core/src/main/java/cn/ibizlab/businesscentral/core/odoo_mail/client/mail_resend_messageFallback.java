package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_resend_message;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_resend_messageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_resend_message] 服务对象接口
 */
@Component
public class mail_resend_messageFallback implements mail_resend_messageFeignClient{


    public Mail_resend_message update(Long id, Mail_resend_message mail_resend_message){
            return null;
     }
    public Boolean updateBatch(List<Mail_resend_message> mail_resend_messages){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Mail_resend_message get(Long id){
            return null;
     }



    public Page<Mail_resend_message> search(Mail_resend_messageSearchContext context){
            return null;
     }


    public Mail_resend_message create(Mail_resend_message mail_resend_message){
            return null;
     }
    public Boolean createBatch(List<Mail_resend_message> mail_resend_messages){
            return false;
     }

    public Page<Mail_resend_message> select(){
            return null;
     }

    public Mail_resend_message getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_resend_message mail_resend_message){
            return false;
     }


    public Boolean save(Mail_resend_message mail_resend_message){
            return false;
     }
    public Boolean saveBatch(List<Mail_resend_message> mail_resend_messages){
            return false;
     }

    public Page<Mail_resend_message> searchDefault(Mail_resend_messageSearchContext context){
            return null;
     }


}
