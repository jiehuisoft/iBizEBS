package cn.ibizlab.businesscentral.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type_mail;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_type_mailSearchContext;
import cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_type_mailService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_event.mapper.Event_type_mailMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[基于活动分类的邮件调度] 服务对象接口实现
 */
@Slf4j
@Service("Event_type_mailServiceImpl")
public class Event_type_mailServiceImpl extends EBSServiceImpl<Event_type_mailMapper, Event_type_mail> implements IEvent_type_mailService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_typeService eventTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_templateService mailTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "event.type.mail" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Event_type_mail et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_type_mailService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Event_type_mail> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Event_type_mail et) {
        Event_type_mail old = new Event_type_mail() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_type_mailService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_type_mailService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Event_type_mail> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Event_type_mail get(Long key) {
        Event_type_mail et = getById(key);
        if(et==null){
            et=new Event_type_mail();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Event_type_mail getDraft(Event_type_mail et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Event_type_mail et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Event_type_mail et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Event_type_mail et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Event_type_mail> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Event_type_mail> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Event_type_mail> selectByEventTypeId(Long id) {
        return baseMapper.selectByEventTypeId(id);
    }
    @Override
    public void removeByEventTypeId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Event_type_mail>().in("event_type_id",ids));
    }

    @Override
    public void removeByEventTypeId(Long id) {
        this.remove(new QueryWrapper<Event_type_mail>().eq("event_type_id",id));
    }

	@Override
    public List<Event_type_mail> selectByTemplateId(Long id) {
        return baseMapper.selectByTemplateId(id);
    }
    @Override
    public List<Event_type_mail> selectByTemplateId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Event_type_mail>().in("id",ids));
    }

    @Override
    public void removeByTemplateId(Long id) {
        this.remove(new QueryWrapper<Event_type_mail>().eq("template_id",id));
    }

	@Override
    public List<Event_type_mail> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Event_type_mail>().eq("create_uid",id));
    }

	@Override
    public List<Event_type_mail> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Event_type_mail>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Event_type_mail> searchDefault(Event_type_mailSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Event_type_mail> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Event_type_mail>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Event_type_mail et){
        //实体关系[DER1N_EVENT_TYPE_MAIL__EVENT_TYPE__EVENT_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getEventTypeId())){
            cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type odooEventType=et.getOdooEventType();
            if(ObjectUtils.isEmpty(odooEventType)){
                cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type majorEntity=eventTypeService.get(et.getEventTypeId());
                et.setOdooEventType(majorEntity);
                odooEventType=majorEntity;
            }
            et.setEventTypeIdText(odooEventType.getName());
        }
        //实体关系[DER1N_EVENT_TYPE_MAIL__MAIL_TEMPLATE__TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooTemplate=et.getOdooTemplate();
            if(ObjectUtils.isEmpty(odooTemplate)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template majorEntity=mailTemplateService.get(et.getTemplateId());
                et.setOdooTemplate(majorEntity);
                odooTemplate=majorEntity;
            }
            et.setTemplateIdText(odooTemplate.getName());
        }
        //实体关系[DER1N_EVENT_TYPE_MAIL__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_EVENT_TYPE_MAIL__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Event_type_mail> getEventTypeMailByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Event_type_mail> getEventTypeMailByEntities(List<Event_type_mail> entities) {
        List ids =new ArrayList();
        for(Event_type_mail entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



