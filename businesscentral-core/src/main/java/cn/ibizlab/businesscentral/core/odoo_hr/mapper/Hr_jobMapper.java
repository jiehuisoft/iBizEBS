package cn.ibizlab.businesscentral.core.odoo_hr.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_jobSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Hr_jobMapper extends BaseMapper<Hr_job>{

    Page<Hr_job> searchDefault(IPage page, @Param("srf") Hr_jobSearchContext context, @Param("ew") Wrapper<Hr_job> wrapper) ;
    Page<Hr_job> searchMaster(IPage page, @Param("srf") Hr_jobSearchContext context, @Param("ew") Wrapper<Hr_job> wrapper) ;
    @Override
    Hr_job selectById(Serializable id);
    @Override
    int insert(Hr_job entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Hr_job entity);
    @Override
    int update(@Param(Constants.ENTITY) Hr_job entity, @Param("ew") Wrapper<Hr_job> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Hr_job> selectBySurveyId(@Param("id") Serializable id) ;

    List<Hr_job> selectByDepartmentId(@Param("id") Serializable id) ;

    List<Hr_job> selectByManagerId(@Param("id") Serializable id) ;

    List<Hr_job> selectByAliasId(@Param("id") Serializable id) ;

    List<Hr_job> selectByCompanyId(@Param("id") Serializable id) ;

    List<Hr_job> selectByAddressId(@Param("id") Serializable id) ;

    List<Hr_job> selectByCreateUid(@Param("id") Serializable id) ;

    List<Hr_job> selectByHrResponsibleId(@Param("id") Serializable id) ;

    List<Hr_job> selectByUserId(@Param("id") Serializable id) ;

    List<Hr_job> selectByWriteUid(@Param("id") Serializable id) ;


}
