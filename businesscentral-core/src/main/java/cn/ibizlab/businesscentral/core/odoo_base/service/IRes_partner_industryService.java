package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_industry;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_industrySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_partner_industry] 服务对象接口
 */
public interface IRes_partner_industryService extends IService<Res_partner_industry>{

    boolean create(Res_partner_industry et) ;
    void createBatch(List<Res_partner_industry> list) ;
    boolean update(Res_partner_industry et) ;
    void updateBatch(List<Res_partner_industry> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_partner_industry get(Long key) ;
    Res_partner_industry getDraft(Res_partner_industry et) ;
    boolean checkKey(Res_partner_industry et) ;
    boolean save(Res_partner_industry et) ;
    void saveBatch(List<Res_partner_industry> list) ;
    Page<Res_partner_industry> searchDefault(Res_partner_industrySearchContext context) ;
    List<Res_partner_industry> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_partner_industry> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_partner_industry> getResPartnerIndustryByIds(List<Long> ids) ;
    List<Res_partner_industry> getResPartnerIndustryByEntities(List<Res_partner_industry> entities) ;
}


