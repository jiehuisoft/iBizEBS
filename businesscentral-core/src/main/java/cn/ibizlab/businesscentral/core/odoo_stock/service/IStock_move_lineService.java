package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move_line;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_move_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_move_line] 服务对象接口
 */
public interface IStock_move_lineService extends IService<Stock_move_line>{

    boolean create(Stock_move_line et) ;
    void createBatch(List<Stock_move_line> list) ;
    boolean update(Stock_move_line et) ;
    void updateBatch(List<Stock_move_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_move_line get(Long key) ;
    Stock_move_line getDraft(Stock_move_line et) ;
    boolean checkKey(Stock_move_line et) ;
    boolean save(Stock_move_line et) ;
    void saveBatch(List<Stock_move_line> list) ;
    Page<Stock_move_line> searchDefault(Stock_move_lineSearchContext context) ;
    List<Stock_move_line> selectByProductionId(Long id);
    void resetByProductionId(Long id);
    void resetByProductionId(Collection<Long> ids);
    void removeByProductionId(Long id);
    List<Stock_move_line> selectByWorkorderId(Long id);
    void resetByWorkorderId(Long id);
    void resetByWorkorderId(Collection<Long> ids);
    void removeByWorkorderId(Long id);
    List<Stock_move_line> selectByProductId(Long id);
    void removeByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Stock_move_line> selectByOwnerId(Long id);
    void resetByOwnerId(Long id);
    void resetByOwnerId(Collection<Long> ids);
    void removeByOwnerId(Long id);
    List<Stock_move_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_move_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_move_line> selectByLocationDestId(Long id);
    void resetByLocationDestId(Long id);
    void resetByLocationDestId(Collection<Long> ids);
    void removeByLocationDestId(Long id);
    List<Stock_move_line> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    List<Stock_move_line> selectByMoveId(Long id);
    void resetByMoveId(Long id);
    void resetByMoveId(Collection<Long> ids);
    void removeByMoveId(Long id);
    List<Stock_move_line> selectByPackageLevelId(Long id);
    void resetByPackageLevelId(Long id);
    void resetByPackageLevelId(Collection<Long> ids);
    void removeByPackageLevelId(Long id);
    List<Stock_move_line> selectByPickingId(Long id);
    void resetByPickingId(Long id);
    void resetByPickingId(Collection<Long> ids);
    void removeByPickingId(Long id);
    List<Stock_move_line> selectByLotId(Long id);
    void resetByLotId(Long id);
    void resetByLotId(Collection<Long> ids);
    void removeByLotId(Long id);
    List<Stock_move_line> selectByLotProducedId(Long id);
    void resetByLotProducedId(Long id);
    void resetByLotProducedId(Collection<Long> ids);
    void removeByLotProducedId(Long id);
    List<Stock_move_line> selectByPackageId(Long id);
    List<Stock_move_line> selectByPackageId(Collection<Long> ids);
    void removeByPackageId(Long id);
    List<Stock_move_line> selectByResultPackageId(Long id);
    List<Stock_move_line> selectByResultPackageId(Collection<Long> ids);
    void removeByResultPackageId(Long id);
    List<Stock_move_line> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_move_line> getStockMoveLineByIds(List<Long> ids) ;
    List<Stock_move_line> getStockMoveLineByEntities(List<Stock_move_line> entities) ;
}


