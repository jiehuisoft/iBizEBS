package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term_line;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_payment_term_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_payment_term_line] 服务对象接口
 */
@Component
public class account_payment_term_lineFallback implements account_payment_term_lineFeignClient{




    public Account_payment_term_line get(Long id){
            return null;
     }


    public Page<Account_payment_term_line> search(Account_payment_term_lineSearchContext context){
            return null;
     }


    public Account_payment_term_line create(Account_payment_term_line account_payment_term_line){
            return null;
     }
    public Boolean createBatch(List<Account_payment_term_line> account_payment_term_lines){
            return false;
     }

    public Account_payment_term_line update(Long id, Account_payment_term_line account_payment_term_line){
            return null;
     }
    public Boolean updateBatch(List<Account_payment_term_line> account_payment_term_lines){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Account_payment_term_line> select(){
            return null;
     }

    public Account_payment_term_line getDraft(){
            return null;
    }



    public Boolean checkKey(Account_payment_term_line account_payment_term_line){
            return false;
     }


    public Boolean save(Account_payment_term_line account_payment_term_line){
            return false;
     }
    public Boolean saveBatch(List<Account_payment_term_line> account_payment_term_lines){
            return false;
     }

    public Page<Account_payment_term_line> searchDefault(Account_payment_term_lineSearchContext context){
            return null;
     }


}
