package cn.ibizlab.businesscentral.core.odoo_base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company;
/**
 * 关系型数据实体[Res_company] 查询条件对象
 */
@Slf4j
@Data
public class Res_companySearchContext extends QueryWrapperContext<Res_company> {

	private String n_sale_quotation_onboarding_state_eq;//[正进行销售面板的状态]
	public void setN_sale_quotation_onboarding_state_eq(String n_sale_quotation_onboarding_state_eq) {
        this.n_sale_quotation_onboarding_state_eq = n_sale_quotation_onboarding_state_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_quotation_onboarding_state_eq)){
            this.getSearchCond().eq("sale_quotation_onboarding_state", n_sale_quotation_onboarding_state_eq);
        }
    }
	private String n_account_onboarding_invoice_layout_state_eq;//[有待被确认的发票步骤的状态]
	public void setN_account_onboarding_invoice_layout_state_eq(String n_account_onboarding_invoice_layout_state_eq) {
        this.n_account_onboarding_invoice_layout_state_eq = n_account_onboarding_invoice_layout_state_eq;
        if(!ObjectUtils.isEmpty(this.n_account_onboarding_invoice_layout_state_eq)){
            this.getSearchCond().eq("account_onboarding_invoice_layout_state", n_account_onboarding_invoice_layout_state_eq);
        }
    }
	private String n_account_dashboard_onboarding_state_eq;//[处于会计面板的状态]
	public void setN_account_dashboard_onboarding_state_eq(String n_account_dashboard_onboarding_state_eq) {
        this.n_account_dashboard_onboarding_state_eq = n_account_dashboard_onboarding_state_eq;
        if(!ObjectUtils.isEmpty(this.n_account_dashboard_onboarding_state_eq)){
            this.getSearchCond().eq("account_dashboard_onboarding_state", n_account_dashboard_onboarding_state_eq);
        }
    }
	private String n_account_setup_coa_state_eq;//[科目状态]
	public void setN_account_setup_coa_state_eq(String n_account_setup_coa_state_eq) {
        this.n_account_setup_coa_state_eq = n_account_setup_coa_state_eq;
        if(!ObjectUtils.isEmpty(this.n_account_setup_coa_state_eq)){
            this.getSearchCond().eq("account_setup_coa_state", n_account_setup_coa_state_eq);
        }
    }
	private String n_account_setup_bank_data_state_eq;//[有待被确认的银行数据步骤的状态]
	public void setN_account_setup_bank_data_state_eq(String n_account_setup_bank_data_state_eq) {
        this.n_account_setup_bank_data_state_eq = n_account_setup_bank_data_state_eq;
        if(!ObjectUtils.isEmpty(this.n_account_setup_bank_data_state_eq)){
            this.getSearchCond().eq("account_setup_bank_data_state", n_account_setup_bank_data_state_eq);
        }
    }
	private String n_account_invoice_onboarding_state_eq;//[处于会计发票面板的状态]
	public void setN_account_invoice_onboarding_state_eq(String n_account_invoice_onboarding_state_eq) {
        this.n_account_invoice_onboarding_state_eq = n_account_invoice_onboarding_state_eq;
        if(!ObjectUtils.isEmpty(this.n_account_invoice_onboarding_state_eq)){
            this.getSearchCond().eq("account_invoice_onboarding_state", n_account_invoice_onboarding_state_eq);
        }
    }
	private String n_payment_acquirer_onboarding_state_eq;//[入职支付收单机构的状态]
	public void setN_payment_acquirer_onboarding_state_eq(String n_payment_acquirer_onboarding_state_eq) {
        this.n_payment_acquirer_onboarding_state_eq = n_payment_acquirer_onboarding_state_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_acquirer_onboarding_state_eq)){
            this.getSearchCond().eq("payment_acquirer_onboarding_state", n_payment_acquirer_onboarding_state_eq);
        }
    }
	private String n_payment_onboarding_payment_method_eq;//[选择付款方式]
	public void setN_payment_onboarding_payment_method_eq(String n_payment_onboarding_payment_method_eq) {
        this.n_payment_onboarding_payment_method_eq = n_payment_onboarding_payment_method_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_onboarding_payment_method_eq)){
            this.getSearchCond().eq("payment_onboarding_payment_method", n_payment_onboarding_payment_method_eq);
        }
    }
	private String n_po_double_validation_eq;//[批准等级]
	public void setN_po_double_validation_eq(String n_po_double_validation_eq) {
        this.n_po_double_validation_eq = n_po_double_validation_eq;
        if(!ObjectUtils.isEmpty(this.n_po_double_validation_eq)){
            this.getSearchCond().eq("po_double_validation", n_po_double_validation_eq);
        }
    }
	private String n_sale_onboarding_sample_quotation_state_eq;//[有待被确认的样品报价单步骤的状态]
	public void setN_sale_onboarding_sample_quotation_state_eq(String n_sale_onboarding_sample_quotation_state_eq) {
        this.n_sale_onboarding_sample_quotation_state_eq = n_sale_onboarding_sample_quotation_state_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_onboarding_sample_quotation_state_eq)){
            this.getSearchCond().eq("sale_onboarding_sample_quotation_state", n_sale_onboarding_sample_quotation_state_eq);
        }
    }
	private String n_sale_onboarding_order_confirmation_state_eq;//[有待被确认的订单步骤的状态]
	public void setN_sale_onboarding_order_confirmation_state_eq(String n_sale_onboarding_order_confirmation_state_eq) {
        this.n_sale_onboarding_order_confirmation_state_eq = n_sale_onboarding_order_confirmation_state_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_onboarding_order_confirmation_state_eq)){
            this.getSearchCond().eq("sale_onboarding_order_confirmation_state", n_sale_onboarding_order_confirmation_state_eq);
        }
    }
	private String n_sale_onboarding_payment_method_eq;//[请选择付款方式]
	public void setN_sale_onboarding_payment_method_eq(String n_sale_onboarding_payment_method_eq) {
        this.n_sale_onboarding_payment_method_eq = n_sale_onboarding_payment_method_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_onboarding_payment_method_eq)){
            this.getSearchCond().eq("sale_onboarding_payment_method", n_sale_onboarding_payment_method_eq);
        }
    }
	private String n_account_onboarding_sample_invoice_state_eq;//[有待被确认的样品报价单步骤的状态]
	public void setN_account_onboarding_sample_invoice_state_eq(String n_account_onboarding_sample_invoice_state_eq) {
        this.n_account_onboarding_sample_invoice_state_eq = n_account_onboarding_sample_invoice_state_eq;
        if(!ObjectUtils.isEmpty(this.n_account_onboarding_sample_invoice_state_eq)){
            this.getSearchCond().eq("account_onboarding_sample_invoice_state", n_account_onboarding_sample_invoice_state_eq);
        }
    }
	private String n_base_onboarding_company_state_eq;//[公司状态]
	public void setN_base_onboarding_company_state_eq(String n_base_onboarding_company_state_eq) {
        this.n_base_onboarding_company_state_eq = n_base_onboarding_company_state_eq;
        if(!ObjectUtils.isEmpty(this.n_base_onboarding_company_state_eq)){
            this.getSearchCond().eq("base_onboarding_company_state", n_base_onboarding_company_state_eq);
        }
    }
	private String n_po_lock_eq;//[销售订单修改]
	public void setN_po_lock_eq(String n_po_lock_eq) {
        this.n_po_lock_eq = n_po_lock_eq;
        if(!ObjectUtils.isEmpty(this.n_po_lock_eq)){
            this.getSearchCond().eq("po_lock", n_po_lock_eq);
        }
    }
	private String n_account_setup_fy_data_state_eq;//[有待被确认的会计年度步骤的状态]
	public void setN_account_setup_fy_data_state_eq(String n_account_setup_fy_data_state_eq) {
        this.n_account_setup_fy_data_state_eq = n_account_setup_fy_data_state_eq;
        if(!ObjectUtils.isEmpty(this.n_account_setup_fy_data_state_eq)){
            this.getSearchCond().eq("account_setup_fy_data_state", n_account_setup_fy_data_state_eq);
        }
    }
	private String n_tax_calculation_rounding_method_eq;//[税率计算的舍入方法]
	public void setN_tax_calculation_rounding_method_eq(String n_tax_calculation_rounding_method_eq) {
        this.n_tax_calculation_rounding_method_eq = n_tax_calculation_rounding_method_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_calculation_rounding_method_eq)){
            this.getSearchCond().eq("tax_calculation_rounding_method", n_tax_calculation_rounding_method_eq);
        }
    }
	private String n_account_onboarding_sale_tax_state_eq;//[有待被确认的报价单步骤的状态]
	public void setN_account_onboarding_sale_tax_state_eq(String n_account_onboarding_sale_tax_state_eq) {
        this.n_account_onboarding_sale_tax_state_eq = n_account_onboarding_sale_tax_state_eq;
        if(!ObjectUtils.isEmpty(this.n_account_onboarding_sale_tax_state_eq)){
            this.getSearchCond().eq("account_onboarding_sale_tax_state", n_account_onboarding_sale_tax_state_eq);
        }
    }
	private String n_website_sale_onboarding_payment_acquirer_state_eq;//[网站销售状态入职付款收单机构步骤]
	public void setN_website_sale_onboarding_payment_acquirer_state_eq(String n_website_sale_onboarding_payment_acquirer_state_eq) {
        this.n_website_sale_onboarding_payment_acquirer_state_eq = n_website_sale_onboarding_payment_acquirer_state_eq;
        if(!ObjectUtils.isEmpty(this.n_website_sale_onboarding_payment_acquirer_state_eq)){
            this.getSearchCond().eq("website_sale_onboarding_payment_acquirer_state", n_website_sale_onboarding_payment_acquirer_state_eq);
        }
    }
	private String n_fiscalyear_last_month_eq;//[会计年度的最后一个月]
	public void setN_fiscalyear_last_month_eq(String n_fiscalyear_last_month_eq) {
        this.n_fiscalyear_last_month_eq = n_fiscalyear_last_month_eq;
        if(!ObjectUtils.isEmpty(this.n_fiscalyear_last_month_eq)){
            this.getSearchCond().eq("fiscalyear_last_month", n_fiscalyear_last_month_eq);
        }
    }
	private String n_property_stock_account_input_categ_id_text_eq;//[库存计价的入库科目]
	public void setN_property_stock_account_input_categ_id_text_eq(String n_property_stock_account_input_categ_id_text_eq) {
        this.n_property_stock_account_input_categ_id_text_eq = n_property_stock_account_input_categ_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_account_input_categ_id_text_eq)){
            this.getSearchCond().eq("property_stock_account_input_categ_id_text", n_property_stock_account_input_categ_id_text_eq);
        }
    }
	private String n_property_stock_account_input_categ_id_text_like;//[库存计价的入库科目]
	public void setN_property_stock_account_input_categ_id_text_like(String n_property_stock_account_input_categ_id_text_like) {
        this.n_property_stock_account_input_categ_id_text_like = n_property_stock_account_input_categ_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_property_stock_account_input_categ_id_text_like)){
            this.getSearchCond().like("property_stock_account_input_categ_id_text", n_property_stock_account_input_categ_id_text_like);
        }
    }
	private String n_account_purchase_tax_id_text_eq;//[默认进项税]
	public void setN_account_purchase_tax_id_text_eq(String n_account_purchase_tax_id_text_eq) {
        this.n_account_purchase_tax_id_text_eq = n_account_purchase_tax_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_purchase_tax_id_text_eq)){
            this.getSearchCond().eq("account_purchase_tax_id_text", n_account_purchase_tax_id_text_eq);
        }
    }
	private String n_account_purchase_tax_id_text_like;//[默认进项税]
	public void setN_account_purchase_tax_id_text_like(String n_account_purchase_tax_id_text_like) {
        this.n_account_purchase_tax_id_text_like = n_account_purchase_tax_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_purchase_tax_id_text_like)){
            this.getSearchCond().like("account_purchase_tax_id_text", n_account_purchase_tax_id_text_like);
        }
    }
	private String n_name_eq;//[公司名称]
	public void setN_name_eq(String n_name_eq) {
        this.n_name_eq = n_name_eq;
        if(!ObjectUtils.isEmpty(this.n_name_eq)){
            this.getSearchCond().eq("name", n_name_eq);
        }
    }
	private String n_name_like;//[公司名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_incoterm_id_text_eq;//[默认国际贸易术语]
	public void setN_incoterm_id_text_eq(String n_incoterm_id_text_eq) {
        this.n_incoterm_id_text_eq = n_incoterm_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_incoterm_id_text_eq)){
            this.getSearchCond().eq("incoterm_id_text", n_incoterm_id_text_eq);
        }
    }
	private String n_incoterm_id_text_like;//[默认国际贸易术语]
	public void setN_incoterm_id_text_like(String n_incoterm_id_text_like) {
        this.n_incoterm_id_text_like = n_incoterm_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_incoterm_id_text_like)){
            this.getSearchCond().like("incoterm_id_text", n_incoterm_id_text_like);
        }
    }
	private String n_transfer_account_id_text_eq;//[银行间转账科目]
	public void setN_transfer_account_id_text_eq(String n_transfer_account_id_text_eq) {
        this.n_transfer_account_id_text_eq = n_transfer_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_transfer_account_id_text_eq)){
            this.getSearchCond().eq("transfer_account_id_text", n_transfer_account_id_text_eq);
        }
    }
	private String n_transfer_account_id_text_like;//[银行间转账科目]
	public void setN_transfer_account_id_text_like(String n_transfer_account_id_text_like) {
        this.n_transfer_account_id_text_like = n_transfer_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_transfer_account_id_text_like)){
            this.getSearchCond().like("transfer_account_id_text", n_transfer_account_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_chart_template_id_text_eq;//[表模板]
	public void setN_chart_template_id_text_eq(String n_chart_template_id_text_eq) {
        this.n_chart_template_id_text_eq = n_chart_template_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_chart_template_id_text_eq)){
            this.getSearchCond().eq("chart_template_id_text", n_chart_template_id_text_eq);
        }
    }
	private String n_chart_template_id_text_like;//[表模板]
	public void setN_chart_template_id_text_like(String n_chart_template_id_text_like) {
        this.n_chart_template_id_text_like = n_chart_template_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_chart_template_id_text_like)){
            this.getSearchCond().like("chart_template_id_text", n_chart_template_id_text_like);
        }
    }
	private String n_account_sale_tax_id_text_eq;//[默认销售税]
	public void setN_account_sale_tax_id_text_eq(String n_account_sale_tax_id_text_eq) {
        this.n_account_sale_tax_id_text_eq = n_account_sale_tax_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_sale_tax_id_text_eq)){
            this.getSearchCond().eq("account_sale_tax_id_text", n_account_sale_tax_id_text_eq);
        }
    }
	private String n_account_sale_tax_id_text_like;//[默认销售税]
	public void setN_account_sale_tax_id_text_like(String n_account_sale_tax_id_text_like) {
        this.n_account_sale_tax_id_text_like = n_account_sale_tax_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_sale_tax_id_text_like)){
            this.getSearchCond().like("account_sale_tax_id_text", n_account_sale_tax_id_text_like);
        }
    }
	private String n_account_opening_move_id_text_eq;//[期初日记账分录]
	public void setN_account_opening_move_id_text_eq(String n_account_opening_move_id_text_eq) {
        this.n_account_opening_move_id_text_eq = n_account_opening_move_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_opening_move_id_text_eq)){
            this.getSearchCond().eq("account_opening_move_id_text", n_account_opening_move_id_text_eq);
        }
    }
	private String n_account_opening_move_id_text_like;//[期初日记账分录]
	public void setN_account_opening_move_id_text_like(String n_account_opening_move_id_text_like) {
        this.n_account_opening_move_id_text_like = n_account_opening_move_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_opening_move_id_text_like)){
            this.getSearchCond().like("account_opening_move_id_text", n_account_opening_move_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_property_stock_account_output_categ_id_text_eq;//[库存计价的出货科目]
	public void setN_property_stock_account_output_categ_id_text_eq(String n_property_stock_account_output_categ_id_text_eq) {
        this.n_property_stock_account_output_categ_id_text_eq = n_property_stock_account_output_categ_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_account_output_categ_id_text_eq)){
            this.getSearchCond().eq("property_stock_account_output_categ_id_text", n_property_stock_account_output_categ_id_text_eq);
        }
    }
	private String n_property_stock_account_output_categ_id_text_like;//[库存计价的出货科目]
	public void setN_property_stock_account_output_categ_id_text_like(String n_property_stock_account_output_categ_id_text_like) {
        this.n_property_stock_account_output_categ_id_text_like = n_property_stock_account_output_categ_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_property_stock_account_output_categ_id_text_like)){
            this.getSearchCond().like("property_stock_account_output_categ_id_text", n_property_stock_account_output_categ_id_text_like);
        }
    }
	private String n_property_stock_valuation_account_id_text_eq;//[库存计价的科目模板]
	public void setN_property_stock_valuation_account_id_text_eq(String n_property_stock_valuation_account_id_text_eq) {
        this.n_property_stock_valuation_account_id_text_eq = n_property_stock_valuation_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_valuation_account_id_text_eq)){
            this.getSearchCond().eq("property_stock_valuation_account_id_text", n_property_stock_valuation_account_id_text_eq);
        }
    }
	private String n_property_stock_valuation_account_id_text_like;//[库存计价的科目模板]
	public void setN_property_stock_valuation_account_id_text_like(String n_property_stock_valuation_account_id_text_like) {
        this.n_property_stock_valuation_account_id_text_like = n_property_stock_valuation_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_property_stock_valuation_account_id_text_like)){
            this.getSearchCond().like("property_stock_valuation_account_id_text", n_property_stock_valuation_account_id_text_like);
        }
    }
	private String n_parent_id_text_eq;//[上级公司]
	public void setN_parent_id_text_eq(String n_parent_id_text_eq) {
        this.n_parent_id_text_eq = n_parent_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_eq)){
            this.getSearchCond().eq("parent_id_text", n_parent_id_text_eq);
        }
    }
	private String n_parent_id_text_like;//[上级公司]
	public void setN_parent_id_text_like(String n_parent_id_text_like) {
        this.n_parent_id_text_like = n_parent_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_like)){
            this.getSearchCond().like("parent_id_text", n_parent_id_text_like);
        }
    }
	private String n_tax_cash_basis_journal_id_text_eq;//[现金收付制日记账]
	public void setN_tax_cash_basis_journal_id_text_eq(String n_tax_cash_basis_journal_id_text_eq) {
        this.n_tax_cash_basis_journal_id_text_eq = n_tax_cash_basis_journal_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_cash_basis_journal_id_text_eq)){
            this.getSearchCond().eq("tax_cash_basis_journal_id_text", n_tax_cash_basis_journal_id_text_eq);
        }
    }
	private String n_tax_cash_basis_journal_id_text_like;//[现金收付制日记账]
	public void setN_tax_cash_basis_journal_id_text_like(String n_tax_cash_basis_journal_id_text_like) {
        this.n_tax_cash_basis_journal_id_text_like = n_tax_cash_basis_journal_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_tax_cash_basis_journal_id_text_like)){
            this.getSearchCond().like("tax_cash_basis_journal_id_text", n_tax_cash_basis_journal_id_text_like);
        }
    }
	private String n_internal_transit_location_id_text_eq;//[内部中转位置]
	public void setN_internal_transit_location_id_text_eq(String n_internal_transit_location_id_text_eq) {
        this.n_internal_transit_location_id_text_eq = n_internal_transit_location_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_internal_transit_location_id_text_eq)){
            this.getSearchCond().eq("internal_transit_location_id_text", n_internal_transit_location_id_text_eq);
        }
    }
	private String n_internal_transit_location_id_text_like;//[内部中转位置]
	public void setN_internal_transit_location_id_text_like(String n_internal_transit_location_id_text_like) {
        this.n_internal_transit_location_id_text_like = n_internal_transit_location_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_internal_transit_location_id_text_like)){
            this.getSearchCond().like("internal_transit_location_id_text", n_internal_transit_location_id_text_like);
        }
    }
	private String n_resource_calendar_id_text_eq;//[默认工作时间]
	public void setN_resource_calendar_id_text_eq(String n_resource_calendar_id_text_eq) {
        this.n_resource_calendar_id_text_eq = n_resource_calendar_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_calendar_id_text_eq)){
            this.getSearchCond().eq("resource_calendar_id_text", n_resource_calendar_id_text_eq);
        }
    }
	private String n_resource_calendar_id_text_like;//[默认工作时间]
	public void setN_resource_calendar_id_text_like(String n_resource_calendar_id_text_like) {
        this.n_resource_calendar_id_text_like = n_resource_calendar_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_resource_calendar_id_text_like)){
            this.getSearchCond().like("resource_calendar_id_text", n_resource_calendar_id_text_like);
        }
    }
	private String n_currency_exchange_journal_id_text_eq;//[汇兑损益]
	public void setN_currency_exchange_journal_id_text_eq(String n_currency_exchange_journal_id_text_eq) {
        this.n_currency_exchange_journal_id_text_eq = n_currency_exchange_journal_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_exchange_journal_id_text_eq)){
            this.getSearchCond().eq("currency_exchange_journal_id_text", n_currency_exchange_journal_id_text_eq);
        }
    }
	private String n_currency_exchange_journal_id_text_like;//[汇兑损益]
	public void setN_currency_exchange_journal_id_text_like(String n_currency_exchange_journal_id_text_like) {
        this.n_currency_exchange_journal_id_text_like = n_currency_exchange_journal_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_exchange_journal_id_text_like)){
            this.getSearchCond().like("currency_exchange_journal_id_text", n_currency_exchange_journal_id_text_like);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_parent_id_eq;//[上级公司]
	public void setN_parent_id_eq(Long n_parent_id_eq) {
        this.n_parent_id_eq = n_parent_id_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_eq)){
            this.getSearchCond().eq("parent_id", n_parent_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_property_stock_account_output_categ_id_eq;//[库存计价的出货科目]
	public void setN_property_stock_account_output_categ_id_eq(Long n_property_stock_account_output_categ_id_eq) {
        this.n_property_stock_account_output_categ_id_eq = n_property_stock_account_output_categ_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_account_output_categ_id_eq)){
            this.getSearchCond().eq("property_stock_account_output_categ_id", n_property_stock_account_output_categ_id_eq);
        }
    }
	private Long n_property_stock_valuation_account_id_eq;//[库存计价的科目模板]
	public void setN_property_stock_valuation_account_id_eq(Long n_property_stock_valuation_account_id_eq) {
        this.n_property_stock_valuation_account_id_eq = n_property_stock_valuation_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_valuation_account_id_eq)){
            this.getSearchCond().eq("property_stock_valuation_account_id", n_property_stock_valuation_account_id_eq);
        }
    }
	private Long n_account_opening_move_id_eq;//[期初日记账分录]
	public void setN_account_opening_move_id_eq(Long n_account_opening_move_id_eq) {
        this.n_account_opening_move_id_eq = n_account_opening_move_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_opening_move_id_eq)){
            this.getSearchCond().eq("account_opening_move_id", n_account_opening_move_id_eq);
        }
    }
	private Long n_internal_transit_location_id_eq;//[内部中转位置]
	public void setN_internal_transit_location_id_eq(Long n_internal_transit_location_id_eq) {
        this.n_internal_transit_location_id_eq = n_internal_transit_location_id_eq;
        if(!ObjectUtils.isEmpty(this.n_internal_transit_location_id_eq)){
            this.getSearchCond().eq("internal_transit_location_id", n_internal_transit_location_id_eq);
        }
    }
	private Long n_account_purchase_tax_id_eq;//[默认进项税]
	public void setN_account_purchase_tax_id_eq(Long n_account_purchase_tax_id_eq) {
        this.n_account_purchase_tax_id_eq = n_account_purchase_tax_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_purchase_tax_id_eq)){
            this.getSearchCond().eq("account_purchase_tax_id", n_account_purchase_tax_id_eq);
        }
    }
	private Long n_chart_template_id_eq;//[表模板]
	public void setN_chart_template_id_eq(Long n_chart_template_id_eq) {
        this.n_chart_template_id_eq = n_chart_template_id_eq;
        if(!ObjectUtils.isEmpty(this.n_chart_template_id_eq)){
            this.getSearchCond().eq("chart_template_id", n_chart_template_id_eq);
        }
    }
	private Long n_account_sale_tax_id_eq;//[默认销售税]
	public void setN_account_sale_tax_id_eq(Long n_account_sale_tax_id_eq) {
        this.n_account_sale_tax_id_eq = n_account_sale_tax_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_sale_tax_id_eq)){
            this.getSearchCond().eq("account_sale_tax_id", n_account_sale_tax_id_eq);
        }
    }
	private Long n_tax_cash_basis_journal_id_eq;//[现金收付制日记账]
	public void setN_tax_cash_basis_journal_id_eq(Long n_tax_cash_basis_journal_id_eq) {
        this.n_tax_cash_basis_journal_id_eq = n_tax_cash_basis_journal_id_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_cash_basis_journal_id_eq)){
            this.getSearchCond().eq("tax_cash_basis_journal_id", n_tax_cash_basis_journal_id_eq);
        }
    }
	private Long n_property_stock_account_input_categ_id_eq;//[库存计价的入库科目]
	public void setN_property_stock_account_input_categ_id_eq(Long n_property_stock_account_input_categ_id_eq) {
        this.n_property_stock_account_input_categ_id_eq = n_property_stock_account_input_categ_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_account_input_categ_id_eq)){
            this.getSearchCond().eq("property_stock_account_input_categ_id", n_property_stock_account_input_categ_id_eq);
        }
    }
	private Long n_partner_id_eq;//[业务伙伴]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_incoterm_id_eq;//[默认国际贸易术语]
	public void setN_incoterm_id_eq(Long n_incoterm_id_eq) {
        this.n_incoterm_id_eq = n_incoterm_id_eq;
        if(!ObjectUtils.isEmpty(this.n_incoterm_id_eq)){
            this.getSearchCond().eq("incoterm_id", n_incoterm_id_eq);
        }
    }
	private Long n_resource_calendar_id_eq;//[默认工作时间]
	public void setN_resource_calendar_id_eq(Long n_resource_calendar_id_eq) {
        this.n_resource_calendar_id_eq = n_resource_calendar_id_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_calendar_id_eq)){
            this.getSearchCond().eq("resource_calendar_id", n_resource_calendar_id_eq);
        }
    }
	private Long n_transfer_account_id_eq;//[银行间转账科目]
	public void setN_transfer_account_id_eq(Long n_transfer_account_id_eq) {
        this.n_transfer_account_id_eq = n_transfer_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_transfer_account_id_eq)){
            this.getSearchCond().eq("transfer_account_id", n_transfer_account_id_eq);
        }
    }
	private Long n_currency_exchange_journal_id_eq;//[汇兑损益]
	public void setN_currency_exchange_journal_id_eq(Long n_currency_exchange_journal_id_eq) {
        this.n_currency_exchange_journal_id_eq = n_currency_exchange_journal_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_exchange_journal_id_eq)){
            this.getSearchCond().eq("currency_exchange_journal_id", n_currency_exchange_journal_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



