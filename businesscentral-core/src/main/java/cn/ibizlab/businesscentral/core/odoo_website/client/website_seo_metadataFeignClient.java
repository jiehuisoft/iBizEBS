package cn.ibizlab.businesscentral.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_seo_metadata;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_seo_metadataSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[website_seo_metadata] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-website:odoo-website}", contextId = "website-seo-metadata", fallback = website_seo_metadataFallback.class)
public interface website_seo_metadataFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata/search")
    Page<Website_seo_metadata> search(@RequestBody Website_seo_metadataSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata")
    Website_seo_metadata create(@RequestBody Website_seo_metadata website_seo_metadata);

    @RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata/batch")
    Boolean createBatch(@RequestBody List<Website_seo_metadata> website_seo_metadata);


    @RequestMapping(method = RequestMethod.GET, value = "/website_seo_metadata/{id}")
    Website_seo_metadata get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.PUT, value = "/website_seo_metadata/{id}")
    Website_seo_metadata update(@PathVariable("id") Long id,@RequestBody Website_seo_metadata website_seo_metadata);

    @RequestMapping(method = RequestMethod.PUT, value = "/website_seo_metadata/batch")
    Boolean updateBatch(@RequestBody List<Website_seo_metadata> website_seo_metadata);


    @RequestMapping(method = RequestMethod.DELETE, value = "/website_seo_metadata/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/website_seo_metadata/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/website_seo_metadata/select")
    Page<Website_seo_metadata> select();


    @RequestMapping(method = RequestMethod.GET, value = "/website_seo_metadata/getdraft")
    Website_seo_metadata getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata/checkkey")
    Boolean checkKey(@RequestBody Website_seo_metadata website_seo_metadata);


    @RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata/save")
    Boolean save(@RequestBody Website_seo_metadata website_seo_metadata);

    @RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata/savebatch")
    Boolean saveBatch(@RequestBody List<Website_seo_metadata> website_seo_metadata);



    @RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata/searchdefault")
    Page<Website_seo_metadata> searchDefault(@RequestBody Website_seo_metadataSearchContext context);


}
