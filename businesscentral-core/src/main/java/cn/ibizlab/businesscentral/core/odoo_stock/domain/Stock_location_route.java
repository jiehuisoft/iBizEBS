package cn.ibizlab.businesscentral.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[库存路线]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "STOCK_LOCATION_ROUTE",resultMap = "Stock_location_routeResultMap")
public class Stock_location_route extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 在销售订单行上可选
     */
    @DEField(name = "sale_selectable")
    @TableField(value = "sale_selectable")
    @JSONField(name = "sale_selectable")
    @JsonProperty("sale_selectable")
    private Boolean saleSelectable;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 产品
     */
    @TableField(exist = false)
    @JSONField(name = "product_ids")
    @JsonProperty("product_ids")
    private String productIds;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 路线
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 产品类别
     */
    @TableField(exist = false)
    @JSONField(name = "categ_ids")
    @JsonProperty("categ_ids")
    private String categIds;
    /**
     * 可应用于仓库
     */
    @DEField(name = "warehouse_selectable")
    @TableField(value = "warehouse_selectable")
    @JSONField(name = "warehouse_selectable")
    @JsonProperty("warehouse_selectable")
    private Boolean warehouseSelectable;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 规则
     */
    @TableField(exist = false)
    @JSONField(name = "rule_ids")
    @JsonProperty("rule_ids")
    private String ruleIds;
    /**
     * 可应用于产品
     */
    @DEField(name = "product_selectable")
    @TableField(value = "product_selectable")
    @JSONField(name = "product_selectable")
    @JsonProperty("product_selectable")
    private Boolean productSelectable;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 可应用于产品类别
     */
    @DEField(name = "product_categ_selectable")
    @TableField(value = "product_categ_selectable")
    @JSONField(name = "product_categ_selectable")
    @JsonProperty("product_categ_selectable")
    private Boolean productCategSelectable;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 仓库
     */
    @TableField(exist = false)
    @JSONField(name = "warehouse_ids")
    @JsonProperty("warehouse_ids")
    private String warehouseIds;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 供应的仓库
     */
    @TableField(exist = false)
    @JSONField(name = "supplied_wh_id_text")
    @JsonProperty("supplied_wh_id_text")
    private String suppliedWhIdText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 供应仓库
     */
    @TableField(exist = false)
    @JSONField(name = "supplier_wh_id_text")
    @JsonProperty("supplier_wh_id_text")
    private String supplierWhIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 供应仓库
     */
    @DEField(name = "supplier_wh_id")
    @TableField(value = "supplier_wh_id")
    @JSONField(name = "supplier_wh_id")
    @JsonProperty("supplier_wh_id")
    private Long supplierWhId;
    /**
     * 供应的仓库
     */
    @DEField(name = "supplied_wh_id")
    @TableField(value = "supplied_wh_id")
    @JSONField(name = "supplied_wh_id")
    @JsonProperty("supplied_wh_id")
    private Long suppliedWhId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooSuppliedWh;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooSupplierWh;



    /**
     * 设置 [在销售订单行上可选]
     */
    public void setSaleSelectable(Boolean saleSelectable){
        this.saleSelectable = saleSelectable ;
        this.modify("sale_selectable",saleSelectable);
    }

    /**
     * 设置 [路线]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [可应用于仓库]
     */
    public void setWarehouseSelectable(Boolean warehouseSelectable){
        this.warehouseSelectable = warehouseSelectable ;
        this.modify("warehouse_selectable",warehouseSelectable);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [可应用于产品]
     */
    public void setProductSelectable(Boolean productSelectable){
        this.productSelectable = productSelectable ;
        this.modify("product_selectable",productSelectable);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [可应用于产品类别]
     */
    public void setProductCategSelectable(Boolean productCategSelectable){
        this.productCategSelectable = productCategSelectable ;
        this.modify("product_categ_selectable",productCategSelectable);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [供应仓库]
     */
    public void setSupplierWhId(Long supplierWhId){
        this.supplierWhId = supplierWhId ;
        this.modify("supplier_wh_id",supplierWhId);
    }

    /**
     * 设置 [供应的仓库]
     */
    public void setSuppliedWhId(Long suppliedWhId){
        this.suppliedWhId = suppliedWhId ;
        this.modify("supplied_wh_id",suppliedWhId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


