package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconciliation_widget;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_reconciliation_widgetSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_reconciliation_widget] 服务对象接口
 */
public interface IAccount_reconciliation_widgetService extends IService<Account_reconciliation_widget>{

    boolean create(Account_reconciliation_widget et) ;
    void createBatch(List<Account_reconciliation_widget> list) ;
    boolean update(Account_reconciliation_widget et) ;
    void updateBatch(List<Account_reconciliation_widget> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_reconciliation_widget get(Long key) ;
    Account_reconciliation_widget getDraft(Account_reconciliation_widget et) ;
    boolean checkKey(Account_reconciliation_widget et) ;
    boolean save(Account_reconciliation_widget et) ;
    void saveBatch(List<Account_reconciliation_widget> list) ;
    Page<Account_reconciliation_widget> searchDefault(Account_reconciliation_widgetSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


