package cn.ibizlab.businesscentral.core.odoo_uom.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom;
import cn.ibizlab.businesscentral.core.odoo_uom.filter.Uom_uomSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Uom_uomMapper extends BaseMapper<Uom_uom>{

    Page<Uom_uom> searchDefault(IPage page, @Param("srf") Uom_uomSearchContext context, @Param("ew") Wrapper<Uom_uom> wrapper) ;
    @Override
    Uom_uom selectById(Serializable id);
    @Override
    int insert(Uom_uom entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Uom_uom entity);
    @Override
    int update(@Param(Constants.ENTITY) Uom_uom entity, @Param("ew") Wrapper<Uom_uom> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Uom_uom> selectByCreateUid(@Param("id") Serializable id) ;

    List<Uom_uom> selectByWriteUid(@Param("id") Serializable id) ;

    List<Uom_uom> selectByCategoryId(@Param("id") Serializable id) ;


}
