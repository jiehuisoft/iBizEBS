package cn.ibizlab.businesscentral.core.odoo_purchase.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition_type;
/**
 * 关系型数据实体[Purchase_requisition_type] 查询条件对象
 */
@Slf4j
@Data
public class Purchase_requisition_typeSearchContext extends QueryWrapperContext<Purchase_requisition_type> {

	private String n_name_like;//[申请类型]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_exclusive_eq;//[供应商选择]
	public void setN_exclusive_eq(String n_exclusive_eq) {
        this.n_exclusive_eq = n_exclusive_eq;
        if(!ObjectUtils.isEmpty(this.n_exclusive_eq)){
            this.getSearchCond().eq("exclusive", n_exclusive_eq);
        }
    }
	private String n_quantity_copy_eq;//[设置数量方式]
	public void setN_quantity_copy_eq(String n_quantity_copy_eq) {
        this.n_quantity_copy_eq = n_quantity_copy_eq;
        if(!ObjectUtils.isEmpty(this.n_quantity_copy_eq)){
            this.getSearchCond().eq("quantity_copy", n_quantity_copy_eq);
        }
    }
	private String n_line_copy_eq;//[明细行]
	public void setN_line_copy_eq(String n_line_copy_eq) {
        this.n_line_copy_eq = n_line_copy_eq;
        if(!ObjectUtils.isEmpty(this.n_line_copy_eq)){
            this.getSearchCond().eq("line_copy", n_line_copy_eq);
        }
    }
	private String n_create_uname_eq;//[创建人]
	public void setN_create_uname_eq(String n_create_uname_eq) {
        this.n_create_uname_eq = n_create_uname_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uname_eq)){
            this.getSearchCond().eq("create_uname", n_create_uname_eq);
        }
    }
	private String n_create_uname_like;//[创建人]
	public void setN_create_uname_like(String n_create_uname_like) {
        this.n_create_uname_like = n_create_uname_like;
        if(!ObjectUtils.isEmpty(this.n_create_uname_like)){
            this.getSearchCond().like("create_uname", n_create_uname_like);
        }
    }
	private String n_write_uname_eq;//[最后更新人]
	public void setN_write_uname_eq(String n_write_uname_eq) {
        this.n_write_uname_eq = n_write_uname_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uname_eq)){
            this.getSearchCond().eq("write_uname", n_write_uname_eq);
        }
    }
	private String n_write_uname_like;//[最后更新人]
	public void setN_write_uname_like(String n_write_uname_like) {
        this.n_write_uname_like = n_write_uname_like;
        if(!ObjectUtils.isEmpty(this.n_write_uname_like)){
            this.getSearchCond().like("write_uname", n_write_uname_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



