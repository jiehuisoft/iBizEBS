package cn.ibizlab.businesscentral.core.odoo_im_livechat.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[实时聊天支持频道报告]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "IM_LIVECHAT_REPORT_CHANNEL",resultMap = "Im_livechat_report_channelResultMap")
public class Im_livechat_report_channel extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 平均时间
     */
    @TableField(value = "duration")
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Double duration;
    /**
     * 会话的开始日期
     */
    @DEField(name = "start_date")
    @TableField(value = "start_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("start_date")
    private Timestamp startDate;
    /**
     * 会话开始的小时数
     */
    @DEField(name = "start_date_hour")
    @TableField(value = "start_date_hour")
    @JSONField(name = "start_date_hour")
    @JsonProperty("start_date_hour")
    private String startDateHour;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 渠道名称
     */
    @DEField(name = "channel_name")
    @TableField(value = "channel_name")
    @JSONField(name = "channel_name")
    @JsonProperty("channel_name")
    private String channelName;
    /**
     * 每个消息
     */
    @DEField(name = "nbr_message")
    @TableField(value = "nbr_message")
    @JSONField(name = "nbr_message")
    @JsonProperty("nbr_message")
    private Integer nbrMessage;
    /**
     * UUID
     */
    @TableField(value = "uuid")
    @JSONField(name = "uuid")
    @JsonProperty("uuid")
    private String uuid;
    /**
     * 代号
     */
    @DEField(name = "technical_name")
    @TableField(value = "technical_name")
    @JSONField(name = "technical_name")
    @JsonProperty("technical_name")
    private String technicalName;
    /**
     * 讲解人
     */
    @DEField(name = "nbr_speaker")
    @TableField(value = "nbr_speaker")
    @JSONField(name = "nbr_speaker")
    @JsonProperty("nbr_speaker")
    private Integer nbrSpeaker;
    /**
     * 渠道
     */
    @TableField(exist = false)
    @JSONField(name = "livechat_channel_id_text")
    @JsonProperty("livechat_channel_id_text")
    private String livechatChannelIdText;
    /**
     * 运算符
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 对话
     */
    @TableField(exist = false)
    @JSONField(name = "channel_id_text")
    @JsonProperty("channel_id_text")
    private String channelIdText;
    /**
     * 对话
     */
    @DEField(name = "channel_id")
    @TableField(value = "channel_id")
    @JSONField(name = "channel_id")
    @JsonProperty("channel_id")
    private Long channelId;
    /**
     * 渠道
     */
    @DEField(name = "livechat_channel_id")
    @TableField(value = "livechat_channel_id")
    @JSONField(name = "livechat_channel_id")
    @JsonProperty("livechat_channel_id")
    private Long livechatChannelId;
    /**
     * 运算符
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel odooLivechatChannel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel odooChannel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;



    /**
     * 设置 [平均时间]
     */
    public void setDuration(Double duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }

    /**
     * 设置 [会话的开始日期]
     */
    public void setStartDate(Timestamp startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 格式化日期 [会话的开始日期]
     */
    public String formatStartDate(){
        if (this.startDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(startDate);
    }
    /**
     * 设置 [会话开始的小时数]
     */
    public void setStartDateHour(String startDateHour){
        this.startDateHour = startDateHour ;
        this.modify("start_date_hour",startDateHour);
    }

    /**
     * 设置 [渠道名称]
     */
    public void setChannelName(String channelName){
        this.channelName = channelName ;
        this.modify("channel_name",channelName);
    }

    /**
     * 设置 [每个消息]
     */
    public void setNbrMessage(Integer nbrMessage){
        this.nbrMessage = nbrMessage ;
        this.modify("nbr_message",nbrMessage);
    }

    /**
     * 设置 [UUID]
     */
    public void setUuid(String uuid){
        this.uuid = uuid ;
        this.modify("uuid",uuid);
    }

    /**
     * 设置 [代号]
     */
    public void setTechnicalName(String technicalName){
        this.technicalName = technicalName ;
        this.modify("technical_name",technicalName);
    }

    /**
     * 设置 [讲解人]
     */
    public void setNbrSpeaker(Integer nbrSpeaker){
        this.nbrSpeaker = nbrSpeaker ;
        this.modify("nbr_speaker",nbrSpeaker);
    }

    /**
     * 设置 [对话]
     */
    public void setChannelId(Long channelId){
        this.channelId = channelId ;
        this.modify("channel_id",channelId);
    }

    /**
     * 设置 [渠道]
     */
    public void setLivechatChannelId(Long livechatChannelId){
        this.livechatChannelId = livechatChannelId ;
        this.modify("livechat_channel_id",livechatChannelId);
    }

    /**
     * 设置 [运算符]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


