package cn.ibizlab.businesscentral.core.extensions.service;

import cn.ibizlab.businesscentral.core.odoo_mail.service.impl.Mail_activityServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[活动] 自定义服务对象
 */
@Slf4j
@Primary
@Service("Mail_activityExService")
public class Mail_activityExService extends Mail_activityServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[Action_done]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Mail_activity action_done(Mail_activity et) {
        return super.action_done(et);
    }
}

