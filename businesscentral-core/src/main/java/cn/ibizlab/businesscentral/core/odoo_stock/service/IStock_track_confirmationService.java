package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_track_confirmation;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_track_confirmationSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_track_confirmation] 服务对象接口
 */
public interface IStock_track_confirmationService extends IService<Stock_track_confirmation>{

    boolean create(Stock_track_confirmation et) ;
    void createBatch(List<Stock_track_confirmation> list) ;
    boolean update(Stock_track_confirmation et) ;
    void updateBatch(List<Stock_track_confirmation> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_track_confirmation get(Long key) ;
    Stock_track_confirmation getDraft(Stock_track_confirmation et) ;
    boolean checkKey(Stock_track_confirmation et) ;
    boolean save(Stock_track_confirmation et) ;
    void saveBatch(List<Stock_track_confirmation> list) ;
    Page<Stock_track_confirmation> searchDefault(Stock_track_confirmationSearchContext context) ;
    List<Stock_track_confirmation> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_track_confirmation> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_track_confirmation> selectByInventoryId(Long id);
    void resetByInventoryId(Long id);
    void resetByInventoryId(Collection<Long> ids);
    void removeByInventoryId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_track_confirmation> getStockTrackConfirmationByIds(List<Long> ids) ;
    List<Stock_track_confirmation> getStockTrackConfirmationByEntities(List<Stock_track_confirmation> entities) ;
}


