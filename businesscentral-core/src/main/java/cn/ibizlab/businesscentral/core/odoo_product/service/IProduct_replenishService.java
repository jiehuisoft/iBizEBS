package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_replenish;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_replenishSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_replenish] 服务对象接口
 */
public interface IProduct_replenishService extends IService<Product_replenish>{

    boolean create(Product_replenish et) ;
    void createBatch(List<Product_replenish> list) ;
    boolean update(Product_replenish et) ;
    void updateBatch(List<Product_replenish> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_replenish get(Long key) ;
    Product_replenish getDraft(Product_replenish et) ;
    boolean checkKey(Product_replenish et) ;
    boolean save(Product_replenish et) ;
    void saveBatch(List<Product_replenish> list) ;
    Page<Product_replenish> searchDefault(Product_replenishSearchContext context) ;
    List<Product_replenish> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Product_replenish> selectByProductTmplId(Long id);
    void resetByProductTmplId(Long id);
    void resetByProductTmplId(Collection<Long> ids);
    void removeByProductTmplId(Long id);
    List<Product_replenish> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_replenish> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Product_replenish> selectByWarehouseId(Long id);
    void resetByWarehouseId(Long id);
    void resetByWarehouseId(Collection<Long> ids);
    void removeByWarehouseId(Long id);
    List<Product_replenish> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_replenish> getProductReplenishByIds(List<Long> ids) ;
    List<Product_replenish> getProductReplenishByEntities(List<Product_replenish> entities) ;
}


