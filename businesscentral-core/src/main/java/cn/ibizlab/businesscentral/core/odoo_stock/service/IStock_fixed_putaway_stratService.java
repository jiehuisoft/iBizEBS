package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_fixed_putaway_strat;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_fixed_putaway_stratSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_fixed_putaway_strat] 服务对象接口
 */
public interface IStock_fixed_putaway_stratService extends IService<Stock_fixed_putaway_strat>{

    boolean create(Stock_fixed_putaway_strat et) ;
    void createBatch(List<Stock_fixed_putaway_strat> list) ;
    boolean update(Stock_fixed_putaway_strat et) ;
    void updateBatch(List<Stock_fixed_putaway_strat> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_fixed_putaway_strat get(Long key) ;
    Stock_fixed_putaway_strat getDraft(Stock_fixed_putaway_strat et) ;
    boolean checkKey(Stock_fixed_putaway_strat et) ;
    boolean save(Stock_fixed_putaway_strat et) ;
    void saveBatch(List<Stock_fixed_putaway_strat> list) ;
    Page<Stock_fixed_putaway_strat> searchDefault(Stock_fixed_putaway_stratSearchContext context) ;
    List<Stock_fixed_putaway_strat> selectByCategoryId(Long id);
    void resetByCategoryId(Long id);
    void resetByCategoryId(Collection<Long> ids);
    void removeByCategoryId(Long id);
    List<Stock_fixed_putaway_strat> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Stock_fixed_putaway_strat> selectByPutawayId(Long id);
    void resetByPutawayId(Long id);
    void resetByPutawayId(Collection<Long> ids);
    void removeByPutawayId(Long id);
    List<Stock_fixed_putaway_strat> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_fixed_putaway_strat> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_fixed_putaway_strat> selectByFixedLocationId(Long id);
    void resetByFixedLocationId(Long id);
    void resetByFixedLocationId(Collection<Long> ids);
    void removeByFixedLocationId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_fixed_putaway_strat> getStockFixedPutawayStratByIds(List<Long> ids) ;
    List<Stock_fixed_putaway_strat> getStockFixedPutawayStratByEntities(List<Stock_fixed_putaway_strat> entities) ;
}


