package cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.IPurchase_requisition_lineproduct_changeLogic;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition_line;

/**
 * 关系型数据实体[product_change] 对象
 */
@Slf4j
@Service
public class Purchase_requisition_lineproduct_changeLogicImpl implements IPurchase_requisition_lineproduct_changeLogic{

    @Autowired
    private KieContainer kieContainer;


    @Autowired
    private cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisition_lineService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisition_lineService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Purchase_requisition_line et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("purchase_requisition_lineproduct_changedefault",et);
           kieSession.setGlobal("iBzSysPurchase_requisition_lineDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.purchase_requisition_lineproduct_change");

        }catch(Exception e){
            throw new RuntimeException("执行[产品更新]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
