package cn.ibizlab.businesscentral.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_char_noreadonly;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_char_noreadonlySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_char_noreadonly] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base-import:odoo-base-import}", contextId = "base-import-tests-models-char-noreadonly", fallback = base_import_tests_models_char_noreadonlyFallback.class)
public interface base_import_tests_models_char_noreadonlyFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_noreadonlies/{id}")
    Base_import_tests_models_char_noreadonly update(@PathVariable("id") Long id,@RequestBody Base_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_noreadonlies/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_char_noreadonly> base_import_tests_models_char_noreadonlies);





    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_noreadonlies/{id}")
    Base_import_tests_models_char_noreadonly get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_noreadonlies")
    Base_import_tests_models_char_noreadonly create(@RequestBody Base_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_noreadonlies/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_char_noreadonly> base_import_tests_models_char_noreadonlies);


    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_noreadonlies/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_noreadonlies/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_noreadonlies/search")
    Page<Base_import_tests_models_char_noreadonly> search(@RequestBody Base_import_tests_models_char_noreadonlySearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_noreadonlies/select")
    Page<Base_import_tests_models_char_noreadonly> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_noreadonlies/getdraft")
    Base_import_tests_models_char_noreadonly getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_noreadonlies/checkkey")
    Boolean checkKey(@RequestBody Base_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly);


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_noreadonlies/save")
    Boolean save(@RequestBody Base_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_noreadonlies/savebatch")
    Boolean saveBatch(@RequestBody List<Base_import_tests_models_char_noreadonly> base_import_tests_models_char_noreadonlies);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_noreadonlies/searchdefault")
    Page<Base_import_tests_models_char_noreadonly> searchDefault(@RequestBody Base_import_tests_models_char_noreadonlySearchContext context);


}
