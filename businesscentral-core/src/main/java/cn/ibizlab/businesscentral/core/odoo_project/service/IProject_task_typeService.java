package cn.ibizlab.businesscentral.core.odoo_project.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_task_type;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_task_typeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Project_task_type] 服务对象接口
 */
public interface IProject_task_typeService extends IService<Project_task_type>{

    boolean create(Project_task_type et) ;
    void createBatch(List<Project_task_type> list) ;
    boolean update(Project_task_type et) ;
    void updateBatch(List<Project_task_type> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Project_task_type get(Long key) ;
    Project_task_type getDraft(Project_task_type et) ;
    boolean checkKey(Project_task_type et) ;
    boolean save(Project_task_type et) ;
    void saveBatch(List<Project_task_type> list) ;
    Page<Project_task_type> searchDefault(Project_task_typeSearchContext context) ;
    List<Project_task_type> selectByMailTemplateId(Long id);
    void resetByMailTemplateId(Long id);
    void resetByMailTemplateId(Collection<Long> ids);
    void removeByMailTemplateId(Long id);
    List<Project_task_type> selectByRatingTemplateId(Long id);
    void resetByRatingTemplateId(Long id);
    void resetByRatingTemplateId(Collection<Long> ids);
    void removeByRatingTemplateId(Long id);
    List<Project_task_type> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Project_task_type> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Project_task_type> getProjectTaskTypeByIds(List<Long> ids) ;
    List<Project_task_type> getProjectTaskTypeByEntities(List<Project_task_type> entities) ;
}


