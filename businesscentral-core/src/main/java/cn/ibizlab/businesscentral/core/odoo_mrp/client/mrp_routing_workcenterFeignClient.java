package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing_workcenter;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_routing_workcenterSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_routing_workcenter] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mrp:odoo-mrp}", contextId = "mrp-routing-workcenter", fallback = mrp_routing_workcenterFallback.class)
public interface mrp_routing_workcenterFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_routing_workcenters/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_routing_workcenters/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters/search")
    Page<Mrp_routing_workcenter> search(@RequestBody Mrp_routing_workcenterSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_routing_workcenters/{id}")
    Mrp_routing_workcenter get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_routing_workcenters/{id}")
    Mrp_routing_workcenter update(@PathVariable("id") Long id,@RequestBody Mrp_routing_workcenter mrp_routing_workcenter);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_routing_workcenters/batch")
    Boolean updateBatch(@RequestBody List<Mrp_routing_workcenter> mrp_routing_workcenters);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters")
    Mrp_routing_workcenter create(@RequestBody Mrp_routing_workcenter mrp_routing_workcenter);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters/batch")
    Boolean createBatch(@RequestBody List<Mrp_routing_workcenter> mrp_routing_workcenters);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_routing_workcenters/select")
    Page<Mrp_routing_workcenter> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_routing_workcenters/getdraft")
    Mrp_routing_workcenter getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters/checkkey")
    Boolean checkKey(@RequestBody Mrp_routing_workcenter mrp_routing_workcenter);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters/save")
    Boolean save(@RequestBody Mrp_routing_workcenter mrp_routing_workcenter);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters/savebatch")
    Boolean saveBatch(@RequestBody List<Mrp_routing_workcenter> mrp_routing_workcenters);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_routing_workcenters/searchdefault")
    Page<Mrp_routing_workcenter> searchDefault(@RequestBody Mrp_routing_workcenterSearchContext context);


}
