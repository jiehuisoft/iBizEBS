package cn.ibizlab.businesscentral.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_order_lineSearchContext;
import cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_purchase.mapper.Purchase_order_lineMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[采购订单行] 服务对象接口实现
 */
@Slf4j
@Service("Purchase_order_lineServiceImpl")
public class Purchase_order_lineServiceImpl extends EBSServiceImpl<Purchase_order_lineMapper, Purchase_order_line> implements IPurchase_order_lineService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_lineService accountInvoiceLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IAccount_tax_purchase_order_line_relService accountTaxPurchaseOrderLineRelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService purchaseOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService saleOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_orderService saleOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouse_orderpointService stockWarehouseOrderpointService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.IPurchase_order_linecalc_priceLogic calc_priceLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.IPurchase_order_linecalc_amountLogic calc_amountLogic;

    protected int batchSize = 500;

    public String getIrModel(){
        return "purchase.order.line" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Purchase_order_line et) {
        String taxes_id = et.getTaxesId() ;

        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        calc_priceLogic.execute(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_order_lineService)AopContext.currentProxy() : this).get(et.getId()),et);
        calc_amountLogic.execute(et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        if(org.apache.commons.lang3.StringUtils.isNotBlank(et.getTaxesId())){
            this.baseMapper.saveRelByTaxesId(et.getId() , com.alibaba.fastjson.JSONArray.parseArray(taxes_id, cn.ibizlab.businesscentral.util.domain.MultiSelectItem.class));
        }

        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Purchase_order_line> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Purchase_order_line et) {
        if(et.getFocusNull().contains("taxes_id") || et.getTaxesId()!=null){
            et.getFocusNull().remove("taxes_id");
            accountTaxPurchaseOrderLineRelService.removeByPurchaseOrderLineId(et.getId());
            if(org.apache.commons.lang3.StringUtils.isNotBlank(et.getTaxesId())){
                this.baseMapper.saveRelByTaxesId(et.getId() , com.alibaba.fastjson.JSONArray.parseArray(et.getTaxesId(), cn.ibizlab.businesscentral.util.domain.MultiSelectItem.class));
            }
        }

        Purchase_order_line old = new Purchase_order_line() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_order_lineService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        calc_priceLogic.execute(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_order_lineService)AopContext.currentProxy() : this).get(et.getId()),et);
        calc_amountLogic.execute(et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Purchase_order_line> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        Purchase_order_line et=new Purchase_order_line();
        et.set("id",key);
        accountInvoiceLineService.resetByPurchaseLineId(key);
        stockMoveService.resetByCreatedPurchaseLineId(key);
        stockMoveService.resetByPurchaseLineId(key);
        boolean result=removeById(key);
        calc_amountLogic.execute(et);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountInvoiceLineService.resetByPurchaseLineId(idList);
        stockMoveService.resetByCreatedPurchaseLineId(idList);
        stockMoveService.resetByPurchaseLineId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Purchase_order_line get(Long key) {
        Purchase_order_line et = getById(key);
        if(et==null){
            et=new Purchase_order_line();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Purchase_order_line getDraft(Purchase_order_line et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Purchase_order_line calc_amount(Purchase_order_line et) {
        calc_amountLogic.execute(et);
         return et ;
    }

    @Override
    @Transactional
    public Purchase_order_line calc_price(Purchase_order_line et) {
        //自定义代码
        return et;
    }

    @Override
    public boolean checkKey(Purchase_order_line et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public Purchase_order_line product_change(Purchase_order_line et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean save(Purchase_order_line et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Purchase_order_line et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Purchase_order_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Purchase_order_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Purchase_order_line> selectByAccountAnalyticId(Long id) {
        return baseMapper.selectByAccountAnalyticId(id);
    }
    @Override
    public void resetByAccountAnalyticId(Long id) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("account_analytic_id",null).eq("account_analytic_id",id));
    }

    @Override
    public void resetByAccountAnalyticId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("account_analytic_id",null).in("account_analytic_id",ids));
    }

    @Override
    public void removeByAccountAnalyticId(Long id) {
        this.remove(new QueryWrapper<Purchase_order_line>().eq("account_analytic_id",id));
    }

	@Override
    public List<Purchase_order_line> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Purchase_order_line>().eq("product_id",id));
    }

	@Override
    public List<Purchase_order_line> selectByOrderId(Long id) {
        return baseMapper.selectByOrderId(id);
    }
    @Override
    public void removeByOrderId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Purchase_order_line>().in("order_id",ids));
    }

    @Override
    public void removeByOrderId(Long id) {
        this.remove(new QueryWrapper<Purchase_order_line>().eq("order_id",id));
    }

	@Override
    public List<Purchase_order_line> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Purchase_order_line>().eq("company_id",id));
    }

	@Override
    public List<Purchase_order_line> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Purchase_order_line>().eq("currency_id",id));
    }

	@Override
    public List<Purchase_order_line> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Purchase_order_line>().eq("partner_id",id));
    }

	@Override
    public List<Purchase_order_line> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Purchase_order_line>().eq("create_uid",id));
    }

	@Override
    public List<Purchase_order_line> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Purchase_order_line>().eq("write_uid",id));
    }

	@Override
    public List<Purchase_order_line> selectBySaleLineId(Long id) {
        return baseMapper.selectBySaleLineId(id);
    }
    @Override
    public void resetBySaleLineId(Long id) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("sale_line_id",null).eq("sale_line_id",id));
    }

    @Override
    public void resetBySaleLineId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("sale_line_id",null).in("sale_line_id",ids));
    }

    @Override
    public void removeBySaleLineId(Long id) {
        this.remove(new QueryWrapper<Purchase_order_line>().eq("sale_line_id",id));
    }

	@Override
    public List<Purchase_order_line> selectBySaleOrderId(Long id) {
        return baseMapper.selectBySaleOrderId(id);
    }
    @Override
    public void resetBySaleOrderId(Long id) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("sale_order_id",null).eq("sale_order_id",id));
    }

    @Override
    public void resetBySaleOrderId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("sale_order_id",null).in("sale_order_id",ids));
    }

    @Override
    public void removeBySaleOrderId(Long id) {
        this.remove(new QueryWrapper<Purchase_order_line>().eq("sale_order_id",id));
    }

	@Override
    public List<Purchase_order_line> selectByOrderpointId(Long id) {
        return baseMapper.selectByOrderpointId(id);
    }
    @Override
    public void resetByOrderpointId(Long id) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("orderpoint_id",null).eq("orderpoint_id",id));
    }

    @Override
    public void resetByOrderpointId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("orderpoint_id",null).in("orderpoint_id",ids));
    }

    @Override
    public void removeByOrderpointId(Long id) {
        this.remove(new QueryWrapper<Purchase_order_line>().eq("orderpoint_id",id));
    }

	@Override
    public List<Purchase_order_line> selectByProductUom(Long id) {
        return baseMapper.selectByProductUom(id);
    }
    @Override
    public void resetByProductUom(Long id) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("product_uom",null).eq("product_uom",id));
    }

    @Override
    public void resetByProductUom(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_order_line>().set("product_uom",null).in("product_uom",ids));
    }

    @Override
    public void removeByProductUom(Long id) {
        this.remove(new QueryWrapper<Purchase_order_line>().eq("product_uom",id));
    }


    /**
     * 查询集合 calc_order_amount
     */
    @Override
    public Page<HashMap> searchCalc_order_amount(Purchase_order_lineSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<HashMap> pages=baseMapper.searchCalc_order_amount(context.getPages(),context,context.getSelectCond());
        return new PageImpl<HashMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Purchase_order_line> searchDefault(Purchase_order_lineSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Purchase_order_line> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Purchase_order_line>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Purchase_order_line et){
        //实体关系[DER1N_PURCHASE_ORDER_LINE__ACCOUNT_ANALYTIC_ACCOUNT__ACCOUNT_ANALYTIC_ID]
        if(!ObjectUtils.isEmpty(et.getAccountAnalyticId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAccountAnalytic=et.getOdooAccountAnalytic();
            if(ObjectUtils.isEmpty(odooAccountAnalytic)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account majorEntity=accountAnalyticAccountService.get(et.getAccountAnalyticId());
                et.setOdooAccountAnalytic(majorEntity);
                odooAccountAnalytic=majorEntity;
            }
            et.setAccountAnalyticIdText(odooAccountAnalytic.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER_LINE__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
            et.setProductType(odooProduct.getType());
            et.setProductImage(odooProduct.getImage());
        }
        //实体关系[DER1N_PURCHASE_ORDER_LINE__PURCHASE_ORDER__ORDER_ID]
        if(!ObjectUtils.isEmpty(et.getOrderId())){
            cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order odooOrder=et.getOdooOrder();
            if(ObjectUtils.isEmpty(odooOrder)){
                cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order majorEntity=purchaseOrderService.get(et.getOrderId());
                et.setOdooOrder(majorEntity);
                odooOrder=majorEntity;
            }
            et.setState(odooOrder.getState());
            et.setOrderIdText(odooOrder.getName());
            et.setDateOrder(odooOrder.getDateOrder());
        }
        //实体关系[DER1N_PURCHASE_ORDER_LINE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER_LINE__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER_LINE__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER_LINE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER_LINE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER_LINE__SALE_ORDER_LINE__SALE_LINE_ID]
        if(!ObjectUtils.isEmpty(et.getSaleLineId())){
            cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line odooSaleLine=et.getOdooSaleLine();
            if(ObjectUtils.isEmpty(odooSaleLine)){
                cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line majorEntity=saleOrderLineService.get(et.getSaleLineId());
                et.setOdooSaleLine(majorEntity);
                odooSaleLine=majorEntity;
            }
            et.setSaleLineIdText(odooSaleLine.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER_LINE__SALE_ORDER__SALE_ORDER_ID]
        if(!ObjectUtils.isEmpty(et.getSaleOrderId())){
            cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order odooSaleOrder=et.getOdooSaleOrder();
            if(ObjectUtils.isEmpty(odooSaleOrder)){
                cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order majorEntity=saleOrderService.get(et.getSaleOrderId());
                et.setOdooSaleOrder(majorEntity);
                odooSaleOrder=majorEntity;
            }
            et.setSaleOrderIdText(odooSaleOrder.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER_LINE__STOCK_WAREHOUSE_ORDERPOINT__ORDERPOINT_ID]
        if(!ObjectUtils.isEmpty(et.getOrderpointId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse_orderpoint odooOrderpoint=et.getOdooOrderpoint();
            if(ObjectUtils.isEmpty(odooOrderpoint)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse_orderpoint majorEntity=stockWarehouseOrderpointService.get(et.getOrderpointId());
                et.setOdooOrderpoint(majorEntity);
                odooOrderpoint=majorEntity;
            }
            et.setOrderpointIdText(odooOrderpoint.getName());
        }
        //实体关系[DER1N_PURCHASE_ORDER_LINE__UOM_UOM__PRODUCT_UOM]
        if(!ObjectUtils.isEmpty(et.getProductUom())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUom());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomText(odooProductUom.getName());
            et.setProductUomCategoryId(odooProductUom.getCategoryId());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Purchase_order_line> getPurchaseOrderLineByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Purchase_order_line> getPurchaseOrderLineByEntities(List<Purchase_order_line> entities) {
        List ids =new ArrayList();
        for(Purchase_order_line entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



