package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_allocation;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leave_allocationSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_leave_allocation] 服务对象接口
 */
public interface IHr_leave_allocationService extends IService<Hr_leave_allocation>{

    boolean create(Hr_leave_allocation et) ;
    void createBatch(List<Hr_leave_allocation> list) ;
    boolean update(Hr_leave_allocation et) ;
    void updateBatch(List<Hr_leave_allocation> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_leave_allocation get(Long key) ;
    Hr_leave_allocation getDraft(Hr_leave_allocation et) ;
    boolean checkKey(Hr_leave_allocation et) ;
    boolean save(Hr_leave_allocation et) ;
    void saveBatch(List<Hr_leave_allocation> list) ;
    Page<Hr_leave_allocation> searchDefault(Hr_leave_allocationSearchContext context) ;
    List<Hr_leave_allocation> selectByDepartmentId(Long id);
    void resetByDepartmentId(Long id);
    void resetByDepartmentId(Collection<Long> ids);
    void removeByDepartmentId(Long id);
    List<Hr_leave_allocation> selectByCategoryId(Long id);
    void resetByCategoryId(Long id);
    void resetByCategoryId(Collection<Long> ids);
    void removeByCategoryId(Long id);
    List<Hr_leave_allocation> selectByEmployeeId(Long id);
    void resetByEmployeeId(Long id);
    void resetByEmployeeId(Collection<Long> ids);
    void removeByEmployeeId(Long id);
    List<Hr_leave_allocation> selectByFirstApproverId(Long id);
    void resetByFirstApproverId(Long id);
    void resetByFirstApproverId(Collection<Long> ids);
    void removeByFirstApproverId(Long id);
    List<Hr_leave_allocation> selectBySecondApproverId(Long id);
    void resetBySecondApproverId(Long id);
    void resetBySecondApproverId(Collection<Long> ids);
    void removeBySecondApproverId(Long id);
    List<Hr_leave_allocation> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Hr_leave_allocation> selectByHolidayStatusId(Long id);
    void resetByHolidayStatusId(Long id);
    void resetByHolidayStatusId(Collection<Long> ids);
    void removeByHolidayStatusId(Long id);
    List<Hr_leave_allocation> selectByModeCompanyId(Long id);
    void resetByModeCompanyId(Long id);
    void resetByModeCompanyId(Collection<Long> ids);
    void removeByModeCompanyId(Long id);
    List<Hr_leave_allocation> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_leave_allocation> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_leave_allocation> getHrLeaveAllocationByIds(List<Long> ids) ;
    List<Hr_leave_allocation> getHrLeaveAllocationByEntities(List<Hr_leave_allocation> entities) ;
}


