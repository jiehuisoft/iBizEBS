package cn.ibizlab.businesscentral.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_goalSearchContext;
import cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_goalService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_gamification.mapper.Gamification_goalMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[游戏化目标] 服务对象接口实现
 */
@Slf4j
@Service("Gamification_goalServiceImpl")
public class Gamification_goalServiceImpl extends EBSServiceImpl<Gamification_goalMapper, Gamification_goal> implements IGamification_goalService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_goal_wizardService gamificationGoalWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_challenge_lineService gamificationChallengeLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_challengeService gamificationChallengeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_goal_definitionService gamificationGoalDefinitionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "gamification.goal" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Gamification_goal et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IGamification_goalService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Gamification_goal> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Gamification_goal et) {
        Gamification_goal old = new Gamification_goal() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IGamification_goalService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IGamification_goalService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Gamification_goal> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        gamificationGoalWizardService.resetByGoalId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        gamificationGoalWizardService.resetByGoalId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Gamification_goal get(Long key) {
        Gamification_goal et = getById(key);
        if(et==null){
            et=new Gamification_goal();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Gamification_goal getDraft(Gamification_goal et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Gamification_goal et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Gamification_goal et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Gamification_goal et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Gamification_goal> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Gamification_goal> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Gamification_goal> selectByLineId(Long id) {
        return baseMapper.selectByLineId(id);
    }
    @Override
    public void removeByLineId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Gamification_goal>().in("line_id",ids));
    }

    @Override
    public void removeByLineId(Long id) {
        this.remove(new QueryWrapper<Gamification_goal>().eq("line_id",id));
    }

	@Override
    public List<Gamification_goal> selectByChallengeId(Long id) {
        return baseMapper.selectByChallengeId(id);
    }
    @Override
    public void resetByChallengeId(Long id) {
        this.update(new UpdateWrapper<Gamification_goal>().set("challenge_id",null).eq("challenge_id",id));
    }

    @Override
    public void resetByChallengeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Gamification_goal>().set("challenge_id",null).in("challenge_id",ids));
    }

    @Override
    public void removeByChallengeId(Long id) {
        this.remove(new QueryWrapper<Gamification_goal>().eq("challenge_id",id));
    }

	@Override
    public List<Gamification_goal> selectByDefinitionId(Long id) {
        return baseMapper.selectByDefinitionId(id);
    }
    @Override
    public void removeByDefinitionId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Gamification_goal>().in("definition_id",ids));
    }

    @Override
    public void removeByDefinitionId(Long id) {
        this.remove(new QueryWrapper<Gamification_goal>().eq("definition_id",id));
    }

	@Override
    public List<Gamification_goal> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Gamification_goal>().eq("create_uid",id));
    }

	@Override
    public List<Gamification_goal> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void removeByUserId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Gamification_goal>().in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Gamification_goal>().eq("user_id",id));
    }

	@Override
    public List<Gamification_goal> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Gamification_goal>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Gamification_goal> searchDefault(Gamification_goalSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Gamification_goal> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Gamification_goal>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Gamification_goal et){
        //实体关系[DER1N_GAMIFICATION_GOAL__GAMIFICATION_CHALLENGE_LINE__LINE_ID]
        if(!ObjectUtils.isEmpty(et.getLineId())){
            cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge_line odooLine=et.getOdooLine();
            if(ObjectUtils.isEmpty(odooLine)){
                cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge_line majorEntity=gamificationChallengeLineService.get(et.getLineId());
                et.setOdooLine(majorEntity);
                odooLine=majorEntity;
            }
            et.setLineIdText(odooLine.getName());
        }
        //实体关系[DER1N_GAMIFICATION_GOAL__GAMIFICATION_CHALLENGE__CHALLENGE_ID]
        if(!ObjectUtils.isEmpty(et.getChallengeId())){
            cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge odooChallenge=et.getOdooChallenge();
            if(ObjectUtils.isEmpty(odooChallenge)){
                cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge majorEntity=gamificationChallengeService.get(et.getChallengeId());
                et.setOdooChallenge(majorEntity);
                odooChallenge=majorEntity;
            }
            et.setChallengeIdText(odooChallenge.getName());
        }
        //实体关系[DER1N_GAMIFICATION_GOAL__GAMIFICATION_GOAL_DEFINITION__DEFINITION_ID]
        if(!ObjectUtils.isEmpty(et.getDefinitionId())){
            cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal_definition odooDefinition=et.getOdooDefinition();
            if(ObjectUtils.isEmpty(odooDefinition)){
                cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal_definition majorEntity=gamificationGoalDefinitionService.get(et.getDefinitionId());
                et.setOdooDefinition(majorEntity);
                odooDefinition=majorEntity;
            }
            et.setDefinitionDescription(odooDefinition.getDescription());
            et.setDefinitionSuffix(odooDefinition.getFullSuffix());
            et.setDefinitionIdText(odooDefinition.getName());
            et.setDefinitionDisplay(odooDefinition.getDisplayMode());
            et.setDefinitionCondition(odooDefinition.getCondition());
            et.setComputationMode(odooDefinition.getComputationMode());
        }
        //实体关系[DER1N_GAMIFICATION_GOAL__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_GAMIFICATION_GOAL__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_GAMIFICATION_GOAL__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Gamification_goal> getGamificationGoalByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Gamification_goal> getGamificationGoalByEntities(List<Gamification_goal> entities) {
        List ids =new ArrayList();
        for(Gamification_goal entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



