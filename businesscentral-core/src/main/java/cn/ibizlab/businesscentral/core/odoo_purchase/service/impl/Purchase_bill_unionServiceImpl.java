package cn.ibizlab.businesscentral.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_bill_union;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_bill_unionSearchContext;
import cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_bill_unionService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_purchase.mapper.Purchase_bill_unionMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[采购 & 账单] 服务对象接口实现
 */
@Slf4j
@Service("Purchase_bill_unionServiceImpl")
public class Purchase_bill_unionServiceImpl extends EBSServiceImpl<Purchase_bill_unionMapper, Purchase_bill_union> implements IPurchase_bill_unionService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService purchaseOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "purchase.bill.union" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Purchase_bill_union et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_bill_unionService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Purchase_bill_union> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Purchase_bill_union et) {
        Purchase_bill_union old = new Purchase_bill_union() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_bill_unionService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_bill_unionService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Purchase_bill_union> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountInvoiceService.resetByVendorBillPurchaseId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountInvoiceService.resetByVendorBillPurchaseId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Purchase_bill_union get(Long key) {
        Purchase_bill_union et = getById(key);
        if(et==null){
            et=new Purchase_bill_union();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Purchase_bill_union getDraft(Purchase_bill_union et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Purchase_bill_union et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Purchase_bill_union et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Purchase_bill_union et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Purchase_bill_union> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Purchase_bill_union> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Purchase_bill_union> selectByVendorBillId(Long id) {
        return baseMapper.selectByVendorBillId(id);
    }
    @Override
    public void resetByVendorBillId(Long id) {
        this.update(new UpdateWrapper<Purchase_bill_union>().set("vendor_bill_id",null).eq("vendor_bill_id",id));
    }

    @Override
    public void resetByVendorBillId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_bill_union>().set("vendor_bill_id",null).in("vendor_bill_id",ids));
    }

    @Override
    public void removeByVendorBillId(Long id) {
        this.remove(new QueryWrapper<Purchase_bill_union>().eq("vendor_bill_id",id));
    }

	@Override
    public List<Purchase_bill_union> selectByPurchaseOrderId(Long id) {
        return baseMapper.selectByPurchaseOrderId(id);
    }
    @Override
    public void resetByPurchaseOrderId(Long id) {
        this.update(new UpdateWrapper<Purchase_bill_union>().set("purchase_order_id",null).eq("purchase_order_id",id));
    }

    @Override
    public void resetByPurchaseOrderId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_bill_union>().set("purchase_order_id",null).in("purchase_order_id",ids));
    }

    @Override
    public void removeByPurchaseOrderId(Long id) {
        this.remove(new QueryWrapper<Purchase_bill_union>().eq("purchase_order_id",id));
    }

	@Override
    public List<Purchase_bill_union> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Purchase_bill_union>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_bill_union>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Purchase_bill_union>().eq("company_id",id));
    }

	@Override
    public List<Purchase_bill_union> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Purchase_bill_union>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_bill_union>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Purchase_bill_union>().eq("currency_id",id));
    }

	@Override
    public List<Purchase_bill_union> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Purchase_bill_union>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_bill_union>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Purchase_bill_union>().eq("partner_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Purchase_bill_union> searchDefault(Purchase_bill_unionSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Purchase_bill_union> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Purchase_bill_union>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Purchase_bill_union et){
        //实体关系[DER1N_PURCHASE_BILL_UNION__ACCOUNT_INVOICE__VENDOR_BILL_ID]
        if(!ObjectUtils.isEmpty(et.getVendorBillId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice odooVendorBill=et.getOdooVendorBill();
            if(ObjectUtils.isEmpty(odooVendorBill)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice majorEntity=accountInvoiceService.get(et.getVendorBillId());
                et.setOdooVendorBill(majorEntity);
                odooVendorBill=majorEntity;
            }
            et.setVendorBillIdText(odooVendorBill.getName());
        }
        //实体关系[DER1N_PURCHASE_BILL_UNION__PURCHASE_ORDER__PURCHASE_ORDER_ID]
        if(!ObjectUtils.isEmpty(et.getPurchaseOrderId())){
            cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order odooPurchaseOrder=et.getOdooPurchaseOrder();
            if(ObjectUtils.isEmpty(odooPurchaseOrder)){
                cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order majorEntity=purchaseOrderService.get(et.getPurchaseOrderId());
                et.setOdooPurchaseOrder(majorEntity);
                odooPurchaseOrder=majorEntity;
            }
            et.setPurchaseOrderIdText(odooPurchaseOrder.getName());
        }
        //实体关系[DER1N_PURCHASE_BILL_UNION__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_PURCHASE_BILL_UNION__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_PURCHASE_BILL_UNION__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }




}



