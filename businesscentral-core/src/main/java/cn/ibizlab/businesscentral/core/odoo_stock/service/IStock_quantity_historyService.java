package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quantity_history;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_quantity_historySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_quantity_history] 服务对象接口
 */
public interface IStock_quantity_historyService extends IService<Stock_quantity_history>{

    boolean create(Stock_quantity_history et) ;
    void createBatch(List<Stock_quantity_history> list) ;
    boolean update(Stock_quantity_history et) ;
    void updateBatch(List<Stock_quantity_history> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_quantity_history get(Long key) ;
    Stock_quantity_history getDraft(Stock_quantity_history et) ;
    boolean checkKey(Stock_quantity_history et) ;
    boolean save(Stock_quantity_history et) ;
    void saveBatch(List<Stock_quantity_history> list) ;
    Page<Stock_quantity_history> searchDefault(Stock_quantity_historySearchContext context) ;
    List<Stock_quantity_history> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_quantity_history> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_quantity_history> getStockQuantityHistoryByIds(List<Long> ids) ;
    List<Stock_quantity_history> getStockQuantityHistoryByEntities(List<Stock_quantity_history> entities) ;
}


