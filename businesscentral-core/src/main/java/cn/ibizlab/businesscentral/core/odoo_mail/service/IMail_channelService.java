package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_channelSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_channel] 服务对象接口
 */
public interface IMail_channelService extends IService<Mail_channel>{

    boolean create(Mail_channel et) ;
    void createBatch(List<Mail_channel> list) ;
    boolean update(Mail_channel et) ;
    void updateBatch(List<Mail_channel> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_channel get(Long key) ;
    Mail_channel getDraft(Mail_channel et) ;
    boolean checkKey(Mail_channel et) ;
    boolean save(Mail_channel et) ;
    void saveBatch(List<Mail_channel> list) ;
    Page<Mail_channel> searchDefault(Mail_channelSearchContext context) ;
    List<Mail_channel> selectByLivechatChannelId(Long id);
    void resetByLivechatChannelId(Long id);
    void resetByLivechatChannelId(Collection<Long> ids);
    void removeByLivechatChannelId(Long id);
    List<Mail_channel> selectByAliasId(Long id);
    List<Mail_channel> selectByAliasId(Collection<Long> ids);
    void removeByAliasId(Long id);
    List<Mail_channel> selectByGroupPublicId(Long id);
    void resetByGroupPublicId(Long id);
    void resetByGroupPublicId(Collection<Long> ids);
    void removeByGroupPublicId(Long id);
    List<Mail_channel> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_channel> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_channel> getMailChannelByIds(List<Long> ids) ;
    List<Mail_channel> getMailChannelByEntities(List<Mail_channel> entities) ;
}


