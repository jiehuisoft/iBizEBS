package cn.ibizlab.businesscentral.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type_mail;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_type_mailSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[event_type_mail] 服务对象接口
 */
@Component
public class event_type_mailFallback implements event_type_mailFeignClient{


    public Event_type_mail update(Long id, Event_type_mail event_type_mail){
            return null;
     }
    public Boolean updateBatch(List<Event_type_mail> event_type_mails){
            return false;
     }


    public Page<Event_type_mail> search(Event_type_mailSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Event_type_mail get(Long id){
            return null;
     }


    public Event_type_mail create(Event_type_mail event_type_mail){
            return null;
     }
    public Boolean createBatch(List<Event_type_mail> event_type_mails){
            return false;
     }

    public Page<Event_type_mail> select(){
            return null;
     }

    public Event_type_mail getDraft(){
            return null;
    }



    public Boolean checkKey(Event_type_mail event_type_mail){
            return false;
     }


    public Boolean save(Event_type_mail event_type_mail){
            return false;
     }
    public Boolean saveBatch(List<Event_type_mail> event_type_mails){
            return false;
     }

    public Page<Event_type_mail> searchDefault(Event_type_mailSearchContext context){
            return null;
     }


}
