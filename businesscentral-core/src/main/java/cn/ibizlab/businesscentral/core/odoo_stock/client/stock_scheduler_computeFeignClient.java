package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_scheduler_compute;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_scheduler_computeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_scheduler_compute] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-scheduler-compute", fallback = stock_scheduler_computeFallback.class)
public interface stock_scheduler_computeFeignClient {




    @RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes")
    Stock_scheduler_compute create(@RequestBody Stock_scheduler_compute stock_scheduler_compute);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes/batch")
    Boolean createBatch(@RequestBody List<Stock_scheduler_compute> stock_scheduler_computes);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_scheduler_computes/{id}")
    Stock_scheduler_compute get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes/search")
    Page<Stock_scheduler_compute> search(@RequestBody Stock_scheduler_computeSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_scheduler_computes/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_scheduler_computes/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_scheduler_computes/{id}")
    Stock_scheduler_compute update(@PathVariable("id") Long id,@RequestBody Stock_scheduler_compute stock_scheduler_compute);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_scheduler_computes/batch")
    Boolean updateBatch(@RequestBody List<Stock_scheduler_compute> stock_scheduler_computes);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_scheduler_computes/select")
    Page<Stock_scheduler_compute> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_scheduler_computes/getdraft")
    Stock_scheduler_compute getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes/checkkey")
    Boolean checkKey(@RequestBody Stock_scheduler_compute stock_scheduler_compute);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes/save")
    Boolean save(@RequestBody Stock_scheduler_compute stock_scheduler_compute);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_scheduler_compute> stock_scheduler_computes);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes/searchdefault")
    Page<Stock_scheduler_compute> searchDefault(@RequestBody Stock_scheduler_computeSearchContext context);


}
