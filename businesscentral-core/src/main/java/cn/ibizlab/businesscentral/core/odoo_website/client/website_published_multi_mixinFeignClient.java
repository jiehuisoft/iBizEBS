package cn.ibizlab.businesscentral.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_published_multi_mixin;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_published_multi_mixinSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[website_published_multi_mixin] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-website:odoo-website}", contextId = "website-published-multi-mixin", fallback = website_published_multi_mixinFallback.class)
public interface website_published_multi_mixinFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/website_published_multi_mixins/{id}")
    Website_published_multi_mixin get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/website_published_multi_mixins/search")
    Page<Website_published_multi_mixin> search(@RequestBody Website_published_multi_mixinSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/website_published_multi_mixins/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/website_published_multi_mixins/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/website_published_multi_mixins")
    Website_published_multi_mixin create(@RequestBody Website_published_multi_mixin website_published_multi_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/website_published_multi_mixins/batch")
    Boolean createBatch(@RequestBody List<Website_published_multi_mixin> website_published_multi_mixins);


    @RequestMapping(method = RequestMethod.PUT, value = "/website_published_multi_mixins/{id}")
    Website_published_multi_mixin update(@PathVariable("id") Long id,@RequestBody Website_published_multi_mixin website_published_multi_mixin);

    @RequestMapping(method = RequestMethod.PUT, value = "/website_published_multi_mixins/batch")
    Boolean updateBatch(@RequestBody List<Website_published_multi_mixin> website_published_multi_mixins);



    @RequestMapping(method = RequestMethod.GET, value = "/website_published_multi_mixins/select")
    Page<Website_published_multi_mixin> select();


    @RequestMapping(method = RequestMethod.GET, value = "/website_published_multi_mixins/getdraft")
    Website_published_multi_mixin getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/website_published_multi_mixins/checkkey")
    Boolean checkKey(@RequestBody Website_published_multi_mixin website_published_multi_mixin);


    @RequestMapping(method = RequestMethod.POST, value = "/website_published_multi_mixins/save")
    Boolean save(@RequestBody Website_published_multi_mixin website_published_multi_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/website_published_multi_mixins/savebatch")
    Boolean saveBatch(@RequestBody List<Website_published_multi_mixin> website_published_multi_mixins);



    @RequestMapping(method = RequestMethod.POST, value = "/website_published_multi_mixins/searchdefault")
    Page<Website_published_multi_mixin> searchDefault(@RequestBody Website_published_multi_mixinSearchContext context);


}
