package cn.ibizlab.businesscentral.core.odoo_account.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_financial_year_op;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_financial_year_opSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Account_financial_year_opMapper extends BaseMapper<Account_financial_year_op>{

    Page<Account_financial_year_op> searchDefault(IPage page, @Param("srf") Account_financial_year_opSearchContext context, @Param("ew") Wrapper<Account_financial_year_op> wrapper) ;
    @Override
    Account_financial_year_op selectById(Serializable id);
    @Override
    int insert(Account_financial_year_op entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Account_financial_year_op entity);
    @Override
    int update(@Param(Constants.ENTITY) Account_financial_year_op entity, @Param("ew") Wrapper<Account_financial_year_op> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Account_financial_year_op> selectByCompanyId(@Param("id") Serializable id) ;

    List<Account_financial_year_op> selectByCreateUid(@Param("id") Serializable id) ;

    List<Account_financial_year_op> selectByWriteUid(@Param("id") Serializable id) ;


}
