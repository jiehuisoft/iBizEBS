package cn.ibizlab.businesscentral.core.odoo_project.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_task;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_taskSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[project_task] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-project:odoo-project}", contextId = "project-task", fallback = project_taskFallback.class)
public interface project_taskFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/project_tasks")
    Project_task create(@RequestBody Project_task project_task);

    @RequestMapping(method = RequestMethod.POST, value = "/project_tasks/batch")
    Boolean createBatch(@RequestBody List<Project_task> project_tasks);


    @RequestMapping(method = RequestMethod.DELETE, value = "/project_tasks/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/project_tasks/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/project_tasks/{id}")
    Project_task get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/project_tasks/{id}")
    Project_task update(@PathVariable("id") Long id,@RequestBody Project_task project_task);

    @RequestMapping(method = RequestMethod.PUT, value = "/project_tasks/batch")
    Boolean updateBatch(@RequestBody List<Project_task> project_tasks);




    @RequestMapping(method = RequestMethod.POST, value = "/project_tasks/search")
    Page<Project_task> search(@RequestBody Project_taskSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/project_tasks/select")
    Page<Project_task> select();


    @RequestMapping(method = RequestMethod.GET, value = "/project_tasks/getdraft")
    Project_task getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/project_tasks/checkkey")
    Boolean checkKey(@RequestBody Project_task project_task);


    @RequestMapping(method = RequestMethod.POST, value = "/project_tasks/save")
    Boolean save(@RequestBody Project_task project_task);

    @RequestMapping(method = RequestMethod.POST, value = "/project_tasks/savebatch")
    Boolean saveBatch(@RequestBody List<Project_task> project_tasks);



    @RequestMapping(method = RequestMethod.POST, value = "/project_tasks/searchdefault")
    Page<Project_task> searchDefault(@RequestBody Project_taskSearchContext context);


}
