package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_config;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_configSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_config] 服务对象接口
 */
@Component
public class res_configFallback implements res_configFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Res_config create(Res_config res_config){
            return null;
     }
    public Boolean createBatch(List<Res_config> res_configs){
            return false;
     }


    public Res_config get(Long id){
            return null;
     }


    public Res_config update(Long id, Res_config res_config){
            return null;
     }
    public Boolean updateBatch(List<Res_config> res_configs){
            return false;
     }



    public Page<Res_config> search(Res_configSearchContext context){
            return null;
     }



    public Page<Res_config> select(){
            return null;
     }

    public Res_config getDraft(){
            return null;
    }



    public Boolean checkKey(Res_config res_config){
            return false;
     }


    public Boolean save(Res_config res_config){
            return false;
     }
    public Boolean saveBatch(List<Res_config> res_configs){
            return false;
     }

    public Page<Res_config> searchDefault(Res_configSearchContext context){
            return null;
     }


}
