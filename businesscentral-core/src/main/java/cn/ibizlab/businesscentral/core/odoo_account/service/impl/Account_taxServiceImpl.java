package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_taxSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_taxService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_taxMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[税率] 服务对象接口实现
 */
@Slf4j
@Service("Account_taxServiceImpl")
public class Account_taxServiceImpl extends EBSServiceImpl<Account_taxMapper, Account_tax> implements IAccount_taxService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_position_taxService accountFiscalPositionTaxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_taxService accountInvoiceTaxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_lineService accountMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_reconcile_modelService accountReconcileModelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IAccount_tax_purchase_order_line_relService accountTaxPurchaseOrderLineRelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_supplier_taxes_relService productSupplierTaxesRelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_taxes_relService productTaxesRelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService accountAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_tax_groupService accountTaxGroupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.tax" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_tax et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_taxService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_tax> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_tax et) {
        Account_tax old = new Account_tax() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_taxService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_taxService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_tax> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountFiscalPositionTaxService.resetByTaxDestId(key);
        accountFiscalPositionTaxService.resetByTaxSrcId(key);
        if(!ObjectUtils.isEmpty(accountInvoiceTaxService.selectByTaxId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_invoice_tax]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(accountMoveLineService.selectByTaxLineId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_move_line]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(accountReconcileModelService.selectBySecondTaxId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_reconcile_model]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(accountReconcileModelService.selectByTaxId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_reconcile_model]数据，无法删除!","","");
        resCompanyService.resetByAccountPurchaseTaxId(key);
        resCompanyService.resetByAccountSaleTaxId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountFiscalPositionTaxService.resetByTaxDestId(idList);
        accountFiscalPositionTaxService.resetByTaxSrcId(idList);
        if(!ObjectUtils.isEmpty(accountInvoiceTaxService.selectByTaxId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_invoice_tax]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(accountMoveLineService.selectByTaxLineId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_move_line]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(accountReconcileModelService.selectBySecondTaxId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_reconcile_model]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(accountReconcileModelService.selectByTaxId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_reconcile_model]数据，无法删除!","","");
        resCompanyService.resetByAccountPurchaseTaxId(idList);
        resCompanyService.resetByAccountSaleTaxId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_tax get(Long key) {
        Account_tax et = getById(key);
        if(et==null){
            et=new Account_tax();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_tax getDraft(Account_tax et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Account_tax calc_tax(Account_tax et) {
        //自定义代码
        return et;
    }

    @Override
    public boolean checkKey(Account_tax et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_tax et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_tax et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_tax> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_tax> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_tax> selectByCashBasisBaseAccountId(Long id) {
        return baseMapper.selectByCashBasisBaseAccountId(id);
    }
    @Override
    public void resetByCashBasisBaseAccountId(Long id) {
        this.update(new UpdateWrapper<Account_tax>().set("cash_basis_base_account_id",null).eq("cash_basis_base_account_id",id));
    }

    @Override
    public void resetByCashBasisBaseAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_tax>().set("cash_basis_base_account_id",null).in("cash_basis_base_account_id",ids));
    }

    @Override
    public void removeByCashBasisBaseAccountId(Long id) {
        this.remove(new QueryWrapper<Account_tax>().eq("cash_basis_base_account_id",id));
    }

	@Override
    public List<Account_tax> selectByTaxGroupId(Long id) {
        return baseMapper.selectByTaxGroupId(id);
    }
    @Override
    public void resetByTaxGroupId(Long id) {
        this.update(new UpdateWrapper<Account_tax>().set("tax_group_id",null).eq("tax_group_id",id));
    }

    @Override
    public void resetByTaxGroupId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_tax>().set("tax_group_id",null).in("tax_group_id",ids));
    }

    @Override
    public void removeByTaxGroupId(Long id) {
        this.remove(new QueryWrapper<Account_tax>().eq("tax_group_id",id));
    }

	@Override
    public List<Account_tax> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Account_tax>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_tax>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Account_tax>().eq("company_id",id));
    }

	@Override
    public List<Account_tax> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_tax>().eq("create_uid",id));
    }

	@Override
    public List<Account_tax> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_tax>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_tax> searchDefault(Account_taxSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_tax> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_tax>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 采购税率
     */
    @Override
    public Page<Account_tax> searchPurchase(Account_taxSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_tax> pages=baseMapper.searchPurchase(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_tax>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 销售税率
     */
    @Override
    public Page<Account_tax> searchSale(Account_taxSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_tax> pages=baseMapper.searchSale(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_tax>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_tax et){
        //实体关系[DER1N_ACCOUNT_TAX__ACCOUNT_ACCOUNT__CASH_BASIS_BASE_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getCashBasisBaseAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooCashBasisBaseAccount=et.getOdooCashBasisBaseAccount();
            if(ObjectUtils.isEmpty(odooCashBasisBaseAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getCashBasisBaseAccountId());
                et.setOdooCashBasisBaseAccount(majorEntity);
                odooCashBasisBaseAccount=majorEntity;
            }
            et.setCashBasisBaseAccountIdText(odooCashBasisBaseAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_TAX__ACCOUNT_TAX_GROUP__TAX_GROUP_ID]
        if(!ObjectUtils.isEmpty(et.getTaxGroupId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_group odooTaxGroup=et.getOdooTaxGroup();
            if(ObjectUtils.isEmpty(odooTaxGroup)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_group majorEntity=accountTaxGroupService.get(et.getTaxGroupId());
                et.setOdooTaxGroup(majorEntity);
                odooTaxGroup=majorEntity;
            }
            et.setTaxGroupIdText(odooTaxGroup.getName());
        }
        //实体关系[DER1N_ACCOUNT_TAX__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setHideTaxExigibility(odooCompany.getTaxExigibility());
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_ACCOUNT_TAX__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_TAX__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_tax> getAccountTaxByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_tax> getAccountTaxByEntities(List<Account_tax> entities) {
        List ids =new ArrayList();
        for(Account_tax entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



