package cn.ibizlab.businesscentral.core.odoo_rating.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[混合评级]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "RATING_MIXIN",resultMap = "Rating_mixinResultMap")
public class Rating_mixin extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 最新反馈评级
     */
    @TableField(exist = false)
    @JSONField(name = "rating_last_feedback")
    @JsonProperty("rating_last_feedback")
    private String ratingLastFeedback;
    /**
     * 评级数
     */
    @TableField(exist = false)
    @JSONField(name = "rating_count")
    @JsonProperty("rating_count")
    private Integer ratingCount;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 评级
     */
    @TableField(exist = false)
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;
    /**
     * 最新图像评级
     */
    @TableField(exist = false)
    @JSONField(name = "rating_last_image")
    @JsonProperty("rating_last_image")
    private byte[] ratingLastImage;
    /**
     * 最新值评级
     */
    @DEField(name = "rating_last_value")
    @TableField(value = "rating_last_value")
    @JSONField(name = "rating_last_value")
    @JsonProperty("rating_last_value")
    private Double ratingLastValue;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;



    /**
     * 设置 [最新值评级]
     */
    public void setRatingLastValue(Double ratingLastValue){
        this.ratingLastValue = ratingLastValue ;
        this.modify("rating_last_value",ratingLastValue);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


