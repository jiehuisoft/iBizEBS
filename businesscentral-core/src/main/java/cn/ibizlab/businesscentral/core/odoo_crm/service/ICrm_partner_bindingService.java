package cn.ibizlab.businesscentral.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_partner_binding;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_partner_bindingSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Crm_partner_binding] 服务对象接口
 */
public interface ICrm_partner_bindingService extends IService<Crm_partner_binding>{

    boolean create(Crm_partner_binding et) ;
    void createBatch(List<Crm_partner_binding> list) ;
    boolean update(Crm_partner_binding et) ;
    void updateBatch(List<Crm_partner_binding> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Crm_partner_binding get(Long key) ;
    Crm_partner_binding getDraft(Crm_partner_binding et) ;
    boolean checkKey(Crm_partner_binding et) ;
    boolean save(Crm_partner_binding et) ;
    void saveBatch(List<Crm_partner_binding> list) ;
    Page<Crm_partner_binding> searchDefault(Crm_partner_bindingSearchContext context) ;
    List<Crm_partner_binding> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Crm_partner_binding> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Crm_partner_binding> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Crm_partner_binding> getCrmPartnerBindingByIds(List<Long> ids) ;
    List<Crm_partner_binding> getCrmPartnerBindingByEntities(List<Crm_partner_binding> entities) ;
}


