package cn.ibizlab.businesscentral.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workorderSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mrp_workorder] 服务对象接口
 */
public interface IMrp_workorderService extends IService<Mrp_workorder>{

    boolean create(Mrp_workorder et) ;
    void createBatch(List<Mrp_workorder> list) ;
    boolean update(Mrp_workorder et) ;
    void updateBatch(List<Mrp_workorder> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mrp_workorder get(Long key) ;
    Mrp_workorder getDraft(Mrp_workorder et) ;
    boolean checkKey(Mrp_workorder et) ;
    boolean save(Mrp_workorder et) ;
    void saveBatch(List<Mrp_workorder> list) ;
    Page<Mrp_workorder> searchDefault(Mrp_workorderSearchContext context) ;
    List<Mrp_workorder> selectByProductionId(Long id);
    void removeByProductionId(Collection<Long> ids);
    void removeByProductionId(Long id);
    List<Mrp_workorder> selectByOperationId(Long id);
    void resetByOperationId(Long id);
    void resetByOperationId(Collection<Long> ids);
    void removeByOperationId(Long id);
    List<Mrp_workorder> selectByWorkcenterId(Long id);
    void resetByWorkcenterId(Long id);
    void resetByWorkcenterId(Collection<Long> ids);
    void removeByWorkcenterId(Long id);
    List<Mrp_workorder> selectByNextWorkOrderId(Long id);
    void resetByNextWorkOrderId(Long id);
    void resetByNextWorkOrderId(Collection<Long> ids);
    void removeByNextWorkOrderId(Long id);
    List<Mrp_workorder> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Mrp_workorder> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mrp_workorder> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Mrp_workorder> selectByFinalLotId(Long id);
    void resetByFinalLotId(Long id);
    void resetByFinalLotId(Collection<Long> ids);
    void removeByFinalLotId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mrp_workorder> getMrpWorkorderByIds(List<Long> ids) ;
    List<Mrp_workorder> getMrpWorkorderByEntities(List<Mrp_workorder> entities) ;
}


