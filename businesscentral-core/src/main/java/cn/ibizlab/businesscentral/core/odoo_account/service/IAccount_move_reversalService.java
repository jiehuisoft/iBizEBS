package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move_reversal;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_move_reversalSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_move_reversal] 服务对象接口
 */
public interface IAccount_move_reversalService extends IService<Account_move_reversal>{

    boolean create(Account_move_reversal et) ;
    void createBatch(List<Account_move_reversal> list) ;
    boolean update(Account_move_reversal et) ;
    void updateBatch(List<Account_move_reversal> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_move_reversal get(Long key) ;
    Account_move_reversal getDraft(Account_move_reversal et) ;
    boolean checkKey(Account_move_reversal et) ;
    boolean save(Account_move_reversal et) ;
    void saveBatch(List<Account_move_reversal> list) ;
    Page<Account_move_reversal> searchDefault(Account_move_reversalSearchContext context) ;
    List<Account_move_reversal> selectByJournalId(Long id);
    void resetByJournalId(Long id);
    void resetByJournalId(Collection<Long> ids);
    void removeByJournalId(Long id);
    List<Account_move_reversal> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_move_reversal> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_move_reversal> getAccountMoveReversalByIds(List<Long> ids) ;
    List<Account_move_reversal> getAccountMoveReversalByEntities(List<Account_move_reversal> entities) ;
}


