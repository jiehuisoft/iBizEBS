package cn.ibizlab.businesscentral.core.odoo_sale.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line;
/**
 * 关系型数据实体[Sale_order_line] 查询条件对象
 */
@Slf4j
@Data
public class Sale_order_lineSearchContext extends QueryWrapperContext<Sale_order_line> {

	private String n_qty_delivered_method_eq;//[更新数量的方法]
	public void setN_qty_delivered_method_eq(String n_qty_delivered_method_eq) {
        this.n_qty_delivered_method_eq = n_qty_delivered_method_eq;
        if(!ObjectUtils.isEmpty(this.n_qty_delivered_method_eq)){
            this.getSearchCond().eq("qty_delivered_method", n_qty_delivered_method_eq);
        }
    }
	private String n_invoice_status_eq;//[发票状态]
	public void setN_invoice_status_eq(String n_invoice_status_eq) {
        this.n_invoice_status_eq = n_invoice_status_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_status_eq)){
            this.getSearchCond().eq("invoice_status", n_invoice_status_eq);
        }
    }
	private String n_display_type_eq;//[显示类型]
	public void setN_display_type_eq(String n_display_type_eq) {
        this.n_display_type_eq = n_display_type_eq;
        if(!ObjectUtils.isEmpty(this.n_display_type_eq)){
            this.getSearchCond().eq("display_type", n_display_type_eq);
        }
    }
	private String n_name_like;//[说明]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_event_ticket_id_text_eq;//[活动入场券]
	public void setN_event_ticket_id_text_eq(String n_event_ticket_id_text_eq) {
        this.n_event_ticket_id_text_eq = n_event_ticket_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_event_ticket_id_text_eq)){
            this.getSearchCond().eq("event_ticket_id_text", n_event_ticket_id_text_eq);
        }
    }
	private String n_event_ticket_id_text_like;//[活动入场券]
	public void setN_event_ticket_id_text_like(String n_event_ticket_id_text_like) {
        this.n_event_ticket_id_text_like = n_event_ticket_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_event_ticket_id_text_like)){
            this.getSearchCond().like("event_ticket_id_text", n_event_ticket_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_linked_line_id_text_eq;//[链接的订单明细]
	public void setN_linked_line_id_text_eq(String n_linked_line_id_text_eq) {
        this.n_linked_line_id_text_eq = n_linked_line_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_linked_line_id_text_eq)){
            this.getSearchCond().eq("linked_line_id_text", n_linked_line_id_text_eq);
        }
    }
	private String n_linked_line_id_text_like;//[链接的订单明细]
	public void setN_linked_line_id_text_like(String n_linked_line_id_text_like) {
        this.n_linked_line_id_text_like = n_linked_line_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_linked_line_id_text_like)){
            this.getSearchCond().like("linked_line_id_text", n_linked_line_id_text_like);
        }
    }
	private String n_order_partner_id_text_eq;//[客户]
	public void setN_order_partner_id_text_eq(String n_order_partner_id_text_eq) {
        this.n_order_partner_id_text_eq = n_order_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_order_partner_id_text_eq)){
            this.getSearchCond().eq("order_partner_id_text", n_order_partner_id_text_eq);
        }
    }
	private String n_order_partner_id_text_like;//[客户]
	public void setN_order_partner_id_text_like(String n_order_partner_id_text_like) {
        this.n_order_partner_id_text_like = n_order_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_order_partner_id_text_like)){
            this.getSearchCond().like("order_partner_id_text", n_order_partner_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_product_packaging_text_eq;//[包裹]
	public void setN_product_packaging_text_eq(String n_product_packaging_text_eq) {
        this.n_product_packaging_text_eq = n_product_packaging_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_packaging_text_eq)){
            this.getSearchCond().eq("product_packaging_text", n_product_packaging_text_eq);
        }
    }
	private String n_product_packaging_text_like;//[包裹]
	public void setN_product_packaging_text_like(String n_product_packaging_text_like) {
        this.n_product_packaging_text_like = n_product_packaging_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_packaging_text_like)){
            this.getSearchCond().like("product_packaging_text", n_product_packaging_text_like);
        }
    }
	private String n_order_id_text_eq;//[订单关联]
	public void setN_order_id_text_eq(String n_order_id_text_eq) {
        this.n_order_id_text_eq = n_order_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_order_id_text_eq)){
            this.getSearchCond().eq("order_id_text", n_order_id_text_eq);
        }
    }
	private String n_order_id_text_like;//[订单关联]
	public void setN_order_id_text_like(String n_order_id_text_like) {
        this.n_order_id_text_like = n_order_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_order_id_text_like)){
            this.getSearchCond().like("order_id_text", n_order_id_text_like);
        }
    }
	private String n_event_id_text_eq;//[活动]
	public void setN_event_id_text_eq(String n_event_id_text_eq) {
        this.n_event_id_text_eq = n_event_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_event_id_text_eq)){
            this.getSearchCond().eq("event_id_text", n_event_id_text_eq);
        }
    }
	private String n_event_id_text_like;//[活动]
	public void setN_event_id_text_like(String n_event_id_text_like) {
        this.n_event_id_text_like = n_event_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_event_id_text_like)){
            this.getSearchCond().like("event_id_text", n_event_id_text_like);
        }
    }
	private String n_salesman_id_text_eq;//[销售员]
	public void setN_salesman_id_text_eq(String n_salesman_id_text_eq) {
        this.n_salesman_id_text_eq = n_salesman_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_salesman_id_text_eq)){
            this.getSearchCond().eq("salesman_id_text", n_salesman_id_text_eq);
        }
    }
	private String n_salesman_id_text_like;//[销售员]
	public void setN_salesman_id_text_like(String n_salesman_id_text_like) {
        this.n_salesman_id_text_like = n_salesman_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_salesman_id_text_like)){
            this.getSearchCond().like("salesman_id_text", n_salesman_id_text_like);
        }
    }
	private String n_product_uom_text_eq;//[计量单位]
	public void setN_product_uom_text_eq(String n_product_uom_text_eq) {
        this.n_product_uom_text_eq = n_product_uom_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_eq)){
            this.getSearchCond().eq("product_uom_text", n_product_uom_text_eq);
        }
    }
	private String n_product_uom_text_like;//[计量单位]
	public void setN_product_uom_text_like(String n_product_uom_text_like) {
        this.n_product_uom_text_like = n_product_uom_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_like)){
            this.getSearchCond().like("product_uom_text", n_product_uom_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_route_id_text_eq;//[路线]
	public void setN_route_id_text_eq(String n_route_id_text_eq) {
        this.n_route_id_text_eq = n_route_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_route_id_text_eq)){
            this.getSearchCond().eq("route_id_text", n_route_id_text_eq);
        }
    }
	private String n_route_id_text_like;//[路线]
	public void setN_route_id_text_like(String n_route_id_text_like) {
        this.n_route_id_text_like = n_route_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_route_id_text_like)){
            this.getSearchCond().like("route_id_text", n_route_id_text_like);
        }
    }
	private Long n_salesman_id_eq;//[销售员]
	public void setN_salesman_id_eq(Long n_salesman_id_eq) {
        this.n_salesman_id_eq = n_salesman_id_eq;
        if(!ObjectUtils.isEmpty(this.n_salesman_id_eq)){
            this.getSearchCond().eq("salesman_id", n_salesman_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_order_id_eq;//[订单关联]
	public void setN_order_id_eq(Long n_order_id_eq) {
        this.n_order_id_eq = n_order_id_eq;
        if(!ObjectUtils.isEmpty(this.n_order_id_eq)){
            this.getSearchCond().eq("order_id", n_order_id_eq);
        }
    }
	private Long n_event_id_eq;//[活动]
	public void setN_event_id_eq(Long n_event_id_eq) {
        this.n_event_id_eq = n_event_id_eq;
        if(!ObjectUtils.isEmpty(this.n_event_id_eq)){
            this.getSearchCond().eq("event_id", n_event_id_eq);
        }
    }
	private Long n_linked_line_id_eq;//[链接的订单明细]
	public void setN_linked_line_id_eq(Long n_linked_line_id_eq) {
        this.n_linked_line_id_eq = n_linked_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_linked_line_id_eq)){
            this.getSearchCond().eq("linked_line_id", n_linked_line_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_route_id_eq;//[路线]
	public void setN_route_id_eq(Long n_route_id_eq) {
        this.n_route_id_eq = n_route_id_eq;
        if(!ObjectUtils.isEmpty(this.n_route_id_eq)){
            this.getSearchCond().eq("route_id", n_route_id_eq);
        }
    }
	private Long n_order_partner_id_eq;//[客户]
	public void setN_order_partner_id_eq(Long n_order_partner_id_eq) {
        this.n_order_partner_id_eq = n_order_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_order_partner_id_eq)){
            this.getSearchCond().eq("order_partner_id", n_order_partner_id_eq);
        }
    }
	private Long n_product_uom_eq;//[计量单位]
	public void setN_product_uom_eq(Long n_product_uom_eq) {
        this.n_product_uom_eq = n_product_uom_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_eq)){
            this.getSearchCond().eq("product_uom", n_product_uom_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_product_packaging_eq;//[包裹]
	public void setN_product_packaging_eq(Long n_product_packaging_eq) {
        this.n_product_packaging_eq = n_product_packaging_eq;
        if(!ObjectUtils.isEmpty(this.n_product_packaging_eq)){
            this.getSearchCond().eq("product_packaging", n_product_packaging_eq);
        }
    }
	private Long n_event_ticket_id_eq;//[活动入场券]
	public void setN_event_ticket_id_eq(Long n_event_ticket_id_eq) {
        this.n_event_ticket_id_eq = n_event_ticket_id_eq;
        if(!ObjectUtils.isEmpty(this.n_event_ticket_id_eq)){
            this.getSearchCond().eq("event_ticket_id", n_event_ticket_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



