package cn.ibizlab.businesscentral.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[调拨]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "STOCK_PICKING",resultMap = "Stock_pickingResultMap")
public class Stock_picking extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 包裹层级
     */
    @TableField(exist = false)
    @JSONField(name = "package_level_ids")
    @JsonProperty("package_level_ids")
    private String packageLevelIds;
    /**
     * 备注
     */
    @TableField(value = "note")
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;
    /**
     * 网站
     */
    @DEField(name = "website_id")
    @TableField(value = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 已打印
     */
    @TableField(value = "printed")
    @JSONField(name = "printed")
    @JsonProperty("printed")
    private Boolean printed;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 产品
     */
    @TableField(exist = false)
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 送货策略
     */
    @DEField(name = "move_type")
    @TableField(value = "move_type")
    @JSONField(name = "move_type")
    @JsonProperty("move_type")
    private String moveType;
    /**
     * 欠单
     */
    @TableField(exist = false)
    @JSONField(name = "backorder_ids")
    @JsonProperty("backorder_ids")
    private String backorderIds;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 有包裹
     */
    @TableField(exist = false)
    @JSONField(name = "has_packages")
    @JsonProperty("has_packages")
    private Boolean hasPackages;
    /**
     * 源文档
     */
    @TableField(value = "origin")
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;
    /**
     * 是锁定
     */
    @DEField(name = "is_locked")
    @TableField(value = "is_locked")
    @JSONField(name = "is_locked")
    @JsonProperty("is_locked")
    private Boolean isLocked;
    /**
     * 显示作业
     */
    @TableField(exist = false)
    @JSONField(name = "show_operations")
    @JsonProperty("show_operations")
    private Boolean showOperations;
    /**
     * 补货组
     */
    @DEField(name = "group_id")
    @TableField(value = "group_id")
    @JSONField(name = "group_id")
    @JsonProperty("group_id")
    private Integer groupId;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 无包裹作业
     */
    @TableField(exist = false)
    @JSONField(name = "move_line_ids_without_package")
    @JsonProperty("move_line_ids_without_package")
    private String moveLineIdsWithoutPackage;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 下一活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 作业
     */
    @TableField(exist = false)
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;
    /**
     * 包裹层级ids 详情
     */
    @TableField(exist = false)
    @JSONField(name = "package_level_ids_details")
    @JsonProperty("package_level_ids_details")
    private String packageLevelIdsDetails;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 创建日期
     */
    @TableField(value = "date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 调拨日期
     */
    @DEField(name = "date_done")
    @TableField(value = "date_done")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_done" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_done")
    private Timestamp dateDone;
    /**
     * 有跟踪
     */
    @TableField(exist = false)
    @JSONField(name = "has_tracking")
    @JsonProperty("has_tracking")
    private Boolean hasTracking;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 立即调拨
     */
    @DEField(name = "immediate_transfer")
    @TableField(value = "immediate_transfer")
    @JSONField(name = "immediate_transfer")
    @JsonProperty("immediate_transfer")
    private Boolean immediateTransfer;
    /**
     * 有报废移动
     */
    @TableField(exist = false)
    @JSONField(name = "has_scrap_move")
    @JsonProperty("has_scrap_move")
    private Boolean hasScrapMove;
    /**
     * 库存移动不在包裹里
     */
    @TableField(exist = false)
    @JSONField(name = "move_ids_without_package")
    @JsonProperty("move_ids_without_package")
    private String moveIdsWithoutPackage;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 显示验证
     */
    @TableField(exist = false)
    @JSONField(name = "show_validate")
    @JsonProperty("show_validate")
    private Boolean showValidate;
    /**
     * 编号
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 优先级
     */
    @TableField(value = "priority")
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;
    /**
     * 库存移动
     */
    @TableField(exist = false)
    @JSONField(name = "move_lines")
    @JsonProperty("move_lines")
    private String moveLines;
    /**
     * 采购订单
     */
    @TableField(exist = false)
    @JSONField(name = "purchase_id")
    @JsonProperty("purchase_id")
    private Integer purchaseId;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 网站信息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 显示标记为代办
     */
    @TableField(exist = false)
    @JSONField(name = "show_mark_as_todo")
    @JsonProperty("show_mark_as_todo")
    private Boolean showMarkAsTodo;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 有包裹作业
     */
    @TableField(exist = false)
    @JSONField(name = "move_line_exist")
    @JsonProperty("move_line_exist")
    private Boolean moveLineExist;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 预定交货日期
     */
    @DEField(name = "scheduled_date")
    @TableField(value = "scheduled_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduled_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduled_date")
    private Timestamp scheduledDate;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 显示检查可用
     */
    @TableField(exist = false)
    @JSONField(name = "show_check_availability")
    @JsonProperty("show_check_availability")
    private Boolean showCheckAvailability;
    /**
     * 显示批次文本
     */
    @TableField(exist = false)
    @JSONField(name = "show_lots_text")
    @JsonProperty("show_lots_text")
    private Boolean showLotsText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 欠单
     */
    @TableField(exist = false)
    @JSONField(name = "backorder_id_text")
    @JsonProperty("backorder_id_text")
    private String backorderIdText;
    /**
     * 作业类型
     */
    @TableField(exist = false)
    @JSONField(name = "picking_type_id_text")
    @JsonProperty("picking_type_id_text")
    private String pickingTypeIdText;
    /**
     * 业务伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 源位置
     */
    @TableField(exist = false)
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;
    /**
     * 目的位置
     */
    @TableField(exist = false)
    @JSONField(name = "location_dest_id_text")
    @JsonProperty("location_dest_id_text")
    private String locationDestIdText;
    /**
     * 作业的类型
     */
    @TableField(exist = false)
    @JSONField(name = "picking_type_code")
    @JsonProperty("picking_type_code")
    private String pickingTypeCode;
    /**
     * 移动整个包裹
     */
    @TableField(exist = false)
    @JSONField(name = "picking_type_entire_packs")
    @JsonProperty("picking_type_entire_packs")
    private Boolean pickingTypeEntirePacks;
    /**
     * 所有者
     */
    @TableField(exist = false)
    @JSONField(name = "owner_id_text")
    @JsonProperty("owner_id_text")
    private String ownerIdText;
    /**
     * 销售订单
     */
    @TableField(exist = false)
    @JSONField(name = "sale_id_text")
    @JsonProperty("sale_id_text")
    private String saleIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 作业类型
     */
    @DEField(name = "picking_type_id")
    @TableField(value = "picking_type_id")
    @JSONField(name = "picking_type_id")
    @JsonProperty("picking_type_id")
    private Long pickingTypeId;
    /**
     * 销售订单
     */
    @DEField(name = "sale_id")
    @TableField(value = "sale_id")
    @JSONField(name = "sale_id")
    @JsonProperty("sale_id")
    private Long saleId;
    /**
     * 目的位置
     */
    @DEField(name = "location_dest_id")
    @TableField(value = "location_dest_id")
    @JSONField(name = "location_dest_id")
    @JsonProperty("location_dest_id")
    private Long locationDestId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 所有者
     */
    @DEField(name = "owner_id")
    @TableField(value = "owner_id")
    @JSONField(name = "owner_id")
    @JsonProperty("owner_id")
    private Long ownerId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 公司
     */
    @DEField(name = "company_id" , preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 源位置
     */
    @DEField(name = "location_id")
    @TableField(value = "location_id")
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Long locationId;
    /**
     * 欠单
     */
    @DEField(name = "backorder_id")
    @TableField(value = "backorder_id")
    @JSONField(name = "backorder_id")
    @JsonProperty("backorder_id")
    private Long backorderId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooOwner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order odooSale;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocationDest;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooPickingType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking odooBackorder;



    /**
     * 设置 [备注]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [已打印]
     */
    public void setPrinted(Boolean printed){
        this.printed = printed ;
        this.modify("printed",printed);
    }

    /**
     * 设置 [送货策略]
     */
    public void setMoveType(String moveType){
        this.moveType = moveType ;
        this.modify("move_type",moveType);
    }

    /**
     * 设置 [源文档]
     */
    public void setOrigin(String origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [是锁定]
     */
    public void setIsLocked(Boolean isLocked){
        this.isLocked = isLocked ;
        this.modify("is_locked",isLocked);
    }

    /**
     * 设置 [补货组]
     */
    public void setGroupId(Integer groupId){
        this.groupId = groupId ;
        this.modify("group_id",groupId);
    }

    /**
     * 设置 [创建日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 格式化日期 [创建日期]
     */
    public String formatDate(){
        if (this.date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
    /**
     * 设置 [调拨日期]
     */
    public void setDateDone(Timestamp dateDone){
        this.dateDone = dateDone ;
        this.modify("date_done",dateDone);
    }

    /**
     * 格式化日期 [调拨日期]
     */
    public String formatDateDone(){
        if (this.dateDone == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateDone);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [立即调拨]
     */
    public void setImmediateTransfer(Boolean immediateTransfer){
        this.immediateTransfer = immediateTransfer ;
        this.modify("immediate_transfer",immediateTransfer);
    }

    /**
     * 设置 [编号]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [优先级]
     */
    public void setPriority(String priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [预定交货日期]
     */
    public void setScheduledDate(Timestamp scheduledDate){
        this.scheduledDate = scheduledDate ;
        this.modify("scheduled_date",scheduledDate);
    }

    /**
     * 格式化日期 [预定交货日期]
     */
    public String formatScheduledDate(){
        if (this.scheduledDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduledDate);
    }
    /**
     * 设置 [作业类型]
     */
    public void setPickingTypeId(Long pickingTypeId){
        this.pickingTypeId = pickingTypeId ;
        this.modify("picking_type_id",pickingTypeId);
    }

    /**
     * 设置 [销售订单]
     */
    public void setSaleId(Long saleId){
        this.saleId = saleId ;
        this.modify("sale_id",saleId);
    }

    /**
     * 设置 [目的位置]
     */
    public void setLocationDestId(Long locationDestId){
        this.locationDestId = locationDestId ;
        this.modify("location_dest_id",locationDestId);
    }

    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [所有者]
     */
    public void setOwnerId(Long ownerId){
        this.ownerId = ownerId ;
        this.modify("owner_id",ownerId);
    }

    /**
     * 设置 [源位置]
     */
    public void setLocationId(Long locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }

    /**
     * 设置 [欠单]
     */
    public void setBackorderId(Long backorderId){
        this.backorderId = backorderId ;
        this.modify("backorder_id",backorderId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


