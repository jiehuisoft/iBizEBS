package cn.ibizlab.businesscentral.core.odoo_payment.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_transaction;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_transactionSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Payment_transaction] 服务对象接口
 */
public interface IPayment_transactionService extends IService<Payment_transaction>{

    boolean create(Payment_transaction et) ;
    void createBatch(List<Payment_transaction> list) ;
    boolean update(Payment_transaction et) ;
    void updateBatch(List<Payment_transaction> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Payment_transaction get(Long key) ;
    Payment_transaction getDraft(Payment_transaction et) ;
    boolean checkKey(Payment_transaction et) ;
    boolean save(Payment_transaction et) ;
    void saveBatch(List<Payment_transaction> list) ;
    Page<Payment_transaction> searchDefault(Payment_transactionSearchContext context) ;
    List<Payment_transaction> selectByPaymentId(Long id);
    void resetByPaymentId(Long id);
    void resetByPaymentId(Collection<Long> ids);
    void removeByPaymentId(Long id);
    List<Payment_transaction> selectByAcquirerId(Long id);
    void resetByAcquirerId(Long id);
    void resetByAcquirerId(Collection<Long> ids);
    void removeByAcquirerId(Long id);
    List<Payment_transaction> selectByPaymentTokenId(Long id);
    void resetByPaymentTokenId(Long id);
    void resetByPaymentTokenId(Collection<Long> ids);
    void removeByPaymentTokenId(Long id);
    List<Payment_transaction> selectByPartnerCountryId(Long id);
    void resetByPartnerCountryId(Long id);
    void resetByPartnerCountryId(Collection<Long> ids);
    void removeByPartnerCountryId(Long id);
    List<Payment_transaction> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Payment_transaction> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Payment_transaction> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Payment_transaction> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Payment_transaction> getPaymentTransactionByIds(List<Long> ids) ;
    List<Payment_transaction> getPaymentTransactionByEntities(List<Payment_transaction> entities) ;
}


