package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_invoice_line] 服务对象接口
 */
public interface IAccount_invoice_lineService extends IService<Account_invoice_line>{

    boolean create(Account_invoice_line et) ;
    void createBatch(List<Account_invoice_line> list) ;
    boolean update(Account_invoice_line et) ;
    void updateBatch(List<Account_invoice_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_invoice_line get(Long key) ;
    Account_invoice_line getDraft(Account_invoice_line et) ;
    boolean checkKey(Account_invoice_line et) ;
    boolean save(Account_invoice_line et) ;
    void saveBatch(List<Account_invoice_line> list) ;
    Page<Account_invoice_line> searchDefault(Account_invoice_lineSearchContext context) ;
    List<Account_invoice_line> selectByAccountId(Long id);
    void resetByAccountId(Long id);
    void resetByAccountId(Collection<Long> ids);
    void removeByAccountId(Long id);
    List<Account_invoice_line> selectByAccountAnalyticId(Long id);
    void resetByAccountAnalyticId(Long id);
    void resetByAccountAnalyticId(Collection<Long> ids);
    void removeByAccountAnalyticId(Long id);
    List<Account_invoice_line> selectByInvoiceId(Long id);
    void removeByInvoiceId(Collection<Long> ids);
    void removeByInvoiceId(Long id);
    List<Account_invoice_line> selectByProductId(Long id);
    List<Account_invoice_line> selectByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Account_invoice_line> selectByPurchaseLineId(Long id);
    void resetByPurchaseLineId(Long id);
    void resetByPurchaseLineId(Collection<Long> ids);
    void removeByPurchaseLineId(Long id);
    List<Account_invoice_line> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_invoice_line> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Account_invoice_line> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Account_invoice_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_invoice_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Account_invoice_line> selectByUomId(Long id);
    void resetByUomId(Long id);
    void resetByUomId(Collection<Long> ids);
    void removeByUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_invoice_line> getAccountInvoiceLineByIds(List<Long> ids) ;
    List<Account_invoice_line> getAccountInvoiceLineByEntities(List<Account_invoice_line> entities) ;
}


