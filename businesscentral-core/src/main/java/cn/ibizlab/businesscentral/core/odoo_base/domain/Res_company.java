package cn.ibizlab.businesscentral.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[公司]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "RES_COMPANY",resultMap = "Res_companyResultMap")
public class Res_company extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 公司口号
     */
    @DEField(name = "report_header")
    @TableField(value = "report_header")
    @JSONField(name = "report_header")
    @JsonProperty("report_header")
    private String reportHeader;
    /**
     * 国家/地区
     */
    @TableField(exist = false)
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;
    /**
     * 正进行销售面板的状态
     */
    @DEField(name = "sale_quotation_onboarding_state")
    @TableField(value = "sale_quotation_onboarding_state")
    @JSONField(name = "sale_quotation_onboarding_state")
    @JsonProperty("sale_quotation_onboarding_state")
    private String saleQuotationOnboardingState;
    /**
     * 默认报价有效期（日）
     */
    @DEField(name = "quotation_validity_days")
    @TableField(value = "quotation_validity_days")
    @JSONField(name = "quotation_validity_days")
    @JsonProperty("quotation_validity_days")
    private Integer quotationValidityDays;
    /**
     * 有待被确认的发票步骤的状态
     */
    @DEField(name = "account_onboarding_invoice_layout_state")
    @TableField(value = "account_onboarding_invoice_layout_state")
    @JSONField(name = "account_onboarding_invoice_layout_state")
    @JsonProperty("account_onboarding_invoice_layout_state")
    private String accountOnboardingInvoiceLayoutState;
    /**
     * 科目号码
     */
    @DEField(name = "account_no")
    @TableField(value = "account_no")
    @JSONField(name = "account_no")
    @JsonProperty("account_no")
    private String accountNo;
    /**
     * 银行科目的前缀
     */
    @DEField(name = "bank_account_code_prefix")
    @TableField(value = "bank_account_code_prefix")
    @JSONField(name = "bank_account_code_prefix")
    @JsonProperty("bank_account_code_prefix")
    private String bankAccountCodePrefix;
    /**
     * 银行日记账
     */
    @TableField(exist = false)
    @JSONField(name = "bank_journal_ids")
    @JsonProperty("bank_journal_ids")
    private String bankJournalIds;
    /**
     * 邮政编码
     */
    @TableField(exist = false)
    @JSONField(name = "zip")
    @JsonProperty("zip")
    private String zip;
    /**
     * 非顾问的锁定日期
     */
    @DEField(name = "period_lock_date")
    @TableField(value = "period_lock_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "period_lock_date" , format="yyyy-MM-dd")
    @JsonProperty("period_lock_date")
    private Timestamp periodLockDate;
    /**
     * 状态
     */
    @TableField(exist = false)
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    private Integer stateId;
    /**
     * 使用现金收付制
     */
    @DEField(name = "tax_exigibility")
    @TableField(value = "tax_exigibility")
    @JSONField(name = "tax_exigibility")
    @JsonProperty("tax_exigibility")
    private Boolean taxExigibility;
    /**
     * 工作时间
     */
    @TableField(exist = false)
    @JSONField(name = "resource_calendar_ids")
    @JsonProperty("resource_calendar_ids")
    private String resourceCalendarIds;
    /**
     * 银行账户
     */
    @TableField(exist = false)
    @JSONField(name = "bank_ids")
    @JsonProperty("bank_ids")
    private String bankIds;
    /**
     * 处于会计面板的状态
     */
    @DEField(name = "account_dashboard_onboarding_state")
    @TableField(value = "account_dashboard_onboarding_state")
    @JSONField(name = "account_dashboard_onboarding_state")
    @JsonProperty("account_dashboard_onboarding_state")
    private String accountDashboardOnboardingState;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 颜色
     */
    @DEField(name = "snailmail_color")
    @TableField(value = "snailmail_color")
    @JSONField(name = "snailmail_color")
    @JsonProperty("snailmail_color")
    private Boolean snailmailColor;
    /**
     * 城市
     */
    @TableField(exist = false)
    @JSONField(name = "city")
    @JsonProperty("city")
    private String city;
    /**
     * 科目状态
     */
    @DEField(name = "account_setup_coa_state")
    @TableField(value = "account_setup_coa_state")
    @JSONField(name = "account_setup_coa_state")
    @JsonProperty("account_setup_coa_state")
    private String accountSetupCoaState;
    /**
     * 预设邮件
     */
    @TableField(exist = false)
    @JSONField(name = "catchall")
    @JsonProperty("catchall")
    private String catchall;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 使用anglo-saxon会计
     */
    @DEField(name = "anglo_saxon_accounting")
    @TableField(value = "anglo_saxon_accounting")
    @JSONField(name = "anglo_saxon_accounting")
    @JsonProperty("anglo_saxon_accounting")
    private Boolean angloSaxonAccounting;
    /**
     * 双面
     */
    @DEField(name = "snailmail_duplex")
    @TableField(value = "snailmail_duplex")
    @JSONField(name = "snailmail_duplex")
    @JsonProperty("snailmail_duplex")
    private Boolean snailmailDuplex;
    /**
     * GitHub账户
     */
    @DEField(name = "social_github")
    @TableField(value = "social_github")
    @JSONField(name = "social_github")
    @JsonProperty("social_github")
    private String socialGithub;
    /**
     * 有待被确认的银行数据步骤的状态
     */
    @DEField(name = "account_setup_bank_data_state")
    @TableField(value = "account_setup_bank_data_state")
    @JSONField(name = "account_setup_bank_data_state")
    @JsonProperty("account_setup_bank_data_state")
    private String accountSetupBankDataState;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 街道 2
     */
    @TableField(exist = false)
    @JSONField(name = "street2")
    @JsonProperty("street2")
    private String street2;
    /**
     * 预计会计科目表
     */
    @DEField(name = "expects_chart_of_accounts")
    @TableField(value = "expects_chart_of_accounts")
    @JSONField(name = "expects_chart_of_accounts")
    @JsonProperty("expects_chart_of_accounts")
    private Boolean expectsChartOfAccounts;
    /**
     * 转账帐户的前缀
     */
    @DEField(name = "transfer_account_code_prefix")
    @TableField(value = "transfer_account_code_prefix")
    @JSONField(name = "transfer_account_code_prefix")
    @JsonProperty("transfer_account_code_prefix")
    private String transferAccountCodePrefix;
    /**
     * 会计年度的最后一天
     */
    @DEField(name = "fiscalyear_last_day")
    @TableField(value = "fiscalyear_last_day")
    @JSONField(name = "fiscalyear_last_day")
    @JsonProperty("fiscalyear_last_day")
    private Integer fiscalyearLastDay;
    /**
     * 接受的用户
     */
    @TableField(exist = false)
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;
    /**
     * 银行核销阈值
     */
    @DEField(name = "account_bank_reconciliation_start")
    @TableField(value = "account_bank_reconciliation_start")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "account_bank_reconciliation_start" , format="yyyy-MM-dd")
    @JsonProperty("account_bank_reconciliation_start")
    private Timestamp accountBankReconciliationStart;
    /**
     * 在线支付
     */
    @DEField(name = "portal_confirmation_pay")
    @TableField(value = "portal_confirmation_pay")
    @JSONField(name = "portal_confirmation_pay")
    @JsonProperty("portal_confirmation_pay")
    private Boolean portalConfirmationPay;
    /**
     * 显示SEPA QR码
     */
    @DEField(name = "qr_code")
    @TableField(value = "qr_code")
    @JSONField(name = "qr_code")
    @JsonProperty("qr_code")
    private Boolean qrCode;
    /**
     * 街道
     */
    @TableField(exist = false)
    @JSONField(name = "street")
    @JsonProperty("street")
    private String street;
    /**
     * 处于会计发票面板的状态
     */
    @DEField(name = "account_invoice_onboarding_state")
    @TableField(value = "account_invoice_onboarding_state")
    @JSONField(name = "account_invoice_onboarding_state")
    @JsonProperty("account_invoice_onboarding_state")
    private String accountInvoiceOnboardingState;
    /**
     * 下级公司
     */
    @TableField(exist = false)
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 命名规则
     */
    @DEField(name = "nomenclature_id")
    @TableField(value = "nomenclature_id")
    @JSONField(name = "nomenclature_id")
    @JsonProperty("nomenclature_id")
    private Integer nomenclatureId;
    /**
     * 入职支付收单机构的状态
     */
    @DEField(name = "payment_acquirer_onboarding_state")
    @TableField(value = "payment_acquirer_onboarding_state")
    @JSONField(name = "payment_acquirer_onboarding_state")
    @JsonProperty("payment_acquirer_onboarding_state")
    private String paymentAcquirerOnboardingState;
    /**
     * 报表页脚
     */
    @DEField(name = "report_footer")
    @TableField(value = "report_footer")
    @JSONField(name = "report_footer")
    @JsonProperty("report_footer")
    private String reportFooter;
    /**
     * 选择付款方式
     */
    @DEField(name = "payment_onboarding_payment_method")
    @TableField(value = "payment_onboarding_payment_method")
    @JSONField(name = "payment_onboarding_payment_method")
    @JsonProperty("payment_onboarding_payment_method")
    private String paymentOnboardingPaymentMethod;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 批准等级
     */
    @DEField(name = "po_double_validation")
    @TableField(value = "po_double_validation")
    @JSONField(name = "po_double_validation")
    @JsonProperty("po_double_validation")
    private String poDoubleValidation;
    /**
     * 采购提前时间
     */
    @DEField(name = "po_lead")
    @TableField(value = "po_lead")
    @JSONField(name = "po_lead")
    @JsonProperty("po_lead")
    private Double poLead;
    /**
     * 有待被确认的样品报价单步骤的状态
     */
    @DEField(name = "sale_onboarding_sample_quotation_state")
    @TableField(value = "sale_onboarding_sample_quotation_state")
    @JSONField(name = "sale_onboarding_sample_quotation_state")
    @JsonProperty("sale_onboarding_sample_quotation_state")
    private String saleOnboardingSampleQuotationState;
    /**
     * 有待被确认的订单步骤的状态
     */
    @DEField(name = "sale_onboarding_order_confirmation_state")
    @TableField(value = "sale_onboarding_order_confirmation_state")
    @JSONField(name = "sale_onboarding_order_confirmation_state")
    @JsonProperty("sale_onboarding_order_confirmation_state")
    private String saleOnboardingOrderConfirmationState;
    /**
     * 文档模板
     */
    @DEField(name = "external_report_layout_id")
    @TableField(value = "external_report_layout_id")
    @JSONField(name = "external_report_layout_id")
    @JsonProperty("external_report_layout_id")
    private Integer externalReportLayoutId;
    /**
     * 请选择付款方式
     */
    @DEField(name = "sale_onboarding_payment_method")
    @TableField(value = "sale_onboarding_payment_method")
    @JSONField(name = "sale_onboarding_payment_method")
    @JsonProperty("sale_onboarding_payment_method")
    private String saleOnboardingPaymentMethod;
    /**
     * 有待被确认的样品报价单步骤的状态
     */
    @DEField(name = "account_onboarding_sample_invoice_state")
    @TableField(value = "account_onboarding_sample_invoice_state")
    @JSONField(name = "account_onboarding_sample_invoice_state")
    @JsonProperty("account_onboarding_sample_invoice_state")
    private String accountOnboardingSampleInvoiceState;
    /**
     * 公司状态
     */
    @DEField(name = "base_onboarding_company_state")
    @TableField(value = "base_onboarding_company_state")
    @JSONField(name = "base_onboarding_company_state")
    @JsonProperty("base_onboarding_company_state")
    private String baseOnboardingCompanyState;
    /**
     * 领英账号
     */
    @DEField(name = "social_linkedin")
    @TableField(value = "social_linkedin")
    @JSONField(name = "social_linkedin")
    @JsonProperty("social_linkedin")
    private String socialLinkedin;
    /**
     * 制造提前期(日)
     */
    @DEField(name = "manufacturing_lead")
    @TableField(value = "manufacturing_lead")
    @JSONField(name = "manufacturing_lead")
    @JsonProperty("manufacturing_lead")
    private Double manufacturingLead;
    /**
     * 再次验证金额
     */
    @DEField(name = "po_double_validation_amount")
    @TableField(value = "po_double_validation_amount")
    @JSONField(name = "po_double_validation_amount")
    @JsonProperty("po_double_validation_amount")
    private BigDecimal poDoubleValidationAmount;
    /**
     * 销售订单修改
     */
    @DEField(name = "po_lock")
    @TableField(value = "po_lock")
    @JSONField(name = "po_lock")
    @JsonProperty("po_lock")
    private String poLock;
    /**
     * Twitter账号
     */
    @DEField(name = "social_twitter")
    @TableField(value = "social_twitter")
    @JSONField(name = "social_twitter")
    @JsonProperty("social_twitter")
    private String socialTwitter;
    /**
     * Instagram 账号
     */
    @DEField(name = "social_instagram")
    @TableField(value = "social_instagram")
    @JSONField(name = "social_instagram")
    @JsonProperty("social_instagram")
    private String socialInstagram;
    /**
     * 有待被确认的会计年度步骤的状态
     */
    @DEField(name = "account_setup_fy_data_state")
    @TableField(value = "account_setup_fy_data_state")
    @JSONField(name = "account_setup_fy_data_state")
    @JsonProperty("account_setup_fy_data_state")
    private String accountSetupFyDataState;
    /**
     * 税率计算的舍入方法
     */
    @DEField(name = "tax_calculation_rounding_method")
    @TableField(value = "tax_calculation_rounding_method")
    @JSONField(name = "tax_calculation_rounding_method")
    @JsonProperty("tax_calculation_rounding_method")
    private String taxCalculationRoundingMethod;
    /**
     * 现金科目的前缀
     */
    @DEField(name = "cash_account_code_prefix")
    @TableField(value = "cash_account_code_prefix")
    @JSONField(name = "cash_account_code_prefix")
    @JsonProperty("cash_account_code_prefix")
    private String cashAccountCodePrefix;
    /**
     * 有待被确认的报价单步骤的状态
     */
    @DEField(name = "account_onboarding_sale_tax_state")
    @TableField(value = "account_onboarding_sale_tax_state")
    @JSONField(name = "account_onboarding_sale_tax_state")
    @JsonProperty("account_onboarding_sale_tax_state")
    private String accountOnboardingSaleTaxState;
    /**
     * 销售安全天数
     */
    @DEField(name = "security_lead")
    @TableField(value = "security_lead")
    @JSONField(name = "security_lead")
    @JsonProperty("security_lead")
    private Double securityLead;
    /**
     * 招聘网站主题一步完成
     */
    @TableField(exist = false)
    @JSONField(name = "website_theme_onboarding_done")
    @JsonProperty("website_theme_onboarding_done")
    private Boolean websiteThemeOnboardingDone;
    /**
     * 通过默认值打印
     */
    @DEField(name = "invoice_is_print")
    @TableField(value = "invoice_is_print")
    @JSONField(name = "invoice_is_print")
    @JsonProperty("invoice_is_print")
    private Boolean invoiceIsPrint;
    /**
     * 公司注册
     */
    @DEField(name = "company_registry")
    @TableField(value = "company_registry")
    @JSONField(name = "company_registry")
    @JsonProperty("company_registry")
    private String companyRegistry;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 网页徽标
     */
    @TableField(exist = false)
    @JSONField(name = "logo_web")
    @JsonProperty("logo_web")
    private byte[] logoWeb;
    /**
     * 锁定日期
     */
    @DEField(name = "fiscalyear_lock_date")
    @TableField(value = "fiscalyear_lock_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "fiscalyear_lock_date" , format="yyyy-MM-dd")
    @JsonProperty("fiscalyear_lock_date")
    private Timestamp fiscalyearLockDate;
    /**
     * 默认以信件发送
     */
    @DEField(name = "invoice_is_snailmail")
    @TableField(value = "invoice_is_snailmail")
    @JSONField(name = "invoice_is_snailmail")
    @JsonProperty("invoice_is_snailmail")
    private Boolean invoiceIsSnailmail;
    /**
     * 网站销售状态入职付款收单机构步骤
     */
    @DEField(name = "website_sale_onboarding_payment_acquirer_state")
    @TableField(value = "website_sale_onboarding_payment_acquirer_state")
    @JSONField(name = "website_sale_onboarding_payment_acquirer_state")
    @JsonProperty("website_sale_onboarding_payment_acquirer_state")
    private String websiteSaleOnboardingPaymentAcquirerState;
    /**
     * 脸书账号
     */
    @DEField(name = "social_facebook")
    @TableField(value = "social_facebook")
    @JSONField(name = "social_facebook")
    @JsonProperty("social_facebook")
    private String socialFacebook;
    /**
     * 在线签名
     */
    @DEField(name = "portal_confirmation_sign")
    @TableField(value = "portal_confirmation_sign")
    @JSONField(name = "portal_confirmation_sign")
    @JsonProperty("portal_confirmation_sign")
    private Boolean portalConfirmationSign;
    /**
     * 纸张格式
     */
    @DEField(name = "paperformat_id")
    @TableField(value = "paperformat_id")
    @JSONField(name = "paperformat_id")
    @JsonProperty("paperformat_id")
    private Integer paperformatId;
    /**
     * 会计年度的最后一个月
     */
    @DEField(name = "fiscalyear_last_month")
    @TableField(value = "fiscalyear_last_month")
    @JSONField(name = "fiscalyear_last_month")
    @JsonProperty("fiscalyear_last_month")
    private String fiscalyearLastMonth;
    /**
     * 默认邮件
     */
    @DEField(name = "invoice_is_email")
    @TableField(value = "invoice_is_email")
    @JSONField(name = "invoice_is_email")
    @JsonProperty("invoice_is_email")
    private Boolean invoiceIsEmail;
    /**
     * Youtube账号
     */
    @DEField(name = "social_youtube")
    @TableField(value = "social_youtube")
    @JSONField(name = "social_youtube")
    @JsonProperty("social_youtube")
    private String socialYoutube;
    /**
     * 汇率损失科目
     */
    @TableField(exist = false)
    @JSONField(name = "expense_currency_exchange_account_id")
    @JsonProperty("expense_currency_exchange_account_id")
    private Long expenseCurrencyExchangeAccountId;
    /**
     * 公司数据库ID
     */
    @TableField(exist = false)
    @JSONField(name = "partner_gid")
    @JsonProperty("partner_gid")
    private Integer partnerGid;
    /**
     * 电话
     */
    @TableField(exist = false)
    @JSONField(name = "phone")
    @JsonProperty("phone")
    private String phone;
    /**
     * 公司 Logo
     */
    @TableField(exist = false)
    @JSONField(name = "logo")
    @JsonProperty("logo")
    private byte[] logo;
    /**
     * 库存计价的入库科目
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_account_input_categ_id_text")
    @JsonProperty("property_stock_account_input_categ_id_text")
    private String propertyStockAccountInputCategIdText;
    /**
     * 默认进项税
     */
    @TableField(exist = false)
    @JSONField(name = "account_purchase_tax_id_text")
    @JsonProperty("account_purchase_tax_id_text")
    private String accountPurchaseTaxIdText;
    /**
     * 公司名称
     */
    @TableField(exist = false)
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 默认国际贸易术语
     */
    @TableField(exist = false)
    @JSONField(name = "incoterm_id_text")
    @JsonProperty("incoterm_id_text")
    private String incotermIdText;
    /**
     * 期初日记账
     */
    @TableField(exist = false)
    @JSONField(name = "account_opening_journal_id")
    @JsonProperty("account_opening_journal_id")
    private Long accountOpeningJournalId;
    /**
     * 银行间转账科目
     */
    @TableField(exist = false)
    @JSONField(name = "transfer_account_id_text")
    @JsonProperty("transfer_account_id_text")
    private String transferAccountIdText;
    /**
     * 汇率增益科目
     */
    @TableField(exist = false)
    @JSONField(name = "income_currency_exchange_account_id")
    @JsonProperty("income_currency_exchange_account_id")
    private Long incomeCurrencyExchangeAccountId;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;
    /**
     * 期初日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "account_opening_date" , format="yyyy-MM-dd")
    @JsonProperty("account_opening_date")
    private Timestamp accountOpeningDate;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 表模板
     */
    @TableField(exist = false)
    @JSONField(name = "chart_template_id_text")
    @JsonProperty("chart_template_id_text")
    private String chartTemplateIdText;
    /**
     * 默认销售税
     */
    @TableField(exist = false)
    @JSONField(name = "account_sale_tax_id_text")
    @JsonProperty("account_sale_tax_id_text")
    private String accountSaleTaxIdText;
    /**
     * 期初日记账分录
     */
    @TableField(exist = false)
    @JSONField(name = "account_opening_move_id_text")
    @JsonProperty("account_opening_move_id_text")
    private String accountOpeningMoveIdText;
    /**
     * EMail
     */
    @TableField(exist = false)
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 库存计价的出货科目
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_account_output_categ_id_text")
    @JsonProperty("property_stock_account_output_categ_id_text")
    private String propertyStockAccountOutputCategIdText;
    /**
     * 库存计价的科目模板
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_valuation_account_id_text")
    @JsonProperty("property_stock_valuation_account_id_text")
    private String propertyStockValuationAccountIdText;
    /**
     * 上级公司
     */
    @TableField(exist = false)
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    private String parentIdText;
    /**
     * 现金收付制日记账
     */
    @TableField(exist = false)
    @JSONField(name = "tax_cash_basis_journal_id_text")
    @JsonProperty("tax_cash_basis_journal_id_text")
    private String taxCashBasisJournalIdText;
    /**
     * 内部中转位置
     */
    @TableField(exist = false)
    @JSONField(name = "internal_transit_location_id_text")
    @JsonProperty("internal_transit_location_id_text")
    private String internalTransitLocationIdText;
    /**
     * 网站
     */
    @TableField(exist = false)
    @JSONField(name = "website")
    @JsonProperty("website")
    private String website;
    /**
     * 税号
     */
    @TableField(exist = false)
    @JSONField(name = "vat")
    @JsonProperty("vat")
    private String vat;
    /**
     * 默认工作时间
     */
    @TableField(exist = false)
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    private String resourceCalendarIdText;
    /**
     * 汇兑损益
     */
    @TableField(exist = false)
    @JSONField(name = "currency_exchange_journal_id_text")
    @JsonProperty("currency_exchange_journal_id_text")
    private String currencyExchangeJournalIdText;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 上级公司
     */
    @DEField(name = "parent_id")
    @TableField(value = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Long parentId;
    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @TableField(value = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 库存计价的出货科目
     */
    @DEField(name = "property_stock_account_output_categ_id")
    @TableField(value = "property_stock_account_output_categ_id")
    @JSONField(name = "property_stock_account_output_categ_id")
    @JsonProperty("property_stock_account_output_categ_id")
    private Long propertyStockAccountOutputCategId;
    /**
     * 库存计价的科目模板
     */
    @DEField(name = "property_stock_valuation_account_id")
    @TableField(value = "property_stock_valuation_account_id")
    @JSONField(name = "property_stock_valuation_account_id")
    @JsonProperty("property_stock_valuation_account_id")
    private Long propertyStockValuationAccountId;
    /**
     * 期初日记账分录
     */
    @DEField(name = "account_opening_move_id")
    @TableField(value = "account_opening_move_id")
    @JSONField(name = "account_opening_move_id")
    @JsonProperty("account_opening_move_id")
    private Long accountOpeningMoveId;
    /**
     * 内部中转位置
     */
    @DEField(name = "internal_transit_location_id")
    @TableField(value = "internal_transit_location_id")
    @JSONField(name = "internal_transit_location_id")
    @JsonProperty("internal_transit_location_id")
    private Long internalTransitLocationId;
    /**
     * 默认进项税
     */
    @DEField(name = "account_purchase_tax_id")
    @TableField(value = "account_purchase_tax_id")
    @JSONField(name = "account_purchase_tax_id")
    @JsonProperty("account_purchase_tax_id")
    private Long accountPurchaseTaxId;
    /**
     * 表模板
     */
    @DEField(name = "chart_template_id")
    @TableField(value = "chart_template_id")
    @JSONField(name = "chart_template_id")
    @JsonProperty("chart_template_id")
    private Long chartTemplateId;
    /**
     * 默认销售税
     */
    @DEField(name = "account_sale_tax_id")
    @TableField(value = "account_sale_tax_id")
    @JSONField(name = "account_sale_tax_id")
    @JsonProperty("account_sale_tax_id")
    private Long accountSaleTaxId;
    /**
     * 现金收付制日记账
     */
    @DEField(name = "tax_cash_basis_journal_id")
    @TableField(value = "tax_cash_basis_journal_id")
    @JSONField(name = "tax_cash_basis_journal_id")
    @JsonProperty("tax_cash_basis_journal_id")
    private Long taxCashBasisJournalId;
    /**
     * 库存计价的入库科目
     */
    @DEField(name = "property_stock_account_input_categ_id")
    @TableField(value = "property_stock_account_input_categ_id")
    @JSONField(name = "property_stock_account_input_categ_id")
    @JsonProperty("property_stock_account_input_categ_id")
    private Long propertyStockAccountInputCategId;
    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 默认国际贸易术语
     */
    @DEField(name = "incoterm_id")
    @TableField(value = "incoterm_id")
    @JSONField(name = "incoterm_id")
    @JsonProperty("incoterm_id")
    private Long incotermId;
    /**
     * 默认工作时间
     */
    @DEField(name = "resource_calendar_id")
    @TableField(value = "resource_calendar_id")
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Long resourceCalendarId;
    /**
     * 银行间转账科目
     */
    @DEField(name = "transfer_account_id")
    @TableField(value = "transfer_account_id")
    @JSONField(name = "transfer_account_id")
    @JsonProperty("transfer_account_id")
    private Long transferAccountId;
    /**
     * 汇兑损益
     */
    @DEField(name = "currency_exchange_journal_id")
    @TableField(value = "currency_exchange_journal_id")
    @JSONField(name = "currency_exchange_journal_id")
    @JsonProperty("currency_exchange_journal_id")
    private Long currencyExchangeJournalId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooPropertyStockAccountInputCateg;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooPropertyStockAccountOutputCateg;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooPropertyStockValuationAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooTransferAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template odooChartTemplate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms odooIncoterm;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooCurrencyExchangeJournal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooTaxCashBasisJournal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move odooAccountOpeningMove;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax odooAccountPurchaseTax;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax odooAccountSaleTax;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar odooResourceCalendar;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooParent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooInternalTransitLocation;



    /**
     * 设置 [公司口号]
     */
    public void setReportHeader(String reportHeader){
        this.reportHeader = reportHeader ;
        this.modify("report_header",reportHeader);
    }

    /**
     * 设置 [正进行销售面板的状态]
     */
    public void setSaleQuotationOnboardingState(String saleQuotationOnboardingState){
        this.saleQuotationOnboardingState = saleQuotationOnboardingState ;
        this.modify("sale_quotation_onboarding_state",saleQuotationOnboardingState);
    }

    /**
     * 设置 [默认报价有效期（日）]
     */
    public void setQuotationValidityDays(Integer quotationValidityDays){
        this.quotationValidityDays = quotationValidityDays ;
        this.modify("quotation_validity_days",quotationValidityDays);
    }

    /**
     * 设置 [有待被确认的发票步骤的状态]
     */
    public void setAccountOnboardingInvoiceLayoutState(String accountOnboardingInvoiceLayoutState){
        this.accountOnboardingInvoiceLayoutState = accountOnboardingInvoiceLayoutState ;
        this.modify("account_onboarding_invoice_layout_state",accountOnboardingInvoiceLayoutState);
    }

    /**
     * 设置 [科目号码]
     */
    public void setAccountNo(String accountNo){
        this.accountNo = accountNo ;
        this.modify("account_no",accountNo);
    }

    /**
     * 设置 [银行科目的前缀]
     */
    public void setBankAccountCodePrefix(String bankAccountCodePrefix){
        this.bankAccountCodePrefix = bankAccountCodePrefix ;
        this.modify("bank_account_code_prefix",bankAccountCodePrefix);
    }

    /**
     * 设置 [非顾问的锁定日期]
     */
    public void setPeriodLockDate(Timestamp periodLockDate){
        this.periodLockDate = periodLockDate ;
        this.modify("period_lock_date",periodLockDate);
    }

    /**
     * 格式化日期 [非顾问的锁定日期]
     */
    public String formatPeriodLockDate(){
        if (this.periodLockDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(periodLockDate);
    }
    /**
     * 设置 [使用现金收付制]
     */
    public void setTaxExigibility(Boolean taxExigibility){
        this.taxExigibility = taxExigibility ;
        this.modify("tax_exigibility",taxExigibility);
    }

    /**
     * 设置 [处于会计面板的状态]
     */
    public void setAccountDashboardOnboardingState(String accountDashboardOnboardingState){
        this.accountDashboardOnboardingState = accountDashboardOnboardingState ;
        this.modify("account_dashboard_onboarding_state",accountDashboardOnboardingState);
    }

    /**
     * 设置 [颜色]
     */
    public void setSnailmailColor(Boolean snailmailColor){
        this.snailmailColor = snailmailColor ;
        this.modify("snailmail_color",snailmailColor);
    }

    /**
     * 设置 [科目状态]
     */
    public void setAccountSetupCoaState(String accountSetupCoaState){
        this.accountSetupCoaState = accountSetupCoaState ;
        this.modify("account_setup_coa_state",accountSetupCoaState);
    }

    /**
     * 设置 [使用anglo-saxon会计]
     */
    public void setAngloSaxonAccounting(Boolean angloSaxonAccounting){
        this.angloSaxonAccounting = angloSaxonAccounting ;
        this.modify("anglo_saxon_accounting",angloSaxonAccounting);
    }

    /**
     * 设置 [双面]
     */
    public void setSnailmailDuplex(Boolean snailmailDuplex){
        this.snailmailDuplex = snailmailDuplex ;
        this.modify("snailmail_duplex",snailmailDuplex);
    }

    /**
     * 设置 [GitHub账户]
     */
    public void setSocialGithub(String socialGithub){
        this.socialGithub = socialGithub ;
        this.modify("social_github",socialGithub);
    }

    /**
     * 设置 [有待被确认的银行数据步骤的状态]
     */
    public void setAccountSetupBankDataState(String accountSetupBankDataState){
        this.accountSetupBankDataState = accountSetupBankDataState ;
        this.modify("account_setup_bank_data_state",accountSetupBankDataState);
    }

    /**
     * 设置 [预计会计科目表]
     */
    public void setExpectsChartOfAccounts(Boolean expectsChartOfAccounts){
        this.expectsChartOfAccounts = expectsChartOfAccounts ;
        this.modify("expects_chart_of_accounts",expectsChartOfAccounts);
    }

    /**
     * 设置 [转账帐户的前缀]
     */
    public void setTransferAccountCodePrefix(String transferAccountCodePrefix){
        this.transferAccountCodePrefix = transferAccountCodePrefix ;
        this.modify("transfer_account_code_prefix",transferAccountCodePrefix);
    }

    /**
     * 设置 [会计年度的最后一天]
     */
    public void setFiscalyearLastDay(Integer fiscalyearLastDay){
        this.fiscalyearLastDay = fiscalyearLastDay ;
        this.modify("fiscalyear_last_day",fiscalyearLastDay);
    }

    /**
     * 设置 [银行核销阈值]
     */
    public void setAccountBankReconciliationStart(Timestamp accountBankReconciliationStart){
        this.accountBankReconciliationStart = accountBankReconciliationStart ;
        this.modify("account_bank_reconciliation_start",accountBankReconciliationStart);
    }

    /**
     * 格式化日期 [银行核销阈值]
     */
    public String formatAccountBankReconciliationStart(){
        if (this.accountBankReconciliationStart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(accountBankReconciliationStart);
    }
    /**
     * 设置 [在线支付]
     */
    public void setPortalConfirmationPay(Boolean portalConfirmationPay){
        this.portalConfirmationPay = portalConfirmationPay ;
        this.modify("portal_confirmation_pay",portalConfirmationPay);
    }

    /**
     * 设置 [显示SEPA QR码]
     */
    public void setQrCode(Boolean qrCode){
        this.qrCode = qrCode ;
        this.modify("qr_code",qrCode);
    }

    /**
     * 设置 [处于会计发票面板的状态]
     */
    public void setAccountInvoiceOnboardingState(String accountInvoiceOnboardingState){
        this.accountInvoiceOnboardingState = accountInvoiceOnboardingState ;
        this.modify("account_invoice_onboarding_state",accountInvoiceOnboardingState);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [命名规则]
     */
    public void setNomenclatureId(Integer nomenclatureId){
        this.nomenclatureId = nomenclatureId ;
        this.modify("nomenclature_id",nomenclatureId);
    }

    /**
     * 设置 [入职支付收单机构的状态]
     */
    public void setPaymentAcquirerOnboardingState(String paymentAcquirerOnboardingState){
        this.paymentAcquirerOnboardingState = paymentAcquirerOnboardingState ;
        this.modify("payment_acquirer_onboarding_state",paymentAcquirerOnboardingState);
    }

    /**
     * 设置 [报表页脚]
     */
    public void setReportFooter(String reportFooter){
        this.reportFooter = reportFooter ;
        this.modify("report_footer",reportFooter);
    }

    /**
     * 设置 [选择付款方式]
     */
    public void setPaymentOnboardingPaymentMethod(String paymentOnboardingPaymentMethod){
        this.paymentOnboardingPaymentMethod = paymentOnboardingPaymentMethod ;
        this.modify("payment_onboarding_payment_method",paymentOnboardingPaymentMethod);
    }

    /**
     * 设置 [批准等级]
     */
    public void setPoDoubleValidation(String poDoubleValidation){
        this.poDoubleValidation = poDoubleValidation ;
        this.modify("po_double_validation",poDoubleValidation);
    }

    /**
     * 设置 [采购提前时间]
     */
    public void setPoLead(Double poLead){
        this.poLead = poLead ;
        this.modify("po_lead",poLead);
    }

    /**
     * 设置 [有待被确认的样品报价单步骤的状态]
     */
    public void setSaleOnboardingSampleQuotationState(String saleOnboardingSampleQuotationState){
        this.saleOnboardingSampleQuotationState = saleOnboardingSampleQuotationState ;
        this.modify("sale_onboarding_sample_quotation_state",saleOnboardingSampleQuotationState);
    }

    /**
     * 设置 [有待被确认的订单步骤的状态]
     */
    public void setSaleOnboardingOrderConfirmationState(String saleOnboardingOrderConfirmationState){
        this.saleOnboardingOrderConfirmationState = saleOnboardingOrderConfirmationState ;
        this.modify("sale_onboarding_order_confirmation_state",saleOnboardingOrderConfirmationState);
    }

    /**
     * 设置 [文档模板]
     */
    public void setExternalReportLayoutId(Integer externalReportLayoutId){
        this.externalReportLayoutId = externalReportLayoutId ;
        this.modify("external_report_layout_id",externalReportLayoutId);
    }

    /**
     * 设置 [请选择付款方式]
     */
    public void setSaleOnboardingPaymentMethod(String saleOnboardingPaymentMethod){
        this.saleOnboardingPaymentMethod = saleOnboardingPaymentMethod ;
        this.modify("sale_onboarding_payment_method",saleOnboardingPaymentMethod);
    }

    /**
     * 设置 [有待被确认的样品报价单步骤的状态]
     */
    public void setAccountOnboardingSampleInvoiceState(String accountOnboardingSampleInvoiceState){
        this.accountOnboardingSampleInvoiceState = accountOnboardingSampleInvoiceState ;
        this.modify("account_onboarding_sample_invoice_state",accountOnboardingSampleInvoiceState);
    }

    /**
     * 设置 [公司状态]
     */
    public void setBaseOnboardingCompanyState(String baseOnboardingCompanyState){
        this.baseOnboardingCompanyState = baseOnboardingCompanyState ;
        this.modify("base_onboarding_company_state",baseOnboardingCompanyState);
    }

    /**
     * 设置 [领英账号]
     */
    public void setSocialLinkedin(String socialLinkedin){
        this.socialLinkedin = socialLinkedin ;
        this.modify("social_linkedin",socialLinkedin);
    }

    /**
     * 设置 [制造提前期(日)]
     */
    public void setManufacturingLead(Double manufacturingLead){
        this.manufacturingLead = manufacturingLead ;
        this.modify("manufacturing_lead",manufacturingLead);
    }

    /**
     * 设置 [再次验证金额]
     */
    public void setPoDoubleValidationAmount(BigDecimal poDoubleValidationAmount){
        this.poDoubleValidationAmount = poDoubleValidationAmount ;
        this.modify("po_double_validation_amount",poDoubleValidationAmount);
    }

    /**
     * 设置 [销售订单修改]
     */
    public void setPoLock(String poLock){
        this.poLock = poLock ;
        this.modify("po_lock",poLock);
    }

    /**
     * 设置 [Twitter账号]
     */
    public void setSocialTwitter(String socialTwitter){
        this.socialTwitter = socialTwitter ;
        this.modify("social_twitter",socialTwitter);
    }

    /**
     * 设置 [Instagram 账号]
     */
    public void setSocialInstagram(String socialInstagram){
        this.socialInstagram = socialInstagram ;
        this.modify("social_instagram",socialInstagram);
    }

    /**
     * 设置 [有待被确认的会计年度步骤的状态]
     */
    public void setAccountSetupFyDataState(String accountSetupFyDataState){
        this.accountSetupFyDataState = accountSetupFyDataState ;
        this.modify("account_setup_fy_data_state",accountSetupFyDataState);
    }

    /**
     * 设置 [税率计算的舍入方法]
     */
    public void setTaxCalculationRoundingMethod(String taxCalculationRoundingMethod){
        this.taxCalculationRoundingMethod = taxCalculationRoundingMethod ;
        this.modify("tax_calculation_rounding_method",taxCalculationRoundingMethod);
    }

    /**
     * 设置 [现金科目的前缀]
     */
    public void setCashAccountCodePrefix(String cashAccountCodePrefix){
        this.cashAccountCodePrefix = cashAccountCodePrefix ;
        this.modify("cash_account_code_prefix",cashAccountCodePrefix);
    }

    /**
     * 设置 [有待被确认的报价单步骤的状态]
     */
    public void setAccountOnboardingSaleTaxState(String accountOnboardingSaleTaxState){
        this.accountOnboardingSaleTaxState = accountOnboardingSaleTaxState ;
        this.modify("account_onboarding_sale_tax_state",accountOnboardingSaleTaxState);
    }

    /**
     * 设置 [销售安全天数]
     */
    public void setSecurityLead(Double securityLead){
        this.securityLead = securityLead ;
        this.modify("security_lead",securityLead);
    }

    /**
     * 设置 [通过默认值打印]
     */
    public void setInvoiceIsPrint(Boolean invoiceIsPrint){
        this.invoiceIsPrint = invoiceIsPrint ;
        this.modify("invoice_is_print",invoiceIsPrint);
    }

    /**
     * 设置 [公司注册]
     */
    public void setCompanyRegistry(String companyRegistry){
        this.companyRegistry = companyRegistry ;
        this.modify("company_registry",companyRegistry);
    }

    /**
     * 设置 [锁定日期]
     */
    public void setFiscalyearLockDate(Timestamp fiscalyearLockDate){
        this.fiscalyearLockDate = fiscalyearLockDate ;
        this.modify("fiscalyear_lock_date",fiscalyearLockDate);
    }

    /**
     * 格式化日期 [锁定日期]
     */
    public String formatFiscalyearLockDate(){
        if (this.fiscalyearLockDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fiscalyearLockDate);
    }
    /**
     * 设置 [默认以信件发送]
     */
    public void setInvoiceIsSnailmail(Boolean invoiceIsSnailmail){
        this.invoiceIsSnailmail = invoiceIsSnailmail ;
        this.modify("invoice_is_snailmail",invoiceIsSnailmail);
    }

    /**
     * 设置 [网站销售状态入职付款收单机构步骤]
     */
    public void setWebsiteSaleOnboardingPaymentAcquirerState(String websiteSaleOnboardingPaymentAcquirerState){
        this.websiteSaleOnboardingPaymentAcquirerState = websiteSaleOnboardingPaymentAcquirerState ;
        this.modify("website_sale_onboarding_payment_acquirer_state",websiteSaleOnboardingPaymentAcquirerState);
    }

    /**
     * 设置 [脸书账号]
     */
    public void setSocialFacebook(String socialFacebook){
        this.socialFacebook = socialFacebook ;
        this.modify("social_facebook",socialFacebook);
    }

    /**
     * 设置 [在线签名]
     */
    public void setPortalConfirmationSign(Boolean portalConfirmationSign){
        this.portalConfirmationSign = portalConfirmationSign ;
        this.modify("portal_confirmation_sign",portalConfirmationSign);
    }

    /**
     * 设置 [纸张格式]
     */
    public void setPaperformatId(Integer paperformatId){
        this.paperformatId = paperformatId ;
        this.modify("paperformat_id",paperformatId);
    }

    /**
     * 设置 [会计年度的最后一个月]
     */
    public void setFiscalyearLastMonth(String fiscalyearLastMonth){
        this.fiscalyearLastMonth = fiscalyearLastMonth ;
        this.modify("fiscalyear_last_month",fiscalyearLastMonth);
    }

    /**
     * 设置 [默认邮件]
     */
    public void setInvoiceIsEmail(Boolean invoiceIsEmail){
        this.invoiceIsEmail = invoiceIsEmail ;
        this.modify("invoice_is_email",invoiceIsEmail);
    }

    /**
     * 设置 [Youtube账号]
     */
    public void setSocialYoutube(String socialYoutube){
        this.socialYoutube = socialYoutube ;
        this.modify("social_youtube",socialYoutube);
    }

    /**
     * 设置 [上级公司]
     */
    public void setParentId(Long parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Long currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [库存计价的出货科目]
     */
    public void setPropertyStockAccountOutputCategId(Long propertyStockAccountOutputCategId){
        this.propertyStockAccountOutputCategId = propertyStockAccountOutputCategId ;
        this.modify("property_stock_account_output_categ_id",propertyStockAccountOutputCategId);
    }

    /**
     * 设置 [库存计价的科目模板]
     */
    public void setPropertyStockValuationAccountId(Long propertyStockValuationAccountId){
        this.propertyStockValuationAccountId = propertyStockValuationAccountId ;
        this.modify("property_stock_valuation_account_id",propertyStockValuationAccountId);
    }

    /**
     * 设置 [期初日记账分录]
     */
    public void setAccountOpeningMoveId(Long accountOpeningMoveId){
        this.accountOpeningMoveId = accountOpeningMoveId ;
        this.modify("account_opening_move_id",accountOpeningMoveId);
    }

    /**
     * 设置 [内部中转位置]
     */
    public void setInternalTransitLocationId(Long internalTransitLocationId){
        this.internalTransitLocationId = internalTransitLocationId ;
        this.modify("internal_transit_location_id",internalTransitLocationId);
    }

    /**
     * 设置 [默认进项税]
     */
    public void setAccountPurchaseTaxId(Long accountPurchaseTaxId){
        this.accountPurchaseTaxId = accountPurchaseTaxId ;
        this.modify("account_purchase_tax_id",accountPurchaseTaxId);
    }

    /**
     * 设置 [表模板]
     */
    public void setChartTemplateId(Long chartTemplateId){
        this.chartTemplateId = chartTemplateId ;
        this.modify("chart_template_id",chartTemplateId);
    }

    /**
     * 设置 [默认销售税]
     */
    public void setAccountSaleTaxId(Long accountSaleTaxId){
        this.accountSaleTaxId = accountSaleTaxId ;
        this.modify("account_sale_tax_id",accountSaleTaxId);
    }

    /**
     * 设置 [现金收付制日记账]
     */
    public void setTaxCashBasisJournalId(Long taxCashBasisJournalId){
        this.taxCashBasisJournalId = taxCashBasisJournalId ;
        this.modify("tax_cash_basis_journal_id",taxCashBasisJournalId);
    }

    /**
     * 设置 [库存计价的入库科目]
     */
    public void setPropertyStockAccountInputCategId(Long propertyStockAccountInputCategId){
        this.propertyStockAccountInputCategId = propertyStockAccountInputCategId ;
        this.modify("property_stock_account_input_categ_id",propertyStockAccountInputCategId);
    }

    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [默认国际贸易术语]
     */
    public void setIncotermId(Long incotermId){
        this.incotermId = incotermId ;
        this.modify("incoterm_id",incotermId);
    }

    /**
     * 设置 [默认工作时间]
     */
    public void setResourceCalendarId(Long resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }

    /**
     * 设置 [银行间转账科目]
     */
    public void setTransferAccountId(Long transferAccountId){
        this.transferAccountId = transferAccountId ;
        this.modify("transfer_account_id",transferAccountId);
    }

    /**
     * 设置 [汇兑损益]
     */
    public void setCurrencyExchangeJournalId(Long currencyExchangeJournalId){
        this.currencyExchangeJournalId = currencyExchangeJournalId ;
        this.modify("currency_exchange_journal_id",currencyExchangeJournalId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


