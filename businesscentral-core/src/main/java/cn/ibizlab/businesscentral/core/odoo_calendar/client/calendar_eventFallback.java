package cn.ibizlab.businesscentral.core.odoo_calendar.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_eventSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[calendar_event] 服务对象接口
 */
@Component
public class calendar_eventFallback implements calendar_eventFeignClient{

    public Calendar_event get(Long id){
            return null;
     }


    public Calendar_event update(Long id, Calendar_event calendar_event){
            return null;
     }
    public Boolean updateBatch(List<Calendar_event> calendar_events){
            return false;
     }


    public Page<Calendar_event> search(Calendar_eventSearchContext context){
            return null;
     }





    public Calendar_event create(Calendar_event calendar_event){
            return null;
     }
    public Boolean createBatch(List<Calendar_event> calendar_events){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Calendar_event> select(){
            return null;
     }

    public Calendar_event getDraft(){
            return null;
    }



    public Boolean checkKey(Calendar_event calendar_event){
            return false;
     }


    public Boolean save(Calendar_event calendar_event){
            return false;
     }
    public Boolean saveBatch(List<Calendar_event> calendar_events){
            return false;
     }

    public Page<Calendar_event> searchDefault(Calendar_eventSearchContext context){
            return null;
     }


}
