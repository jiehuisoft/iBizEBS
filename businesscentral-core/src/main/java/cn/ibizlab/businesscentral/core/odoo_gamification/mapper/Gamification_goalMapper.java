package cn.ibizlab.businesscentral.core.odoo_gamification.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_goalSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Gamification_goalMapper extends BaseMapper<Gamification_goal>{

    Page<Gamification_goal> searchDefault(IPage page, @Param("srf") Gamification_goalSearchContext context, @Param("ew") Wrapper<Gamification_goal> wrapper) ;
    @Override
    Gamification_goal selectById(Serializable id);
    @Override
    int insert(Gamification_goal entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Gamification_goal entity);
    @Override
    int update(@Param(Constants.ENTITY) Gamification_goal entity, @Param("ew") Wrapper<Gamification_goal> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Gamification_goal> selectByLineId(@Param("id") Serializable id) ;

    List<Gamification_goal> selectByChallengeId(@Param("id") Serializable id) ;

    List<Gamification_goal> selectByDefinitionId(@Param("id") Serializable id) ;

    List<Gamification_goal> selectByCreateUid(@Param("id") Serializable id) ;

    List<Gamification_goal> selectByUserId(@Param("id") Serializable id) ;

    List<Gamification_goal> selectByWriteUid(@Param("id") Serializable id) ;


}
