package cn.ibizlab.businesscentral.core.odoo_event.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_confirm;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_confirmSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[event_confirm] 服务对象接口
 */
@Component
public class event_confirmFallback implements event_confirmFeignClient{

    public Page<Event_confirm> search(Event_confirmSearchContext context){
            return null;
     }


    public Event_confirm create(Event_confirm event_confirm){
            return null;
     }
    public Boolean createBatch(List<Event_confirm> event_confirms){
            return false;
     }

    public Event_confirm update(Long id, Event_confirm event_confirm){
            return null;
     }
    public Boolean updateBatch(List<Event_confirm> event_confirms){
            return false;
     }




    public Event_confirm get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Event_confirm> select(){
            return null;
     }

    public Event_confirm getDraft(){
            return null;
    }



    public Boolean checkKey(Event_confirm event_confirm){
            return false;
     }


    public Boolean save(Event_confirm event_confirm){
            return false;
     }
    public Boolean saveBatch(List<Event_confirm> event_confirms){
            return false;
     }

    public Page<Event_confirm> searchDefault(Event_confirmSearchContext context){
            return null;
     }


}
