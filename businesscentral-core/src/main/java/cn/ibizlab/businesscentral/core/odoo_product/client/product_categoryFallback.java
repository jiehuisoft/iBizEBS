package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_categorySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_category] 服务对象接口
 */
@Component
public class product_categoryFallback implements product_categoryFeignClient{

    public Product_category update(Long id, Product_category product_category){
            return null;
     }
    public Boolean updateBatch(List<Product_category> product_categories){
            return false;
     }



    public Product_category create(Product_category product_category){
            return null;
     }
    public Boolean createBatch(List<Product_category> product_categories){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Product_category get(Long id){
            return null;
     }


    public Page<Product_category> search(Product_categorySearchContext context){
            return null;
     }



    public Page<Product_category> select(){
            return null;
     }

    public Product_category getDraft(){
            return null;
    }



    public Boolean checkKey(Product_category product_category){
            return false;
     }


    public Boolean save(Product_category product_category){
            return false;
     }
    public Boolean saveBatch(List<Product_category> product_categories){
            return false;
     }

    public Page<Product_category> searchDefault(Product_categorySearchContext context){
            return null;
     }


}
