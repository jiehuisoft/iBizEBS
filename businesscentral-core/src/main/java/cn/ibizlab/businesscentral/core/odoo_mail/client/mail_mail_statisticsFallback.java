package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mail_statistics;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mail_statisticsSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_mail_statistics] 服务对象接口
 */
@Component
public class mail_mail_statisticsFallback implements mail_mail_statisticsFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Mail_mail_statistics> search(Mail_mail_statisticsSearchContext context){
            return null;
     }



    public Mail_mail_statistics create(Mail_mail_statistics mail_mail_statistics){
            return null;
     }
    public Boolean createBatch(List<Mail_mail_statistics> mail_mail_statistics){
            return false;
     }



    public Mail_mail_statistics update(Long id, Mail_mail_statistics mail_mail_statistics){
            return null;
     }
    public Boolean updateBatch(List<Mail_mail_statistics> mail_mail_statistics){
            return false;
     }


    public Mail_mail_statistics get(Long id){
            return null;
     }


    public Page<Mail_mail_statistics> select(){
            return null;
     }

    public Mail_mail_statistics getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_mail_statistics mail_mail_statistics){
            return false;
     }


    public Boolean save(Mail_mail_statistics mail_mail_statistics){
            return false;
     }
    public Boolean saveBatch(List<Mail_mail_statistics> mail_mail_statistics){
            return false;
     }

    public Page<Mail_mail_statistics> searchDefault(Mail_mail_statisticsSearchContext context){
            return null;
     }


}
