package cn.ibizlab.businesscentral.core.odoo_maintenance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_stage;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_stageSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Maintenance_stage] 服务对象接口
 */
public interface IMaintenance_stageService extends IService<Maintenance_stage>{

    boolean create(Maintenance_stage et) ;
    void createBatch(List<Maintenance_stage> list) ;
    boolean update(Maintenance_stage et) ;
    void updateBatch(List<Maintenance_stage> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Maintenance_stage get(Long key) ;
    Maintenance_stage getDraft(Maintenance_stage et) ;
    boolean checkKey(Maintenance_stage et) ;
    boolean save(Maintenance_stage et) ;
    void saveBatch(List<Maintenance_stage> list) ;
    Page<Maintenance_stage> searchDefault(Maintenance_stageSearchContext context) ;
    List<Maintenance_stage> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Maintenance_stage> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Maintenance_stage> getMaintenanceStageByIds(List<Long> ids) ;
    List<Maintenance_stage> getMaintenanceStageByEntities(List<Maintenance_stage> entities) ;
}


