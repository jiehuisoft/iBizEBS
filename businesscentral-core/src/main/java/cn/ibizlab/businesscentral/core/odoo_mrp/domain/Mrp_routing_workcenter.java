package cn.ibizlab.businesscentral.core.odoo_mrp.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[工作中心使用情况]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MRP_ROUTING_WORKCENTER",resultMap = "Mrp_routing_workcenterResultMap")
public class Mrp_routing_workcenter extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 基于
     */
    @DEField(name = "time_mode_batch")
    @TableField(value = "time_mode_batch")
    @JSONField(name = "time_mode_batch")
    @JsonProperty("time_mode_batch")
    private Integer timeModeBatch;
    /**
     * 待处理的数量
     */
    @DEField(name = "batch_size")
    @TableField(value = "batch_size")
    @JSONField(name = "batch_size")
    @JsonProperty("batch_size")
    private Double batchSize;
    /**
     * 工单
     */
    @TableField(exist = false)
    @JSONField(name = "workorder_ids")
    @JsonProperty("workorder_ids")
    private String workorderIds;
    /**
     * # 工单
     */
    @TableField(exist = false)
    @JSONField(name = "workorder_count")
    @JsonProperty("workorder_count")
    private Integer workorderCount;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 下一作业
     */
    @TableField(value = "batch")
    @JSONField(name = "batch")
    @JsonProperty("batch")
    private String batch;
    /**
     * 人工时长
     */
    @DEField(name = "time_cycle_manual")
    @TableField(value = "time_cycle_manual")
    @JSONField(name = "time_cycle_manual")
    @JsonProperty("time_cycle_manual")
    private Double timeCycleManual;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 说明
     */
    @TableField(value = "note")
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;
    /**
     * 时长计算
     */
    @DEField(name = "time_mode")
    @TableField(value = "time_mode")
    @JSONField(name = "time_mode")
    @JsonProperty("time_mode")
    private String timeMode;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 操作
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 时长
     */
    @TableField(exist = false)
    @JSONField(name = "time_cycle")
    @JsonProperty("time_cycle")
    private Double timeCycle;
    /**
     * 工作记录表
     */
    @TableField(exist = false)
    @JSONField(name = "worksheet")
    @JsonProperty("worksheet")
    private byte[] worksheet;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 工作中心
     */
    @TableField(exist = false)
    @JSONField(name = "workcenter_id_text")
    @JsonProperty("workcenter_id_text")
    private String workcenterIdText;
    /**
     * 父级工艺路线
     */
    @TableField(exist = false)
    @JSONField(name = "routing_id_text")
    @JsonProperty("routing_id_text")
    private String routingIdText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 父级工艺路线
     */
    @DEField(name = "routing_id")
    @TableField(value = "routing_id")
    @JSONField(name = "routing_id")
    @JsonProperty("routing_id")
    private Long routingId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 工作中心
     */
    @DEField(name = "workcenter_id")
    @TableField(value = "workcenter_id")
    @JSONField(name = "workcenter_id")
    @JsonProperty("workcenter_id")
    private Long workcenterId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing odooRouting;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter odooWorkcenter;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [基于]
     */
    public void setTimeModeBatch(Integer timeModeBatch){
        this.timeModeBatch = timeModeBatch ;
        this.modify("time_mode_batch",timeModeBatch);
    }

    /**
     * 设置 [待处理的数量]
     */
    public void setBatchSize(Double batchSize){
        this.batchSize = batchSize ;
        this.modify("batch_size",batchSize);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [下一作业]
     */
    public void setBatch(String batch){
        this.batch = batch ;
        this.modify("batch",batch);
    }

    /**
     * 设置 [人工时长]
     */
    public void setTimeCycleManual(Double timeCycleManual){
        this.timeCycleManual = timeCycleManual ;
        this.modify("time_cycle_manual",timeCycleManual);
    }

    /**
     * 设置 [说明]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [时长计算]
     */
    public void setTimeMode(String timeMode){
        this.timeMode = timeMode ;
        this.modify("time_mode",timeMode);
    }

    /**
     * 设置 [操作]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [父级工艺路线]
     */
    public void setRoutingId(Long routingId){
        this.routingId = routingId ;
        this.modify("routing_id",routingId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [工作中心]
     */
    public void setWorkcenterId(Long workcenterId){
        this.workcenterId = workcenterId ;
        this.modify("workcenter_id",workcenterId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


