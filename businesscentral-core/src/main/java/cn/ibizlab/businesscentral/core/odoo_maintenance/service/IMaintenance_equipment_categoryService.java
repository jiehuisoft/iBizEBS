package cn.ibizlab.businesscentral.core.odoo_maintenance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment_category;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_equipment_categorySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Maintenance_equipment_category] 服务对象接口
 */
public interface IMaintenance_equipment_categoryService extends IService<Maintenance_equipment_category>{

    boolean create(Maintenance_equipment_category et) ;
    void createBatch(List<Maintenance_equipment_category> list) ;
    boolean update(Maintenance_equipment_category et) ;
    void updateBatch(List<Maintenance_equipment_category> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Maintenance_equipment_category get(Long key) ;
    Maintenance_equipment_category getDraft(Maintenance_equipment_category et) ;
    boolean checkKey(Maintenance_equipment_category et) ;
    boolean save(Maintenance_equipment_category et) ;
    void saveBatch(List<Maintenance_equipment_category> list) ;
    Page<Maintenance_equipment_category> searchDefault(Maintenance_equipment_categorySearchContext context) ;
    List<Maintenance_equipment_category> selectByAliasId(Long id);
    List<Maintenance_equipment_category> selectByAliasId(Collection<Long> ids);
    void removeByAliasId(Long id);
    List<Maintenance_equipment_category> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Maintenance_equipment_category> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Maintenance_equipment_category> selectByTechnicianUserId(Long id);
    void resetByTechnicianUserId(Long id);
    void resetByTechnicianUserId(Collection<Long> ids);
    void removeByTechnicianUserId(Long id);
    List<Maintenance_equipment_category> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Maintenance_equipment_category> getMaintenanceEquipmentCategoryByIds(List<Long> ids) ;
    List<Maintenance_equipment_category> getMaintenanceEquipmentCategoryByEntities(List<Maintenance_equipment_category> entities) ;
}


