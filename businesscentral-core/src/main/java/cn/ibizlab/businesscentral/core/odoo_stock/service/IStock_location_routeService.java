package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_location_routeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_location_route] 服务对象接口
 */
public interface IStock_location_routeService extends IService<Stock_location_route>{

    boolean create(Stock_location_route et) ;
    void createBatch(List<Stock_location_route> list) ;
    boolean update(Stock_location_route et) ;
    void updateBatch(List<Stock_location_route> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_location_route get(Long key) ;
    Stock_location_route getDraft(Stock_location_route et) ;
    boolean checkKey(Stock_location_route et) ;
    boolean save(Stock_location_route et) ;
    void saveBatch(List<Stock_location_route> list) ;
    Page<Stock_location_route> searchDefault(Stock_location_routeSearchContext context) ;
    List<Stock_location_route> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Stock_location_route> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_location_route> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_location_route> selectBySuppliedWhId(Long id);
    void resetBySuppliedWhId(Long id);
    void resetBySuppliedWhId(Collection<Long> ids);
    void removeBySuppliedWhId(Long id);
    List<Stock_location_route> selectBySupplierWhId(Long id);
    void resetBySupplierWhId(Long id);
    void resetBySupplierWhId(Collection<Long> ids);
    void removeBySupplierWhId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_location_route> getStockLocationRouteByIds(List<Long> ids) ;
    List<Stock_location_route> getStockLocationRouteByEntities(List<Stock_location_route> entities) ;
}


