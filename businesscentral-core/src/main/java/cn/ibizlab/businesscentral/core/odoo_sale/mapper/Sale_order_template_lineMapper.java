package cn.ibizlab.businesscentral.core.odoo_sale.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template_line;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_template_lineSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Sale_order_template_lineMapper extends BaseMapper<Sale_order_template_line>{

    Page<Sale_order_template_line> searchDefault(IPage page, @Param("srf") Sale_order_template_lineSearchContext context, @Param("ew") Wrapper<Sale_order_template_line> wrapper) ;
    @Override
    Sale_order_template_line selectById(Serializable id);
    @Override
    int insert(Sale_order_template_line entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Sale_order_template_line entity);
    @Override
    int update(@Param(Constants.ENTITY) Sale_order_template_line entity, @Param("ew") Wrapper<Sale_order_template_line> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Sale_order_template_line> selectByProductId(@Param("id") Serializable id) ;

    List<Sale_order_template_line> selectByCreateUid(@Param("id") Serializable id) ;

    List<Sale_order_template_line> selectByWriteUid(@Param("id") Serializable id) ;

    List<Sale_order_template_line> selectBySaleOrderTemplateId(@Param("id") Serializable id) ;

    List<Sale_order_template_line> selectByProductUomId(@Param("id") Serializable id) ;


}
