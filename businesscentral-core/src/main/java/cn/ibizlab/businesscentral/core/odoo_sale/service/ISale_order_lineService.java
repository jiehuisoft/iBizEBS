package cn.ibizlab.businesscentral.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Sale_order_line] 服务对象接口
 */
public interface ISale_order_lineService extends IService<Sale_order_line>{

    boolean create(Sale_order_line et) ;
    void createBatch(List<Sale_order_line> list) ;
    boolean update(Sale_order_line et) ;
    void updateBatch(List<Sale_order_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Sale_order_line get(Long key) ;
    Sale_order_line getDraft(Sale_order_line et) ;
    boolean checkKey(Sale_order_line et) ;
    boolean save(Sale_order_line et) ;
    void saveBatch(List<Sale_order_line> list) ;
    Page<Sale_order_line> searchDefault(Sale_order_lineSearchContext context) ;
    List<Sale_order_line> selectByEventTicketId(Long id);
    void resetByEventTicketId(Long id);
    void resetByEventTicketId(Collection<Long> ids);
    void removeByEventTicketId(Long id);
    List<Sale_order_line> selectByEventId(Long id);
    void resetByEventId(Long id);
    void resetByEventId(Collection<Long> ids);
    void removeByEventId(Long id);
    List<Sale_order_line> selectByProductPackaging(Long id);
    void resetByProductPackaging(Long id);
    void resetByProductPackaging(Collection<Long> ids);
    void removeByProductPackaging(Long id);
    List<Sale_order_line> selectByProductId(Long id);
    List<Sale_order_line> selectByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Sale_order_line> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Sale_order_line> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Sale_order_line> selectByOrderPartnerId(Long id);
    void resetByOrderPartnerId(Long id);
    void resetByOrderPartnerId(Collection<Long> ids);
    void removeByOrderPartnerId(Long id);
    List<Sale_order_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Sale_order_line> selectBySalesmanId(Long id);
    void resetBySalesmanId(Long id);
    void resetBySalesmanId(Collection<Long> ids);
    void removeBySalesmanId(Long id);
    List<Sale_order_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Sale_order_line> selectByLinkedLineId(Long id);
    void removeByLinkedLineId(Collection<Long> ids);
    void removeByLinkedLineId(Long id);
    List<Sale_order_line> selectByOrderId(Long id);
    void removeByOrderId(Collection<Long> ids);
    void removeByOrderId(Long id);
    List<Sale_order_line> selectByRouteId(Long id);
    List<Sale_order_line> selectByRouteId(Collection<Long> ids);
    void removeByRouteId(Long id);
    List<Sale_order_line> selectByProductUom(Long id);
    void resetByProductUom(Long id);
    void resetByProductUom(Collection<Long> ids);
    void removeByProductUom(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Sale_order_line> getSaleOrderLineByIds(List<Long> ids) ;
    List<Sale_order_line> getSaleOrderLineByEntities(List<Sale_order_line> entities) ;
}


