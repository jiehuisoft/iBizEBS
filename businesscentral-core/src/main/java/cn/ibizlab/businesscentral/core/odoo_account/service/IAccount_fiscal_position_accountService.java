package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position_account;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_fiscal_position_accountSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_fiscal_position_account] 服务对象接口
 */
public interface IAccount_fiscal_position_accountService extends IService<Account_fiscal_position_account>{

    boolean create(Account_fiscal_position_account et) ;
    void createBatch(List<Account_fiscal_position_account> list) ;
    boolean update(Account_fiscal_position_account et) ;
    void updateBatch(List<Account_fiscal_position_account> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_fiscal_position_account get(Long key) ;
    Account_fiscal_position_account getDraft(Account_fiscal_position_account et) ;
    boolean checkKey(Account_fiscal_position_account et) ;
    boolean save(Account_fiscal_position_account et) ;
    void saveBatch(List<Account_fiscal_position_account> list) ;
    Page<Account_fiscal_position_account> searchDefault(Account_fiscal_position_accountSearchContext context) ;
    List<Account_fiscal_position_account> selectByAccountDestId(Long id);
    void resetByAccountDestId(Long id);
    void resetByAccountDestId(Collection<Long> ids);
    void removeByAccountDestId(Long id);
    List<Account_fiscal_position_account> selectByAccountSrcId(Long id);
    void resetByAccountSrcId(Long id);
    void resetByAccountSrcId(Collection<Long> ids);
    void removeByAccountSrcId(Long id);
    List<Account_fiscal_position_account> selectByPositionId(Long id);
    void removeByPositionId(Collection<Long> ids);
    void removeByPositionId(Long id);
    List<Account_fiscal_position_account> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_fiscal_position_account> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_fiscal_position_account> getAccountFiscalPositionAccountByIds(List<Long> ids) ;
    List<Account_fiscal_position_account> getAccountFiscalPositionAccountByEntities(List<Account_fiscal_position_account> entities) ;
}


