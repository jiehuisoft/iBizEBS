package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconcile_model_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_reconcile_model_templateSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_reconcile_model_template] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-reconcile-model-template", fallback = account_reconcile_model_templateFallback.class)
public interface account_reconcile_model_templateFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_reconcile_model_templates/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_reconcile_model_templates/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates")
    Account_reconcile_model_template create(@RequestBody Account_reconcile_model_template account_reconcile_model_template);

    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates/batch")
    Boolean createBatch(@RequestBody List<Account_reconcile_model_template> account_reconcile_model_templates);



    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates/search")
    Page<Account_reconcile_model_template> search(@RequestBody Account_reconcile_model_templateSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_reconcile_model_templates/{id}")
    Account_reconcile_model_template update(@PathVariable("id") Long id,@RequestBody Account_reconcile_model_template account_reconcile_model_template);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_reconcile_model_templates/batch")
    Boolean updateBatch(@RequestBody List<Account_reconcile_model_template> account_reconcile_model_templates);



    @RequestMapping(method = RequestMethod.GET, value = "/account_reconcile_model_templates/{id}")
    Account_reconcile_model_template get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.GET, value = "/account_reconcile_model_templates/select")
    Page<Account_reconcile_model_template> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_reconcile_model_templates/getdraft")
    Account_reconcile_model_template getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates/checkkey")
    Boolean checkKey(@RequestBody Account_reconcile_model_template account_reconcile_model_template);


    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates/save")
    Boolean save(@RequestBody Account_reconcile_model_template account_reconcile_model_template);

    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates/savebatch")
    Boolean saveBatch(@RequestBody List<Account_reconcile_model_template> account_reconcile_model_templates);



    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates/searchdefault")
    Page<Account_reconcile_model_template> searchDefault(@RequestBody Account_reconcile_model_templateSearchContext context);


}
