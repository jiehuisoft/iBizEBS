package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_group;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_groupSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_analytic_group] 服务对象接口
 */
@Component
public class account_analytic_groupFallback implements account_analytic_groupFeignClient{

    public Account_analytic_group update(Long id, Account_analytic_group account_analytic_group){
            return null;
     }
    public Boolean updateBatch(List<Account_analytic_group> account_analytic_groups){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Account_analytic_group create(Account_analytic_group account_analytic_group){
            return null;
     }
    public Boolean createBatch(List<Account_analytic_group> account_analytic_groups){
            return false;
     }

    public Account_analytic_group get(Long id){
            return null;
     }




    public Page<Account_analytic_group> search(Account_analytic_groupSearchContext context){
            return null;
     }


    public Page<Account_analytic_group> select(){
            return null;
     }

    public Account_analytic_group getDraft(){
            return null;
    }



    public Boolean checkKey(Account_analytic_group account_analytic_group){
            return false;
     }


    public Boolean save(Account_analytic_group account_analytic_group){
            return false;
     }
    public Boolean saveBatch(List<Account_analytic_group> account_analytic_groups){
            return false;
     }

    public Page<Account_analytic_group> searchDefault(Account_analytic_groupSearchContext context){
            return null;
     }


}
