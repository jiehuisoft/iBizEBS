package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_financial_year_op;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_financial_year_opSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_financial_year_op] 服务对象接口
 */
public interface IAccount_financial_year_opService extends IService<Account_financial_year_op>{

    boolean create(Account_financial_year_op et) ;
    void createBatch(List<Account_financial_year_op> list) ;
    boolean update(Account_financial_year_op et) ;
    void updateBatch(List<Account_financial_year_op> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_financial_year_op get(Long key) ;
    Account_financial_year_op getDraft(Account_financial_year_op et) ;
    boolean checkKey(Account_financial_year_op et) ;
    boolean save(Account_financial_year_op et) ;
    void saveBatch(List<Account_financial_year_op> list) ;
    Page<Account_financial_year_op> searchDefault(Account_financial_year_opSearchContext context) ;
    List<Account_financial_year_op> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_financial_year_op> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_financial_year_op> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_financial_year_op> getAccountFinancialYearOpByIds(List<Long> ids) ;
    List<Account_financial_year_op> getAccountFinancialYearOpByEntities(List<Account_financial_year_op> entities) ;
}


