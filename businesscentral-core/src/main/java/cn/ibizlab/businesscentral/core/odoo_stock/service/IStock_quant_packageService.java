package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_quant_packageSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_quant_package] 服务对象接口
 */
public interface IStock_quant_packageService extends IService<Stock_quant_package>{

    boolean create(Stock_quant_package et) ;
    void createBatch(List<Stock_quant_package> list) ;
    boolean update(Stock_quant_package et) ;
    void updateBatch(List<Stock_quant_package> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_quant_package get(Long key) ;
    Stock_quant_package getDraft(Stock_quant_package et) ;
    boolean checkKey(Stock_quant_package et) ;
    boolean save(Stock_quant_package et) ;
    void saveBatch(List<Stock_quant_package> list) ;
    Page<Stock_quant_package> searchDefault(Stock_quant_packageSearchContext context) ;
    List<Stock_quant_package> selectByPackagingId(Long id);
    void resetByPackagingId(Long id);
    void resetByPackagingId(Collection<Long> ids);
    void removeByPackagingId(Long id);
    List<Stock_quant_package> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Stock_quant_package> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_quant_package> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_quant_package> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_quant_package> getStockQuantPackageByIds(List<Long> ids) ;
    List<Stock_quant_package> getStockQuantPackageByEntities(List<Stock_quant_package> entities) ;
}


