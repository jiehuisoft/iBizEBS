package cn.ibizlab.businesscentral.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[用户]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "RES_USERS",resultMap = "Res_usersResultMap")
public class Res_users extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 管理员
     */
    @TableField(exist = false)
    @JSONField(name = "is_moderator")
    @JsonProperty("is_moderator")
    private Boolean isModerator;
    /**
     * 资源
     */
    @TableField(exist = false)
    @JSONField(name = "resource_ids")
    @JsonProperty("resource_ids")
    private String resourceIds;
    /**
     * 创建日期
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 标签
     */
    @TableField(exist = false)
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private String categoryId;
    /**
     * 默认工作时间
     */
    @TableField(exist = false)
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Integer resourceCalendarId;
    /**
     * 用户登录记录
     */
    @TableField(exist = false)
    @JSONField(name = "log_ids")
    @JsonProperty("log_ids")
    private String logIds;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 徽章
     */
    @TableField(exist = false)
    @JSONField(name = "badge_ids")
    @JsonProperty("badge_ids")
    private String badgeIds;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_ids")
    @JsonProperty("company_ids")
    private String companyIds;
    /**
     * 联系人
     */
    @TableField(exist = false)
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;
    /**
     * 时区偏移
     */
    @TableField(exist = false)
    @JSONField(name = "tz_offset")
    @JsonProperty("tz_offset")
    private String tzOffset;
    /**
     * 活动达成
     */
    @DEField(name = "target_sales_done")
    @TableField(value = "target_sales_done")
    @JSONField(name = "target_sales_done")
    @JsonProperty("target_sales_done")
    private Integer targetSalesDone;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 通知管理
     */
    @DEField(name = "notification_type")
    @TableField(value = "notification_type")
    @JSONField(name = "notification_type")
    @JsonProperty("notification_type")
    private String notificationType;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * IM的状态
     */
    @TableField(exist = false)
    @JSONField(name = "im_status")
    @JsonProperty("im_status")
    private String imStatus;
    /**
     * 贡献值
     */
    @TableField(value = "karma")
    @JSONField(name = "karma")
    @JsonProperty("karma")
    private Integer karma;
    /**
     * 登记网站
     */
    @DEField(name = "website_id")
    @TableField(value = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 金质徽章个数
     */
    @TableField(exist = false)
    @JSONField(name = "gold_badge")
    @JsonProperty("gold_badge")
    private Integer goldBadge;
    /**
     * 相关的员工
     */
    @TableField(exist = false)
    @JSONField(name = "employee_ids")
    @JsonProperty("employee_ids")
    private String employeeIds;
    /**
     * 状态
     */
    @TableField(exist = false)
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 管理频道
     */
    @TableField(exist = false)
    @JSONField(name = "moderation_channel_ids")
    @JsonProperty("moderation_channel_ids")
    private String moderationChannelIds;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 银质徽章个数
     */
    @TableField(exist = false)
    @JSONField(name = "silver_badge")
    @JsonProperty("silver_badge")
    private Integer silverBadge;
    /**
     * 付款令牌
     */
    @TableField(exist = false)
    @JSONField(name = "payment_token_ids")
    @JsonProperty("payment_token_ids")
    private String paymentTokenIds;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 公司数量
     */
    @TableField(exist = false)
    @JSONField(name = "companies_count")
    @JsonProperty("companies_count")
    private Integer companiesCount;
    /**
     * 销售订单目标发票
     */
    @DEField(name = "target_sales_invoiced")
    @TableField(value = "target_sales_invoiced")
    @JSONField(name = "target_sales_invoiced")
    @JsonProperty("target_sales_invoiced")
    private Integer targetSalesInvoiced;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 最后连接
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "login_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("login_date")
    private Timestamp loginDate;
    /**
     * 渠道
     */
    @TableField(exist = false)
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;
    /**
     * 群组
     */
    @TableField(exist = false)
    @JSONField(name = "groups_id")
    @JsonProperty("groups_id")
    private String groupsId;
    /**
     * 共享用户
     */
    @TableField(value = "share")
    @JSONField(name = "share")
    @JsonProperty("share")
    private Boolean share;
    /**
     * 银行
     */
    @TableField(exist = false)
    @JSONField(name = "bank_ids")
    @JsonProperty("bank_ids")
    private String bankIds;
    /**
     * 销售订单
     */
    @TableField(exist = false)
    @JSONField(name = "sale_order_ids")
    @JsonProperty("sale_order_ids")
    private String saleOrderIds;
    /**
     * 设置密码
     */
    @TableField(exist = false)
    @JSONField(name = "new_password")
    @JsonProperty("new_password")
    private String newPassword;
    /**
     * OdooBot 状态
     */
    @DEField(name = "odoobot_state")
    @TableField(value = "odoobot_state")
    @JSONField(name = "odoobot_state")
    @JsonProperty("odoobot_state")
    private String odoobotState;
    /**
     * 公司是指业务伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "ref_company_ids")
    @JsonProperty("ref_company_ids")
    private String refCompanyIds;
    /**
     * 密码
     */
    @TableField(exist = false)
    @JSONField(name = "password")
    @JsonProperty("password")
    private String password;
    /**
     * 青铜徽章数目
     */
    @TableField(exist = false)
    @JSONField(name = "bronze_badge")
    @JsonProperty("bronze_badge")
    private Integer bronzeBadge;
    /**
     * 会议
     */
    @TableField(exist = false)
    @JSONField(name = "meeting_ids")
    @JsonProperty("meeting_ids")
    private String meetingIds;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 待发布的帖子
     */
    @TableField(exist = false)
    @JSONField(name = "forum_waiting_posts_count")
    @JsonProperty("forum_waiting_posts_count")
    private Integer forumWaitingPostsCount;
    /**
     * 目标
     */
    @TableField(exist = false)
    @JSONField(name = "goal_ids")
    @JsonProperty("goal_ids")
    private String goalIds;
    /**
     * 签单是商机的最终目标
     */
    @DEField(name = "target_sales_won")
    @TableField(value = "target_sales_won")
    @JSONField(name = "target_sales_won")
    @JsonProperty("target_sales_won")
    private Integer targetSalesWon;
    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 主页动作
     */
    @DEField(name = "action_id")
    @TableField(value = "action_id")
    @JSONField(name = "action_id")
    @JsonProperty("action_id")
    private Integer actionId;
    /**
     * 登录
     */
    @TableField(value = "login")
    @JSONField(name = "login")
    @JsonProperty("login")
    private String login;
    /**
     * 客户合同
     */
    @TableField(exist = false)
    @JSONField(name = "contract_ids")
    @JsonProperty("contract_ids")
    private String contractIds;
    /**
     * 审核数
     */
    @TableField(exist = false)
    @JSONField(name = "moderation_counter")
    @JsonProperty("moderation_counter")
    private Integer moderationCounter;
    /**
     * 签名
     */
    @TableField(value = "signature")
    @JSONField(name = "signature")
    @JsonProperty("signature")
    private String signature;
    /**
     * 用户
     */
    @TableField(exist = false)
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;
    /**
     * 商机
     */
    @TableField(exist = false)
    @JSONField(name = "opportunity_ids")
    @JsonProperty("opportunity_ids")
    private String opportunityIds;
    /**
     * 任务
     */
    @TableField(exist = false)
    @JSONField(name = "task_ids")
    @JsonProperty("task_ids")
    private String taskIds;
    /**
     * 发票
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;
    /**
     * 内部参考
     */
    @TableField(exist = false)
    @JSONField(name = "ref")
    @JsonProperty("ref")
    private String ref;
    /**
     * 错误个数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 最近的在线销售订单
     */
    @TableField(exist = false)
    @JSONField(name = "last_website_so_id")
    @JsonProperty("last_website_so_id")
    private Integer lastWebsiteSoId;
    /**
     * 日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 客户位置
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_customer")
    @JsonProperty("property_stock_customer")
    private Integer propertyStockCustomer;
    /**
     * 最近的发票和付款匹配时间
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_time_entries_checked" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("last_time_entries_checked")
    private Timestamp lastTimeEntriesChecked;
    /**
     * 语言
     */
    @TableField(exist = false)
    @JSONField(name = "lang")
    @JsonProperty("lang")
    private String lang;
    /**
     * 销售警告
     */
    @TableField(exist = false)
    @JSONField(name = "sale_warn")
    @JsonProperty("sale_warn")
    private String saleWarn;
    /**
     * #会议
     */
    @TableField(exist = false)
    @JSONField(name = "meeting_count")
    @JsonProperty("meeting_count")
    private Integer meetingCount;
    /**
     * 街道
     */
    @TableField(exist = false)
    @JSONField(name = "street")
    @JsonProperty("street")
    private String street;
    /**
     * 发票
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_warn")
    @JsonProperty("invoice_warn")
    private String invoiceWarn;
    /**
     * 注册令牌 Token
     */
    @TableField(exist = false)
    @JSONField(name = "signup_token")
    @JsonProperty("signup_token")
    private String signupToken;
    /**
     * # 任务
     */
    @TableField(exist = false)
    @JSONField(name = "task_count")
    @JsonProperty("task_count")
    private Integer taskCount;
    /**
     * 注册令牌（ Token  ）是有效的
     */
    @TableField(exist = false)
    @JSONField(name = "signup_valid")
    @JsonProperty("signup_valid")
    private Boolean signupValid;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 注册令牌（Token）类型
     */
    @TableField(exist = false)
    @JSONField(name = "signup_type")
    @JsonProperty("signup_type")
    private String signupType;
    /**
     * 应收账款
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_receivable_id")
    @JsonProperty("property_account_receivable_id")
    private Integer propertyAccountReceivableId;
    /**
     * 网站opengraph图像
     */
    @TableField(exist = false)
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "event_count")
    @JsonProperty("event_count")
    private Integer eventCount;
    /**
     * 日记账项目
     */
    @TableField(exist = false)
    @JSONField(name = "journal_item_count")
    @JsonProperty("journal_item_count")
    private Integer journalItemCount;
    /**
     * 上级名称
     */
    @TableField(exist = false)
    @JSONField(name = "parent_name")
    @JsonProperty("parent_name")
    private String parentName;
    /**
     * 用户的销售团队
     */
    @TableField(exist = false)
    @JSONField(name = "sale_team_id_text")
    @JsonProperty("sale_team_id_text")
    private String saleTeamIdText;
    /**
     * 应收总计
     */
    @TableField(exist = false)
    @JSONField(name = "credit")
    @JsonProperty("credit")
    private BigDecimal credit;
    /**
     * 商机
     */
    @TableField(exist = false)
    @JSONField(name = "opportunity_count")
    @JsonProperty("opportunity_count")
    private Integer opportunityCount;
    /**
     * 注册网址
     */
    @TableField(exist = false)
    @JSONField(name = "signup_url")
    @JsonProperty("signup_url")
    private String signupUrl;
    /**
     * 应付账款
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_payable_id")
    @JsonProperty("property_account_payable_id")
    private Integer propertyAccountPayableId;
    /**
     * 省/ 州
     */
    @TableField(exist = false)
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    private Long stateId;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 销售订单消息
     */
    @TableField(exist = false)
    @JSONField(name = "sale_warn_msg")
    @JsonProperty("sale_warn_msg")
    private String saleWarnMsg;
    /**
     * 在当前网站显示
     */
    @TableField(exist = false)
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;
    /**
     * 已开票总计
     */
    @TableField(exist = false)
    @JSONField(name = "total_invoiced")
    @JsonProperty("total_invoiced")
    private BigDecimal totalInvoiced;
    /**
     * 应付总计
     */
    @TableField(exist = false)
    @JSONField(name = "debit")
    @JsonProperty("debit")
    private BigDecimal debit;
    /**
     * 销售点订单计数
     */
    @TableField(exist = false)
    @JSONField(name = "pos_order_count")
    @JsonProperty("pos_order_count")
    private Integer posOrderCount;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 称谓
     */
    @TableField(exist = false)
    @JSONField(name = "title")
    @JsonProperty("title")
    private Long title;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * ＃供应商账单
     */
    @TableField(exist = false)
    @JSONField(name = "supplier_invoice_count")
    @JsonProperty("supplier_invoice_count")
    private Integer supplierInvoiceCount;
    /**
     * 城市
     */
    @TableField(exist = false)
    @JSONField(name = "city")
    @JsonProperty("city")
    private String city;
    /**
     * 库存拣货
     */
    @TableField(exist = false)
    @JSONField(name = "picking_warn")
    @JsonProperty("picking_warn")
    private String pickingWarn;
    /**
     * 附件
     */
    @TableField(exist = false)
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * 附加信息
     */
    @TableField(exist = false)
    @JSONField(name = "additional_info")
    @JsonProperty("additional_info")
    private String additionalInfo;
    /**
     * 工作岗位
     */
    @TableField(exist = false)
    @JSONField(name = "function")
    @JsonProperty("function")
    private String function;
    /**
     * 网站网址
     */
    @TableField(exist = false)
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;
    /**
     * 最后的提醒已经标志为已读
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "calendar_last_notif_ack" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("calendar_last_notif_ack")
    private Timestamp calendarLastNotifAck;
    /**
     * 操作次数
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 员工
     */
    @TableField(exist = false)
    @JSONField(name = "employee")
    @JsonProperty("employee")
    private Boolean employee;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 条码
     */
    @TableField(exist = false)
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;
    /**
     * 公司数据库ID
     */
    @TableField(exist = false)
    @JSONField(name = "partner_gid")
    @JsonProperty("partner_gid")
    private Integer partnerGid;
    /**
     * 地址类型
     */
    @TableField(exist = false)
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 税号
     */
    @TableField(exist = false)
    @JSONField(name = "vat")
    @JsonProperty("vat")
    private String vat;
    /**
     * 采购订单消息
     */
    @TableField(exist = false)
    @JSONField(name = "purchase_warn_msg")
    @JsonProperty("purchase_warn_msg")
    private String purchaseWarnMsg;
    /**
     * 便签
     */
    @TableField(exist = false)
    @JSONField(name = "comment")
    @JsonProperty("comment")
    private String comment;
    /**
     * 网站meta关键词
     */
    @TableField(exist = false)
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;
    /**
     * 关联公司
     */
    @TableField(exist = false)
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Long parentId;
    /**
     * 采购订单
     */
    @TableField(exist = false)
    @JSONField(name = "purchase_warn")
    @JsonProperty("purchase_warn")
    private String purchaseWarn;
    /**
     * 激活的合作伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "active_partner")
    @JsonProperty("active_partner")
    private Boolean activePartner;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;
    /**
     * 工业
     */
    @TableField(exist = false)
    @JSONField(name = "industry_id")
    @JsonProperty("industry_id")
    private Long industryId;
    /**
     * 供应商位置
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_supplier")
    @JsonProperty("property_stock_supplier")
    private Integer propertyStockSupplier;
    /**
     * 付款令牌计数
     */
    @TableField(exist = false)
    @JSONField(name = "payment_token_count")
    @JsonProperty("payment_token_count")
    private Integer paymentTokenCount;
    /**
     * 前置操作
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 销售员
     */
    @TableField(exist = false)
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 客户付款条款
     */
    @TableField(exist = false)
    @JSONField(name = "property_payment_term_id")
    @JsonProperty("property_payment_term_id")
    private Integer propertyPaymentTermId;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 合同统计
     */
    @TableField(exist = false)
    @JSONField(name = "contracts_count")
    @JsonProperty("contracts_count")
    private Integer contractsCount;
    /**
     * 自己
     */
    @TableField(exist = false)
    @JSONField(name = "self")
    @JsonProperty("self")
    private Integer self;
    /**
     * 网站元说明
     */
    @TableField(exist = false)
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;
    /**
     * 图像
     */
    @TableField(exist = false)
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;
    /**
     * EMail
     */
    @TableField(exist = false)
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;
    /**
     * 中等尺寸图像
     */
    @TableField(exist = false)
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;
    /**
     * 下一个活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 应付限额
     */
    @TableField(exist = false)
    @JSONField(name = "debit_limit")
    @JsonProperty("debit_limit")
    private BigDecimal debitLimit;
    /**
     * 国家/地区
     */
    @TableField(exist = false)
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Long countryId;
    /**
     * 信用额度
     */
    @TableField(exist = false)
    @JSONField(name = "credit_limit")
    @JsonProperty("credit_limit")
    private Double creditLimit;
    /**
     * 公司名称实体
     */
    @TableField(exist = false)
    @JSONField(name = "commercial_company_name")
    @JsonProperty("commercial_company_name")
    private String commercialCompanyName;
    /**
     * 发票消息
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_warn_msg")
    @JsonProperty("invoice_warn_msg")
    private String invoiceWarnMsg;
    /**
     * 已发布
     */
    @TableField(exist = false)
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private Boolean isPublished;
    /**
     * 对此债务人的信任度
     */
    @TableField(exist = false)
    @JSONField(name = "trust")
    @JsonProperty("trust")
    private String trust;
    /**
     * 手机
     */
    @TableField(exist = false)
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    private String mobile;
    /**
     * 格式化的邮件
     */
    @TableField(exist = false)
    @JSONField(name = "email_formatted")
    @JsonProperty("email_formatted")
    private String emailFormatted;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "is_company")
    @JsonProperty("is_company")
    private Boolean isCompany;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 销售团队
     */
    @TableField(exist = false)
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Long teamId;
    /**
     * 黑名单
     */
    @TableField(exist = false)
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private Boolean isBlacklisted;
    /**
     * 银行
     */
    @TableField(exist = false)
    @JSONField(name = "bank_account_count")
    @JsonProperty("bank_account_count")
    private Integer bankAccountCount;
    /**
     * 价格表
     */
    @TableField(exist = false)
    @JSONField(name = "property_product_pricelist")
    @JsonProperty("property_product_pricelist")
    private Integer propertyProductPricelist;
    /**
     * 名称
     */
    @TableField(exist = false)
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 库存拣货单消息
     */
    @TableField(exist = false)
    @JSONField(name = "picking_warn_msg")
    @JsonProperty("picking_warn_msg")
    private String pickingWarnMsg;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 网站
     */
    @TableField(exist = false)
    @JSONField(name = "website")
    @JsonProperty("website")
    private String website;
    /**
     * 电话
     */
    @TableField(exist = false)
    @JSONField(name = "phone")
    @JsonProperty("phone")
    private String phone;
    /**
     * 街道 2
     */
    @TableField(exist = false)
    @JSONField(name = "street2")
    @JsonProperty("street2")
    private String street2;
    /**
     * 有未核销的分录
     */
    @TableField(exist = false)
    @JSONField(name = "has_unreconciled_entries")
    @JsonProperty("has_unreconciled_entries")
    private Boolean hasUnreconciledEntries;
    /**
     * 注册到期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "signup_expiration" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("signup_expiration")
    private Timestamp signupExpiration;
    /**
     * 时区
     */
    @TableField(exist = false)
    @JSONField(name = "tz")
    @JsonProperty("tz")
    private String tz;
    /**
     * 完整地址
     */
    @TableField(exist = false)
    @JSONField(name = "contact_address")
    @JsonProperty("contact_address")
    private String contactAddress;
    /**
     * 网站业务伙伴简介
     */
    @TableField(exist = false)
    @JSONField(name = "website_short_description")
    @JsonProperty("website_short_description")
    private String websiteShortDescription;
    /**
     * 共享合作伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "partner_share")
    @JsonProperty("partner_share")
    private Boolean partnerShare;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 颜色索引
     */
    @TableField(exist = false)
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;
    /**
     * 邮政编码
     */
    @TableField(exist = false)
    @JSONField(name = "zip")
    @JsonProperty("zip")
    private String zip;
    /**
     * 销售订单个数
     */
    @TableField(exist = false)
    @JSONField(name = "sale_order_count")
    @JsonProperty("sale_order_count")
    private Integer saleOrderCount;
    /**
     * 公司类别
     */
    @TableField(exist = false)
    @JSONField(name = "company_type")
    @JsonProperty("company_type")
    private String companyType;
    /**
     * 税科目调整
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_position_id")
    @JsonProperty("property_account_position_id")
    private Integer propertyAccountPositionId;
    /**
     * SEO优化
     */
    @TableField(exist = false)
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private Boolean isSeoOptimized;
    /**
     * 退回
     */
    @TableField(exist = false)
    @JSONField(name = "message_bounce")
    @JsonProperty("message_bounce")
    private Integer messageBounce;
    /**
     * 网站meta标题
     */
    @TableField(exist = false)
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;
    /**
     * 小尺寸图像
     */
    @TableField(exist = false)
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;
    /**
     * 供应商货币
     */
    @TableField(exist = false)
    @JSONField(name = "property_purchase_currency_id")
    @JsonProperty("property_purchase_currency_id")
    private Integer propertyPurchaseCurrencyId;
    /**
     * 采购订单数
     */
    @TableField(exist = false)
    @JSONField(name = "purchase_order_count")
    @JsonProperty("purchase_order_count")
    private Integer purchaseOrderCount;
    /**
     * 网站业务伙伴的详细说明
     */
    @TableField(exist = false)
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    private String websiteDescription;
    /**
     * 供应商付款条款
     */
    @TableField(exist = false)
    @JSONField(name = "property_supplier_payment_term_id")
    @JsonProperty("property_supplier_payment_term_id")
    private Integer propertySupplierPaymentTermId;
    /**
     * 商业实体
     */
    @TableField(exist = false)
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    private Long commercialPartnerId;
    /**
     * 安全联系人别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    private String aliasContact;
    /**
     * 公司名称
     */
    @TableField(exist = false)
    @JSONField(name = "company_name")
    @JsonProperty("company_name")
    private String companyName;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 相关的业务伙伴
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 别名
     */
    @DEField(name = "alias_id")
    @TableField(value = "alias_id")
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Long aliasId;
    /**
     * 用户的销售团队
     */
    @DEField(name = "sale_team_id")
    @TableField(value = "sale_team_id")
    @JSONField(name = "sale_team_id")
    @JsonProperty("sale_team_id")
    private Long saleTeamId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team odooSaleTeam;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias odooAlias;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [活动达成]
     */
    public void setTargetSalesDone(Integer targetSalesDone){
        this.targetSalesDone = targetSalesDone ;
        this.modify("target_sales_done",targetSalesDone);
    }

    /**
     * 设置 [通知管理]
     */
    public void setNotificationType(String notificationType){
        this.notificationType = notificationType ;
        this.modify("notification_type",notificationType);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [贡献值]
     */
    public void setKarma(Integer karma){
        this.karma = karma ;
        this.modify("karma",karma);
    }

    /**
     * 设置 [登记网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [销售订单目标发票]
     */
    public void setTargetSalesInvoiced(Integer targetSalesInvoiced){
        this.targetSalesInvoiced = targetSalesInvoiced ;
        this.modify("target_sales_invoiced",targetSalesInvoiced);
    }

    /**
     * 设置 [共享用户]
     */
    public void setShare(Boolean share){
        this.share = share ;
        this.modify("share",share);
    }

    /**
     * 设置 [OdooBot 状态]
     */
    public void setOdoobotState(String odoobotState){
        this.odoobotState = odoobotState ;
        this.modify("odoobot_state",odoobotState);
    }

    /**
     * 设置 [签单是商机的最终目标]
     */
    public void setTargetSalesWon(Integer targetSalesWon){
        this.targetSalesWon = targetSalesWon ;
        this.modify("target_sales_won",targetSalesWon);
    }

    /**
     * 设置 [主页动作]
     */
    public void setActionId(Integer actionId){
        this.actionId = actionId ;
        this.modify("action_id",actionId);
    }

    /**
     * 设置 [登录]
     */
    public void setLogin(String login){
        this.login = login ;
        this.modify("login",login);
    }

    /**
     * 设置 [签名]
     */
    public void setSignature(String signature){
        this.signature = signature ;
        this.modify("signature",signature);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [相关的业务伙伴]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [别名]
     */
    public void setAliasId(Long aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }

    /**
     * 设置 [用户的销售团队]
     */
    public void setSaleTeamId(Long saleTeamId){
        this.saleTeamId = saleTeamId ;
        this.modify("sale_team_id",saleTeamId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


