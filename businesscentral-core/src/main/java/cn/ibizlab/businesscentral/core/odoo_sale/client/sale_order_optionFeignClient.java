package cn.ibizlab.businesscentral.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_option;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_optionSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[sale_order_option] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-sale:odoo-sale}", contextId = "sale-order-option", fallback = sale_order_optionFallback.class)
public interface sale_order_optionFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_options/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_options/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.PUT, value = "/sale_order_options/{id}")
    Sale_order_option update(@PathVariable("id") Long id,@RequestBody Sale_order_option sale_order_option);

    @RequestMapping(method = RequestMethod.PUT, value = "/sale_order_options/batch")
    Boolean updateBatch(@RequestBody List<Sale_order_option> sale_order_options);



    @RequestMapping(method = RequestMethod.GET, value = "/sale_order_options/{id}")
    Sale_order_option get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_options/search")
    Page<Sale_order_option> search(@RequestBody Sale_order_optionSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_options")
    Sale_order_option create(@RequestBody Sale_order_option sale_order_option);

    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_options/batch")
    Boolean createBatch(@RequestBody List<Sale_order_option> sale_order_options);


    @RequestMapping(method = RequestMethod.GET, value = "/sale_order_options/select")
    Page<Sale_order_option> select();


    @RequestMapping(method = RequestMethod.GET, value = "/sale_order_options/getdraft")
    Sale_order_option getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_options/checkkey")
    Boolean checkKey(@RequestBody Sale_order_option sale_order_option);


    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_options/save")
    Boolean save(@RequestBody Sale_order_option sale_order_option);

    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_options/savebatch")
    Boolean saveBatch(@RequestBody List<Sale_order_option> sale_order_options);



    @RequestMapping(method = RequestMethod.POST, value = "/sale_order_options/searchdefault")
    Page<Sale_order_option> searchDefault(@RequestBody Sale_order_optionSearchContext context);


}
