package cn.ibizlab.businesscentral.core.odoo_purchase.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Account_tax_purchase_order_line_rel;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Account_tax_purchase_order_line_relSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Account_tax_purchase_order_line_relMapper extends BaseMapper<Account_tax_purchase_order_line_rel>{

    Page<Account_tax_purchase_order_line_rel> searchDefault(IPage page, @Param("srf") Account_tax_purchase_order_line_relSearchContext context, @Param("ew") Wrapper<Account_tax_purchase_order_line_rel> wrapper) ;
    @Override
    Account_tax_purchase_order_line_rel selectById(Serializable id);
    @Override
    int insert(Account_tax_purchase_order_line_rel entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Account_tax_purchase_order_line_rel entity);
    @Override
    int update(@Param(Constants.ENTITY) Account_tax_purchase_order_line_rel entity, @Param("ew") Wrapper<Account_tax_purchase_order_line_rel> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Account_tax_purchase_order_line_rel> selectByAccountTaxId(@Param("id") Serializable id) ;

    List<Account_tax_purchase_order_line_rel> selectByPurchaseOrderLineId(@Param("id") Serializable id) ;


}
