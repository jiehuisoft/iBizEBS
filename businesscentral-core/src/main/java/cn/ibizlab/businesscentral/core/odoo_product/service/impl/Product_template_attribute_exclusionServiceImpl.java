package cn.ibizlab.businesscentral.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template_attribute_exclusion;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_template_attribute_exclusionSearchContext;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_template_attribute_exclusionService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_product.mapper.Product_template_attribute_exclusionMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[产品模板属性排除] 服务对象接口实现
 */
@Slf4j
@Service("Product_template_attribute_exclusionServiceImpl")
public class Product_template_attribute_exclusionServiceImpl extends EBSServiceImpl<Product_template_attribute_exclusionMapper, Product_template_attribute_exclusion> implements IProduct_template_attribute_exclusionService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_template_attribute_valueService productTemplateAttributeValueService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_templateService productTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "product.template.attribute.exclusion" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Product_template_attribute_exclusion et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProduct_template_attribute_exclusionService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Product_template_attribute_exclusion> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Product_template_attribute_exclusion et) {
        Product_template_attribute_exclusion old = new Product_template_attribute_exclusion() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProduct_template_attribute_exclusionService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProduct_template_attribute_exclusionService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Product_template_attribute_exclusion> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Product_template_attribute_exclusion get(Long key) {
        Product_template_attribute_exclusion et = getById(key);
        if(et==null){
            et=new Product_template_attribute_exclusion();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Product_template_attribute_exclusion getDraft(Product_template_attribute_exclusion et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Product_template_attribute_exclusion et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Product_template_attribute_exclusion et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Product_template_attribute_exclusion et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Product_template_attribute_exclusion> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Product_template_attribute_exclusion> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Product_template_attribute_exclusion> selectByProductTemplateAttributeValueId(Long id) {
        return baseMapper.selectByProductTemplateAttributeValueId(id);
    }
    @Override
    public void removeByProductTemplateAttributeValueId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Product_template_attribute_exclusion>().in("product_template_attribute_value_id",ids));
    }

    @Override
    public void removeByProductTemplateAttributeValueId(Long id) {
        this.remove(new QueryWrapper<Product_template_attribute_exclusion>().eq("product_template_attribute_value_id",id));
    }

	@Override
    public List<Product_template_attribute_exclusion> selectByProductTmplId(Long id) {
        return baseMapper.selectByProductTmplId(id);
    }
    @Override
    public void removeByProductTmplId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Product_template_attribute_exclusion>().in("product_tmpl_id",ids));
    }

    @Override
    public void removeByProductTmplId(Long id) {
        this.remove(new QueryWrapper<Product_template_attribute_exclusion>().eq("product_tmpl_id",id));
    }

	@Override
    public List<Product_template_attribute_exclusion> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Product_template_attribute_exclusion>().eq("create_uid",id));
    }

	@Override
    public List<Product_template_attribute_exclusion> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Product_template_attribute_exclusion>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Product_template_attribute_exclusion> searchDefault(Product_template_attribute_exclusionSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Product_template_attribute_exclusion> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Product_template_attribute_exclusion>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Product_template_attribute_exclusion et){
        //实体关系[DER1N_PRODUCT_TEMPLATE_ATTRIBUTE_EXCLUSION__PRODUCT_TEMPLATE_ATTRIBUTE_VALUE__PRODUCT_TEMPLATE_ATTRIBUTE_VALUE_ID]
        if(!ObjectUtils.isEmpty(et.getProductTemplateAttributeValueId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template_attribute_value odooProductTemplateAttributeValue=et.getOdooProductTemplateAttributeValue();
            if(ObjectUtils.isEmpty(odooProductTemplateAttributeValue)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template_attribute_value majorEntity=productTemplateAttributeValueService.get(et.getProductTemplateAttributeValueId());
                et.setOdooProductTemplateAttributeValue(majorEntity);
                odooProductTemplateAttributeValue=majorEntity;
            }
            et.setProductTemplateAttributeValueIdText(odooProductTemplateAttributeValue.getName());
        }
        //实体关系[DER1N_PRODUCT_TEMPLATE_ATTRIBUTE_EXCLUSION__PRODUCT_TEMPLATE__PRODUCT_TMPL_ID]
        if(!ObjectUtils.isEmpty(et.getProductTmplId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template odooProductTmpl=et.getOdooProductTmpl();
            if(ObjectUtils.isEmpty(odooProductTmpl)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template majorEntity=productTemplateService.get(et.getProductTmplId());
                et.setOdooProductTmpl(majorEntity);
                odooProductTmpl=majorEntity;
            }
            et.setProductTmplIdText(odooProductTmpl.getName());
        }
        //实体关系[DER1N_PRODUCT_TEMPLATE_ATTRIBUTE_EXCLUSION__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_PRODUCT_TEMPLATE_ATTRIBUTE_EXCLUSION__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Product_template_attribute_exclusion> getProductTemplateAttributeExclusionByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Product_template_attribute_exclusion> getProductTemplateAttributeExclusionByEntities(List<Product_template_attribute_exclusion> entities) {
        List ids =new ArrayList();
        for(Product_template_attribute_exclusion entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



