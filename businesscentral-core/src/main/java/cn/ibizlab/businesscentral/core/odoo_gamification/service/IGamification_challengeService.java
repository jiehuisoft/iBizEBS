package cn.ibizlab.businesscentral.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_challengeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Gamification_challenge] 服务对象接口
 */
public interface IGamification_challengeService extends IService<Gamification_challenge>{

    boolean create(Gamification_challenge et) ;
    void createBatch(List<Gamification_challenge> list) ;
    boolean update(Gamification_challenge et) ;
    void updateBatch(List<Gamification_challenge> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Gamification_challenge get(Long key) ;
    Gamification_challenge getDraft(Gamification_challenge et) ;
    boolean checkKey(Gamification_challenge et) ;
    boolean save(Gamification_challenge et) ;
    void saveBatch(List<Gamification_challenge> list) ;
    Page<Gamification_challenge> searchDefault(Gamification_challengeSearchContext context) ;
    List<Gamification_challenge> selectByRewardFirstId(Long id);
    void resetByRewardFirstId(Long id);
    void resetByRewardFirstId(Collection<Long> ids);
    void removeByRewardFirstId(Long id);
    List<Gamification_challenge> selectByRewardId(Long id);
    void resetByRewardId(Long id);
    void resetByRewardId(Collection<Long> ids);
    void removeByRewardId(Long id);
    List<Gamification_challenge> selectByRewardSecondId(Long id);
    void resetByRewardSecondId(Long id);
    void resetByRewardSecondId(Collection<Long> ids);
    void removeByRewardSecondId(Long id);
    List<Gamification_challenge> selectByRewardThirdId(Long id);
    void resetByRewardThirdId(Long id);
    void resetByRewardThirdId(Collection<Long> ids);
    void removeByRewardThirdId(Long id);
    List<Gamification_challenge> selectByReportMessageGroupId(Long id);
    void resetByReportMessageGroupId(Long id);
    void resetByReportMessageGroupId(Collection<Long> ids);
    void removeByReportMessageGroupId(Long id);
    List<Gamification_challenge> selectByReportTemplateId(Long id);
    void resetByReportTemplateId(Long id);
    void resetByReportTemplateId(Collection<Long> ids);
    void removeByReportTemplateId(Long id);
    List<Gamification_challenge> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Gamification_challenge> selectByManagerId(Long id);
    void resetByManagerId(Long id);
    void resetByManagerId(Collection<Long> ids);
    void removeByManagerId(Long id);
    List<Gamification_challenge> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Gamification_challenge> getGamificationChallengeByIds(List<Long> ids) ;
    List<Gamification_challenge> getGamificationChallengeByEntities(List<Gamification_challenge> entities) ;
}


