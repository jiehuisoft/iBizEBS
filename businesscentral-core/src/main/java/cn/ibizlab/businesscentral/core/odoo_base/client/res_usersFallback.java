package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_usersSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_users] 服务对象接口
 */
@Component
public class res_usersFallback implements res_usersFeignClient{

    public Res_users get(Long id){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Res_users create(Res_users res_users){
            return null;
     }
    public Boolean createBatch(List<Res_users> res_users){
            return false;
     }



    public Res_users update(Long id, Res_users res_users){
            return null;
     }
    public Boolean updateBatch(List<Res_users> res_users){
            return false;
     }


    public Page<Res_users> search(Res_usersSearchContext context){
            return null;
     }


    public Page<Res_users> select(){
            return null;
     }

    public Res_users getDraft(){
            return null;
    }



    public Res_users a( Long id, Res_users res_users){
            return null;
     }

    public Boolean checkKey(Res_users res_users){
            return false;
     }


    public Boolean save(Res_users res_users){
            return false;
     }
    public Boolean saveBatch(List<Res_users> res_users){
            return false;
     }

    public Page<Res_users> searchActive(Res_usersSearchContext context){
            return null;
     }


    public Page<Res_users> searchDefault(Res_usersSearchContext context){
            return null;
     }


}
