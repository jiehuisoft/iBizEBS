package cn.ibizlab.businesscentral.core.odoo_maintenance.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_equipmentSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[maintenance_equipment] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-maintenance:odoo-maintenance}", contextId = "maintenance-equipment", fallback = maintenance_equipmentFallback.class)
public interface maintenance_equipmentFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_equipments/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_equipments/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments")
    Maintenance_equipment create(@RequestBody Maintenance_equipment maintenance_equipment);

    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments/batch")
    Boolean createBatch(@RequestBody List<Maintenance_equipment> maintenance_equipments);



    @RequestMapping(method = RequestMethod.PUT, value = "/maintenance_equipments/{id}")
    Maintenance_equipment update(@PathVariable("id") Long id,@RequestBody Maintenance_equipment maintenance_equipment);

    @RequestMapping(method = RequestMethod.PUT, value = "/maintenance_equipments/batch")
    Boolean updateBatch(@RequestBody List<Maintenance_equipment> maintenance_equipments);


    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_equipments/{id}")
    Maintenance_equipment get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments/search")
    Page<Maintenance_equipment> search(@RequestBody Maintenance_equipmentSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_equipments/select")
    Page<Maintenance_equipment> select();


    @RequestMapping(method = RequestMethod.GET, value = "/maintenance_equipments/getdraft")
    Maintenance_equipment getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments/checkkey")
    Boolean checkKey(@RequestBody Maintenance_equipment maintenance_equipment);


    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments/save")
    Boolean save(@RequestBody Maintenance_equipment maintenance_equipment);

    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments/savebatch")
    Boolean saveBatch(@RequestBody List<Maintenance_equipment> maintenance_equipments);



    @RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments/searchdefault")
    Page<Maintenance_equipment> searchDefault(@RequestBody Maintenance_equipmentSearchContext context);


}
