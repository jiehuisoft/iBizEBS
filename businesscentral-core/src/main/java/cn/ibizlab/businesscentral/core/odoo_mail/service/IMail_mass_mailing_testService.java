package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_test;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_testSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_mass_mailing_test] 服务对象接口
 */
public interface IMail_mass_mailing_testService extends IService<Mail_mass_mailing_test>{

    boolean create(Mail_mass_mailing_test et) ;
    void createBatch(List<Mail_mass_mailing_test> list) ;
    boolean update(Mail_mass_mailing_test et) ;
    void updateBatch(List<Mail_mass_mailing_test> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_mass_mailing_test get(Long key) ;
    Mail_mass_mailing_test getDraft(Mail_mass_mailing_test et) ;
    boolean checkKey(Mail_mass_mailing_test et) ;
    boolean save(Mail_mass_mailing_test et) ;
    void saveBatch(List<Mail_mass_mailing_test> list) ;
    Page<Mail_mass_mailing_test> searchDefault(Mail_mass_mailing_testSearchContext context) ;
    List<Mail_mass_mailing_test> selectByMassMailingId(Long id);
    void removeByMassMailingId(Collection<Long> ids);
    void removeByMassMailingId(Long id);
    List<Mail_mass_mailing_test> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_mass_mailing_test> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_mass_mailing_test> getMailMassMailingTestByIds(List<Long> ids) ;
    List<Mail_mass_mailing_test> getMailMassMailingTestByEntities(List<Mail_mass_mailing_test> entities) ;
}


