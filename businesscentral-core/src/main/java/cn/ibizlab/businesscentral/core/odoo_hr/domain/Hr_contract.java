package cn.ibizlab.businesscentral.core.odoo_hr.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[合同]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "HR_CONTRACT",resultMap = "Hr_contractResultMap")
public class Hr_contract extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 结束日期
     */
    @DEField(name = "date_end")
    @TableField(value = "date_end")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_end" , format="yyyy-MM-dd")
    @JsonProperty("date_end")
    private Timestamp dateEnd;
    /**
     * 补贴
     */
    @TableField(value = "advantages")
    @JSONField(name = "advantages")
    @JsonProperty("advantages")
    private String advantages;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 备注
     */
    @TableField(value = "notes")
    @JSONField(name = "notes")
    @JsonProperty("notes")
    private String notes;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 下一活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 合同参考
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 工资
     */
    @TableField(value = "wage")
    @JSONField(name = "wage")
    @JsonProperty("wage")
    private Double wage;
    /**
     * 开始日期
     */
    @DEField(name = "date_start")
    @TableField(value = "date_start")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd")
    @JsonProperty("date_start")
    private Timestamp dateStart;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 信息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 试用期结束
     */
    @DEField(name = "trial_date_end")
    @TableField(value = "trial_date_end")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "trial_date_end" , format="yyyy-MM-dd")
    @JsonProperty("trial_date_end")
    private Timestamp trialDateEnd;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 工作许可编号
     */
    @TableField(exist = false)
    @JSONField(name = "permit_no")
    @JsonProperty("permit_no")
    private String permitNo;
    /**
     * 工作安排
     */
    @TableField(exist = false)
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    private String resourceCalendarIdText;
    /**
     * 部门
     */
    @TableField(exist = false)
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    private String departmentIdText;
    /**
     * 工作岗位
     */
    @TableField(exist = false)
    @JSONField(name = "job_id_text")
    @JsonProperty("job_id_text")
    private String jobIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 员工
     */
    @TableField(exist = false)
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    private String employeeIdText;
    /**
     * 人力资源主管
     */
    @TableField(exist = false)
    @JSONField(name = "hr_responsible_name")
    @JsonProperty("hr_responsible_name")
    private String hrResponsibleName;
    /**
     * 签证到期日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "visa_expire" , format="yyyy-MM-dd")
    @JsonProperty("visa_expire")
    private Timestamp visaExpire;
    /**
     * 签证号
     */
    @TableField(exist = false)
    @JSONField(name = "visa_no")
    @JsonProperty("visa_no")
    private String visaNo;
    /**
     * 工作安排
     */
    @DEField(name = "resource_calendar_id")
    @TableField(value = "resource_calendar_id")
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Long resourceCalendarId;
    /**
     * 人力资源主管
     */
    @DEField(name = "hr_responsible_id")
    @TableField(value = "hr_responsible_id")
    @JSONField(name = "hr_responsible_id")
    @JsonProperty("hr_responsible_id")
    private Long hrResponsibleId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 员工
     */
    @DEField(name = "employee_id")
    @TableField(value = "employee_id")
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    private Long employeeId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 工作岗位
     */
    @DEField(name = "job_id")
    @TableField(value = "job_id")
    @JSONField(name = "job_id")
    @JsonProperty("job_id")
    private Long jobId;
    /**
     * 部门
     */
    @DEField(name = "department_id")
    @TableField(value = "department_id")
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    private Long departmentId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooHrResponsible;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department odooDepartment;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmployee;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job odooJob;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar odooResourceCalendar;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [结束日期]
     */
    public void setDateEnd(Timestamp dateEnd){
        this.dateEnd = dateEnd ;
        this.modify("date_end",dateEnd);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatDateEnd(){
        if (this.dateEnd == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dateEnd);
    }
    /**
     * 设置 [补贴]
     */
    public void setAdvantages(String advantages){
        this.advantages = advantages ;
        this.modify("advantages",advantages);
    }

    /**
     * 设置 [备注]
     */
    public void setNotes(String notes){
        this.notes = notes ;
        this.modify("notes",notes);
    }

    /**
     * 设置 [合同参考]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [工资]
     */
    public void setWage(Double wage){
        this.wage = wage ;
        this.modify("wage",wage);
    }

    /**
     * 设置 [开始日期]
     */
    public void setDateStart(Timestamp dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatDateStart(){
        if (this.dateStart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dateStart);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [试用期结束]
     */
    public void setTrialDateEnd(Timestamp trialDateEnd){
        this.trialDateEnd = trialDateEnd ;
        this.modify("trial_date_end",trialDateEnd);
    }

    /**
     * 格式化日期 [试用期结束]
     */
    public String formatTrialDateEnd(){
        if (this.trialDateEnd == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(trialDateEnd);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [工作安排]
     */
    public void setResourceCalendarId(Long resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }

    /**
     * 设置 [人力资源主管]
     */
    public void setHrResponsibleId(Long hrResponsibleId){
        this.hrResponsibleId = hrResponsibleId ;
        this.modify("hr_responsible_id",hrResponsibleId);
    }

    /**
     * 设置 [员工]
     */
    public void setEmployeeId(Long employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [工作岗位]
     */
    public void setJobId(Long jobId){
        this.jobId = jobId ;
        this.modify("job_id",jobId);
    }

    /**
     * 设置 [部门]
     */
    public void setDepartmentId(Long departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


