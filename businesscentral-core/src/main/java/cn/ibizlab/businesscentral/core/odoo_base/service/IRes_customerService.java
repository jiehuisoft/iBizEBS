package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_customer;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_customerSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_customer] 服务对象接口
 */
public interface IRes_customerService extends IService<Res_customer>{

    boolean create(Res_customer et) ;
    void createBatch(List<Res_customer> list) ;
    boolean update(Res_customer et) ;
    void updateBatch(List<Res_customer> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_customer get(Long key) ;
    Res_customer getDraft(Res_customer et) ;
    boolean checkKey(Res_customer et) ;
    boolean save(Res_customer et) ;
    void saveBatch(List<Res_customer> list) ;
    Page<Res_customer> searchDefault(Res_customerSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


