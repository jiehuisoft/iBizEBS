package cn.ibizlab.businesscentral.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead_tag;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_lead_tagSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[crm_lead_tag] 服务对象接口
 */
@Component
public class crm_lead_tagFallback implements crm_lead_tagFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Crm_lead_tag get(Long id){
            return null;
     }


    public Crm_lead_tag update(Long id, Crm_lead_tag crm_lead_tag){
            return null;
     }
    public Boolean updateBatch(List<Crm_lead_tag> crm_lead_tags){
            return false;
     }



    public Crm_lead_tag create(Crm_lead_tag crm_lead_tag){
            return null;
     }
    public Boolean createBatch(List<Crm_lead_tag> crm_lead_tags){
            return false;
     }


    public Page<Crm_lead_tag> search(Crm_lead_tagSearchContext context){
            return null;
     }



    public Page<Crm_lead_tag> select(){
            return null;
     }

    public Crm_lead_tag getDraft(){
            return null;
    }



    public Boolean checkKey(Crm_lead_tag crm_lead_tag){
            return false;
     }


    public Boolean save(Crm_lead_tag crm_lead_tag){
            return false;
     }
    public Boolean saveBatch(List<Crm_lead_tag> crm_lead_tags){
            return false;
     }

    public Page<Crm_lead_tag> searchDefault(Crm_lead_tagSearchContext context){
            return null;
     }


}
