package cn.ibizlab.businesscentral.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mail;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mailSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mailService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mail.mapper.Mail_mailMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[寄出邮件] 服务对象接口实现
 */
@Slf4j
@Service("Mail_mailServiceImpl")
public class Mail_mailServiceImpl extends EBSServiceImpl<Mail_mailMapper, Mail_mail> implements IMail_mailService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mail_statisticsService mailMailStatisticsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_notificationService mailNotificationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fetchmail.service.IFetchmail_serverService fetchmailServerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailingService mailMassMailingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_messageService mailMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mail.mail" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mail_mail et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_mailService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mail_mail> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mail_mail et) {
        Mail_mail old = new Mail_mail() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_mailService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_mailService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mail_mail> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        mailMailStatisticsService.resetByMailMailId(key);
        mailNotificationService.resetByMailId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        mailMailStatisticsService.resetByMailMailId(idList);
        mailNotificationService.resetByMailId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mail_mail get(Long key) {
        Mail_mail et = getById(key);
        if(et==null){
            et=new Mail_mail();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mail_mail getDraft(Mail_mail et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mail_mail et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mail_mail et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mail_mail et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mail_mail> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mail_mail> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mail_mail> selectByFetchmailServerId(Long id) {
        return baseMapper.selectByFetchmailServerId(id);
    }
    @Override
    public void resetByFetchmailServerId(Long id) {
        this.update(new UpdateWrapper<Mail_mail>().set("fetchmail_server_id",null).eq("fetchmail_server_id",id));
    }

    @Override
    public void resetByFetchmailServerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_mail>().set("fetchmail_server_id",null).in("fetchmail_server_id",ids));
    }

    @Override
    public void removeByFetchmailServerId(Long id) {
        this.remove(new QueryWrapper<Mail_mail>().eq("fetchmail_server_id",id));
    }

	@Override
    public List<Mail_mail> selectByMailingId(Long id) {
        return baseMapper.selectByMailingId(id);
    }
    @Override
    public void resetByMailingId(Long id) {
        this.update(new UpdateWrapper<Mail_mail>().set("mailing_id",null).eq("mailing_id",id));
    }

    @Override
    public void resetByMailingId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_mail>().set("mailing_id",null).in("mailing_id",ids));
    }

    @Override
    public void removeByMailingId(Long id) {
        this.remove(new QueryWrapper<Mail_mail>().eq("mailing_id",id));
    }

	@Override
    public List<Mail_mail> selectByMailMessageId(Long id) {
        return baseMapper.selectByMailMessageId(id);
    }
    @Override
    public void removeByMailMessageId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Mail_mail>().in("mail_message_id",ids));
    }

    @Override
    public void removeByMailMessageId(Long id) {
        this.remove(new QueryWrapper<Mail_mail>().eq("mail_message_id",id));
    }

	@Override
    public List<Mail_mail> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mail_mail>().eq("create_uid",id));
    }

	@Override
    public List<Mail_mail> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mail_mail>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mail_mail> searchDefault(Mail_mailSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mail_mail> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mail_mail>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mail_mail et){
        //实体关系[DER1N_MAIL_MAIL__FETCHMAIL_SERVER__FETCHMAIL_SERVER_ID]
        if(!ObjectUtils.isEmpty(et.getFetchmailServerId())){
            cn.ibizlab.businesscentral.core.odoo_fetchmail.domain.Fetchmail_server odooFetchmailServer=et.getOdooFetchmailServer();
            if(ObjectUtils.isEmpty(odooFetchmailServer)){
                cn.ibizlab.businesscentral.core.odoo_fetchmail.domain.Fetchmail_server majorEntity=fetchmailServerService.get(et.getFetchmailServerId());
                et.setOdooFetchmailServer(majorEntity);
                odooFetchmailServer=majorEntity;
            }
            et.setFetchmailServerIdText(odooFetchmailServer.getName());
        }
        //实体关系[DER1N_MAIL_MAIL__MAIL_MASS_MAILING__MAILING_ID]
        if(!ObjectUtils.isEmpty(et.getMailingId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing odooMailing=et.getOdooMailing();
            if(ObjectUtils.isEmpty(odooMailing)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing majorEntity=mailMassMailingService.get(et.getMailingId());
                et.setOdooMailing(majorEntity);
                odooMailing=majorEntity;
            }
            et.setMailingIdText(odooMailing.getName());
        }
        //实体关系[DER1N_MAIL_MAIL__MAIL_MESSAGE__MAIL_MESSAGE_ID]
        if(!ObjectUtils.isEmpty(et.getMailMessageId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message odooMailMessage=et.getOdooMailMessage();
            if(ObjectUtils.isEmpty(odooMailMessage)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message majorEntity=mailMessageService.get(et.getMailMessageId());
                et.setOdooMailMessage(majorEntity);
                odooMailMessage=majorEntity;
            }
            et.setReplyTo(odooMailMessage.getReplyTo());
            et.setNeedModeration(odooMailMessage.getNeedModeration());
            et.setMessageId(odooMailMessage.getMessageId());
            et.setNoAutoThread(odooMailMessage.getNoAutoThread());
            et.setAuthorId(odooMailMessage.getAuthorId());
            et.setEmailFrom(odooMailMessage.getEmailFrom());
            et.setModel(odooMailMessage.getModel());
            et.setRecordName(odooMailMessage.getRecordName());
            et.setStarred(odooMailMessage.getStarred());
            et.setAddSign(odooMailMessage.getAddSign());
            et.setParentId(odooMailMessage.getParentId());
            et.setRatingValue(odooMailMessage.getRatingValue());
            et.setWebsitePublished(odooMailMessage.getWebsitePublished());
            et.setBody(odooMailMessage.getBody());
            et.setMailServerId(odooMailMessage.getMailServerId());
            et.setDescription(odooMailMessage.getDescription());
            et.setResId(odooMailMessage.getResId());
            et.setSubtypeId(odooMailMessage.getSubtypeId());
            et.setHasError(odooMailMessage.getHasError());
            et.setDate(odooMailMessage.getDate());
            et.setAuthorAvatar(odooMailMessage.getAuthorAvatar());
            et.setModerationStatus(odooMailMessage.getModerationStatus());
            et.setMailActivityTypeId(odooMailMessage.getMailActivityTypeId());
            et.setModeratorId(odooMailMessage.getModeratorId());
            et.setMessageType(odooMailMessage.getMessageType());
            et.setSubject(odooMailMessage.getSubject());
            et.setNeedaction(odooMailMessage.getNeedaction());
        }
        //实体关系[DER1N_MAIL_MAIL__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MAIL_MAIL__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mail_mail> getMailMailByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mail_mail> getMailMailByEntities(List<Mail_mail> entities) {
        List ids =new ArrayList();
        for(Mail_mail entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



