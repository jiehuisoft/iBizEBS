package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_track_confirmation;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_track_confirmationSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_track_confirmation] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-track-confirmation", fallback = stock_track_confirmationFallback.class)
public interface stock_track_confirmationFeignClient {




    @RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations")
    Stock_track_confirmation create(@RequestBody Stock_track_confirmation stock_track_confirmation);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations/batch")
    Boolean createBatch(@RequestBody List<Stock_track_confirmation> stock_track_confirmations);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_track_confirmations/{id}")
    Stock_track_confirmation update(@PathVariable("id") Long id,@RequestBody Stock_track_confirmation stock_track_confirmation);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_track_confirmations/batch")
    Boolean updateBatch(@RequestBody List<Stock_track_confirmation> stock_track_confirmations);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_track_confirmations/{id}")
    Stock_track_confirmation get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations/search")
    Page<Stock_track_confirmation> search(@RequestBody Stock_track_confirmationSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_track_confirmations/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_track_confirmations/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_track_confirmations/select")
    Page<Stock_track_confirmation> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_track_confirmations/getdraft")
    Stock_track_confirmation getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations/checkkey")
    Boolean checkKey(@RequestBody Stock_track_confirmation stock_track_confirmation);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations/save")
    Boolean save(@RequestBody Stock_track_confirmation stock_track_confirmation);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_track_confirmation> stock_track_confirmations);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations/searchdefault")
    Page<Stock_track_confirmation> searchDefault(@RequestBody Stock_track_confirmationSearchContext context);


}
