package cn.ibizlab.businesscentral.core.odoo_product.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_templateSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Product_templateMapper extends BaseMapper<Product_template>{

    Page<Product_template> searchDefault(IPage page, @Param("srf") Product_templateSearchContext context, @Param("ew") Wrapper<Product_template> wrapper) ;
    Page<Product_template> searchMaster(IPage page, @Param("srf") Product_templateSearchContext context, @Param("ew") Wrapper<Product_template> wrapper) ;
    @Override
    Product_template selectById(Serializable id);
    @Override
    int insert(Product_template entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Product_template entity);
    @Override
    int update(@Param(Constants.ENTITY) Product_template entity, @Param("ew") Wrapper<Product_template> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Product_template> selectByCategId(@Param("id") Serializable id) ;

    List<Product_template> selectByCompanyId(@Param("id") Serializable id) ;

    List<Product_template> selectByCreateUid(@Param("id") Serializable id) ;

    List<Product_template> selectByWriteUid(@Param("id") Serializable id) ;

    List<Product_template> selectByUomId(@Param("id") Serializable id) ;

    List<Product_template> selectByUomPoId(@Param("id") Serializable id) ;


        
    boolean saveRelBySupplierTaxesId(@Param("prod_id") Long prod_id, List<cn.ibizlab.businesscentral.util.domain.MultiSelectItem> account_taxes);

        
    boolean saveRelByTaxesId(@Param("prod_id") Long prod_id, List<cn.ibizlab.businesscentral.util.domain.MultiSelectItem> account_taxes);

}
