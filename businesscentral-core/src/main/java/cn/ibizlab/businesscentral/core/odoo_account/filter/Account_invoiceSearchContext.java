package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice;
/**
 * 关系型数据实体[Account_invoice] 查询条件对象
 */
@Slf4j
@Data
public class Account_invoiceSearchContext extends QueryWrapperContext<Account_invoice> {

	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_name_like;//[参考/说明]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_type_eq;//[类型]
	public void setN_type_eq(String n_type_eq) {
        this.n_type_eq = n_type_eq;
        if(!ObjectUtils.isEmpty(this.n_type_eq)){
            this.getSearchCond().eq("type", n_type_eq);
        }
    }
	private String n_journal_id_text_eq;//[日记账]
	public void setN_journal_id_text_eq(String n_journal_id_text_eq) {
        this.n_journal_id_text_eq = n_journal_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_eq)){
            this.getSearchCond().eq("journal_id_text", n_journal_id_text_eq);
        }
    }
	private String n_journal_id_text_like;//[日记账]
	public void setN_journal_id_text_like(String n_journal_id_text_like) {
        this.n_journal_id_text_like = n_journal_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_like)){
            this.getSearchCond().like("journal_id_text", n_journal_id_text_like);
        }
    }
	private String n_purchase_id_text_eq;//[添加采购订单]
	public void setN_purchase_id_text_eq(String n_purchase_id_text_eq) {
        this.n_purchase_id_text_eq = n_purchase_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_purchase_id_text_eq)){
            this.getSearchCond().eq("purchase_id_text", n_purchase_id_text_eq);
        }
    }
	private String n_purchase_id_text_like;//[添加采购订单]
	public void setN_purchase_id_text_like(String n_purchase_id_text_like) {
        this.n_purchase_id_text_like = n_purchase_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_purchase_id_text_like)){
            this.getSearchCond().like("purchase_id_text", n_purchase_id_text_like);
        }
    }
	private String n_vendor_bill_purchase_id_text_eq;//[自动完成]
	public void setN_vendor_bill_purchase_id_text_eq(String n_vendor_bill_purchase_id_text_eq) {
        this.n_vendor_bill_purchase_id_text_eq = n_vendor_bill_purchase_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_vendor_bill_purchase_id_text_eq)){
            this.getSearchCond().eq("vendor_bill_purchase_id_text", n_vendor_bill_purchase_id_text_eq);
        }
    }
	private String n_vendor_bill_purchase_id_text_like;//[自动完成]
	public void setN_vendor_bill_purchase_id_text_like(String n_vendor_bill_purchase_id_text_like) {
        this.n_vendor_bill_purchase_id_text_like = n_vendor_bill_purchase_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_vendor_bill_purchase_id_text_like)){
            this.getSearchCond().like("vendor_bill_purchase_id_text", n_vendor_bill_purchase_id_text_like);
        }
    }
	private String n_source_id_text_eq;//[来源]
	public void setN_source_id_text_eq(String n_source_id_text_eq) {
        this.n_source_id_text_eq = n_source_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_source_id_text_eq)){
            this.getSearchCond().eq("source_id_text", n_source_id_text_eq);
        }
    }
	private String n_source_id_text_like;//[来源]
	public void setN_source_id_text_like(String n_source_id_text_like) {
        this.n_source_id_text_like = n_source_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_source_id_text_like)){
            this.getSearchCond().like("source_id_text", n_source_id_text_like);
        }
    }
	private String n_campaign_id_text_eq;//[营销]
	public void setN_campaign_id_text_eq(String n_campaign_id_text_eq) {
        this.n_campaign_id_text_eq = n_campaign_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_text_eq)){
            this.getSearchCond().eq("campaign_id_text", n_campaign_id_text_eq);
        }
    }
	private String n_campaign_id_text_like;//[营销]
	public void setN_campaign_id_text_like(String n_campaign_id_text_like) {
        this.n_campaign_id_text_like = n_campaign_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_text_like)){
            this.getSearchCond().like("campaign_id_text", n_campaign_id_text_like);
        }
    }
	private String n_partner_shipping_id_text_eq;//[送货地址]
	public void setN_partner_shipping_id_text_eq(String n_partner_shipping_id_text_eq) {
        this.n_partner_shipping_id_text_eq = n_partner_shipping_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_shipping_id_text_eq)){
            this.getSearchCond().eq("partner_shipping_id_text", n_partner_shipping_id_text_eq);
        }
    }
	private String n_partner_shipping_id_text_like;//[送货地址]
	public void setN_partner_shipping_id_text_like(String n_partner_shipping_id_text_like) {
        this.n_partner_shipping_id_text_like = n_partner_shipping_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_shipping_id_text_like)){
            this.getSearchCond().like("partner_shipping_id_text", n_partner_shipping_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_incoterms_id_text_eq;//[贸易条款]
	public void setN_incoterms_id_text_eq(String n_incoterms_id_text_eq) {
        this.n_incoterms_id_text_eq = n_incoterms_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_incoterms_id_text_eq)){
            this.getSearchCond().eq("incoterms_id_text", n_incoterms_id_text_eq);
        }
    }
	private String n_incoterms_id_text_like;//[贸易条款]
	public void setN_incoterms_id_text_like(String n_incoterms_id_text_like) {
        this.n_incoterms_id_text_like = n_incoterms_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_incoterms_id_text_like)){
            this.getSearchCond().like("incoterms_id_text", n_incoterms_id_text_like);
        }
    }
	private String n_account_id_text_eq;//[科目]
	public void setN_account_id_text_eq(String n_account_id_text_eq) {
        this.n_account_id_text_eq = n_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_eq)){
            this.getSearchCond().eq("account_id_text", n_account_id_text_eq);
        }
    }
	private String n_account_id_text_like;//[科目]
	public void setN_account_id_text_like(String n_account_id_text_like) {
        this.n_account_id_text_like = n_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_like)){
            this.getSearchCond().like("account_id_text", n_account_id_text_like);
        }
    }
	private String n_incoterm_id_text_eq;//[国际贸易术语]
	public void setN_incoterm_id_text_eq(String n_incoterm_id_text_eq) {
        this.n_incoterm_id_text_eq = n_incoterm_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_incoterm_id_text_eq)){
            this.getSearchCond().eq("incoterm_id_text", n_incoterm_id_text_eq);
        }
    }
	private String n_incoterm_id_text_like;//[国际贸易术语]
	public void setN_incoterm_id_text_like(String n_incoterm_id_text_like) {
        this.n_incoterm_id_text_like = n_incoterm_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_incoterm_id_text_like)){
            this.getSearchCond().like("incoterm_id_text", n_incoterm_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[业务伙伴]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[业务伙伴]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_medium_id_text_eq;//[媒体]
	public void setN_medium_id_text_eq(String n_medium_id_text_eq) {
        this.n_medium_id_text_eq = n_medium_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_medium_id_text_eq)){
            this.getSearchCond().eq("medium_id_text", n_medium_id_text_eq);
        }
    }
	private String n_medium_id_text_like;//[媒体]
	public void setN_medium_id_text_like(String n_medium_id_text_like) {
        this.n_medium_id_text_like = n_medium_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_medium_id_text_like)){
            this.getSearchCond().like("medium_id_text", n_medium_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[销售员]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[销售员]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_cash_rounding_id_text_eq;//[现金舍入方式]
	public void setN_cash_rounding_id_text_eq(String n_cash_rounding_id_text_eq) {
        this.n_cash_rounding_id_text_eq = n_cash_rounding_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_cash_rounding_id_text_eq)){
            this.getSearchCond().eq("cash_rounding_id_text", n_cash_rounding_id_text_eq);
        }
    }
	private String n_cash_rounding_id_text_like;//[现金舍入方式]
	public void setN_cash_rounding_id_text_like(String n_cash_rounding_id_text_like) {
        this.n_cash_rounding_id_text_like = n_cash_rounding_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_cash_rounding_id_text_like)){
            this.getSearchCond().like("cash_rounding_id_text", n_cash_rounding_id_text_like);
        }
    }
	private String n_vendor_bill_id_text_eq;//[供应商账单]
	public void setN_vendor_bill_id_text_eq(String n_vendor_bill_id_text_eq) {
        this.n_vendor_bill_id_text_eq = n_vendor_bill_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_vendor_bill_id_text_eq)){
            this.getSearchCond().eq("vendor_bill_id_text", n_vendor_bill_id_text_eq);
        }
    }
	private String n_vendor_bill_id_text_like;//[供应商账单]
	public void setN_vendor_bill_id_text_like(String n_vendor_bill_id_text_like) {
        this.n_vendor_bill_id_text_like = n_vendor_bill_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_vendor_bill_id_text_like)){
            this.getSearchCond().like("vendor_bill_id_text", n_vendor_bill_id_text_like);
        }
    }
	private String n_refund_invoice_id_text_eq;//[此发票为信用票的发票]
	public void setN_refund_invoice_id_text_eq(String n_refund_invoice_id_text_eq) {
        this.n_refund_invoice_id_text_eq = n_refund_invoice_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_refund_invoice_id_text_eq)){
            this.getSearchCond().eq("refund_invoice_id_text", n_refund_invoice_id_text_eq);
        }
    }
	private String n_refund_invoice_id_text_like;//[此发票为信用票的发票]
	public void setN_refund_invoice_id_text_like(String n_refund_invoice_id_text_like) {
        this.n_refund_invoice_id_text_like = n_refund_invoice_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_refund_invoice_id_text_like)){
            this.getSearchCond().like("refund_invoice_id_text", n_refund_invoice_id_text_like);
        }
    }
	private String n_fiscal_position_id_text_eq;//[税科目调整]
	public void setN_fiscal_position_id_text_eq(String n_fiscal_position_id_text_eq) {
        this.n_fiscal_position_id_text_eq = n_fiscal_position_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_text_eq)){
            this.getSearchCond().eq("fiscal_position_id_text", n_fiscal_position_id_text_eq);
        }
    }
	private String n_fiscal_position_id_text_like;//[税科目调整]
	public void setN_fiscal_position_id_text_like(String n_fiscal_position_id_text_like) {
        this.n_fiscal_position_id_text_like = n_fiscal_position_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_text_like)){
            this.getSearchCond().like("fiscal_position_id_text", n_fiscal_position_id_text_like);
        }
    }
	private String n_number_eq;//[号码]
	public void setN_number_eq(String n_number_eq) {
        this.n_number_eq = n_number_eq;
        if(!ObjectUtils.isEmpty(this.n_number_eq)){
            this.getSearchCond().eq("number", n_number_eq);
        }
    }
	private String n_number_like;//[号码]
	public void setN_number_like(String n_number_like) {
        this.n_number_like = n_number_like;
        if(!ObjectUtils.isEmpty(this.n_number_like)){
            this.getSearchCond().like("number", n_number_like);
        }
    }
	private String n_commercial_partner_id_text_eq;//[商业实体]
	public void setN_commercial_partner_id_text_eq(String n_commercial_partner_id_text_eq) {
        this.n_commercial_partner_id_text_eq = n_commercial_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_text_eq)){
            this.getSearchCond().eq("commercial_partner_id_text", n_commercial_partner_id_text_eq);
        }
    }
	private String n_commercial_partner_id_text_like;//[商业实体]
	public void setN_commercial_partner_id_text_like(String n_commercial_partner_id_text_like) {
        this.n_commercial_partner_id_text_like = n_commercial_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_text_like)){
            this.getSearchCond().like("commercial_partner_id_text", n_commercial_partner_id_text_like);
        }
    }
	private String n_team_id_text_eq;//[销售团队]
	public void setN_team_id_text_eq(String n_team_id_text_eq) {
        this.n_team_id_text_eq = n_team_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_eq)){
            this.getSearchCond().eq("team_id_text", n_team_id_text_eq);
        }
    }
	private String n_team_id_text_like;//[销售团队]
	public void setN_team_id_text_like(String n_team_id_text_like) {
        this.n_team_id_text_like = n_team_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_like)){
            this.getSearchCond().like("team_id_text", n_team_id_text_like);
        }
    }
	private String n_payment_term_id_text_eq;//[付款条款]
	public void setN_payment_term_id_text_eq(String n_payment_term_id_text_eq) {
        this.n_payment_term_id_text_eq = n_payment_term_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_term_id_text_eq)){
            this.getSearchCond().eq("payment_term_id_text", n_payment_term_id_text_eq);
        }
    }
	private String n_payment_term_id_text_like;//[付款条款]
	public void setN_payment_term_id_text_like(String n_payment_term_id_text_like) {
        this.n_payment_term_id_text_like = n_payment_term_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_payment_term_id_text_like)){
            this.getSearchCond().like("payment_term_id_text", n_payment_term_id_text_like);
        }
    }
	private Long n_user_id_eq;//[销售员]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_team_id_eq;//[销售团队]
	public void setN_team_id_eq(Long n_team_id_eq) {
        this.n_team_id_eq = n_team_id_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_eq)){
            this.getSearchCond().eq("team_id", n_team_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_account_id_eq;//[科目]
	public void setN_account_id_eq(Long n_account_id_eq) {
        this.n_account_id_eq = n_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_eq)){
            this.getSearchCond().eq("account_id", n_account_id_eq);
        }
    }
	private Long n_medium_id_eq;//[媒体]
	public void setN_medium_id_eq(Long n_medium_id_eq) {
        this.n_medium_id_eq = n_medium_id_eq;
        if(!ObjectUtils.isEmpty(this.n_medium_id_eq)){
            this.getSearchCond().eq("medium_id", n_medium_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_move_id_eq;//[日记账分录]
	public void setN_move_id_eq(Long n_move_id_eq) {
        this.n_move_id_eq = n_move_id_eq;
        if(!ObjectUtils.isEmpty(this.n_move_id_eq)){
            this.getSearchCond().eq("move_id", n_move_id_eq);
        }
    }
	private Long n_payment_term_id_eq;//[付款条款]
	public void setN_payment_term_id_eq(Long n_payment_term_id_eq) {
        this.n_payment_term_id_eq = n_payment_term_id_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_term_id_eq)){
            this.getSearchCond().eq("payment_term_id", n_payment_term_id_eq);
        }
    }
	private Long n_journal_id_eq;//[日记账]
	public void setN_journal_id_eq(Long n_journal_id_eq) {
        this.n_journal_id_eq = n_journal_id_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_eq)){
            this.getSearchCond().eq("journal_id", n_journal_id_eq);
        }
    }
	private Long n_incoterm_id_eq;//[国际贸易术语]
	public void setN_incoterm_id_eq(Long n_incoterm_id_eq) {
        this.n_incoterm_id_eq = n_incoterm_id_eq;
        if(!ObjectUtils.isEmpty(this.n_incoterm_id_eq)){
            this.getSearchCond().eq("incoterm_id", n_incoterm_id_eq);
        }
    }
	private Long n_purchase_id_eq;//[添加采购订单]
	public void setN_purchase_id_eq(Long n_purchase_id_eq) {
        this.n_purchase_id_eq = n_purchase_id_eq;
        if(!ObjectUtils.isEmpty(this.n_purchase_id_eq)){
            this.getSearchCond().eq("purchase_id", n_purchase_id_eq);
        }
    }
	private Long n_fiscal_position_id_eq;//[税科目调整]
	public void setN_fiscal_position_id_eq(Long n_fiscal_position_id_eq) {
        this.n_fiscal_position_id_eq = n_fiscal_position_id_eq;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_eq)){
            this.getSearchCond().eq("fiscal_position_id", n_fiscal_position_id_eq);
        }
    }
	private Long n_refund_invoice_id_eq;//[此发票为信用票的发票]
	public void setN_refund_invoice_id_eq(Long n_refund_invoice_id_eq) {
        this.n_refund_invoice_id_eq = n_refund_invoice_id_eq;
        if(!ObjectUtils.isEmpty(this.n_refund_invoice_id_eq)){
            this.getSearchCond().eq("refund_invoice_id", n_refund_invoice_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_partner_id_eq;//[业务伙伴]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_partner_bank_id_eq;//[银行账户]
	public void setN_partner_bank_id_eq(Long n_partner_bank_id_eq) {
        this.n_partner_bank_id_eq = n_partner_bank_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_bank_id_eq)){
            this.getSearchCond().eq("partner_bank_id", n_partner_bank_id_eq);
        }
    }
	private Long n_cash_rounding_id_eq;//[现金舍入方式]
	public void setN_cash_rounding_id_eq(Long n_cash_rounding_id_eq) {
        this.n_cash_rounding_id_eq = n_cash_rounding_id_eq;
        if(!ObjectUtils.isEmpty(this.n_cash_rounding_id_eq)){
            this.getSearchCond().eq("cash_rounding_id", n_cash_rounding_id_eq);
        }
    }
	private Long n_commercial_partner_id_eq;//[商业实体]
	public void setN_commercial_partner_id_eq(Long n_commercial_partner_id_eq) {
        this.n_commercial_partner_id_eq = n_commercial_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_eq)){
            this.getSearchCond().eq("commercial_partner_id", n_commercial_partner_id_eq);
        }
    }
	private Long n_partner_shipping_id_eq;//[送货地址]
	public void setN_partner_shipping_id_eq(Long n_partner_shipping_id_eq) {
        this.n_partner_shipping_id_eq = n_partner_shipping_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_shipping_id_eq)){
            this.getSearchCond().eq("partner_shipping_id", n_partner_shipping_id_eq);
        }
    }
	private Long n_incoterms_id_eq;//[贸易条款]
	public void setN_incoterms_id_eq(Long n_incoterms_id_eq) {
        this.n_incoterms_id_eq = n_incoterms_id_eq;
        if(!ObjectUtils.isEmpty(this.n_incoterms_id_eq)){
            this.getSearchCond().eq("incoterms_id", n_incoterms_id_eq);
        }
    }
	private Long n_source_id_eq;//[来源]
	public void setN_source_id_eq(Long n_source_id_eq) {
        this.n_source_id_eq = n_source_id_eq;
        if(!ObjectUtils.isEmpty(this.n_source_id_eq)){
            this.getSearchCond().eq("source_id", n_source_id_eq);
        }
    }
	private Long n_vendor_bill_id_eq;//[供应商账单]
	public void setN_vendor_bill_id_eq(Long n_vendor_bill_id_eq) {
        this.n_vendor_bill_id_eq = n_vendor_bill_id_eq;
        if(!ObjectUtils.isEmpty(this.n_vendor_bill_id_eq)){
            this.getSearchCond().eq("vendor_bill_id", n_vendor_bill_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_vendor_bill_purchase_id_eq;//[自动完成]
	public void setN_vendor_bill_purchase_id_eq(Long n_vendor_bill_purchase_id_eq) {
        this.n_vendor_bill_purchase_id_eq = n_vendor_bill_purchase_id_eq;
        if(!ObjectUtils.isEmpty(this.n_vendor_bill_purchase_id_eq)){
            this.getSearchCond().eq("vendor_bill_purchase_id", n_vendor_bill_purchase_id_eq);
        }
    }
	private Long n_campaign_id_eq;//[营销]
	public void setN_campaign_id_eq(Long n_campaign_id_eq) {
        this.n_campaign_id_eq = n_campaign_id_eq;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_eq)){
            this.getSearchCond().eq("campaign_id", n_campaign_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



