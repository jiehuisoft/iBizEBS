package cn.ibizlab.businesscentral.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge_user_wizard;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_badge_user_wizardSearchContext;
import cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_badge_user_wizardService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_gamification.mapper.Gamification_badge_user_wizardMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[游戏化用户徽章向导] 服务对象接口实现
 */
@Slf4j
@Service("Gamification_badge_user_wizardServiceImpl")
public class Gamification_badge_user_wizardServiceImpl extends EBSServiceImpl<Gamification_badge_user_wizardMapper, Gamification_badge_user_wizard> implements IGamification_badge_user_wizardService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_badgeService gamificationBadgeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "gamification.badge.user.wizard" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Gamification_badge_user_wizard et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IGamification_badge_user_wizardService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Gamification_badge_user_wizard> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Gamification_badge_user_wizard et) {
        Gamification_badge_user_wizard old = new Gamification_badge_user_wizard() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IGamification_badge_user_wizardService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IGamification_badge_user_wizardService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Gamification_badge_user_wizard> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Gamification_badge_user_wizard get(Long key) {
        Gamification_badge_user_wizard et = getById(key);
        if(et==null){
            et=new Gamification_badge_user_wizard();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Gamification_badge_user_wizard getDraft(Gamification_badge_user_wizard et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Gamification_badge_user_wizard et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Gamification_badge_user_wizard et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Gamification_badge_user_wizard et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Gamification_badge_user_wizard> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Gamification_badge_user_wizard> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Gamification_badge_user_wizard> selectByBadgeId(Long id) {
        return baseMapper.selectByBadgeId(id);
    }
    @Override
    public void resetByBadgeId(Long id) {
        this.update(new UpdateWrapper<Gamification_badge_user_wizard>().set("badge_id",null).eq("badge_id",id));
    }

    @Override
    public void resetByBadgeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Gamification_badge_user_wizard>().set("badge_id",null).in("badge_id",ids));
    }

    @Override
    public void removeByBadgeId(Long id) {
        this.remove(new QueryWrapper<Gamification_badge_user_wizard>().eq("badge_id",id));
    }

	@Override
    public List<Gamification_badge_user_wizard> selectByEmployeeId(Long id) {
        return baseMapper.selectByEmployeeId(id);
    }
    @Override
    public void resetByEmployeeId(Long id) {
        this.update(new UpdateWrapper<Gamification_badge_user_wizard>().set("employee_id",null).eq("employee_id",id));
    }

    @Override
    public void resetByEmployeeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Gamification_badge_user_wizard>().set("employee_id",null).in("employee_id",ids));
    }

    @Override
    public void removeByEmployeeId(Long id) {
        this.remove(new QueryWrapper<Gamification_badge_user_wizard>().eq("employee_id",id));
    }

	@Override
    public List<Gamification_badge_user_wizard> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Gamification_badge_user_wizard>().eq("create_uid",id));
    }

	@Override
    public List<Gamification_badge_user_wizard> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Gamification_badge_user_wizard>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Gamification_badge_user_wizard> searchDefault(Gamification_badge_user_wizardSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Gamification_badge_user_wizard> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Gamification_badge_user_wizard>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Gamification_badge_user_wizard et){
        //实体关系[DER1N_GAMIFICATION_BADGE_USER_WIZARD__GAMIFICATION_BADGE__BADGE_ID]
        if(!ObjectUtils.isEmpty(et.getBadgeId())){
            cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge odooBadge=et.getOdooBadge();
            if(ObjectUtils.isEmpty(odooBadge)){
                cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge majorEntity=gamificationBadgeService.get(et.getBadgeId());
                et.setOdooBadge(majorEntity);
                odooBadge=majorEntity;
            }
            et.setBadgeIdText(odooBadge.getName());
        }
        //实体关系[DER1N_GAMIFICATION_BADGE_USER_WIZARD__HR_EMPLOYEE__EMPLOYEE_ID]
        if(!ObjectUtils.isEmpty(et.getEmployeeId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmployee=et.getOdooEmployee();
            if(ObjectUtils.isEmpty(odooEmployee)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getEmployeeId());
                et.setOdooEmployee(majorEntity);
                odooEmployee=majorEntity;
            }
            et.setUserId(odooEmployee.getUserId());
            et.setEmployeeIdText(odooEmployee.getName());
        }
        //实体关系[DER1N_GAMIFICATION_BADGE_USER_WIZARD__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_GAMIFICATION_BADGE_USER_WIZARD__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Gamification_badge_user_wizard> getGamificationBadgeUserWizardByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Gamification_badge_user_wizard> getGamificationBadgeUserWizardByEntities(List<Gamification_badge_user_wizard> entities) {
        List ids =new ArrayList();
        for(Gamification_badge_user_wizard entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



