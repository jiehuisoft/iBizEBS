package cn.ibizlab.businesscentral.core.odoo_web_editor.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test;
import cn.ibizlab.businesscentral.core.odoo_web_editor.filter.Web_editor_converter_testSearchContext;
import cn.ibizlab.businesscentral.core.odoo_web_editor.service.IWeb_editor_converter_testService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_web_editor.mapper.Web_editor_converter_testMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[Web编辑器转换器测试] 服务对象接口实现
 */
@Slf4j
@Service("Web_editor_converter_testServiceImpl")
public class Web_editor_converter_testServiceImpl extends EBSServiceImpl<Web_editor_converter_testMapper, Web_editor_converter_test> implements IWeb_editor_converter_testService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_web_editor.service.IWeb_editor_converter_test_subService webEditorConverterTestSubService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "web.editor.converter.test" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Web_editor_converter_test et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IWeb_editor_converter_testService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Web_editor_converter_test> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Web_editor_converter_test et) {
        Web_editor_converter_test old = new Web_editor_converter_test() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IWeb_editor_converter_testService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IWeb_editor_converter_testService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Web_editor_converter_test> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Web_editor_converter_test get(Long key) {
        Web_editor_converter_test et = getById(key);
        if(et==null){
            et=new Web_editor_converter_test();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Web_editor_converter_test getDraft(Web_editor_converter_test et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Web_editor_converter_test et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Web_editor_converter_test et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Web_editor_converter_test et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Web_editor_converter_test> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Web_editor_converter_test> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Web_editor_converter_test> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Web_editor_converter_test>().eq("create_uid",id));
    }

	@Override
    public List<Web_editor_converter_test> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Web_editor_converter_test>().eq("write_uid",id));
    }

	@Override
    public List<Web_editor_converter_test> selectByMany2one(Long id) {
        return baseMapper.selectByMany2one(id);
    }
    @Override
    public void resetByMany2one(Long id) {
        this.update(new UpdateWrapper<Web_editor_converter_test>().set("many2one",null).eq("many2one",id));
    }

    @Override
    public void resetByMany2one(Collection<Long> ids) {
        this.update(new UpdateWrapper<Web_editor_converter_test>().set("many2one",null).in("many2one",ids));
    }

    @Override
    public void removeByMany2one(Long id) {
        this.remove(new QueryWrapper<Web_editor_converter_test>().eq("many2one",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Web_editor_converter_test> searchDefault(Web_editor_converter_testSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Web_editor_converter_test> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Web_editor_converter_test>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Web_editor_converter_test et){
        //实体关系[DER1N_WEB_EDITOR_CONVERTER_TEST__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_WEB_EDITOR_CONVERTER_TEST__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_WEB_EDITOR_CONVERTER_TEST__WEB_EDITOR_CONVERTER_TEST_SUB__MANY2ONE]
        if(!ObjectUtils.isEmpty(et.getMany2one())){
            cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test_sub odooMany2one=et.getOdooMany2one();
            if(ObjectUtils.isEmpty(odooMany2one)){
                cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test_sub majorEntity=webEditorConverterTestSubService.get(et.getMany2one());
                et.setOdooMany2one(majorEntity);
                odooMany2one=majorEntity;
            }
            et.setMany2oneText(odooMany2one.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Web_editor_converter_test> getWebEditorConverterTestByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Web_editor_converter_test> getWebEditorConverterTestByEntities(List<Web_editor_converter_test> entities) {
        List ids =new ArrayList();
        for(Web_editor_converter_test entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



