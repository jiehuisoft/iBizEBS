package cn.ibizlab.businesscentral.core.odoo_base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_bank;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_bankSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Res_bankMapper extends BaseMapper<Res_bank>{

    Page<Res_bank> searchDefault(IPage page, @Param("srf") Res_bankSearchContext context, @Param("ew") Wrapper<Res_bank> wrapper) ;
    @Override
    Res_bank selectById(Serializable id);
    @Override
    int insert(Res_bank entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Res_bank entity);
    @Override
    int update(@Param(Constants.ENTITY) Res_bank entity, @Param("ew") Wrapper<Res_bank> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Res_bank> selectByState(@Param("id") Serializable id) ;

    List<Res_bank> selectByCountry(@Param("id") Serializable id) ;

    List<Res_bank> selectByCreateUid(@Param("id") Serializable id) ;

    List<Res_bank> selectByWriteUid(@Param("id") Serializable id) ;


}
