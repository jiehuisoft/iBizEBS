package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_track_line;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_track_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_track_line] 服务对象接口
 */
public interface IStock_track_lineService extends IService<Stock_track_line>{

    boolean create(Stock_track_line et) ;
    void createBatch(List<Stock_track_line> list) ;
    boolean update(Stock_track_line et) ;
    void updateBatch(List<Stock_track_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_track_line get(Long key) ;
    Stock_track_line getDraft(Stock_track_line et) ;
    boolean checkKey(Stock_track_line et) ;
    boolean save(Stock_track_line et) ;
    void saveBatch(List<Stock_track_line> list) ;
    Page<Stock_track_line> searchDefault(Stock_track_lineSearchContext context) ;
    List<Stock_track_line> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Stock_track_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_track_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_track_line> selectByWizardId(Long id);
    void resetByWizardId(Long id);
    void resetByWizardId(Collection<Long> ids);
    void removeByWizardId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_track_line> getStockTrackLineByIds(List<Long> ids) ;
    List<Stock_track_line> getStockTrackLineByEntities(List<Stock_track_line> entities) ;
}


