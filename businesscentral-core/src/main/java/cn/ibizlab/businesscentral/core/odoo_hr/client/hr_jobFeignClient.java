package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_jobSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_job] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-hr:odoo-hr}", contextId = "hr-job", fallback = hr_jobFallback.class)
public interface hr_jobFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_jobs/{id}")
    Hr_job update(@PathVariable("id") Long id,@RequestBody Hr_job hr_job);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_jobs/batch")
    Boolean updateBatch(@RequestBody List<Hr_job> hr_jobs);



    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_jobs/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_jobs/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_jobs")
    Hr_job create(@RequestBody Hr_job hr_job);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_jobs/batch")
    Boolean createBatch(@RequestBody List<Hr_job> hr_jobs);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_jobs/search")
    Page<Hr_job> search(@RequestBody Hr_jobSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_jobs/{id}")
    Hr_job get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_jobs/select")
    Page<Hr_job> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_jobs/getdraft")
    Hr_job getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/hr_jobs/checkkey")
    Boolean checkKey(@RequestBody Hr_job hr_job);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_jobs/save")
    Boolean save(@RequestBody Hr_job hr_job);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_jobs/savebatch")
    Boolean saveBatch(@RequestBody List<Hr_job> hr_jobs);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_jobs/searchdefault")
    Page<Hr_job> searchDefault(@RequestBody Hr_jobSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_jobs/searchmaster")
    Page<Hr_job> searchMaster(@RequestBody Hr_jobSearchContext context);


}
