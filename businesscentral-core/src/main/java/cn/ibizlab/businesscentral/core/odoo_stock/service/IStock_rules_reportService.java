package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rules_report;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_rules_reportSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_rules_report] 服务对象接口
 */
public interface IStock_rules_reportService extends IService<Stock_rules_report>{

    boolean create(Stock_rules_report et) ;
    void createBatch(List<Stock_rules_report> list) ;
    boolean update(Stock_rules_report et) ;
    void updateBatch(List<Stock_rules_report> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_rules_report get(Long key) ;
    Stock_rules_report getDraft(Stock_rules_report et) ;
    boolean checkKey(Stock_rules_report et) ;
    boolean save(Stock_rules_report et) ;
    void saveBatch(List<Stock_rules_report> list) ;
    Page<Stock_rules_report> searchDefault(Stock_rules_reportSearchContext context) ;
    List<Stock_rules_report> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Stock_rules_report> selectByProductTmplId(Long id);
    void resetByProductTmplId(Long id);
    void resetByProductTmplId(Collection<Long> ids);
    void removeByProductTmplId(Long id);
    List<Stock_rules_report> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_rules_report> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_rules_report> getStockRulesReportByIds(List<Long> ids) ;
    List<Stock_rules_report> getStockRulesReportByEntities(List<Stock_rules_report> entities) ;
}


