package cn.ibizlab.businesscentral.core.odoo_rating.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_rating.domain.Rating_mixin;
import cn.ibizlab.businesscentral.core.odoo_rating.filter.Rating_mixinSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[rating_mixin] 服务对象接口
 */
@Component
public class rating_mixinFallback implements rating_mixinFeignClient{


    public Rating_mixin create(Rating_mixin rating_mixin){
            return null;
     }
    public Boolean createBatch(List<Rating_mixin> rating_mixins){
            return false;
     }

    public Rating_mixin get(Long id){
            return null;
     }



    public Page<Rating_mixin> search(Rating_mixinSearchContext context){
            return null;
     }


    public Rating_mixin update(Long id, Rating_mixin rating_mixin){
            return null;
     }
    public Boolean updateBatch(List<Rating_mixin> rating_mixins){
            return false;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Rating_mixin> select(){
            return null;
     }

    public Rating_mixin getDraft(){
            return null;
    }



    public Boolean checkKey(Rating_mixin rating_mixin){
            return false;
     }


    public Boolean save(Rating_mixin rating_mixin){
            return false;
     }
    public Boolean saveBatch(List<Rating_mixin> rating_mixins){
            return false;
     }

    public Page<Rating_mixin> searchDefault(Rating_mixinSearchContext context){
            return null;
     }


}
