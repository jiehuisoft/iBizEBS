package cn.ibizlab.businesscentral.core.odoo_purchase.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition_line;

/**
 * 关系型数据实体[product_change] 对象
 */
public interface IPurchase_requisition_lineproduct_changeLogic {

    void execute(Purchase_requisition_line et) ;

}
