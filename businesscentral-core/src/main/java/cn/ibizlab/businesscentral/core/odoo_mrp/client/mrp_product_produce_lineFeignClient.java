package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_product_produce_line;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_product_produce_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_product_produce_line] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mrp:odoo-mrp}", contextId = "mrp-product-produce-line", fallback = mrp_product_produce_lineFallback.class)
public interface mrp_product_produce_lineFeignClient {



    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_product_produce_lines/{id}")
    Mrp_product_produce_line update(@PathVariable("id") Long id,@RequestBody Mrp_product_produce_line mrp_product_produce_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_product_produce_lines/batch")
    Boolean updateBatch(@RequestBody List<Mrp_product_produce_line> mrp_product_produce_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines/search")
    Page<Mrp_product_produce_line> search(@RequestBody Mrp_product_produce_lineSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_product_produce_lines/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_product_produce_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_product_produce_lines/{id}")
    Mrp_product_produce_line get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines")
    Mrp_product_produce_line create(@RequestBody Mrp_product_produce_line mrp_product_produce_line);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines/batch")
    Boolean createBatch(@RequestBody List<Mrp_product_produce_line> mrp_product_produce_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_product_produce_lines/select")
    Page<Mrp_product_produce_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_product_produce_lines/getdraft")
    Mrp_product_produce_line getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines/checkkey")
    Boolean checkKey(@RequestBody Mrp_product_produce_line mrp_product_produce_line);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines/save")
    Boolean save(@RequestBody Mrp_product_produce_line mrp_product_produce_line);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines/savebatch")
    Boolean saveBatch(@RequestBody List<Mrp_product_produce_line> mrp_product_produce_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines/searchdefault")
    Page<Mrp_product_produce_line> searchDefault(@RequestBody Mrp_product_produce_lineSearchContext context);


}
