package cn.ibizlab.businesscentral.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge_user_wizard;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_badge_user_wizardSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Gamification_badge_user_wizard] 服务对象接口
 */
public interface IGamification_badge_user_wizardService extends IService<Gamification_badge_user_wizard>{

    boolean create(Gamification_badge_user_wizard et) ;
    void createBatch(List<Gamification_badge_user_wizard> list) ;
    boolean update(Gamification_badge_user_wizard et) ;
    void updateBatch(List<Gamification_badge_user_wizard> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Gamification_badge_user_wizard get(Long key) ;
    Gamification_badge_user_wizard getDraft(Gamification_badge_user_wizard et) ;
    boolean checkKey(Gamification_badge_user_wizard et) ;
    boolean save(Gamification_badge_user_wizard et) ;
    void saveBatch(List<Gamification_badge_user_wizard> list) ;
    Page<Gamification_badge_user_wizard> searchDefault(Gamification_badge_user_wizardSearchContext context) ;
    List<Gamification_badge_user_wizard> selectByBadgeId(Long id);
    void resetByBadgeId(Long id);
    void resetByBadgeId(Collection<Long> ids);
    void removeByBadgeId(Long id);
    List<Gamification_badge_user_wizard> selectByEmployeeId(Long id);
    void resetByEmployeeId(Long id);
    void resetByEmployeeId(Collection<Long> ids);
    void removeByEmployeeId(Long id);
    List<Gamification_badge_user_wizard> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Gamification_badge_user_wizard> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Gamification_badge_user_wizard> getGamificationBadgeUserWizardByIds(List<Long> ids) ;
    List<Gamification_badge_user_wizard> getGamificationBadgeUserWizardByEntities(List<Gamification_badge_user_wizard> entities) ;
}


