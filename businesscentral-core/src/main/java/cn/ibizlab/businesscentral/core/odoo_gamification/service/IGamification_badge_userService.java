package cn.ibizlab.businesscentral.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge_user;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_badge_userSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Gamification_badge_user] 服务对象接口
 */
public interface IGamification_badge_userService extends IService<Gamification_badge_user>{

    boolean create(Gamification_badge_user et) ;
    void createBatch(List<Gamification_badge_user> list) ;
    boolean update(Gamification_badge_user et) ;
    void updateBatch(List<Gamification_badge_user> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Gamification_badge_user get(Long key) ;
    Gamification_badge_user getDraft(Gamification_badge_user et) ;
    boolean checkKey(Gamification_badge_user et) ;
    boolean save(Gamification_badge_user et) ;
    void saveBatch(List<Gamification_badge_user> list) ;
    Page<Gamification_badge_user> searchDefault(Gamification_badge_userSearchContext context) ;
    List<Gamification_badge_user> selectByBadgeId(Long id);
    void removeByBadgeId(Collection<Long> ids);
    void removeByBadgeId(Long id);
    List<Gamification_badge_user> selectByChallengeId(Long id);
    void resetByChallengeId(Long id);
    void resetByChallengeId(Collection<Long> ids);
    void removeByChallengeId(Long id);
    List<Gamification_badge_user> selectByEmployeeId(Long id);
    void resetByEmployeeId(Long id);
    void resetByEmployeeId(Collection<Long> ids);
    void removeByEmployeeId(Long id);
    List<Gamification_badge_user> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Gamification_badge_user> selectBySenderId(Long id);
    void resetBySenderId(Long id);
    void resetBySenderId(Collection<Long> ids);
    void removeBySenderId(Long id);
    List<Gamification_badge_user> selectByUserId(Long id);
    void removeByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Gamification_badge_user> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Gamification_badge_user> getGamificationBadgeUserByIds(List<Long> ids) ;
    List<Gamification_badge_user> getGamificationBadgeUserByEntities(List<Gamification_badge_user> entities) ;
}


