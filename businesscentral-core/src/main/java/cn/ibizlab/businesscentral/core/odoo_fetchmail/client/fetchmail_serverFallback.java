package cn.ibizlab.businesscentral.core.odoo_fetchmail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fetchmail.domain.Fetchmail_server;
import cn.ibizlab.businesscentral.core.odoo_fetchmail.filter.Fetchmail_serverSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[fetchmail_server] 服务对象接口
 */
@Component
public class fetchmail_serverFallback implements fetchmail_serverFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Fetchmail_server create(Fetchmail_server fetchmail_server){
            return null;
     }
    public Boolean createBatch(List<Fetchmail_server> fetchmail_servers){
            return false;
     }

    public Fetchmail_server update(Long id, Fetchmail_server fetchmail_server){
            return null;
     }
    public Boolean updateBatch(List<Fetchmail_server> fetchmail_servers){
            return false;
     }


    public Page<Fetchmail_server> search(Fetchmail_serverSearchContext context){
            return null;
     }




    public Fetchmail_server get(Long id){
            return null;
     }


    public Page<Fetchmail_server> select(){
            return null;
     }

    public Fetchmail_server getDraft(){
            return null;
    }



    public Boolean checkKey(Fetchmail_server fetchmail_server){
            return false;
     }


    public Boolean save(Fetchmail_server fetchmail_server){
            return false;
     }
    public Boolean saveBatch(List<Fetchmail_server> fetchmail_servers){
            return false;
     }

    public Page<Fetchmail_server> searchDefault(Fetchmail_serverSearchContext context){
            return null;
     }


}
