package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_scrap;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_scrapSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_scrapService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_scrapMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[报废] 服务对象接口实现
 */
@Slf4j
@Service("Stock_scrapServiceImpl")
public class Stock_scrapServiceImpl extends EBSServiceImpl<Stock_scrapMapper, Stock_scrap> implements IStock_scrapService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qty_scrapService stockWarnInsufficientQtyScrapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_productionService mrpProductionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workorderService mrpWorkorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_pickingService stockPickingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_production_lotService stockProductionLotService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quant_packageService stockQuantPackageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.scrap" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_scrap et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_scrapService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_scrap> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_scrap et) {
        Stock_scrap old = new Stock_scrap() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_scrapService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_scrapService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_scrap> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        stockWarnInsufficientQtyScrapService.resetByScrapId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        stockWarnInsufficientQtyScrapService.resetByScrapId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_scrap get(Long key) {
        Stock_scrap et = getById(key);
        if(et==null){
            et=new Stock_scrap();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_scrap getDraft(Stock_scrap et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_scrap et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_scrap et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_scrap et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_scrap> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_scrap> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_scrap> selectByProductionId(Long id) {
        return baseMapper.selectByProductionId(id);
    }
    @Override
    public void resetByProductionId(Long id) {
        this.update(new UpdateWrapper<Stock_scrap>().set("production_id",null).eq("production_id",id));
    }

    @Override
    public void resetByProductionId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_scrap>().set("production_id",null).in("production_id",ids));
    }

    @Override
    public void removeByProductionId(Long id) {
        this.remove(new QueryWrapper<Stock_scrap>().eq("production_id",id));
    }

	@Override
    public List<Stock_scrap> selectByWorkorderId(Long id) {
        return baseMapper.selectByWorkorderId(id);
    }
    @Override
    public void resetByWorkorderId(Long id) {
        this.update(new UpdateWrapper<Stock_scrap>().set("workorder_id",null).eq("workorder_id",id));
    }

    @Override
    public void resetByWorkorderId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_scrap>().set("workorder_id",null).in("workorder_id",ids));
    }

    @Override
    public void removeByWorkorderId(Long id) {
        this.remove(new QueryWrapper<Stock_scrap>().eq("workorder_id",id));
    }

	@Override
    public List<Stock_scrap> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Stock_scrap>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_scrap>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Stock_scrap>().eq("product_id",id));
    }

	@Override
    public List<Stock_scrap> selectByOwnerId(Long id) {
        return baseMapper.selectByOwnerId(id);
    }
    @Override
    public void resetByOwnerId(Long id) {
        this.update(new UpdateWrapper<Stock_scrap>().set("owner_id",null).eq("owner_id",id));
    }

    @Override
    public void resetByOwnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_scrap>().set("owner_id",null).in("owner_id",ids));
    }

    @Override
    public void removeByOwnerId(Long id) {
        this.remove(new QueryWrapper<Stock_scrap>().eq("owner_id",id));
    }

	@Override
    public List<Stock_scrap> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_scrap>().eq("create_uid",id));
    }

	@Override
    public List<Stock_scrap> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_scrap>().eq("write_uid",id));
    }

	@Override
    public List<Stock_scrap> selectByLocationId(Long id) {
        return baseMapper.selectByLocationId(id);
    }
    @Override
    public void resetByLocationId(Long id) {
        this.update(new UpdateWrapper<Stock_scrap>().set("location_id",null).eq("location_id",id));
    }

    @Override
    public void resetByLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_scrap>().set("location_id",null).in("location_id",ids));
    }

    @Override
    public void removeByLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_scrap>().eq("location_id",id));
    }

	@Override
    public List<Stock_scrap> selectByScrapLocationId(Long id) {
        return baseMapper.selectByScrapLocationId(id);
    }
    @Override
    public void resetByScrapLocationId(Long id) {
        this.update(new UpdateWrapper<Stock_scrap>().set("scrap_location_id",null).eq("scrap_location_id",id));
    }

    @Override
    public void resetByScrapLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_scrap>().set("scrap_location_id",null).in("scrap_location_id",ids));
    }

    @Override
    public void removeByScrapLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_scrap>().eq("scrap_location_id",id));
    }

	@Override
    public List<Stock_scrap> selectByMoveId(Long id) {
        return baseMapper.selectByMoveId(id);
    }
    @Override
    public void resetByMoveId(Long id) {
        this.update(new UpdateWrapper<Stock_scrap>().set("move_id",null).eq("move_id",id));
    }

    @Override
    public void resetByMoveId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_scrap>().set("move_id",null).in("move_id",ids));
    }

    @Override
    public void removeByMoveId(Long id) {
        this.remove(new QueryWrapper<Stock_scrap>().eq("move_id",id));
    }

	@Override
    public List<Stock_scrap> selectByPickingId(Long id) {
        return baseMapper.selectByPickingId(id);
    }
    @Override
    public void resetByPickingId(Long id) {
        this.update(new UpdateWrapper<Stock_scrap>().set("picking_id",null).eq("picking_id",id));
    }

    @Override
    public void resetByPickingId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_scrap>().set("picking_id",null).in("picking_id",ids));
    }

    @Override
    public void removeByPickingId(Long id) {
        this.remove(new QueryWrapper<Stock_scrap>().eq("picking_id",id));
    }

	@Override
    public List<Stock_scrap> selectByLotId(Long id) {
        return baseMapper.selectByLotId(id);
    }
    @Override
    public void resetByLotId(Long id) {
        this.update(new UpdateWrapper<Stock_scrap>().set("lot_id",null).eq("lot_id",id));
    }

    @Override
    public void resetByLotId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_scrap>().set("lot_id",null).in("lot_id",ids));
    }

    @Override
    public void removeByLotId(Long id) {
        this.remove(new QueryWrapper<Stock_scrap>().eq("lot_id",id));
    }

	@Override
    public List<Stock_scrap> selectByPackageId(Long id) {
        return baseMapper.selectByPackageId(id);
    }
    @Override
    public void resetByPackageId(Long id) {
        this.update(new UpdateWrapper<Stock_scrap>().set("package_id",null).eq("package_id",id));
    }

    @Override
    public void resetByPackageId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_scrap>().set("package_id",null).in("package_id",ids));
    }

    @Override
    public void removeByPackageId(Long id) {
        this.remove(new QueryWrapper<Stock_scrap>().eq("package_id",id));
    }

	@Override
    public List<Stock_scrap> selectByProductUomId(Long id) {
        return baseMapper.selectByProductUomId(id);
    }
    @Override
    public void resetByProductUomId(Long id) {
        this.update(new UpdateWrapper<Stock_scrap>().set("product_uom_id",null).eq("product_uom_id",id));
    }

    @Override
    public void resetByProductUomId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_scrap>().set("product_uom_id",null).in("product_uom_id",ids));
    }

    @Override
    public void removeByProductUomId(Long id) {
        this.remove(new QueryWrapper<Stock_scrap>().eq("product_uom_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_scrap> searchDefault(Stock_scrapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_scrap> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_scrap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_scrap et){
        //实体关系[DER1N_STOCK_SCRAP__MRP_PRODUCTION__PRODUCTION_ID]
        if(!ObjectUtils.isEmpty(et.getProductionId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production odooProduction=et.getOdooProduction();
            if(ObjectUtils.isEmpty(odooProduction)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production majorEntity=mrpProductionService.get(et.getProductionId());
                et.setOdooProduction(majorEntity);
                odooProduction=majorEntity;
            }
            et.setProductionIdText(odooProduction.getName());
        }
        //实体关系[DER1N_STOCK_SCRAP__MRP_WORKORDER__WORKORDER_ID]
        if(!ObjectUtils.isEmpty(et.getWorkorderId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder odooWorkorder=et.getOdooWorkorder();
            if(ObjectUtils.isEmpty(odooWorkorder)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder majorEntity=mrpWorkorderService.get(et.getWorkorderId());
                et.setOdooWorkorder(majorEntity);
                odooWorkorder=majorEntity;
            }
            et.setWorkorderIdText(odooWorkorder.getName());
        }
        //实体关系[DER1N_STOCK_SCRAP__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setTracking(odooProduct.getTracking());
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_STOCK_SCRAP__RES_PARTNER__OWNER_ID]
        if(!ObjectUtils.isEmpty(et.getOwnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooOwner=et.getOdooOwner();
            if(ObjectUtils.isEmpty(odooOwner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getOwnerId());
                et.setOdooOwner(majorEntity);
                odooOwner=majorEntity;
            }
            et.setOwnerIdText(odooOwner.getName());
        }
        //实体关系[DER1N_STOCK_SCRAP__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_SCRAP__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_SCRAP__STOCK_LOCATION__LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation=et.getOdooLocation();
            if(ObjectUtils.isEmpty(odooLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationId());
                et.setOdooLocation(majorEntity);
                odooLocation=majorEntity;
            }
            et.setLocationIdText(odooLocation.getName());
        }
        //实体关系[DER1N_STOCK_SCRAP__STOCK_LOCATION__SCRAP_LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getScrapLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooScrapLocation=et.getOdooScrapLocation();
            if(ObjectUtils.isEmpty(odooScrapLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getScrapLocationId());
                et.setOdooScrapLocation(majorEntity);
                odooScrapLocation=majorEntity;
            }
            et.setScrapLocationIdText(odooScrapLocation.getName());
        }
        //实体关系[DER1N_STOCK_SCRAP__STOCK_MOVE__MOVE_ID]
        if(!ObjectUtils.isEmpty(et.getMoveId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move odooMove=et.getOdooMove();
            if(ObjectUtils.isEmpty(odooMove)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move majorEntity=stockMoveService.get(et.getMoveId());
                et.setOdooMove(majorEntity);
                odooMove=majorEntity;
            }
            et.setMoveIdText(odooMove.getName());
        }
        //实体关系[DER1N_STOCK_SCRAP__STOCK_PICKING__PICKING_ID]
        if(!ObjectUtils.isEmpty(et.getPickingId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking odooPicking=et.getOdooPicking();
            if(ObjectUtils.isEmpty(odooPicking)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking majorEntity=stockPickingService.get(et.getPickingId());
                et.setOdooPicking(majorEntity);
                odooPicking=majorEntity;
            }
            et.setPickingIdText(odooPicking.getName());
        }
        //实体关系[DER1N_STOCK_SCRAP__STOCK_PRODUCTION_LOT__LOT_ID]
        if(!ObjectUtils.isEmpty(et.getLotId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot odooLot=et.getOdooLot();
            if(ObjectUtils.isEmpty(odooLot)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot majorEntity=stockProductionLotService.get(et.getLotId());
                et.setOdooLot(majorEntity);
                odooLot=majorEntity;
            }
            et.setLotIdText(odooLot.getName());
        }
        //实体关系[DER1N_STOCK_SCRAP__STOCK_QUANT_PACKAGE__PACKAGE_ID]
        if(!ObjectUtils.isEmpty(et.getPackageId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package odooPackage=et.getOdooPackage();
            if(ObjectUtils.isEmpty(odooPackage)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package majorEntity=stockQuantPackageService.get(et.getPackageId());
                et.setOdooPackage(majorEntity);
                odooPackage=majorEntity;
            }
            et.setPackageIdText(odooPackage.getName());
        }
        //实体关系[DER1N_STOCK_SCRAP__UOM_UOM__PRODUCT_UOM_ID]
        if(!ObjectUtils.isEmpty(et.getProductUomId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUomId());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomIdText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_scrap> getStockScrapByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_scrap> getStockScrapByEntities(List<Stock_scrap> entities) {
        List ids =new ArrayList();
        for(Stock_scrap entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



