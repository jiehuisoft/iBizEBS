package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_product_produce_line;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_product_produce_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_product_produce_line] 服务对象接口
 */
@Component
public class mrp_product_produce_lineFallback implements mrp_product_produce_lineFeignClient{



    public Mrp_product_produce_line update(Long id, Mrp_product_produce_line mrp_product_produce_line){
            return null;
     }
    public Boolean updateBatch(List<Mrp_product_produce_line> mrp_product_produce_lines){
            return false;
     }


    public Page<Mrp_product_produce_line> search(Mrp_product_produce_lineSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Mrp_product_produce_line get(Long id){
            return null;
     }



    public Mrp_product_produce_line create(Mrp_product_produce_line mrp_product_produce_line){
            return null;
     }
    public Boolean createBatch(List<Mrp_product_produce_line> mrp_product_produce_lines){
            return false;
     }

    public Page<Mrp_product_produce_line> select(){
            return null;
     }

    public Mrp_product_produce_line getDraft(){
            return null;
    }



    public Boolean checkKey(Mrp_product_produce_line mrp_product_produce_line){
            return false;
     }


    public Boolean save(Mrp_product_produce_line mrp_product_produce_line){
            return false;
     }
    public Boolean saveBatch(List<Mrp_product_produce_line> mrp_product_produce_lines){
            return false;
     }

    public Page<Mrp_product_produce_line> searchDefault(Mrp_product_produce_lineSearchContext context){
            return null;
     }


}
