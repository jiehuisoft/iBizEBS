package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_compose_message;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_compose_messageSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_compose_message] 服务对象接口
 */
public interface IMail_compose_messageService extends IService<Mail_compose_message>{

    boolean create(Mail_compose_message et) ;
    void createBatch(List<Mail_compose_message> list) ;
    boolean update(Mail_compose_message et) ;
    void updateBatch(List<Mail_compose_message> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_compose_message get(Long key) ;
    Mail_compose_message getDraft(Mail_compose_message et) ;
    boolean checkKey(Mail_compose_message et) ;
    boolean save(Mail_compose_message et) ;
    void saveBatch(List<Mail_compose_message> list) ;
    Page<Mail_compose_message> searchDefault(Mail_compose_messageSearchContext context) ;
    List<Mail_compose_message> selectByMailActivityTypeId(Long id);
    void resetByMailActivityTypeId(Long id);
    void resetByMailActivityTypeId(Collection<Long> ids);
    void removeByMailActivityTypeId(Long id);
    List<Mail_compose_message> selectByMassMailingCampaignId(Long id);
    void resetByMassMailingCampaignId(Long id);
    void resetByMassMailingCampaignId(Collection<Long> ids);
    void removeByMassMailingCampaignId(Long id);
    List<Mail_compose_message> selectByMassMailingId(Long id);
    void removeByMassMailingId(Collection<Long> ids);
    void removeByMassMailingId(Long id);
    List<Mail_compose_message> selectBySubtypeId(Long id);
    void resetBySubtypeId(Long id);
    void resetBySubtypeId(Collection<Long> ids);
    void removeBySubtypeId(Long id);
    List<Mail_compose_message> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Mail_compose_message> selectByTemplateId(Long id);
    void resetByTemplateId(Long id);
    void resetByTemplateId(Collection<Long> ids);
    void removeByTemplateId(Long id);
    List<Mail_compose_message> selectByAuthorId(Long id);
    void resetByAuthorId(Long id);
    void resetByAuthorId(Collection<Long> ids);
    void removeByAuthorId(Long id);
    List<Mail_compose_message> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_compose_message> selectByModeratorId(Long id);
    void resetByModeratorId(Long id);
    void resetByModeratorId(Collection<Long> ids);
    void removeByModeratorId(Long id);
    List<Mail_compose_message> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_compose_message> getMailComposeMessageByIds(List<Long> ids) ;
    List<Mail_compose_message> getMailComposeMessageByEntities(List<Mail_compose_message> entities) ;
}


