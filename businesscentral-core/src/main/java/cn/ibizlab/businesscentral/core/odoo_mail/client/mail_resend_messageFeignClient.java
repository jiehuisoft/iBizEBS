package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_resend_message;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_resend_messageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_resend_message] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-resend-message", fallback = mail_resend_messageFallback.class)
public interface mail_resend_messageFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_messages/{id}")
    Mail_resend_message update(@PathVariable("id") Long id,@RequestBody Mail_resend_message mail_resend_message);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_messages/batch")
    Boolean updateBatch(@RequestBody List<Mail_resend_message> mail_resend_messages);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_messages/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_messages/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_resend_messages/{id}")
    Mail_resend_message get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages/search")
    Page<Mail_resend_message> search(@RequestBody Mail_resend_messageSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages")
    Mail_resend_message create(@RequestBody Mail_resend_message mail_resend_message);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages/batch")
    Boolean createBatch(@RequestBody List<Mail_resend_message> mail_resend_messages);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_resend_messages/select")
    Page<Mail_resend_message> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_resend_messages/getdraft")
    Mail_resend_message getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages/checkkey")
    Boolean checkKey(@RequestBody Mail_resend_message mail_resend_message);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages/save")
    Boolean save(@RequestBody Mail_resend_message mail_resend_message);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_resend_message> mail_resend_messages);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages/searchdefault")
    Page<Mail_resend_message> searchDefault(@RequestBody Mail_resend_messageSearchContext context);


}
