package cn.ibizlab.businesscentral.core.odoo_web_editor.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test;
/**
 * 关系型数据实体[Web_editor_converter_test] 查询条件对象
 */
@Slf4j
@Data
public class Web_editor_converter_testSearchContext extends QueryWrapperContext<Web_editor_converter_test> {

	private String n_selection_str_eq;//[Lorsqu'un pancake prend l'avion à destination de Toronto et]
	public void setN_selection_str_eq(String n_selection_str_eq) {
        this.n_selection_str_eq = n_selection_str_eq;
        if(!ObjectUtils.isEmpty(this.n_selection_str_eq)){
            this.getSearchCond().eq("selection_str", n_selection_str_eq);
        }
    }
	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_selection_eq;//[Selection]
	public void setN_selection_eq(String n_selection_eq) {
        this.n_selection_eq = n_selection_eq;
        if(!ObjectUtils.isEmpty(this.n_selection_eq)){
            this.getSearchCond().eq("selection", n_selection_eq);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_many2one_text_eq;//[Many2One]
	public void setN_many2one_text_eq(String n_many2one_text_eq) {
        this.n_many2one_text_eq = n_many2one_text_eq;
        if(!ObjectUtils.isEmpty(this.n_many2one_text_eq)){
            this.getSearchCond().eq("many2one_text", n_many2one_text_eq);
        }
    }
	private String n_many2one_text_like;//[Many2One]
	public void setN_many2one_text_like(String n_many2one_text_like) {
        this.n_many2one_text_like = n_many2one_text_like;
        if(!ObjectUtils.isEmpty(this.n_many2one_text_like)){
            this.getSearchCond().like("many2one_text", n_many2one_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_many2one_eq;//[Many2One]
	public void setN_many2one_eq(Long n_many2one_eq) {
        this.n_many2one_eq = n_many2one_eq;
        if(!ObjectUtils.isEmpty(this.n_many2one_eq)){
            this.getSearchCond().eq("many2one", n_many2one_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



