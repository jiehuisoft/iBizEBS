package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_send;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_sendSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_sendService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_invoice_sendMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[发送会计发票] 服务对象接口实现
 */
@Slf4j
@Service("Account_invoice_sendServiceImpl")
public class Account_invoice_sendServiceImpl extends EBSServiceImpl<Account_invoice_sendMapper, Account_invoice_send> implements IAccount_invoice_sendService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_compose_messageService mailComposeMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_templateService mailTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.invoice.send" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_invoice_send et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoice_sendService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_invoice_send> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_invoice_send et) {
        Account_invoice_send old = new Account_invoice_send() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoice_sendService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoice_sendService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_invoice_send> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_invoice_send get(Long key) {
        Account_invoice_send et = getById(key);
        if(et==null){
            et=new Account_invoice_send();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_invoice_send getDraft(Account_invoice_send et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_invoice_send et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_invoice_send et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_invoice_send et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_invoice_send> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_invoice_send> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_invoice_send> selectByComposerId(Long id) {
        return baseMapper.selectByComposerId(id);
    }
    @Override
    public void removeByComposerId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Account_invoice_send>().in("composer_id",ids));
    }

    @Override
    public void removeByComposerId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_send>().eq("composer_id",id));
    }

	@Override
    public List<Account_invoice_send> selectByTemplateId(Long id) {
        return baseMapper.selectByTemplateId(id);
    }
    @Override
    public void resetByTemplateId(Long id) {
        this.update(new UpdateWrapper<Account_invoice_send>().set("template_id",null).eq("template_id",id));
    }

    @Override
    public void resetByTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice_send>().set("template_id",null).in("template_id",ids));
    }

    @Override
    public void removeByTemplateId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_send>().eq("template_id",id));
    }

	@Override
    public List<Account_invoice_send> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_invoice_send>().eq("create_uid",id));
    }

	@Override
    public List<Account_invoice_send> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_invoice_send>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_invoice_send> searchDefault(Account_invoice_sendSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_invoice_send> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_invoice_send>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_invoice_send et){
        //实体关系[DER1N_ACCOUNT_INVOICE_SEND__MAIL_COMPOSE_MESSAGE__COMPOSER_ID]
        if(!ObjectUtils.isEmpty(et.getComposerId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_compose_message odooComposer=et.getOdooComposer();
            if(ObjectUtils.isEmpty(odooComposer)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_compose_message majorEntity=mailComposeMessageService.get(et.getComposerId());
                et.setOdooComposer(majorEntity);
                odooComposer=majorEntity;
            }
            et.setParentId(odooComposer.getParentId());
            et.setUseActiveDomain(odooComposer.getUseActiveDomain());
            et.setModerationStatus(odooComposer.getModerationStatus());
            et.setModeratorId(odooComposer.getModeratorId());
            et.setHasError(odooComposer.getHasError());
            et.setActiveDomain(odooComposer.getActiveDomain());
            et.setDescription(odooComposer.getDescription());
            et.setNoAutoThread(odooComposer.getNoAutoThread());
            et.setCompositionMode(odooComposer.getCompositionMode());
            et.setAddSign(odooComposer.getAddSign());
            et.setEmailFrom(odooComposer.getEmailFrom());
            et.setDate(odooComposer.getDate());
            et.setMailActivityTypeId(odooComposer.getMailActivityTypeId());
            et.setNeedaction(odooComposer.getNeedaction());
            et.setSubject(odooComposer.getSubject());
            et.setRecordName(odooComposer.getRecordName());
            et.setAuthorAvatar(odooComposer.getAuthorAvatar());
            et.setRatingValue(odooComposer.getRatingValue());
            et.setAutoDelete(odooComposer.getAutoDelete());
            et.setAutoDeleteMessage(odooComposer.getAutoDeleteMessage());
            et.setBody(odooComposer.getBody());
            et.setMailServerId(odooComposer.getMailServerId());
            et.setMessageId(odooComposer.getMessageId());
            et.setModel(odooComposer.getModel());
            et.setReplyTo(odooComposer.getReplyTo());
            et.setMessageType(odooComposer.getMessageType());
            et.setStarred(odooComposer.getStarred());
            et.setIsLog(odooComposer.getIsLog());
            et.setNotify(odooComposer.getNotify());
            et.setMassMailingCampaignId(odooComposer.getMassMailingCampaignId());
            et.setNeedModeration(odooComposer.getNeedModeration());
            et.setSubtypeId(odooComposer.getSubtypeId());
            et.setAuthorId(odooComposer.getAuthorId());
            et.setWebsitePublished(odooComposer.getWebsitePublished());
            et.setMassMailingId(odooComposer.getMassMailingId());
            et.setResId(odooComposer.getResId());
            et.setMassMailingName(odooComposer.getMassMailingName());
            et.setLayout(odooComposer.getLayout());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_SEND__MAIL_TEMPLATE__TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooTemplate=et.getOdooTemplate();
            if(ObjectUtils.isEmpty(odooTemplate)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template majorEntity=mailTemplateService.get(et.getTemplateId());
                et.setOdooTemplate(majorEntity);
                odooTemplate=majorEntity;
            }
            et.setTemplateIdText(odooTemplate.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_SEND__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_SEND__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_invoice_send> getAccountInvoiceSendByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_invoice_send> getAccountInvoiceSendByEntities(List<Account_invoice_send> entities) {
        List ids =new ArrayList();
        for(Account_invoice_send entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



