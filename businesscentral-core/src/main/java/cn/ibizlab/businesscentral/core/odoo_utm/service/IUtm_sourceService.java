package cn.ibizlab.businesscentral.core.odoo_utm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_sourceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Utm_source] 服务对象接口
 */
public interface IUtm_sourceService extends IService<Utm_source>{

    boolean create(Utm_source et) ;
    void createBatch(List<Utm_source> list) ;
    boolean update(Utm_source et) ;
    void updateBatch(List<Utm_source> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Utm_source get(Long key) ;
    Utm_source getDraft(Utm_source et) ;
    boolean checkKey(Utm_source et) ;
    boolean save(Utm_source et) ;
    void saveBatch(List<Utm_source> list) ;
    Page<Utm_source> searchDefault(Utm_sourceSearchContext context) ;
    List<Utm_source> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Utm_source> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Utm_source> getUtmSourceByIds(List<Long> ids) ;
    List<Utm_source> getUtmSourceByEntities(List<Utm_source> entities) ;
}


