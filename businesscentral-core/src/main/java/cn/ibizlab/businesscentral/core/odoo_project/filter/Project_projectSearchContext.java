package cn.ibizlab.businesscentral.core.odoo_project.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_project;
/**
 * 关系型数据实体[Project_project] 查询条件对象
 */
@Slf4j
@Data
public class Project_projectSearchContext extends QueryWrapperContext<Project_project> {

	private String n_privacy_visibility_eq;//[隐私]
	public void setN_privacy_visibility_eq(String n_privacy_visibility_eq) {
        this.n_privacy_visibility_eq = n_privacy_visibility_eq;
        if(!ObjectUtils.isEmpty(this.n_privacy_visibility_eq)){
            this.getSearchCond().eq("privacy_visibility", n_privacy_visibility_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_rating_status_period_eq;//[点评频率]
	public void setN_rating_status_period_eq(String n_rating_status_period_eq) {
        this.n_rating_status_period_eq = n_rating_status_period_eq;
        if(!ObjectUtils.isEmpty(this.n_rating_status_period_eq)){
            this.getSearchCond().eq("rating_status_period", n_rating_status_period_eq);
        }
    }
	private String n_rating_status_eq;//[客户点评]
	public void setN_rating_status_eq(String n_rating_status_eq) {
        this.n_rating_status_eq = n_rating_status_eq;
        if(!ObjectUtils.isEmpty(this.n_rating_status_eq)){
            this.getSearchCond().eq("rating_status", n_rating_status_eq);
        }
    }
	private String n_partner_id_text_eq;//[客户]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[客户]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_resource_calendar_id_text_eq;//[工作时间]
	public void setN_resource_calendar_id_text_eq(String n_resource_calendar_id_text_eq) {
        this.n_resource_calendar_id_text_eq = n_resource_calendar_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_calendar_id_text_eq)){
            this.getSearchCond().eq("resource_calendar_id_text", n_resource_calendar_id_text_eq);
        }
    }
	private String n_resource_calendar_id_text_like;//[工作时间]
	public void setN_resource_calendar_id_text_like(String n_resource_calendar_id_text_like) {
        this.n_resource_calendar_id_text_like = n_resource_calendar_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_resource_calendar_id_text_like)){
            this.getSearchCond().like("resource_calendar_id_text", n_resource_calendar_id_text_like);
        }
    }
	private String n_subtask_project_id_text_eq;//[子任务项目]
	public void setN_subtask_project_id_text_eq(String n_subtask_project_id_text_eq) {
        this.n_subtask_project_id_text_eq = n_subtask_project_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_subtask_project_id_text_eq)){
            this.getSearchCond().eq("subtask_project_id_text", n_subtask_project_id_text_eq);
        }
    }
	private String n_subtask_project_id_text_like;//[子任务项目]
	public void setN_subtask_project_id_text_like(String n_subtask_project_id_text_like) {
        this.n_subtask_project_id_text_like = n_subtask_project_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_subtask_project_id_text_like)){
            this.getSearchCond().like("subtask_project_id_text", n_subtask_project_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[项目管理员]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[项目管理员]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_user_id_eq;//[项目管理员]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_partner_id_eq;//[客户]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_subtask_project_id_eq;//[子任务项目]
	public void setN_subtask_project_id_eq(Long n_subtask_project_id_eq) {
        this.n_subtask_project_id_eq = n_subtask_project_id_eq;
        if(!ObjectUtils.isEmpty(this.n_subtask_project_id_eq)){
            this.getSearchCond().eq("subtask_project_id", n_subtask_project_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_resource_calendar_id_eq;//[工作时间]
	public void setN_resource_calendar_id_eq(Long n_resource_calendar_id_eq) {
        this.n_resource_calendar_id_eq = n_resource_calendar_id_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_calendar_id_eq)){
            this.getSearchCond().eq("resource_calendar_id", n_resource_calendar_id_eq);
        }
    }
	private Long n_alias_id_eq;//[别名]
	public void setN_alias_id_eq(Long n_alias_id_eq) {
        this.n_alias_id_eq = n_alias_id_eq;
        if(!ObjectUtils.isEmpty(this.n_alias_id_eq)){
            this.getSearchCond().eq("alias_id", n_alias_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



