package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_holidays_summary_dept;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_holidays_summary_deptSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_holidays_summary_dept] 服务对象接口
 */
public interface IHr_holidays_summary_deptService extends IService<Hr_holidays_summary_dept>{

    boolean create(Hr_holidays_summary_dept et) ;
    void createBatch(List<Hr_holidays_summary_dept> list) ;
    boolean update(Hr_holidays_summary_dept et) ;
    void updateBatch(List<Hr_holidays_summary_dept> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_holidays_summary_dept get(Long key) ;
    Hr_holidays_summary_dept getDraft(Hr_holidays_summary_dept et) ;
    boolean checkKey(Hr_holidays_summary_dept et) ;
    boolean save(Hr_holidays_summary_dept et) ;
    void saveBatch(List<Hr_holidays_summary_dept> list) ;
    Page<Hr_holidays_summary_dept> searchDefault(Hr_holidays_summary_deptSearchContext context) ;
    List<Hr_holidays_summary_dept> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_holidays_summary_dept> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_holidays_summary_dept> getHrHolidaysSummaryDeptByIds(List<Long> ids) ;
    List<Hr_holidays_summary_dept> getHrHolidaysSummaryDeptByEntities(List<Hr_holidays_summary_dept> entities) ;
}


