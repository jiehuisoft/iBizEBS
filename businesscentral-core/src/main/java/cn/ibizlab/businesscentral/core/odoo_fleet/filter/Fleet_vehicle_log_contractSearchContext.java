package cn.ibizlab.businesscentral.core.odoo_fleet.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
/**
 * 关系型数据实体[Fleet_vehicle_log_contract] 查询条件对象
 */
@Slf4j
@Data
public class Fleet_vehicle_log_contractSearchContext extends QueryWrapperContext<Fleet_vehicle_log_contract> {

	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_cost_frequency_eq;//[经常成本频率]
	public void setN_cost_frequency_eq(String n_cost_frequency_eq) {
        this.n_cost_frequency_eq = n_cost_frequency_eq;
        if(!ObjectUtils.isEmpty(this.n_cost_frequency_eq)){
            this.getSearchCond().eq("cost_frequency", n_cost_frequency_eq);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_purchaser_id_text_eq;//[驾驶员]
	public void setN_purchaser_id_text_eq(String n_purchaser_id_text_eq) {
        this.n_purchaser_id_text_eq = n_purchaser_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_purchaser_id_text_eq)){
            this.getSearchCond().eq("purchaser_id_text", n_purchaser_id_text_eq);
        }
    }
	private String n_purchaser_id_text_like;//[驾驶员]
	public void setN_purchaser_id_text_like(String n_purchaser_id_text_like) {
        this.n_purchaser_id_text_like = n_purchaser_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_purchaser_id_text_like)){
            this.getSearchCond().like("purchaser_id_text", n_purchaser_id_text_like);
        }
    }
	private String n_insurer_id_text_eq;//[供应商]
	public void setN_insurer_id_text_eq(String n_insurer_id_text_eq) {
        this.n_insurer_id_text_eq = n_insurer_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_insurer_id_text_eq)){
            this.getSearchCond().eq("insurer_id_text", n_insurer_id_text_eq);
        }
    }
	private String n_insurer_id_text_like;//[供应商]
	public void setN_insurer_id_text_like(String n_insurer_id_text_like) {
        this.n_insurer_id_text_like = n_insurer_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_insurer_id_text_like)){
            this.getSearchCond().like("insurer_id_text", n_insurer_id_text_like);
        }
    }
	private String n_cost_id_text_eq;//[成本]
	public void setN_cost_id_text_eq(String n_cost_id_text_eq) {
        this.n_cost_id_text_eq = n_cost_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_cost_id_text_eq)){
            this.getSearchCond().eq("cost_id_text", n_cost_id_text_eq);
        }
    }
	private String n_cost_id_text_like;//[成本]
	public void setN_cost_id_text_like(String n_cost_id_text_like) {
        this.n_cost_id_text_like = n_cost_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_cost_id_text_like)){
            this.getSearchCond().like("cost_id_text", n_cost_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[负责]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[负责]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_cost_id_eq;//[成本]
	public void setN_cost_id_eq(Long n_cost_id_eq) {
        this.n_cost_id_eq = n_cost_id_eq;
        if(!ObjectUtils.isEmpty(this.n_cost_id_eq)){
            this.getSearchCond().eq("cost_id", n_cost_id_eq);
        }
    }
	private Long n_insurer_id_eq;//[供应商]
	public void setN_insurer_id_eq(Long n_insurer_id_eq) {
        this.n_insurer_id_eq = n_insurer_id_eq;
        if(!ObjectUtils.isEmpty(this.n_insurer_id_eq)){
            this.getSearchCond().eq("insurer_id", n_insurer_id_eq);
        }
    }
	private Long n_user_id_eq;//[负责]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_purchaser_id_eq;//[驾驶员]
	public void setN_purchaser_id_eq(Long n_purchaser_id_eq) {
        this.n_purchaser_id_eq = n_purchaser_id_eq;
        if(!ObjectUtils.isEmpty(this.n_purchaser_id_eq)){
            this.getSearchCond().eq("purchaser_id", n_purchaser_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



