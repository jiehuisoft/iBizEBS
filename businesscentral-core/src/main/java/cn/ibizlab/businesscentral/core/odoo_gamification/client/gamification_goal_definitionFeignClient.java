package cn.ibizlab.businesscentral.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal_definition;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_goal_definitionSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[gamification_goal_definition] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-gamification:odoo-gamification}", contextId = "gamification-goal-definition", fallback = gamification_goal_definitionFallback.class)
public interface gamification_goal_definitionFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_goal_definitions/{id}")
    Gamification_goal_definition update(@PathVariable("id") Long id,@RequestBody Gamification_goal_definition gamification_goal_definition);

    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_goal_definitions/batch")
    Boolean updateBatch(@RequestBody List<Gamification_goal_definition> gamification_goal_definitions);


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_definitions/{id}")
    Gamification_goal_definition get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions/search")
    Page<Gamification_goal_definition> search(@RequestBody Gamification_goal_definitionSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions")
    Gamification_goal_definition create(@RequestBody Gamification_goal_definition gamification_goal_definition);

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions/batch")
    Boolean createBatch(@RequestBody List<Gamification_goal_definition> gamification_goal_definitions);


    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goal_definitions/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goal_definitions/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_definitions/select")
    Page<Gamification_goal_definition> select();


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_definitions/getdraft")
    Gamification_goal_definition getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions/checkkey")
    Boolean checkKey(@RequestBody Gamification_goal_definition gamification_goal_definition);


    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions/save")
    Boolean save(@RequestBody Gamification_goal_definition gamification_goal_definition);

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions/savebatch")
    Boolean saveBatch(@RequestBody List<Gamification_goal_definition> gamification_goal_definitions);



    @RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions/searchdefault")
    Page<Gamification_goal_definition> searchDefault(@RequestBody Gamification_goal_definitionSearchContext context);


}
