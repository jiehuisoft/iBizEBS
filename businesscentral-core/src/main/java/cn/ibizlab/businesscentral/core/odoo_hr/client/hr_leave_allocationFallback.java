package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_allocation;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leave_allocationSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_leave_allocation] 服务对象接口
 */
@Component
public class hr_leave_allocationFallback implements hr_leave_allocationFeignClient{


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Hr_leave_allocation get(Long id){
            return null;
     }



    public Hr_leave_allocation update(Long id, Hr_leave_allocation hr_leave_allocation){
            return null;
     }
    public Boolean updateBatch(List<Hr_leave_allocation> hr_leave_allocations){
            return false;
     }


    public Hr_leave_allocation create(Hr_leave_allocation hr_leave_allocation){
            return null;
     }
    public Boolean createBatch(List<Hr_leave_allocation> hr_leave_allocations){
            return false;
     }


    public Page<Hr_leave_allocation> search(Hr_leave_allocationSearchContext context){
            return null;
     }


    public Page<Hr_leave_allocation> select(){
            return null;
     }

    public Hr_leave_allocation getDraft(){
            return null;
    }



    public Boolean checkKey(Hr_leave_allocation hr_leave_allocation){
            return false;
     }


    public Boolean save(Hr_leave_allocation hr_leave_allocation){
            return false;
     }
    public Boolean saveBatch(List<Hr_leave_allocation> hr_leave_allocations){
            return false;
     }

    public Page<Hr_leave_allocation> searchDefault(Hr_leave_allocationSearchContext context){
            return null;
     }


}
