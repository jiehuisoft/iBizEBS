package cn.ibizlab.businesscentral.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_cashmove;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_cashmoveSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Lunch_cashmove] 服务对象接口
 */
public interface ILunch_cashmoveService extends IService<Lunch_cashmove>{

    boolean create(Lunch_cashmove et) ;
    void createBatch(List<Lunch_cashmove> list) ;
    boolean update(Lunch_cashmove et) ;
    void updateBatch(List<Lunch_cashmove> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Lunch_cashmove get(Long key) ;
    Lunch_cashmove getDraft(Lunch_cashmove et) ;
    boolean checkKey(Lunch_cashmove et) ;
    boolean save(Lunch_cashmove et) ;
    void saveBatch(List<Lunch_cashmove> list) ;
    Page<Lunch_cashmove> searchDefault(Lunch_cashmoveSearchContext context) ;
    List<Lunch_cashmove> selectByOrderId(Long id);
    void removeByOrderId(Collection<Long> ids);
    void removeByOrderId(Long id);
    List<Lunch_cashmove> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Lunch_cashmove> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Lunch_cashmove> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Lunch_cashmove> getLunchCashmoveByIds(List<Long> ids) ;
    List<Lunch_cashmove> getLunchCashmoveByEntities(List<Lunch_cashmove> entities) ;
}


