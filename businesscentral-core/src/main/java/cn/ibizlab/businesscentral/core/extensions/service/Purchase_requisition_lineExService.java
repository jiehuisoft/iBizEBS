package cn.ibizlab.businesscentral.core.extensions.service;

import cn.ibizlab.businesscentral.core.odoo_purchase.service.impl.Purchase_requisition_lineServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition_line;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[采购申请行] 自定义服务对象
 */
@Slf4j
@Primary
@Service("Purchase_requisition_lineExService")
public class Purchase_requisition_lineExService extends Purchase_requisition_lineServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[Calc_price]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Purchase_requisition_line calc_price(Purchase_requisition_line et) {
        return super.calc_price(et);
    }
}

