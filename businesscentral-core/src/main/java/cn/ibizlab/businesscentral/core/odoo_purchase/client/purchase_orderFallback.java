package cn.ibizlab.businesscentral.core.odoo_purchase.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_orderSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[purchase_order] 服务对象接口
 */
@Component
public class purchase_orderFallback implements purchase_orderFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Purchase_order> search(Purchase_orderSearchContext context){
            return null;
     }


    public Purchase_order update(Long id, Purchase_order purchase_order){
            return null;
     }
    public Boolean updateBatch(List<Purchase_order> purchase_orders){
            return false;
     }





    public Purchase_order get(Long id){
            return null;
     }


    public Purchase_order create(Purchase_order purchase_order){
            return null;
     }
    public Boolean createBatch(List<Purchase_order> purchase_orders){
            return false;
     }

    public Page<Purchase_order> select(){
            return null;
     }

    public Purchase_order getDraft(){
            return null;
    }



    public Purchase_order button_approve( Long id, Purchase_order purchase_order){
            return null;
     }

    public Purchase_order button_cancel( Long id, Purchase_order purchase_order){
            return null;
     }

    public Purchase_order button_confirm( Long id, Purchase_order purchase_order){
            return null;
     }

    public Purchase_order button_done( Long id, Purchase_order purchase_order){
            return null;
     }

    public Purchase_order button_unlock( Long id, Purchase_order purchase_order){
            return null;
     }

    public Purchase_order calc_amount( Long id, Purchase_order purchase_order){
            return null;
     }

    public Boolean checkKey(Purchase_order purchase_order){
            return false;
     }


    public Purchase_order get_name( Long id, Purchase_order purchase_order){
            return null;
     }

    public Boolean save(Purchase_order purchase_order){
            return false;
     }
    public Boolean saveBatch(List<Purchase_order> purchase_orders){
            return false;
     }

    public Page<Purchase_order> searchDefault(Purchase_orderSearchContext context){
            return null;
     }


    public Page<Purchase_order> searchMaster(Purchase_orderSearchContext context){
            return null;
     }


    public Page<Purchase_order> searchOrder(Purchase_orderSearchContext context){
            return null;
     }


}
