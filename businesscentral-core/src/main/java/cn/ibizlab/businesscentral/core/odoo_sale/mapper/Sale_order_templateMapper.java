package cn.ibizlab.businesscentral.core.odoo_sale.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_templateSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Sale_order_templateMapper extends BaseMapper<Sale_order_template>{

    Page<Sale_order_template> searchDefault(IPage page, @Param("srf") Sale_order_templateSearchContext context, @Param("ew") Wrapper<Sale_order_template> wrapper) ;
    @Override
    Sale_order_template selectById(Serializable id);
    @Override
    int insert(Sale_order_template entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Sale_order_template entity);
    @Override
    int update(@Param(Constants.ENTITY) Sale_order_template entity, @Param("ew") Wrapper<Sale_order_template> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Sale_order_template> selectByMailTemplateId(@Param("id") Serializable id) ;

    List<Sale_order_template> selectByCreateUid(@Param("id") Serializable id) ;

    List<Sale_order_template> selectByWriteUid(@Param("id") Serializable id) ;


}
