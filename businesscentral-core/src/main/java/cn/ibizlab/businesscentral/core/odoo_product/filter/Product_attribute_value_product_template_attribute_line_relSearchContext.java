package cn.ibizlab.businesscentral.core.odoo_product.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_attribute_value_product_template_attribute_line_rel;
/**
 * 关系型数据实体[Product_attribute_value_product_template_attribute_line_rel] 查询条件对象
 */
@Slf4j
@Data
public class Product_attribute_value_product_template_attribute_line_relSearchContext extends QueryWrapperContext<Product_attribute_value_product_template_attribute_line_rel> {

	private String n_id_like;//[ID]
	public void setN_id_like(String n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private Long n_product_template_attribute_line_id_eq;//[ID]
	public void setN_product_template_attribute_line_id_eq(Long n_product_template_attribute_line_id_eq) {
        this.n_product_template_attribute_line_id_eq = n_product_template_attribute_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_template_attribute_line_id_eq)){
            this.getSearchCond().eq("product_template_attribute_line_id", n_product_template_attribute_line_id_eq);
        }
    }
	private Long n_product_attribute_value_id_eq;//[ID]
	public void setN_product_attribute_value_id_eq(Long n_product_attribute_value_id_eq) {
        this.n_product_attribute_value_id_eq = n_product_attribute_value_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_attribute_value_id_eq)){
            this.getSearchCond().eq("product_attribute_value_id", n_product_attribute_value_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



