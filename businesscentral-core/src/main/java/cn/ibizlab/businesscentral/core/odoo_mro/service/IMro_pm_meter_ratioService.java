package cn.ibizlab.businesscentral.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_ratio;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_meter_ratioSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mro_pm_meter_ratio] 服务对象接口
 */
public interface IMro_pm_meter_ratioService extends IService<Mro_pm_meter_ratio>{

    boolean create(Mro_pm_meter_ratio et) ;
    void createBatch(List<Mro_pm_meter_ratio> list) ;
    boolean update(Mro_pm_meter_ratio et) ;
    void updateBatch(List<Mro_pm_meter_ratio> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mro_pm_meter_ratio get(Long key) ;
    Mro_pm_meter_ratio getDraft(Mro_pm_meter_ratio et) ;
    boolean checkKey(Mro_pm_meter_ratio et) ;
    boolean save(Mro_pm_meter_ratio et) ;
    void saveBatch(List<Mro_pm_meter_ratio> list) ;
    Page<Mro_pm_meter_ratio> searchDefault(Mro_pm_meter_ratioSearchContext context) ;
    List<Mro_pm_meter_ratio> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mro_pm_meter_ratio> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mro_pm_meter_ratio> getMroPmMeterRatioByIds(List<Long> ids) ;
    List<Mro_pm_meter_ratio> getMroPmMeterRatioByEntities(List<Mro_pm_meter_ratio> entities) ;
}


