package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_source;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_recruitment_sourceSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_recruitment_source] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-hr:odoo-hr}", contextId = "hr-recruitment-source", fallback = hr_recruitment_sourceFallback.class)
public interface hr_recruitment_sourceFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_sources/{id}")
    Hr_recruitment_source get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources/search")
    Page<Hr_recruitment_source> search(@RequestBody Hr_recruitment_sourceSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_sources/{id}")
    Hr_recruitment_source update(@PathVariable("id") Long id,@RequestBody Hr_recruitment_source hr_recruitment_source);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_sources/batch")
    Boolean updateBatch(@RequestBody List<Hr_recruitment_source> hr_recruitment_sources);


    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_sources/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_sources/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources")
    Hr_recruitment_source create(@RequestBody Hr_recruitment_source hr_recruitment_source);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources/batch")
    Boolean createBatch(@RequestBody List<Hr_recruitment_source> hr_recruitment_sources);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_sources/select")
    Page<Hr_recruitment_source> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_sources/getdraft")
    Hr_recruitment_source getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources/checkkey")
    Boolean checkKey(@RequestBody Hr_recruitment_source hr_recruitment_source);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources/save")
    Boolean save(@RequestBody Hr_recruitment_source hr_recruitment_source);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources/savebatch")
    Boolean saveBatch(@RequestBody List<Hr_recruitment_source> hr_recruitment_sources);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources/searchdefault")
    Page<Hr_recruitment_source> searchDefault(@RequestBody Hr_recruitment_sourceSearchContext context);


}
