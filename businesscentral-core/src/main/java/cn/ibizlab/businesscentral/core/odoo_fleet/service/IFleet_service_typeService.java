package cn.ibizlab.businesscentral.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_service_type;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_service_typeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Fleet_service_type] 服务对象接口
 */
public interface IFleet_service_typeService extends IService<Fleet_service_type>{

    boolean create(Fleet_service_type et) ;
    void createBatch(List<Fleet_service_type> list) ;
    boolean update(Fleet_service_type et) ;
    void updateBatch(List<Fleet_service_type> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Fleet_service_type get(Long key) ;
    Fleet_service_type getDraft(Fleet_service_type et) ;
    boolean checkKey(Fleet_service_type et) ;
    boolean save(Fleet_service_type et) ;
    void saveBatch(List<Fleet_service_type> list) ;
    Page<Fleet_service_type> searchDefault(Fleet_service_typeSearchContext context) ;
    List<Fleet_service_type> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Fleet_service_type> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Fleet_service_type> getFleetServiceTypeByIds(List<Long> ids) ;
    List<Fleet_service_type> getFleetServiceTypeByEntities(List<Fleet_service_type> entities) ;
}


