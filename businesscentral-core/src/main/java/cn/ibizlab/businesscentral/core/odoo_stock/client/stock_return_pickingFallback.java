package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_return_picking;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_return_pickingSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_return_picking] 服务对象接口
 */
@Component
public class stock_return_pickingFallback implements stock_return_pickingFeignClient{


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Stock_return_picking> search(Stock_return_pickingSearchContext context){
            return null;
     }




    public Stock_return_picking create(Stock_return_picking stock_return_picking){
            return null;
     }
    public Boolean createBatch(List<Stock_return_picking> stock_return_pickings){
            return false;
     }

    public Stock_return_picking update(Long id, Stock_return_picking stock_return_picking){
            return null;
     }
    public Boolean updateBatch(List<Stock_return_picking> stock_return_pickings){
            return false;
     }


    public Stock_return_picking get(Long id){
            return null;
     }


    public Page<Stock_return_picking> select(){
            return null;
     }

    public Stock_return_picking getDraft(){
            return null;
    }



    public Boolean checkKey(Stock_return_picking stock_return_picking){
            return false;
     }


    public Boolean save(Stock_return_picking stock_return_picking){
            return false;
     }
    public Boolean saveBatch(List<Stock_return_picking> stock_return_pickings){
            return false;
     }

    public Page<Stock_return_picking> searchDefault(Stock_return_pickingSearchContext context){
            return null;
     }


}
