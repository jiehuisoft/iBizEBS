package cn.ibizlab.businesscentral.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_payment_acquirer_onboarding_wizardSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
public interface ISale_payment_acquirer_onboarding_wizardService extends IService<Sale_payment_acquirer_onboarding_wizard>{

    boolean create(Sale_payment_acquirer_onboarding_wizard et) ;
    void createBatch(List<Sale_payment_acquirer_onboarding_wizard> list) ;
    boolean update(Sale_payment_acquirer_onboarding_wizard et) ;
    void updateBatch(List<Sale_payment_acquirer_onboarding_wizard> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Sale_payment_acquirer_onboarding_wizard get(Long key) ;
    Sale_payment_acquirer_onboarding_wizard getDraft(Sale_payment_acquirer_onboarding_wizard et) ;
    boolean checkKey(Sale_payment_acquirer_onboarding_wizard et) ;
    boolean save(Sale_payment_acquirer_onboarding_wizard et) ;
    void saveBatch(List<Sale_payment_acquirer_onboarding_wizard> list) ;
    Page<Sale_payment_acquirer_onboarding_wizard> searchDefault(Sale_payment_acquirer_onboarding_wizardSearchContext context) ;
    List<Sale_payment_acquirer_onboarding_wizard> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Sale_payment_acquirer_onboarding_wizard> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Sale_payment_acquirer_onboarding_wizard> getSalePaymentAcquirerOnboardingWizardByIds(List<Long> ids) ;
    List<Sale_payment_acquirer_onboarding_wizard> getSalePaymentAcquirerOnboardingWizardByEntities(List<Sale_payment_acquirer_onboarding_wizard> entities) ;
}


