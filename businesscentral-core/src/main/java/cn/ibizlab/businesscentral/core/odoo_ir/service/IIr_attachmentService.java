package cn.ibizlab.businesscentral.core.odoo_ir.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_attachment;
import cn.ibizlab.businesscentral.core.odoo_ir.filter.Ir_attachmentSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Ir_attachment] 服务对象接口
 */
public interface IIr_attachmentService extends IService<Ir_attachment>{

    boolean create(Ir_attachment et) ;
    void createBatch(List<Ir_attachment> list) ;
    boolean update(Ir_attachment et) ;
    void updateBatch(List<Ir_attachment> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Ir_attachment get(Long key) ;
    Ir_attachment getDraft(Ir_attachment et) ;
    boolean checkKey(Ir_attachment et) ;
    boolean save(Ir_attachment et) ;
    void saveBatch(List<Ir_attachment> list) ;
    Page<Ir_attachment> searchDefault(Ir_attachmentSearchContext context) ;
    List<Ir_attachment> selectByCompanyId(Long id);
    void removeByCompanyId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


