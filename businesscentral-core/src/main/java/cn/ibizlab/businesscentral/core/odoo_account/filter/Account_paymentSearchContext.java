package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment;
/**
 * 关系型数据实体[Account_payment] 查询条件对象
 */
@Slf4j
@Data
public class Account_paymentSearchContext extends QueryWrapperContext<Account_payment> {

	private String n_payment_type_eq;//[付款类型]
	public void setN_payment_type_eq(String n_payment_type_eq) {
        this.n_payment_type_eq = n_payment_type_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_type_eq)){
            this.getSearchCond().eq("payment_type", n_payment_type_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_payment_difference_handling_eq;//[付款差异处理]
	public void setN_payment_difference_handling_eq(String n_payment_difference_handling_eq) {
        this.n_payment_difference_handling_eq = n_payment_difference_handling_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_difference_handling_eq)){
            this.getSearchCond().eq("payment_difference_handling", n_payment_difference_handling_eq);
        }
    }
	private String n_partner_type_eq;//[业务伙伴类型]
	public void setN_partner_type_eq(String n_partner_type_eq) {
        this.n_partner_type_eq = n_partner_type_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_type_eq)){
            this.getSearchCond().eq("partner_type", n_partner_type_eq);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_destination_journal_id_text_eq;//[转账到]
	public void setN_destination_journal_id_text_eq(String n_destination_journal_id_text_eq) {
        this.n_destination_journal_id_text_eq = n_destination_journal_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_destination_journal_id_text_eq)){
            this.getSearchCond().eq("destination_journal_id_text", n_destination_journal_id_text_eq);
        }
    }
	private String n_destination_journal_id_text_like;//[转账到]
	public void setN_destination_journal_id_text_like(String n_destination_journal_id_text_like) {
        this.n_destination_journal_id_text_like = n_destination_journal_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_destination_journal_id_text_like)){
            this.getSearchCond().like("destination_journal_id_text", n_destination_journal_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_journal_id_text_eq;//[付款日记账]
	public void setN_journal_id_text_eq(String n_journal_id_text_eq) {
        this.n_journal_id_text_eq = n_journal_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_eq)){
            this.getSearchCond().eq("journal_id_text", n_journal_id_text_eq);
        }
    }
	private String n_journal_id_text_like;//[付款日记账]
	public void setN_journal_id_text_like(String n_journal_id_text_like) {
        this.n_journal_id_text_like = n_journal_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_like)){
            this.getSearchCond().like("journal_id_text", n_journal_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_writeoff_account_id_text_eq;//[差异科目]
	public void setN_writeoff_account_id_text_eq(String n_writeoff_account_id_text_eq) {
        this.n_writeoff_account_id_text_eq = n_writeoff_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_writeoff_account_id_text_eq)){
            this.getSearchCond().eq("writeoff_account_id_text", n_writeoff_account_id_text_eq);
        }
    }
	private String n_writeoff_account_id_text_like;//[差异科目]
	public void setN_writeoff_account_id_text_like(String n_writeoff_account_id_text_like) {
        this.n_writeoff_account_id_text_like = n_writeoff_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_writeoff_account_id_text_like)){
            this.getSearchCond().like("writeoff_account_id_text", n_writeoff_account_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_payment_method_id_text_eq;//[付款方法类型]
	public void setN_payment_method_id_text_eq(String n_payment_method_id_text_eq) {
        this.n_payment_method_id_text_eq = n_payment_method_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_method_id_text_eq)){
            this.getSearchCond().eq("payment_method_id_text", n_payment_method_id_text_eq);
        }
    }
	private String n_payment_method_id_text_like;//[付款方法类型]
	public void setN_payment_method_id_text_like(String n_payment_method_id_text_like) {
        this.n_payment_method_id_text_like = n_payment_method_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_payment_method_id_text_like)){
            this.getSearchCond().like("payment_method_id_text", n_payment_method_id_text_like);
        }
    }
	private String n_payment_token_id_text_eq;//[保存的付款令牌]
	public void setN_payment_token_id_text_eq(String n_payment_token_id_text_eq) {
        this.n_payment_token_id_text_eq = n_payment_token_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_token_id_text_eq)){
            this.getSearchCond().eq("payment_token_id_text", n_payment_token_id_text_eq);
        }
    }
	private String n_payment_token_id_text_like;//[保存的付款令牌]
	public void setN_payment_token_id_text_like(String n_payment_token_id_text_like) {
        this.n_payment_token_id_text_like = n_payment_token_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_payment_token_id_text_like)){
            this.getSearchCond().like("payment_token_id_text", n_payment_token_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[业务伙伴]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[业务伙伴]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_payment_token_id_eq;//[保存的付款令牌]
	public void setN_payment_token_id_eq(Long n_payment_token_id_eq) {
        this.n_payment_token_id_eq = n_payment_token_id_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_token_id_eq)){
            this.getSearchCond().eq("payment_token_id", n_payment_token_id_eq);
        }
    }
	private Long n_payment_transaction_id_eq;//[付款交易]
	public void setN_payment_transaction_id_eq(Long n_payment_transaction_id_eq) {
        this.n_payment_transaction_id_eq = n_payment_transaction_id_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_transaction_id_eq)){
            this.getSearchCond().eq("payment_transaction_id", n_payment_transaction_id_eq);
        }
    }
	private Long n_writeoff_account_id_eq;//[差异科目]
	public void setN_writeoff_account_id_eq(Long n_writeoff_account_id_eq) {
        this.n_writeoff_account_id_eq = n_writeoff_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_writeoff_account_id_eq)){
            this.getSearchCond().eq("writeoff_account_id", n_writeoff_account_id_eq);
        }
    }
	private Long n_partner_bank_account_id_eq;//[收款银行账号]
	public void setN_partner_bank_account_id_eq(Long n_partner_bank_account_id_eq) {
        this.n_partner_bank_account_id_eq = n_partner_bank_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_bank_account_id_eq)){
            this.getSearchCond().eq("partner_bank_account_id", n_partner_bank_account_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_payment_method_id_eq;//[付款方法类型]
	public void setN_payment_method_id_eq(Long n_payment_method_id_eq) {
        this.n_payment_method_id_eq = n_payment_method_id_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_method_id_eq)){
            this.getSearchCond().eq("payment_method_id", n_payment_method_id_eq);
        }
    }
	private Long n_destination_journal_id_eq;//[转账到]
	public void setN_destination_journal_id_eq(Long n_destination_journal_id_eq) {
        this.n_destination_journal_id_eq = n_destination_journal_id_eq;
        if(!ObjectUtils.isEmpty(this.n_destination_journal_id_eq)){
            this.getSearchCond().eq("destination_journal_id", n_destination_journal_id_eq);
        }
    }
	private Long n_partner_id_eq;//[业务伙伴]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_journal_id_eq;//[付款日记账]
	public void setN_journal_id_eq(Long n_journal_id_eq) {
        this.n_journal_id_eq = n_journal_id_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_eq)){
            this.getSearchCond().eq("journal_id", n_journal_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



