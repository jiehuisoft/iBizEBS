package cn.ibizlab.businesscentral.core.odoo_product.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_pricelist_itemSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Product_pricelist_itemMapper extends BaseMapper<Product_pricelist_item>{

    Page<Product_pricelist_item> searchDefault(IPage page, @Param("srf") Product_pricelist_itemSearchContext context, @Param("ew") Wrapper<Product_pricelist_item> wrapper) ;
    @Override
    Product_pricelist_item selectById(Serializable id);
    @Override
    int insert(Product_pricelist_item entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Product_pricelist_item entity);
    @Override
    int update(@Param(Constants.ENTITY) Product_pricelist_item entity, @Param("ew") Wrapper<Product_pricelist_item> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Product_pricelist_item> selectByCategId(@Param("id") Serializable id) ;

    List<Product_pricelist_item> selectByBasePricelistId(@Param("id") Serializable id) ;

    List<Product_pricelist_item> selectByPricelistId(@Param("id") Serializable id) ;

    List<Product_pricelist_item> selectByProductId(@Param("id") Serializable id) ;

    List<Product_pricelist_item> selectByProductTmplId(@Param("id") Serializable id) ;

    List<Product_pricelist_item> selectByCompanyId(@Param("id") Serializable id) ;

    List<Product_pricelist_item> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Product_pricelist_item> selectByCreateUid(@Param("id") Serializable id) ;

    List<Product_pricelist_item> selectByWriteUid(@Param("id") Serializable id) ;


}
