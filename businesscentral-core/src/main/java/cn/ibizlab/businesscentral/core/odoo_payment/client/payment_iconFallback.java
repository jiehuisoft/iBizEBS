package cn.ibizlab.businesscentral.core.odoo_payment.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_icon;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_iconSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[payment_icon] 服务对象接口
 */
@Component
public class payment_iconFallback implements payment_iconFeignClient{


    public Payment_icon create(Payment_icon payment_icon){
            return null;
     }
    public Boolean createBatch(List<Payment_icon> payment_icons){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Payment_icon> search(Payment_iconSearchContext context){
            return null;
     }


    public Payment_icon get(Long id){
            return null;
     }


    public Payment_icon update(Long id, Payment_icon payment_icon){
            return null;
     }
    public Boolean updateBatch(List<Payment_icon> payment_icons){
            return false;
     }


    public Page<Payment_icon> select(){
            return null;
     }

    public Payment_icon getDraft(){
            return null;
    }



    public Boolean checkKey(Payment_icon payment_icon){
            return false;
     }


    public Boolean save(Payment_icon payment_icon){
            return false;
     }
    public Boolean saveBatch(List<Payment_icon> payment_icons){
            return false;
     }

    public Page<Payment_icon> searchDefault(Payment_iconSearchContext context){
            return null;
     }


}
