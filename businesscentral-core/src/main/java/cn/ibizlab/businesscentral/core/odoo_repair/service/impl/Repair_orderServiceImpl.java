package cn.ibizlab.businesscentral.core.odoo_repair.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_orderSearchContext;
import cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_orderService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_repair.mapper.Repair_orderMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[维修单] 服务对象接口实现
 */
@Slf4j
@Service("Repair_orderServiceImpl")
public class Repair_orderServiceImpl extends EBSServiceImpl<Repair_orderMapper, Repair_order> implements IRepair_orderService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_feeService repairFeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_lineService repairLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qty_repairService stockWarnInsufficientQtyRepairService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelistService productPricelistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_production_lotService stockProductionLotService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "repair.order" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Repair_order et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRepair_orderService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Repair_order> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Repair_order et) {
        Repair_order old = new Repair_order() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRepair_orderService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRepair_orderService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Repair_order> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        repairFeeService.removeByRepairId(key);
        repairLineService.removeByRepairId(key);
        stockMoveService.resetByRepairId(key);
        stockWarnInsufficientQtyRepairService.resetByRepairId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        repairFeeService.removeByRepairId(idList);
        repairLineService.removeByRepairId(idList);
        stockMoveService.resetByRepairId(idList);
        stockWarnInsufficientQtyRepairService.resetByRepairId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Repair_order get(Long key) {
        Repair_order et = getById(key);
        if(et==null){
            et=new Repair_order();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Repair_order getDraft(Repair_order et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Repair_order et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Repair_order et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Repair_order et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Repair_order> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Repair_order> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Repair_order> selectByInvoiceId(Long id) {
        return baseMapper.selectByInvoiceId(id);
    }
    @Override
    public void resetByInvoiceId(Long id) {
        this.update(new UpdateWrapper<Repair_order>().set("invoice_id",null).eq("invoice_id",id));
    }

    @Override
    public void resetByInvoiceId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_order>().set("invoice_id",null).in("invoice_id",ids));
    }

    @Override
    public void removeByInvoiceId(Long id) {
        this.remove(new QueryWrapper<Repair_order>().eq("invoice_id",id));
    }

	@Override
    public List<Repair_order> selectByPricelistId(Long id) {
        return baseMapper.selectByPricelistId(id);
    }
    @Override
    public void resetByPricelistId(Long id) {
        this.update(new UpdateWrapper<Repair_order>().set("pricelist_id",null).eq("pricelist_id",id));
    }

    @Override
    public void resetByPricelistId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_order>().set("pricelist_id",null).in("pricelist_id",ids));
    }

    @Override
    public void removeByPricelistId(Long id) {
        this.remove(new QueryWrapper<Repair_order>().eq("pricelist_id",id));
    }

	@Override
    public List<Repair_order> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Repair_order>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_order>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Repair_order>().eq("product_id",id));
    }

	@Override
    public List<Repair_order> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Repair_order>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_order>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Repair_order>().eq("company_id",id));
    }

	@Override
    public List<Repair_order> selectByAddressId(Long id) {
        return baseMapper.selectByAddressId(id);
    }
    @Override
    public void resetByAddressId(Long id) {
        this.update(new UpdateWrapper<Repair_order>().set("address_id",null).eq("address_id",id));
    }

    @Override
    public void resetByAddressId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_order>().set("address_id",null).in("address_id",ids));
    }

    @Override
    public void removeByAddressId(Long id) {
        this.remove(new QueryWrapper<Repair_order>().eq("address_id",id));
    }

	@Override
    public List<Repair_order> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Repair_order>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_order>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Repair_order>().eq("partner_id",id));
    }

	@Override
    public List<Repair_order> selectByPartnerInvoiceId(Long id) {
        return baseMapper.selectByPartnerInvoiceId(id);
    }
    @Override
    public void resetByPartnerInvoiceId(Long id) {
        this.update(new UpdateWrapper<Repair_order>().set("partner_invoice_id",null).eq("partner_invoice_id",id));
    }

    @Override
    public void resetByPartnerInvoiceId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_order>().set("partner_invoice_id",null).in("partner_invoice_id",ids));
    }

    @Override
    public void removeByPartnerInvoiceId(Long id) {
        this.remove(new QueryWrapper<Repair_order>().eq("partner_invoice_id",id));
    }

	@Override
    public List<Repair_order> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Repair_order>().eq("create_uid",id));
    }

	@Override
    public List<Repair_order> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Repair_order>().eq("write_uid",id));
    }

	@Override
    public List<Repair_order> selectByLocationId(Long id) {
        return baseMapper.selectByLocationId(id);
    }
    @Override
    public void resetByLocationId(Long id) {
        this.update(new UpdateWrapper<Repair_order>().set("location_id",null).eq("location_id",id));
    }

    @Override
    public void resetByLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_order>().set("location_id",null).in("location_id",ids));
    }

    @Override
    public void removeByLocationId(Long id) {
        this.remove(new QueryWrapper<Repair_order>().eq("location_id",id));
    }

	@Override
    public List<Repair_order> selectByMoveId(Long id) {
        return baseMapper.selectByMoveId(id);
    }
    @Override
    public void resetByMoveId(Long id) {
        this.update(new UpdateWrapper<Repair_order>().set("move_id",null).eq("move_id",id));
    }

    @Override
    public void resetByMoveId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_order>().set("move_id",null).in("move_id",ids));
    }

    @Override
    public void removeByMoveId(Long id) {
        this.remove(new QueryWrapper<Repair_order>().eq("move_id",id));
    }

	@Override
    public List<Repair_order> selectByLotId(Long id) {
        return baseMapper.selectByLotId(id);
    }
    @Override
    public void resetByLotId(Long id) {
        this.update(new UpdateWrapper<Repair_order>().set("lot_id",null).eq("lot_id",id));
    }

    @Override
    public void resetByLotId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_order>().set("lot_id",null).in("lot_id",ids));
    }

    @Override
    public void removeByLotId(Long id) {
        this.remove(new QueryWrapper<Repair_order>().eq("lot_id",id));
    }

	@Override
    public List<Repair_order> selectByProductUom(Long id) {
        return baseMapper.selectByProductUom(id);
    }
    @Override
    public void resetByProductUom(Long id) {
        this.update(new UpdateWrapper<Repair_order>().set("product_uom",null).eq("product_uom",id));
    }

    @Override
    public void resetByProductUom(Collection<Long> ids) {
        this.update(new UpdateWrapper<Repair_order>().set("product_uom",null).in("product_uom",ids));
    }

    @Override
    public void removeByProductUom(Long id) {
        this.remove(new QueryWrapper<Repair_order>().eq("product_uom",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Repair_order> searchDefault(Repair_orderSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Repair_order> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Repair_order>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Repair_order et){
        //实体关系[DER1N_REPAIR_ORDER__ACCOUNT_INVOICE__INVOICE_ID]
        if(!ObjectUtils.isEmpty(et.getInvoiceId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice odooInvoice=et.getOdooInvoice();
            if(ObjectUtils.isEmpty(odooInvoice)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice majorEntity=accountInvoiceService.get(et.getInvoiceId());
                et.setOdooInvoice(majorEntity);
                odooInvoice=majorEntity;
            }
            et.setInvoiceIdText(odooInvoice.getName());
        }
        //实体关系[DER1N_REPAIR_ORDER__PRODUCT_PRICELIST__PRICELIST_ID]
        if(!ObjectUtils.isEmpty(et.getPricelistId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist odooPricelist=et.getOdooPricelist();
            if(ObjectUtils.isEmpty(odooPricelist)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist majorEntity=productPricelistService.get(et.getPricelistId());
                et.setOdooPricelist(majorEntity);
                odooPricelist=majorEntity;
            }
            et.setPricelistIdText(odooPricelist.getName());
        }
        //实体关系[DER1N_REPAIR_ORDER__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
            et.setTracking(odooProduct.getTracking());
        }
        //实体关系[DER1N_REPAIR_ORDER__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_REPAIR_ORDER__RES_PARTNER__ADDRESS_ID]
        if(!ObjectUtils.isEmpty(et.getAddressId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooAddress=et.getOdooAddress();
            if(ObjectUtils.isEmpty(odooAddress)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getAddressId());
                et.setOdooAddress(majorEntity);
                odooAddress=majorEntity;
            }
            et.setAddressIdText(odooAddress.getName());
        }
        //实体关系[DER1N_REPAIR_ORDER__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_REPAIR_ORDER__RES_PARTNER__PARTNER_INVOICE_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerInvoiceId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartnerInvoice=et.getOdooPartnerInvoice();
            if(ObjectUtils.isEmpty(odooPartnerInvoice)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerInvoiceId());
                et.setOdooPartnerInvoice(majorEntity);
                odooPartnerInvoice=majorEntity;
            }
            et.setPartnerInvoiceIdText(odooPartnerInvoice.getName());
        }
        //实体关系[DER1N_REPAIR_ORDER__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_REPAIR_ORDER__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_REPAIR_ORDER__STOCK_LOCATION__LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation=et.getOdooLocation();
            if(ObjectUtils.isEmpty(odooLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationId());
                et.setOdooLocation(majorEntity);
                odooLocation=majorEntity;
            }
            et.setLocationIdText(odooLocation.getName());
        }
        //实体关系[DER1N_REPAIR_ORDER__STOCK_MOVE__MOVE_ID]
        if(!ObjectUtils.isEmpty(et.getMoveId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move odooMove=et.getOdooMove();
            if(ObjectUtils.isEmpty(odooMove)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move majorEntity=stockMoveService.get(et.getMoveId());
                et.setOdooMove(majorEntity);
                odooMove=majorEntity;
            }
            et.setMoveIdText(odooMove.getName());
        }
        //实体关系[DER1N_REPAIR_ORDER__STOCK_PRODUCTION_LOT__LOT_ID]
        if(!ObjectUtils.isEmpty(et.getLotId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot odooLot=et.getOdooLot();
            if(ObjectUtils.isEmpty(odooLot)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot majorEntity=stockProductionLotService.get(et.getLotId());
                et.setOdooLot(majorEntity);
                odooLot=majorEntity;
            }
            et.setLotIdText(odooLot.getName());
        }
        //实体关系[DER1N_REPAIR_ORDER__UOM_UOM__PRODUCT_UOM]
        if(!ObjectUtils.isEmpty(et.getProductUom())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUom());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Repair_order> getRepairOrderByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Repair_order> getRepairOrderByEntities(List<Repair_order> entities) {
        List ids =new ArrayList();
        for(Repair_order entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



