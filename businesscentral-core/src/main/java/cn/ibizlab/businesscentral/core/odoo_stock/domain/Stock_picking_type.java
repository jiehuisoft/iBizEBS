package cn.ibizlab.businesscentral.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[拣货类型]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "STOCK_PICKING_TYPE",resultMap = "Stock_picking_typeResultMap")
public class Stock_picking_type extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 显示详细作业
     */
    @DEField(name = "show_operations")
    @TableField(value = "show_operations")
    @JSONField(name = "show_operations")
    @JsonProperty("show_operations")
    private Boolean showOperations;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 参考序列
     */
    @DEField(name = "sequence_id")
    @TableField(value = "sequence_id")
    @JSONField(name = "sequence_id")
    @JsonProperty("sequence_id")
    private Integer sequenceId;
    /**
     * 创建新批次/序列号码
     */
    @DEField(name = "use_create_lots")
    @TableField(value = "use_create_lots")
    @JSONField(name = "use_create_lots")
    @JsonProperty("use_create_lots")
    private Boolean useCreateLots;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 条码
     */
    @TableField(value = "barcode")
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;
    /**
     * 显示预留
     */
    @DEField(name = "show_reserved")
    @TableField(value = "show_reserved")
    @JSONField(name = "show_reserved")
    @JsonProperty("show_reserved")
    private Boolean showReserved;
    /**
     * 欠单比率
     */
    @TableField(exist = false)
    @JSONField(name = "rate_picking_backorders")
    @JsonProperty("rate_picking_backorders")
    private Integer ratePickingBackorders;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 拣货欠单个数
     */
    @TableField(exist = false)
    @JSONField(name = "count_picking_backorders")
    @JsonProperty("count_picking_backorders")
    private Integer countPickingBackorders;
    /**
     * 生产单的数量
     */
    @TableField(exist = false)
    @JSONField(name = "count_mo_todo")
    @JsonProperty("count_mo_todo")
    private Integer countMoTodo;
    /**
     * 颜色
     */
    @TableField(value = "color")
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;
    /**
     * 移动整个包裹
     */
    @DEField(name = "show_entire_packs")
    @TableField(value = "show_entire_packs")
    @JSONField(name = "show_entire_packs")
    @JsonProperty("show_entire_packs")
    private Boolean showEntirePacks;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 生产单等待的数量
     */
    @TableField(exist = false)
    @JSONField(name = "count_mo_waiting")
    @JsonProperty("count_mo_waiting")
    private Integer countMoWaiting;
    /**
     * 草稿拣货个数
     */
    @TableField(exist = false)
    @JSONField(name = "count_picking_draft")
    @JsonProperty("count_picking_draft")
    private Integer countPickingDraft;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 迟到拣货个数
     */
    @TableField(exist = false)
    @JSONField(name = "count_picking_late")
    @JsonProperty("count_picking_late")
    private Integer countPickingLate;
    /**
     * 拣货个数
     */
    @TableField(exist = false)
    @JSONField(name = "count_picking")
    @JsonProperty("count_picking")
    private Integer countPicking;
    /**
     * 拣货个数准备好
     */
    @TableField(exist = false)
    @JSONField(name = "count_picking_ready")
    @JsonProperty("count_picking_ready")
    private Integer countPickingReady;
    /**
     * 最近10笔完成的拣货
     */
    @TableField(exist = false)
    @JSONField(name = "last_done_picking")
    @JsonProperty("last_done_picking")
    private String lastDonePicking;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 等待拣货个数
     */
    @TableField(exist = false)
    @JSONField(name = "count_picking_waiting")
    @JsonProperty("count_picking_waiting")
    private Integer countPickingWaiting;
    /**
     * 作业的类型
     */
    @TableField(value = "code")
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 延迟比率
     */
    @TableField(exist = false)
    @JSONField(name = "rate_picking_late")
    @JsonProperty("rate_picking_late")
    private Integer ratePickingLate;
    /**
     * 作业类型
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 生产单数量延迟
     */
    @TableField(exist = false)
    @JSONField(name = "count_mo_late")
    @JsonProperty("count_mo_late")
    private Integer countMoLate;
    /**
     * 使用已有批次/序列号码
     */
    @DEField(name = "use_existing_lots")
    @TableField(value = "use_existing_lots")
    @JSONField(name = "use_existing_lots")
    @JsonProperty("use_existing_lots")
    private Boolean useExistingLots;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 默认源位置
     */
    @TableField(exist = false)
    @JSONField(name = "default_location_src_id_text")
    @JsonProperty("default_location_src_id_text")
    private String defaultLocationSrcIdText;
    /**
     * 退回的作业类型
     */
    @TableField(exist = false)
    @JSONField(name = "return_picking_type_id_text")
    @JsonProperty("return_picking_type_id_text")
    private String returnPickingTypeIdText;
    /**
     * 仓库
     */
    @TableField(exist = false)
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    private String warehouseIdText;
    /**
     * 默认目的位置
     */
    @TableField(exist = false)
    @JSONField(name = "default_location_dest_id_text")
    @JsonProperty("default_location_dest_id_text")
    private String defaultLocationDestIdText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 默认目的位置
     */
    @DEField(name = "default_location_dest_id")
    @TableField(value = "default_location_dest_id")
    @JSONField(name = "default_location_dest_id")
    @JsonProperty("default_location_dest_id")
    private Long defaultLocationDestId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 仓库
     */
    @DEField(name = "warehouse_id")
    @TableField(value = "warehouse_id")
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Long warehouseId;
    /**
     * 默认源位置
     */
    @DEField(name = "default_location_src_id")
    @TableField(value = "default_location_src_id")
    @JSONField(name = "default_location_src_id")
    @JsonProperty("default_location_src_id")
    private Long defaultLocationSrcId;
    /**
     * 退回的作业类型
     */
    @DEField(name = "return_picking_type_id")
    @TableField(value = "return_picking_type_id")
    @JSONField(name = "return_picking_type_id")
    @JsonProperty("return_picking_type_id")
    private Long returnPickingTypeId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooDefaultLocationDest;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooDefaultLocationSrc;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooReturnPickingType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooWarehouse;



    /**
     * 设置 [显示详细作业]
     */
    public void setShowOperations(Boolean showOperations){
        this.showOperations = showOperations ;
        this.modify("show_operations",showOperations);
    }

    /**
     * 设置 [参考序列]
     */
    public void setSequenceId(Integer sequenceId){
        this.sequenceId = sequenceId ;
        this.modify("sequence_id",sequenceId);
    }

    /**
     * 设置 [创建新批次/序列号码]
     */
    public void setUseCreateLots(Boolean useCreateLots){
        this.useCreateLots = useCreateLots ;
        this.modify("use_create_lots",useCreateLots);
    }

    /**
     * 设置 [条码]
     */
    public void setBarcode(String barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }

    /**
     * 设置 [显示预留]
     */
    public void setShowReserved(Boolean showReserved){
        this.showReserved = showReserved ;
        this.modify("show_reserved",showReserved);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [颜色]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [移动整个包裹]
     */
    public void setShowEntirePacks(Boolean showEntirePacks){
        this.showEntirePacks = showEntirePacks ;
        this.modify("show_entire_packs",showEntirePacks);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [作业的类型]
     */
    public void setCode(String code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [作业类型]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [使用已有批次/序列号码]
     */
    public void setUseExistingLots(Boolean useExistingLots){
        this.useExistingLots = useExistingLots ;
        this.modify("use_existing_lots",useExistingLots);
    }

    /**
     * 设置 [默认目的位置]
     */
    public void setDefaultLocationDestId(Long defaultLocationDestId){
        this.defaultLocationDestId = defaultLocationDestId ;
        this.modify("default_location_dest_id",defaultLocationDestId);
    }

    /**
     * 设置 [仓库]
     */
    public void setWarehouseId(Long warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }

    /**
     * 设置 [默认源位置]
     */
    public void setDefaultLocationSrcId(Long defaultLocationSrcId){
        this.defaultLocationSrcId = defaultLocationSrcId ;
        this.modify("default_location_src_id",defaultLocationSrcId);
    }

    /**
     * 设置 [退回的作业类型]
     */
    public void setReturnPickingTypeId(Long returnPickingTypeId){
        this.returnPickingTypeId = returnPickingTypeId ;
        this.modify("return_picking_type_id",returnPickingTypeId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


