package cn.ibizlab.businesscentral.core.odoo_event.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[自动发邮件]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "EVENT_MAIL",resultMap = "Event_mailResultMap")
public class Event_mail extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 邮箱注册
     */
    @TableField(exist = false)
    @JSONField(name = "mail_registration_ids")
    @JsonProperty("mail_registration_ids")
    private String mailRegistrationIds;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 间隔
     */
    @DEField(name = "interval_nbr")
    @TableField(value = "interval_nbr")
    @JSONField(name = "interval_nbr")
    @JsonProperty("interval_nbr")
    private Integer intervalNbr;
    /**
     * 已汇
     */
    @TableField(value = "done")
    @JSONField(name = "done")
    @JsonProperty("done")
    private Boolean done;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 触发器
     */
    @DEField(name = "interval_type")
    @TableField(value = "interval_type")
    @JSONField(name = "interval_type")
    @JsonProperty("interval_type")
    private String intervalType;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 在事件上发送EMail
     */
    @DEField(name = "mail_sent")
    @TableField(value = "mail_sent")
    @JSONField(name = "mail_sent")
    @JsonProperty("mail_sent")
    private Boolean mailSent;
    /**
     * 现实顺序
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 计划发出的邮件
     */
    @DEField(name = "scheduled_date")
    @TableField(value = "scheduled_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduled_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduled_date")
    private Timestamp scheduledDate;
    /**
     * 单位
     */
    @DEField(name = "interval_unit")
    @TableField(value = "interval_unit")
    @JSONField(name = "interval_unit")
    @JsonProperty("interval_unit")
    private String intervalUnit;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "event_id_text")
    @JsonProperty("event_id_text")
    private String eventIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * EMail模板
     */
    @TableField(exist = false)
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    private String templateIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 活动
     */
    @DEField(name = "event_id")
    @TableField(value = "event_id")
    @JSONField(name = "event_id")
    @JsonProperty("event_id")
    private Long eventId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * EMail模板
     */
    @DEField(name = "template_id")
    @TableField(value = "template_id")
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    private Long templateId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event odooEvent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooTemplate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [间隔]
     */
    public void setIntervalNbr(Integer intervalNbr){
        this.intervalNbr = intervalNbr ;
        this.modify("interval_nbr",intervalNbr);
    }

    /**
     * 设置 [已汇]
     */
    public void setDone(Boolean done){
        this.done = done ;
        this.modify("done",done);
    }

    /**
     * 设置 [触发器]
     */
    public void setIntervalType(String intervalType){
        this.intervalType = intervalType ;
        this.modify("interval_type",intervalType);
    }

    /**
     * 设置 [在事件上发送EMail]
     */
    public void setMailSent(Boolean mailSent){
        this.mailSent = mailSent ;
        this.modify("mail_sent",mailSent);
    }

    /**
     * 设置 [现实顺序]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [计划发出的邮件]
     */
    public void setScheduledDate(Timestamp scheduledDate){
        this.scheduledDate = scheduledDate ;
        this.modify("scheduled_date",scheduledDate);
    }

    /**
     * 格式化日期 [计划发出的邮件]
     */
    public String formatScheduledDate(){
        if (this.scheduledDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(scheduledDate);
    }
    /**
     * 设置 [单位]
     */
    public void setIntervalUnit(String intervalUnit){
        this.intervalUnit = intervalUnit ;
        this.modify("interval_unit",intervalUnit);
    }

    /**
     * 设置 [活动]
     */
    public void setEventId(Long eventId){
        this.eventId = eventId ;
        this.modify("event_id",eventId);
    }

    /**
     * 设置 [EMail模板]
     */
    public void setTemplateId(Long templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


