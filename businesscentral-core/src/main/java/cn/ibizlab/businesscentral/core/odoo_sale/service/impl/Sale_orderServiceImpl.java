package cn.ibizlab.businesscentral.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_orderSearchContext;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_orderService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_sale.mapper.Sale_orderMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[销售订单] 服务对象接口实现
 */
@Slf4j
@Service("Sale_orderServiceImpl")
public class Sale_orderServiceImpl extends EBSServiceImpl<Sale_orderMapper, Sale_order> implements ISale_orderService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_registrationService eventRegistrationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expenseService hrExpenseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService purchaseOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService saleOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_optionService saleOrderOptionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_reportService saleReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_pickingService stockPickingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_positionService accountFiscalPositionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_incotermsService accountIncotermsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_termService accountPaymentTermService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_leadService crmLeadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_teamService crmTeamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelistService productPricelistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_templateService saleOrderTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService stockWarehouseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_campaignService utmCampaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_mediumService utmMediumService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_sourceService utmSourceService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "sale.order" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Sale_order et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_orderService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Sale_order> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Sale_order et) {
        Sale_order old = new Sale_order() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_orderService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_orderService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Sale_order> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        eventRegistrationService.removeBySaleOrderId(key);
        hrExpenseService.resetBySaleOrderId(key);
        purchaseOrderLineService.resetBySaleOrderId(key);
        saleOrderLineService.removeByOrderId(key);
        saleOrderOptionService.removeByOrderId(key);
        saleReportService.resetByOrderId(key);
        stockPickingService.resetBySaleId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        eventRegistrationService.removeBySaleOrderId(idList);
        hrExpenseService.resetBySaleOrderId(idList);
        purchaseOrderLineService.resetBySaleOrderId(idList);
        saleOrderLineService.removeByOrderId(idList);
        saleOrderOptionService.removeByOrderId(idList);
        saleReportService.resetByOrderId(idList);
        stockPickingService.resetBySaleId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Sale_order get(Long key) {
        Sale_order et = getById(key);
        if(et==null){
            et=new Sale_order();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Sale_order getDraft(Sale_order et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Sale_order et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Sale_order et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Sale_order et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Sale_order> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Sale_order> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Sale_order> selectByAnalyticAccountId(Long id) {
        return baseMapper.selectByAnalyticAccountId(id);
    }
    @Override
    public void resetByAnalyticAccountId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("analytic_account_id",null).eq("analytic_account_id",id));
    }

    @Override
    public void resetByAnalyticAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("analytic_account_id",null).in("analytic_account_id",ids));
    }

    @Override
    public void removeByAnalyticAccountId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("analytic_account_id",id));
    }

	@Override
    public List<Sale_order> selectByFiscalPositionId(Long id) {
        return baseMapper.selectByFiscalPositionId(id);
    }
    @Override
    public void resetByFiscalPositionId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("fiscal_position_id",null).eq("fiscal_position_id",id));
    }

    @Override
    public void resetByFiscalPositionId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("fiscal_position_id",null).in("fiscal_position_id",ids));
    }

    @Override
    public void removeByFiscalPositionId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("fiscal_position_id",id));
    }

	@Override
    public List<Sale_order> selectByIncoterm(Long id) {
        return baseMapper.selectByIncoterm(id);
    }
    @Override
    public void resetByIncoterm(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("incoterm",null).eq("incoterm",id));
    }

    @Override
    public void resetByIncoterm(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("incoterm",null).in("incoterm",ids));
    }

    @Override
    public void removeByIncoterm(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("incoterm",id));
    }

	@Override
    public List<Sale_order> selectByPaymentTermId(Long id) {
        return baseMapper.selectByPaymentTermId(id);
    }
    @Override
    public void resetByPaymentTermId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("payment_term_id",null).eq("payment_term_id",id));
    }

    @Override
    public void resetByPaymentTermId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("payment_term_id",null).in("payment_term_id",ids));
    }

    @Override
    public void removeByPaymentTermId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("payment_term_id",id));
    }

	@Override
    public List<Sale_order> selectByOpportunityId(Long id) {
        return baseMapper.selectByOpportunityId(id);
    }
    @Override
    public void resetByOpportunityId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("opportunity_id",null).eq("opportunity_id",id));
    }

    @Override
    public void resetByOpportunityId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("opportunity_id",null).in("opportunity_id",ids));
    }

    @Override
    public void removeByOpportunityId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("opportunity_id",id));
    }

	@Override
    public List<Sale_order> selectByTeamId(Long id) {
        return baseMapper.selectByTeamId(id);
    }
    @Override
    public void resetByTeamId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("team_id",null).eq("team_id",id));
    }

    @Override
    public void resetByTeamId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("team_id",null).in("team_id",ids));
    }

    @Override
    public void removeByTeamId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("team_id",id));
    }

	@Override
    public List<Sale_order> selectByPricelistId(Long id) {
        return baseMapper.selectByPricelistId(id);
    }
    @Override
    public void resetByPricelistId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("pricelist_id",null).eq("pricelist_id",id));
    }

    @Override
    public void resetByPricelistId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("pricelist_id",null).in("pricelist_id",ids));
    }

    @Override
    public void removeByPricelistId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("pricelist_id",id));
    }

	@Override
    public List<Sale_order> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("company_id",id));
    }

	@Override
    public List<Sale_order> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("partner_id",id));
    }

	@Override
    public List<Sale_order> selectByPartnerInvoiceId(Long id) {
        return baseMapper.selectByPartnerInvoiceId(id);
    }
    @Override
    public void resetByPartnerInvoiceId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("partner_invoice_id",null).eq("partner_invoice_id",id));
    }

    @Override
    public void resetByPartnerInvoiceId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("partner_invoice_id",null).in("partner_invoice_id",ids));
    }

    @Override
    public void removeByPartnerInvoiceId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("partner_invoice_id",id));
    }

	@Override
    public List<Sale_order> selectByPartnerShippingId(Long id) {
        return baseMapper.selectByPartnerShippingId(id);
    }
    @Override
    public void resetByPartnerShippingId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("partner_shipping_id",null).eq("partner_shipping_id",id));
    }

    @Override
    public void resetByPartnerShippingId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("partner_shipping_id",null).in("partner_shipping_id",ids));
    }

    @Override
    public void removeByPartnerShippingId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("partner_shipping_id",id));
    }

	@Override
    public List<Sale_order> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("create_uid",id));
    }

	@Override
    public List<Sale_order> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("user_id",id));
    }

	@Override
    public List<Sale_order> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("write_uid",id));
    }

	@Override
    public List<Sale_order> selectBySaleOrderTemplateId(Long id) {
        return baseMapper.selectBySaleOrderTemplateId(id);
    }
    @Override
    public void resetBySaleOrderTemplateId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("sale_order_template_id",null).eq("sale_order_template_id",id));
    }

    @Override
    public void resetBySaleOrderTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("sale_order_template_id",null).in("sale_order_template_id",ids));
    }

    @Override
    public void removeBySaleOrderTemplateId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("sale_order_template_id",id));
    }

	@Override
    public List<Sale_order> selectByWarehouseId(Long id) {
        return baseMapper.selectByWarehouseId(id);
    }
    @Override
    public void resetByWarehouseId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("warehouse_id",null).eq("warehouse_id",id));
    }

    @Override
    public void resetByWarehouseId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("warehouse_id",null).in("warehouse_id",ids));
    }

    @Override
    public void removeByWarehouseId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("warehouse_id",id));
    }

	@Override
    public List<Sale_order> selectByCampaignId(Long id) {
        return baseMapper.selectByCampaignId(id);
    }
    @Override
    public void resetByCampaignId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("campaign_id",null).eq("campaign_id",id));
    }

    @Override
    public void resetByCampaignId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("campaign_id",null).in("campaign_id",ids));
    }

    @Override
    public void removeByCampaignId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("campaign_id",id));
    }

	@Override
    public List<Sale_order> selectByMediumId(Long id) {
        return baseMapper.selectByMediumId(id);
    }
    @Override
    public void resetByMediumId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("medium_id",null).eq("medium_id",id));
    }

    @Override
    public void resetByMediumId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("medium_id",null).in("medium_id",ids));
    }

    @Override
    public void removeByMediumId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("medium_id",id));
    }

	@Override
    public List<Sale_order> selectBySourceId(Long id) {
        return baseMapper.selectBySourceId(id);
    }
    @Override
    public void resetBySourceId(Long id) {
        this.update(new UpdateWrapper<Sale_order>().set("source_id",null).eq("source_id",id));
    }

    @Override
    public void resetBySourceId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_order>().set("source_id",null).in("source_id",ids));
    }

    @Override
    public void removeBySourceId(Long id) {
        this.remove(new QueryWrapper<Sale_order>().eq("source_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Sale_order> searchDefault(Sale_orderSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Sale_order> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Sale_order>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Sale_order et){
        //实体关系[DER1N_SALE_ORDER__ACCOUNT_ANALYTIC_ACCOUNT__ANALYTIC_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAnalyticAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAnalyticAccount=et.getOdooAnalyticAccount();
            if(ObjectUtils.isEmpty(odooAnalyticAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account majorEntity=accountAnalyticAccountService.get(et.getAnalyticAccountId());
                et.setOdooAnalyticAccount(majorEntity);
                odooAnalyticAccount=majorEntity;
            }
            et.setAnalyticAccountIdText(odooAnalyticAccount.getName());
        }
        //实体关系[DER1N_SALE_ORDER__ACCOUNT_FISCAL_POSITION__FISCAL_POSITION_ID]
        if(!ObjectUtils.isEmpty(et.getFiscalPositionId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position odooFiscalPosition=et.getOdooFiscalPosition();
            if(ObjectUtils.isEmpty(odooFiscalPosition)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position majorEntity=accountFiscalPositionService.get(et.getFiscalPositionId());
                et.setOdooFiscalPosition(majorEntity);
                odooFiscalPosition=majorEntity;
            }
            et.setFiscalPositionIdText(odooFiscalPosition.getName());
        }
        //实体关系[DER1N_SALE_ORDER__ACCOUNT_INCOTERMS__INCOTERM]
        if(!ObjectUtils.isEmpty(et.getIncoterm())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms odooIncoterm=et.getOdooIncoterm();
            if(ObjectUtils.isEmpty(odooIncoterm)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms majorEntity=accountIncotermsService.get(et.getIncoterm());
                et.setOdooIncoterm(majorEntity);
                odooIncoterm=majorEntity;
            }
            et.setIncotermText(odooIncoterm.getName());
        }
        //实体关系[DER1N_SALE_ORDER__ACCOUNT_PAYMENT_TERM__PAYMENT_TERM_ID]
        if(!ObjectUtils.isEmpty(et.getPaymentTermId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term odooPaymentTerm=et.getOdooPaymentTerm();
            if(ObjectUtils.isEmpty(odooPaymentTerm)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term majorEntity=accountPaymentTermService.get(et.getPaymentTermId());
                et.setOdooPaymentTerm(majorEntity);
                odooPaymentTerm=majorEntity;
            }
            et.setPaymentTermIdText(odooPaymentTerm.getName());
        }
        //实体关系[DER1N_SALE_ORDER__CRM_LEAD__OPPORTUNITY_ID]
        if(!ObjectUtils.isEmpty(et.getOpportunityId())){
            cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead odooOpportunity=et.getOdooOpportunity();
            if(ObjectUtils.isEmpty(odooOpportunity)){
                cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead majorEntity=crmLeadService.get(et.getOpportunityId());
                et.setOdooOpportunity(majorEntity);
                odooOpportunity=majorEntity;
            }
            et.setOpportunityIdText(odooOpportunity.getName());
        }
        //实体关系[DER1N_SALE_ORDER__CRM_TEAM__TEAM_ID]
        if(!ObjectUtils.isEmpty(et.getTeamId())){
            cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team odooTeam=et.getOdooTeam();
            if(ObjectUtils.isEmpty(odooTeam)){
                cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team majorEntity=crmTeamService.get(et.getTeamId());
                et.setOdooTeam(majorEntity);
                odooTeam=majorEntity;
            }
            et.setTeamIdText(odooTeam.getName());
        }
        //实体关系[DER1N_SALE_ORDER__PRODUCT_PRICELIST__PRICELIST_ID]
        if(!ObjectUtils.isEmpty(et.getPricelistId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist odooPricelist=et.getOdooPricelist();
            if(ObjectUtils.isEmpty(odooPricelist)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist majorEntity=productPricelistService.get(et.getPricelistId());
                et.setOdooPricelist(majorEntity);
                odooPricelist=majorEntity;
            }
            et.setPricelistIdText(odooPricelist.getName());
            et.setCurrencyId(odooPricelist.getCurrencyId());
        }
        //实体关系[DER1N_SALE_ORDER__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_SALE_ORDER__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_SALE_ORDER__RES_PARTNER__PARTNER_INVOICE_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerInvoiceId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartnerInvoice=et.getOdooPartnerInvoice();
            if(ObjectUtils.isEmpty(odooPartnerInvoice)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerInvoiceId());
                et.setOdooPartnerInvoice(majorEntity);
                odooPartnerInvoice=majorEntity;
            }
            et.setPartnerInvoiceIdText(odooPartnerInvoice.getName());
        }
        //实体关系[DER1N_SALE_ORDER__RES_PARTNER__PARTNER_SHIPPING_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerShippingId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartnerShipping=et.getOdooPartnerShipping();
            if(ObjectUtils.isEmpty(odooPartnerShipping)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerShippingId());
                et.setOdooPartnerShipping(majorEntity);
                odooPartnerShipping=majorEntity;
            }
            et.setPartnerShippingIdText(odooPartnerShipping.getName());
        }
        //实体关系[DER1N_SALE_ORDER__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_SALE_ORDER__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_SALE_ORDER__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_SALE_ORDER__SALE_ORDER_TEMPLATE__SALE_ORDER_TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getSaleOrderTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template odooSaleOrderTemplate=et.getOdooSaleOrderTemplate();
            if(ObjectUtils.isEmpty(odooSaleOrderTemplate)){
                cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template majorEntity=saleOrderTemplateService.get(et.getSaleOrderTemplateId());
                et.setOdooSaleOrderTemplate(majorEntity);
                odooSaleOrderTemplate=majorEntity;
            }
            et.setSaleOrderTemplateIdText(odooSaleOrderTemplate.getName());
        }
        //实体关系[DER1N_SALE_ORDER__STOCK_WAREHOUSE__WAREHOUSE_ID]
        if(!ObjectUtils.isEmpty(et.getWarehouseId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooWarehouse=et.getOdooWarehouse();
            if(ObjectUtils.isEmpty(odooWarehouse)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse majorEntity=stockWarehouseService.get(et.getWarehouseId());
                et.setOdooWarehouse(majorEntity);
                odooWarehouse=majorEntity;
            }
            et.setWarehouseIdText(odooWarehouse.getName());
        }
        //实体关系[DER1N_SALE_ORDER__UTM_CAMPAIGN__CAMPAIGN_ID]
        if(!ObjectUtils.isEmpty(et.getCampaignId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign odooCampaign=et.getOdooCampaign();
            if(ObjectUtils.isEmpty(odooCampaign)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign majorEntity=utmCampaignService.get(et.getCampaignId());
                et.setOdooCampaign(majorEntity);
                odooCampaign=majorEntity;
            }
            et.setCampaignIdText(odooCampaign.getName());
        }
        //实体关系[DER1N_SALE_ORDER__UTM_MEDIUM__MEDIUM_ID]
        if(!ObjectUtils.isEmpty(et.getMediumId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium odooMedium=et.getOdooMedium();
            if(ObjectUtils.isEmpty(odooMedium)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium majorEntity=utmMediumService.get(et.getMediumId());
                et.setOdooMedium(majorEntity);
                odooMedium=majorEntity;
            }
            et.setMediumIdText(odooMedium.getName());
        }
        //实体关系[DER1N_SALE_ORDER__UTM_SOURCE__SOURCE_ID]
        if(!ObjectUtils.isEmpty(et.getSourceId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource=et.getOdooSource();
            if(ObjectUtils.isEmpty(odooSource)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source majorEntity=utmSourceService.get(et.getSourceId());
                et.setOdooSource(majorEntity);
                odooSource=majorEntity;
            }
            et.setSourceIdText(odooSource.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Sale_order> getSaleOrderByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Sale_order> getSaleOrderByEntities(List<Sale_order> entities) {
        List ids =new ArrayList();
        for(Sale_order entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



