package cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;

import cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.IPurchase_order_linecalc_priceLogic;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line;

/**
 * 关系型数据实体[calc_price] 对象
 */
@Slf4j
@Service
public class Purchase_order_linecalc_priceLogicImpl implements IPurchase_order_linecalc_priceLogic{

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService purchase_order_lineservice;

    public cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService getPurchase_order_lineService() {
        return this.purchase_order_lineservice;
    }


    @Autowired
    private cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService iBzSysDefaultService;

    public cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    public void execute(Purchase_order_line et){

          KieSession kieSession = null;
        try{
           kieSession=kieContainer.newKieSession();
           kieSession.insert(et); 
           kieSession.setGlobal("purchase_order_linecalc_pricedefault",et);
           kieSession.setGlobal("purchase_order_lineservice",purchase_order_lineservice);
           kieSession.setGlobal("iBzSysPurchase_order_lineDefaultService",iBzSysDefaultService);
           kieSession.setGlobal("curuser", cn.ibizlab.businesscentral.util.security.AuthenticationUser.getAuthenticationUser());
           kieSession.startProcess("cn.ibizlab.businesscentral.core.odoo_purchase.service.logic.purchase_order_linecalc_price");

        }catch(Exception e){
            throw new RuntimeException("执行[计算行金额]处理逻辑发生异常"+e);
        }finally {
            if(kieSession!=null)
            kieSession.destroy();
        }
    }

}
