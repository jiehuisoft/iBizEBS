package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_contact;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_contactSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_mass_mailing_contact] 服务对象接口
 */
@Component
public class mail_mass_mailing_contactFallback implements mail_mass_mailing_contactFeignClient{



    public Page<Mail_mass_mailing_contact> search(Mail_mass_mailing_contactSearchContext context){
            return null;
     }


    public Mail_mass_mailing_contact create(Mail_mass_mailing_contact mail_mass_mailing_contact){
            return null;
     }
    public Boolean createBatch(List<Mail_mass_mailing_contact> mail_mass_mailing_contacts){
            return false;
     }


    public Mail_mass_mailing_contact get(Long id){
            return null;
     }


    public Mail_mass_mailing_contact update(Long id, Mail_mass_mailing_contact mail_mass_mailing_contact){
            return null;
     }
    public Boolean updateBatch(List<Mail_mass_mailing_contact> mail_mass_mailing_contacts){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Mail_mass_mailing_contact> select(){
            return null;
     }

    public Mail_mass_mailing_contact getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_mass_mailing_contact mail_mass_mailing_contact){
            return false;
     }


    public Boolean save(Mail_mass_mailing_contact mail_mass_mailing_contact){
            return false;
     }
    public Boolean saveBatch(List<Mail_mass_mailing_contact> mail_mass_mailing_contacts){
            return false;
     }

    public Page<Mail_mass_mailing_contact> searchDefault(Mail_mass_mailing_contactSearchContext context){
            return null;
     }


}
