package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_countrySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_country] 服务对象接口
 */
public interface IRes_countryService extends IService<Res_country>{

    boolean create(Res_country et) ;
    void createBatch(List<Res_country> list) ;
    boolean update(Res_country et) ;
    void updateBatch(List<Res_country> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_country get(Long key) ;
    Res_country getDraft(Res_country et) ;
    boolean checkKey(Res_country et) ;
    boolean save(Res_country et) ;
    void saveBatch(List<Res_country> list) ;
    Page<Res_country> searchDefault(Res_countrySearchContext context) ;
    List<Res_country> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Res_country> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_country> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_country> getResCountryByIds(List<Long> ids) ;
    List<Res_country> getResCountryByEntities(List<Res_country> entities) ;
}


