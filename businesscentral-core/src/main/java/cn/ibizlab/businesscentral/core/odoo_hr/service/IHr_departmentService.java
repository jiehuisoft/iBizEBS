package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_departmentSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_department] 服务对象接口
 */
public interface IHr_departmentService extends IService<Hr_department>{

    boolean create(Hr_department et) ;
    void createBatch(List<Hr_department> list) ;
    boolean update(Hr_department et) ;
    void updateBatch(List<Hr_department> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_department get(Long key) ;
    Hr_department getDraft(Hr_department et) ;
    boolean checkKey(Hr_department et) ;
    boolean save(Hr_department et) ;
    void saveBatch(List<Hr_department> list) ;
    Page<Hr_department> searchDefault(Hr_departmentSearchContext context) ;
    Page<Hr_department> searchMaster(Hr_departmentSearchContext context) ;
    Page<Hr_department> searchROOT(Hr_departmentSearchContext context) ;
    List<Hr_department> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Hr_department> selectByManagerId(Long id);
    void resetByManagerId(Long id);
    void resetByManagerId(Collection<Long> ids);
    void removeByManagerId(Long id);
    List<Hr_department> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Hr_department> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_department> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_department> getHrDepartmentByIds(List<Long> ids) ;
    List<Hr_department> getHrDepartmentByEntities(List<Hr_department> entities) ;
}


