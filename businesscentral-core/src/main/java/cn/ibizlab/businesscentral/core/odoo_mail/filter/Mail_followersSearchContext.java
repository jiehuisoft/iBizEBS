package cn.ibizlab.businesscentral.core.odoo_mail.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_followers;
/**
 * 关系型数据实体[Mail_followers] 查询条件对象
 */
@Slf4j
@Data
public class Mail_followersSearchContext extends QueryWrapperContext<Mail_followers> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private Integer n_res_id_eq;//[相关文档编号]
	public void setN_res_id_eq(Integer n_res_id_eq) {
        this.n_res_id_eq = n_res_id_eq;
        if(!ObjectUtils.isEmpty(this.n_res_id_eq)){
            this.getSearchCond().eq("res_id", n_res_id_eq);
        }
    }
	private String n_res_model_eq;//[相关的文档模型名称]
	public void setN_res_model_eq(String n_res_model_eq) {
        this.n_res_model_eq = n_res_model_eq;
        if(!ObjectUtils.isEmpty(this.n_res_model_eq)){
            this.getSearchCond().eq("res_model", n_res_model_eq);
        }
    }
	private String n_partner_id_text_eq;//[相关的业务伙伴]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[相关的业务伙伴]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_channel_id_text_eq;//[监听器]
	public void setN_channel_id_text_eq(String n_channel_id_text_eq) {
        this.n_channel_id_text_eq = n_channel_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_channel_id_text_eq)){
            this.getSearchCond().eq("channel_id_text", n_channel_id_text_eq);
        }
    }
	private String n_channel_id_text_like;//[监听器]
	public void setN_channel_id_text_like(String n_channel_id_text_like) {
        this.n_channel_id_text_like = n_channel_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_channel_id_text_like)){
            this.getSearchCond().like("channel_id_text", n_channel_id_text_like);
        }
    }
	private Long n_channel_id_eq;//[监听器]
	public void setN_channel_id_eq(Long n_channel_id_eq) {
        this.n_channel_id_eq = n_channel_id_eq;
        if(!ObjectUtils.isEmpty(this.n_channel_id_eq)){
            this.getSearchCond().eq("channel_id", n_channel_id_eq);
        }
    }
	private Long n_partner_id_eq;//[相关的业务伙伴]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



