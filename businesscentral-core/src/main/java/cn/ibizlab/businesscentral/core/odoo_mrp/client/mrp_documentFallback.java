package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_document;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_documentSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_document] 服务对象接口
 */
@Component
public class mrp_documentFallback implements mrp_documentFeignClient{


    public Mrp_document create(Mrp_document mrp_document){
            return null;
     }
    public Boolean createBatch(List<Mrp_document> mrp_documents){
            return false;
     }

    public Page<Mrp_document> search(Mrp_documentSearchContext context){
            return null;
     }




    public Mrp_document get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Mrp_document update(Long id, Mrp_document mrp_document){
            return null;
     }
    public Boolean updateBatch(List<Mrp_document> mrp_documents){
            return false;
     }


    public Page<Mrp_document> select(){
            return null;
     }

    public Mrp_document getDraft(){
            return null;
    }



    public Boolean checkKey(Mrp_document mrp_document){
            return false;
     }


    public Boolean save(Mrp_document mrp_document){
            return false;
     }
    public Boolean saveBatch(List<Mrp_document> mrp_documents){
            return false;
     }

    public Page<Mrp_document> searchDefault(Mrp_documentSearchContext context){
            return null;
     }


}
