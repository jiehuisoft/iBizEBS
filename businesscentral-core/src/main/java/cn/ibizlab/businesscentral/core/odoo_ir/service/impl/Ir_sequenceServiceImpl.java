package cn.ibizlab.businesscentral.core.odoo_ir.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_sequence;
import cn.ibizlab.businesscentral.core.odoo_ir.filter.Ir_sequenceSearchContext;
import cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_sequenceService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_ir.mapper.Ir_sequenceMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[序列] 服务对象接口实现
 */
@Slf4j
@Service("Ir_sequenceServiceImpl")
public class Ir_sequenceServiceImpl extends EBSServiceImpl<Ir_sequenceMapper, Ir_sequence> implements IIr_sequenceService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_sequence_date_rangeService irSequenceDateRangeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_ir.service.logic.IIr_sequencecreate_sequenceLogic create_sequenceLogic;

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_ir.service.logic.IIr_sequencealter_sequenceLogic alter_sequenceLogic;

    protected int batchSize = 500;

    public String getIrModel(){
        return "ir.sequence" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Ir_sequence et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IIr_sequenceService)AopContext.currentProxy() : this).get(et.getId()),et);
        create_sequenceLogic.execute(et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Ir_sequence> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Ir_sequence et) {
        Ir_sequence old = new Ir_sequence() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IIr_sequenceService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IIr_sequenceService)AopContext.currentProxy() : this).get(et.getId()),et);
        alter_sequenceLogic.execute(et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Ir_sequence> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Ir_sequence get(Long key) {
        Ir_sequence et = getById(key);
        if(et==null){
            et=new Ir_sequence();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Ir_sequence getDraft(Ir_sequence et) {
        fillParentData(et);
        return et;
    }

    @Override
    @Transactional
    public Ir_sequence alter_sequence(Ir_sequence et) {
        //自定义代码
        return et;
    }

    @Override
    public boolean checkKey(Ir_sequence et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public Ir_sequence create_sequence(Ir_sequence et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Ir_sequence drop_sequence(Ir_sequence et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Ir_sequence get_next(Ir_sequence et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Ir_sequence get_next_by_code(Ir_sequence et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Ir_sequence predict_nextval(Ir_sequence et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean save(Ir_sequence et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Ir_sequence et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Ir_sequence> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Ir_sequence> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }

    @Override
    @Transactional
    public Ir_sequence select_nextval(Ir_sequence et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public Ir_sequence update_nogap(Ir_sequence et) {
        //自定义代码
        return et;
    }


	@Override
    public List<Ir_sequence> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Ir_sequence>().eq("company_id",id));
    }

	@Override
    public List<Ir_sequence> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Ir_sequence>().eq("create_uid",id));
    }

	@Override
    public List<Ir_sequence> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Ir_sequence>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Ir_sequence> searchDefault(Ir_sequenceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Ir_sequence> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Ir_sequence>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Ir_sequence et){
        //实体关系[DER1N_IR_SEQUENCE_RES_USERS_CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUname(odooCreate.getName());
        }
        //实体关系[DER1N_IR_SEQUENCE_RES_USERS_WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUname(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }




}



