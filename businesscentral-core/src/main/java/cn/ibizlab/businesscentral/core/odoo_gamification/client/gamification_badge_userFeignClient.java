package cn.ibizlab.businesscentral.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge_user;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_badge_userSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[gamification_badge_user] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-gamification:odoo-gamification}", contextId = "gamification-badge-user", fallback = gamification_badge_userFallback.class)
public interface gamification_badge_userFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_badge_users/{id}")
    Gamification_badge_user get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_badge_users/{id}")
    Gamification_badge_user update(@PathVariable("id") Long id,@RequestBody Gamification_badge_user gamification_badge_user);

    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_badge_users/batch")
    Boolean updateBatch(@RequestBody List<Gamification_badge_user> gamification_badge_users);



    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users")
    Gamification_badge_user create(@RequestBody Gamification_badge_user gamification_badge_user);

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users/batch")
    Boolean createBatch(@RequestBody List<Gamification_badge_user> gamification_badge_users);



    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users/search")
    Page<Gamification_badge_user> search(@RequestBody Gamification_badge_userSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badge_users/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badge_users/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_badge_users/select")
    Page<Gamification_badge_user> select();


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_badge_users/getdraft")
    Gamification_badge_user getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users/checkkey")
    Boolean checkKey(@RequestBody Gamification_badge_user gamification_badge_user);


    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users/save")
    Boolean save(@RequestBody Gamification_badge_user gamification_badge_user);

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users/savebatch")
    Boolean saveBatch(@RequestBody List<Gamification_badge_user> gamification_badge_users);



    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users/searchdefault")
    Page<Gamification_badge_user> searchDefault(@RequestBody Gamification_badge_userSearchContext context);


}
