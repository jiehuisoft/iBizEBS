package cn.ibizlab.businesscentral.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_list_contact_relSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_list_contact_relService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mail.mapper.Mail_mass_mailing_list_contact_relMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[群发邮件订阅信息] 服务对象接口实现
 */
@Slf4j
@Service("Mail_mass_mailing_list_contact_relServiceImpl")
public class Mail_mass_mailing_list_contact_relServiceImpl extends EBSServiceImpl<Mail_mass_mailing_list_contact_relMapper, Mail_mass_mailing_list_contact_rel> implements IMail_mass_mailing_list_contact_relService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_contactService mailMassMailingContactService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_listService mailMassMailingListService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mail.mass.mailing.list.contact.rel" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mail_mass_mailing_list_contact_rel et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_mass_mailing_list_contact_relService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mail_mass_mailing_list_contact_rel> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mail_mass_mailing_list_contact_rel et) {
        Mail_mass_mailing_list_contact_rel old = new Mail_mass_mailing_list_contact_rel() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_mass_mailing_list_contact_relService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_mass_mailing_list_contact_relService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mail_mass_mailing_list_contact_rel> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mail_mass_mailing_list_contact_rel get(Long key) {
        Mail_mass_mailing_list_contact_rel et = getById(key);
        if(et==null){
            et=new Mail_mass_mailing_list_contact_rel();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mail_mass_mailing_list_contact_rel getDraft(Mail_mass_mailing_list_contact_rel et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mail_mass_mailing_list_contact_rel et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mail_mass_mailing_list_contact_rel et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mail_mass_mailing_list_contact_rel et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mail_mass_mailing_list_contact_rel> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mail_mass_mailing_list_contact_rel> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mail_mass_mailing_list_contact_rel> selectByContactId(Long id) {
        return baseMapper.selectByContactId(id);
    }
    @Override
    public void removeByContactId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Mail_mass_mailing_list_contact_rel>().in("contact_id",ids));
    }

    @Override
    public void removeByContactId(Long id) {
        this.remove(new QueryWrapper<Mail_mass_mailing_list_contact_rel>().eq("contact_id",id));
    }

	@Override
    public List<Mail_mass_mailing_list_contact_rel> selectByListId(Long id) {
        return baseMapper.selectByListId(id);
    }
    @Override
    public void removeByListId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Mail_mass_mailing_list_contact_rel>().in("list_id",ids));
    }

    @Override
    public void removeByListId(Long id) {
        this.remove(new QueryWrapper<Mail_mass_mailing_list_contact_rel>().eq("list_id",id));
    }

	@Override
    public List<Mail_mass_mailing_list_contact_rel> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mail_mass_mailing_list_contact_rel>().eq("create_uid",id));
    }

	@Override
    public List<Mail_mass_mailing_list_contact_rel> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mail_mass_mailing_list_contact_rel>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mail_mass_mailing_list_contact_rel> searchDefault(Mail_mass_mailing_list_contact_relSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mail_mass_mailing_list_contact_rel> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mail_mass_mailing_list_contact_rel>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mail_mass_mailing_list_contact_rel et){
        //实体关系[DER1N_MAIL_MASS_MAILING_LIST_CONTACT_REL__MAIL_MASS_MAILING_CONTACT__CONTACT_ID]
        if(!ObjectUtils.isEmpty(et.getContactId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_contact odooContact=et.getOdooContact();
            if(ObjectUtils.isEmpty(odooContact)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_contact majorEntity=mailMassMailingContactService.get(et.getContactId());
                et.setOdooContact(majorEntity);
                odooContact=majorEntity;
            }
            et.setMessageBounce(odooContact.getMessageBounce());
            et.setIsBlacklisted(odooContact.getIsBlacklisted());
            et.setContactIdText(odooContact.getName());
        }
        //实体关系[DER1N_MAIL_MASS_MAILING_LIST_CONTACT_REL__MAIL_MASS_MAILING_LIST__LIST_ID]
        if(!ObjectUtils.isEmpty(et.getListId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_list odooList=et.getOdooList();
            if(ObjectUtils.isEmpty(odooList)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_list majorEntity=mailMassMailingListService.get(et.getListId());
                et.setOdooList(majorEntity);
                odooList=majorEntity;
            }
            et.setListIdText(odooList.getName());
            et.setContactCount(odooList.getContactNbr());
        }
        //实体关系[DER1N_MAIL_MASS_MAILING_LIST_CONTACT_REL__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MAIL_MASS_MAILING_LIST_CONTACT_REL__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mail_mass_mailing_list_contact_rel> getMailMassMailingListContactRelByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mail_mass_mailing_list_contact_rel> getMailMassMailingListContactRelByEntities(List<Mail_mass_mailing_list_contact_rel> entities) {
        List ids =new ArrayList();
        for(Mail_mass_mailing_list_contact_rel entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



