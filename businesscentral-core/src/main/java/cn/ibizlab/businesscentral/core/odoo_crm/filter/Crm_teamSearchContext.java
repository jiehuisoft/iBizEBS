package cn.ibizlab.businesscentral.core.odoo_crm.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team;
/**
 * 关系型数据实体[Crm_team] 查询条件对象
 */
@Slf4j
@Data
public class Crm_teamSearchContext extends QueryWrapperContext<Crm_team> {

	private String n_dashboard_graph_type_eq;//[类型]
	public void setN_dashboard_graph_type_eq(String n_dashboard_graph_type_eq) {
        this.n_dashboard_graph_type_eq = n_dashboard_graph_type_eq;
        if(!ObjectUtils.isEmpty(this.n_dashboard_graph_type_eq)){
            this.getSearchCond().eq("dashboard_graph_type", n_dashboard_graph_type_eq);
        }
    }
	private String n_team_type_eq;//[团队类型]
	public void setN_team_type_eq(String n_team_type_eq) {
        this.n_team_type_eq = n_team_type_eq;
        if(!ObjectUtils.isEmpty(this.n_team_type_eq)){
            this.getSearchCond().eq("team_type", n_team_type_eq);
        }
    }
	private String n_dashboard_graph_model_eq;//[内容]
	public void setN_dashboard_graph_model_eq(String n_dashboard_graph_model_eq) {
        this.n_dashboard_graph_model_eq = n_dashboard_graph_model_eq;
        if(!ObjectUtils.isEmpty(this.n_dashboard_graph_model_eq)){
            this.getSearchCond().eq("dashboard_graph_model", n_dashboard_graph_model_eq);
        }
    }
	private String n_dashboard_graph_period_pipeline_eq;//[预计关闭]
	public void setN_dashboard_graph_period_pipeline_eq(String n_dashboard_graph_period_pipeline_eq) {
        this.n_dashboard_graph_period_pipeline_eq = n_dashboard_graph_period_pipeline_eq;
        if(!ObjectUtils.isEmpty(this.n_dashboard_graph_period_pipeline_eq)){
            this.getSearchCond().eq("dashboard_graph_period_pipeline", n_dashboard_graph_period_pipeline_eq);
        }
    }
	private String n_dashboard_graph_group_pos_eq;//[POS分组]
	public void setN_dashboard_graph_group_pos_eq(String n_dashboard_graph_group_pos_eq) {
        this.n_dashboard_graph_group_pos_eq = n_dashboard_graph_group_pos_eq;
        if(!ObjectUtils.isEmpty(this.n_dashboard_graph_group_pos_eq)){
            this.getSearchCond().eq("dashboard_graph_group_pos", n_dashboard_graph_group_pos_eq);
        }
    }
	private String n_dashboard_graph_group_eq;//[分组]
	public void setN_dashboard_graph_group_eq(String n_dashboard_graph_group_eq) {
        this.n_dashboard_graph_group_eq = n_dashboard_graph_group_eq;
        if(!ObjectUtils.isEmpty(this.n_dashboard_graph_group_eq)){
            this.getSearchCond().eq("dashboard_graph_group", n_dashboard_graph_group_eq);
        }
    }
	private String n_dashboard_graph_period_eq;//[比例]
	public void setN_dashboard_graph_period_eq(String n_dashboard_graph_period_eq) {
        this.n_dashboard_graph_period_eq = n_dashboard_graph_period_eq;
        if(!ObjectUtils.isEmpty(this.n_dashboard_graph_period_eq)){
            this.getSearchCond().eq("dashboard_graph_period", n_dashboard_graph_period_eq);
        }
    }
	private String n_dashboard_graph_group_pipeline_eq;//[分组方式]
	public void setN_dashboard_graph_group_pipeline_eq(String n_dashboard_graph_group_pipeline_eq) {
        this.n_dashboard_graph_group_pipeline_eq = n_dashboard_graph_group_pipeline_eq;
        if(!ObjectUtils.isEmpty(this.n_dashboard_graph_group_pipeline_eq)){
            this.getSearchCond().eq("dashboard_graph_group_pipeline", n_dashboard_graph_group_pipeline_eq);
        }
    }
	private String n_name_like;//[销售团队]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_user_id_text_eq;//[团队负责人]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[团队负责人]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_alias_id_eq;//[别名]
	public void setN_alias_id_eq(Long n_alias_id_eq) {
        this.n_alias_id_eq = n_alias_id_eq;
        if(!ObjectUtils.isEmpty(this.n_alias_id_eq)){
            this.getSearchCond().eq("alias_id", n_alias_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_user_id_eq;//[团队负责人]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



