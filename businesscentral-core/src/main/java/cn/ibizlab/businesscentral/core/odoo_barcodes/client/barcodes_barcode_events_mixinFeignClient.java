package cn.ibizlab.businesscentral.core.odoo_barcodes.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin;
import cn.ibizlab.businesscentral.core.odoo_barcodes.filter.Barcodes_barcode_events_mixinSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[barcodes_barcode_events_mixin] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-barcodes:odoo-barcodes}", contextId = "barcodes-barcode-events-mixin", fallback = barcodes_barcode_events_mixinFallback.class)
public interface barcodes_barcode_events_mixinFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins")
    Barcodes_barcode_events_mixin create(@RequestBody Barcodes_barcode_events_mixin barcodes_barcode_events_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins/batch")
    Boolean createBatch(@RequestBody List<Barcodes_barcode_events_mixin> barcodes_barcode_events_mixins);


    @RequestMapping(method = RequestMethod.PUT, value = "/barcodes_barcode_events_mixins/{id}")
    Barcodes_barcode_events_mixin update(@PathVariable("id") Long id,@RequestBody Barcodes_barcode_events_mixin barcodes_barcode_events_mixin);

    @RequestMapping(method = RequestMethod.PUT, value = "/barcodes_barcode_events_mixins/batch")
    Boolean updateBatch(@RequestBody List<Barcodes_barcode_events_mixin> barcodes_barcode_events_mixins);


    @RequestMapping(method = RequestMethod.DELETE, value = "/barcodes_barcode_events_mixins/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/barcodes_barcode_events_mixins/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins/search")
    Page<Barcodes_barcode_events_mixin> search(@RequestBody Barcodes_barcode_events_mixinSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/barcodes_barcode_events_mixins/{id}")
    Barcodes_barcode_events_mixin get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.GET, value = "/barcodes_barcode_events_mixins/select")
    Page<Barcodes_barcode_events_mixin> select();


    @RequestMapping(method = RequestMethod.GET, value = "/barcodes_barcode_events_mixins/getdraft")
    Barcodes_barcode_events_mixin getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins/checkkey")
    Boolean checkKey(@RequestBody Barcodes_barcode_events_mixin barcodes_barcode_events_mixin);


    @RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins/save")
    Boolean save(@RequestBody Barcodes_barcode_events_mixin barcodes_barcode_events_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins/savebatch")
    Boolean saveBatch(@RequestBody List<Barcodes_barcode_events_mixin> barcodes_barcode_events_mixins);



    @RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins/searchdefault")
    Page<Barcodes_barcode_events_mixin> searchDefault(@RequestBody Barcodes_barcode_events_mixinSearchContext context);


}
