package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_product_produce;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_product_produceSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mrp_product_produce] 服务对象接口
 */
@Component
public class mrp_product_produceFallback implements mrp_product_produceFeignClient{


    public Page<Mrp_product_produce> search(Mrp_product_produceSearchContext context){
            return null;
     }




    public Mrp_product_produce get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Mrp_product_produce create(Mrp_product_produce mrp_product_produce){
            return null;
     }
    public Boolean createBatch(List<Mrp_product_produce> mrp_product_produces){
            return false;
     }

    public Mrp_product_produce update(Long id, Mrp_product_produce mrp_product_produce){
            return null;
     }
    public Boolean updateBatch(List<Mrp_product_produce> mrp_product_produces){
            return false;
     }


    public Page<Mrp_product_produce> select(){
            return null;
     }

    public Mrp_product_produce getDraft(){
            return null;
    }



    public Boolean checkKey(Mrp_product_produce mrp_product_produce){
            return false;
     }


    public Boolean save(Mrp_product_produce mrp_product_produce){
            return false;
     }
    public Boolean saveBatch(List<Mrp_product_produce> mrp_product_produces){
            return false;
     }

    public Page<Mrp_product_produce> searchDefault(Mrp_product_produceSearchContext context){
            return null;
     }


}
