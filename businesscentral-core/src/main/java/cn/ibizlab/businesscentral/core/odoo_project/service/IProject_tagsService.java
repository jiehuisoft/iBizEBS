package cn.ibizlab.businesscentral.core.odoo_project.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_tags;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_tagsSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Project_tags] 服务对象接口
 */
public interface IProject_tagsService extends IService<Project_tags>{

    boolean create(Project_tags et) ;
    void createBatch(List<Project_tags> list) ;
    boolean update(Project_tags et) ;
    void updateBatch(List<Project_tags> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Project_tags get(Long key) ;
    Project_tags getDraft(Project_tags et) ;
    boolean checkKey(Project_tags et) ;
    boolean save(Project_tags et) ;
    void saveBatch(List<Project_tags> list) ;
    Page<Project_tags> searchDefault(Project_tagsSearchContext context) ;
    List<Project_tags> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Project_tags> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Project_tags> getProjectTagsByIds(List<Long> ids) ;
    List<Project_tags> getProjectTagsByEntities(List<Project_tags> entities) ;
}


