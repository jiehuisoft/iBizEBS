package cn.ibizlab.businesscentral.core.odoo_ir.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[附件]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "IR_ATTACHMENT",resultMap = "Ir_attachmentResultMap")
public class Ir_attachment extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * res_field
     */
    @DEField(name = "res_field")
    @TableField(value = "res_field")
    @JSONField(name = "res_field")
    @JsonProperty("res_field")
    private String resField;
    /**
     * file_size
     */
    @DEField(name = "file_size")
    @TableField(value = "file_size")
    @JSONField(name = "file_size")
    @JsonProperty("file_size")
    private Integer fileSize;
    /**
     * store_fname
     */
    @DEField(name = "store_fname")
    @TableField(value = "store_fname")
    @JSONField(name = "store_fname")
    @JsonProperty("store_fname")
    private String storeFname;
    /**
     * public
     */
    @DEField(name = "public")
    @TableField(value = "public")
    @JSONField(name = "ibizpublic")
    @JsonProperty("ibizpublic")
    private Boolean ibizpublic;
    /**
     * type
     */
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * res_id
     */
    @DEField(name = "res_id")
    @TableField(value = "res_id")
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;
    /**
     * checksum
     */
    @TableField(value = "checksum")
    @JSONField(name = "checksum")
    @JsonProperty("checksum")
    private String checksum;
    /**
     * res_model
     */
    @DEField(name = "res_model")
    @TableField(value = "res_model")
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    private String resModel;
    /**
     * description
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * access_token
     */
    @DEField(name = "access_token")
    @TableField(value = "access_token")
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    private String accessToken;
    /**
     * url
     */
    @TableField(value = "url")
    @JSONField(name = "url")
    @JsonProperty("url")
    private String url;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * key
     */
    @TableField(value = "key")
    @JSONField(name = "key")
    @JsonProperty("key")
    private String key;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * mimetype
     */
    @TableField(value = "mimetype")
    @JSONField(name = "mimetype")
    @JsonProperty("mimetype")
    private String mimetype;
    /**
     * index_content
     */
    @DEField(name = "index_content")
    @TableField(value = "index_content")
    @JSONField(name = "index_content")
    @JsonProperty("index_content")
    private String indexContent;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;



    /**
     * 设置 [res_field]
     */
    public void setResField(String resField){
        this.resField = resField ;
        this.modify("res_field",resField);
    }

    /**
     * 设置 [file_size]
     */
    public void setFileSize(Integer fileSize){
        this.fileSize = fileSize ;
        this.modify("file_size",fileSize);
    }

    /**
     * 设置 [store_fname]
     */
    public void setStoreFname(String storeFname){
        this.storeFname = storeFname ;
        this.modify("store_fname",storeFname);
    }

    /**
     * 设置 [public]
     */
    public void setIbizpublic(Boolean ibizpublic){
        this.ibizpublic = ibizpublic ;
        this.modify("public",ibizpublic);
    }

    /**
     * 设置 [type]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [res_id]
     */
    public void setResId(Integer resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [checksum]
     */
    public void setChecksum(String checksum){
        this.checksum = checksum ;
        this.modify("checksum",checksum);
    }

    /**
     * 设置 [res_model]
     */
    public void setResModel(String resModel){
        this.resModel = resModel ;
        this.modify("res_model",resModel);
    }

    /**
     * 设置 [description]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [access_token]
     */
    public void setAccessToken(String accessToken){
        this.accessToken = accessToken ;
        this.modify("access_token",accessToken);
    }

    /**
     * 设置 [url]
     */
    public void setUrl(String url){
        this.url = url ;
        this.modify("url",url);
    }

    /**
     * 设置 [key]
     */
    public void setKey(String key){
        this.key = key ;
        this.modify("key",key);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [mimetype]
     */
    public void setMimetype(String mimetype){
        this.mimetype = mimetype ;
        this.modify("mimetype",mimetype);
    }

    /**
     * 设置 [index_content]
     */
    public void setIndexContent(String indexContent){
        this.indexContent = indexContent ;
        this.modify("index_content",indexContent);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


