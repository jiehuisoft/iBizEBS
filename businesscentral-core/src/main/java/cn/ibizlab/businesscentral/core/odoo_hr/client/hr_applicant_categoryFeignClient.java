package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_applicant_category;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_applicant_categorySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_applicant_category] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-hr:odoo-hr}", contextId = "hr-applicant-category", fallback = hr_applicant_categoryFallback.class)
public interface hr_applicant_categoryFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/hr_applicant_categories/search")
    Page<Hr_applicant_category> search(@RequestBody Hr_applicant_categorySearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_applicant_categories/{id}")
    Hr_applicant_category get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_applicant_categories")
    Hr_applicant_category create(@RequestBody Hr_applicant_category hr_applicant_category);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_applicant_categories/batch")
    Boolean createBatch(@RequestBody List<Hr_applicant_category> hr_applicant_categories);



    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_applicant_categories/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_applicant_categories/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/hr_applicant_categories/{id}")
    Hr_applicant_category update(@PathVariable("id") Long id,@RequestBody Hr_applicant_category hr_applicant_category);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_applicant_categories/batch")
    Boolean updateBatch(@RequestBody List<Hr_applicant_category> hr_applicant_categories);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_applicant_categories/select")
    Page<Hr_applicant_category> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_applicant_categories/getdraft")
    Hr_applicant_category getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/hr_applicant_categories/checkkey")
    Boolean checkKey(@RequestBody Hr_applicant_category hr_applicant_category);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_applicant_categories/save")
    Boolean save(@RequestBody Hr_applicant_category hr_applicant_category);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_applicant_categories/savebatch")
    Boolean saveBatch(@RequestBody List<Hr_applicant_category> hr_applicant_categories);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_applicant_categories/searchdefault")
    Page<Hr_applicant_category> searchDefault(@RequestBody Hr_applicant_categorySearchContext context);


}
