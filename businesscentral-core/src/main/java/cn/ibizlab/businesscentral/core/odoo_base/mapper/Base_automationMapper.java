package cn.ibizlab.businesscentral.core.odoo_base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_automation;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_automationSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Base_automationMapper extends BaseMapper<Base_automation>{

    Page<Base_automation> searchDefault(IPage page, @Param("srf") Base_automationSearchContext context, @Param("ew") Wrapper<Base_automation> wrapper) ;
    @Override
    Base_automation selectById(Serializable id);
    @Override
    int insert(Base_automation entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Base_automation entity);
    @Override
    int update(@Param(Constants.ENTITY) Base_automation entity, @Param("ew") Wrapper<Base_automation> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Base_automation> selectByTrgDateCalendarId(@Param("id") Serializable id) ;

    List<Base_automation> selectByCreateUid(@Param("id") Serializable id) ;

    List<Base_automation> selectByWriteUid(@Param("id") Serializable id) ;


}
