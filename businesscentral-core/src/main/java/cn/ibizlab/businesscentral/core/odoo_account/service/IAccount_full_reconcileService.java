package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_full_reconcile;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_full_reconcileSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_full_reconcile] 服务对象接口
 */
public interface IAccount_full_reconcileService extends IService<Account_full_reconcile>{

    boolean create(Account_full_reconcile et) ;
    void createBatch(List<Account_full_reconcile> list) ;
    boolean update(Account_full_reconcile et) ;
    void updateBatch(List<Account_full_reconcile> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_full_reconcile get(Long key) ;
    Account_full_reconcile getDraft(Account_full_reconcile et) ;
    boolean checkKey(Account_full_reconcile et) ;
    boolean save(Account_full_reconcile et) ;
    void saveBatch(List<Account_full_reconcile> list) ;
    Page<Account_full_reconcile> searchDefault(Account_full_reconcileSearchContext context) ;
    List<Account_full_reconcile> selectByExchangeMoveId(Long id);
    void resetByExchangeMoveId(Long id);
    void resetByExchangeMoveId(Collection<Long> ids);
    void removeByExchangeMoveId(Long id);
    List<Account_full_reconcile> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_full_reconcile> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_full_reconcile> getAccountFullReconcileByIds(List<Long> ids) ;
    List<Account_full_reconcile> getAccountFullReconcileByEntities(List<Account_full_reconcile> entities) ;
}


