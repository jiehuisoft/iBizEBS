package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_accountSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_account] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-account", fallback = account_accountFallback.class)
public interface account_accountFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_accounts/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_accounts/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/account_accounts/search")
    Page<Account_account> search(@RequestBody Account_accountSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/account_accounts/{id}")
    Account_account get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_accounts/{id}")
    Account_account update(@PathVariable("id") Long id,@RequestBody Account_account account_account);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_accounts/batch")
    Boolean updateBatch(@RequestBody List<Account_account> account_accounts);



    @RequestMapping(method = RequestMethod.POST, value = "/account_accounts")
    Account_account create(@RequestBody Account_account account_account);

    @RequestMapping(method = RequestMethod.POST, value = "/account_accounts/batch")
    Boolean createBatch(@RequestBody List<Account_account> account_accounts);



    @RequestMapping(method = RequestMethod.GET, value = "/account_accounts/select")
    Page<Account_account> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_accounts/getdraft")
    Account_account getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_accounts/checkkey")
    Boolean checkKey(@RequestBody Account_account account_account);


    @RequestMapping(method = RequestMethod.POST, value = "/account_accounts/save")
    Boolean save(@RequestBody Account_account account_account);

    @RequestMapping(method = RequestMethod.POST, value = "/account_accounts/savebatch")
    Boolean saveBatch(@RequestBody List<Account_account> account_accounts);



    @RequestMapping(method = RequestMethod.POST, value = "/account_accounts/searchdefault")
    Page<Account_account> searchDefault(@RequestBody Account_accountSearchContext context);


}
