package cn.ibizlab.businesscentral.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_product_produce_line;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_product_produce_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mrp_product_produce_line] 服务对象接口
 */
public interface IMrp_product_produce_lineService extends IService<Mrp_product_produce_line>{

    boolean create(Mrp_product_produce_line et) ;
    void createBatch(List<Mrp_product_produce_line> list) ;
    boolean update(Mrp_product_produce_line et) ;
    void updateBatch(List<Mrp_product_produce_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mrp_product_produce_line get(Long key) ;
    Mrp_product_produce_line getDraft(Mrp_product_produce_line et) ;
    boolean checkKey(Mrp_product_produce_line et) ;
    boolean save(Mrp_product_produce_line et) ;
    void saveBatch(List<Mrp_product_produce_line> list) ;
    Page<Mrp_product_produce_line> searchDefault(Mrp_product_produce_lineSearchContext context) ;
    List<Mrp_product_produce_line> selectByProductProduceId(Long id);
    void resetByProductProduceId(Long id);
    void resetByProductProduceId(Collection<Long> ids);
    void removeByProductProduceId(Long id);
    List<Mrp_product_produce_line> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Mrp_product_produce_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mrp_product_produce_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Mrp_product_produce_line> selectByMoveId(Long id);
    void resetByMoveId(Long id);
    void resetByMoveId(Collection<Long> ids);
    void removeByMoveId(Long id);
    List<Mrp_product_produce_line> selectByLotId(Long id);
    void resetByLotId(Long id);
    void resetByLotId(Collection<Long> ids);
    void removeByLotId(Long id);
    List<Mrp_product_produce_line> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mrp_product_produce_line> getMrpProductProduceLineByIds(List<Long> ids) ;
    List<Mrp_product_produce_line> getMrpProductProduceLineByEntities(List<Mrp_product_produce_line> entities) ;
}


