package cn.ibizlab.businesscentral.core.odoo_payment.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_token;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_tokenSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Payment_tokenMapper extends BaseMapper<Payment_token>{

    Page<Payment_token> searchDefault(IPage page, @Param("srf") Payment_tokenSearchContext context, @Param("ew") Wrapper<Payment_token> wrapper) ;
    @Override
    Payment_token selectById(Serializable id);
    @Override
    int insert(Payment_token entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Payment_token entity);
    @Override
    int update(@Param(Constants.ENTITY) Payment_token entity, @Param("ew") Wrapper<Payment_token> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Payment_token> selectByAcquirerId(@Param("id") Serializable id) ;

    List<Payment_token> selectByPartnerId(@Param("id") Serializable id) ;

    List<Payment_token> selectByCreateUid(@Param("id") Serializable id) ;

    List<Payment_token> selectByWriteUid(@Param("id") Serializable id) ;


}
