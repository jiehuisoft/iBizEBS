package cn.ibizlab.businesscentral.core.odoo_purchase.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Account_tax_purchase_order_line_rel;
/**
 * 关系型数据实体[Account_tax_purchase_order_line_rel] 查询条件对象
 */
@Slf4j
@Data
public class Account_tax_purchase_order_line_relSearchContext extends QueryWrapperContext<Account_tax_purchase_order_line_rel> {

	private Long n_account_tax_id_eq;//[ID]
	public void setN_account_tax_id_eq(Long n_account_tax_id_eq) {
        this.n_account_tax_id_eq = n_account_tax_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_tax_id_eq)){
            this.getSearchCond().eq("account_tax_id", n_account_tax_id_eq);
        }
    }
	private Long n_purchase_order_line_id_eq;//[ID]
	public void setN_purchase_order_line_id_eq(Long n_purchase_order_line_id_eq) {
        this.n_purchase_order_line_id_eq = n_purchase_order_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_purchase_order_line_id_eq)){
            this.getSearchCond().eq("purchase_order_line_id", n_purchase_order_line_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



