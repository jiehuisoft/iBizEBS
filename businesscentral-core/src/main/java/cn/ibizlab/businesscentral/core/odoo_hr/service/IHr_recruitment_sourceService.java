package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_source;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_recruitment_sourceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_recruitment_source] 服务对象接口
 */
public interface IHr_recruitment_sourceService extends IService<Hr_recruitment_source>{

    boolean create(Hr_recruitment_source et) ;
    void createBatch(List<Hr_recruitment_source> list) ;
    boolean update(Hr_recruitment_source et) ;
    void updateBatch(List<Hr_recruitment_source> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_recruitment_source get(Long key) ;
    Hr_recruitment_source getDraft(Hr_recruitment_source et) ;
    boolean checkKey(Hr_recruitment_source et) ;
    boolean save(Hr_recruitment_source et) ;
    void saveBatch(List<Hr_recruitment_source> list) ;
    Page<Hr_recruitment_source> searchDefault(Hr_recruitment_sourceSearchContext context) ;
    List<Hr_recruitment_source> selectByJobId(Long id);
    void resetByJobId(Long id);
    void resetByJobId(Collection<Long> ids);
    void removeByJobId(Long id);
    List<Hr_recruitment_source> selectByAliasId(Long id);
    void resetByAliasId(Long id);
    void resetByAliasId(Collection<Long> ids);
    void removeByAliasId(Long id);
    List<Hr_recruitment_source> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_recruitment_source> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Hr_recruitment_source> selectBySourceId(Long id);
    void removeBySourceId(Collection<Long> ids);
    void removeBySourceId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_recruitment_source> getHrRecruitmentSourceByIds(List<Long> ids) ;
    List<Hr_recruitment_source> getHrRecruitmentSourceByEntities(List<Hr_recruitment_source> entities) ;
}


