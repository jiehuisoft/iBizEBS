package cn.ibizlab.businesscentral.core.odoo_iap.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_iap.domain.Iap_account;
import cn.ibizlab.businesscentral.core.odoo_iap.filter.Iap_accountSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Iap_account] 服务对象接口
 */
public interface IIap_accountService extends IService<Iap_account>{

    boolean create(Iap_account et) ;
    void createBatch(List<Iap_account> list) ;
    boolean update(Iap_account et) ;
    void updateBatch(List<Iap_account> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Iap_account get(Long key) ;
    Iap_account getDraft(Iap_account et) ;
    boolean checkKey(Iap_account et) ;
    boolean save(Iap_account et) ;
    void saveBatch(List<Iap_account> list) ;
    Page<Iap_account> searchDefault(Iap_accountSearchContext context) ;
    List<Iap_account> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Iap_account> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Iap_account> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Iap_account> getIapAccountByIds(List<Long> ids) ;
    List<Iap_account> getIapAccountByEntities(List<Iap_account> entities) ;
}


