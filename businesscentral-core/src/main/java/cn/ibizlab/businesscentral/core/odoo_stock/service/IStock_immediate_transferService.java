package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_immediate_transfer;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_immediate_transferSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_immediate_transfer] 服务对象接口
 */
public interface IStock_immediate_transferService extends IService<Stock_immediate_transfer>{

    boolean create(Stock_immediate_transfer et) ;
    void createBatch(List<Stock_immediate_transfer> list) ;
    boolean update(Stock_immediate_transfer et) ;
    void updateBatch(List<Stock_immediate_transfer> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_immediate_transfer get(Long key) ;
    Stock_immediate_transfer getDraft(Stock_immediate_transfer et) ;
    boolean checkKey(Stock_immediate_transfer et) ;
    boolean save(Stock_immediate_transfer et) ;
    void saveBatch(List<Stock_immediate_transfer> list) ;
    Page<Stock_immediate_transfer> searchDefault(Stock_immediate_transferSearchContext context) ;
    List<Stock_immediate_transfer> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_immediate_transfer> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_immediate_transfer> getStockImmediateTransferByIds(List<Long> ids) ;
    List<Stock_immediate_transfer> getStockImmediateTransferByEntities(List<Stock_immediate_transfer> entities) ;
}


