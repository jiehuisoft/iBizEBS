package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_aliasSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_alias] 服务对象接口
 */
@Component
public class mail_aliasFallback implements mail_aliasFeignClient{


    public Mail_alias update(Long id, Mail_alias mail_alias){
            return null;
     }
    public Boolean updateBatch(List<Mail_alias> mail_aliases){
            return false;
     }



    public Mail_alias get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Mail_alias create(Mail_alias mail_alias){
            return null;
     }
    public Boolean createBatch(List<Mail_alias> mail_aliases){
            return false;
     }


    public Page<Mail_alias> search(Mail_aliasSearchContext context){
            return null;
     }


    public Page<Mail_alias> select(){
            return null;
     }

    public Mail_alias getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_alias mail_alias){
            return false;
     }


    public Boolean save(Mail_alias mail_alias){
            return false;
     }
    public Boolean saveBatch(List<Mail_alias> mail_aliases){
            return false;
     }

    public Page<Mail_alias> searchDefault(Mail_aliasSearchContext context){
            return null;
     }


}
