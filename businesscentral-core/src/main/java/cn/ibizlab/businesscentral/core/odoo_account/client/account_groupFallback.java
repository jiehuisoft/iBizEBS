package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_group;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_groupSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_group] 服务对象接口
 */
@Component
public class account_groupFallback implements account_groupFeignClient{


    public Page<Account_group> search(Account_groupSearchContext context){
            return null;
     }


    public Account_group update(Long id, Account_group account_group){
            return null;
     }
    public Boolean updateBatch(List<Account_group> account_groups){
            return false;
     }


    public Account_group create(Account_group account_group){
            return null;
     }
    public Boolean createBatch(List<Account_group> account_groups){
            return false;
     }


    public Account_group get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Account_group> select(){
            return null;
     }

    public Account_group getDraft(){
            return null;
    }



    public Boolean checkKey(Account_group account_group){
            return false;
     }


    public Boolean save(Account_group account_group){
            return false;
     }
    public Boolean saveBatch(List<Account_group> account_groups){
            return false;
     }

    public Page<Account_group> searchDefault(Account_groupSearchContext context){
            return null;
     }


}
