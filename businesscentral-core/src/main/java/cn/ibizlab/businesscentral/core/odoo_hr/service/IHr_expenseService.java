package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expenseSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_expense] 服务对象接口
 */
public interface IHr_expenseService extends IService<Hr_expense>{

    boolean create(Hr_expense et) ;
    void createBatch(List<Hr_expense> list) ;
    boolean update(Hr_expense et) ;
    void updateBatch(List<Hr_expense> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_expense get(Long key) ;
    Hr_expense getDraft(Hr_expense et) ;
    boolean checkKey(Hr_expense et) ;
    boolean save(Hr_expense et) ;
    void saveBatch(List<Hr_expense> list) ;
    Page<Hr_expense> searchDefault(Hr_expenseSearchContext context) ;
    List<Hr_expense> selectByAccountId(Long id);
    void resetByAccountId(Long id);
    void resetByAccountId(Collection<Long> ids);
    void removeByAccountId(Long id);
    List<Hr_expense> selectByAnalyticAccountId(Long id);
    void resetByAnalyticAccountId(Long id);
    void resetByAnalyticAccountId(Collection<Long> ids);
    void removeByAnalyticAccountId(Long id);
    List<Hr_expense> selectByEmployeeId(Long id);
    void resetByEmployeeId(Long id);
    void resetByEmployeeId(Collection<Long> ids);
    void removeByEmployeeId(Long id);
    List<Hr_expense> selectBySheetId(Long id);
    void resetBySheetId(Long id);
    void resetBySheetId(Collection<Long> ids);
    void removeBySheetId(Long id);
    List<Hr_expense> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Hr_expense> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Hr_expense> selectByCompanyCurrencyId(Long id);
    void resetByCompanyCurrencyId(Long id);
    void resetByCompanyCurrencyId(Collection<Long> ids);
    void removeByCompanyCurrencyId(Long id);
    List<Hr_expense> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Hr_expense> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_expense> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Hr_expense> selectBySaleOrderId(Long id);
    void resetBySaleOrderId(Long id);
    void resetBySaleOrderId(Collection<Long> ids);
    void removeBySaleOrderId(Long id);
    List<Hr_expense> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_expense> getHrExpenseByIds(List<Long> ids) ;
    List<Hr_expense> getHrExpenseByEntities(List<Hr_expense> entities) ;
}


