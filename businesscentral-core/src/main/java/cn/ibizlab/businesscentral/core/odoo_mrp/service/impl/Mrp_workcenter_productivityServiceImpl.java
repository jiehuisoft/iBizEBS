package cn.ibizlab.businesscentral.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenter_productivitySearchContext;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenter_productivityService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mrp.mapper.Mrp_workcenter_productivityMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[工作中心生产力日志] 服务对象接口实现
 */
@Slf4j
@Service("Mrp_workcenter_productivityServiceImpl")
public class Mrp_workcenter_productivityServiceImpl extends EBSServiceImpl<Mrp_workcenter_productivityMapper, Mrp_workcenter_productivity> implements IMrp_workcenter_productivityService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenter_productivity_lossService mrpWorkcenterProductivityLossService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenterService mrpWorkcenterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workorderService mrpWorkorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mrp.workcenter.productivity" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mrp_workcenter_productivity et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_workcenter_productivityService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mrp_workcenter_productivity> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mrp_workcenter_productivity et) {
        Mrp_workcenter_productivity old = new Mrp_workcenter_productivity() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_workcenter_productivityService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_workcenter_productivityService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mrp_workcenter_productivity> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mrp_workcenter_productivity get(Long key) {
        Mrp_workcenter_productivity et = getById(key);
        if(et==null){
            et=new Mrp_workcenter_productivity();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mrp_workcenter_productivity getDraft(Mrp_workcenter_productivity et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mrp_workcenter_productivity et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mrp_workcenter_productivity et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mrp_workcenter_productivity et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mrp_workcenter_productivity> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mrp_workcenter_productivity> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mrp_workcenter_productivity> selectByLossId(Long id) {
        return baseMapper.selectByLossId(id);
    }
    @Override
    public List<Mrp_workcenter_productivity> selectByLossId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Mrp_workcenter_productivity>().in("id",ids));
    }

    @Override
    public void removeByLossId(Long id) {
        this.remove(new QueryWrapper<Mrp_workcenter_productivity>().eq("loss_id",id));
    }

	@Override
    public List<Mrp_workcenter_productivity> selectByWorkcenterId(Long id) {
        return baseMapper.selectByWorkcenterId(id);
    }
    @Override
    public void resetByWorkcenterId(Long id) {
        this.update(new UpdateWrapper<Mrp_workcenter_productivity>().set("workcenter_id",null).eq("workcenter_id",id));
    }

    @Override
    public void resetByWorkcenterId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_workcenter_productivity>().set("workcenter_id",null).in("workcenter_id",ids));
    }

    @Override
    public void removeByWorkcenterId(Long id) {
        this.remove(new QueryWrapper<Mrp_workcenter_productivity>().eq("workcenter_id",id));
    }

	@Override
    public List<Mrp_workcenter_productivity> selectByWorkorderId(Long id) {
        return baseMapper.selectByWorkorderId(id);
    }
    @Override
    public void resetByWorkorderId(Long id) {
        this.update(new UpdateWrapper<Mrp_workcenter_productivity>().set("workorder_id",null).eq("workorder_id",id));
    }

    @Override
    public void resetByWorkorderId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_workcenter_productivity>().set("workorder_id",null).in("workorder_id",ids));
    }

    @Override
    public void removeByWorkorderId(Long id) {
        this.remove(new QueryWrapper<Mrp_workcenter_productivity>().eq("workorder_id",id));
    }

	@Override
    public List<Mrp_workcenter_productivity> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mrp_workcenter_productivity>().eq("create_uid",id));
    }

	@Override
    public List<Mrp_workcenter_productivity> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Mrp_workcenter_productivity>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_workcenter_productivity>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Mrp_workcenter_productivity>().eq("user_id",id));
    }

	@Override
    public List<Mrp_workcenter_productivity> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mrp_workcenter_productivity>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mrp_workcenter_productivity> searchDefault(Mrp_workcenter_productivitySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mrp_workcenter_productivity> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mrp_workcenter_productivity>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mrp_workcenter_productivity et){
        //实体关系[DER1N_MRP_WORKCENTER_PRODUCTIVITY__MRP_WORKCENTER_PRODUCTIVITY_LOSS__LOSS_ID]
        if(!ObjectUtils.isEmpty(et.getLossId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss odooLoss=et.getOdooLoss();
            if(ObjectUtils.isEmpty(odooLoss)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss majorEntity=mrpWorkcenterProductivityLossService.get(et.getLossId());
                et.setOdooLoss(majorEntity);
                odooLoss=majorEntity;
            }
            et.setLossType(odooLoss.getLossType());
            et.setLossIdText(odooLoss.getName());
        }
        //实体关系[DER1N_MRP_WORKCENTER_PRODUCTIVITY__MRP_WORKCENTER__WORKCENTER_ID]
        if(!ObjectUtils.isEmpty(et.getWorkcenterId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter odooWorkcenter=et.getOdooWorkcenter();
            if(ObjectUtils.isEmpty(odooWorkcenter)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter majorEntity=mrpWorkcenterService.get(et.getWorkcenterId());
                et.setOdooWorkcenter(majorEntity);
                odooWorkcenter=majorEntity;
            }
            et.setWorkcenterIdText(odooWorkcenter.getName());
        }
        //实体关系[DER1N_MRP_WORKCENTER_PRODUCTIVITY__MRP_WORKORDER__WORKORDER_ID]
        if(!ObjectUtils.isEmpty(et.getWorkorderId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder odooWorkorder=et.getOdooWorkorder();
            if(ObjectUtils.isEmpty(odooWorkorder)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder majorEntity=mrpWorkorderService.get(et.getWorkorderId());
                et.setOdooWorkorder(majorEntity);
                odooWorkorder=majorEntity;
            }
            et.setWorkorderIdText(odooWorkorder.getName());
            et.setProductionId(odooWorkorder.getProductionId());
        }
        //实体关系[DER1N_MRP_WORKCENTER_PRODUCTIVITY__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MRP_WORKCENTER_PRODUCTIVITY__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_MRP_WORKCENTER_PRODUCTIVITY__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mrp_workcenter_productivity> getMrpWorkcenterProductivityByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mrp_workcenter_productivity> getMrpWorkcenterProductivityByEntities(List<Mrp_workcenter_productivity> entities) {
        List ids =new ArrayList();
        for(Mrp_workcenter_productivity entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



