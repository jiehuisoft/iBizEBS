package cn.ibizlab.businesscentral.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_channelSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_channelService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mail.mapper.Mail_channelMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[讨论频道] 服务对象接口实现
 */
@Slf4j
@Service("Mail_channelServiceImpl")
public class Mail_channelServiceImpl extends EBSServiceImpl<Mail_channelMapper, Mail_channel> implements IMail_channelService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_challengeService gamificationChallengeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_report_channelService imLivechatReportChannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_report_operatorService imLivechatReportOperatorService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_channel_partnerService mailChannelPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_followersService mailFollowersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_moderationService mailModerationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_channelService imLivechatChannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_aliasService mailAliasService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_groupsService resGroupsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mail.channel" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mail_channel et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_channelService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mail_channel> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mail_channel et) {
        Mail_channel old = new Mail_channel() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_channelService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_channelService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mail_channel> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        gamificationChallengeService.resetByReportMessageGroupId(key);
        mailChannelPartnerService.removeByChannelId(key);
        mailFollowersService.removeByChannelId(key);
        mailModerationService.resetByChannelId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        gamificationChallengeService.resetByReportMessageGroupId(idList);
        mailChannelPartnerService.removeByChannelId(idList);
        mailFollowersService.removeByChannelId(idList);
        mailModerationService.resetByChannelId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mail_channel get(Long key) {
        Mail_channel et = getById(key);
        if(et==null){
            et=new Mail_channel();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mail_channel getDraft(Mail_channel et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mail_channel et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mail_channel et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mail_channel et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mail_channel> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mail_channel> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mail_channel> selectByLivechatChannelId(Long id) {
        return baseMapper.selectByLivechatChannelId(id);
    }
    @Override
    public void resetByLivechatChannelId(Long id) {
        this.update(new UpdateWrapper<Mail_channel>().set("livechat_channel_id",null).eq("livechat_channel_id",id));
    }

    @Override
    public void resetByLivechatChannelId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_channel>().set("livechat_channel_id",null).in("livechat_channel_id",ids));
    }

    @Override
    public void removeByLivechatChannelId(Long id) {
        this.remove(new QueryWrapper<Mail_channel>().eq("livechat_channel_id",id));
    }

	@Override
    public List<Mail_channel> selectByAliasId(Long id) {
        return baseMapper.selectByAliasId(id);
    }
    @Override
    public List<Mail_channel> selectByAliasId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Mail_channel>().in("id",ids));
    }

    @Override
    public void removeByAliasId(Long id) {
        this.remove(new QueryWrapper<Mail_channel>().eq("alias_id",id));
    }

	@Override
    public List<Mail_channel> selectByGroupPublicId(Long id) {
        return baseMapper.selectByGroupPublicId(id);
    }
    @Override
    public void resetByGroupPublicId(Long id) {
        this.update(new UpdateWrapper<Mail_channel>().set("group_public_id",null).eq("group_public_id",id));
    }

    @Override
    public void resetByGroupPublicId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_channel>().set("group_public_id",null).in("group_public_id",ids));
    }

    @Override
    public void removeByGroupPublicId(Long id) {
        this.remove(new QueryWrapper<Mail_channel>().eq("group_public_id",id));
    }

	@Override
    public List<Mail_channel> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mail_channel>().eq("create_uid",id));
    }

	@Override
    public List<Mail_channel> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mail_channel>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mail_channel> searchDefault(Mail_channelSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mail_channel> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mail_channel>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mail_channel et){
        //实体关系[DER1N_MAIL_CHANNEL__IM_LIVECHAT_CHANNEL__LIVECHAT_CHANNEL_ID]
        if(!ObjectUtils.isEmpty(et.getLivechatChannelId())){
            cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel odooLivechatChannel=et.getOdooLivechatChannel();
            if(ObjectUtils.isEmpty(odooLivechatChannel)){
                cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel majorEntity=imLivechatChannelService.get(et.getLivechatChannelId());
                et.setOdooLivechatChannel(majorEntity);
                odooLivechatChannel=majorEntity;
            }
            et.setLivechatChannelIdText(odooLivechatChannel.getName());
        }
        //实体关系[DER1N_MAIL_CHANNEL__MAIL_ALIAS__ALIAS_ID]
        if(!ObjectUtils.isEmpty(et.getAliasId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias odooAlias=et.getOdooAlias();
            if(ObjectUtils.isEmpty(odooAlias)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias majorEntity=mailAliasService.get(et.getAliasId());
                et.setOdooAlias(majorEntity);
                odooAlias=majorEntity;
            }
            et.setAliasForceThreadId(odooAlias.getAliasForceThreadId());
            et.setAliasDefaults(odooAlias.getAliasDefaults());
            et.setAliasUserId(odooAlias.getAliasUserId());
            et.setAliasParentModelId(odooAlias.getAliasParentModelId());
            et.setAliasParentThreadId(odooAlias.getAliasParentThreadId());
            et.setAliasName(odooAlias.getAliasName());
            et.setAliasModelId(odooAlias.getAliasModelId());
            et.setAliasDomain(odooAlias.getAliasDomain());
            et.setAliasContact(odooAlias.getAliasContact());
        }
        //实体关系[DER1N_MAIL_CHANNEL__RES_GROUPS__GROUP_PUBLIC_ID]
        if(!ObjectUtils.isEmpty(et.getGroupPublicId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_groups odooGroupPublic=et.getOdooGroupPublic();
            if(ObjectUtils.isEmpty(odooGroupPublic)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_groups majorEntity=resGroupsService.get(et.getGroupPublicId());
                et.setOdooGroupPublic(majorEntity);
                odooGroupPublic=majorEntity;
            }
            et.setGroupPublicIdText(odooGroupPublic.getName());
        }
        //实体关系[DER1N_MAIL_CHANNEL__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MAIL_CHANNEL__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mail_channel> getMailChannelByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mail_channel> getMailChannelByEntities(List<Mail_channel> entities) {
        List ids =new ArrayList();
        for(Mail_channel entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



