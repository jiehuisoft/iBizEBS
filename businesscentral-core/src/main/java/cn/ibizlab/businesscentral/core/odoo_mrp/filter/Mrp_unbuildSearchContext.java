package cn.ibizlab.businesscentral.core.odoo_mrp.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild;
/**
 * 关系型数据实体[Mrp_unbuild] 查询条件对象
 */
@Slf4j
@Data
public class Mrp_unbuildSearchContext extends QueryWrapperContext<Mrp_unbuild> {

	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_name_like;//[参考]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_mo_id_text_eq;//[制造订单]
	public void setN_mo_id_text_eq(String n_mo_id_text_eq) {
        this.n_mo_id_text_eq = n_mo_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_mo_id_text_eq)){
            this.getSearchCond().eq("mo_id_text", n_mo_id_text_eq);
        }
    }
	private String n_mo_id_text_like;//[制造订单]
	public void setN_mo_id_text_like(String n_mo_id_text_like) {
        this.n_mo_id_text_like = n_mo_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_mo_id_text_like)){
            this.getSearchCond().like("mo_id_text", n_mo_id_text_like);
        }
    }
	private String n_location_id_text_eq;//[地点]
	public void setN_location_id_text_eq(String n_location_id_text_eq) {
        this.n_location_id_text_eq = n_location_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_location_id_text_eq)){
            this.getSearchCond().eq("location_id_text", n_location_id_text_eq);
        }
    }
	private String n_location_id_text_like;//[地点]
	public void setN_location_id_text_like(String n_location_id_text_like) {
        this.n_location_id_text_like = n_location_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_location_id_text_like)){
            this.getSearchCond().like("location_id_text", n_location_id_text_like);
        }
    }
	private String n_location_dest_id_text_eq;//[目的位置]
	public void setN_location_dest_id_text_eq(String n_location_dest_id_text_eq) {
        this.n_location_dest_id_text_eq = n_location_dest_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_location_dest_id_text_eq)){
            this.getSearchCond().eq("location_dest_id_text", n_location_dest_id_text_eq);
        }
    }
	private String n_location_dest_id_text_like;//[目的位置]
	public void setN_location_dest_id_text_like(String n_location_dest_id_text_like) {
        this.n_location_dest_id_text_like = n_location_dest_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_location_dest_id_text_like)){
            this.getSearchCond().like("location_dest_id_text", n_location_dest_id_text_like);
        }
    }
	private String n_product_uom_id_text_eq;//[计量单位]
	public void setN_product_uom_id_text_eq(String n_product_uom_id_text_eq) {
        this.n_product_uom_id_text_eq = n_product_uom_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_text_eq)){
            this.getSearchCond().eq("product_uom_id_text", n_product_uom_id_text_eq);
        }
    }
	private String n_product_uom_id_text_like;//[计量单位]
	public void setN_product_uom_id_text_like(String n_product_uom_id_text_like) {
        this.n_product_uom_id_text_like = n_product_uom_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_text_like)){
            this.getSearchCond().like("product_uom_id_text", n_product_uom_id_text_like);
        }
    }
	private String n_lot_id_text_eq;//[批次]
	public void setN_lot_id_text_eq(String n_lot_id_text_eq) {
        this.n_lot_id_text_eq = n_lot_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_lot_id_text_eq)){
            this.getSearchCond().eq("lot_id_text", n_lot_id_text_eq);
        }
    }
	private String n_lot_id_text_like;//[批次]
	public void setN_lot_id_text_like(String n_lot_id_text_like) {
        this.n_lot_id_text_like = n_lot_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_lot_id_text_like)){
            this.getSearchCond().like("lot_id_text", n_lot_id_text_like);
        }
    }
	private Long n_product_uom_id_eq;//[计量单位]
	public void setN_product_uom_id_eq(Long n_product_uom_id_eq) {
        this.n_product_uom_id_eq = n_product_uom_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_id_eq)){
            this.getSearchCond().eq("product_uom_id", n_product_uom_id_eq);
        }
    }
	private Long n_bom_id_eq;//[物料清单]
	public void setN_bom_id_eq(Long n_bom_id_eq) {
        this.n_bom_id_eq = n_bom_id_eq;
        if(!ObjectUtils.isEmpty(this.n_bom_id_eq)){
            this.getSearchCond().eq("bom_id", n_bom_id_eq);
        }
    }
	private Long n_mo_id_eq;//[制造订单]
	public void setN_mo_id_eq(Long n_mo_id_eq) {
        this.n_mo_id_eq = n_mo_id_eq;
        if(!ObjectUtils.isEmpty(this.n_mo_id_eq)){
            this.getSearchCond().eq("mo_id", n_mo_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_lot_id_eq;//[批次]
	public void setN_lot_id_eq(Long n_lot_id_eq) {
        this.n_lot_id_eq = n_lot_id_eq;
        if(!ObjectUtils.isEmpty(this.n_lot_id_eq)){
            this.getSearchCond().eq("lot_id", n_lot_id_eq);
        }
    }
	private Long n_location_id_eq;//[地点]
	public void setN_location_id_eq(Long n_location_id_eq) {
        this.n_location_id_eq = n_location_id_eq;
        if(!ObjectUtils.isEmpty(this.n_location_id_eq)){
            this.getSearchCond().eq("location_id", n_location_id_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_location_dest_id_eq;//[目的位置]
	public void setN_location_dest_id_eq(Long n_location_dest_id_eq) {
        this.n_location_dest_id_eq = n_location_dest_id_eq;
        if(!ObjectUtils.isEmpty(this.n_location_dest_id_eq)){
            this.getSearchCond().eq("location_dest_id", n_location_dest_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



