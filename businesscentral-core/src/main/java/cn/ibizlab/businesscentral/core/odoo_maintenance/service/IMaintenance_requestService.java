package cn.ibizlab.businesscentral.core.odoo_maintenance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_request;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_requestSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Maintenance_request] 服务对象接口
 */
public interface IMaintenance_requestService extends IService<Maintenance_request>{

    boolean create(Maintenance_request et) ;
    void createBatch(List<Maintenance_request> list) ;
    boolean update(Maintenance_request et) ;
    void updateBatch(List<Maintenance_request> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Maintenance_request get(Long key) ;
    Maintenance_request getDraft(Maintenance_request et) ;
    boolean checkKey(Maintenance_request et) ;
    boolean save(Maintenance_request et) ;
    void saveBatch(List<Maintenance_request> list) ;
    Page<Maintenance_request> searchDefault(Maintenance_requestSearchContext context) ;
    List<Maintenance_request> selectByDepartmentId(Long id);
    void resetByDepartmentId(Long id);
    void resetByDepartmentId(Collection<Long> ids);
    void removeByDepartmentId(Long id);
    List<Maintenance_request> selectByEmployeeId(Long id);
    void resetByEmployeeId(Long id);
    void resetByEmployeeId(Collection<Long> ids);
    void removeByEmployeeId(Long id);
    List<Maintenance_request> selectByCategoryId(Long id);
    void resetByCategoryId(Long id);
    void resetByCategoryId(Collection<Long> ids);
    void removeByCategoryId(Long id);
    List<Maintenance_request> selectByEquipmentId(Long id);
    List<Maintenance_request> selectByEquipmentId(Collection<Long> ids);
    void removeByEquipmentId(Long id);
    List<Maintenance_request> selectByStageId(Long id);
    List<Maintenance_request> selectByStageId(Collection<Long> ids);
    void removeByStageId(Long id);
    List<Maintenance_request> selectByMaintenanceTeamId(Long id);
    void resetByMaintenanceTeamId(Long id);
    void resetByMaintenanceTeamId(Collection<Long> ids);
    void removeByMaintenanceTeamId(Long id);
    List<Maintenance_request> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Maintenance_request> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Maintenance_request> selectByOwnerUserId(Long id);
    void resetByOwnerUserId(Long id);
    void resetByOwnerUserId(Collection<Long> ids);
    void removeByOwnerUserId(Long id);
    List<Maintenance_request> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Maintenance_request> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Maintenance_request> getMaintenanceRequestByIds(List<Long> ids) ;
    List<Maintenance_request> getMaintenanceRequestByEntities(List<Maintenance_request> entities) ;
}


