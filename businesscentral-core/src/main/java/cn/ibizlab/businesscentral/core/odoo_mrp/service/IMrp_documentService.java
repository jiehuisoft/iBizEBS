package cn.ibizlab.businesscentral.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_document;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_documentSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mrp_document] 服务对象接口
 */
public interface IMrp_documentService extends IService<Mrp_document>{

    boolean create(Mrp_document et) ;
    void createBatch(List<Mrp_document> list) ;
    boolean update(Mrp_document et) ;
    void updateBatch(List<Mrp_document> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mrp_document get(Long key) ;
    Mrp_document getDraft(Mrp_document et) ;
    boolean checkKey(Mrp_document et) ;
    boolean save(Mrp_document et) ;
    void saveBatch(List<Mrp_document> list) ;
    Page<Mrp_document> searchDefault(Mrp_documentSearchContext context) ;
    List<Mrp_document> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mrp_document> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mrp_document> getMrpDocumentByIds(List<Long> ids) ;
    List<Mrp_document> getMrpDocumentByEntities(List<Mrp_document> entities) ;
}


