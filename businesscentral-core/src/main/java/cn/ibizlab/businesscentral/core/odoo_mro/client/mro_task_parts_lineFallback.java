package cn.ibizlab.businesscentral.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_task_parts_line;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_task_parts_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_task_parts_line] 服务对象接口
 */
@Component
public class mro_task_parts_lineFallback implements mro_task_parts_lineFeignClient{

    public Page<Mro_task_parts_line> search(Mro_task_parts_lineSearchContext context){
            return null;
     }




    public Mro_task_parts_line create(Mro_task_parts_line mro_task_parts_line){
            return null;
     }
    public Boolean createBatch(List<Mro_task_parts_line> mro_task_parts_lines){
            return false;
     }

    public Mro_task_parts_line get(Long id){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Mro_task_parts_line update(Long id, Mro_task_parts_line mro_task_parts_line){
            return null;
     }
    public Boolean updateBatch(List<Mro_task_parts_line> mro_task_parts_lines){
            return false;
     }


    public Page<Mro_task_parts_line> select(){
            return null;
     }

    public Mro_task_parts_line getDraft(){
            return null;
    }



    public Boolean checkKey(Mro_task_parts_line mro_task_parts_line){
            return false;
     }


    public Boolean save(Mro_task_parts_line mro_task_parts_line){
            return false;
     }
    public Boolean saveBatch(List<Mro_task_parts_line> mro_task_parts_lines){
            return false;
     }

    public Page<Mro_task_parts_line> searchDefault(Mro_task_parts_lineSearchContext context){
            return null;
     }


}
