package cn.ibizlab.businesscentral.core.odoo_project.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_task;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_taskSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Project_task] 服务对象接口
 */
public interface IProject_taskService extends IService<Project_task>{

    boolean create(Project_task et) ;
    void createBatch(List<Project_task> list) ;
    boolean update(Project_task et) ;
    void updateBatch(List<Project_task> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Project_task get(Long key) ;
    Project_task getDraft(Project_task et) ;
    boolean checkKey(Project_task et) ;
    boolean save(Project_task et) ;
    void saveBatch(List<Project_task> list) ;
    Page<Project_task> searchDefault(Project_taskSearchContext context) ;
    List<Project_task> selectByProjectId(Long id);
    void resetByProjectId(Long id);
    void resetByProjectId(Collection<Long> ids);
    void removeByProjectId(Long id);
    List<Project_task> selectByStageId(Long id);
    List<Project_task> selectByStageId(Collection<Long> ids);
    void removeByStageId(Long id);
    List<Project_task> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Project_task> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Project_task> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Project_task> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Project_task> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Project_task> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Project_task> getProjectTaskByIds(List<Long> ids) ;
    List<Project_task> getProjectTaskByEntities(List<Project_task> entities) ;
}


