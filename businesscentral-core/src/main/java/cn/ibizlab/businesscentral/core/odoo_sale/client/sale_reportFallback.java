package cn.ibizlab.businesscentral.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_report;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_reportSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[sale_report] 服务对象接口
 */
@Component
public class sale_reportFallback implements sale_reportFeignClient{

    public Page<Sale_report> search(Sale_reportSearchContext context){
            return null;
     }


    public Sale_report get(Long id){
            return null;
     }



    public Sale_report update(Long id, Sale_report sale_report){
            return null;
     }
    public Boolean updateBatch(List<Sale_report> sale_reports){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Sale_report create(Sale_report sale_report){
            return null;
     }
    public Boolean createBatch(List<Sale_report> sale_reports){
            return false;
     }



    public Page<Sale_report> select(){
            return null;
     }

    public Sale_report getDraft(){
            return null;
    }



    public Boolean checkKey(Sale_report sale_report){
            return false;
     }


    public Boolean save(Sale_report sale_report){
            return false;
     }
    public Boolean saveBatch(List<Sale_report> sale_reports){
            return false;
     }

    public Page<Sale_report> searchDefault(Sale_reportSearchContext context){
            return null;
     }


}
