package cn.ibizlab.businesscentral.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_unbuildSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_unbuildService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mrp.mapper.Mrp_unbuildMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[拆解单] 服务对象接口实现
 */
@Slf4j
@Service("Mrp_unbuildServiceImpl")
public class Mrp_unbuildServiceImpl extends EBSServiceImpl<Mrp_unbuildMapper, Mrp_unbuild> implements IMrp_unbuildService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qty_unbuildService stockWarnInsufficientQtyUnbuildService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bomService mrpBomService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_productionService mrpProductionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_production_lotService stockProductionLotService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mrp.unbuild" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mrp_unbuild et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_unbuildService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mrp_unbuild> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mrp_unbuild et) {
        Mrp_unbuild old = new Mrp_unbuild() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_unbuildService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_unbuildService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mrp_unbuild> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        stockMoveService.resetByConsumeUnbuildId(key);
        stockMoveService.resetByUnbuildId(key);
        stockWarnInsufficientQtyUnbuildService.resetByUnbuildId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        stockMoveService.resetByConsumeUnbuildId(idList);
        stockMoveService.resetByUnbuildId(idList);
        stockWarnInsufficientQtyUnbuildService.resetByUnbuildId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mrp_unbuild get(Long key) {
        Mrp_unbuild et = getById(key);
        if(et==null){
            et=new Mrp_unbuild();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mrp_unbuild getDraft(Mrp_unbuild et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mrp_unbuild et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mrp_unbuild et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mrp_unbuild et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mrp_unbuild> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mrp_unbuild> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mrp_unbuild> selectByBomId(Long id) {
        return baseMapper.selectByBomId(id);
    }
    @Override
    public void resetByBomId(Long id) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("bom_id",null).eq("bom_id",id));
    }

    @Override
    public void resetByBomId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("bom_id",null).in("bom_id",ids));
    }

    @Override
    public void removeByBomId(Long id) {
        this.remove(new QueryWrapper<Mrp_unbuild>().eq("bom_id",id));
    }

	@Override
    public List<Mrp_unbuild> selectByMoId(Long id) {
        return baseMapper.selectByMoId(id);
    }
    @Override
    public void resetByMoId(Long id) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("mo_id",null).eq("mo_id",id));
    }

    @Override
    public void resetByMoId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("mo_id",null).in("mo_id",ids));
    }

    @Override
    public void removeByMoId(Long id) {
        this.remove(new QueryWrapper<Mrp_unbuild>().eq("mo_id",id));
    }

	@Override
    public List<Mrp_unbuild> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Mrp_unbuild>().eq("product_id",id));
    }

	@Override
    public List<Mrp_unbuild> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mrp_unbuild>().eq("create_uid",id));
    }

	@Override
    public List<Mrp_unbuild> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mrp_unbuild>().eq("write_uid",id));
    }

	@Override
    public List<Mrp_unbuild> selectByLocationDestId(Long id) {
        return baseMapper.selectByLocationDestId(id);
    }
    @Override
    public void resetByLocationDestId(Long id) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("location_dest_id",null).eq("location_dest_id",id));
    }

    @Override
    public void resetByLocationDestId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("location_dest_id",null).in("location_dest_id",ids));
    }

    @Override
    public void removeByLocationDestId(Long id) {
        this.remove(new QueryWrapper<Mrp_unbuild>().eq("location_dest_id",id));
    }

	@Override
    public List<Mrp_unbuild> selectByLocationId(Long id) {
        return baseMapper.selectByLocationId(id);
    }
    @Override
    public void resetByLocationId(Long id) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("location_id",null).eq("location_id",id));
    }

    @Override
    public void resetByLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("location_id",null).in("location_id",ids));
    }

    @Override
    public void removeByLocationId(Long id) {
        this.remove(new QueryWrapper<Mrp_unbuild>().eq("location_id",id));
    }

	@Override
    public List<Mrp_unbuild> selectByLotId(Long id) {
        return baseMapper.selectByLotId(id);
    }
    @Override
    public void resetByLotId(Long id) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("lot_id",null).eq("lot_id",id));
    }

    @Override
    public void resetByLotId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("lot_id",null).in("lot_id",ids));
    }

    @Override
    public void removeByLotId(Long id) {
        this.remove(new QueryWrapper<Mrp_unbuild>().eq("lot_id",id));
    }

	@Override
    public List<Mrp_unbuild> selectByProductUomId(Long id) {
        return baseMapper.selectByProductUomId(id);
    }
    @Override
    public void resetByProductUomId(Long id) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("product_uom_id",null).eq("product_uom_id",id));
    }

    @Override
    public void resetByProductUomId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_unbuild>().set("product_uom_id",null).in("product_uom_id",ids));
    }

    @Override
    public void removeByProductUomId(Long id) {
        this.remove(new QueryWrapper<Mrp_unbuild>().eq("product_uom_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mrp_unbuild> searchDefault(Mrp_unbuildSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mrp_unbuild> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mrp_unbuild>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mrp_unbuild et){
        //实体关系[DER1N_MRP_UNBUILD__MRP_PRODUCTION__MO_ID]
        if(!ObjectUtils.isEmpty(et.getMoId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production odooMo=et.getOdooMo();
            if(ObjectUtils.isEmpty(odooMo)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production majorEntity=mrpProductionService.get(et.getMoId());
                et.setOdooMo(majorEntity);
                odooMo=majorEntity;
            }
            et.setMoIdText(odooMo.getName());
        }
        //实体关系[DER1N_MRP_UNBUILD__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
            et.setHasTracking(odooProduct.getTracking());
        }
        //实体关系[DER1N_MRP_UNBUILD__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MRP_UNBUILD__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_MRP_UNBUILD__STOCK_LOCATION__LOCATION_DEST_ID]
        if(!ObjectUtils.isEmpty(et.getLocationDestId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocationDest=et.getOdooLocationDest();
            if(ObjectUtils.isEmpty(odooLocationDest)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationDestId());
                et.setOdooLocationDest(majorEntity);
                odooLocationDest=majorEntity;
            }
            et.setLocationDestIdText(odooLocationDest.getName());
        }
        //实体关系[DER1N_MRP_UNBUILD__STOCK_LOCATION__LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation=et.getOdooLocation();
            if(ObjectUtils.isEmpty(odooLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationId());
                et.setOdooLocation(majorEntity);
                odooLocation=majorEntity;
            }
            et.setLocationIdText(odooLocation.getName());
        }
        //实体关系[DER1N_MRP_UNBUILD__STOCK_PRODUCTION_LOT__LOT_ID]
        if(!ObjectUtils.isEmpty(et.getLotId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot odooLot=et.getOdooLot();
            if(ObjectUtils.isEmpty(odooLot)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot majorEntity=stockProductionLotService.get(et.getLotId());
                et.setOdooLot(majorEntity);
                odooLot=majorEntity;
            }
            et.setLotIdText(odooLot.getName());
        }
        //实体关系[DER1N_MRP_UNBUILD__UOM_UOM__PRODUCT_UOM_ID]
        if(!ObjectUtils.isEmpty(et.getProductUomId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUomId());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomIdText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mrp_unbuild> getMrpUnbuildByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mrp_unbuild> getMrpUnbuildByEntities(List<Mrp_unbuild> entities) {
        List ids =new ArrayList();
        for(Mrp_unbuild entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



