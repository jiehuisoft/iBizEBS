package cn.ibizlab.businesscentral.core.odoo_website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_seo_metadata;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_seo_metadataSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Website_seo_metadata] 服务对象接口
 */
public interface IWebsite_seo_metadataService extends IService<Website_seo_metadata>{

    boolean create(Website_seo_metadata et) ;
    void createBatch(List<Website_seo_metadata> list) ;
    boolean update(Website_seo_metadata et) ;
    void updateBatch(List<Website_seo_metadata> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Website_seo_metadata get(Long key) ;
    Website_seo_metadata getDraft(Website_seo_metadata et) ;
    boolean checkKey(Website_seo_metadata et) ;
    boolean save(Website_seo_metadata et) ;
    void saveBatch(List<Website_seo_metadata> list) ;
    Page<Website_seo_metadata> searchDefault(Website_seo_metadataSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


