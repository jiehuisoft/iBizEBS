package cn.ibizlab.businesscentral.core.odoo_web_tour.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_web_tour.domain.Web_tour_tour;
import cn.ibizlab.businesscentral.core.odoo_web_tour.filter.Web_tour_tourSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[web_tour_tour] 服务对象接口
 */
@Component
public class web_tour_tourFallback implements web_tour_tourFeignClient{


    public Page<Web_tour_tour> search(Web_tour_tourSearchContext context){
            return null;
     }


    public Web_tour_tour get(Long id){
            return null;
     }




    public Web_tour_tour update(Long id, Web_tour_tour web_tour_tour){
            return null;
     }
    public Boolean updateBatch(List<Web_tour_tour> web_tour_tours){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Web_tour_tour create(Web_tour_tour web_tour_tour){
            return null;
     }
    public Boolean createBatch(List<Web_tour_tour> web_tour_tours){
            return false;
     }

    public Page<Web_tour_tour> select(){
            return null;
     }

    public Web_tour_tour getDraft(){
            return null;
    }



    public Boolean checkKey(Web_tour_tour web_tour_tour){
            return false;
     }


    public Boolean save(Web_tour_tour web_tour_tour){
            return false;
     }
    public Boolean saveBatch(List<Web_tour_tour> web_tour_tours){
            return false;
     }

    public Page<Web_tour_tour> searchDefault(Web_tour_tourSearchContext context){
            return null;
     }


}
