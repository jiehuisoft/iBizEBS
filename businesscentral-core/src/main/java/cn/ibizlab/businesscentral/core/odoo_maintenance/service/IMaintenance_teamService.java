package cn.ibizlab.businesscentral.core.odoo_maintenance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_team;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_teamSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Maintenance_team] 服务对象接口
 */
public interface IMaintenance_teamService extends IService<Maintenance_team>{

    boolean create(Maintenance_team et) ;
    void createBatch(List<Maintenance_team> list) ;
    boolean update(Maintenance_team et) ;
    void updateBatch(List<Maintenance_team> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Maintenance_team get(Long key) ;
    Maintenance_team getDraft(Maintenance_team et) ;
    boolean checkKey(Maintenance_team et) ;
    boolean save(Maintenance_team et) ;
    void saveBatch(List<Maintenance_team> list) ;
    Page<Maintenance_team> searchDefault(Maintenance_teamSearchContext context) ;
    List<Maintenance_team> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Maintenance_team> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Maintenance_team> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Maintenance_team> getMaintenanceTeamByIds(List<Long> ids) ;
    List<Maintenance_team> getMaintenanceTeamByEntities(List<Maintenance_team> entities) ;
}


