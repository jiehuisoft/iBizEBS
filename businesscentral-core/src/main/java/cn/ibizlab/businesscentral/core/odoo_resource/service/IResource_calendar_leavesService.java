package cn.ibizlab.businesscentral.core.odoo_resource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar_leaves;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_calendar_leavesSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Resource_calendar_leaves] 服务对象接口
 */
public interface IResource_calendar_leavesService extends IService<Resource_calendar_leaves>{

    boolean create(Resource_calendar_leaves et) ;
    void createBatch(List<Resource_calendar_leaves> list) ;
    boolean update(Resource_calendar_leaves et) ;
    void updateBatch(List<Resource_calendar_leaves> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Resource_calendar_leaves get(Long key) ;
    Resource_calendar_leaves getDraft(Resource_calendar_leaves et) ;
    boolean checkKey(Resource_calendar_leaves et) ;
    boolean save(Resource_calendar_leaves et) ;
    void saveBatch(List<Resource_calendar_leaves> list) ;
    Page<Resource_calendar_leaves> searchDefault(Resource_calendar_leavesSearchContext context) ;
    List<Resource_calendar_leaves> selectByHolidayId(Long id);
    void resetByHolidayId(Long id);
    void resetByHolidayId(Collection<Long> ids);
    void removeByHolidayId(Long id);
    List<Resource_calendar_leaves> selectByCalendarId(Long id);
    void resetByCalendarId(Long id);
    void resetByCalendarId(Collection<Long> ids);
    void removeByCalendarId(Long id);
    List<Resource_calendar_leaves> selectByResourceId(Long id);
    void resetByResourceId(Long id);
    void resetByResourceId(Collection<Long> ids);
    void removeByResourceId(Long id);
    List<Resource_calendar_leaves> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Resource_calendar_leaves> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Resource_calendar_leaves> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Resource_calendar_leaves> getResourceCalendarLeavesByIds(List<Long> ids) ;
    List<Resource_calendar_leaves> getResourceCalendarLeavesByEntities(List<Resource_calendar_leaves> entities) ;
}


