package cn.ibizlab.businesscentral.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_companySearchContext;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_base.mapper.Res_companyMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[公司] 服务对象接口实现
 */
@Slf4j
@Service("Res_companyServiceImpl")
public class Res_companyServiceImpl extends EBSServiceImpl<Res_companyMapper, Res_company> implements IRes_companyService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService accountAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_groupService accountAnalyticGroupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_lineService accountAnalyticLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_tagService accountAnalyticTagService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_lineService accountBankStatementLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statementService accountBankStatementService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_common_journal_reportService accountCommonJournalReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_common_reportService accountCommonReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_financial_year_opService accountFinancialYearOpService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_positionService accountFiscalPositionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_yearService accountFiscalYearService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_lineService accountInvoiceLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_reportService accountInvoiceReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_taxService accountInvoiceTaxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_journalService accountJournalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_lineService accountMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_moveService accountMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_partial_reconcileService accountPartialReconcileService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_termService accountPaymentTermService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_print_journalService accountPrintJournalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_reconcile_modelService accountReconcileModelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_taxService accountTaxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_activity_reportService crmActivityReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_leadService crmLeadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_teamService crmTeamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IDelivery_carrierService deliveryCarrierService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_digest.service.IDigest_digestService digestDigestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_eventService eventEventService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_registrationService eventRegistrationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicleService fleetVehicleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_applicantService hrApplicantService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_contractService hrContractService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_departmentService hrDepartmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheetService hrExpenseSheetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expenseService hrExpenseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_jobService hrJobService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leave_allocationService hrLeaveAllocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leave_typeService hrLeaveTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leaveService hrLeaveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_iap.service.IIap_accountService iapAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_attachmentService irAttachmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_propertyService irPropertyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_sequenceService irSequenceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_orderService lunchOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipment_categoryService maintenanceEquipmentCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipmentService maintenanceEquipmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_requestService maintenanceRequestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_teamService maintenanceTeamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_orderService mroOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_workorderService mroWorkorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bomService mrpBomService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_productionService mrpProductionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_routing_workcenterService mrpRoutingWorkcenterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_routingService mrpRoutingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenterService mrpWorkcenterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_acquirerService paymentAcquirerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelist_itemService productPricelistItemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelistService productPricelistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_price_historyService productPriceHistoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_supplierinfoService productSupplierinfoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_templateService productTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_project.service.IProject_projectService projectProjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_project.service.IProject_taskService projectTaskService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_bill_unionService purchaseBillUnionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService purchaseOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService purchaseOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_reportService purchaseReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisition_lineService purchaseRequisitionLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisitionService purchaseRequisitionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_orderService repairOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendar_leavesService resourceCalendarLeavesService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendarService resourceCalendarService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_mixinService resourceMixinService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_resourceService resourceResourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_testService resourceTestService;

    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_config_settingsService resConfigSettingsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currency_rateService resCurrencyRateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_bankService resPartnerBankService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_supplierService resSupplierService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService saleOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_orderService saleOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_reportService saleReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_snailmail.service.ISnailmail_letterService snailmailLetterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventory_lineService stockInventoryLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventoryService stockInventoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_location_routeService stockLocationRouteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_pickingService stockPickingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quant_packageService stockQuantPackageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quantService stockQuantService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_ruleService stockRuleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouse_orderpointService stockWarehouseOrderpointService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService stockWarehouseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_chart_templateService accountChartTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_incotermsService accountIncotermsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "res.company" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Res_company et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_companyService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Res_company> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Res_company et) {
        Res_company old = new Res_company() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_companyService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_companyService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Res_company> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountAccountService.resetByCompanyId(key);
        accountAnalyticAccountService.resetByCompanyId(key);
        accountAnalyticGroupService.resetByCompanyId(key);
        accountAnalyticLineService.resetByCompanyId(key);
        accountAnalyticTagService.resetByCompanyId(key);
        accountBankStatementLineService.resetByCompanyId(key);
        accountBankStatementService.resetByCompanyId(key);
        accountCommonJournalReportService.resetByCompanyId(key);
        accountCommonReportService.resetByCompanyId(key);
        accountFinancialYearOpService.resetByCompanyId(key);
        accountFiscalPositionService.resetByCompanyId(key);
        accountFiscalYearService.resetByCompanyId(key);
        accountInvoiceLineService.resetByCompanyId(key);
        accountInvoiceTaxService.resetByCompanyId(key);
        accountInvoiceService.resetByCompanyId(key);
        accountJournalService.resetByCompanyId(key);
        accountMoveLineService.resetByCompanyId(key);
        accountMoveService.resetByCompanyId(key);
        accountPartialReconcileService.resetByCompanyId(key);
        accountPaymentTermService.resetByCompanyId(key);
        accountPrintJournalService.resetByCompanyId(key);
        accountReconcileModelService.resetByCompanyId(key);
        accountTaxService.resetByCompanyId(key);
        crmLeadService.resetByCompanyId(key);
        crmTeamService.resetByCompanyId(key);
        digestDigestService.resetByCompanyId(key);
        eventEventService.resetByCompanyId(key);
        eventRegistrationService.resetByCompanyId(key);
        fleetVehicleService.resetByCompanyId(key);
        hrApplicantService.resetByCompanyId(key);
        hrContractService.resetByCompanyId(key);
        hrDepartmentService.resetByCompanyId(key);
        hrEmployeeService.resetByCompanyId(key);
        hrExpenseSheetService.resetByCompanyId(key);
        hrExpenseService.resetByCompanyId(key);
        hrJobService.resetByCompanyId(key);
        hrLeaveAllocationService.resetByModeCompanyId(key);
        hrLeaveTypeService.resetByCompanyId(key);
        hrLeaveService.resetByModeCompanyId(key);
        iapAccountService.resetByCompanyId(key);
        lunchOrderService.resetByCompanyId(key);
        maintenanceEquipmentCategoryService.resetByCompanyId(key);
        maintenanceEquipmentService.resetByCompanyId(key);
        maintenanceRequestService.resetByCompanyId(key);
        maintenanceTeamService.resetByCompanyId(key);
        mroOrderService.resetByCompanyId(key);
        mroWorkorderService.resetByCompanyId(key);
        mrpBomService.resetByCompanyId(key);
        mrpProductionService.resetByCompanyId(key);
        mrpRoutingWorkcenterService.resetByCompanyId(key);
        mrpRoutingService.resetByCompanyId(key);
        mrpWorkcenterService.resetByCompanyId(key);
        paymentAcquirerService.resetByCompanyId(key);
        productPricelistItemService.resetByCompanyId(key);
        productPricelistService.resetByCompanyId(key);
        productPriceHistoryService.resetByCompanyId(key);
        productSupplierinfoService.resetByCompanyId(key);
        productTemplateService.resetByCompanyId(key);
        projectProjectService.resetByCompanyId(key);
        projectTaskService.resetByCompanyId(key);
        purchaseBillUnionService.resetByCompanyId(key);
        purchaseOrderLineService.resetByCompanyId(key);
        purchaseOrderService.resetByCompanyId(key);
        purchaseReportService.resetByCompanyId(key);
        repairOrderService.resetByCompanyId(key);
        resourceCalendarLeavesService.resetByCompanyId(key);
        resourceCalendarService.resetByCompanyId(key);
        resourceMixinService.resetByCompanyId(key);
        resourceResourceService.resetByCompanyId(key);
        resourceTestService.resetByCompanyId(key);
        resCompanyService.resetByParentId(key);
        resConfigSettingsService.resetByCompanyId(key);
        resCurrencyRateService.resetByCompanyId(key);
        resPartnerBankService.removeByCompanyId(key);
        resPartnerService.resetByCompanyId(key);
        resUsersService.resetByCompanyId(key);
        saleOrderLineService.resetByCompanyId(key);
        saleOrderService.resetByCompanyId(key);
        saleReportService.resetByCompanyId(key);
        snailmailLetterService.resetByCompanyId(key);
        stockInventoryLineService.resetByCompanyId(key);
        stockInventoryService.resetByCompanyId(key);
        stockLocationRouteService.resetByCompanyId(key);
        stockLocationService.resetByCompanyId(key);
        stockMoveService.resetByCompanyId(key);
        stockPickingService.resetByCompanyId(key);
        stockQuantPackageService.resetByCompanyId(key);
        stockQuantService.resetByCompanyId(key);
        stockRuleService.resetByCompanyId(key);
        stockWarehouseOrderpointService.resetByCompanyId(key);
        stockWarehouseService.resetByCompanyId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountAccountService.resetByCompanyId(idList);
        accountAnalyticAccountService.resetByCompanyId(idList);
        accountAnalyticGroupService.resetByCompanyId(idList);
        accountAnalyticLineService.resetByCompanyId(idList);
        accountAnalyticTagService.resetByCompanyId(idList);
        accountBankStatementLineService.resetByCompanyId(idList);
        accountBankStatementService.resetByCompanyId(idList);
        accountCommonJournalReportService.resetByCompanyId(idList);
        accountCommonReportService.resetByCompanyId(idList);
        accountFinancialYearOpService.resetByCompanyId(idList);
        accountFiscalPositionService.resetByCompanyId(idList);
        accountFiscalYearService.resetByCompanyId(idList);
        accountInvoiceLineService.resetByCompanyId(idList);
        accountInvoiceTaxService.resetByCompanyId(idList);
        accountInvoiceService.resetByCompanyId(idList);
        accountJournalService.resetByCompanyId(idList);
        accountMoveLineService.resetByCompanyId(idList);
        accountMoveService.resetByCompanyId(idList);
        accountPartialReconcileService.resetByCompanyId(idList);
        accountPaymentTermService.resetByCompanyId(idList);
        accountPrintJournalService.resetByCompanyId(idList);
        accountReconcileModelService.resetByCompanyId(idList);
        accountTaxService.resetByCompanyId(idList);
        crmLeadService.resetByCompanyId(idList);
        crmTeamService.resetByCompanyId(idList);
        digestDigestService.resetByCompanyId(idList);
        eventEventService.resetByCompanyId(idList);
        eventRegistrationService.resetByCompanyId(idList);
        fleetVehicleService.resetByCompanyId(idList);
        hrApplicantService.resetByCompanyId(idList);
        hrContractService.resetByCompanyId(idList);
        hrDepartmentService.resetByCompanyId(idList);
        hrEmployeeService.resetByCompanyId(idList);
        hrExpenseSheetService.resetByCompanyId(idList);
        hrExpenseService.resetByCompanyId(idList);
        hrJobService.resetByCompanyId(idList);
        hrLeaveAllocationService.resetByModeCompanyId(idList);
        hrLeaveTypeService.resetByCompanyId(idList);
        hrLeaveService.resetByModeCompanyId(idList);
        iapAccountService.resetByCompanyId(idList);
        lunchOrderService.resetByCompanyId(idList);
        maintenanceEquipmentCategoryService.resetByCompanyId(idList);
        maintenanceEquipmentService.resetByCompanyId(idList);
        maintenanceRequestService.resetByCompanyId(idList);
        maintenanceTeamService.resetByCompanyId(idList);
        mroOrderService.resetByCompanyId(idList);
        mroWorkorderService.resetByCompanyId(idList);
        mrpBomService.resetByCompanyId(idList);
        mrpProductionService.resetByCompanyId(idList);
        mrpRoutingWorkcenterService.resetByCompanyId(idList);
        mrpRoutingService.resetByCompanyId(idList);
        mrpWorkcenterService.resetByCompanyId(idList);
        paymentAcquirerService.resetByCompanyId(idList);
        productPricelistItemService.resetByCompanyId(idList);
        productPricelistService.resetByCompanyId(idList);
        productPriceHistoryService.resetByCompanyId(idList);
        productSupplierinfoService.resetByCompanyId(idList);
        productTemplateService.resetByCompanyId(idList);
        projectProjectService.resetByCompanyId(idList);
        projectTaskService.resetByCompanyId(idList);
        purchaseBillUnionService.resetByCompanyId(idList);
        purchaseOrderLineService.resetByCompanyId(idList);
        purchaseOrderService.resetByCompanyId(idList);
        purchaseReportService.resetByCompanyId(idList);
        repairOrderService.resetByCompanyId(idList);
        resourceCalendarLeavesService.resetByCompanyId(idList);
        resourceCalendarService.resetByCompanyId(idList);
        resourceMixinService.resetByCompanyId(idList);
        resourceResourceService.resetByCompanyId(idList);
        resourceTestService.resetByCompanyId(idList);
        resCompanyService.resetByParentId(idList);
        resConfigSettingsService.resetByCompanyId(idList);
        resCurrencyRateService.resetByCompanyId(idList);
        resPartnerBankService.removeByCompanyId(idList);
        resPartnerService.resetByCompanyId(idList);
        resUsersService.resetByCompanyId(idList);
        saleOrderLineService.resetByCompanyId(idList);
        saleOrderService.resetByCompanyId(idList);
        saleReportService.resetByCompanyId(idList);
        snailmailLetterService.resetByCompanyId(idList);
        stockInventoryLineService.resetByCompanyId(idList);
        stockInventoryService.resetByCompanyId(idList);
        stockLocationRouteService.resetByCompanyId(idList);
        stockLocationService.resetByCompanyId(idList);
        stockMoveService.resetByCompanyId(idList);
        stockPickingService.resetByCompanyId(idList);
        stockQuantPackageService.resetByCompanyId(idList);
        stockQuantService.resetByCompanyId(idList);
        stockRuleService.resetByCompanyId(idList);
        stockWarehouseOrderpointService.resetByCompanyId(idList);
        stockWarehouseService.resetByCompanyId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Res_company get(Long key) {
        Res_company et = getById(key);
        if(et==null){
            et=new Res_company();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Res_company getDraft(Res_company et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Res_company et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Res_company et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Res_company et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Res_company> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Res_company> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Res_company> selectByPropertyStockAccountInputCategId(Long id) {
        return baseMapper.selectByPropertyStockAccountInputCategId(id);
    }
    @Override
    public void resetByPropertyStockAccountInputCategId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("property_stock_account_input_categ_id",null).eq("property_stock_account_input_categ_id",id));
    }

    @Override
    public void resetByPropertyStockAccountInputCategId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("property_stock_account_input_categ_id",null).in("property_stock_account_input_categ_id",ids));
    }

    @Override
    public void removeByPropertyStockAccountInputCategId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("property_stock_account_input_categ_id",id));
    }

	@Override
    public List<Res_company> selectByPropertyStockAccountOutputCategId(Long id) {
        return baseMapper.selectByPropertyStockAccountOutputCategId(id);
    }
    @Override
    public void resetByPropertyStockAccountOutputCategId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("property_stock_account_output_categ_id",null).eq("property_stock_account_output_categ_id",id));
    }

    @Override
    public void resetByPropertyStockAccountOutputCategId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("property_stock_account_output_categ_id",null).in("property_stock_account_output_categ_id",ids));
    }

    @Override
    public void removeByPropertyStockAccountOutputCategId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("property_stock_account_output_categ_id",id));
    }

	@Override
    public List<Res_company> selectByPropertyStockValuationAccountId(Long id) {
        return baseMapper.selectByPropertyStockValuationAccountId(id);
    }
    @Override
    public void resetByPropertyStockValuationAccountId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("property_stock_valuation_account_id",null).eq("property_stock_valuation_account_id",id));
    }

    @Override
    public void resetByPropertyStockValuationAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("property_stock_valuation_account_id",null).in("property_stock_valuation_account_id",ids));
    }

    @Override
    public void removeByPropertyStockValuationAccountId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("property_stock_valuation_account_id",id));
    }

	@Override
    public List<Res_company> selectByTransferAccountId(Long id) {
        return baseMapper.selectByTransferAccountId(id);
    }
    @Override
    public void resetByTransferAccountId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("transfer_account_id",null).eq("transfer_account_id",id));
    }

    @Override
    public void resetByTransferAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("transfer_account_id",null).in("transfer_account_id",ids));
    }

    @Override
    public void removeByTransferAccountId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("transfer_account_id",id));
    }

	@Override
    public List<Res_company> selectByChartTemplateId(Long id) {
        return baseMapper.selectByChartTemplateId(id);
    }
    @Override
    public void resetByChartTemplateId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("chart_template_id",null).eq("chart_template_id",id));
    }

    @Override
    public void resetByChartTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("chart_template_id",null).in("chart_template_id",ids));
    }

    @Override
    public void removeByChartTemplateId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("chart_template_id",id));
    }

	@Override
    public List<Res_company> selectByIncotermId(Long id) {
        return baseMapper.selectByIncotermId(id);
    }
    @Override
    public void resetByIncotermId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("incoterm_id",null).eq("incoterm_id",id));
    }

    @Override
    public void resetByIncotermId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("incoterm_id",null).in("incoterm_id",ids));
    }

    @Override
    public void removeByIncotermId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("incoterm_id",id));
    }

	@Override
    public List<Res_company> selectByCurrencyExchangeJournalId(Long id) {
        return baseMapper.selectByCurrencyExchangeJournalId(id);
    }
    @Override
    public void resetByCurrencyExchangeJournalId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("currency_exchange_journal_id",null).eq("currency_exchange_journal_id",id));
    }

    @Override
    public void resetByCurrencyExchangeJournalId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("currency_exchange_journal_id",null).in("currency_exchange_journal_id",ids));
    }

    @Override
    public void removeByCurrencyExchangeJournalId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("currency_exchange_journal_id",id));
    }

	@Override
    public List<Res_company> selectByTaxCashBasisJournalId(Long id) {
        return baseMapper.selectByTaxCashBasisJournalId(id);
    }
    @Override
    public void resetByTaxCashBasisJournalId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("tax_cash_basis_journal_id",null).eq("tax_cash_basis_journal_id",id));
    }

    @Override
    public void resetByTaxCashBasisJournalId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("tax_cash_basis_journal_id",null).in("tax_cash_basis_journal_id",ids));
    }

    @Override
    public void removeByTaxCashBasisJournalId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("tax_cash_basis_journal_id",id));
    }

	@Override
    public List<Res_company> selectByAccountOpeningMoveId(Long id) {
        return baseMapper.selectByAccountOpeningMoveId(id);
    }
    @Override
    public void resetByAccountOpeningMoveId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("account_opening_move_id",null).eq("account_opening_move_id",id));
    }

    @Override
    public void resetByAccountOpeningMoveId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("account_opening_move_id",null).in("account_opening_move_id",ids));
    }

    @Override
    public void removeByAccountOpeningMoveId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("account_opening_move_id",id));
    }

	@Override
    public List<Res_company> selectByAccountPurchaseTaxId(Long id) {
        return baseMapper.selectByAccountPurchaseTaxId(id);
    }
    @Override
    public void resetByAccountPurchaseTaxId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("account_purchase_tax_id",null).eq("account_purchase_tax_id",id));
    }

    @Override
    public void resetByAccountPurchaseTaxId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("account_purchase_tax_id",null).in("account_purchase_tax_id",ids));
    }

    @Override
    public void removeByAccountPurchaseTaxId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("account_purchase_tax_id",id));
    }

	@Override
    public List<Res_company> selectByAccountSaleTaxId(Long id) {
        return baseMapper.selectByAccountSaleTaxId(id);
    }
    @Override
    public void resetByAccountSaleTaxId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("account_sale_tax_id",null).eq("account_sale_tax_id",id));
    }

    @Override
    public void resetByAccountSaleTaxId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("account_sale_tax_id",null).in("account_sale_tax_id",ids));
    }

    @Override
    public void removeByAccountSaleTaxId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("account_sale_tax_id",id));
    }

	@Override
    public List<Res_company> selectByResourceCalendarId(Long id) {
        return baseMapper.selectByResourceCalendarId(id);
    }
    @Override
    public List<Res_company> selectByResourceCalendarId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Res_company>().in("id",ids));
    }

    @Override
    public void removeByResourceCalendarId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("resource_calendar_id",id));
    }

	@Override
    public List<Res_company> selectByParentId(Long id) {
        return baseMapper.selectByParentId(id);
    }
    @Override
    public void resetByParentId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("parent_id",null).eq("parent_id",id));
    }

    @Override
    public void resetByParentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("parent_id",null).in("parent_id",ids));
    }

    @Override
    public void removeByParentId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("parent_id",id));
    }

	@Override
    public List<Res_company> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("currency_id",id));
    }

	@Override
    public List<Res_company> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("partner_id",id));
    }

	@Override
    public List<Res_company> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("create_uid",id));
    }

	@Override
    public List<Res_company> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("write_uid",id));
    }

	@Override
    public List<Res_company> selectByInternalTransitLocationId(Long id) {
        return baseMapper.selectByInternalTransitLocationId(id);
    }
    @Override
    public void resetByInternalTransitLocationId(Long id) {
        this.update(new UpdateWrapper<Res_company>().set("internal_transit_location_id",null).eq("internal_transit_location_id",id));
    }

    @Override
    public void resetByInternalTransitLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_company>().set("internal_transit_location_id",null).in("internal_transit_location_id",ids));
    }

    @Override
    public void removeByInternalTransitLocationId(Long id) {
        this.remove(new QueryWrapper<Res_company>().eq("internal_transit_location_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Res_company> searchDefault(Res_companySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Res_company> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Res_company>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 根节点查询
     */
    @Override
    public Page<Res_company> searchROOT(Res_companySearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Res_company> pages=baseMapper.searchROOT(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Res_company>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Res_company et){
        //实体关系[DER1N_RES_COMPANY__ACCOUNT_ACCOUNT__PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
        if(!ObjectUtils.isEmpty(et.getPropertyStockAccountInputCategId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooPropertyStockAccountInputCateg=et.getOdooPropertyStockAccountInputCateg();
            if(ObjectUtils.isEmpty(odooPropertyStockAccountInputCateg)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getPropertyStockAccountInputCategId());
                et.setOdooPropertyStockAccountInputCateg(majorEntity);
                odooPropertyStockAccountInputCateg=majorEntity;
            }
            et.setPropertyStockAccountInputCategIdText(odooPropertyStockAccountInputCateg.getName());
        }
        //实体关系[DER1N_RES_COMPANY__ACCOUNT_ACCOUNT__PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
        if(!ObjectUtils.isEmpty(et.getPropertyStockAccountOutputCategId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooPropertyStockAccountOutputCateg=et.getOdooPropertyStockAccountOutputCateg();
            if(ObjectUtils.isEmpty(odooPropertyStockAccountOutputCateg)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getPropertyStockAccountOutputCategId());
                et.setOdooPropertyStockAccountOutputCateg(majorEntity);
                odooPropertyStockAccountOutputCateg=majorEntity;
            }
            et.setPropertyStockAccountOutputCategIdText(odooPropertyStockAccountOutputCateg.getName());
        }
        //实体关系[DER1N_RES_COMPANY__ACCOUNT_ACCOUNT__PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getPropertyStockValuationAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooPropertyStockValuationAccount=et.getOdooPropertyStockValuationAccount();
            if(ObjectUtils.isEmpty(odooPropertyStockValuationAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getPropertyStockValuationAccountId());
                et.setOdooPropertyStockValuationAccount(majorEntity);
                odooPropertyStockValuationAccount=majorEntity;
            }
            et.setPropertyStockValuationAccountIdText(odooPropertyStockValuationAccount.getName());
        }
        //实体关系[DER1N_RES_COMPANY__ACCOUNT_ACCOUNT__TRANSFER_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getTransferAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooTransferAccount=et.getOdooTransferAccount();
            if(ObjectUtils.isEmpty(odooTransferAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getTransferAccountId());
                et.setOdooTransferAccount(majorEntity);
                odooTransferAccount=majorEntity;
            }
            et.setTransferAccountIdText(odooTransferAccount.getName());
        }
        //实体关系[DER1N_RES_COMPANY__ACCOUNT_CHART_TEMPLATE__CHART_TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getChartTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template odooChartTemplate=et.getOdooChartTemplate();
            if(ObjectUtils.isEmpty(odooChartTemplate)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template majorEntity=accountChartTemplateService.get(et.getChartTemplateId());
                et.setOdooChartTemplate(majorEntity);
                odooChartTemplate=majorEntity;
            }
            et.setChartTemplateIdText(odooChartTemplate.getName());
        }
        //实体关系[DER1N_RES_COMPANY__ACCOUNT_INCOTERMS__INCOTERM_ID]
        if(!ObjectUtils.isEmpty(et.getIncotermId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms odooIncoterm=et.getOdooIncoterm();
            if(ObjectUtils.isEmpty(odooIncoterm)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms majorEntity=accountIncotermsService.get(et.getIncotermId());
                et.setOdooIncoterm(majorEntity);
                odooIncoterm=majorEntity;
            }
            et.setIncotermIdText(odooIncoterm.getName());
        }
        //实体关系[DER1N_RES_COMPANY__ACCOUNT_JOURNAL__CURRENCY_EXCHANGE_JOURNAL_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyExchangeJournalId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooCurrencyExchangeJournal=et.getOdooCurrencyExchangeJournal();
            if(ObjectUtils.isEmpty(odooCurrencyExchangeJournal)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal majorEntity=accountJournalService.get(et.getCurrencyExchangeJournalId());
                et.setOdooCurrencyExchangeJournal(majorEntity);
                odooCurrencyExchangeJournal=majorEntity;
            }
            et.setExpenseCurrencyExchangeAccountId(odooCurrencyExchangeJournal.getDefaultDebitAccountId());
            et.setIncomeCurrencyExchangeAccountId(odooCurrencyExchangeJournal.getDefaultCreditAccountId());
            et.setCurrencyExchangeJournalIdText(odooCurrencyExchangeJournal.getName());
        }
        //实体关系[DER1N_RES_COMPANY__ACCOUNT_JOURNAL__TAX_CASH_BASIS_JOURNAL_ID]
        if(!ObjectUtils.isEmpty(et.getTaxCashBasisJournalId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooTaxCashBasisJournal=et.getOdooTaxCashBasisJournal();
            if(ObjectUtils.isEmpty(odooTaxCashBasisJournal)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal majorEntity=accountJournalService.get(et.getTaxCashBasisJournalId());
                et.setOdooTaxCashBasisJournal(majorEntity);
                odooTaxCashBasisJournal=majorEntity;
            }
            et.setTaxCashBasisJournalIdText(odooTaxCashBasisJournal.getName());
        }
        //实体关系[DER1N_RES_COMPANY__ACCOUNT_MOVE__ACCOUNT_OPENING_MOVE_ID]
        if(!ObjectUtils.isEmpty(et.getAccountOpeningMoveId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move odooAccountOpeningMove=et.getOdooAccountOpeningMove();
            if(ObjectUtils.isEmpty(odooAccountOpeningMove)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move majorEntity=accountMoveService.get(et.getAccountOpeningMoveId());
                et.setOdooAccountOpeningMove(majorEntity);
                odooAccountOpeningMove=majorEntity;
            }
            et.setAccountOpeningJournalId(odooAccountOpeningMove.getJournalId());
            et.setAccountOpeningDate(odooAccountOpeningMove.getDate());
            et.setAccountOpeningMoveIdText(odooAccountOpeningMove.getName());
        }
        //实体关系[DER1N_RES_COMPANY__ACCOUNT_TAX__ACCOUNT_PURCHASE_TAX_ID]
        if(!ObjectUtils.isEmpty(et.getAccountPurchaseTaxId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax odooAccountPurchaseTax=et.getOdooAccountPurchaseTax();
            if(ObjectUtils.isEmpty(odooAccountPurchaseTax)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax majorEntity=accountTaxService.get(et.getAccountPurchaseTaxId());
                et.setOdooAccountPurchaseTax(majorEntity);
                odooAccountPurchaseTax=majorEntity;
            }
            et.setAccountPurchaseTaxIdText(odooAccountPurchaseTax.getName());
        }
        //实体关系[DER1N_RES_COMPANY__ACCOUNT_TAX__ACCOUNT_SALE_TAX_ID]
        if(!ObjectUtils.isEmpty(et.getAccountSaleTaxId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax odooAccountSaleTax=et.getOdooAccountSaleTax();
            if(ObjectUtils.isEmpty(odooAccountSaleTax)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax majorEntity=accountTaxService.get(et.getAccountSaleTaxId());
                et.setOdooAccountSaleTax(majorEntity);
                odooAccountSaleTax=majorEntity;
            }
            et.setAccountSaleTaxIdText(odooAccountSaleTax.getName());
        }
        //实体关系[DER1N_RES_COMPANY__RESOURCE_CALENDAR__RESOURCE_CALENDAR_ID]
        if(!ObjectUtils.isEmpty(et.getResourceCalendarId())){
            cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar odooResourceCalendar=et.getOdooResourceCalendar();
            if(ObjectUtils.isEmpty(odooResourceCalendar)){
                cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar majorEntity=resourceCalendarService.get(et.getResourceCalendarId());
                et.setOdooResourceCalendar(majorEntity);
                odooResourceCalendar=majorEntity;
            }
            et.setResourceCalendarIdText(odooResourceCalendar.getName());
        }
        //实体关系[DER1N_RES_COMPANY__RES_COMPANY__PARENT_ID]
        if(!ObjectUtils.isEmpty(et.getParentId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooParent=et.getOdooParent();
            if(ObjectUtils.isEmpty(odooParent)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getParentId());
                et.setOdooParent(majorEntity);
                odooParent=majorEntity;
            }
            et.setParentIdText(odooParent.getName());
        }
        //实体关系[DER1N_RES_COMPANY__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_RES_COMPANY__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerGid(odooPartner.getPartnerGid());
            et.setPhone(odooPartner.getPhone());
            et.setLogo(odooPartner.getImage());
            et.setName(odooPartner.getName());
            et.setEmail(odooPartner.getEmail());
            et.setWebsite(odooPartner.getWebsite());
            et.setVat(odooPartner.getVat());
        }
        //实体关系[DER1N_RES_COMPANY__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_RES_COMPANY__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_RES_COMPANY__STOCK_LOCATION__INTERNAL_TRANSIT_LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getInternalTransitLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooInternalTransitLocation=et.getOdooInternalTransitLocation();
            if(ObjectUtils.isEmpty(odooInternalTransitLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getInternalTransitLocationId());
                et.setOdooInternalTransitLocation(majorEntity);
                odooInternalTransitLocation=majorEntity;
            }
            et.setInternalTransitLocationIdText(odooInternalTransitLocation.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Res_company> getResCompanyByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Res_company> getResCompanyByEntities(List<Res_company> entities) {
        List ids =new ArrayList();
        for(Res_company entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



