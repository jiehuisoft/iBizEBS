package cn.ibizlab.businesscentral.core.odoo_maintenance.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment_category;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_equipment_categorySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[maintenance_equipment_category] 服务对象接口
 */
@Component
public class maintenance_equipment_categoryFallback implements maintenance_equipment_categoryFeignClient{


    public Maintenance_equipment_category create(Maintenance_equipment_category maintenance_equipment_category){
            return null;
     }
    public Boolean createBatch(List<Maintenance_equipment_category> maintenance_equipment_categories){
            return false;
     }

    public Page<Maintenance_equipment_category> search(Maintenance_equipment_categorySearchContext context){
            return null;
     }


    public Maintenance_equipment_category update(Long id, Maintenance_equipment_category maintenance_equipment_category){
            return null;
     }
    public Boolean updateBatch(List<Maintenance_equipment_category> maintenance_equipment_categories){
            return false;
     }


    public Maintenance_equipment_category get(Long id){
            return null;
     }




    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Maintenance_equipment_category> select(){
            return null;
     }

    public Maintenance_equipment_category getDraft(){
            return null;
    }



    public Boolean checkKey(Maintenance_equipment_category maintenance_equipment_category){
            return false;
     }


    public Boolean save(Maintenance_equipment_category maintenance_equipment_category){
            return false;
     }
    public Boolean saveBatch(List<Maintenance_equipment_category> maintenance_equipment_categories){
            return false;
     }

    public Page<Maintenance_equipment_category> searchDefault(Maintenance_equipment_categorySearchContext context){
            return null;
     }


}
