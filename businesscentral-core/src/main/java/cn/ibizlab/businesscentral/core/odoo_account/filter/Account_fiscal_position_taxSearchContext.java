package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position_tax;
/**
 * 关系型数据实体[Account_fiscal_position_tax] 查询条件对象
 */
@Slf4j
@Data
public class Account_fiscal_position_taxSearchContext extends QueryWrapperContext<Account_fiscal_position_tax> {

	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_tax_src_id_text_eq;//[产品上的税]
	public void setN_tax_src_id_text_eq(String n_tax_src_id_text_eq) {
        this.n_tax_src_id_text_eq = n_tax_src_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_src_id_text_eq)){
            this.getSearchCond().eq("tax_src_id_text", n_tax_src_id_text_eq);
        }
    }
	private String n_tax_src_id_text_like;//[产品上的税]
	public void setN_tax_src_id_text_like(String n_tax_src_id_text_like) {
        this.n_tax_src_id_text_like = n_tax_src_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_tax_src_id_text_like)){
            this.getSearchCond().like("tax_src_id_text", n_tax_src_id_text_like);
        }
    }
	private String n_position_id_text_eq;//[税科目调整]
	public void setN_position_id_text_eq(String n_position_id_text_eq) {
        this.n_position_id_text_eq = n_position_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_position_id_text_eq)){
            this.getSearchCond().eq("position_id_text", n_position_id_text_eq);
        }
    }
	private String n_position_id_text_like;//[税科目调整]
	public void setN_position_id_text_like(String n_position_id_text_like) {
        this.n_position_id_text_like = n_position_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_position_id_text_like)){
            this.getSearchCond().like("position_id_text", n_position_id_text_like);
        }
    }
	private String n_tax_dest_id_text_eq;//[采用的税]
	public void setN_tax_dest_id_text_eq(String n_tax_dest_id_text_eq) {
        this.n_tax_dest_id_text_eq = n_tax_dest_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_dest_id_text_eq)){
            this.getSearchCond().eq("tax_dest_id_text", n_tax_dest_id_text_eq);
        }
    }
	private String n_tax_dest_id_text_like;//[采用的税]
	public void setN_tax_dest_id_text_like(String n_tax_dest_id_text_like) {
        this.n_tax_dest_id_text_like = n_tax_dest_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_tax_dest_id_text_like)){
            this.getSearchCond().like("tax_dest_id_text", n_tax_dest_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_position_id_eq;//[税科目调整]
	public void setN_position_id_eq(Long n_position_id_eq) {
        this.n_position_id_eq = n_position_id_eq;
        if(!ObjectUtils.isEmpty(this.n_position_id_eq)){
            this.getSearchCond().eq("position_id", n_position_id_eq);
        }
    }
	private Long n_tax_dest_id_eq;//[采用的税]
	public void setN_tax_dest_id_eq(Long n_tax_dest_id_eq) {
        this.n_tax_dest_id_eq = n_tax_dest_id_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_dest_id_eq)){
            this.getSearchCond().eq("tax_dest_id", n_tax_dest_id_eq);
        }
    }
	private Long n_tax_src_id_eq;//[产品上的税]
	public void setN_tax_src_id_eq(Long n_tax_src_id_eq) {
        this.n_tax_src_id_eq = n_tax_src_id_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_src_id_eq)){
            this.getSearchCond().eq("tax_src_id", n_tax_src_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



