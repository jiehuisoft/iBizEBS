package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_tag;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_tagSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_analytic_tag] 服务对象接口
 */
@Component
public class account_analytic_tagFallback implements account_analytic_tagFeignClient{



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Account_analytic_tag update(Long id, Account_analytic_tag account_analytic_tag){
            return null;
     }
    public Boolean updateBatch(List<Account_analytic_tag> account_analytic_tags){
            return false;
     }


    public Account_analytic_tag get(Long id){
            return null;
     }


    public Account_analytic_tag create(Account_analytic_tag account_analytic_tag){
            return null;
     }
    public Boolean createBatch(List<Account_analytic_tag> account_analytic_tags){
            return false;
     }

    public Page<Account_analytic_tag> search(Account_analytic_tagSearchContext context){
            return null;
     }



    public Page<Account_analytic_tag> select(){
            return null;
     }

    public Account_analytic_tag getDraft(){
            return null;
    }



    public Boolean checkKey(Account_analytic_tag account_analytic_tag){
            return false;
     }


    public Boolean save(Account_analytic_tag account_analytic_tag){
            return false;
     }
    public Boolean saveBatch(List<Account_analytic_tag> account_analytic_tags){
            return false;
     }

    public Page<Account_analytic_tag> searchDefault(Account_analytic_tagSearchContext context){
            return null;
     }


}
