package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partnerSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_partner] 服务对象接口
 */
public interface IRes_partnerService extends IService<Res_partner>{

    boolean create(Res_partner et) ;
    void createBatch(List<Res_partner> list) ;
    boolean update(Res_partner et) ;
    void updateBatch(List<Res_partner> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_partner get(Long key) ;
    Res_partner getDraft(Res_partner et) ;
    boolean checkKey(Res_partner et) ;
    boolean save(Res_partner et) ;
    void saveBatch(List<Res_partner> list) ;
    Page<Res_partner> searchDefault(Res_partnerSearchContext context) ;
    List<Res_partner> selectByTeamId(Long id);
    void resetByTeamId(Long id);
    void resetByTeamId(Collection<Long> ids);
    void removeByTeamId(Long id);
    List<Res_partner> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Res_partner> selectByStateId(Long id);
    List<Res_partner> selectByStateId(Collection<Long> ids);
    void removeByStateId(Long id);
    List<Res_partner> selectByCountryId(Long id);
    List<Res_partner> selectByCountryId(Collection<Long> ids);
    void removeByCountryId(Long id);
    List<Res_partner> selectByIndustryId(Long id);
    void resetByIndustryId(Long id);
    void resetByIndustryId(Collection<Long> ids);
    void removeByIndustryId(Long id);
    List<Res_partner> selectByTitle(Long id);
    void resetByTitle(Long id);
    void resetByTitle(Collection<Long> ids);
    void removeByTitle(Long id);
    List<Res_partner> selectByCommercialPartnerId(Long id);
    void resetByCommercialPartnerId(Long id);
    void resetByCommercialPartnerId(Collection<Long> ids);
    void removeByCommercialPartnerId(Long id);
    List<Res_partner> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Res_partner> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_partner> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Res_partner> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_partner> getResPartnerByIds(List<Long> ids) ;
    List<Res_partner> getResPartnerByEntities(List<Res_partner> entities) ;
}


