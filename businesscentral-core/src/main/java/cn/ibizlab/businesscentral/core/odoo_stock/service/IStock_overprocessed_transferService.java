package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_overprocessed_transfer;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_overprocessed_transferSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_overprocessed_transfer] 服务对象接口
 */
public interface IStock_overprocessed_transferService extends IService<Stock_overprocessed_transfer>{

    boolean create(Stock_overprocessed_transfer et) ;
    void createBatch(List<Stock_overprocessed_transfer> list) ;
    boolean update(Stock_overprocessed_transfer et) ;
    void updateBatch(List<Stock_overprocessed_transfer> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_overprocessed_transfer get(Long key) ;
    Stock_overprocessed_transfer getDraft(Stock_overprocessed_transfer et) ;
    boolean checkKey(Stock_overprocessed_transfer et) ;
    boolean save(Stock_overprocessed_transfer et) ;
    void saveBatch(List<Stock_overprocessed_transfer> list) ;
    Page<Stock_overprocessed_transfer> searchDefault(Stock_overprocessed_transferSearchContext context) ;
    List<Stock_overprocessed_transfer> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_overprocessed_transfer> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_overprocessed_transfer> selectByPickingId(Long id);
    void resetByPickingId(Long id);
    void resetByPickingId(Collection<Long> ids);
    void removeByPickingId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_overprocessed_transfer> getStockOverprocessedTransferByIds(List<Long> ids) ;
    List<Stock_overprocessed_transfer> getStockOverprocessedTransferByEntities(List<Stock_overprocessed_transfer> entities) ;
}


