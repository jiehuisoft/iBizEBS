package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_state;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_stateSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[fleet_vehicle_state] 服务对象接口
 */
@Component
public class fleet_vehicle_stateFallback implements fleet_vehicle_stateFeignClient{


    public Fleet_vehicle_state create(Fleet_vehicle_state fleet_vehicle_state){
            return null;
     }
    public Boolean createBatch(List<Fleet_vehicle_state> fleet_vehicle_states){
            return false;
     }



    public Page<Fleet_vehicle_state> search(Fleet_vehicle_stateSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Fleet_vehicle_state update(Long id, Fleet_vehicle_state fleet_vehicle_state){
            return null;
     }
    public Boolean updateBatch(List<Fleet_vehicle_state> fleet_vehicle_states){
            return false;
     }


    public Fleet_vehicle_state get(Long id){
            return null;
     }


    public Page<Fleet_vehicle_state> select(){
            return null;
     }

    public Fleet_vehicle_state getDraft(){
            return null;
    }



    public Boolean checkKey(Fleet_vehicle_state fleet_vehicle_state){
            return false;
     }


    public Boolean save(Fleet_vehicle_state fleet_vehicle_state){
            return false;
     }
    public Boolean saveBatch(List<Fleet_vehicle_state> fleet_vehicle_states){
            return false;
     }

    public Page<Fleet_vehicle_state> searchDefault(Fleet_vehicle_stateSearchContext context){
            return null;
     }


}
