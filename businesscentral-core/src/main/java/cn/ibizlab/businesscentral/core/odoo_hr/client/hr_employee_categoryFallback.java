package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee_category;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_employee_categorySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_employee_category] 服务对象接口
 */
@Component
public class hr_employee_categoryFallback implements hr_employee_categoryFeignClient{


    public Page<Hr_employee_category> search(Hr_employee_categorySearchContext context){
            return null;
     }


    public Hr_employee_category get(Long id){
            return null;
     }



    public Hr_employee_category update(Long id, Hr_employee_category hr_employee_category){
            return null;
     }
    public Boolean updateBatch(List<Hr_employee_category> hr_employee_categories){
            return false;
     }



    public Hr_employee_category create(Hr_employee_category hr_employee_category){
            return null;
     }
    public Boolean createBatch(List<Hr_employee_category> hr_employee_categories){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Hr_employee_category> select(){
            return null;
     }

    public Hr_employee_category getDraft(){
            return null;
    }



    public Boolean checkKey(Hr_employee_category hr_employee_category){
            return false;
     }


    public Boolean save(Hr_employee_category hr_employee_category){
            return false;
     }
    public Boolean saveBatch(List<Hr_employee_category> hr_employee_categories){
            return false;
     }

    public Page<Hr_employee_category> searchDefault(Hr_employee_categorySearchContext context){
            return null;
     }


}
