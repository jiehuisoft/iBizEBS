package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_unreconcile;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_unreconcileSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_unreconcile] 服务对象接口
 */
public interface IAccount_unreconcileService extends IService<Account_unreconcile>{

    boolean create(Account_unreconcile et) ;
    void createBatch(List<Account_unreconcile> list) ;
    boolean update(Account_unreconcile et) ;
    void updateBatch(List<Account_unreconcile> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_unreconcile get(Long key) ;
    Account_unreconcile getDraft(Account_unreconcile et) ;
    boolean checkKey(Account_unreconcile et) ;
    boolean save(Account_unreconcile et) ;
    void saveBatch(List<Account_unreconcile> list) ;
    Page<Account_unreconcile> searchDefault(Account_unreconcileSearchContext context) ;
    List<Account_unreconcile> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_unreconcile> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_unreconcile> getAccountUnreconcileByIds(List<Long> ids) ;
    List<Account_unreconcile> getAccountUnreconcileByEntities(List<Account_unreconcile> entities) ;
}


