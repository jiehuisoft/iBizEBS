package cn.ibizlab.businesscentral.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[EMail别名 Mixin]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAIL_ALIAS_MIXIN",resultMap = "Mail_alias_mixinResultMap")
public class Mail_alias_mixin extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 上级记录ID
     */
    @TableField(exist = false)
    @JSONField(name = "alias_parent_thread_id")
    @JsonProperty("alias_parent_thread_id")
    private Integer aliasParentThreadId;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 网域别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;
    /**
     * 最后更新时间
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 创建时间
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 默认值
     */
    @TableField(exist = false)
    @JSONField(name = "alias_defaults")
    @JsonProperty("alias_defaults")
    private String aliasDefaults;
    /**
     * 别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;
    /**
     * 安全联系人别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    private String aliasContact;
    /**
     * 模型别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_model_id")
    @JsonProperty("alias_model_id")
    private Integer aliasModelId;
    /**
     * 记录线索ID
     */
    @TableField(exist = false)
    @JSONField(name = "alias_force_thread_id")
    @JsonProperty("alias_force_thread_id")
    private Integer aliasForceThreadId;
    /**
     * 上级模型
     */
    @TableField(exist = false)
    @JSONField(name = "alias_parent_model_id")
    @JsonProperty("alias_parent_model_id")
    private Integer aliasParentModelId;
    /**
     * 所有者
     */
    @TableField(exist = false)
    @JSONField(name = "alias_user_id")
    @JsonProperty("alias_user_id")
    private Long aliasUserId;
    /**
     * 别名
     */
    @DEField(name = "alias_id")
    @TableField(value = "alias_id")
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Long aliasId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias odooAlias;



    /**
     * 设置 [别名]
     */
    public void setAliasId(Long aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


