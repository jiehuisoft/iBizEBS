package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_contact;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_contactSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_mass_mailing_contact] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-mass-mailing-contact", fallback = mail_mass_mailing_contactFallback.class)
public interface mail_mass_mailing_contactFeignClient {




    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts/search")
    Page<Mail_mass_mailing_contact> search(@RequestBody Mail_mass_mailing_contactSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts")
    Mail_mass_mailing_contact create(@RequestBody Mail_mass_mailing_contact mail_mass_mailing_contact);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts/batch")
    Boolean createBatch(@RequestBody List<Mail_mass_mailing_contact> mail_mass_mailing_contacts);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_contacts/{id}")
    Mail_mass_mailing_contact get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_contacts/{id}")
    Mail_mass_mailing_contact update(@PathVariable("id") Long id,@RequestBody Mail_mass_mailing_contact mail_mass_mailing_contact);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_contacts/batch")
    Boolean updateBatch(@RequestBody List<Mail_mass_mailing_contact> mail_mass_mailing_contacts);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_contacts/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_contacts/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_contacts/select")
    Page<Mail_mass_mailing_contact> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_contacts/getdraft")
    Mail_mass_mailing_contact getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts/checkkey")
    Boolean checkKey(@RequestBody Mail_mass_mailing_contact mail_mass_mailing_contact);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts/save")
    Boolean save(@RequestBody Mail_mass_mailing_contact mail_mass_mailing_contact);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_mass_mailing_contact> mail_mass_mailing_contacts);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts/searchdefault")
    Page<Mail_mass_mailing_contact> searchDefault(@RequestBody Mail_mass_mailing_contactSearchContext context);


}
