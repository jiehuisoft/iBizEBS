package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_productSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_product] 服务对象接口
 */
@Component
public class product_productFallback implements product_productFeignClient{

    public Product_product create(Product_product product_product){
            return null;
     }
    public Boolean createBatch(List<Product_product> product_products){
            return false;
     }

    public Product_product get(Long id){
            return null;
     }


    public Page<Product_product> search(Product_productSearchContext context){
            return null;
     }




    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Product_product update(Long id, Product_product product_product){
            return null;
     }
    public Boolean updateBatch(List<Product_product> product_products){
            return false;
     }


    public Page<Product_product> select(){
            return null;
     }

    public Product_product getDraft(){
            return null;
    }



    public Boolean checkKey(Product_product product_product){
            return false;
     }


    public Boolean save(Product_product product_product){
            return false;
     }
    public Boolean saveBatch(List<Product_product> product_products){
            return false;
     }

    public Page<Product_product> searchDefault(Product_productSearchContext context){
            return null;
     }


    public Page<Product_product> searchMaster(Product_productSearchContext context){
            return null;
     }


}
