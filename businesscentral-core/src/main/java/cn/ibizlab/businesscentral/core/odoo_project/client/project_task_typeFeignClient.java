package cn.ibizlab.businesscentral.core.odoo_project.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_task_type;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_task_typeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[project_task_type] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-project:odoo-project}", contextId = "project-task-type", fallback = project_task_typeFallback.class)
public interface project_task_typeFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/project_task_types/{id}")
    Project_task_type get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/project_task_types/search")
    Page<Project_task_type> search(@RequestBody Project_task_typeSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/project_task_types/{id}")
    Project_task_type update(@PathVariable("id") Long id,@RequestBody Project_task_type project_task_type);

    @RequestMapping(method = RequestMethod.PUT, value = "/project_task_types/batch")
    Boolean updateBatch(@RequestBody List<Project_task_type> project_task_types);



    @RequestMapping(method = RequestMethod.POST, value = "/project_task_types")
    Project_task_type create(@RequestBody Project_task_type project_task_type);

    @RequestMapping(method = RequestMethod.POST, value = "/project_task_types/batch")
    Boolean createBatch(@RequestBody List<Project_task_type> project_task_types);


    @RequestMapping(method = RequestMethod.DELETE, value = "/project_task_types/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/project_task_types/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/project_task_types/select")
    Page<Project_task_type> select();


    @RequestMapping(method = RequestMethod.GET, value = "/project_task_types/getdraft")
    Project_task_type getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/project_task_types/checkkey")
    Boolean checkKey(@RequestBody Project_task_type project_task_type);


    @RequestMapping(method = RequestMethod.POST, value = "/project_task_types/save")
    Boolean save(@RequestBody Project_task_type project_task_type);

    @RequestMapping(method = RequestMethod.POST, value = "/project_task_types/savebatch")
    Boolean saveBatch(@RequestBody List<Project_task_type> project_task_types);



    @RequestMapping(method = RequestMethod.POST, value = "/project_task_types/searchdefault")
    Page<Project_task_type> searchDefault(@RequestBody Project_task_typeSearchContext context);


}
