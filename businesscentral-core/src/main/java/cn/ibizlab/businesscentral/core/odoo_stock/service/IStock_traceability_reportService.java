package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_traceability_report;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_traceability_reportSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_traceability_report] 服务对象接口
 */
public interface IStock_traceability_reportService extends IService<Stock_traceability_report>{

    boolean create(Stock_traceability_report et) ;
    void createBatch(List<Stock_traceability_report> list) ;
    boolean update(Stock_traceability_report et) ;
    void updateBatch(List<Stock_traceability_report> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_traceability_report get(Long key) ;
    Stock_traceability_report getDraft(Stock_traceability_report et) ;
    boolean checkKey(Stock_traceability_report et) ;
    boolean save(Stock_traceability_report et) ;
    void saveBatch(List<Stock_traceability_report> list) ;
    Page<Stock_traceability_report> searchDefault(Stock_traceability_reportSearchContext context) ;
    List<Stock_traceability_report> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_traceability_report> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_traceability_report> getStockTraceabilityReportByIds(List<Long> ids) ;
    List<Stock_traceability_report> getStockTraceabilityReportByEntities(List<Stock_traceability_report> entities) ;
}


