package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_method;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_payment_methodSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_payment_method] 服务对象接口
 */
@Component
public class account_payment_methodFallback implements account_payment_methodFeignClient{

    public Account_payment_method get(Long id){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Account_payment_method create(Account_payment_method account_payment_method){
            return null;
     }
    public Boolean createBatch(List<Account_payment_method> account_payment_methods){
            return false;
     }


    public Page<Account_payment_method> search(Account_payment_methodSearchContext context){
            return null;
     }


    public Account_payment_method update(Long id, Account_payment_method account_payment_method){
            return null;
     }
    public Boolean updateBatch(List<Account_payment_method> account_payment_methods){
            return false;
     }



    public Page<Account_payment_method> select(){
            return null;
     }

    public Account_payment_method getDraft(){
            return null;
    }



    public Boolean checkKey(Account_payment_method account_payment_method){
            return false;
     }


    public Boolean save(Account_payment_method account_payment_method){
            return false;
     }
    public Boolean saveBatch(List<Account_payment_method> account_payment_methods){
            return false;
     }

    public Page<Account_payment_method> searchDefault(Account_payment_methodSearchContext context){
            return null;
     }


}
