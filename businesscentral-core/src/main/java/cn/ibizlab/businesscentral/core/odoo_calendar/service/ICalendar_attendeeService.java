package cn.ibizlab.businesscentral.core.odoo_calendar.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_attendee;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_attendeeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Calendar_attendee] 服务对象接口
 */
public interface ICalendar_attendeeService extends IService<Calendar_attendee>{

    boolean create(Calendar_attendee et) ;
    void createBatch(List<Calendar_attendee> list) ;
    boolean update(Calendar_attendee et) ;
    void updateBatch(List<Calendar_attendee> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Calendar_attendee get(Long key) ;
    Calendar_attendee getDraft(Calendar_attendee et) ;
    boolean checkKey(Calendar_attendee et) ;
    boolean save(Calendar_attendee et) ;
    void saveBatch(List<Calendar_attendee> list) ;
    Page<Calendar_attendee> searchDefault(Calendar_attendeeSearchContext context) ;
    List<Calendar_attendee> selectByEventId(Long id);
    void removeByEventId(Collection<Long> ids);
    void removeByEventId(Long id);
    List<Calendar_attendee> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Calendar_attendee> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Calendar_attendee> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Calendar_attendee> getCalendarAttendeeByIds(List<Long> ids) ;
    List<Calendar_attendee> getCalendarAttendeeByEntities(List<Calendar_attendee> entities) ;
}


