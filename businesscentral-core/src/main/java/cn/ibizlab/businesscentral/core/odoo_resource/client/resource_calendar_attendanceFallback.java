package cn.ibizlab.businesscentral.core.odoo_resource.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar_attendance;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_calendar_attendanceSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[resource_calendar_attendance] 服务对象接口
 */
@Component
public class resource_calendar_attendanceFallback implements resource_calendar_attendanceFeignClient{


    public Resource_calendar_attendance update(Long id, Resource_calendar_attendance resource_calendar_attendance){
            return null;
     }
    public Boolean updateBatch(List<Resource_calendar_attendance> resource_calendar_attendances){
            return false;
     }


    public Resource_calendar_attendance get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Resource_calendar_attendance create(Resource_calendar_attendance resource_calendar_attendance){
            return null;
     }
    public Boolean createBatch(List<Resource_calendar_attendance> resource_calendar_attendances){
            return false;
     }

    public Page<Resource_calendar_attendance> search(Resource_calendar_attendanceSearchContext context){
            return null;
     }



    public Page<Resource_calendar_attendance> select(){
            return null;
     }

    public Resource_calendar_attendance getDraft(){
            return null;
    }



    public Boolean checkKey(Resource_calendar_attendance resource_calendar_attendance){
            return false;
     }


    public Boolean save(Resource_calendar_attendance resource_calendar_attendance){
            return false;
     }
    public Boolean saveBatch(List<Resource_calendar_attendance> resource_calendar_attendances){
            return false;
     }

    public Page<Resource_calendar_attendance> searchDefault(Resource_calendar_attendanceSearchContext context){
            return null;
     }


}
