package cn.ibizlab.businesscentral.core.odoo_calendar.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_eventSearchContext;
import cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_eventService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_calendar.mapper.Calendar_eventMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[活动] 服务对象接口实现
 */
@Slf4j
@Service("Calendar_eventServiceImpl")
public class Calendar_eventServiceImpl extends EBSServiceImpl<Calendar_eventMapper, Calendar_event> implements ICalendar_eventService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_attendeeService calendarAttendeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leaveService hrLeaveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_activityService mailActivityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_leadService crmLeadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_applicantService hrApplicantService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "calendar.event" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Calendar_event et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ICalendar_eventService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Calendar_event> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Calendar_event et) {
        Calendar_event old = new Calendar_event() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ICalendar_eventService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ICalendar_eventService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Calendar_event> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        calendarAttendeeService.removeByEventId(key);
        hrLeaveService.resetByMeetingId(key);
        mailActivityService.removeByCalendarEventId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        calendarAttendeeService.removeByEventId(idList);
        hrLeaveService.resetByMeetingId(idList);
        mailActivityService.removeByCalendarEventId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Calendar_event get(Long key) {
        Calendar_event et = getById(key);
        if(et==null){
            et=new Calendar_event();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Calendar_event getDraft(Calendar_event et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Calendar_event et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Calendar_event et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Calendar_event et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Calendar_event> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Calendar_event> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Calendar_event> selectByOpportunityId(Long id) {
        return baseMapper.selectByOpportunityId(id);
    }
    @Override
    public void resetByOpportunityId(Long id) {
        this.update(new UpdateWrapper<Calendar_event>().set("opportunity_id",null).eq("opportunity_id",id));
    }

    @Override
    public void resetByOpportunityId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Calendar_event>().set("opportunity_id",null).in("opportunity_id",ids));
    }

    @Override
    public void removeByOpportunityId(Long id) {
        this.remove(new QueryWrapper<Calendar_event>().eq("opportunity_id",id));
    }

	@Override
    public List<Calendar_event> selectByApplicantId(Long id) {
        return baseMapper.selectByApplicantId(id);
    }
    @Override
    public void resetByApplicantId(Long id) {
        this.update(new UpdateWrapper<Calendar_event>().set("applicant_id",null).eq("applicant_id",id));
    }

    @Override
    public void resetByApplicantId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Calendar_event>().set("applicant_id",null).in("applicant_id",ids));
    }

    @Override
    public void removeByApplicantId(Long id) {
        this.remove(new QueryWrapper<Calendar_event>().eq("applicant_id",id));
    }

	@Override
    public List<Calendar_event> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Calendar_event>().eq("create_uid",id));
    }

	@Override
    public List<Calendar_event> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Calendar_event>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Calendar_event>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Calendar_event>().eq("user_id",id));
    }

	@Override
    public List<Calendar_event> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Calendar_event>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Calendar_event> searchDefault(Calendar_eventSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Calendar_event> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Calendar_event>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Calendar_event et){
        //实体关系[DER1N_CALENDAR_EVENT__CRM_LEAD__OPPORTUNITY_ID]
        if(!ObjectUtils.isEmpty(et.getOpportunityId())){
            cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead odooOpportunity=et.getOdooOpportunity();
            if(ObjectUtils.isEmpty(odooOpportunity)){
                cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead majorEntity=crmLeadService.get(et.getOpportunityId());
                et.setOdooOpportunity(majorEntity);
                odooOpportunity=majorEntity;
            }
            et.setOpportunityIdText(odooOpportunity.getName());
        }
        //实体关系[DER1N_CALENDAR_EVENT__HR_APPLICANT__APPLICANT_ID]
        if(!ObjectUtils.isEmpty(et.getApplicantId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_applicant odooApplicant=et.getOdooApplicant();
            if(ObjectUtils.isEmpty(odooApplicant)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_applicant majorEntity=hrApplicantService.get(et.getApplicantId());
                et.setOdooApplicant(majorEntity);
                odooApplicant=majorEntity;
            }
            et.setApplicantIdText(odooApplicant.getName());
        }
        //实体关系[DER1N_CALENDAR_EVENT__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_CALENDAR_EVENT__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
            et.setPartnerId(odooUser.getPartnerId());
        }
        //实体关系[DER1N_CALENDAR_EVENT__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Calendar_event> getCalendarEventByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Calendar_event> getCalendarEventByEntities(List<Calendar_event> entities) {
        List ids =new ArrayList();
        for(Calendar_event entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



