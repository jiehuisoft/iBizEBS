package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_common_report;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_common_reportSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_common_report] 服务对象接口
 */
public interface IAccount_common_reportService extends IService<Account_common_report>{

    boolean create(Account_common_report et) ;
    void createBatch(List<Account_common_report> list) ;
    boolean update(Account_common_report et) ;
    void updateBatch(List<Account_common_report> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_common_report get(Long key) ;
    Account_common_report getDraft(Account_common_report et) ;
    boolean checkKey(Account_common_report et) ;
    boolean save(Account_common_report et) ;
    void saveBatch(List<Account_common_report> list) ;
    Page<Account_common_report> searchDefault(Account_common_reportSearchContext context) ;
    List<Account_common_report> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_common_report> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_common_report> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_common_report> getAccountCommonReportByIds(List<Long> ids) ;
    List<Account_common_report> getAccountCommonReportByEntities(List<Account_common_report> entities) ;
}


