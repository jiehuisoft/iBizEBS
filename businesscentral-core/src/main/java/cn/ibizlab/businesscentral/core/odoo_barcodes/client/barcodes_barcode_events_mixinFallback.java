package cn.ibizlab.businesscentral.core.odoo_barcodes.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin;
import cn.ibizlab.businesscentral.core.odoo_barcodes.filter.Barcodes_barcode_events_mixinSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[barcodes_barcode_events_mixin] 服务对象接口
 */
@Component
public class barcodes_barcode_events_mixinFallback implements barcodes_barcode_events_mixinFeignClient{


    public Barcodes_barcode_events_mixin create(Barcodes_barcode_events_mixin barcodes_barcode_events_mixin){
            return null;
     }
    public Boolean createBatch(List<Barcodes_barcode_events_mixin> barcodes_barcode_events_mixins){
            return false;
     }

    public Barcodes_barcode_events_mixin update(Long id, Barcodes_barcode_events_mixin barcodes_barcode_events_mixin){
            return null;
     }
    public Boolean updateBatch(List<Barcodes_barcode_events_mixin> barcodes_barcode_events_mixins){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Barcodes_barcode_events_mixin> search(Barcodes_barcode_events_mixinSearchContext context){
            return null;
     }


    public Barcodes_barcode_events_mixin get(Long id){
            return null;
     }




    public Page<Barcodes_barcode_events_mixin> select(){
            return null;
     }

    public Barcodes_barcode_events_mixin getDraft(){
            return null;
    }



    public Boolean checkKey(Barcodes_barcode_events_mixin barcodes_barcode_events_mixin){
            return false;
     }


    public Boolean save(Barcodes_barcode_events_mixin barcodes_barcode_events_mixin){
            return false;
     }
    public Boolean saveBatch(List<Barcodes_barcode_events_mixin> barcodes_barcode_events_mixins){
            return false;
     }

    public Page<Barcodes_barcode_events_mixin> searchDefault(Barcodes_barcode_events_mixinSearchContext context){
            return null;
     }


}
