package cn.ibizlab.businesscentral.core.odoo_gamification.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal_wizard;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_goal_wizardSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Gamification_goal_wizardMapper extends BaseMapper<Gamification_goal_wizard>{

    Page<Gamification_goal_wizard> searchDefault(IPage page, @Param("srf") Gamification_goal_wizardSearchContext context, @Param("ew") Wrapper<Gamification_goal_wizard> wrapper) ;
    @Override
    Gamification_goal_wizard selectById(Serializable id);
    @Override
    int insert(Gamification_goal_wizard entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Gamification_goal_wizard entity);
    @Override
    int update(@Param(Constants.ENTITY) Gamification_goal_wizard entity, @Param("ew") Wrapper<Gamification_goal_wizard> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Gamification_goal_wizard> selectByGoalId(@Param("id") Serializable id) ;

    List<Gamification_goal_wizard> selectByCreateUid(@Param("id") Serializable id) ;

    List<Gamification_goal_wizard> selectByWriteUid(@Param("id") Serializable id) ;


}
