package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message_subtype;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_message_subtypeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_message_subtype] 服务对象接口
 */
public interface IMail_message_subtypeService extends IService<Mail_message_subtype>{

    boolean create(Mail_message_subtype et) ;
    void createBatch(List<Mail_message_subtype> list) ;
    boolean update(Mail_message_subtype et) ;
    void updateBatch(List<Mail_message_subtype> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_message_subtype get(Long key) ;
    Mail_message_subtype getDraft(Mail_message_subtype et) ;
    boolean checkKey(Mail_message_subtype et) ;
    boolean save(Mail_message_subtype et) ;
    void saveBatch(List<Mail_message_subtype> list) ;
    Page<Mail_message_subtype> searchDefault(Mail_message_subtypeSearchContext context) ;
    Page<Mail_message_subtype> searchDefaultEx(Mail_message_subtypeSearchContext context) ;
    List<Mail_message_subtype> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Mail_message_subtype> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_message_subtype> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_message_subtype> getMailMessageSubtypeByIds(List<Long> ids) ;
    List<Mail_message_subtype> getMailMessageSubtypeByEntities(List<Mail_message_subtype> entities) ;
}


