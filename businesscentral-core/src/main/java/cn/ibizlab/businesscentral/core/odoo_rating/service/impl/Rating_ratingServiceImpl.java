package cn.ibizlab.businesscentral.core.odoo_rating.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_rating.domain.Rating_rating;
import cn.ibizlab.businesscentral.core.odoo_rating.filter.Rating_ratingSearchContext;
import cn.ibizlab.businesscentral.core.odoo_rating.service.IRating_ratingService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_rating.mapper.Rating_ratingMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[评级] 服务对象接口实现
 */
@Slf4j
@Service("Rating_ratingServiceImpl")
public class Rating_ratingServiceImpl extends EBSServiceImpl<Rating_ratingMapper, Rating_rating> implements IRating_ratingService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_messageService mailMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "rating.rating" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Rating_rating et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRating_ratingService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Rating_rating> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Rating_rating et) {
        Rating_rating old = new Rating_rating() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRating_ratingService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRating_ratingService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Rating_rating> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Rating_rating get(Long key) {
        Rating_rating et = getById(key);
        if(et==null){
            et=new Rating_rating();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Rating_rating getDraft(Rating_rating et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Rating_rating et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Rating_rating et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Rating_rating et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Rating_rating> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Rating_rating> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Rating_rating> selectByMessageId(Long id) {
        return baseMapper.selectByMessageId(id);
    }
    @Override
    public void resetByMessageId(Long id) {
        this.update(new UpdateWrapper<Rating_rating>().set("message_id",null).eq("message_id",id));
    }

    @Override
    public void resetByMessageId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Rating_rating>().set("message_id",null).in("message_id",ids));
    }

    @Override
    public void removeByMessageId(Long id) {
        this.remove(new QueryWrapper<Rating_rating>().eq("message_id",id));
    }

	@Override
    public List<Rating_rating> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Rating_rating>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Rating_rating>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Rating_rating>().eq("partner_id",id));
    }

	@Override
    public List<Rating_rating> selectByRatedPartnerId(Long id) {
        return baseMapper.selectByRatedPartnerId(id);
    }
    @Override
    public void resetByRatedPartnerId(Long id) {
        this.update(new UpdateWrapper<Rating_rating>().set("rated_partner_id",null).eq("rated_partner_id",id));
    }

    @Override
    public void resetByRatedPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Rating_rating>().set("rated_partner_id",null).in("rated_partner_id",ids));
    }

    @Override
    public void removeByRatedPartnerId(Long id) {
        this.remove(new QueryWrapper<Rating_rating>().eq("rated_partner_id",id));
    }

	@Override
    public List<Rating_rating> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Rating_rating>().eq("create_uid",id));
    }

	@Override
    public List<Rating_rating> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Rating_rating>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Rating_rating> searchDefault(Rating_ratingSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Rating_rating> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Rating_rating>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Rating_rating et){
        //实体关系[DER1N_RATING_RATING__MAIL_MESSAGE__MESSAGE_ID]
        if(!ObjectUtils.isEmpty(et.getMessageId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message odooMessage=et.getOdooMessage();
            if(ObjectUtils.isEmpty(odooMessage)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message majorEntity=mailMessageService.get(et.getMessageId());
                et.setOdooMessage(majorEntity);
                odooMessage=majorEntity;
            }
            et.setWebsitePublished(odooMessage.getWebsitePublished());
        }
        //实体关系[DER1N_RATING_RATING__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_RATING_RATING__RES_PARTNER__RATED_PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getRatedPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooRatedPartner=et.getOdooRatedPartner();
            if(ObjectUtils.isEmpty(odooRatedPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getRatedPartnerId());
                et.setOdooRatedPartner(majorEntity);
                odooRatedPartner=majorEntity;
            }
            et.setRatedPartnerIdText(odooRatedPartner.getName());
        }
        //实体关系[DER1N_RATING_RATING__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_RATING_RATING__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Rating_rating> getRatingRatingByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Rating_rating> getRatingRatingByEntities(List<Rating_rating> entities) {
        List ids =new ArrayList();
        for(Rating_rating entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



