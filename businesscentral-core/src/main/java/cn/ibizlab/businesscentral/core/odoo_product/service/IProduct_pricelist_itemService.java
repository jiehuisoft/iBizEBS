package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_pricelist_itemSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_pricelist_item] 服务对象接口
 */
public interface IProduct_pricelist_itemService extends IService<Product_pricelist_item>{

    boolean create(Product_pricelist_item et) ;
    void createBatch(List<Product_pricelist_item> list) ;
    boolean update(Product_pricelist_item et) ;
    void updateBatch(List<Product_pricelist_item> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_pricelist_item get(Long key) ;
    Product_pricelist_item getDraft(Product_pricelist_item et) ;
    boolean checkKey(Product_pricelist_item et) ;
    boolean save(Product_pricelist_item et) ;
    void saveBatch(List<Product_pricelist_item> list) ;
    Page<Product_pricelist_item> searchDefault(Product_pricelist_itemSearchContext context) ;
    List<Product_pricelist_item> selectByCategId(Long id);
    void removeByCategId(Collection<Long> ids);
    void removeByCategId(Long id);
    List<Product_pricelist_item> selectByBasePricelistId(Long id);
    void resetByBasePricelistId(Long id);
    void resetByBasePricelistId(Collection<Long> ids);
    void removeByBasePricelistId(Long id);
    List<Product_pricelist_item> selectByPricelistId(Long id);
    void removeByPricelistId(Collection<Long> ids);
    void removeByPricelistId(Long id);
    List<Product_pricelist_item> selectByProductId(Long id);
    void removeByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Product_pricelist_item> selectByProductTmplId(Long id);
    void removeByProductTmplId(Collection<Long> ids);
    void removeByProductTmplId(Long id);
    List<Product_pricelist_item> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Product_pricelist_item> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Product_pricelist_item> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_pricelist_item> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_pricelist_item> getProductPricelistItemByIds(List<Long> ids) ;
    List<Product_pricelist_item> getProductPricelistItemByEntities(List<Product_pricelist_item> entities) ;
}


