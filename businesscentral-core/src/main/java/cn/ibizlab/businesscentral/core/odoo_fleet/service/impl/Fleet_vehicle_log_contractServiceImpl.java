package cn.ibizlab.businesscentral.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_log_contractSearchContext;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_contractService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_fleet.mapper.Fleet_vehicle_log_contractMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[车辆合同信息] 服务对象接口实现
 */
@Slf4j
@Service("Fleet_vehicle_log_contractServiceImpl")
public class Fleet_vehicle_log_contractServiceImpl extends EBSServiceImpl<Fleet_vehicle_log_contractMapper, Fleet_vehicle_log_contract> implements IFleet_vehicle_log_contractService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_costService fleetVehicleCostService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "fleet.vehicle.log.contract" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Fleet_vehicle_log_contract et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IFleet_vehicle_log_contractService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Fleet_vehicle_log_contract> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Fleet_vehicle_log_contract et) {
        Fleet_vehicle_log_contract old = new Fleet_vehicle_log_contract() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IFleet_vehicle_log_contractService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IFleet_vehicle_log_contractService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Fleet_vehicle_log_contract> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        fleetVehicleCostService.resetByContractId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        fleetVehicleCostService.resetByContractId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Fleet_vehicle_log_contract get(Long key) {
        Fleet_vehicle_log_contract et = getById(key);
        if(et==null){
            et=new Fleet_vehicle_log_contract();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Fleet_vehicle_log_contract getDraft(Fleet_vehicle_log_contract et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Fleet_vehicle_log_contract et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Fleet_vehicle_log_contract et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Fleet_vehicle_log_contract et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Fleet_vehicle_log_contract> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Fleet_vehicle_log_contract> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Fleet_vehicle_log_contract> selectByCostId(Long id) {
        return baseMapper.selectByCostId(id);
    }
    @Override
    public void removeByCostId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Fleet_vehicle_log_contract>().in("cost_id",ids));
    }

    @Override
    public void removeByCostId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_log_contract>().eq("cost_id",id));
    }

	@Override
    public List<Fleet_vehicle_log_contract> selectByInsurerId(Long id) {
        return baseMapper.selectByInsurerId(id);
    }
    @Override
    public void resetByInsurerId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle_log_contract>().set("insurer_id",null).eq("insurer_id",id));
    }

    @Override
    public void resetByInsurerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle_log_contract>().set("insurer_id",null).in("insurer_id",ids));
    }

    @Override
    public void removeByInsurerId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_log_contract>().eq("insurer_id",id));
    }

	@Override
    public List<Fleet_vehicle_log_contract> selectByPurchaserId(Long id) {
        return baseMapper.selectByPurchaserId(id);
    }
    @Override
    public void resetByPurchaserId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle_log_contract>().set("purchaser_id",null).eq("purchaser_id",id));
    }

    @Override
    public void resetByPurchaserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle_log_contract>().set("purchaser_id",null).in("purchaser_id",ids));
    }

    @Override
    public void removeByPurchaserId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_log_contract>().eq("purchaser_id",id));
    }

	@Override
    public List<Fleet_vehicle_log_contract> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_log_contract>().eq("create_uid",id));
    }

	@Override
    public List<Fleet_vehicle_log_contract> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Fleet_vehicle_log_contract>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Fleet_vehicle_log_contract>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_log_contract>().eq("user_id",id));
    }

	@Override
    public List<Fleet_vehicle_log_contract> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Fleet_vehicle_log_contract>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Fleet_vehicle_log_contract> searchDefault(Fleet_vehicle_log_contractSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Fleet_vehicle_log_contract> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Fleet_vehicle_log_contract>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Fleet_vehicle_log_contract et){
        //实体关系[DER1N_FLEET_VEHICLE_LOG_CONTRACT__FLEET_VEHICLE_COST__COST_ID]
        if(!ObjectUtils.isEmpty(et.getCostId())){
            cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost odooCost=et.getOdooCost();
            if(ObjectUtils.isEmpty(odooCost)){
                cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost majorEntity=fleetVehicleCostService.get(et.getCostId());
                et.setOdooCost(majorEntity);
                odooCost=majorEntity;
            }
            et.setVehicleId(odooCost.getVehicleId());
            et.setAutoGenerated(odooCost.getAutoGenerated());
            et.setOdometerId(odooCost.getOdometerId());
            et.setCostType(odooCost.getCostType());
            et.setParentId(odooCost.getParentId());
            et.setCostSubtypeId(odooCost.getCostSubtypeId());
            et.setCostAmount(odooCost.getAmount());
            et.setAmount(odooCost.getAmount());
            et.setCostIdText(odooCost.getName());
            et.setDescription(odooCost.getDescription());
            et.setDate(odooCost.getDate());
            et.setOdometerUnit(odooCost.getOdometerUnit());
            et.setContractId(odooCost.getContractId());
        }
        //实体关系[DER1N_FLEET_VEHICLE_LOG_CONTRACT__RES_PARTNER__INSURER_ID]
        if(!ObjectUtils.isEmpty(et.getInsurerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooInsurer=et.getOdooInsurer();
            if(ObjectUtils.isEmpty(odooInsurer)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getInsurerId());
                et.setOdooInsurer(majorEntity);
                odooInsurer=majorEntity;
            }
            et.setInsurerIdText(odooInsurer.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE_LOG_CONTRACT__RES_PARTNER__PURCHASER_ID]
        if(!ObjectUtils.isEmpty(et.getPurchaserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPurchaser=et.getOdooPurchaser();
            if(ObjectUtils.isEmpty(odooPurchaser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPurchaserId());
                et.setOdooPurchaser(majorEntity);
                odooPurchaser=majorEntity;
            }
            et.setPurchaserIdText(odooPurchaser.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE_LOG_CONTRACT__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE_LOG_CONTRACT__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_FLEET_VEHICLE_LOG_CONTRACT__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Fleet_vehicle_log_contract> getFleetVehicleLogContractByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Fleet_vehicle_log_contract> getFleetVehicleLogContractByEntities(List<Fleet_vehicle_log_contract> entities) {
        List ids =new ArrayList();
        for(Fleet_vehicle_log_contract entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



