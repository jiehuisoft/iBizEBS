package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency_rate;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_currency_rateSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_currency_rate] 服务对象接口
 */
public interface IRes_currency_rateService extends IService<Res_currency_rate>{

    boolean create(Res_currency_rate et) ;
    void createBatch(List<Res_currency_rate> list) ;
    boolean update(Res_currency_rate et) ;
    void updateBatch(List<Res_currency_rate> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_currency_rate get(Long key) ;
    Res_currency_rate getDraft(Res_currency_rate et) ;
    boolean checkKey(Res_currency_rate et) ;
    boolean save(Res_currency_rate et) ;
    void saveBatch(List<Res_currency_rate> list) ;
    Page<Res_currency_rate> searchDefault(Res_currency_rateSearchContext context) ;
    List<Res_currency_rate> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Res_currency_rate> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Res_currency_rate> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_currency_rate> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_currency_rate> getResCurrencyRateByIds(List<Long> ids) ;
    List<Res_currency_rate> getResCurrencyRateByEntities(List<Res_currency_rate> entities) ;
}


