package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_picking_typeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_picking_type] 服务对象接口
 */
@Component
public class stock_picking_typeFallback implements stock_picking_typeFeignClient{

    public Page<Stock_picking_type> search(Stock_picking_typeSearchContext context){
            return null;
     }




    public Stock_picking_type create(Stock_picking_type stock_picking_type){
            return null;
     }
    public Boolean createBatch(List<Stock_picking_type> stock_picking_types){
            return false;
     }

    public Stock_picking_type update(Long id, Stock_picking_type stock_picking_type){
            return null;
     }
    public Boolean updateBatch(List<Stock_picking_type> stock_picking_types){
            return false;
     }


    public Stock_picking_type get(Long id){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Stock_picking_type> select(){
            return null;
     }

    public Stock_picking_type getDraft(){
            return null;
    }



    public Boolean checkKey(Stock_picking_type stock_picking_type){
            return false;
     }


    public Boolean save(Stock_picking_type stock_picking_type){
            return false;
     }
    public Boolean saveBatch(List<Stock_picking_type> stock_picking_types){
            return false;
     }

    public Page<Stock_picking_type> searchDefault(Stock_picking_typeSearchContext context){
            return null;
     }


}
