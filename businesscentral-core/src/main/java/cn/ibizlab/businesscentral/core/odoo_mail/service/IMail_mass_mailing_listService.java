package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_list;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_listSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_mass_mailing_list] 服务对象接口
 */
public interface IMail_mass_mailing_listService extends IService<Mail_mass_mailing_list>{

    boolean create(Mail_mass_mailing_list et) ;
    void createBatch(List<Mail_mass_mailing_list> list) ;
    boolean update(Mail_mass_mailing_list et) ;
    void updateBatch(List<Mail_mass_mailing_list> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_mass_mailing_list get(Long key) ;
    Mail_mass_mailing_list getDraft(Mail_mass_mailing_list et) ;
    boolean checkKey(Mail_mass_mailing_list et) ;
    boolean save(Mail_mass_mailing_list et) ;
    void saveBatch(List<Mail_mass_mailing_list> list) ;
    Page<Mail_mass_mailing_list> searchDefault(Mail_mass_mailing_listSearchContext context) ;
    List<Mail_mass_mailing_list> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_mass_mailing_list> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_mass_mailing_list> getMailMassMailingListByIds(List<Long> ids) ;
    List<Mail_mass_mailing_list> getMailMassMailingListByEntities(List<Mail_mass_mailing_list> entities) ;
}


