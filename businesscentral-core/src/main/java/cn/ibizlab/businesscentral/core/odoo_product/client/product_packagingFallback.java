package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_packaging;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_packagingSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_packaging] 服务对象接口
 */
@Component
public class product_packagingFallback implements product_packagingFeignClient{


    public Page<Product_packaging> search(Product_packagingSearchContext context){
            return null;
     }



    public Product_packaging get(Long id){
            return null;
     }



    public Product_packaging create(Product_packaging product_packaging){
            return null;
     }
    public Boolean createBatch(List<Product_packaging> product_packagings){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Product_packaging update(Long id, Product_packaging product_packaging){
            return null;
     }
    public Boolean updateBatch(List<Product_packaging> product_packagings){
            return false;
     }


    public Page<Product_packaging> select(){
            return null;
     }

    public Product_packaging getDraft(){
            return null;
    }



    public Boolean checkKey(Product_packaging product_packaging){
            return false;
     }


    public Boolean save(Product_packaging product_packaging){
            return false;
     }
    public Boolean saveBatch(List<Product_packaging> product_packagings){
            return false;
     }

    public Page<Product_packaging> searchDefault(Product_packagingSearchContext context){
            return null;
     }


}
