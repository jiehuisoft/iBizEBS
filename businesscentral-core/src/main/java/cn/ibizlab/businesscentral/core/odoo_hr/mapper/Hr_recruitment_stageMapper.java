package cn.ibizlab.businesscentral.core.odoo_hr.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_stage;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_recruitment_stageSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Hr_recruitment_stageMapper extends BaseMapper<Hr_recruitment_stage>{

    Page<Hr_recruitment_stage> searchDefault(IPage page, @Param("srf") Hr_recruitment_stageSearchContext context, @Param("ew") Wrapper<Hr_recruitment_stage> wrapper) ;
    @Override
    Hr_recruitment_stage selectById(Serializable id);
    @Override
    int insert(Hr_recruitment_stage entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Hr_recruitment_stage entity);
    @Override
    int update(@Param(Constants.ENTITY) Hr_recruitment_stage entity, @Param("ew") Wrapper<Hr_recruitment_stage> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Hr_recruitment_stage> selectByJobId(@Param("id") Serializable id) ;

    List<Hr_recruitment_stage> selectByTemplateId(@Param("id") Serializable id) ;

    List<Hr_recruitment_stage> selectByCreateUid(@Param("id") Serializable id) ;

    List<Hr_recruitment_stage> selectByWriteUid(@Param("id") Serializable id) ;


}
