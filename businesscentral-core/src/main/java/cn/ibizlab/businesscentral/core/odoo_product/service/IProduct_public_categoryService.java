package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_public_category;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_public_categorySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_public_category] 服务对象接口
 */
public interface IProduct_public_categoryService extends IService<Product_public_category>{

    boolean create(Product_public_category et) ;
    void createBatch(List<Product_public_category> list) ;
    boolean update(Product_public_category et) ;
    void updateBatch(List<Product_public_category> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_public_category get(Long key) ;
    Product_public_category getDraft(Product_public_category et) ;
    boolean checkKey(Product_public_category et) ;
    boolean save(Product_public_category et) ;
    void saveBatch(List<Product_public_category> list) ;
    Page<Product_public_category> searchDefault(Product_public_categorySearchContext context) ;
    List<Product_public_category> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Product_public_category> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_public_category> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_public_category> getProductPublicCategoryByIds(List<Long> ids) ;
    List<Product_public_category> getProductPublicCategoryByEntities(List<Product_public_category> entities) ;
}


