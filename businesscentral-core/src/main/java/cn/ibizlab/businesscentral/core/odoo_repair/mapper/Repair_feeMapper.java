package cn.ibizlab.businesscentral.core.odoo_repair.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_fee;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_feeSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Repair_feeMapper extends BaseMapper<Repair_fee>{

    Page<Repair_fee> searchDefault(IPage page, @Param("srf") Repair_feeSearchContext context, @Param("ew") Wrapper<Repair_fee> wrapper) ;
    @Override
    Repair_fee selectById(Serializable id);
    @Override
    int insert(Repair_fee entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Repair_fee entity);
    @Override
    int update(@Param(Constants.ENTITY) Repair_fee entity, @Param("ew") Wrapper<Repair_fee> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Repair_fee> selectByInvoiceLineId(@Param("id") Serializable id) ;

    List<Repair_fee> selectByProductId(@Param("id") Serializable id) ;

    List<Repair_fee> selectByRepairId(@Param("id") Serializable id) ;

    List<Repair_fee> selectByCreateUid(@Param("id") Serializable id) ;

    List<Repair_fee> selectByWriteUid(@Param("id") Serializable id) ;

    List<Repair_fee> selectByProductUom(@Param("id") Serializable id) ;


}
