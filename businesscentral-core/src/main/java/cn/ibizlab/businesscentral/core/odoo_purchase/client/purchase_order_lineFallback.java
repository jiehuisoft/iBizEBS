package cn.ibizlab.businesscentral.core.odoo_purchase.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_order_lineSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[purchase_order_line] 服务对象接口
 */
@Component
public class purchase_order_lineFallback implements purchase_order_lineFeignClient{



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Purchase_order_line get(Long id){
            return null;
     }


    public Purchase_order_line create(Purchase_order_line purchase_order_line){
            return null;
     }
    public Boolean createBatch(List<Purchase_order_line> purchase_order_lines){
            return false;
     }

    public Page<Purchase_order_line> search(Purchase_order_lineSearchContext context){
            return null;
     }


    public Purchase_order_line update(Long id, Purchase_order_line purchase_order_line){
            return null;
     }
    public Boolean updateBatch(List<Purchase_order_line> purchase_order_lines){
            return false;
     }


    public Page<Purchase_order_line> select(){
            return null;
     }

    public Purchase_order_line getDraft(){
            return null;
    }



    public Purchase_order_line calc_amount( Long id, Purchase_order_line purchase_order_line){
            return null;
     }

    public Purchase_order_line calc_price( Long id, Purchase_order_line purchase_order_line){
            return null;
     }

    public Boolean checkKey(Purchase_order_line purchase_order_line){
            return false;
     }


    public Purchase_order_line product_change( Long id, Purchase_order_line purchase_order_line){
            return null;
     }

    public Boolean save(Purchase_order_line purchase_order_line){
            return false;
     }
    public Boolean saveBatch(List<Purchase_order_line> purchase_order_lines){
            return false;
     }

    public Page<Purchase_order_line> searchCalc_order_amount(Purchase_order_lineSearchContext context){
            return null;
     }


    public Page<Purchase_order_line> searchDefault(Purchase_order_lineSearchContext context){
            return null;
     }


}
