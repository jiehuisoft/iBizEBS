package cn.ibizlab.businesscentral.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_report;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_reportSearchContext;
import cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_reportService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_purchase.mapper.Purchase_reportMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[采购报表] 服务对象接口实现
 */
@Slf4j
@Service("Purchase_reportServiceImpl")
public class Purchase_reportServiceImpl extends EBSServiceImpl<Purchase_reportMapper, Purchase_report> implements IPurchase_reportService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_positionService accountFiscalPositionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_categoryService productCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_templateService productTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_countryService resCountryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService stockWarehouseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "purchase.report" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Purchase_report et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_reportService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Purchase_report> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Purchase_report et) {
        Purchase_report old = new Purchase_report() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_reportService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IPurchase_reportService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Purchase_report> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Purchase_report get(Long key) {
        Purchase_report et = getById(key);
        if(et==null){
            et=new Purchase_report();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Purchase_report getDraft(Purchase_report et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Purchase_report et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Purchase_report et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Purchase_report et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Purchase_report> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Purchase_report> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Purchase_report> selectByAccountAnalyticId(Long id) {
        return baseMapper.selectByAccountAnalyticId(id);
    }
    @Override
    public void resetByAccountAnalyticId(Long id) {
        this.update(new UpdateWrapper<Purchase_report>().set("account_analytic_id",null).eq("account_analytic_id",id));
    }

    @Override
    public void resetByAccountAnalyticId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_report>().set("account_analytic_id",null).in("account_analytic_id",ids));
    }

    @Override
    public void removeByAccountAnalyticId(Long id) {
        this.remove(new QueryWrapper<Purchase_report>().eq("account_analytic_id",id));
    }

	@Override
    public List<Purchase_report> selectByFiscalPositionId(Long id) {
        return baseMapper.selectByFiscalPositionId(id);
    }
    @Override
    public void resetByFiscalPositionId(Long id) {
        this.update(new UpdateWrapper<Purchase_report>().set("fiscal_position_id",null).eq("fiscal_position_id",id));
    }

    @Override
    public void resetByFiscalPositionId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_report>().set("fiscal_position_id",null).in("fiscal_position_id",ids));
    }

    @Override
    public void removeByFiscalPositionId(Long id) {
        this.remove(new QueryWrapper<Purchase_report>().eq("fiscal_position_id",id));
    }

	@Override
    public List<Purchase_report> selectByCategoryId(Long id) {
        return baseMapper.selectByCategoryId(id);
    }
    @Override
    public void resetByCategoryId(Long id) {
        this.update(new UpdateWrapper<Purchase_report>().set("category_id",null).eq("category_id",id));
    }

    @Override
    public void resetByCategoryId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_report>().set("category_id",null).in("category_id",ids));
    }

    @Override
    public void removeByCategoryId(Long id) {
        this.remove(new QueryWrapper<Purchase_report>().eq("category_id",id));
    }

	@Override
    public List<Purchase_report> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Purchase_report>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_report>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Purchase_report>().eq("product_id",id));
    }

	@Override
    public List<Purchase_report> selectByProductTmplId(Long id) {
        return baseMapper.selectByProductTmplId(id);
    }
    @Override
    public void resetByProductTmplId(Long id) {
        this.update(new UpdateWrapper<Purchase_report>().set("product_tmpl_id",null).eq("product_tmpl_id",id));
    }

    @Override
    public void resetByProductTmplId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_report>().set("product_tmpl_id",null).in("product_tmpl_id",ids));
    }

    @Override
    public void removeByProductTmplId(Long id) {
        this.remove(new QueryWrapper<Purchase_report>().eq("product_tmpl_id",id));
    }

	@Override
    public List<Purchase_report> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Purchase_report>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_report>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Purchase_report>().eq("company_id",id));
    }

	@Override
    public List<Purchase_report> selectByCountryId(Long id) {
        return baseMapper.selectByCountryId(id);
    }
    @Override
    public void resetByCountryId(Long id) {
        this.update(new UpdateWrapper<Purchase_report>().set("country_id",null).eq("country_id",id));
    }

    @Override
    public void resetByCountryId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_report>().set("country_id",null).in("country_id",ids));
    }

    @Override
    public void removeByCountryId(Long id) {
        this.remove(new QueryWrapper<Purchase_report>().eq("country_id",id));
    }

	@Override
    public List<Purchase_report> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Purchase_report>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_report>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Purchase_report>().eq("currency_id",id));
    }

	@Override
    public List<Purchase_report> selectByCommercialPartnerId(Long id) {
        return baseMapper.selectByCommercialPartnerId(id);
    }
    @Override
    public void resetByCommercialPartnerId(Long id) {
        this.update(new UpdateWrapper<Purchase_report>().set("commercial_partner_id",null).eq("commercial_partner_id",id));
    }

    @Override
    public void resetByCommercialPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_report>().set("commercial_partner_id",null).in("commercial_partner_id",ids));
    }

    @Override
    public void removeByCommercialPartnerId(Long id) {
        this.remove(new QueryWrapper<Purchase_report>().eq("commercial_partner_id",id));
    }

	@Override
    public List<Purchase_report> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Purchase_report>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_report>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Purchase_report>().eq("partner_id",id));
    }

	@Override
    public List<Purchase_report> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Purchase_report>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_report>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Purchase_report>().eq("user_id",id));
    }

	@Override
    public List<Purchase_report> selectByPickingTypeId(Long id) {
        return baseMapper.selectByPickingTypeId(id);
    }
    @Override
    public void resetByPickingTypeId(Long id) {
        this.update(new UpdateWrapper<Purchase_report>().set("picking_type_id",null).eq("picking_type_id",id));
    }

    @Override
    public void resetByPickingTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_report>().set("picking_type_id",null).in("picking_type_id",ids));
    }

    @Override
    public void removeByPickingTypeId(Long id) {
        this.remove(new QueryWrapper<Purchase_report>().eq("picking_type_id",id));
    }

	@Override
    public List<Purchase_report> selectByProductUom(Long id) {
        return baseMapper.selectByProductUom(id);
    }
    @Override
    public void resetByProductUom(Long id) {
        this.update(new UpdateWrapper<Purchase_report>().set("product_uom",null).eq("product_uom",id));
    }

    @Override
    public void resetByProductUom(Collection<Long> ids) {
        this.update(new UpdateWrapper<Purchase_report>().set("product_uom",null).in("product_uom",ids));
    }

    @Override
    public void removeByProductUom(Long id) {
        this.remove(new QueryWrapper<Purchase_report>().eq("product_uom",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Purchase_report> searchDefault(Purchase_reportSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Purchase_report> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Purchase_report>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Purchase_report et){
        //实体关系[DER1N_PURCHASE_REPORT__ACCOUNT_ANALYTIC_ACCOUNT__ACCOUNT_ANALYTIC_ID]
        if(!ObjectUtils.isEmpty(et.getAccountAnalyticId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAccountAnalytic=et.getOdooAccountAnalytic();
            if(ObjectUtils.isEmpty(odooAccountAnalytic)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account majorEntity=accountAnalyticAccountService.get(et.getAccountAnalyticId());
                et.setOdooAccountAnalytic(majorEntity);
                odooAccountAnalytic=majorEntity;
            }
            et.setAccountAnalyticIdText(odooAccountAnalytic.getName());
        }
        //实体关系[DER1N_PURCHASE_REPORT__ACCOUNT_FISCAL_POSITION__FISCAL_POSITION_ID]
        if(!ObjectUtils.isEmpty(et.getFiscalPositionId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position odooFiscalPosition=et.getOdooFiscalPosition();
            if(ObjectUtils.isEmpty(odooFiscalPosition)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position majorEntity=accountFiscalPositionService.get(et.getFiscalPositionId());
                et.setOdooFiscalPosition(majorEntity);
                odooFiscalPosition=majorEntity;
            }
            et.setFiscalPositionIdText(odooFiscalPosition.getName());
        }
        //实体关系[DER1N_PURCHASE_REPORT__PRODUCT_CATEGORY__CATEGORY_ID]
        if(!ObjectUtils.isEmpty(et.getCategoryId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category odooCategory=et.getOdooCategory();
            if(ObjectUtils.isEmpty(odooCategory)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category majorEntity=productCategoryService.get(et.getCategoryId());
                et.setOdooCategory(majorEntity);
                odooCategory=majorEntity;
            }
            et.setCategoryIdText(odooCategory.getName());
        }
        //实体关系[DER1N_PURCHASE_REPORT__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_PURCHASE_REPORT__PRODUCT_TEMPLATE__PRODUCT_TMPL_ID]
        if(!ObjectUtils.isEmpty(et.getProductTmplId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template odooProductTmpl=et.getOdooProductTmpl();
            if(ObjectUtils.isEmpty(odooProductTmpl)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template majorEntity=productTemplateService.get(et.getProductTmplId());
                et.setOdooProductTmpl(majorEntity);
                odooProductTmpl=majorEntity;
            }
            et.setProductTmplIdText(odooProductTmpl.getName());
        }
        //实体关系[DER1N_PURCHASE_REPORT__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_PURCHASE_REPORT__RES_COUNTRY__COUNTRY_ID]
        if(!ObjectUtils.isEmpty(et.getCountryId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountry=et.getOdooCountry();
            if(ObjectUtils.isEmpty(odooCountry)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country majorEntity=resCountryService.get(et.getCountryId());
                et.setOdooCountry(majorEntity);
                odooCountry=majorEntity;
            }
            et.setCountryIdText(odooCountry.getName());
        }
        //实体关系[DER1N_PURCHASE_REPORT__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_PURCHASE_REPORT__RES_PARTNER__COMMERCIAL_PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getCommercialPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooCommercialPartner=et.getOdooCommercialPartner();
            if(ObjectUtils.isEmpty(odooCommercialPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getCommercialPartnerId());
                et.setOdooCommercialPartner(majorEntity);
                odooCommercialPartner=majorEntity;
            }
            et.setCommercialPartnerIdText(odooCommercialPartner.getName());
        }
        //实体关系[DER1N_PURCHASE_REPORT__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_PURCHASE_REPORT__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_PURCHASE_REPORT__STOCK_WAREHOUSE__PICKING_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getPickingTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooPickingType=et.getOdooPickingType();
            if(ObjectUtils.isEmpty(odooPickingType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse majorEntity=stockWarehouseService.get(et.getPickingTypeId());
                et.setOdooPickingType(majorEntity);
                odooPickingType=majorEntity;
            }
            et.setPickingTypeIdText(odooPickingType.getName());
        }
        //实体关系[DER1N_PURCHASE_REPORT__UOM_UOM__PRODUCT_UOM]
        if(!ObjectUtils.isEmpty(et.getProductUom())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUom());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }




}



