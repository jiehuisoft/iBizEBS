package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_taxSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_tax] 服务对象接口
 */
public interface IAccount_taxService extends IService<Account_tax>{

    boolean create(Account_tax et) ;
    void createBatch(List<Account_tax> list) ;
    boolean update(Account_tax et) ;
    void updateBatch(List<Account_tax> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_tax get(Long key) ;
    Account_tax getDraft(Account_tax et) ;
    Account_tax calc_tax(Account_tax et) ;
    boolean checkKey(Account_tax et) ;
    boolean save(Account_tax et) ;
    void saveBatch(List<Account_tax> list) ;
    Page<Account_tax> searchDefault(Account_taxSearchContext context) ;
    Page<Account_tax> searchPurchase(Account_taxSearchContext context) ;
    Page<Account_tax> searchSale(Account_taxSearchContext context) ;
    List<Account_tax> selectByCashBasisBaseAccountId(Long id);
    void resetByCashBasisBaseAccountId(Long id);
    void resetByCashBasisBaseAccountId(Collection<Long> ids);
    void removeByCashBasisBaseAccountId(Long id);
    List<Account_tax> selectByTaxGroupId(Long id);
    void resetByTaxGroupId(Long id);
    void resetByTaxGroupId(Collection<Long> ids);
    void removeByTaxGroupId(Long id);
    List<Account_tax> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_tax> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_tax> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_tax> getAccountTaxByIds(List<Long> ids) ;
    List<Account_tax> getAccountTaxByEntities(List<Account_tax> entities) ;
}


