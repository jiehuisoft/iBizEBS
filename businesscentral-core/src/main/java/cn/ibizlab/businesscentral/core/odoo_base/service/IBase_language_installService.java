package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_language_install;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_language_installSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Base_language_install] 服务对象接口
 */
public interface IBase_language_installService extends IService<Base_language_install>{

    boolean create(Base_language_install et) ;
    void createBatch(List<Base_language_install> list) ;
    boolean update(Base_language_install et) ;
    void updateBatch(List<Base_language_install> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Base_language_install get(Long key) ;
    Base_language_install getDraft(Base_language_install et) ;
    boolean checkKey(Base_language_install et) ;
    boolean save(Base_language_install et) ;
    void saveBatch(List<Base_language_install> list) ;
    Page<Base_language_install> searchDefault(Base_language_installSearchContext context) ;
    List<Base_language_install> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Base_language_install> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Base_language_install> getBaseLanguageInstallByIds(List<Long> ids) ;
    List<Base_language_install> getBaseLanguageInstallByEntities(List<Base_language_install> entities) ;
}


