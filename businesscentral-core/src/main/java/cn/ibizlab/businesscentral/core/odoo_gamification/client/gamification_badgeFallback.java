package cn.ibizlab.businesscentral.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_badgeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[gamification_badge] 服务对象接口
 */
@Component
public class gamification_badgeFallback implements gamification_badgeFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Gamification_badge get(Long id){
            return null;
     }



    public Page<Gamification_badge> search(Gamification_badgeSearchContext context){
            return null;
     }




    public Gamification_badge update(Long id, Gamification_badge gamification_badge){
            return null;
     }
    public Boolean updateBatch(List<Gamification_badge> gamification_badges){
            return false;
     }


    public Gamification_badge create(Gamification_badge gamification_badge){
            return null;
     }
    public Boolean createBatch(List<Gamification_badge> gamification_badges){
            return false;
     }

    public Page<Gamification_badge> select(){
            return null;
     }

    public Gamification_badge getDraft(){
            return null;
    }



    public Boolean checkKey(Gamification_badge gamification_badge){
            return false;
     }


    public Boolean save(Gamification_badge gamification_badge){
            return false;
     }
    public Boolean saveBatch(List<Gamification_badge> gamification_badges){
            return false;
     }

    public Page<Gamification_badge> searchDefault(Gamification_badgeSearchContext context){
            return null;
     }


}
