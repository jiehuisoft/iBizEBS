package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_language_export;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_language_exportSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_language_export] 服务对象接口
 */
@Component
public class base_language_exportFallback implements base_language_exportFeignClient{

    public Base_language_export create(Base_language_export base_language_export){
            return null;
     }
    public Boolean createBatch(List<Base_language_export> base_language_exports){
            return false;
     }



    public Base_language_export get(Long id){
            return null;
     }


    public Base_language_export update(Long id, Base_language_export base_language_export){
            return null;
     }
    public Boolean updateBatch(List<Base_language_export> base_language_exports){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Base_language_export> search(Base_language_exportSearchContext context){
            return null;
     }



    public Page<Base_language_export> select(){
            return null;
     }

    public Base_language_export getDraft(){
            return null;
    }



    public Boolean checkKey(Base_language_export base_language_export){
            return false;
     }


    public Boolean save(Base_language_export base_language_export){
            return false;
     }
    public Boolean saveBatch(List<Base_language_export> base_language_exports){
            return false;
     }

    public Page<Base_language_export> searchDefault(Base_language_exportSearchContext context){
            return null;
     }


}
