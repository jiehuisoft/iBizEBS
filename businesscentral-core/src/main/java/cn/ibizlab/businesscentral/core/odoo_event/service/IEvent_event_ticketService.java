package cn.ibizlab.businesscentral.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event_ticket;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_event_ticketSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Event_event_ticket] 服务对象接口
 */
public interface IEvent_event_ticketService extends IService<Event_event_ticket>{

    boolean create(Event_event_ticket et) ;
    void createBatch(List<Event_event_ticket> list) ;
    boolean update(Event_event_ticket et) ;
    void updateBatch(List<Event_event_ticket> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Event_event_ticket get(Long key) ;
    Event_event_ticket getDraft(Event_event_ticket et) ;
    boolean checkKey(Event_event_ticket et) ;
    boolean save(Event_event_ticket et) ;
    void saveBatch(List<Event_event_ticket> list) ;
    Page<Event_event_ticket> searchDefault(Event_event_ticketSearchContext context) ;
    List<Event_event_ticket> selectByEventId(Long id);
    void removeByEventId(Collection<Long> ids);
    void removeByEventId(Long id);
    List<Event_event_ticket> selectByEventTypeId(Long id);
    void removeByEventTypeId(Collection<Long> ids);
    void removeByEventTypeId(Long id);
    List<Event_event_ticket> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Event_event_ticket> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Event_event_ticket> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Event_event_ticket> getEventEventTicketByIds(List<Long> ids) ;
    List<Event_event_ticket> getEventEventTicketByEntities(List<Event_event_ticket> entities) ;
}


