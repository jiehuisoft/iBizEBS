package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_lang;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_langSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_lang] 服务对象接口
 */
public interface IRes_langService extends IService<Res_lang>{

    boolean create(Res_lang et) ;
    void createBatch(List<Res_lang> list) ;
    boolean update(Res_lang et) ;
    void updateBatch(List<Res_lang> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_lang get(Long key) ;
    Res_lang getDraft(Res_lang et) ;
    boolean checkKey(Res_lang et) ;
    boolean save(Res_lang et) ;
    void saveBatch(List<Res_lang> list) ;
    Page<Res_lang> searchDefault(Res_langSearchContext context) ;
    List<Res_lang> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_lang> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_lang> getResLangByIds(List<Long> ids) ;
    List<Res_lang> getResLangByEntities(List<Res_lang> entities) ;
}


