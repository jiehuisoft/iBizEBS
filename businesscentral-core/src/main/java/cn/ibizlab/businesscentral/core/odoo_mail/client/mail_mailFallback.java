package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mail;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mailSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_mail] 服务对象接口
 */
@Component
public class mail_mailFallback implements mail_mailFeignClient{



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Mail_mail update(Long id, Mail_mail mail_mail){
            return null;
     }
    public Boolean updateBatch(List<Mail_mail> mail_mails){
            return false;
     }



    public Mail_mail get(Long id){
            return null;
     }


    public Mail_mail create(Mail_mail mail_mail){
            return null;
     }
    public Boolean createBatch(List<Mail_mail> mail_mails){
            return false;
     }

    public Page<Mail_mail> search(Mail_mailSearchContext context){
            return null;
     }


    public Page<Mail_mail> select(){
            return null;
     }

    public Mail_mail getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_mail mail_mail){
            return false;
     }


    public Boolean save(Mail_mail mail_mail){
            return false;
     }
    public Boolean saveBatch(List<Mail_mail> mail_mails){
            return false;
     }

    public Page<Mail_mail> searchDefault(Mail_mailSearchContext context){
            return null;
     }


}
