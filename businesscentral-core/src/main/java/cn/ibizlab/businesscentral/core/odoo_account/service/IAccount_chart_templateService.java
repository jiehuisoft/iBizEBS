package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_chart_templateSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_chart_template] 服务对象接口
 */
public interface IAccount_chart_templateService extends IService<Account_chart_template>{

    boolean create(Account_chart_template et) ;
    void createBatch(List<Account_chart_template> list) ;
    boolean update(Account_chart_template et) ;
    void updateBatch(List<Account_chart_template> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_chart_template get(Long key) ;
    Account_chart_template getDraft(Account_chart_template et) ;
    boolean checkKey(Account_chart_template et) ;
    boolean save(Account_chart_template et) ;
    void saveBatch(List<Account_chart_template> list) ;
    Page<Account_chart_template> searchDefault(Account_chart_templateSearchContext context) ;
    List<Account_chart_template> selectByExpenseCurrencyExchangeAccountId(Long id);
    void resetByExpenseCurrencyExchangeAccountId(Long id);
    void resetByExpenseCurrencyExchangeAccountId(Collection<Long> ids);
    void removeByExpenseCurrencyExchangeAccountId(Long id);
    List<Account_chart_template> selectByIncomeCurrencyExchangeAccountId(Long id);
    void resetByIncomeCurrencyExchangeAccountId(Long id);
    void resetByIncomeCurrencyExchangeAccountId(Collection<Long> ids);
    void removeByIncomeCurrencyExchangeAccountId(Long id);
    List<Account_chart_template> selectByPropertyAccountExpenseCategId(Long id);
    void resetByPropertyAccountExpenseCategId(Long id);
    void resetByPropertyAccountExpenseCategId(Collection<Long> ids);
    void removeByPropertyAccountExpenseCategId(Long id);
    List<Account_chart_template> selectByPropertyAccountExpenseId(Long id);
    void resetByPropertyAccountExpenseId(Long id);
    void resetByPropertyAccountExpenseId(Collection<Long> ids);
    void removeByPropertyAccountExpenseId(Long id);
    List<Account_chart_template> selectByPropertyAccountIncomeCategId(Long id);
    void resetByPropertyAccountIncomeCategId(Long id);
    void resetByPropertyAccountIncomeCategId(Collection<Long> ids);
    void removeByPropertyAccountIncomeCategId(Long id);
    List<Account_chart_template> selectByPropertyAccountIncomeId(Long id);
    void resetByPropertyAccountIncomeId(Long id);
    void resetByPropertyAccountIncomeId(Collection<Long> ids);
    void removeByPropertyAccountIncomeId(Long id);
    List<Account_chart_template> selectByPropertyAccountPayableId(Long id);
    void resetByPropertyAccountPayableId(Long id);
    void resetByPropertyAccountPayableId(Collection<Long> ids);
    void removeByPropertyAccountPayableId(Long id);
    List<Account_chart_template> selectByPropertyAccountReceivableId(Long id);
    void resetByPropertyAccountReceivableId(Long id);
    void resetByPropertyAccountReceivableId(Collection<Long> ids);
    void removeByPropertyAccountReceivableId(Long id);
    List<Account_chart_template> selectByPropertyStockAccountInputCategId(Long id);
    void resetByPropertyStockAccountInputCategId(Long id);
    void resetByPropertyStockAccountInputCategId(Collection<Long> ids);
    void removeByPropertyStockAccountInputCategId(Long id);
    List<Account_chart_template> selectByPropertyStockAccountOutputCategId(Long id);
    void resetByPropertyStockAccountOutputCategId(Long id);
    void resetByPropertyStockAccountOutputCategId(Collection<Long> ids);
    void removeByPropertyStockAccountOutputCategId(Long id);
    List<Account_chart_template> selectByPropertyStockValuationAccountId(Long id);
    void resetByPropertyStockValuationAccountId(Long id);
    void resetByPropertyStockValuationAccountId(Collection<Long> ids);
    void removeByPropertyStockValuationAccountId(Long id);
    List<Account_chart_template> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Account_chart_template> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Account_chart_template> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_chart_template> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_chart_template> getAccountChartTemplateByIds(List<Long> ids) ;
    List<Account_chart_template> getAccountChartTemplateByEntities(List<Account_chart_template> entities) ;
}


