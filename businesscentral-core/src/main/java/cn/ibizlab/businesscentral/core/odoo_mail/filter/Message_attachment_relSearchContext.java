package cn.ibizlab.businesscentral.core.odoo_mail.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Message_attachment_rel;
/**
 * 关系型数据实体[Message_attachment_rel] 查询条件对象
 */
@Slf4j
@Data
public class Message_attachment_relSearchContext extends QueryWrapperContext<Message_attachment_rel> {

	private Long n_id_like;//[id]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private Long n_message_id_eq;//[ID]
	public void setN_message_id_eq(Long n_message_id_eq) {
        this.n_message_id_eq = n_message_id_eq;
        if(!ObjectUtils.isEmpty(this.n_message_id_eq)){
            this.getSearchCond().eq("message_id", n_message_id_eq);
        }
    }
	private Long n_attachment_id_eq;//[ID]
	public void setN_attachment_id_eq(Long n_attachment_id_eq) {
        this.n_attachment_id_eq = n_attachment_id_eq;
        if(!ObjectUtils.isEmpty(this.n_attachment_id_eq)){
            this.getSearchCond().eq("attachment_id", n_attachment_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}



