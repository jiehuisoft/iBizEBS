package cn.ibizlab.businesscentral.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_taxes_rel;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_taxes_relSearchContext;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_taxes_relService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_product.mapper.Product_taxes_relMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[产品销项税] 服务对象接口实现
 */
@Slf4j
@Service("Product_taxes_relServiceImpl")
public class Product_taxes_relServiceImpl extends EBSServiceImpl<Product_taxes_relMapper, Product_taxes_rel> implements IProduct_taxes_relService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_taxService accountTaxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_templateService productTemplateService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "product.taxes.rel" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Product_taxes_rel et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;

        if (messageinfo && !mail_create_nosubscribe && false) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog && false) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack && false) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Product_taxes_rel> list) {
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Product_taxes_rel et) {
        Product_taxes_rel old = new Product_taxes_rel() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProduct_taxes_relService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProduct_taxes_relService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack && false) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Product_taxes_rel> list) {
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Product_taxes_rel get(Long key) {
        Product_taxes_rel et = getById(key);
        if(et==null){
            et=new Product_taxes_rel();
        }
        else{
        }
        return et;
    }

    @Override
    public Product_taxes_rel getDraft(Product_taxes_rel et) {
        return et;
    }

    @Override
    public boolean checkKey(Product_taxes_rel et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Product_taxes_rel et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Product_taxes_rel et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Product_taxes_rel> list) {
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Product_taxes_rel> list) {
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Product_taxes_rel> selectByTaxId(Long id) {
        return baseMapper.selectByTaxId(id);
    }
    @Override
    public void removeByTaxId(Long id) {
        this.remove(new QueryWrapper<Product_taxes_rel>().eq("tax_id",id));
    }

	@Override
    public List<Product_taxes_rel> selectByProdId(Long id) {
        return baseMapper.selectByProdId(id);
    }
    @Override
    public void removeByProdId(Long id) {
        this.remove(new QueryWrapper<Product_taxes_rel>().eq("prod_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Product_taxes_rel> searchDefault(Product_taxes_relSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Product_taxes_rel> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Product_taxes_rel>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }




}



