package cn.ibizlab.businesscentral.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_productionSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_productionService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mrp.mapper.Mrp_productionMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[Production Order] 服务对象接口实现
 */
@Slf4j
@Service("Mrp_productionServiceImpl")
public class Mrp_productionServiceImpl extends EBSServiceImpl<Mrp_productionMapper, Mrp_production> implements IMrp_productionService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_product_produceService mrpProductProduceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_unbuildService mrpUnbuildService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workorderService mrpWorkorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_move_lineService stockMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_scrapService stockScrapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bomService mrpBomService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_routingService mrpRoutingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_picking_typeService stockPickingTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mrp.production" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mrp_production et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_productionService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mrp_production> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mrp_production et) {
        Mrp_production old = new Mrp_production() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_productionService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_productionService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mrp_production> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        mrpProductProduceService.resetByProductionId(key);
        mrpUnbuildService.resetByMoId(key);
        mrpWorkorderService.removeByProductionId(key);
        stockMoveLineService.resetByProductionId(key);
        stockMoveService.resetByCreatedProductionId(key);
        stockMoveService.resetByProductionId(key);
        stockMoveService.resetByRawMaterialProductionId(key);
        stockScrapService.resetByProductionId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        mrpProductProduceService.resetByProductionId(idList);
        mrpUnbuildService.resetByMoId(idList);
        mrpWorkorderService.removeByProductionId(idList);
        stockMoveLineService.resetByProductionId(idList);
        stockMoveService.resetByCreatedProductionId(idList);
        stockMoveService.resetByProductionId(idList);
        stockMoveService.resetByRawMaterialProductionId(idList);
        stockScrapService.resetByProductionId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mrp_production get(Long key) {
        Mrp_production et = getById(key);
        if(et==null){
            et=new Mrp_production();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mrp_production getDraft(Mrp_production et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mrp_production et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mrp_production et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mrp_production et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mrp_production> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mrp_production> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mrp_production> selectByBomId(Long id) {
        return baseMapper.selectByBomId(id);
    }
    @Override
    public void resetByBomId(Long id) {
        this.update(new UpdateWrapper<Mrp_production>().set("bom_id",null).eq("bom_id",id));
    }

    @Override
    public void resetByBomId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_production>().set("bom_id",null).in("bom_id",ids));
    }

    @Override
    public void removeByBomId(Long id) {
        this.remove(new QueryWrapper<Mrp_production>().eq("bom_id",id));
    }

	@Override
    public List<Mrp_production> selectByRoutingId(Long id) {
        return baseMapper.selectByRoutingId(id);
    }
    @Override
    public void resetByRoutingId(Long id) {
        this.update(new UpdateWrapper<Mrp_production>().set("routing_id",null).eq("routing_id",id));
    }

    @Override
    public void resetByRoutingId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_production>().set("routing_id",null).in("routing_id",ids));
    }

    @Override
    public void removeByRoutingId(Long id) {
        this.remove(new QueryWrapper<Mrp_production>().eq("routing_id",id));
    }

	@Override
    public List<Mrp_production> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Mrp_production>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_production>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Mrp_production>().eq("product_id",id));
    }

	@Override
    public List<Mrp_production> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Mrp_production>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_production>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Mrp_production>().eq("company_id",id));
    }

	@Override
    public List<Mrp_production> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mrp_production>().eq("create_uid",id));
    }

	@Override
    public List<Mrp_production> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Mrp_production>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_production>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Mrp_production>().eq("user_id",id));
    }

	@Override
    public List<Mrp_production> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mrp_production>().eq("write_uid",id));
    }

	@Override
    public List<Mrp_production> selectByLocationDestId(Long id) {
        return baseMapper.selectByLocationDestId(id);
    }
    @Override
    public void resetByLocationDestId(Long id) {
        this.update(new UpdateWrapper<Mrp_production>().set("location_dest_id",null).eq("location_dest_id",id));
    }

    @Override
    public void resetByLocationDestId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_production>().set("location_dest_id",null).in("location_dest_id",ids));
    }

    @Override
    public void removeByLocationDestId(Long id) {
        this.remove(new QueryWrapper<Mrp_production>().eq("location_dest_id",id));
    }

	@Override
    public List<Mrp_production> selectByLocationSrcId(Long id) {
        return baseMapper.selectByLocationSrcId(id);
    }
    @Override
    public void resetByLocationSrcId(Long id) {
        this.update(new UpdateWrapper<Mrp_production>().set("location_src_id",null).eq("location_src_id",id));
    }

    @Override
    public void resetByLocationSrcId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_production>().set("location_src_id",null).in("location_src_id",ids));
    }

    @Override
    public void removeByLocationSrcId(Long id) {
        this.remove(new QueryWrapper<Mrp_production>().eq("location_src_id",id));
    }

	@Override
    public List<Mrp_production> selectByPickingTypeId(Long id) {
        return baseMapper.selectByPickingTypeId(id);
    }
    @Override
    public void resetByPickingTypeId(Long id) {
        this.update(new UpdateWrapper<Mrp_production>().set("picking_type_id",null).eq("picking_type_id",id));
    }

    @Override
    public void resetByPickingTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_production>().set("picking_type_id",null).in("picking_type_id",ids));
    }

    @Override
    public void removeByPickingTypeId(Long id) {
        this.remove(new QueryWrapper<Mrp_production>().eq("picking_type_id",id));
    }

	@Override
    public List<Mrp_production> selectByProductUomId(Long id) {
        return baseMapper.selectByProductUomId(id);
    }
    @Override
    public void resetByProductUomId(Long id) {
        this.update(new UpdateWrapper<Mrp_production>().set("product_uom_id",null).eq("product_uom_id",id));
    }

    @Override
    public void resetByProductUomId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_production>().set("product_uom_id",null).in("product_uom_id",ids));
    }

    @Override
    public void removeByProductUomId(Long id) {
        this.remove(new QueryWrapper<Mrp_production>().eq("product_uom_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mrp_production> searchDefault(Mrp_productionSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mrp_production> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mrp_production>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mrp_production et){
        //实体关系[DER1N_MRP_PRODUCTION__MRP_ROUTING__ROUTING_ID]
        if(!ObjectUtils.isEmpty(et.getRoutingId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing odooRouting=et.getOdooRouting();
            if(ObjectUtils.isEmpty(odooRouting)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing majorEntity=mrpRoutingService.get(et.getRoutingId());
                et.setOdooRouting(majorEntity);
                odooRouting=majorEntity;
            }
            et.setRoutingIdText(odooRouting.getName());
        }
        //实体关系[DER1N_MRP_PRODUCTION__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductionLocationId(odooProduct.getPropertyStockProduction());
            et.setProductIdText(odooProduct.getName());
            et.setProductTmplId(odooProduct.getProductTmplId());
        }
        //实体关系[DER1N_MRP_PRODUCTION__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_MRP_PRODUCTION__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MRP_PRODUCTION__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_MRP_PRODUCTION__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_MRP_PRODUCTION__STOCK_LOCATION__LOCATION_DEST_ID]
        if(!ObjectUtils.isEmpty(et.getLocationDestId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocationDest=et.getOdooLocationDest();
            if(ObjectUtils.isEmpty(odooLocationDest)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationDestId());
                et.setOdooLocationDest(majorEntity);
                odooLocationDest=majorEntity;
            }
            et.setLocationDestIdText(odooLocationDest.getName());
        }
        //实体关系[DER1N_MRP_PRODUCTION__STOCK_LOCATION__LOCATION_SRC_ID]
        if(!ObjectUtils.isEmpty(et.getLocationSrcId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocationSrc=et.getOdooLocationSrc();
            if(ObjectUtils.isEmpty(odooLocationSrc)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationSrcId());
                et.setOdooLocationSrc(majorEntity);
                odooLocationSrc=majorEntity;
            }
            et.setLocationSrcIdText(odooLocationSrc.getName());
        }
        //实体关系[DER1N_MRP_PRODUCTION__STOCK_PICKING_TYPE__PICKING_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getPickingTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooPickingType=et.getOdooPickingType();
            if(ObjectUtils.isEmpty(odooPickingType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getPickingTypeId());
                et.setOdooPickingType(majorEntity);
                odooPickingType=majorEntity;
            }
            et.setPickingTypeIdText(odooPickingType.getName());
        }
        //实体关系[DER1N_MRP_PRODUCTION__UOM_UOM__PRODUCT_UOM_ID]
        if(!ObjectUtils.isEmpty(et.getProductUomId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUomId());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomIdText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mrp_production> getMrpProductionByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mrp_production> getMrpProductionByEntities(List<Mrp_production> entities) {
        List ids =new ArrayList();
        for(Mrp_production entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



