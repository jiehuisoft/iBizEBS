package cn.ibizlab.businesscentral.core.odoo_web_tour.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_web_tour.domain.Web_tour_tour;
import cn.ibizlab.businesscentral.core.odoo_web_tour.filter.Web_tour_tourSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Web_tour_tour] 服务对象接口
 */
public interface IWeb_tour_tourService extends IService<Web_tour_tour>{

    boolean create(Web_tour_tour et) ;
    void createBatch(List<Web_tour_tour> list) ;
    boolean update(Web_tour_tour et) ;
    void updateBatch(List<Web_tour_tour> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Web_tour_tour get(Long key) ;
    Web_tour_tour getDraft(Web_tour_tour et) ;
    boolean checkKey(Web_tour_tour et) ;
    boolean save(Web_tour_tour et) ;
    void saveBatch(List<Web_tour_tour> list) ;
    Page<Web_tour_tour> searchDefault(Web_tour_tourSearchContext context) ;
    List<Web_tour_tour> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


