package cn.ibizlab.businesscentral.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[库存移动]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "STOCK_MOVE",resultMap = "Stock_moveResultMap")
public class Stock_move extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 全部退回移动
     */
    @TableField(exist = false)
    @JSONField(name = "returned_move_ids")
    @JsonProperty("returned_move_ids")
    private String returnedMoveIds;
    /**
     * 在分拣确认后是否添加了移动
     */
    @TableField(value = "additional")
    @JSONField(name = "additional")
    @JsonProperty("additional")
    private Boolean additional;
    /**
     * 单价
     */
    @DEField(name = "price_unit")
    @TableField(value = "price_unit")
    @JSONField(name = "price_unit")
    @JsonProperty("price_unit")
    private Double priceUnit;
    /**
     * 补货组
     */
    @DEField(name = "group_id")
    @TableField(value = "group_id")
    @JSONField(name = "group_id")
    @JsonProperty("group_id")
    private Integer groupId;
    /**
     * 报废
     */
    @TableField(exist = false)
    @JSONField(name = "scrap_ids")
    @JsonProperty("scrap_ids")
    private String scrapIds;
    /**
     * 会计凭证
     */
    @TableField(exist = false)
    @JSONField(name = "account_move_ids")
    @JsonProperty("account_move_ids")
    private String accountMoveIds;
    /**
     * 目的地移动
     */
    @TableField(exist = false)
    @JSONField(name = "move_dest_ids")
    @JsonProperty("move_dest_ids")
    private String moveDestIds;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 退款 (更新 SO/PO)
     */
    @DEField(name = "to_refund")
    @TableField(value = "to_refund")
    @JSONField(name = "to_refund")
    @JsonProperty("to_refund")
    private Boolean toRefund;
    /**
     * 单位因子
     */
    @DEField(name = "unit_factor")
    @TableField(value = "unit_factor")
    @JSONField(name = "unit_factor")
    @JsonProperty("unit_factor")
    private Double unitFactor;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 预测数量
     */
    @TableField(exist = false)
    @JSONField(name = "availability")
    @JsonProperty("availability")
    private Double availability;
    /**
     * 剩余数量
     */
    @DEField(name = "remaining_qty")
    @TableField(value = "remaining_qty")
    @JSONField(name = "remaining_qty")
    @JsonProperty("remaining_qty")
    private Double remainingQty;
    /**
     * 传播取消以及拆分
     */
    @TableField(value = "propagate")
    @JSONField(name = "propagate")
    @JsonProperty("propagate")
    private Boolean propagate;
    /**
     * 编号
     */
    @TableField(value = "reference")
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;
    /**
     * 批次
     */
    @TableField(exist = false)
    @JSONField(name = "active_move_line_ids")
    @JsonProperty("active_move_line_ids")
    private String activeMoveLineIds;
    /**
     * 移动行无建议
     */
    @TableField(exist = false)
    @JSONField(name = "move_line_nosuggest_ids")
    @JsonProperty("move_line_nosuggest_ids")
    private String moveLineNosuggestIds;
    /**
     * 详情可见
     */
    @TableField(exist = false)
    @JSONField(name = "show_details_visible")
    @JsonProperty("show_details_visible")
    private Boolean showDetailsVisible;
    /**
     * 实际数量
     */
    @DEField(name = "product_qty")
    @TableField(value = "product_qty")
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 源文档
     */
    @TableField(value = "origin")
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;
    /**
     * 初始需求
     */
    @DEField(name = "product_uom_qty")
    @TableField(value = "product_uom_qty")
    @JSONField(name = "product_uom_qty")
    @JsonProperty("product_uom_qty")
    private Double productUomQty;
    /**
     * 追踪
     */
    @TableField(exist = false)
    @JSONField(name = "needs_lots")
    @JsonProperty("needs_lots")
    private Boolean needsLots;
    /**
     * 优先级
     */
    @TableField(value = "priority")
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;
    /**
     * 日期
     */
    @TableField(value = "date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 剩余价值
     */
    @DEField(name = "remaining_value")
    @TableField(value = "remaining_value")
    @JSONField(name = "remaining_value")
    @JsonProperty("remaining_value")
    private Double remainingValue;
    /**
     * 订单完成批次
     */
    @TableField(exist = false)
    @JSONField(name = "order_finished_lot_ids")
    @JsonProperty("order_finished_lot_ids")
    private String orderFinishedLotIds;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 创建日期
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 原始移动
     */
    @TableField(exist = false)
    @JSONField(name = "move_orig_ids")
    @JsonProperty("move_orig_ids")
    private String moveOrigIds;
    /**
     * 凭证明细
     */
    @TableField(exist = false)
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;
    /**
     * 备注
     */
    @TableField(value = "note")
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;
    /**
     * 已预留数量
     */
    @TableField(exist = false)
    @JSONField(name = "reserved_availability")
    @JsonProperty("reserved_availability")
    private Double reservedAvailability;
    /**
     * 初始需求是否可以编辑
     */
    @TableField(exist = false)
    @JSONField(name = "is_initial_demand_editable")
    @JsonProperty("is_initial_demand_editable")
    private Boolean isInitialDemandEditable;
    /**
     * 完成数量
     */
    @TableField(exist = false)
    @JSONField(name = "quantity_done")
    @JsonProperty("quantity_done")
    private Double quantityDone;
    /**
     * 是锁定
     */
    @TableField(exist = false)
    @JSONField(name = "is_locked")
    @JsonProperty("is_locked")
    private Boolean isLocked;
    /**
     * 可用量
     */
    @TableField(exist = false)
    @JSONField(name = "string_availability_info")
    @JsonProperty("string_availability_info")
    private String stringAvailabilityInfo;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 从供应商
     */
    @TableField(exist = false)
    @JSONField(name = "show_reserved_availability")
    @JsonProperty("show_reserved_availability")
    private Boolean showReservedAvailability;
    /**
     * 目的路线
     */
    @TableField(exist = false)
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    private String routeIds;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 说明
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 值
     */
    @TableField(value = "value")
    @JSONField(name = "value")
    @JsonProperty("value")
    private Double value;
    /**
     * 完工批次已存在
     */
    @TableField(exist = false)
    @JSONField(name = "finished_lots_exist")
    @JsonProperty("finished_lots_exist")
    private Boolean finishedLotsExist;
    /**
     * 完成
     */
    @DEField(name = "is_done")
    @TableField(value = "is_done")
    @JSONField(name = "is_done")
    @JsonProperty("is_done")
    private Boolean isDone;
    /**
     * 完成数量是否可以编辑
     */
    @TableField(exist = false)
    @JSONField(name = "is_quantity_done_editable")
    @JsonProperty("is_quantity_done_editable")
    private Boolean isQuantityDoneEditable;
    /**
     * 有移动行
     */
    @TableField(exist = false)
    @JSONField(name = "has_move_lines")
    @JsonProperty("has_move_lines")
    private Boolean hasMoveLines;
    /**
     * 预计日期
     */
    @DEField(name = "date_expected")
    @TableField(value = "date_expected")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_expected" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_expected")
    private Timestamp dateExpected;
    /**
     * 供应方法
     */
    @DEField(name = "procure_method")
    @TableField(value = "procure_method")
    @JSONField(name = "procure_method")
    @JsonProperty("procure_method")
    private String procureMethod;
    /**
     * 产品类型
     */
    @TableField(exist = false)
    @JSONField(name = "product_type")
    @JsonProperty("product_type")
    private String productType;
    /**
     * 拆卸顺序
     */
    @TableField(exist = false)
    @JSONField(name = "unbuild_id_text")
    @JsonProperty("unbuild_id_text")
    private String unbuildIdText;
    /**
     * 已报废
     */
    @TableField(exist = false)
    @JSONField(name = "scrapped")
    @JsonProperty("scrapped")
    private Boolean scrapped;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 成品的生产订单
     */
    @TableField(exist = false)
    @JSONField(name = "production_id_text")
    @JsonProperty("production_id_text")
    private String productionIdText;
    /**
     * 仓库
     */
    @TableField(exist = false)
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    private String warehouseIdText;
    /**
     * 显示详细作业
     */
    @TableField(exist = false)
    @JSONField(name = "show_operations")
    @JsonProperty("show_operations")
    private Boolean showOperations;
    /**
     * 创建生产订单
     */
    @TableField(exist = false)
    @JSONField(name = "created_production_id_text")
    @JsonProperty("created_production_id_text")
    private String createdProductionIdText;
    /**
     * 创建采购订单行
     */
    @TableField(exist = false)
    @JSONField(name = "created_purchase_line_id_text")
    @JsonProperty("created_purchase_line_id_text")
    private String createdPurchaseLineIdText;
    /**
     * 目的位置
     */
    @TableField(exist = false)
    @JSONField(name = "location_dest_id_text")
    @JsonProperty("location_dest_id_text")
    private String locationDestIdText;
    /**
     * 使用追踪的产品
     */
    @TableField(exist = false)
    @JSONField(name = "has_tracking")
    @JsonProperty("has_tracking")
    private String hasTracking;
    /**
     * 作业类型
     */
    @TableField(exist = false)
    @JSONField(name = "picking_type_id_text")
    @JsonProperty("picking_type_id_text")
    private String pickingTypeIdText;
    /**
     * 销售明细行
     */
    @TableField(exist = false)
    @JSONField(name = "sale_line_id_text")
    @JsonProperty("sale_line_id_text")
    private String saleLineIdText;
    /**
     * 目的地地址
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 调拨目的地地址
     */
    @TableField(exist = false)
    @JSONField(name = "picking_partner_id")
    @JsonProperty("picking_partner_id")
    private Long pickingPartnerId;
    /**
     * 原材料的生产订单
     */
    @TableField(exist = false)
    @JSONField(name = "raw_material_production_id_text")
    @JsonProperty("raw_material_production_id_text")
    private String rawMaterialProductionIdText;
    /**
     * 原始退回移动
     */
    @TableField(exist = false)
    @JSONField(name = "origin_returned_move_id_text")
    @JsonProperty("origin_returned_move_id_text")
    private String originReturnedMoveIdText;
    /**
     * 维修
     */
    @TableField(exist = false)
    @JSONField(name = "repair_id_text")
    @JsonProperty("repair_id_text")
    private String repairIdText;
    /**
     * 欠单
     */
    @TableField(exist = false)
    @JSONField(name = "backorder_id")
    @JsonProperty("backorder_id")
    private Long backorderId;
    /**
     * 采购订单行
     */
    @TableField(exist = false)
    @JSONField(name = "purchase_line_id_text")
    @JsonProperty("purchase_line_id_text")
    private String purchaseLineIdText;
    /**
     * 调拨参照
     */
    @TableField(exist = false)
    @JSONField(name = "picking_id_text")
    @JsonProperty("picking_id_text")
    private String pickingIdText;
    /**
     * 产品
     */
    @TableField(exist = false)
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;
    /**
     * 源位置
     */
    @TableField(exist = false)
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "product_uom_text")
    @JsonProperty("product_uom_text")
    private String productUomText;
    /**
     * 待加工的作业
     */
    @TableField(exist = false)
    @JSONField(name = "operation_id_text")
    @JsonProperty("operation_id_text")
    private String operationIdText;
    /**
     * 待消耗的工单
     */
    @TableField(exist = false)
    @JSONField(name = "workorder_id_text")
    @JsonProperty("workorder_id_text")
    private String workorderIdText;
    /**
     * 拆卸单
     */
    @TableField(exist = false)
    @JSONField(name = "consume_unbuild_id_text")
    @JsonProperty("consume_unbuild_id_text")
    private String consumeUnbuildIdText;
    /**
     * 首选包装
     */
    @TableField(exist = false)
    @JSONField(name = "product_packaging_text")
    @JsonProperty("product_packaging_text")
    private String productPackagingText;
    /**
     * 所有者
     */
    @TableField(exist = false)
    @JSONField(name = "restrict_partner_id_text")
    @JsonProperty("restrict_partner_id_text")
    private String restrictPartnerIdText;
    /**
     * 库存
     */
    @TableField(exist = false)
    @JSONField(name = "inventory_id_text")
    @JsonProperty("inventory_id_text")
    private String inventoryIdText;
    /**
     * 产品模板
     */
    @TableField(exist = false)
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    private Long productTmplId;
    /**
     * 库存规则
     */
    @TableField(exist = false)
    @JSONField(name = "rule_id_text")
    @JsonProperty("rule_id_text")
    private String ruleIdText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 作业的类型
     */
    @TableField(exist = false)
    @JSONField(name = "picking_code")
    @JsonProperty("picking_code")
    private String pickingCode;
    /**
     * 移动整个包裹
     */
    @TableField(exist = false)
    @JSONField(name = "picking_type_entire_packs")
    @JsonProperty("picking_type_entire_packs")
    private Boolean pickingTypeEntirePacks;
    /**
     * 创建生产订单
     */
    @DEField(name = "created_production_id")
    @TableField(value = "created_production_id")
    @JSONField(name = "created_production_id")
    @JsonProperty("created_production_id")
    private Long createdProductionId;
    /**
     * 拆卸单
     */
    @DEField(name = "consume_unbuild_id")
    @TableField(value = "consume_unbuild_id")
    @JSONField(name = "consume_unbuild_id")
    @JsonProperty("consume_unbuild_id")
    private Long consumeUnbuildId;
    /**
     * 销售明细行
     */
    @DEField(name = "sale_line_id")
    @TableField(value = "sale_line_id")
    @JSONField(name = "sale_line_id")
    @JsonProperty("sale_line_id")
    private Long saleLineId;
    /**
     * 原始退回移动
     */
    @DEField(name = "origin_returned_move_id")
    @TableField(value = "origin_returned_move_id")
    @JSONField(name = "origin_returned_move_id")
    @JsonProperty("origin_returned_move_id")
    private Long originReturnedMoveId;
    /**
     * 作业类型
     */
    @DEField(name = "picking_type_id")
    @TableField(value = "picking_type_id")
    @JSONField(name = "picking_type_id")
    @JsonProperty("picking_type_id")
    private Long pickingTypeId;
    /**
     * 待加工的作业
     */
    @DEField(name = "operation_id")
    @TableField(value = "operation_id")
    @JSONField(name = "operation_id")
    @JsonProperty("operation_id")
    private Long operationId;
    /**
     * BOM行
     */
    @DEField(name = "bom_line_id")
    @TableField(value = "bom_line_id")
    @JSONField(name = "bom_line_id")
    @JsonProperty("bom_line_id")
    private Long bomLineId;
    /**
     * 维修
     */
    @DEField(name = "repair_id")
    @TableField(value = "repair_id")
    @JSONField(name = "repair_id")
    @JsonProperty("repair_id")
    private Long repairId;
    /**
     * 产品
     */
    @DEField(name = "product_id")
    @TableField(value = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Long productId;
    /**
     * 拆卸顺序
     */
    @DEField(name = "unbuild_id")
    @TableField(value = "unbuild_id")
    @JSONField(name = "unbuild_id")
    @JsonProperty("unbuild_id")
    private Long unbuildId;
    /**
     * 成品的生产订单
     */
    @DEField(name = "production_id")
    @TableField(value = "production_id")
    @JSONField(name = "production_id")
    @JsonProperty("production_id")
    private Long productionId;
    /**
     * 创建采购订单行
     */
    @DEField(name = "created_purchase_line_id")
    @TableField(value = "created_purchase_line_id")
    @JSONField(name = "created_purchase_line_id")
    @JsonProperty("created_purchase_line_id")
    private Long createdPurchaseLineId;
    /**
     * 待消耗的工单
     */
    @DEField(name = "workorder_id")
    @TableField(value = "workorder_id")
    @JSONField(name = "workorder_id")
    @JsonProperty("workorder_id")
    private Long workorderId;
    /**
     * 单位
     */
    @DEField(name = "product_uom")
    @TableField(value = "product_uom")
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    private Long productUom;
    /**
     * 调拨参照
     */
    @DEField(name = "picking_id")
    @TableField(value = "picking_id")
    @JSONField(name = "picking_id")
    @JsonProperty("picking_id")
    private Long pickingId;
    /**
     * 目的地地址
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 包裹层级
     */
    @DEField(name = "package_level_id")
    @TableField(value = "package_level_id")
    @JSONField(name = "package_level_id")
    @JsonProperty("package_level_id")
    private Long packageLevelId;
    /**
     * 首选包装
     */
    @DEField(name = "product_packaging")
    @TableField(value = "product_packaging")
    @JSONField(name = "product_packaging")
    @JsonProperty("product_packaging")
    private Long productPackaging;
    /**
     * 所有者
     */
    @DEField(name = "restrict_partner_id")
    @TableField(value = "restrict_partner_id")
    @JSONField(name = "restrict_partner_id")
    @JsonProperty("restrict_partner_id")
    private Long restrictPartnerId;
    /**
     * 库存规则
     */
    @DEField(name = "rule_id")
    @TableField(value = "rule_id")
    @JSONField(name = "rule_id")
    @JsonProperty("rule_id")
    private Long ruleId;
    /**
     * 仓库
     */
    @DEField(name = "warehouse_id")
    @TableField(value = "warehouse_id")
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Long warehouseId;
    /**
     * 目的位置
     */
    @DEField(name = "location_dest_id")
    @TableField(value = "location_dest_id")
    @JSONField(name = "location_dest_id")
    @JsonProperty("location_dest_id")
    private Long locationDestId;
    /**
     * 原材料的生产订单
     */
    @DEField(name = "raw_material_production_id")
    @TableField(value = "raw_material_production_id")
    @JSONField(name = "raw_material_production_id")
    @JsonProperty("raw_material_production_id")
    private Long rawMaterialProductionId;
    /**
     * 库存
     */
    @DEField(name = "inventory_id")
    @TableField(value = "inventory_id")
    @JSONField(name = "inventory_id")
    @JsonProperty("inventory_id")
    private Long inventoryId;
    /**
     * 采购订单行
     */
    @DEField(name = "purchase_line_id")
    @TableField(value = "purchase_line_id")
    @JSONField(name = "purchase_line_id")
    @JsonProperty("purchase_line_id")
    private Long purchaseLineId;
    /**
     * 源位置
     */
    @DEField(name = "location_id")
    @TableField(value = "location_id")
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Long locationId;
    /**
     * 公司
     */
    @DEField(name = "company_id" , preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_bom_line odooBomLine;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production odooCreatedProduction;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production odooProduction;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production odooRawMaterialProduction;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing_workcenter odooOperation;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild odooConsumeUnbuild;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild odooUnbuild;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder odooWorkorder;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_packaging odooProductPackging;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line odooCreatedPurchaseLine;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line odooPurchaseLine;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order odooRepair;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooRestrictPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line odooSaleLine;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_inventory odooInventory;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocationDest;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move odooOriginReturnedMove;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_package_level odooPackageLevel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooPickingType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking odooPicking;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule odooRule;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooWarehouse;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom;



    /**
     * 设置 [在分拣确认后是否添加了移动]
     */
    public void setAdditional(Boolean additional){
        this.additional = additional ;
        this.modify("additional",additional);
    }

    /**
     * 设置 [单价]
     */
    public void setPriceUnit(Double priceUnit){
        this.priceUnit = priceUnit ;
        this.modify("price_unit",priceUnit);
    }

    /**
     * 设置 [补货组]
     */
    public void setGroupId(Integer groupId){
        this.groupId = groupId ;
        this.modify("group_id",groupId);
    }

    /**
     * 设置 [退款 (更新 SO/PO)]
     */
    public void setToRefund(Boolean toRefund){
        this.toRefund = toRefund ;
        this.modify("to_refund",toRefund);
    }

    /**
     * 设置 [单位因子]
     */
    public void setUnitFactor(Double unitFactor){
        this.unitFactor = unitFactor ;
        this.modify("unit_factor",unitFactor);
    }

    /**
     * 设置 [剩余数量]
     */
    public void setRemainingQty(Double remainingQty){
        this.remainingQty = remainingQty ;
        this.modify("remaining_qty",remainingQty);
    }

    /**
     * 设置 [传播取消以及拆分]
     */
    public void setPropagate(Boolean propagate){
        this.propagate = propagate ;
        this.modify("propagate",propagate);
    }

    /**
     * 设置 [编号]
     */
    public void setReference(String reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [实际数量]
     */
    public void setProductQty(Double productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [源文档]
     */
    public void setOrigin(String origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [初始需求]
     */
    public void setProductUomQty(Double productUomQty){
        this.productUomQty = productUomQty ;
        this.modify("product_uom_qty",productUomQty);
    }

    /**
     * 设置 [优先级]
     */
    public void setPriority(String priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 格式化日期 [日期]
     */
    public String formatDate(){
        if (this.date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
    /**
     * 设置 [剩余价值]
     */
    public void setRemainingValue(Double remainingValue){
        this.remainingValue = remainingValue ;
        this.modify("remaining_value",remainingValue);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [备注]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [说明]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [值]
     */
    public void setValue(Double value){
        this.value = value ;
        this.modify("value",value);
    }

    /**
     * 设置 [完成]
     */
    public void setIsDone(Boolean isDone){
        this.isDone = isDone ;
        this.modify("is_done",isDone);
    }

    /**
     * 设置 [预计日期]
     */
    public void setDateExpected(Timestamp dateExpected){
        this.dateExpected = dateExpected ;
        this.modify("date_expected",dateExpected);
    }

    /**
     * 格式化日期 [预计日期]
     */
    public String formatDateExpected(){
        if (this.dateExpected == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateExpected);
    }
    /**
     * 设置 [供应方法]
     */
    public void setProcureMethod(String procureMethod){
        this.procureMethod = procureMethod ;
        this.modify("procure_method",procureMethod);
    }

    /**
     * 设置 [创建生产订单]
     */
    public void setCreatedProductionId(Long createdProductionId){
        this.createdProductionId = createdProductionId ;
        this.modify("created_production_id",createdProductionId);
    }

    /**
     * 设置 [拆卸单]
     */
    public void setConsumeUnbuildId(Long consumeUnbuildId){
        this.consumeUnbuildId = consumeUnbuildId ;
        this.modify("consume_unbuild_id",consumeUnbuildId);
    }

    /**
     * 设置 [销售明细行]
     */
    public void setSaleLineId(Long saleLineId){
        this.saleLineId = saleLineId ;
        this.modify("sale_line_id",saleLineId);
    }

    /**
     * 设置 [原始退回移动]
     */
    public void setOriginReturnedMoveId(Long originReturnedMoveId){
        this.originReturnedMoveId = originReturnedMoveId ;
        this.modify("origin_returned_move_id",originReturnedMoveId);
    }

    /**
     * 设置 [作业类型]
     */
    public void setPickingTypeId(Long pickingTypeId){
        this.pickingTypeId = pickingTypeId ;
        this.modify("picking_type_id",pickingTypeId);
    }

    /**
     * 设置 [待加工的作业]
     */
    public void setOperationId(Long operationId){
        this.operationId = operationId ;
        this.modify("operation_id",operationId);
    }

    /**
     * 设置 [BOM行]
     */
    public void setBomLineId(Long bomLineId){
        this.bomLineId = bomLineId ;
        this.modify("bom_line_id",bomLineId);
    }

    /**
     * 设置 [维修]
     */
    public void setRepairId(Long repairId){
        this.repairId = repairId ;
        this.modify("repair_id",repairId);
    }

    /**
     * 设置 [产品]
     */
    public void setProductId(Long productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [拆卸顺序]
     */
    public void setUnbuildId(Long unbuildId){
        this.unbuildId = unbuildId ;
        this.modify("unbuild_id",unbuildId);
    }

    /**
     * 设置 [成品的生产订单]
     */
    public void setProductionId(Long productionId){
        this.productionId = productionId ;
        this.modify("production_id",productionId);
    }

    /**
     * 设置 [创建采购订单行]
     */
    public void setCreatedPurchaseLineId(Long createdPurchaseLineId){
        this.createdPurchaseLineId = createdPurchaseLineId ;
        this.modify("created_purchase_line_id",createdPurchaseLineId);
    }

    /**
     * 设置 [待消耗的工单]
     */
    public void setWorkorderId(Long workorderId){
        this.workorderId = workorderId ;
        this.modify("workorder_id",workorderId);
    }

    /**
     * 设置 [单位]
     */
    public void setProductUom(Long productUom){
        this.productUom = productUom ;
        this.modify("product_uom",productUom);
    }

    /**
     * 设置 [调拨参照]
     */
    public void setPickingId(Long pickingId){
        this.pickingId = pickingId ;
        this.modify("picking_id",pickingId);
    }

    /**
     * 设置 [目的地地址]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [包裹层级]
     */
    public void setPackageLevelId(Long packageLevelId){
        this.packageLevelId = packageLevelId ;
        this.modify("package_level_id",packageLevelId);
    }

    /**
     * 设置 [首选包装]
     */
    public void setProductPackaging(Long productPackaging){
        this.productPackaging = productPackaging ;
        this.modify("product_packaging",productPackaging);
    }

    /**
     * 设置 [所有者]
     */
    public void setRestrictPartnerId(Long restrictPartnerId){
        this.restrictPartnerId = restrictPartnerId ;
        this.modify("restrict_partner_id",restrictPartnerId);
    }

    /**
     * 设置 [库存规则]
     */
    public void setRuleId(Long ruleId){
        this.ruleId = ruleId ;
        this.modify("rule_id",ruleId);
    }

    /**
     * 设置 [仓库]
     */
    public void setWarehouseId(Long warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }

    /**
     * 设置 [目的位置]
     */
    public void setLocationDestId(Long locationDestId){
        this.locationDestId = locationDestId ;
        this.modify("location_dest_id",locationDestId);
    }

    /**
     * 设置 [原材料的生产订单]
     */
    public void setRawMaterialProductionId(Long rawMaterialProductionId){
        this.rawMaterialProductionId = rawMaterialProductionId ;
        this.modify("raw_material_production_id",rawMaterialProductionId);
    }

    /**
     * 设置 [库存]
     */
    public void setInventoryId(Long inventoryId){
        this.inventoryId = inventoryId ;
        this.modify("inventory_id",inventoryId);
    }

    /**
     * 设置 [采购订单行]
     */
    public void setPurchaseLineId(Long purchaseLineId){
        this.purchaseLineId = purchaseLineId ;
        this.modify("purchase_line_id",purchaseLineId);
    }

    /**
     * 设置 [源位置]
     */
    public void setLocationId(Long locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


