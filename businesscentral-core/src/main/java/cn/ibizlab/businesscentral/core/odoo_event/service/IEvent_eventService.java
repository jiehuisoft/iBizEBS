package cn.ibizlab.businesscentral.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_eventSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Event_event] 服务对象接口
 */
public interface IEvent_eventService extends IService<Event_event>{

    boolean create(Event_event et) ;
    void createBatch(List<Event_event> list) ;
    boolean update(Event_event et) ;
    void updateBatch(List<Event_event> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Event_event get(Long key) ;
    Event_event getDraft(Event_event et) ;
    boolean checkKey(Event_event et) ;
    boolean save(Event_event et) ;
    void saveBatch(List<Event_event> list) ;
    Page<Event_event> searchDefault(Event_eventSearchContext context) ;
    List<Event_event> selectByEventTypeId(Long id);
    void resetByEventTypeId(Long id);
    void resetByEventTypeId(Collection<Long> ids);
    void removeByEventTypeId(Long id);
    List<Event_event> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Event_event> selectByCountryId(Long id);
    void resetByCountryId(Long id);
    void resetByCountryId(Collection<Long> ids);
    void removeByCountryId(Long id);
    List<Event_event> selectByAddressId(Long id);
    void resetByAddressId(Long id);
    void resetByAddressId(Collection<Long> ids);
    void removeByAddressId(Long id);
    List<Event_event> selectByOrganizerId(Long id);
    void resetByOrganizerId(Long id);
    void resetByOrganizerId(Collection<Long> ids);
    void removeByOrganizerId(Long id);
    List<Event_event> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Event_event> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Event_event> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Event_event> selectByMenuId(Long id);
    void resetByMenuId(Long id);
    void resetByMenuId(Collection<Long> ids);
    void removeByMenuId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Event_event> getEventEventByIds(List<Long> ids) ;
    List<Event_event> getEventEventByEntities(List<Event_event> entities) ;
}


