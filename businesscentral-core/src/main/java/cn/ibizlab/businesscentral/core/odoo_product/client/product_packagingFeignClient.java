package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_packaging;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_packagingSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_packaging] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-product:odoo-product}", contextId = "product-packaging", fallback = product_packagingFallback.class)
public interface product_packagingFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/product_packagings/search")
    Page<Product_packaging> search(@RequestBody Product_packagingSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/product_packagings/{id}")
    Product_packaging get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/product_packagings")
    Product_packaging create(@RequestBody Product_packaging product_packaging);

    @RequestMapping(method = RequestMethod.POST, value = "/product_packagings/batch")
    Boolean createBatch(@RequestBody List<Product_packaging> product_packagings);


    @RequestMapping(method = RequestMethod.DELETE, value = "/product_packagings/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_packagings/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_packagings/{id}")
    Product_packaging update(@PathVariable("id") Long id,@RequestBody Product_packaging product_packaging);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_packagings/batch")
    Boolean updateBatch(@RequestBody List<Product_packaging> product_packagings);


    @RequestMapping(method = RequestMethod.GET, value = "/product_packagings/select")
    Page<Product_packaging> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_packagings/getdraft")
    Product_packaging getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/product_packagings/checkkey")
    Boolean checkKey(@RequestBody Product_packaging product_packaging);


    @RequestMapping(method = RequestMethod.POST, value = "/product_packagings/save")
    Boolean save(@RequestBody Product_packaging product_packaging);

    @RequestMapping(method = RequestMethod.POST, value = "/product_packagings/savebatch")
    Boolean saveBatch(@RequestBody List<Product_packaging> product_packagings);



    @RequestMapping(method = RequestMethod.POST, value = "/product_packagings/searchdefault")
    Page<Product_packaging> searchDefault(@RequestBody Product_packagingSearchContext context);


}
