package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_automation_line_test;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_automation_line_testSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_automation_line_test] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "base-automation-line-test", fallback = base_automation_line_testFallback.class)
public interface base_automation_line_testFeignClient {



    @RequestMapping(method = RequestMethod.DELETE, value = "/base_automation_line_tests/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_automation_line_tests/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/base_automation_line_tests/{id}")
    Base_automation_line_test get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/base_automation_line_tests")
    Base_automation_line_test create(@RequestBody Base_automation_line_test base_automation_line_test);

    @RequestMapping(method = RequestMethod.POST, value = "/base_automation_line_tests/batch")
    Boolean createBatch(@RequestBody List<Base_automation_line_test> base_automation_line_tests);


    @RequestMapping(method = RequestMethod.PUT, value = "/base_automation_line_tests/{id}")
    Base_automation_line_test update(@PathVariable("id") Long id,@RequestBody Base_automation_line_test base_automation_line_test);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_automation_line_tests/batch")
    Boolean updateBatch(@RequestBody List<Base_automation_line_test> base_automation_line_tests);



    @RequestMapping(method = RequestMethod.POST, value = "/base_automation_line_tests/search")
    Page<Base_automation_line_test> search(@RequestBody Base_automation_line_testSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/base_automation_line_tests/select")
    Page<Base_automation_line_test> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_automation_line_tests/getdraft")
    Base_automation_line_test getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/base_automation_line_tests/checkkey")
    Boolean checkKey(@RequestBody Base_automation_line_test base_automation_line_test);


    @RequestMapping(method = RequestMethod.POST, value = "/base_automation_line_tests/save")
    Boolean save(@RequestBody Base_automation_line_test base_automation_line_test);

    @RequestMapping(method = RequestMethod.POST, value = "/base_automation_line_tests/savebatch")
    Boolean saveBatch(@RequestBody List<Base_automation_line_test> base_automation_line_tests);



    @RequestMapping(method = RequestMethod.POST, value = "/base_automation_line_tests/searchdefault")
    Page<Base_automation_line_test> searchDefault(@RequestBody Base_automation_line_testSearchContext context);


}
