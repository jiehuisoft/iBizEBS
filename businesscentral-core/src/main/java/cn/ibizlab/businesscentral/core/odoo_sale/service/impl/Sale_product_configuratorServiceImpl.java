package cn.ibizlab.businesscentral.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_product_configurator;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_product_configuratorSearchContext;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_product_configuratorService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_sale.mapper.Sale_product_configuratorMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[销售产品配置器] 服务对象接口实现
 */
@Slf4j
@Service("Sale_product_configuratorServiceImpl")
public class Sale_product_configuratorServiceImpl extends EBSServiceImpl<Sale_product_configuratorMapper, Sale_product_configurator> implements ISale_product_configuratorService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelistService productPricelistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_templateService productTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "sale.product.configurator" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Sale_product_configurator et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_product_configuratorService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Sale_product_configurator> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Sale_product_configurator et) {
        Sale_product_configurator old = new Sale_product_configurator() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_product_configuratorService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_product_configuratorService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Sale_product_configurator> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Sale_product_configurator get(Long key) {
        Sale_product_configurator et = getById(key);
        if(et==null){
            et=new Sale_product_configurator();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Sale_product_configurator getDraft(Sale_product_configurator et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Sale_product_configurator et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Sale_product_configurator et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Sale_product_configurator et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Sale_product_configurator> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Sale_product_configurator> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Sale_product_configurator> selectByPricelistId(Long id) {
        return baseMapper.selectByPricelistId(id);
    }
    @Override
    public void resetByPricelistId(Long id) {
        this.update(new UpdateWrapper<Sale_product_configurator>().set("pricelist_id",null).eq("pricelist_id",id));
    }

    @Override
    public void resetByPricelistId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_product_configurator>().set("pricelist_id",null).in("pricelist_id",ids));
    }

    @Override
    public void removeByPricelistId(Long id) {
        this.remove(new QueryWrapper<Sale_product_configurator>().eq("pricelist_id",id));
    }

	@Override
    public List<Sale_product_configurator> selectByProductTemplateId(Long id) {
        return baseMapper.selectByProductTemplateId(id);
    }
    @Override
    public void resetByProductTemplateId(Long id) {
        this.update(new UpdateWrapper<Sale_product_configurator>().set("product_template_id",null).eq("product_template_id",id));
    }

    @Override
    public void resetByProductTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_product_configurator>().set("product_template_id",null).in("product_template_id",ids));
    }

    @Override
    public void removeByProductTemplateId(Long id) {
        this.remove(new QueryWrapper<Sale_product_configurator>().eq("product_template_id",id));
    }

	@Override
    public List<Sale_product_configurator> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Sale_product_configurator>().eq("create_uid",id));
    }

	@Override
    public List<Sale_product_configurator> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Sale_product_configurator>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Sale_product_configurator> searchDefault(Sale_product_configuratorSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Sale_product_configurator> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Sale_product_configurator>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Sale_product_configurator et){
        //实体关系[DER1N_SALE_PRODUCT_CONFIGURATOR__PRODUCT_PRICELIST__PRICELIST_ID]
        if(!ObjectUtils.isEmpty(et.getPricelistId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist odooPricelist=et.getOdooPricelist();
            if(ObjectUtils.isEmpty(odooPricelist)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist majorEntity=productPricelistService.get(et.getPricelistId());
                et.setOdooPricelist(majorEntity);
                odooPricelist=majorEntity;
            }
            et.setPricelistIdText(odooPricelist.getName());
        }
        //实体关系[DER1N_SALE_PRODUCT_CONFIGURATOR__PRODUCT_TEMPLATE__PRODUCT_TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getProductTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template odooProductTemplate=et.getOdooProductTemplate();
            if(ObjectUtils.isEmpty(odooProductTemplate)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template majorEntity=productTemplateService.get(et.getProductTemplateId());
                et.setOdooProductTemplate(majorEntity);
                odooProductTemplate=majorEntity;
            }
            et.setProductTemplateIdText(odooProductTemplate.getName());
        }
        //实体关系[DER1N_SALE_PRODUCT_CONFIGURATOR__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_SALE_PRODUCT_CONFIGURATOR__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Sale_product_configurator> getSaleProductConfiguratorByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Sale_product_configurator> getSaleProductConfiguratorByEntities(List<Sale_product_configurator> entities) {
        List ids =new ArrayList();
        for(Sale_product_configurator entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



