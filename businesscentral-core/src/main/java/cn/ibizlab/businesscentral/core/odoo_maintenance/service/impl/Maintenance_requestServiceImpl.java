package cn.ibizlab.businesscentral.core.odoo_maintenance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_request;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_requestSearchContext;
import cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_requestService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_maintenance.mapper.Maintenance_requestMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[保养请求] 服务对象接口实现
 */
@Slf4j
@Service("Maintenance_requestServiceImpl")
public class Maintenance_requestServiceImpl extends EBSServiceImpl<Maintenance_requestMapper, Maintenance_request> implements IMaintenance_requestService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_departmentService hrDepartmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipment_categoryService maintenanceEquipmentCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipmentService maintenanceEquipmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_stageService maintenanceStageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_teamService maintenanceTeamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "maintenance.request" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Maintenance_request et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMaintenance_requestService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Maintenance_request> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Maintenance_request et) {
        Maintenance_request old = new Maintenance_request() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMaintenance_requestService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMaintenance_requestService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Maintenance_request> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Maintenance_request get(Long key) {
        Maintenance_request et = getById(key);
        if(et==null){
            et=new Maintenance_request();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Maintenance_request getDraft(Maintenance_request et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Maintenance_request et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Maintenance_request et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Maintenance_request et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Maintenance_request> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Maintenance_request> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Maintenance_request> selectByDepartmentId(Long id) {
        return baseMapper.selectByDepartmentId(id);
    }
    @Override
    public void resetByDepartmentId(Long id) {
        this.update(new UpdateWrapper<Maintenance_request>().set("department_id",null).eq("department_id",id));
    }

    @Override
    public void resetByDepartmentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_request>().set("department_id",null).in("department_id",ids));
    }

    @Override
    public void removeByDepartmentId(Long id) {
        this.remove(new QueryWrapper<Maintenance_request>().eq("department_id",id));
    }

	@Override
    public List<Maintenance_request> selectByEmployeeId(Long id) {
        return baseMapper.selectByEmployeeId(id);
    }
    @Override
    public void resetByEmployeeId(Long id) {
        this.update(new UpdateWrapper<Maintenance_request>().set("employee_id",null).eq("employee_id",id));
    }

    @Override
    public void resetByEmployeeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_request>().set("employee_id",null).in("employee_id",ids));
    }

    @Override
    public void removeByEmployeeId(Long id) {
        this.remove(new QueryWrapper<Maintenance_request>().eq("employee_id",id));
    }

	@Override
    public List<Maintenance_request> selectByCategoryId(Long id) {
        return baseMapper.selectByCategoryId(id);
    }
    @Override
    public void resetByCategoryId(Long id) {
        this.update(new UpdateWrapper<Maintenance_request>().set("category_id",null).eq("category_id",id));
    }

    @Override
    public void resetByCategoryId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_request>().set("category_id",null).in("category_id",ids));
    }

    @Override
    public void removeByCategoryId(Long id) {
        this.remove(new QueryWrapper<Maintenance_request>().eq("category_id",id));
    }

	@Override
    public List<Maintenance_request> selectByEquipmentId(Long id) {
        return baseMapper.selectByEquipmentId(id);
    }
    @Override
    public List<Maintenance_request> selectByEquipmentId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Maintenance_request>().in("id",ids));
    }

    @Override
    public void removeByEquipmentId(Long id) {
        this.remove(new QueryWrapper<Maintenance_request>().eq("equipment_id",id));
    }

	@Override
    public List<Maintenance_request> selectByStageId(Long id) {
        return baseMapper.selectByStageId(id);
    }
    @Override
    public List<Maintenance_request> selectByStageId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Maintenance_request>().in("id",ids));
    }

    @Override
    public void removeByStageId(Long id) {
        this.remove(new QueryWrapper<Maintenance_request>().eq("stage_id",id));
    }

	@Override
    public List<Maintenance_request> selectByMaintenanceTeamId(Long id) {
        return baseMapper.selectByMaintenanceTeamId(id);
    }
    @Override
    public void resetByMaintenanceTeamId(Long id) {
        this.update(new UpdateWrapper<Maintenance_request>().set("maintenance_team_id",null).eq("maintenance_team_id",id));
    }

    @Override
    public void resetByMaintenanceTeamId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_request>().set("maintenance_team_id",null).in("maintenance_team_id",ids));
    }

    @Override
    public void removeByMaintenanceTeamId(Long id) {
        this.remove(new QueryWrapper<Maintenance_request>().eq("maintenance_team_id",id));
    }

	@Override
    public List<Maintenance_request> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Maintenance_request>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_request>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Maintenance_request>().eq("company_id",id));
    }

	@Override
    public List<Maintenance_request> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Maintenance_request>().eq("create_uid",id));
    }

	@Override
    public List<Maintenance_request> selectByOwnerUserId(Long id) {
        return baseMapper.selectByOwnerUserId(id);
    }
    @Override
    public void resetByOwnerUserId(Long id) {
        this.update(new UpdateWrapper<Maintenance_request>().set("owner_user_id",null).eq("owner_user_id",id));
    }

    @Override
    public void resetByOwnerUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_request>().set("owner_user_id",null).in("owner_user_id",ids));
    }

    @Override
    public void removeByOwnerUserId(Long id) {
        this.remove(new QueryWrapper<Maintenance_request>().eq("owner_user_id",id));
    }

	@Override
    public List<Maintenance_request> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Maintenance_request>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Maintenance_request>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Maintenance_request>().eq("user_id",id));
    }

	@Override
    public List<Maintenance_request> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Maintenance_request>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Maintenance_request> searchDefault(Maintenance_requestSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Maintenance_request> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Maintenance_request>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Maintenance_request et){
        //实体关系[DER1N_MAINTENANCE_REQUEST__HR_DEPARTMENT__DEPARTMENT_ID]
        if(!ObjectUtils.isEmpty(et.getDepartmentId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department odooDepartment=et.getOdooDepartment();
            if(ObjectUtils.isEmpty(odooDepartment)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department majorEntity=hrDepartmentService.get(et.getDepartmentId());
                et.setOdooDepartment(majorEntity);
                odooDepartment=majorEntity;
            }
            et.setDepartmentIdText(odooDepartment.getName());
        }
        //实体关系[DER1N_MAINTENANCE_REQUEST__HR_EMPLOYEE__EMPLOYEE_ID]
        if(!ObjectUtils.isEmpty(et.getEmployeeId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmployee=et.getOdooEmployee();
            if(ObjectUtils.isEmpty(odooEmployee)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getEmployeeId());
                et.setOdooEmployee(majorEntity);
                odooEmployee=majorEntity;
            }
            et.setEmployeeIdText(odooEmployee.getName());
        }
        //实体关系[DER1N_MAINTENANCE_REQUEST__MAINTENANCE_EQUIPMENT_CATEGORY__CATEGORY_ID]
        if(!ObjectUtils.isEmpty(et.getCategoryId())){
            cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment_category odooCategory=et.getOdooCategory();
            if(ObjectUtils.isEmpty(odooCategory)){
                cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment_category majorEntity=maintenanceEquipmentCategoryService.get(et.getCategoryId());
                et.setOdooCategory(majorEntity);
                odooCategory=majorEntity;
            }
            et.setCategoryIdText(odooCategory.getName());
        }
        //实体关系[DER1N_MAINTENANCE_REQUEST__MAINTENANCE_EQUIPMENT__EQUIPMENT_ID]
        if(!ObjectUtils.isEmpty(et.getEquipmentId())){
            cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment odooEquipment=et.getOdooEquipment();
            if(ObjectUtils.isEmpty(odooEquipment)){
                cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment majorEntity=maintenanceEquipmentService.get(et.getEquipmentId());
                et.setOdooEquipment(majorEntity);
                odooEquipment=majorEntity;
            }
            et.setEquipmentIdText(odooEquipment.getName());
        }
        //实体关系[DER1N_MAINTENANCE_REQUEST__MAINTENANCE_STAGE__STAGE_ID]
        if(!ObjectUtils.isEmpty(et.getStageId())){
            cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_stage odooStage=et.getOdooStage();
            if(ObjectUtils.isEmpty(odooStage)){
                cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_stage majorEntity=maintenanceStageService.get(et.getStageId());
                et.setOdooStage(majorEntity);
                odooStage=majorEntity;
            }
            et.setStageIdText(odooStage.getName());
        }
        //实体关系[DER1N_MAINTENANCE_REQUEST__MAINTENANCE_TEAM__MAINTENANCE_TEAM_ID]
        if(!ObjectUtils.isEmpty(et.getMaintenanceTeamId())){
            cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_team odooMaintenanceTeam=et.getOdooMaintenanceTeam();
            if(ObjectUtils.isEmpty(odooMaintenanceTeam)){
                cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_team majorEntity=maintenanceTeamService.get(et.getMaintenanceTeamId());
                et.setOdooMaintenanceTeam(majorEntity);
                odooMaintenanceTeam=majorEntity;
            }
            et.setMaintenanceTeamIdText(odooMaintenanceTeam.getName());
        }
        //实体关系[DER1N_MAINTENANCE_REQUEST__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_MAINTENANCE_REQUEST__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MAINTENANCE_REQUEST__RES_USERS__OWNER_USER_ID]
        if(!ObjectUtils.isEmpty(et.getOwnerUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooOwnerUser=et.getOdooOwnerUser();
            if(ObjectUtils.isEmpty(odooOwnerUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getOwnerUserId());
                et.setOdooOwnerUser(majorEntity);
                odooOwnerUser=majorEntity;
            }
            et.setOwnerUserIdText(odooOwnerUser.getName());
        }
        //实体关系[DER1N_MAINTENANCE_REQUEST__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_MAINTENANCE_REQUEST__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Maintenance_request> getMaintenanceRequestByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Maintenance_request> getMaintenanceRequestByEntities(List<Maintenance_request> entities) {
        List ids =new ArrayList();
        for(Maintenance_request entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



