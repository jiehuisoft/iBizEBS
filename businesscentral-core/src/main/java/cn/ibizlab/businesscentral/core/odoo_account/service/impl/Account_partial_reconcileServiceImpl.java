package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_partial_reconcile;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_partial_reconcileSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_partial_reconcileService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_partial_reconcileMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[部分核销] 服务对象接口实现
 */
@Slf4j
@Service("Account_partial_reconcileServiceImpl")
public class Account_partial_reconcileServiceImpl extends EBSServiceImpl<Account_partial_reconcileMapper, Account_partial_reconcile> implements IAccount_partial_reconcileService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_moveService accountMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_full_reconcileService accountFullReconcileService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_lineService accountMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.partial.reconcile" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_partial_reconcile et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_partial_reconcileService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_partial_reconcile> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_partial_reconcile et) {
        Account_partial_reconcile old = new Account_partial_reconcile() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_partial_reconcileService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_partial_reconcileService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_partial_reconcile> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountMoveService.resetByTaxCashBasisRecId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountMoveService.resetByTaxCashBasisRecId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_partial_reconcile get(Long key) {
        Account_partial_reconcile et = getById(key);
        if(et==null){
            et=new Account_partial_reconcile();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_partial_reconcile getDraft(Account_partial_reconcile et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_partial_reconcile et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_partial_reconcile et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_partial_reconcile et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_partial_reconcile> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_partial_reconcile> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_partial_reconcile> selectByFullReconcileId(Long id) {
        return baseMapper.selectByFullReconcileId(id);
    }
    @Override
    public void resetByFullReconcileId(Long id) {
        this.update(new UpdateWrapper<Account_partial_reconcile>().set("full_reconcile_id",null).eq("full_reconcile_id",id));
    }

    @Override
    public void resetByFullReconcileId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_partial_reconcile>().set("full_reconcile_id",null).in("full_reconcile_id",ids));
    }

    @Override
    public void removeByFullReconcileId(Long id) {
        this.remove(new QueryWrapper<Account_partial_reconcile>().eq("full_reconcile_id",id));
    }

	@Override
    public List<Account_partial_reconcile> selectByCreditMoveId(Long id) {
        return baseMapper.selectByCreditMoveId(id);
    }
    @Override
    public void resetByCreditMoveId(Long id) {
        this.update(new UpdateWrapper<Account_partial_reconcile>().set("credit_move_id",null).eq("credit_move_id",id));
    }

    @Override
    public void resetByCreditMoveId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_partial_reconcile>().set("credit_move_id",null).in("credit_move_id",ids));
    }

    @Override
    public void removeByCreditMoveId(Long id) {
        this.remove(new QueryWrapper<Account_partial_reconcile>().eq("credit_move_id",id));
    }

	@Override
    public List<Account_partial_reconcile> selectByDebitMoveId(Long id) {
        return baseMapper.selectByDebitMoveId(id);
    }
    @Override
    public void resetByDebitMoveId(Long id) {
        this.update(new UpdateWrapper<Account_partial_reconcile>().set("debit_move_id",null).eq("debit_move_id",id));
    }

    @Override
    public void resetByDebitMoveId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_partial_reconcile>().set("debit_move_id",null).in("debit_move_id",ids));
    }

    @Override
    public void removeByDebitMoveId(Long id) {
        this.remove(new QueryWrapper<Account_partial_reconcile>().eq("debit_move_id",id));
    }

	@Override
    public List<Account_partial_reconcile> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Account_partial_reconcile>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_partial_reconcile>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Account_partial_reconcile>().eq("company_id",id));
    }

	@Override
    public List<Account_partial_reconcile> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Account_partial_reconcile>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_partial_reconcile>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Account_partial_reconcile>().eq("currency_id",id));
    }

	@Override
    public List<Account_partial_reconcile> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_partial_reconcile>().eq("create_uid",id));
    }

	@Override
    public List<Account_partial_reconcile> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_partial_reconcile>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_partial_reconcile> searchDefault(Account_partial_reconcileSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_partial_reconcile> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_partial_reconcile>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_partial_reconcile et){
        //实体关系[DER1N_ACCOUNT_PARTIAL_RECONCILE__ACCOUNT_FULL_RECONCILE__FULL_RECONCILE_ID]
        if(!ObjectUtils.isEmpty(et.getFullReconcileId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_full_reconcile odooFullReconcile=et.getOdooFullReconcile();
            if(ObjectUtils.isEmpty(odooFullReconcile)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_full_reconcile majorEntity=accountFullReconcileService.get(et.getFullReconcileId());
                et.setOdooFullReconcile(majorEntity);
                odooFullReconcile=majorEntity;
            }
            et.setFullReconcileIdText(odooFullReconcile.getName());
        }
        //实体关系[DER1N_ACCOUNT_PARTIAL_RECONCILE__ACCOUNT_MOVE_LINE__CREDIT_MOVE_ID]
        if(!ObjectUtils.isEmpty(et.getCreditMoveId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move_line odooCreditMove=et.getOdooCreditMove();
            if(ObjectUtils.isEmpty(odooCreditMove)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move_line majorEntity=accountMoveLineService.get(et.getCreditMoveId());
                et.setOdooCreditMove(majorEntity);
                odooCreditMove=majorEntity;
            }
            et.setCreditMoveIdText(odooCreditMove.getName());
        }
        //实体关系[DER1N_ACCOUNT_PARTIAL_RECONCILE__ACCOUNT_MOVE_LINE__DEBIT_MOVE_ID]
        if(!ObjectUtils.isEmpty(et.getDebitMoveId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move_line odooDebitMove=et.getOdooDebitMove();
            if(ObjectUtils.isEmpty(odooDebitMove)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move_line majorEntity=accountMoveLineService.get(et.getDebitMoveId());
                et.setOdooDebitMove(majorEntity);
                odooDebitMove=majorEntity;
            }
            et.setDebitMoveIdText(odooDebitMove.getName());
        }
        //实体关系[DER1N_ACCOUNT_PARTIAL_RECONCILE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
            et.setCompanyCurrencyId(odooCompany.getCurrencyId());
        }
        //实体关系[DER1N_ACCOUNT_PARTIAL_RECONCILE__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_ACCOUNT_PARTIAL_RECONCILE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_PARTIAL_RECONCILE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_partial_reconcile> getAccountPartialReconcileByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_partial_reconcile> getAccountPartialReconcileByEntities(List<Account_partial_reconcile> entities) {
        List ids =new ArrayList();
        for(Account_partial_reconcile entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



