package cn.ibizlab.businesscentral.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead2opportunity_partner;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_lead2opportunity_partnerSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Crm_lead2opportunity_partner] 服务对象接口
 */
public interface ICrm_lead2opportunity_partnerService extends IService<Crm_lead2opportunity_partner>{

    boolean create(Crm_lead2opportunity_partner et) ;
    void createBatch(List<Crm_lead2opportunity_partner> list) ;
    boolean update(Crm_lead2opportunity_partner et) ;
    void updateBatch(List<Crm_lead2opportunity_partner> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Crm_lead2opportunity_partner get(Long key) ;
    Crm_lead2opportunity_partner getDraft(Crm_lead2opportunity_partner et) ;
    boolean checkKey(Crm_lead2opportunity_partner et) ;
    boolean save(Crm_lead2opportunity_partner et) ;
    void saveBatch(List<Crm_lead2opportunity_partner> list) ;
    Page<Crm_lead2opportunity_partner> searchDefault(Crm_lead2opportunity_partnerSearchContext context) ;
    List<Crm_lead2opportunity_partner> selectByTeamId(Long id);
    void resetByTeamId(Long id);
    void resetByTeamId(Collection<Long> ids);
    void removeByTeamId(Long id);
    List<Crm_lead2opportunity_partner> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Crm_lead2opportunity_partner> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Crm_lead2opportunity_partner> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Crm_lead2opportunity_partner> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Crm_lead2opportunity_partner> getCrmLead2opportunityPartnerByIds(List<Long> ids) ;
    List<Crm_lead2opportunity_partner> getCrmLead2opportunityPartnerByEntities(List<Crm_lead2opportunity_partner> entities) ;
}


