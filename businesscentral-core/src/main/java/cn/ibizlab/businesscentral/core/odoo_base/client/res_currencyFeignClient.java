package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_currencySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_currency] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "res-currency", fallback = res_currencyFallback.class)
public interface res_currencyFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/res_currencies/{id}")
    Res_currency get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.POST, value = "/res_currencies/search")
    Page<Res_currency> search(@RequestBody Res_currencySearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/res_currencies/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_currencies/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/res_currencies/{id}")
    Res_currency update(@PathVariable("id") Long id,@RequestBody Res_currency res_currency);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_currencies/batch")
    Boolean updateBatch(@RequestBody List<Res_currency> res_currencies);


    @RequestMapping(method = RequestMethod.POST, value = "/res_currencies")
    Res_currency create(@RequestBody Res_currency res_currency);

    @RequestMapping(method = RequestMethod.POST, value = "/res_currencies/batch")
    Boolean createBatch(@RequestBody List<Res_currency> res_currencies);




    @RequestMapping(method = RequestMethod.GET, value = "/res_currencies/select")
    Page<Res_currency> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_currencies/getdraft")
    Res_currency getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/res_currencies/checkkey")
    Boolean checkKey(@RequestBody Res_currency res_currency);


    @RequestMapping(method = RequestMethod.POST, value = "/res_currencies/save")
    Boolean save(@RequestBody Res_currency res_currency);

    @RequestMapping(method = RequestMethod.POST, value = "/res_currencies/savebatch")
    Boolean saveBatch(@RequestBody List<Res_currency> res_currencies);



    @RequestMapping(method = RequestMethod.POST, value = "/res_currencies/searchdefault")
    Page<Res_currency> searchDEFAULT(@RequestBody Res_currencySearchContext context);


}
