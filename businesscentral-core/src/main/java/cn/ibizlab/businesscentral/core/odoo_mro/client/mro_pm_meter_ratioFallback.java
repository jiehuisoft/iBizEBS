package cn.ibizlab.businesscentral.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_ratio;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_meter_ratioSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mro_pm_meter_ratio] 服务对象接口
 */
@Component
public class mro_pm_meter_ratioFallback implements mro_pm_meter_ratioFeignClient{

    public Page<Mro_pm_meter_ratio> search(Mro_pm_meter_ratioSearchContext context){
            return null;
     }


    public Mro_pm_meter_ratio get(Long id){
            return null;
     }


    public Mro_pm_meter_ratio create(Mro_pm_meter_ratio mro_pm_meter_ratio){
            return null;
     }
    public Boolean createBatch(List<Mro_pm_meter_ratio> mro_pm_meter_ratios){
            return false;
     }

    public Mro_pm_meter_ratio update(Long id, Mro_pm_meter_ratio mro_pm_meter_ratio){
            return null;
     }
    public Boolean updateBatch(List<Mro_pm_meter_ratio> mro_pm_meter_ratios){
            return false;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Page<Mro_pm_meter_ratio> select(){
            return null;
     }

    public Mro_pm_meter_ratio getDraft(){
            return null;
    }



    public Boolean checkKey(Mro_pm_meter_ratio mro_pm_meter_ratio){
            return false;
     }


    public Boolean save(Mro_pm_meter_ratio mro_pm_meter_ratio){
            return false;
     }
    public Boolean saveBatch(List<Mro_pm_meter_ratio> mro_pm_meter_ratios){
            return false;
     }

    public Page<Mro_pm_meter_ratio> searchDefault(Mro_pm_meter_ratioSearchContext context){
            return null;
     }


}
