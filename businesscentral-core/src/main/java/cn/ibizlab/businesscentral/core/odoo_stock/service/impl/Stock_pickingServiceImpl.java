package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_pickingSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_pickingService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_pickingMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[调拨] 服务对象接口实现
 */
@Slf4j
@Service("Stock_pickingServiceImpl")
public class Stock_pickingServiceImpl extends EBSServiceImpl<Stock_pickingMapper, Stock_picking> implements IStock_pickingService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_move_lineService stockMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_overprocessed_transferService stockOverprocessedTransferService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_package_destinationService stockPackageDestinationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_package_levelService stockPackageLevelService;

    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_pickingService stockPickingService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_return_pickingService stockReturnPickingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_scrapService stockScrapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_orderService saleOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_picking_typeService stockPickingTypeService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.picking" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_picking et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_pickingService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_picking> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_picking et) {
        Stock_picking old = new Stock_picking() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_pickingService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_pickingService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_picking> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        stockMoveLineService.resetByPickingId(key);
        stockMoveService.resetByPickingId(key);
        stockOverprocessedTransferService.resetByPickingId(key);
        stockPackageDestinationService.resetByPickingId(key);
        stockPackageLevelService.resetByPickingId(key);
        stockPickingService.resetByBackorderId(key);
        stockReturnPickingService.resetByPickingId(key);
        stockScrapService.resetByPickingId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        stockMoveLineService.resetByPickingId(idList);
        stockMoveService.resetByPickingId(idList);
        stockOverprocessedTransferService.resetByPickingId(idList);
        stockPackageDestinationService.resetByPickingId(idList);
        stockPackageLevelService.resetByPickingId(idList);
        stockPickingService.resetByBackorderId(idList);
        stockReturnPickingService.resetByPickingId(idList);
        stockScrapService.resetByPickingId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_picking get(Long key) {
        Stock_picking et = getById(key);
        if(et==null){
            et=new Stock_picking();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_picking getDraft(Stock_picking et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_picking et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_picking et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_picking et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_picking> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_picking> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_picking> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Stock_picking>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_picking>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Stock_picking>().eq("company_id",id));
    }

	@Override
    public List<Stock_picking> selectByOwnerId(Long id) {
        return baseMapper.selectByOwnerId(id);
    }
    @Override
    public void resetByOwnerId(Long id) {
        this.update(new UpdateWrapper<Stock_picking>().set("owner_id",null).eq("owner_id",id));
    }

    @Override
    public void resetByOwnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_picking>().set("owner_id",null).in("owner_id",ids));
    }

    @Override
    public void removeByOwnerId(Long id) {
        this.remove(new QueryWrapper<Stock_picking>().eq("owner_id",id));
    }

	@Override
    public List<Stock_picking> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Stock_picking>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_picking>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Stock_picking>().eq("partner_id",id));
    }

	@Override
    public List<Stock_picking> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_picking>().eq("create_uid",id));
    }

	@Override
    public List<Stock_picking> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_picking>().eq("write_uid",id));
    }

	@Override
    public List<Stock_picking> selectBySaleId(Long id) {
        return baseMapper.selectBySaleId(id);
    }
    @Override
    public void resetBySaleId(Long id) {
        this.update(new UpdateWrapper<Stock_picking>().set("sale_id",null).eq("sale_id",id));
    }

    @Override
    public void resetBySaleId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_picking>().set("sale_id",null).in("sale_id",ids));
    }

    @Override
    public void removeBySaleId(Long id) {
        this.remove(new QueryWrapper<Stock_picking>().eq("sale_id",id));
    }

	@Override
    public List<Stock_picking> selectByLocationDestId(Long id) {
        return baseMapper.selectByLocationDestId(id);
    }
    @Override
    public void resetByLocationDestId(Long id) {
        this.update(new UpdateWrapper<Stock_picking>().set("location_dest_id",null).eq("location_dest_id",id));
    }

    @Override
    public void resetByLocationDestId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_picking>().set("location_dest_id",null).in("location_dest_id",ids));
    }

    @Override
    public void removeByLocationDestId(Long id) {
        this.remove(new QueryWrapper<Stock_picking>().eq("location_dest_id",id));
    }

	@Override
    public List<Stock_picking> selectByLocationId(Long id) {
        return baseMapper.selectByLocationId(id);
    }
    @Override
    public void resetByLocationId(Long id) {
        this.update(new UpdateWrapper<Stock_picking>().set("location_id",null).eq("location_id",id));
    }

    @Override
    public void resetByLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_picking>().set("location_id",null).in("location_id",ids));
    }

    @Override
    public void removeByLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_picking>().eq("location_id",id));
    }

	@Override
    public List<Stock_picking> selectByPickingTypeId(Long id) {
        return baseMapper.selectByPickingTypeId(id);
    }
    @Override
    public void resetByPickingTypeId(Long id) {
        this.update(new UpdateWrapper<Stock_picking>().set("picking_type_id",null).eq("picking_type_id",id));
    }

    @Override
    public void resetByPickingTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_picking>().set("picking_type_id",null).in("picking_type_id",ids));
    }

    @Override
    public void removeByPickingTypeId(Long id) {
        this.remove(new QueryWrapper<Stock_picking>().eq("picking_type_id",id));
    }

	@Override
    public List<Stock_picking> selectByBackorderId(Long id) {
        return baseMapper.selectByBackorderId(id);
    }
    @Override
    public void resetByBackorderId(Long id) {
        this.update(new UpdateWrapper<Stock_picking>().set("backorder_id",null).eq("backorder_id",id));
    }

    @Override
    public void resetByBackorderId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_picking>().set("backorder_id",null).in("backorder_id",ids));
    }

    @Override
    public void removeByBackorderId(Long id) {
        this.remove(new QueryWrapper<Stock_picking>().eq("backorder_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_picking> searchDefault(Stock_pickingSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_picking> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_picking>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_picking et){
        //实体关系[DER1N_STOCK_PICKING__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_STOCK_PICKING__RES_PARTNER__OWNER_ID]
        if(!ObjectUtils.isEmpty(et.getOwnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooOwner=et.getOdooOwner();
            if(ObjectUtils.isEmpty(odooOwner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getOwnerId());
                et.setOdooOwner(majorEntity);
                odooOwner=majorEntity;
            }
            et.setOwnerIdText(odooOwner.getName());
        }
        //实体关系[DER1N_STOCK_PICKING__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_STOCK_PICKING__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_PICKING__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_PICKING__SALE_ORDER__SALE_ID]
        if(!ObjectUtils.isEmpty(et.getSaleId())){
            cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order odooSale=et.getOdooSale();
            if(ObjectUtils.isEmpty(odooSale)){
                cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order majorEntity=saleOrderService.get(et.getSaleId());
                et.setOdooSale(majorEntity);
                odooSale=majorEntity;
            }
            et.setSaleIdText(odooSale.getName());
        }
        //实体关系[DER1N_STOCK_PICKING__STOCK_LOCATION__LOCATION_DEST_ID]
        if(!ObjectUtils.isEmpty(et.getLocationDestId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocationDest=et.getOdooLocationDest();
            if(ObjectUtils.isEmpty(odooLocationDest)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationDestId());
                et.setOdooLocationDest(majorEntity);
                odooLocationDest=majorEntity;
            }
            et.setLocationDestIdText(odooLocationDest.getName());
        }
        //实体关系[DER1N_STOCK_PICKING__STOCK_LOCATION__LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation=et.getOdooLocation();
            if(ObjectUtils.isEmpty(odooLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationId());
                et.setOdooLocation(majorEntity);
                odooLocation=majorEntity;
            }
            et.setLocationIdText(odooLocation.getName());
        }
        //实体关系[DER1N_STOCK_PICKING__STOCK_PICKING_TYPE__PICKING_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getPickingTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooPickingType=et.getOdooPickingType();
            if(ObjectUtils.isEmpty(odooPickingType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getPickingTypeId());
                et.setOdooPickingType(majorEntity);
                odooPickingType=majorEntity;
            }
            et.setPickingTypeIdText(odooPickingType.getName());
            et.setPickingTypeCode(odooPickingType.getCode());
            et.setPickingTypeEntirePacks(odooPickingType.getShowEntirePacks());
        }
        //实体关系[DER1N_STOCK_PICKING__STOCK_PICKING__BACKORDER_ID]
        if(!ObjectUtils.isEmpty(et.getBackorderId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking odooBackorder=et.getOdooBackorder();
            if(ObjectUtils.isEmpty(odooBackorder)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking majorEntity=stockPickingService.get(et.getBackorderId());
                et.setOdooBackorder(majorEntity);
                odooBackorder=majorEntity;
            }
            et.setBackorderIdText(odooBackorder.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_picking> getStockPickingByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_picking> getStockPickingByEntities(List<Stock_picking> entities) {
        List ids =new ArrayList();
        for(Stock_picking entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



