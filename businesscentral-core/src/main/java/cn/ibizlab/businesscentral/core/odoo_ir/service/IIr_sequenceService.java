package cn.ibizlab.businesscentral.core.odoo_ir.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_sequence;
import cn.ibizlab.businesscentral.core.odoo_ir.filter.Ir_sequenceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Ir_sequence] 服务对象接口
 */
public interface IIr_sequenceService extends IService<Ir_sequence>{

    boolean create(Ir_sequence et) ;
    void createBatch(List<Ir_sequence> list) ;
    boolean update(Ir_sequence et) ;
    void updateBatch(List<Ir_sequence> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Ir_sequence get(Long key) ;
    Ir_sequence getDraft(Ir_sequence et) ;
    Ir_sequence alter_sequence(Ir_sequence et) ;
    boolean checkKey(Ir_sequence et) ;
    Ir_sequence create_sequence(Ir_sequence et) ;
    Ir_sequence drop_sequence(Ir_sequence et) ;
    Ir_sequence get_next(Ir_sequence et) ;
    Ir_sequence get_next_by_code(Ir_sequence et) ;
    Ir_sequence predict_nextval(Ir_sequence et) ;
    boolean save(Ir_sequence et) ;
    void saveBatch(List<Ir_sequence> list) ;
    Ir_sequence select_nextval(Ir_sequence et) ;
    Ir_sequence update_nogap(Ir_sequence et) ;
    Page<Ir_sequence> searchDefault(Ir_sequenceSearchContext context) ;
    List<Ir_sequence> selectByCompanyId(Long id);
    void removeByCompanyId(Long id);
    List<Ir_sequence> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Ir_sequence> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


