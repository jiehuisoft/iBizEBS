package cn.ibizlab.businesscentral.core.odoo_digest.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_digest.domain.Digest_tip;
import cn.ibizlab.businesscentral.core.odoo_digest.filter.Digest_tipSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Digest_tipMapper extends BaseMapper<Digest_tip>{

    Page<Digest_tip> searchDefault(IPage page, @Param("srf") Digest_tipSearchContext context, @Param("ew") Wrapper<Digest_tip> wrapper) ;
    @Override
    Digest_tip selectById(Serializable id);
    @Override
    int insert(Digest_tip entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Digest_tip entity);
    @Override
    int update(@Param(Constants.ENTITY) Digest_tip entity, @Param("ew") Wrapper<Digest_tip> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Digest_tip> selectByGroupId(@Param("id") Serializable id) ;

    List<Digest_tip> selectByCreateUid(@Param("id") Serializable id) ;

    List<Digest_tip> selectByWriteUid(@Param("id") Serializable id) ;


}
