package cn.ibizlab.businesscentral.core.odoo_web_editor.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test;
import cn.ibizlab.businesscentral.core.odoo_web_editor.filter.Web_editor_converter_testSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[web_editor_converter_test] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-web-editor:odoo-web-editor}", contextId = "web-editor-converter-test", fallback = web_editor_converter_testFallback.class)
public interface web_editor_converter_testFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests/search")
    Page<Web_editor_converter_test> search(@RequestBody Web_editor_converter_testSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/web_editor_converter_tests/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/web_editor_converter_tests/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/web_editor_converter_tests/{id}")
    Web_editor_converter_test update(@PathVariable("id") Long id,@RequestBody Web_editor_converter_test web_editor_converter_test);

    @RequestMapping(method = RequestMethod.PUT, value = "/web_editor_converter_tests/batch")
    Boolean updateBatch(@RequestBody List<Web_editor_converter_test> web_editor_converter_tests);



    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests")
    Web_editor_converter_test create(@RequestBody Web_editor_converter_test web_editor_converter_test);

    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests/batch")
    Boolean createBatch(@RequestBody List<Web_editor_converter_test> web_editor_converter_tests);


    @RequestMapping(method = RequestMethod.GET, value = "/web_editor_converter_tests/{id}")
    Web_editor_converter_test get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.GET, value = "/web_editor_converter_tests/select")
    Page<Web_editor_converter_test> select();


    @RequestMapping(method = RequestMethod.GET, value = "/web_editor_converter_tests/getdraft")
    Web_editor_converter_test getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests/checkkey")
    Boolean checkKey(@RequestBody Web_editor_converter_test web_editor_converter_test);


    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests/save")
    Boolean save(@RequestBody Web_editor_converter_test web_editor_converter_test);

    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests/savebatch")
    Boolean saveBatch(@RequestBody List<Web_editor_converter_test> web_editor_converter_tests);



    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests/searchdefault")
    Page<Web_editor_converter_test> searchDefault(@RequestBody Web_editor_converter_testSearchContext context);


}
