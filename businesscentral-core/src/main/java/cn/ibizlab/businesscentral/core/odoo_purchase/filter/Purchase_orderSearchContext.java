package cn.ibizlab.businesscentral.core.odoo_purchase.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order;
/**
 * 关系型数据实体[Purchase_order] 查询条件对象
 */
@Slf4j
@Data
public class Purchase_orderSearchContext extends QueryWrapperContext<Purchase_order> {

	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_default_location_dest_id_usage_eq;//[目标位置类型]
	public void setN_default_location_dest_id_usage_eq(String n_default_location_dest_id_usage_eq) {
        this.n_default_location_dest_id_usage_eq = n_default_location_dest_id_usage_eq;
        if(!ObjectUtils.isEmpty(this.n_default_location_dest_id_usage_eq)){
            this.getSearchCond().eq("default_location_dest_id_usage", n_default_location_dest_id_usage_eq);
        }
    }
	private String n_name_like;//[订单关联]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_invoice_status_eq;//[账单状态]
	public void setN_invoice_status_eq(String n_invoice_status_eq) {
        this.n_invoice_status_eq = n_invoice_status_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_status_eq)){
            this.getSearchCond().eq("invoice_status", n_invoice_status_eq);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_user_id_text_eq;//[采购员]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[采购员]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_fiscal_position_id_text_eq;//[税科目调整]
	public void setN_fiscal_position_id_text_eq(String n_fiscal_position_id_text_eq) {
        this.n_fiscal_position_id_text_eq = n_fiscal_position_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_text_eq)){
            this.getSearchCond().eq("fiscal_position_id_text", n_fiscal_position_id_text_eq);
        }
    }
	private String n_fiscal_position_id_text_like;//[税科目调整]
	public void setN_fiscal_position_id_text_like(String n_fiscal_position_id_text_like) {
        this.n_fiscal_position_id_text_like = n_fiscal_position_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_text_like)){
            this.getSearchCond().like("fiscal_position_id_text", n_fiscal_position_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_dest_address_id_text_eq;//[代发货地址]
	public void setN_dest_address_id_text_eq(String n_dest_address_id_text_eq) {
        this.n_dest_address_id_text_eq = n_dest_address_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_dest_address_id_text_eq)){
            this.getSearchCond().eq("dest_address_id_text", n_dest_address_id_text_eq);
        }
    }
	private String n_dest_address_id_text_like;//[代发货地址]
	public void setN_dest_address_id_text_like(String n_dest_address_id_text_like) {
        this.n_dest_address_id_text_like = n_dest_address_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_dest_address_id_text_like)){
            this.getSearchCond().like("dest_address_id_text", n_dest_address_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_requisition_id_text_eq;//[采购申请单]
	public void setN_requisition_id_text_eq(String n_requisition_id_text_eq) {
        this.n_requisition_id_text_eq = n_requisition_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_requisition_id_text_eq)){
            this.getSearchCond().eq("requisition_id_text", n_requisition_id_text_eq);
        }
    }
	private String n_requisition_id_text_like;//[采购申请单]
	public void setN_requisition_id_text_like(String n_requisition_id_text_like) {
        this.n_requisition_id_text_like = n_requisition_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_requisition_id_text_like)){
            this.getSearchCond().like("requisition_id_text", n_requisition_id_text_like);
        }
    }
	private String n_payment_term_id_text_eq;//[付款条款]
	public void setN_payment_term_id_text_eq(String n_payment_term_id_text_eq) {
        this.n_payment_term_id_text_eq = n_payment_term_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_term_id_text_eq)){
            this.getSearchCond().eq("payment_term_id_text", n_payment_term_id_text_eq);
        }
    }
	private String n_payment_term_id_text_like;//[付款条款]
	public void setN_payment_term_id_text_like(String n_payment_term_id_text_like) {
        this.n_payment_term_id_text_like = n_payment_term_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_payment_term_id_text_like)){
            this.getSearchCond().like("payment_term_id_text", n_payment_term_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_partner_id_text_eq;//[供应商]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[供应商]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_picking_type_id_text_eq;//[交货到]
	public void setN_picking_type_id_text_eq(String n_picking_type_id_text_eq) {
        this.n_picking_type_id_text_eq = n_picking_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_text_eq)){
            this.getSearchCond().eq("picking_type_id_text", n_picking_type_id_text_eq);
        }
    }
	private String n_picking_type_id_text_like;//[交货到]
	public void setN_picking_type_id_text_like(String n_picking_type_id_text_like) {
        this.n_picking_type_id_text_like = n_picking_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_text_like)){
            this.getSearchCond().like("picking_type_id_text", n_picking_type_id_text_like);
        }
    }
	private String n_incoterm_id_text_eq;//[国际贸易术语]
	public void setN_incoterm_id_text_eq(String n_incoterm_id_text_eq) {
        this.n_incoterm_id_text_eq = n_incoterm_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_incoterm_id_text_eq)){
            this.getSearchCond().eq("incoterm_id_text", n_incoterm_id_text_eq);
        }
    }
	private String n_incoterm_id_text_like;//[国际贸易术语]
	public void setN_incoterm_id_text_like(String n_incoterm_id_text_like) {
        this.n_incoterm_id_text_like = n_incoterm_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_incoterm_id_text_like)){
            this.getSearchCond().like("incoterm_id_text", n_incoterm_id_text_like);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_partner_id_eq;//[供应商]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_dest_address_id_eq;//[代发货地址]
	public void setN_dest_address_id_eq(Long n_dest_address_id_eq) {
        this.n_dest_address_id_eq = n_dest_address_id_eq;
        if(!ObjectUtils.isEmpty(this.n_dest_address_id_eq)){
            this.getSearchCond().eq("dest_address_id", n_dest_address_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_requisition_id_eq;//[采购申请单]
	public void setN_requisition_id_eq(Long n_requisition_id_eq) {
        this.n_requisition_id_eq = n_requisition_id_eq;
        if(!ObjectUtils.isEmpty(this.n_requisition_id_eq)){
            this.getSearchCond().eq("requisition_id", n_requisition_id_eq);
        }
    }
	private Long n_payment_term_id_eq;//[付款条款]
	public void setN_payment_term_id_eq(Long n_payment_term_id_eq) {
        this.n_payment_term_id_eq = n_payment_term_id_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_term_id_eq)){
            this.getSearchCond().eq("payment_term_id", n_payment_term_id_eq);
        }
    }
	private Long n_user_id_eq;//[采购员]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_incoterm_id_eq;//[国际贸易术语]
	public void setN_incoterm_id_eq(Long n_incoterm_id_eq) {
        this.n_incoterm_id_eq = n_incoterm_id_eq;
        if(!ObjectUtils.isEmpty(this.n_incoterm_id_eq)){
            this.getSearchCond().eq("incoterm_id", n_incoterm_id_eq);
        }
    }
	private Long n_fiscal_position_id_eq;//[税科目调整]
	public void setN_fiscal_position_id_eq(Long n_fiscal_position_id_eq) {
        this.n_fiscal_position_id_eq = n_fiscal_position_id_eq;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_eq)){
            this.getSearchCond().eq("fiscal_position_id", n_fiscal_position_id_eq);
        }
    }
	private Long n_picking_type_id_eq;//[交货到]
	public void setN_picking_type_id_eq(Long n_picking_type_id_eq) {
        this.n_picking_type_id_eq = n_picking_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_eq)){
            this.getSearchCond().eq("picking_type_id", n_picking_type_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}



