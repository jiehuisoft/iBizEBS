package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_group;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_country_groupSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_country_group] 服务对象接口
 */
@Component
public class res_country_groupFallback implements res_country_groupFeignClient{

    public Res_country_group get(Long id){
            return null;
     }


    public Res_country_group update(Long id, Res_country_group res_country_group){
            return null;
     }
    public Boolean updateBatch(List<Res_country_group> res_country_groups){
            return false;
     }




    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Res_country_group> search(Res_country_groupSearchContext context){
            return null;
     }


    public Res_country_group create(Res_country_group res_country_group){
            return null;
     }
    public Boolean createBatch(List<Res_country_group> res_country_groups){
            return false;
     }

    public Page<Res_country_group> select(){
            return null;
     }

    public Res_country_group getDraft(){
            return null;
    }



    public Boolean checkKey(Res_country_group res_country_group){
            return false;
     }


    public Boolean save(Res_country_group res_country_group){
            return false;
     }
    public Boolean saveBatch(List<Res_country_group> res_country_groups){
            return false;
     }

    public Page<Res_country_group> searchDefault(Res_country_groupSearchContext context){
            return null;
     }


}
