package cn.ibizlab.businesscentral.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_compose_message;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_compose_messageSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_compose_messageService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mail.mapper.Mail_compose_messageMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[邮件撰写向导] 服务对象接口实现
 */
@Slf4j
@Service("Mail_compose_messageServiceImpl")
public class Mail_compose_messageServiceImpl extends EBSServiceImpl<Mail_compose_messageMapper, Mail_compose_message> implements IMail_compose_messageService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_sendService accountInvoiceSendService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_activity_typeService mailActivityTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_campaignService mailMassMailingCampaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailingService mailMassMailingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_message_subtypeService mailMessageSubtypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_messageService mailMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_templateService mailTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mail.compose.message" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mail_compose_message et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_compose_messageService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mail_compose_message> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mail_compose_message et) {
        Mail_compose_message old = new Mail_compose_message() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_compose_messageService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_compose_messageService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mail_compose_message> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountInvoiceSendService.removeByComposerId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountInvoiceSendService.removeByComposerId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mail_compose_message get(Long key) {
        Mail_compose_message et = getById(key);
        if(et==null){
            et=new Mail_compose_message();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mail_compose_message getDraft(Mail_compose_message et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mail_compose_message et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mail_compose_message et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mail_compose_message et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mail_compose_message> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mail_compose_message> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mail_compose_message> selectByMailActivityTypeId(Long id) {
        return baseMapper.selectByMailActivityTypeId(id);
    }
    @Override
    public void resetByMailActivityTypeId(Long id) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("mail_activity_type_id",null).eq("mail_activity_type_id",id));
    }

    @Override
    public void resetByMailActivityTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("mail_activity_type_id",null).in("mail_activity_type_id",ids));
    }

    @Override
    public void removeByMailActivityTypeId(Long id) {
        this.remove(new QueryWrapper<Mail_compose_message>().eq("mail_activity_type_id",id));
    }

	@Override
    public List<Mail_compose_message> selectByMassMailingCampaignId(Long id) {
        return baseMapper.selectByMassMailingCampaignId(id);
    }
    @Override
    public void resetByMassMailingCampaignId(Long id) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("mass_mailing_campaign_id",null).eq("mass_mailing_campaign_id",id));
    }

    @Override
    public void resetByMassMailingCampaignId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("mass_mailing_campaign_id",null).in("mass_mailing_campaign_id",ids));
    }

    @Override
    public void removeByMassMailingCampaignId(Long id) {
        this.remove(new QueryWrapper<Mail_compose_message>().eq("mass_mailing_campaign_id",id));
    }

	@Override
    public List<Mail_compose_message> selectByMassMailingId(Long id) {
        return baseMapper.selectByMassMailingId(id);
    }
    @Override
    public void removeByMassMailingId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Mail_compose_message>().in("mass_mailing_id",ids));
    }

    @Override
    public void removeByMassMailingId(Long id) {
        this.remove(new QueryWrapper<Mail_compose_message>().eq("mass_mailing_id",id));
    }

	@Override
    public List<Mail_compose_message> selectBySubtypeId(Long id) {
        return baseMapper.selectBySubtypeId(id);
    }
    @Override
    public void resetBySubtypeId(Long id) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("subtype_id",null).eq("subtype_id",id));
    }

    @Override
    public void resetBySubtypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("subtype_id",null).in("subtype_id",ids));
    }

    @Override
    public void removeBySubtypeId(Long id) {
        this.remove(new QueryWrapper<Mail_compose_message>().eq("subtype_id",id));
    }

	@Override
    public List<Mail_compose_message> selectByParentId(Long id) {
        return baseMapper.selectByParentId(id);
    }
    @Override
    public void resetByParentId(Long id) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("parent_id",null).eq("parent_id",id));
    }

    @Override
    public void resetByParentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("parent_id",null).in("parent_id",ids));
    }

    @Override
    public void removeByParentId(Long id) {
        this.remove(new QueryWrapper<Mail_compose_message>().eq("parent_id",id));
    }

	@Override
    public List<Mail_compose_message> selectByTemplateId(Long id) {
        return baseMapper.selectByTemplateId(id);
    }
    @Override
    public void resetByTemplateId(Long id) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("template_id",null).eq("template_id",id));
    }

    @Override
    public void resetByTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("template_id",null).in("template_id",ids));
    }

    @Override
    public void removeByTemplateId(Long id) {
        this.remove(new QueryWrapper<Mail_compose_message>().eq("template_id",id));
    }

	@Override
    public List<Mail_compose_message> selectByAuthorId(Long id) {
        return baseMapper.selectByAuthorId(id);
    }
    @Override
    public void resetByAuthorId(Long id) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("author_id",null).eq("author_id",id));
    }

    @Override
    public void resetByAuthorId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("author_id",null).in("author_id",ids));
    }

    @Override
    public void removeByAuthorId(Long id) {
        this.remove(new QueryWrapper<Mail_compose_message>().eq("author_id",id));
    }

	@Override
    public List<Mail_compose_message> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mail_compose_message>().eq("create_uid",id));
    }

	@Override
    public List<Mail_compose_message> selectByModeratorId(Long id) {
        return baseMapper.selectByModeratorId(id);
    }
    @Override
    public void resetByModeratorId(Long id) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("moderator_id",null).eq("moderator_id",id));
    }

    @Override
    public void resetByModeratorId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_compose_message>().set("moderator_id",null).in("moderator_id",ids));
    }

    @Override
    public void removeByModeratorId(Long id) {
        this.remove(new QueryWrapper<Mail_compose_message>().eq("moderator_id",id));
    }

	@Override
    public List<Mail_compose_message> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mail_compose_message>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mail_compose_message> searchDefault(Mail_compose_messageSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mail_compose_message> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mail_compose_message>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mail_compose_message et){
        //实体关系[DER1N_MAIL_COMPOSE_MESSAGE__MAIL_ACTIVITY_TYPE__MAIL_ACTIVITY_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getMailActivityTypeId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity_type odooMailActivityType=et.getOdooMailActivityType();
            if(ObjectUtils.isEmpty(odooMailActivityType)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity_type majorEntity=mailActivityTypeService.get(et.getMailActivityTypeId());
                et.setOdooMailActivityType(majorEntity);
                odooMailActivityType=majorEntity;
            }
            et.setMailActivityTypeIdText(odooMailActivityType.getName());
        }
        //实体关系[DER1N_MAIL_COMPOSE_MESSAGE__MAIL_MASS_MAILING_CAMPAIGN__MASS_MAILING_CAMPAIGN_ID]
        if(!ObjectUtils.isEmpty(et.getMassMailingCampaignId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_campaign odooMassMailingCampaign=et.getOdooMassMailingCampaign();
            if(ObjectUtils.isEmpty(odooMassMailingCampaign)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_campaign majorEntity=mailMassMailingCampaignService.get(et.getMassMailingCampaignId());
                et.setOdooMassMailingCampaign(majorEntity);
                odooMassMailingCampaign=majorEntity;
            }
            et.setMassMailingCampaignIdText(odooMassMailingCampaign.getName());
        }
        //实体关系[DER1N_MAIL_COMPOSE_MESSAGE__MAIL_MASS_MAILING__MASS_MAILING_ID]
        if(!ObjectUtils.isEmpty(et.getMassMailingId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing odooMassMailing=et.getOdooMassMailing();
            if(ObjectUtils.isEmpty(odooMassMailing)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing majorEntity=mailMassMailingService.get(et.getMassMailingId());
                et.setOdooMassMailing(majorEntity);
                odooMassMailing=majorEntity;
            }
            et.setMassMailingIdText(odooMassMailing.getName());
        }
        //实体关系[DER1N_MAIL_COMPOSE_MESSAGE__MAIL_MESSAGE_SUBTYPE__SUBTYPE_ID]
        if(!ObjectUtils.isEmpty(et.getSubtypeId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message_subtype odooSubtype=et.getOdooSubtype();
            if(ObjectUtils.isEmpty(odooSubtype)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message_subtype majorEntity=mailMessageSubtypeService.get(et.getSubtypeId());
                et.setOdooSubtype(majorEntity);
                odooSubtype=majorEntity;
            }
            et.setSubtypeIdText(odooSubtype.getName());
        }
        //实体关系[DER1N_MAIL_COMPOSE_MESSAGE__MAIL_TEMPLATE__TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooTemplate=et.getOdooTemplate();
            if(ObjectUtils.isEmpty(odooTemplate)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template majorEntity=mailTemplateService.get(et.getTemplateId());
                et.setOdooTemplate(majorEntity);
                odooTemplate=majorEntity;
            }
            et.setTemplateIdText(odooTemplate.getName());
        }
        //实体关系[DER1N_MAIL_COMPOSE_MESSAGE__RES_PARTNER__AUTHOR_ID]
        if(!ObjectUtils.isEmpty(et.getAuthorId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooAuthor=et.getOdooAuthor();
            if(ObjectUtils.isEmpty(odooAuthor)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getAuthorId());
                et.setOdooAuthor(majorEntity);
                odooAuthor=majorEntity;
            }
            et.setAuthorIdText(odooAuthor.getName());
            et.setAuthorAvatar(odooAuthor.getImageSmall());
        }
        //实体关系[DER1N_MAIL_COMPOSE_MESSAGE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MAIL_COMPOSE_MESSAGE__RES_USERS__MODERATOR_ID]
        if(!ObjectUtils.isEmpty(et.getModeratorId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooModerator=et.getOdooModerator();
            if(ObjectUtils.isEmpty(odooModerator)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getModeratorId());
                et.setOdooModerator(majorEntity);
                odooModerator=majorEntity;
            }
            et.setModeratorIdText(odooModerator.getName());
        }
        //实体关系[DER1N_MAIL_COMPOSE_MESSAGE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mail_compose_message> getMailComposeMessageByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mail_compose_message> getMailComposeMessageByEntities(List<Mail_compose_message> entities) {
        List ids =new ArrayList();
        for(Mail_compose_message entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}



