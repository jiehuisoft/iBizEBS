package cn.ibizlab.businesscentral.core.odoo_repair.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_orderSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Repair_order] 服务对象接口
 */
public interface IRepair_orderService extends IService<Repair_order>{

    boolean create(Repair_order et) ;
    void createBatch(List<Repair_order> list) ;
    boolean update(Repair_order et) ;
    void updateBatch(List<Repair_order> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Repair_order get(Long key) ;
    Repair_order getDraft(Repair_order et) ;
    boolean checkKey(Repair_order et) ;
    boolean save(Repair_order et) ;
    void saveBatch(List<Repair_order> list) ;
    Page<Repair_order> searchDefault(Repair_orderSearchContext context) ;
    List<Repair_order> selectByInvoiceId(Long id);
    void resetByInvoiceId(Long id);
    void resetByInvoiceId(Collection<Long> ids);
    void removeByInvoiceId(Long id);
    List<Repair_order> selectByPricelistId(Long id);
    void resetByPricelistId(Long id);
    void resetByPricelistId(Collection<Long> ids);
    void removeByPricelistId(Long id);
    List<Repair_order> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Repair_order> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Repair_order> selectByAddressId(Long id);
    void resetByAddressId(Long id);
    void resetByAddressId(Collection<Long> ids);
    void removeByAddressId(Long id);
    List<Repair_order> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Repair_order> selectByPartnerInvoiceId(Long id);
    void resetByPartnerInvoiceId(Long id);
    void resetByPartnerInvoiceId(Collection<Long> ids);
    void removeByPartnerInvoiceId(Long id);
    List<Repair_order> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Repair_order> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Repair_order> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    List<Repair_order> selectByMoveId(Long id);
    void resetByMoveId(Long id);
    void resetByMoveId(Collection<Long> ids);
    void removeByMoveId(Long id);
    List<Repair_order> selectByLotId(Long id);
    void resetByLotId(Long id);
    void resetByLotId(Collection<Long> ids);
    void removeByLotId(Long id);
    List<Repair_order> selectByProductUom(Long id);
    void resetByProductUom(Long id);
    void resetByProductUom(Collection<Long> ids);
    void removeByProductUom(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Repair_order> getRepairOrderByIds(List<Long> ids) ;
    List<Repair_order> getRepairOrderByEntities(List<Repair_order> entities) ;
}


