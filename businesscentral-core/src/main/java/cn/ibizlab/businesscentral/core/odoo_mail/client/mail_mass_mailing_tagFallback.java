package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_tag;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_tagSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_mass_mailing_tag] 服务对象接口
 */
@Component
public class mail_mass_mailing_tagFallback implements mail_mass_mailing_tagFeignClient{

    public Page<Mail_mass_mailing_tag> search(Mail_mass_mailing_tagSearchContext context){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Mail_mass_mailing_tag create(Mail_mass_mailing_tag mail_mass_mailing_tag){
            return null;
     }
    public Boolean createBatch(List<Mail_mass_mailing_tag> mail_mass_mailing_tags){
            return false;
     }

    public Mail_mass_mailing_tag update(Long id, Mail_mass_mailing_tag mail_mass_mailing_tag){
            return null;
     }
    public Boolean updateBatch(List<Mail_mass_mailing_tag> mail_mass_mailing_tags){
            return false;
     }



    public Mail_mass_mailing_tag get(Long id){
            return null;
     }


    public Page<Mail_mass_mailing_tag> select(){
            return null;
     }

    public Mail_mass_mailing_tag getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_mass_mailing_tag mail_mass_mailing_tag){
            return false;
     }


    public Boolean save(Mail_mass_mailing_tag mail_mass_mailing_tag){
            return false;
     }
    public Boolean saveBatch(List<Mail_mass_mailing_tag> mail_mass_mailing_tags){
            return false;
     }

    public Page<Mail_mass_mailing_tag> searchDefault(Mail_mass_mailing_tagSearchContext context){
            return null;
     }


}
