package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_image;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_imageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_image] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-product:odoo-product}", contextId = "product-image", fallback = product_imageFallback.class)
public interface product_imageFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/product_images")
    Product_image create(@RequestBody Product_image product_image);

    @RequestMapping(method = RequestMethod.POST, value = "/product_images/batch")
    Boolean createBatch(@RequestBody List<Product_image> product_images);


    @RequestMapping(method = RequestMethod.DELETE, value = "/product_images/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_images/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_images/{id}")
    Product_image update(@PathVariable("id") Long id,@RequestBody Product_image product_image);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_images/batch")
    Boolean updateBatch(@RequestBody List<Product_image> product_images);



    @RequestMapping(method = RequestMethod.POST, value = "/product_images/search")
    Page<Product_image> search(@RequestBody Product_imageSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/product_images/{id}")
    Product_image get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.GET, value = "/product_images/select")
    Page<Product_image> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_images/getdraft")
    Product_image getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/product_images/checkkey")
    Boolean checkKey(@RequestBody Product_image product_image);


    @RequestMapping(method = RequestMethod.POST, value = "/product_images/save")
    Boolean save(@RequestBody Product_image product_image);

    @RequestMapping(method = RequestMethod.POST, value = "/product_images/savebatch")
    Boolean saveBatch(@RequestBody List<Product_image> product_images);



    @RequestMapping(method = RequestMethod.POST, value = "/product_images/searchdefault")
    Page<Product_image> searchDefault(@RequestBody Product_imageSearchContext context);


}
