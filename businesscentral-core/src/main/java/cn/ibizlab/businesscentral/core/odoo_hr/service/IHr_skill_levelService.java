package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_skill_level;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_skill_levelSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_skill_level] 服务对象接口
 */
public interface IHr_skill_levelService extends IService<Hr_skill_level>{

    boolean create(Hr_skill_level et) ;
    void createBatch(List<Hr_skill_level> list) ;
    boolean update(Hr_skill_level et) ;
    void updateBatch(List<Hr_skill_level> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_skill_level get(Long key) ;
    Hr_skill_level getDraft(Hr_skill_level et) ;
    boolean checkKey(Hr_skill_level et) ;
    boolean save(Hr_skill_level et) ;
    void saveBatch(List<Hr_skill_level> list) ;
    Page<Hr_skill_level> searchDefault(Hr_skill_levelSearchContext context) ;
    List<Hr_skill_level> selectBySkillTypeId(Long id);
    void removeBySkillTypeId(Long id);
    List<Hr_skill_level> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_skill_level> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


