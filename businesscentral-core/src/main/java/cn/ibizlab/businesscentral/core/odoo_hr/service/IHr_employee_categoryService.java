package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee_category;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_employee_categorySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_employee_category] 服务对象接口
 */
public interface IHr_employee_categoryService extends IService<Hr_employee_category>{

    boolean create(Hr_employee_category et) ;
    void createBatch(List<Hr_employee_category> list) ;
    boolean update(Hr_employee_category et) ;
    void updateBatch(List<Hr_employee_category> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_employee_category get(Long key) ;
    Hr_employee_category getDraft(Hr_employee_category et) ;
    boolean checkKey(Hr_employee_category et) ;
    boolean save(Hr_employee_category et) ;
    void saveBatch(List<Hr_employee_category> list) ;
    Page<Hr_employee_category> searchDefault(Hr_employee_categorySearchContext context) ;
    List<Hr_employee_category> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_employee_category> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_employee_category> getHrEmployeeCategoryByIds(List<Long> ids) ;
    List<Hr_employee_category> getHrEmployeeCategoryByEntities(List<Hr_employee_category> entities) ;
}


