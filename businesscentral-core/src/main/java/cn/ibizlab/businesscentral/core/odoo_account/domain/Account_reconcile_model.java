package cn.ibizlab.businesscentral.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[请在发票和付款匹配期间创建日记账分录]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_RECONCILE_MODEL",resultMap = "Account_reconcile_modelResultMap")
public class Account_reconcile_model extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日记账项目标签
     */
    @TableField(value = "label")
    @JSONField(name = "label")
    @JsonProperty("label")
    private String label;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 含税价
     */
    @DEField(name = "force_tax_included")
    @TableField(value = "force_tax_included")
    @JSONField(name = "force_tax_included")
    @JsonProperty("force_tax_included")
    private Boolean forceTaxIncluded;
    /**
     * 金额类型
     */
    @DEField(name = "amount_type")
    @TableField(value = "amount_type")
    @JSONField(name = "amount_type")
    @JsonProperty("amount_type")
    private String amountType;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 第二含税价‎
     */
    @DEField(name = "force_second_tax_included")
    @TableField(value = "force_second_tax_included")
    @JSONField(name = "force_second_tax_included")
    @JsonProperty("force_second_tax_included")
    private Boolean forceSecondTaxIncluded;
    /**
     * 自动验证
     */
    @DEField(name = "auto_reconcile")
    @TableField(value = "auto_reconcile")
    @JSONField(name = "auto_reconcile")
    @JsonProperty("auto_reconcile")
    private Boolean autoReconcile;
    /**
     * 会计匹配
     */
    @DEField(name = "match_total_amount")
    @TableField(value = "match_total_amount")
    @JSONField(name = "match_total_amount")
    @JsonProperty("match_total_amount")
    private Boolean matchTotalAmount;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 凭证类型
     */
    @TableField(exist = false)
    @JSONField(name = "match_journal_ids")
    @JsonProperty("match_journal_ids")
    private String matchJournalIds;
    /**
     * 添加第二行
     */
    @DEField(name = "has_second_line")
    @TableField(value = "has_second_line")
    @JSONField(name = "has_second_line")
    @JsonProperty("has_second_line")
    private Boolean hasSecondLine;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 数量性质
     */
    @DEField(name = "match_nature")
    @TableField(value = "match_nature")
    @JSONField(name = "match_nature")
    @JsonProperty("match_nature")
    private String matchNature;
    /**
     * 第二金额类型
     */
    @DEField(name = "second_amount_type")
    @TableField(value = "second_amount_type")
    @JSONField(name = "second_amount_type")
    @JsonProperty("second_amount_type")
    private String secondAmountType;
    /**
     * 标签
     */
    @DEField(name = "match_label")
    @TableField(value = "match_label")
    @JSONField(name = "match_label")
    @JsonProperty("match_label")
    private String matchLabel;
    /**
     * 参数最大金额
     */
    @DEField(name = "match_amount_max")
    @TableField(value = "match_amount_max")
    @JSONField(name = "match_amount_max")
    @JsonProperty("match_amount_max")
    private Double matchAmountMax;
    /**
     * 会计匹配%
     */
    @DEField(name = "match_total_amount_param")
    @TableField(value = "match_total_amount_param")
    @JSONField(name = "match_total_amount_param")
    @JsonProperty("match_total_amount_param")
    private Double matchTotalAmountParam;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 第二核销金额
     */
    @DEField(name = "second_amount")
    @TableField(value = "second_amount")
    @JSONField(name = "second_amount")
    @JsonProperty("second_amount")
    private Double secondAmount;
    /**
     * 金额
     */
    @DEField(name = "match_amount")
    @TableField(value = "match_amount")
    @JSONField(name = "match_amount")
    @JsonProperty("match_amount")
    private String matchAmount;
    /**
     * 分析标签
     */
    @TableField(exist = false)
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    private String analyticTagIds;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 类型
     */
    @DEField(name = "rule_type")
    @TableField(value = "rule_type")
    @JSONField(name = "rule_type")
    @JsonProperty("rule_type")
    private String ruleType;
    /**
     * 限制合作伙伴为
     */
    @TableField(exist = false)
    @JSONField(name = "match_partner_ids")
    @JsonProperty("match_partner_ids")
    private String matchPartnerIds;
    /**
     * 限制合作伙伴类别为
     */
    @TableField(exist = false)
    @JSONField(name = "match_partner_category_ids")
    @JsonProperty("match_partner_category_ids")
    private String matchPartnerCategoryIds;
    /**
     * 第二个分录项目标签
     */
    @DEField(name = "second_label")
    @TableField(value = "second_label")
    @JSONField(name = "second_label")
    @JsonProperty("second_label")
    private String secondLabel;
    /**
     * 已经匹配合作伙伴
     */
    @DEField(name = "match_partner")
    @TableField(value = "match_partner")
    @JSONField(name = "match_partner")
    @JsonProperty("match_partner")
    private Boolean matchPartner;
    /**
     * 参数最小金额
     */
    @DEField(name = "match_amount_min")
    @TableField(value = "match_amount_min")
    @JSONField(name = "match_amount_min")
    @JsonProperty("match_amount_min")
    private Double matchAmountMin;
    /**
     * 同币种匹配
     */
    @DEField(name = "match_same_currency")
    @TableField(value = "match_same_currency")
    @JSONField(name = "match_same_currency")
    @JsonProperty("match_same_currency")
    private Boolean matchSameCurrency;
    /**
     * 第二分析标签
     */
    @TableField(exist = false)
    @JSONField(name = "second_analytic_tag_ids")
    @JsonProperty("second_analytic_tag_ids")
    private String secondAnalyticTagIds;
    /**
     * 核销金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;
    /**
     * 标签参数
     */
    @DEField(name = "match_label_param")
    @TableField(value = "match_label_param")
    @JSONField(name = "match_label_param")
    @JsonProperty("match_label_param")
    private String matchLabelParam;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 第二分析帐户
     */
    @TableField(exist = false)
    @JSONField(name = "second_analytic_account_id_text")
    @JsonProperty("second_analytic_account_id_text")
    private String secondAnalyticAccountIdText;
    /**
     * 第二个税
     */
    @TableField(exist = false)
    @JSONField(name = "second_tax_id_text")
    @JsonProperty("second_tax_id_text")
    private String secondTaxIdText;
    /**
     * 第二税率类别
     */
    @TableField(exist = false)
    @JSONField(name = "second_tax_amount_type")
    @JsonProperty("second_tax_amount_type")
    private String secondTaxAmountType;
    /**
     * 税率类别
     */
    @TableField(exist = false)
    @JSONField(name = "tax_amount_type")
    @JsonProperty("tax_amount_type")
    private String taxAmountType;
    /**
     * 日记账
     */
    @TableField(exist = false)
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;
    /**
     * 第二个分录
     */
    @TableField(exist = false)
    @JSONField(name = "second_journal_id_text")
    @JsonProperty("second_journal_id_text")
    private String secondJournalIdText;
    /**
     * 含税价
     */
    @TableField(exist = false)
    @JSONField(name = "is_tax_price_included")
    @JsonProperty("is_tax_price_included")
    private Boolean isTaxPriceIncluded;
    /**
     * 第二科目
     */
    @TableField(exist = false)
    @JSONField(name = "second_account_id_text")
    @JsonProperty("second_account_id_text")
    private String secondAccountIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 分析账户
     */
    @TableField(exist = false)
    @JSONField(name = "analytic_account_id_text")
    @JsonProperty("analytic_account_id_text")
    private String analyticAccountIdText;
    /**
     * 科目
     */
    @TableField(exist = false)
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 税率
     */
    @TableField(exist = false)
    @JSONField(name = "tax_id_text")
    @JsonProperty("tax_id_text")
    private String taxIdText;
    /**
     * 第二含税价‎
     */
    @TableField(exist = false)
    @JSONField(name = "is_second_tax_price_included")
    @JsonProperty("is_second_tax_price_included")
    private Boolean isSecondTaxPriceIncluded;
    /**
     * 第二分析帐户
     */
    @DEField(name = "second_analytic_account_id")
    @TableField(value = "second_analytic_account_id")
    @JSONField(name = "second_analytic_account_id")
    @JsonProperty("second_analytic_account_id")
    private Long secondAnalyticAccountId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 税率
     */
    @DEField(name = "tax_id")
    @TableField(value = "tax_id")
    @JSONField(name = "tax_id")
    @JsonProperty("tax_id")
    private Long taxId;
    /**
     * 第二科目
     */
    @DEField(name = "second_account_id")
    @TableField(value = "second_account_id")
    @JSONField(name = "second_account_id")
    @JsonProperty("second_account_id")
    private Long secondAccountId;
    /**
     * 第二个分录
     */
    @DEField(name = "second_journal_id")
    @TableField(value = "second_journal_id")
    @JSONField(name = "second_journal_id")
    @JsonProperty("second_journal_id")
    private Long secondJournalId;
    /**
     * 分析账户
     */
    @DEField(name = "analytic_account_id")
    @TableField(value = "analytic_account_id")
    @JSONField(name = "analytic_account_id")
    @JsonProperty("analytic_account_id")
    private Long analyticAccountId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 第二个税
     */
    @DEField(name = "second_tax_id")
    @TableField(value = "second_tax_id")
    @JSONField(name = "second_tax_id")
    @JsonProperty("second_tax_id")
    private Long secondTaxId;
    /**
     * 科目
     */
    @DEField(name = "account_id")
    @TableField(value = "account_id")
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Long accountId;
    /**
     * 日记账
     */
    @DEField(name = "journal_id")
    @TableField(value = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Long journalId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooSecondAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAnalyticAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooSecondAnalyticAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooSecondJournal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax odooSecondTax;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax odooTax;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [日记账项目标签]
     */
    public void setLabel(String label){
        this.label = label ;
        this.modify("label",label);
    }

    /**
     * 设置 [含税价]
     */
    public void setForceTaxIncluded(Boolean forceTaxIncluded){
        this.forceTaxIncluded = forceTaxIncluded ;
        this.modify("force_tax_included",forceTaxIncluded);
    }

    /**
     * 设置 [金额类型]
     */
    public void setAmountType(String amountType){
        this.amountType = amountType ;
        this.modify("amount_type",amountType);
    }

    /**
     * 设置 [第二含税价‎]
     */
    public void setForceSecondTaxIncluded(Boolean forceSecondTaxIncluded){
        this.forceSecondTaxIncluded = forceSecondTaxIncluded ;
        this.modify("force_second_tax_included",forceSecondTaxIncluded);
    }

    /**
     * 设置 [自动验证]
     */
    public void setAutoReconcile(Boolean autoReconcile){
        this.autoReconcile = autoReconcile ;
        this.modify("auto_reconcile",autoReconcile);
    }

    /**
     * 设置 [会计匹配]
     */
    public void setMatchTotalAmount(Boolean matchTotalAmount){
        this.matchTotalAmount = matchTotalAmount ;
        this.modify("match_total_amount",matchTotalAmount);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [添加第二行]
     */
    public void setHasSecondLine(Boolean hasSecondLine){
        this.hasSecondLine = hasSecondLine ;
        this.modify("has_second_line",hasSecondLine);
    }

    /**
     * 设置 [数量性质]
     */
    public void setMatchNature(String matchNature){
        this.matchNature = matchNature ;
        this.modify("match_nature",matchNature);
    }

    /**
     * 设置 [第二金额类型]
     */
    public void setSecondAmountType(String secondAmountType){
        this.secondAmountType = secondAmountType ;
        this.modify("second_amount_type",secondAmountType);
    }

    /**
     * 设置 [标签]
     */
    public void setMatchLabel(String matchLabel){
        this.matchLabel = matchLabel ;
        this.modify("match_label",matchLabel);
    }

    /**
     * 设置 [参数最大金额]
     */
    public void setMatchAmountMax(Double matchAmountMax){
        this.matchAmountMax = matchAmountMax ;
        this.modify("match_amount_max",matchAmountMax);
    }

    /**
     * 设置 [会计匹配%]
     */
    public void setMatchTotalAmountParam(Double matchTotalAmountParam){
        this.matchTotalAmountParam = matchTotalAmountParam ;
        this.modify("match_total_amount_param",matchTotalAmountParam);
    }

    /**
     * 设置 [第二核销金额]
     */
    public void setSecondAmount(Double secondAmount){
        this.secondAmount = secondAmount ;
        this.modify("second_amount",secondAmount);
    }

    /**
     * 设置 [金额]
     */
    public void setMatchAmount(String matchAmount){
        this.matchAmount = matchAmount ;
        this.modify("match_amount",matchAmount);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [类型]
     */
    public void setRuleType(String ruleType){
        this.ruleType = ruleType ;
        this.modify("rule_type",ruleType);
    }

    /**
     * 设置 [第二个分录项目标签]
     */
    public void setSecondLabel(String secondLabel){
        this.secondLabel = secondLabel ;
        this.modify("second_label",secondLabel);
    }

    /**
     * 设置 [已经匹配合作伙伴]
     */
    public void setMatchPartner(Boolean matchPartner){
        this.matchPartner = matchPartner ;
        this.modify("match_partner",matchPartner);
    }

    /**
     * 设置 [参数最小金额]
     */
    public void setMatchAmountMin(Double matchAmountMin){
        this.matchAmountMin = matchAmountMin ;
        this.modify("match_amount_min",matchAmountMin);
    }

    /**
     * 设置 [同币种匹配]
     */
    public void setMatchSameCurrency(Boolean matchSameCurrency){
        this.matchSameCurrency = matchSameCurrency ;
        this.modify("match_same_currency",matchSameCurrency);
    }

    /**
     * 设置 [核销金额]
     */
    public void setAmount(Double amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [标签参数]
     */
    public void setMatchLabelParam(String matchLabelParam){
        this.matchLabelParam = matchLabelParam ;
        this.modify("match_label_param",matchLabelParam);
    }

    /**
     * 设置 [第二分析帐户]
     */
    public void setSecondAnalyticAccountId(Long secondAnalyticAccountId){
        this.secondAnalyticAccountId = secondAnalyticAccountId ;
        this.modify("second_analytic_account_id",secondAnalyticAccountId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [税率]
     */
    public void setTaxId(Long taxId){
        this.taxId = taxId ;
        this.modify("tax_id",taxId);
    }

    /**
     * 设置 [第二科目]
     */
    public void setSecondAccountId(Long secondAccountId){
        this.secondAccountId = secondAccountId ;
        this.modify("second_account_id",secondAccountId);
    }

    /**
     * 设置 [第二个分录]
     */
    public void setSecondJournalId(Long secondJournalId){
        this.secondJournalId = secondJournalId ;
        this.modify("second_journal_id",secondJournalId);
    }

    /**
     * 设置 [分析账户]
     */
    public void setAnalyticAccountId(Long analyticAccountId){
        this.analyticAccountId = analyticAccountId ;
        this.modify("analytic_account_id",analyticAccountId);
    }

    /**
     * 设置 [第二个税]
     */
    public void setSecondTaxId(Long secondTaxId){
        this.secondTaxId = secondTaxId ;
        this.modify("second_tax_id",secondTaxId);
    }

    /**
     * 设置 [科目]
     */
    public void setAccountId(Long accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [日记账]
     */
    public void setJournalId(Long journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


