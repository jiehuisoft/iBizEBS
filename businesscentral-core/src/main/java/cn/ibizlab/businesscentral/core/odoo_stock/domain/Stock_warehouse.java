package cn.ibizlab.businesscentral.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[仓库]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "STOCK_WAREHOUSE",resultMap = "Stock_warehouseResultMap")
public class Stock_warehouse extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 补充路线
     */
    @TableField(exist = false)
    @JSONField(name = "resupply_route_ids")
    @JsonProperty("resupply_route_ids")
    private String resupplyRouteIds;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 补给 自
     */
    @TableField(exist = false)
    @JSONField(name = "resupply_wh_ids")
    @JsonProperty("resupply_wh_ids")
    private String resupplyWhIds;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 仓库
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 出向运输
     */
    @DEField(name = "delivery_steps")
    @TableField(value = "delivery_steps")
    @JSONField(name = "delivery_steps")
    @JsonProperty("delivery_steps")
    private String deliverySteps;
    /**
     * 仓库个数
     */
    @TableField(exist = false)
    @JSONField(name = "warehouse_count")
    @JsonProperty("warehouse_count")
    private Integer warehouseCount;
    /**
     * 制造补给
     */
    @DEField(name = "manufacture_to_resupply")
    @TableField(value = "manufacture_to_resupply")
    @JSONField(name = "manufacture_to_resupply")
    @JsonProperty("manufacture_to_resupply")
    private Boolean manufactureToResupply;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 路线
     */
    @TableField(exist = false)
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    private String routeIds;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 入库
     */
    @DEField(name = "reception_steps")
    @TableField(value = "reception_steps")
    @JSONField(name = "reception_steps")
    @JsonProperty("reception_steps")
    private String receptionSteps;
    /**
     * 购买补给
     */
    @DEField(name = "buy_to_resupply")
    @TableField(value = "buy_to_resupply")
    @JSONField(name = "buy_to_resupply")
    @JsonProperty("buy_to_resupply")
    private Boolean buyToResupply;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 缩写
     */
    @TableField(value = "code")
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;
    /**
     * 制造
     */
    @DEField(name = "manufacture_steps")
    @TableField(value = "manufacture_steps")
    @JSONField(name = "manufacture_steps")
    @JsonProperty("manufacture_steps")
    private String manufactureSteps;
    /**
     * 显示补给
     */
    @TableField(exist = false)
    @JSONField(name = "show_resupply")
    @JsonProperty("show_resupply")
    private Boolean showResupply;
    /**
     * 视图位置
     */
    @TableField(exist = false)
    @JSONField(name = "view_location_id_text")
    @JsonProperty("view_location_id_text")
    private String viewLocationIdText;
    /**
     * 进货位置
     */
    @TableField(exist = false)
    @JSONField(name = "wh_input_stock_loc_id_text")
    @JsonProperty("wh_input_stock_loc_id_text")
    private String whInputStockLocIdText;
    /**
     * 制造地点后的库存
     */
    @TableField(exist = false)
    @JSONField(name = "sam_loc_id_text")
    @JsonProperty("sam_loc_id_text")
    private String samLocIdText;
    /**
     * 越库路线
     */
    @TableField(exist = false)
    @JSONField(name = "crossdock_route_id_text")
    @JsonProperty("crossdock_route_id_text")
    private String crossdockRouteIdText;
    /**
     * 在制造路线前拣货
     */
    @TableField(exist = false)
    @JSONField(name = "pbm_route_id_text")
    @JsonProperty("pbm_route_id_text")
    private String pbmRouteIdText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 在制造作业类型前拣货
     */
    @TableField(exist = false)
    @JSONField(name = "pbm_type_id_text")
    @JsonProperty("pbm_type_id_text")
    private String pbmTypeIdText;
    /**
     * 分拣类型
     */
    @TableField(exist = false)
    @JSONField(name = "pick_type_id_text")
    @JsonProperty("pick_type_id_text")
    private String pickTypeIdText;
    /**
     * 内部类型
     */
    @TableField(exist = false)
    @JSONField(name = "int_type_id_text")
    @JsonProperty("int_type_id_text")
    private String intTypeIdText;
    /**
     * 生产操作类型
     */
    @TableField(exist = false)
    @JSONField(name = "manu_type_id_text")
    @JsonProperty("manu_type_id_text")
    private String manuTypeIdText;
    /**
     * 购买规则
     */
    @TableField(exist = false)
    @JSONField(name = "buy_pull_id_text")
    @JsonProperty("buy_pull_id_text")
    private String buyPullIdText;
    /**
     * 打包位置
     */
    @TableField(exist = false)
    @JSONField(name = "wh_pack_stock_loc_id_text")
    @JsonProperty("wh_pack_stock_loc_id_text")
    private String whPackStockLocIdText;
    /**
     * 制造规则
     */
    @TableField(exist = false)
    @JSONField(name = "manufacture_pull_id_text")
    @JsonProperty("manufacture_pull_id_text")
    private String manufacturePullIdText;
    /**
     * 收货路线
     */
    @TableField(exist = false)
    @JSONField(name = "reception_route_id_text")
    @JsonProperty("reception_route_id_text")
    private String receptionRouteIdText;
    /**
     * 交货路线
     */
    @TableField(exist = false)
    @JSONField(name = "delivery_route_id_text")
    @JsonProperty("delivery_route_id_text")
    private String deliveryRouteIdText;
    /**
     * 制造运营类型后的库存
     */
    @TableField(exist = false)
    @JSONField(name = "sam_type_id_text")
    @JsonProperty("sam_type_id_text")
    private String samTypeIdText;
    /**
     * 地址
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 出库类型
     */
    @TableField(exist = false)
    @JSONField(name = "out_type_id_text")
    @JsonProperty("out_type_id_text")
    private String outTypeIdText;
    /**
     * MTO规则
     */
    @TableField(exist = false)
    @JSONField(name = "mto_pull_id_text")
    @JsonProperty("mto_pull_id_text")
    private String mtoPullIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 库存位置
     */
    @TableField(exist = false)
    @JSONField(name = "lot_stock_id_text")
    @JsonProperty("lot_stock_id_text")
    private String lotStockIdText;
    /**
     * 在制造（按订单补货）MTO规则之前拣货
     */
    @TableField(exist = false)
    @JSONField(name = "pbm_mto_pull_id_text")
    @JsonProperty("pbm_mto_pull_id_text")
    private String pbmMtoPullIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 在制造位置前拣货
     */
    @TableField(exist = false)
    @JSONField(name = "pbm_loc_id_text")
    @JsonProperty("pbm_loc_id_text")
    private String pbmLocIdText;
    /**
     * 包裹类型
     */
    @TableField(exist = false)
    @JSONField(name = "pack_type_id_text")
    @JsonProperty("pack_type_id_text")
    private String packTypeIdText;
    /**
     * 制造规则后的库存
     */
    @TableField(exist = false)
    @JSONField(name = "sam_rule_id_text")
    @JsonProperty("sam_rule_id_text")
    private String samRuleIdText;
    /**
     * 入库类型
     */
    @TableField(exist = false)
    @JSONField(name = "in_type_id_text")
    @JsonProperty("in_type_id_text")
    private String inTypeIdText;
    /**
     * 出货位置
     */
    @TableField(exist = false)
    @JSONField(name = "wh_output_stock_loc_id_text")
    @JsonProperty("wh_output_stock_loc_id_text")
    private String whOutputStockLocIdText;
    /**
     * 质量管理位置
     */
    @TableField(exist = false)
    @JSONField(name = "wh_qc_stock_loc_id_text")
    @JsonProperty("wh_qc_stock_loc_id_text")
    private String whQcStockLocIdText;
    /**
     * 出货位置
     */
    @DEField(name = "wh_output_stock_loc_id")
    @TableField(value = "wh_output_stock_loc_id")
    @JSONField(name = "wh_output_stock_loc_id")
    @JsonProperty("wh_output_stock_loc_id")
    private Long whOutputStockLocId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 在制造位置前拣货
     */
    @DEField(name = "pbm_loc_id")
    @TableField(value = "pbm_loc_id")
    @JSONField(name = "pbm_loc_id")
    @JsonProperty("pbm_loc_id")
    private Long pbmLocId;
    /**
     * 视图位置
     */
    @DEField(name = "view_location_id")
    @TableField(value = "view_location_id")
    @JSONField(name = "view_location_id")
    @JsonProperty("view_location_id")
    private Long viewLocationId;
    /**
     * 出库类型
     */
    @DEField(name = "out_type_id")
    @TableField(value = "out_type_id")
    @JSONField(name = "out_type_id")
    @JsonProperty("out_type_id")
    private Long outTypeId;
    /**
     * 生产操作类型
     */
    @DEField(name = "manu_type_id")
    @TableField(value = "manu_type_id")
    @JSONField(name = "manu_type_id")
    @JsonProperty("manu_type_id")
    private Long manuTypeId;
    /**
     * 在制造（按订单补货）MTO规则之前拣货
     */
    @DEField(name = "pbm_mto_pull_id")
    @TableField(value = "pbm_mto_pull_id")
    @JSONField(name = "pbm_mto_pull_id")
    @JsonProperty("pbm_mto_pull_id")
    private Long pbmMtoPullId;
    /**
     * MTO规则
     */
    @DEField(name = "mto_pull_id")
    @TableField(value = "mto_pull_id")
    @JSONField(name = "mto_pull_id")
    @JsonProperty("mto_pull_id")
    private Long mtoPullId;
    /**
     * 制造运营类型后的库存
     */
    @DEField(name = "sam_type_id")
    @TableField(value = "sam_type_id")
    @JSONField(name = "sam_type_id")
    @JsonProperty("sam_type_id")
    private Long samTypeId;
    /**
     * 制造规则
     */
    @DEField(name = "manufacture_pull_id")
    @TableField(value = "manufacture_pull_id")
    @JSONField(name = "manufacture_pull_id")
    @JsonProperty("manufacture_pull_id")
    private Long manufacturePullId;
    /**
     * 制造地点后的库存
     */
    @DEField(name = "sam_loc_id")
    @TableField(value = "sam_loc_id")
    @JSONField(name = "sam_loc_id")
    @JsonProperty("sam_loc_id")
    private Long samLocId;
    /**
     * 购买规则
     */
    @DEField(name = "buy_pull_id")
    @TableField(value = "buy_pull_id")
    @JSONField(name = "buy_pull_id")
    @JsonProperty("buy_pull_id")
    private Long buyPullId;
    /**
     * 内部类型
     */
    @DEField(name = "int_type_id")
    @TableField(value = "int_type_id")
    @JSONField(name = "int_type_id")
    @JsonProperty("int_type_id")
    private Long intTypeId;
    /**
     * 质量管理位置
     */
    @DEField(name = "wh_qc_stock_loc_id")
    @TableField(value = "wh_qc_stock_loc_id")
    @JSONField(name = "wh_qc_stock_loc_id")
    @JsonProperty("wh_qc_stock_loc_id")
    private Long whQcStockLocId;
    /**
     * 入库类型
     */
    @DEField(name = "in_type_id")
    @TableField(value = "in_type_id")
    @JSONField(name = "in_type_id")
    @JsonProperty("in_type_id")
    private Long inTypeId;
    /**
     * 在制造路线前拣货
     */
    @DEField(name = "pbm_route_id")
    @TableField(value = "pbm_route_id")
    @JSONField(name = "pbm_route_id")
    @JsonProperty("pbm_route_id")
    private Long pbmRouteId;
    /**
     * 分拣类型
     */
    @DEField(name = "pick_type_id")
    @TableField(value = "pick_type_id")
    @JSONField(name = "pick_type_id")
    @JsonProperty("pick_type_id")
    private Long pickTypeId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 地址
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 公司
     */
    @DEField(name = "company_id" , preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 收货路线
     */
    @DEField(name = "reception_route_id")
    @TableField(value = "reception_route_id")
    @JSONField(name = "reception_route_id")
    @JsonProperty("reception_route_id")
    private Long receptionRouteId;
    /**
     * 包裹类型
     */
    @DEField(name = "pack_type_id")
    @TableField(value = "pack_type_id")
    @JSONField(name = "pack_type_id")
    @JsonProperty("pack_type_id")
    private Long packTypeId;
    /**
     * 在制造作业类型前拣货
     */
    @DEField(name = "pbm_type_id")
    @TableField(value = "pbm_type_id")
    @JSONField(name = "pbm_type_id")
    @JsonProperty("pbm_type_id")
    private Long pbmTypeId;
    /**
     * 交货路线
     */
    @DEField(name = "delivery_route_id")
    @TableField(value = "delivery_route_id")
    @JSONField(name = "delivery_route_id")
    @JsonProperty("delivery_route_id")
    private Long deliveryRouteId;
    /**
     * 进货位置
     */
    @DEField(name = "wh_input_stock_loc_id")
    @TableField(value = "wh_input_stock_loc_id")
    @JSONField(name = "wh_input_stock_loc_id")
    @JsonProperty("wh_input_stock_loc_id")
    private Long whInputStockLocId;
    /**
     * 库存位置
     */
    @DEField(name = "lot_stock_id")
    @TableField(value = "lot_stock_id")
    @JSONField(name = "lot_stock_id")
    @JsonProperty("lot_stock_id")
    private Long lotStockId;
    /**
     * 制造规则后的库存
     */
    @DEField(name = "sam_rule_id")
    @TableField(value = "sam_rule_id")
    @JSONField(name = "sam_rule_id")
    @JsonProperty("sam_rule_id")
    private Long samRuleId;
    /**
     * 越库路线
     */
    @DEField(name = "crossdock_route_id")
    @TableField(value = "crossdock_route_id")
    @JSONField(name = "crossdock_route_id")
    @JsonProperty("crossdock_route_id")
    private Long crossdockRouteId;
    /**
     * 打包位置
     */
    @DEField(name = "wh_pack_stock_loc_id")
    @TableField(value = "wh_pack_stock_loc_id")
    @JSONField(name = "wh_pack_stock_loc_id")
    @JsonProperty("wh_pack_stock_loc_id")
    private Long whPackStockLocId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route odooCrossdockRoute;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route odooDeliveryRoute;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route odooPbmRoute;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route odooReceptionRoute;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLotStock;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooPbmLoc;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooSamLoc;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooViewLocation;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooWhInputStockLoc;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooWhOutputStockLoc;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooWhPackStockLoc;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooWhQcStockLoc;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooIntType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooInType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooManuType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooOutType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooPackType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooPbmType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooPickType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooSamType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule odooBuyPull;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule odooManufacturePull;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule odooMtoPull;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule odooPbmMtoPull;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule odooSamRule;



    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [仓库]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [出向运输]
     */
    public void setDeliverySteps(String deliverySteps){
        this.deliverySteps = deliverySteps ;
        this.modify("delivery_steps",deliverySteps);
    }

    /**
     * 设置 [制造补给]
     */
    public void setManufactureToResupply(Boolean manufactureToResupply){
        this.manufactureToResupply = manufactureToResupply ;
        this.modify("manufacture_to_resupply",manufactureToResupply);
    }

    /**
     * 设置 [入库]
     */
    public void setReceptionSteps(String receptionSteps){
        this.receptionSteps = receptionSteps ;
        this.modify("reception_steps",receptionSteps);
    }

    /**
     * 设置 [购买补给]
     */
    public void setBuyToResupply(Boolean buyToResupply){
        this.buyToResupply = buyToResupply ;
        this.modify("buy_to_resupply",buyToResupply);
    }

    /**
     * 设置 [缩写]
     */
    public void setCode(String code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [制造]
     */
    public void setManufactureSteps(String manufactureSteps){
        this.manufactureSteps = manufactureSteps ;
        this.modify("manufacture_steps",manufactureSteps);
    }

    /**
     * 设置 [出货位置]
     */
    public void setWhOutputStockLocId(Long whOutputStockLocId){
        this.whOutputStockLocId = whOutputStockLocId ;
        this.modify("wh_output_stock_loc_id",whOutputStockLocId);
    }

    /**
     * 设置 [在制造位置前拣货]
     */
    public void setPbmLocId(Long pbmLocId){
        this.pbmLocId = pbmLocId ;
        this.modify("pbm_loc_id",pbmLocId);
    }

    /**
     * 设置 [视图位置]
     */
    public void setViewLocationId(Long viewLocationId){
        this.viewLocationId = viewLocationId ;
        this.modify("view_location_id",viewLocationId);
    }

    /**
     * 设置 [出库类型]
     */
    public void setOutTypeId(Long outTypeId){
        this.outTypeId = outTypeId ;
        this.modify("out_type_id",outTypeId);
    }

    /**
     * 设置 [生产操作类型]
     */
    public void setManuTypeId(Long manuTypeId){
        this.manuTypeId = manuTypeId ;
        this.modify("manu_type_id",manuTypeId);
    }

    /**
     * 设置 [在制造（按订单补货）MTO规则之前拣货]
     */
    public void setPbmMtoPullId(Long pbmMtoPullId){
        this.pbmMtoPullId = pbmMtoPullId ;
        this.modify("pbm_mto_pull_id",pbmMtoPullId);
    }

    /**
     * 设置 [MTO规则]
     */
    public void setMtoPullId(Long mtoPullId){
        this.mtoPullId = mtoPullId ;
        this.modify("mto_pull_id",mtoPullId);
    }

    /**
     * 设置 [制造运营类型后的库存]
     */
    public void setSamTypeId(Long samTypeId){
        this.samTypeId = samTypeId ;
        this.modify("sam_type_id",samTypeId);
    }

    /**
     * 设置 [制造规则]
     */
    public void setManufacturePullId(Long manufacturePullId){
        this.manufacturePullId = manufacturePullId ;
        this.modify("manufacture_pull_id",manufacturePullId);
    }

    /**
     * 设置 [制造地点后的库存]
     */
    public void setSamLocId(Long samLocId){
        this.samLocId = samLocId ;
        this.modify("sam_loc_id",samLocId);
    }

    /**
     * 设置 [购买规则]
     */
    public void setBuyPullId(Long buyPullId){
        this.buyPullId = buyPullId ;
        this.modify("buy_pull_id",buyPullId);
    }

    /**
     * 设置 [内部类型]
     */
    public void setIntTypeId(Long intTypeId){
        this.intTypeId = intTypeId ;
        this.modify("int_type_id",intTypeId);
    }

    /**
     * 设置 [质量管理位置]
     */
    public void setWhQcStockLocId(Long whQcStockLocId){
        this.whQcStockLocId = whQcStockLocId ;
        this.modify("wh_qc_stock_loc_id",whQcStockLocId);
    }

    /**
     * 设置 [入库类型]
     */
    public void setInTypeId(Long inTypeId){
        this.inTypeId = inTypeId ;
        this.modify("in_type_id",inTypeId);
    }

    /**
     * 设置 [在制造路线前拣货]
     */
    public void setPbmRouteId(Long pbmRouteId){
        this.pbmRouteId = pbmRouteId ;
        this.modify("pbm_route_id",pbmRouteId);
    }

    /**
     * 设置 [分拣类型]
     */
    public void setPickTypeId(Long pickTypeId){
        this.pickTypeId = pickTypeId ;
        this.modify("pick_type_id",pickTypeId);
    }

    /**
     * 设置 [地址]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [收货路线]
     */
    public void setReceptionRouteId(Long receptionRouteId){
        this.receptionRouteId = receptionRouteId ;
        this.modify("reception_route_id",receptionRouteId);
    }

    /**
     * 设置 [包裹类型]
     */
    public void setPackTypeId(Long packTypeId){
        this.packTypeId = packTypeId ;
        this.modify("pack_type_id",packTypeId);
    }

    /**
     * 设置 [在制造作业类型前拣货]
     */
    public void setPbmTypeId(Long pbmTypeId){
        this.pbmTypeId = pbmTypeId ;
        this.modify("pbm_type_id",pbmTypeId);
    }

    /**
     * 设置 [交货路线]
     */
    public void setDeliveryRouteId(Long deliveryRouteId){
        this.deliveryRouteId = deliveryRouteId ;
        this.modify("delivery_route_id",deliveryRouteId);
    }

    /**
     * 设置 [进货位置]
     */
    public void setWhInputStockLocId(Long whInputStockLocId){
        this.whInputStockLocId = whInputStockLocId ;
        this.modify("wh_input_stock_loc_id",whInputStockLocId);
    }

    /**
     * 设置 [库存位置]
     */
    public void setLotStockId(Long lotStockId){
        this.lotStockId = lotStockId ;
        this.modify("lot_stock_id",lotStockId);
    }

    /**
     * 设置 [制造规则后的库存]
     */
    public void setSamRuleId(Long samRuleId){
        this.samRuleId = samRuleId ;
        this.modify("sam_rule_id",samRuleId);
    }

    /**
     * 设置 [越库路线]
     */
    public void setCrossdockRouteId(Long crossdockRouteId){
        this.crossdockRouteId = crossdockRouteId ;
        this.modify("crossdock_route_id",crossdockRouteId);
    }

    /**
     * 设置 [打包位置]
     */
    public void setWhPackStockLocId(Long whPackStockLocId){
        this.whPackStockLocId = whPackStockLocId ;
        this.modify("wh_pack_stock_loc_id",whPackStockLocId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}


