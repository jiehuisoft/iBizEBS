package cn.ibizlab.businesscentral.core.odoo_board.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_board.domain.Board_board;
import cn.ibizlab.businesscentral.core.odoo_board.filter.Board_boardSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[board_board] 服务对象接口
 */
@Component
public class board_boardFallback implements board_boardFeignClient{


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Board_board create(Board_board board_board){
            return null;
     }
    public Boolean createBatch(List<Board_board> board_boards){
            return false;
     }

    public Board_board update(Long id, Board_board board_board){
            return null;
     }
    public Boolean updateBatch(List<Board_board> board_boards){
            return false;
     }


    public Page<Board_board> search(Board_boardSearchContext context){
            return null;
     }



    public Board_board get(Long id){
            return null;
     }



    public Page<Board_board> select(){
            return null;
     }

    public Board_board getDraft(){
            return null;
    }



    public Boolean checkKey(Board_board board_board){
            return false;
     }


    public Boolean save(Board_board board_board){
            return false;
     }
    public Boolean saveBatch(List<Board_board> board_boards){
            return false;
     }

    public Page<Board_board> searchDefault(Board_boardSearchContext context){
            return null;
     }


}
