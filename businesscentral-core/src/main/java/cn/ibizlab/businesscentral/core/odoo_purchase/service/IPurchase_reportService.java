package cn.ibizlab.businesscentral.core.odoo_purchase.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_report;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_reportSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Purchase_report] 服务对象接口
 */
public interface IPurchase_reportService extends IService<Purchase_report>{

    boolean create(Purchase_report et) ;
    void createBatch(List<Purchase_report> list) ;
    boolean update(Purchase_report et) ;
    void updateBatch(List<Purchase_report> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Purchase_report get(Long key) ;
    Purchase_report getDraft(Purchase_report et) ;
    boolean checkKey(Purchase_report et) ;
    boolean save(Purchase_report et) ;
    void saveBatch(List<Purchase_report> list) ;
    Page<Purchase_report> searchDefault(Purchase_reportSearchContext context) ;
    List<Purchase_report> selectByAccountAnalyticId(Long id);
    void resetByAccountAnalyticId(Long id);
    void resetByAccountAnalyticId(Collection<Long> ids);
    void removeByAccountAnalyticId(Long id);
    List<Purchase_report> selectByFiscalPositionId(Long id);
    void resetByFiscalPositionId(Long id);
    void resetByFiscalPositionId(Collection<Long> ids);
    void removeByFiscalPositionId(Long id);
    List<Purchase_report> selectByCategoryId(Long id);
    void resetByCategoryId(Long id);
    void resetByCategoryId(Collection<Long> ids);
    void removeByCategoryId(Long id);
    List<Purchase_report> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Purchase_report> selectByProductTmplId(Long id);
    void resetByProductTmplId(Long id);
    void resetByProductTmplId(Collection<Long> ids);
    void removeByProductTmplId(Long id);
    List<Purchase_report> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Purchase_report> selectByCountryId(Long id);
    void resetByCountryId(Long id);
    void resetByCountryId(Collection<Long> ids);
    void removeByCountryId(Long id);
    List<Purchase_report> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Purchase_report> selectByCommercialPartnerId(Long id);
    void resetByCommercialPartnerId(Long id);
    void resetByCommercialPartnerId(Collection<Long> ids);
    void removeByCommercialPartnerId(Long id);
    List<Purchase_report> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Purchase_report> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Purchase_report> selectByPickingTypeId(Long id);
    void resetByPickingTypeId(Long id);
    void resetByPickingTypeId(Collection<Long> ids);
    void removeByPickingTypeId(Long id);
    List<Purchase_report> selectByProductUom(Long id);
    void resetByProductUom(Long id);
    void resetByProductUom(Collection<Long> ids);
    void removeByProductUom(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}


