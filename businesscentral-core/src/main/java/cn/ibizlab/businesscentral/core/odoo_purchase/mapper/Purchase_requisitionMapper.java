package cn.ibizlab.businesscentral.core.odoo_purchase.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_requisitionSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Purchase_requisitionMapper extends BaseMapper<Purchase_requisition>{

    Page<Purchase_requisition> searchDefault(IPage page, @Param("srf") Purchase_requisitionSearchContext context, @Param("ew") Wrapper<Purchase_requisition> wrapper) ;
    Page<Purchase_requisition> searchMaster(IPage page, @Param("srf") Purchase_requisitionSearchContext context, @Param("ew") Wrapper<Purchase_requisition> wrapper) ;
    @Override
    Purchase_requisition selectById(Serializable id);
    @Override
    int insert(Purchase_requisition entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Purchase_requisition entity);
    @Override
    int update(@Param(Constants.ENTITY) Purchase_requisition entity, @Param("ew") Wrapper<Purchase_requisition> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Purchase_requisition> selectByTypeId(@Param("id") Serializable id) ;

    List<Purchase_requisition> selectByCompanyId(@Param("id") Serializable id) ;

    List<Purchase_requisition> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Purchase_requisition> selectByVendorId(@Param("id") Serializable id) ;

    List<Purchase_requisition> selectByCreateUid(@Param("id") Serializable id) ;

    List<Purchase_requisition> selectByUserId(@Param("id") Serializable id) ;

    List<Purchase_requisition> selectByWriteUid(@Param("id") Serializable id) ;

    List<Purchase_requisition> selectByPickingTypeId(@Param("id") Serializable id) ;

    List<Purchase_requisition> selectByWarehouseId(@Param("id") Serializable id) ;


}
