/**
 * 技能等级
 *
 * @export
 * @interface Hr_skill_level
 */
export interface Hr_skill_level {

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Hr_skill_level
     */
    name?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_skill_level
     */
    create_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_skill_level
     */
    id?: any;

    /**
     * 进度
     *
     * @returns {*}
     * @memberof Hr_skill_level
     */
    level_progress?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_skill_level
     */
    write_date?: any;

    /**
     * 技能类型
     *
     * @returns {*}
     * @memberof Hr_skill_level
     */
    skill_type_name?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_skill_level
     */
    write_uid?: any;

    /**
     * 技能类型
     *
     * @returns {*}
     * @memberof Hr_skill_level
     */
    skill_type_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_skill_level
     */
    create_uid?: any;
}