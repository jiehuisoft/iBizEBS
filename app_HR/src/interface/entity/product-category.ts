/**
 * 产品种类
 *
 * @export
 * @interface Product_category
 */
export interface Product_category {

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Product_category
     */
    __last_update?: any;

    /**
     * 库存计价科目
     *
     * @returns {*}
     * @memberof Product_category
     */
    property_stock_valuation_account_id?: any;

    /**
     * 下级类别
     *
     * @returns {*}
     * @memberof Product_category
     */
    child_id?: any;

    /**
     * # 产品
     *
     * @returns {*}
     * @memberof Product_category
     */
    product_count?: any;

    /**
     * 路线
     *
     * @returns {*}
     * @memberof Product_category
     */
    route_ids?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Product_category
     */
    name?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Product_category
     */
    display_name?: any;

    /**
     * 库存计价
     *
     * @returns {*}
     * @memberof Product_category
     */
    property_valuation?: any;

    /**
     * 父级路径
     *
     * @returns {*}
     * @memberof Product_category
     */
    parent_path?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Product_category
     */
    id?: any;

    /**
     * 完整名称
     *
     * @returns {*}
     * @memberof Product_category
     */
    complete_name?: any;

    /**
     * 价格差异科目
     *
     * @returns {*}
     * @memberof Product_category
     */
    property_account_creditor_price_difference_categ?: any;

    /**
     * 库存出货科目
     *
     * @returns {*}
     * @memberof Product_category
     */
    property_stock_account_output_categ_id?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Product_category
     */
    create_date?: any;

    /**
     * 路线合计
     *
     * @returns {*}
     * @memberof Product_category
     */
    total_route_ids?: any;

    /**
     * 成本方法
     *
     * @returns {*}
     * @memberof Product_category
     */
    property_cost_method?: any;

    /**
     * 收入科目
     *
     * @returns {*}
     * @memberof Product_category
     */
    property_account_income_categ_id?: any;

    /**
     * 费用科目
     *
     * @returns {*}
     * @memberof Product_category
     */
    property_account_expense_categ_id?: any;

    /**
     * 库存日记账
     *
     * @returns {*}
     * @memberof Product_category
     */
    property_stock_journal?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Product_category
     */
    write_date?: any;

    /**
     * 库存进货科目
     *
     * @returns {*}
     * @memberof Product_category
     */
    property_stock_account_input_categ_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Product_category
     */
    write_uid_text?: any;

    /**
     * 上级类别
     *
     * @returns {*}
     * @memberof Product_category
     */
    parent_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Product_category
     */
    create_uid_text?: any;

    /**
     * 强制下架策略
     *
     * @returns {*}
     * @memberof Product_category
     */
    removal_strategy_id_text?: any;

    /**
     * 上级类别
     *
     * @returns {*}
     * @memberof Product_category
     */
    parent_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Product_category
     */
    create_uid?: any;

    /**
     * 强制下架策略
     *
     * @returns {*}
     * @memberof Product_category
     */
    removal_strategy_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Product_category
     */
    write_uid?: any;
}