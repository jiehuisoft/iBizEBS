/**
 * 销售订单行
 *
 * @export
 * @interface Sale_order_line
 */
export interface Sale_order_line {

    /**
     * 可报销
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    is_expense?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    write_date?: any;

    /**
     * 发票数量
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    qty_to_invoice?: any;

    /**
     * 分析标签
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    analytic_tag_ids?: any;

    /**
     * 警告
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    warning_stock?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    id?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    display_name?: any;

    /**
     * 不含税价
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    price_reduce_taxexcl?: any;

    /**
     * 用户输入自定义产品属性值
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    product_custom_attribute_value_ids?: any;

    /**
     * 手动发货
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    qty_delivered_manual?: any;

    /**
     * 税额总计
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    price_tax?: any;

    /**
     * 更新数量的方法
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    qty_delivered_method?: any;

    /**
     * 发票状态
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    invoice_status?: any;

    /**
     * 显示类型
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    display_type?: any;

    /**
     * 库存移动
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    move_ids?: any;

    /**
     * 未含税的发票金额
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    untaxed_amount_invoiced?: any;

    /**
     * 没有创建变量的产品属性值
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    product_no_variant_attribute_value_ids?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    __last_update?: any;

    /**
     * 是首付款吗？
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    is_downpayment?: any;

    /**
     * 分析明细行
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    analytic_line_ids?: any;

    /**
     * 减税后价格
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    price_reduce_taxinc?: any;

    /**
     * 已交货数量
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    qty_delivered?: any;

    /**
     * 折扣(%)
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    discount?: any;

    /**
     * 订购数量
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    product_uom_qty?: any;

    /**
     * 降价
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    price_reduce?: any;

    /**
     * 交货提前时间
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    customer_lead?: any;

    /**
     * 允许编辑
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    product_updatable?: any;

    /**
     * 已开发票数量
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    qty_invoiced?: any;

    /**
     * 不含税待开票金额
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    untaxed_amount_to_invoice?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    name?: any;

    /**
     * 单价
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    price_unit?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    create_date?: any;

    /**
     * 生成采购订单号
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    purchase_line_count?: any;

    /**
     * 姓名简称
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    name_short?: any;

    /**
     * 链接选项
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    option_line_ids?: any;

    /**
     * 税率
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    tax_id?: any;

    /**
     * 可选产品行
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    sale_order_option_ids?: any;

    /**
     * 小计
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    price_subtotal?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    sequence?: any;

    /**
     * 总计
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    price_total?: any;

    /**
     * 发票行
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    invoice_lines?: any;

    /**
     * 生成采购订单明细行
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    purchase_line_ids?: any;

    /**
     * 是一张活动票吗？
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    event_ok?: any;

    /**
     * 活动入场券
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    event_ticket_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    create_uid_text?: any;

    /**
     * 链接的订单明细
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    linked_line_id_text?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    order_partner_id_text?: any;

    /**
     * 产品图片
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    product_image?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    currency_id_text?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    product_id_text?: any;

    /**
     * 包裹
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    product_packaging_text?: any;

    /**
     * 订单关联
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    order_id_text?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    event_id_text?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    salesman_id_text?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    product_uom_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    write_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    company_id_text?: any;

    /**
     * 订单状态
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    state?: any;

    /**
     * 路线
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    route_id_text?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    salesman_id?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    currency_id?: any;

    /**
     * 订单关联
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    order_id?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    event_id?: any;

    /**
     * 链接的订单明细
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    linked_line_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    create_uid?: any;

    /**
     * 路线
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    route_id?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    order_partner_id?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    product_uom?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    product_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    company_id?: any;

    /**
     * 包裹
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    product_packaging?: any;

    /**
     * 活动入场券
     *
     * @returns {*}
     * @memberof Sale_order_line
     */
    event_ticket_id?: any;
}