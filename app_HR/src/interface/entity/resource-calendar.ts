/**
 * 资源工作时间
 *
 * @export
 * @interface Resource_calendar
 */
export interface Resource_calendar {

    /**
     * 时区
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    tz?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    name?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    __last_update?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    write_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    id?: any;

    /**
     * 全员休假
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    global_leave_ids?: any;

    /**
     * 工作时间
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    attendance_ids?: any;

    /**
     * 每天平均小时数
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    hours_per_day?: any;

    /**
     * 休假
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    leave_ids?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    display_name?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    create_date?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    create_uid_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    write_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    company_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    company_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    create_uid?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Resource_calendar
     */
    write_uid?: any;
}