/**
 * 合同类型
 *
 * @export
 * @interface Hr_contract_type
 */
export interface Hr_contract_type {

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Hr_contract_type
     */
    sequence?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Hr_contract_type
     */
    display_name?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Hr_contract_type
     */
    __last_update?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_contract_type
     */
    create_date?: any;

    /**
     * 合同类型
     *
     * @returns {*}
     * @memberof Hr_contract_type
     */
    name?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_contract_type
     */
    write_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_contract_type
     */
    id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_contract_type
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_contract_type
     */
    create_uid_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_contract_type
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_contract_type
     */
    create_uid?: any;
}