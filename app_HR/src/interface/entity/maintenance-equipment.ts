/**
 * 保养设备
 *
 * @export
 * @interface Maintenance_equipment
 */
export interface Maintenance_equipment {

    /**
     * 下一活动摘要
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    activity_summary?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    __last_update?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    activity_ids?: any;

    /**
     * 行动数量
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    message_needaction_counter?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    message_unread?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    id?: any;

    /**
     * 用于
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    equipment_assign_to?: any;

    /**
     * 地点
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    location?: any;

    /**
     * 保修截止日期
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    warranty_date?: any;

    /**
     * 网站消息
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    website_message_ids?: any;

    /**
     * 设备名称
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    name?: any;

    /**
     * 保养时长
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    maintenance_duration?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    display_name?: any;

    /**
     * 型号
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    model?: any;

    /**
     * 当前维护
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    maintenance_open_count?: any;

    /**
     * 是关注者
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    message_is_follower?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    activity_state?: any;

    /**
     * 下一活动截止日期
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    activity_date_deadline?: any;

    /**
     * 维修统计
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    maintenance_count?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    write_date?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    message_unread_counter?: any;

    /**
     * 笔记
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    note?: any;

    /**
     * 供应商参考
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    partner_ref?: any;

    /**
     * 保养
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    maintenance_ids?: any;

    /**
     * 错误数
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    message_has_error_counter?: any;

    /**
     * 责任用户
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    activity_user_id?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    message_main_attachment_id?: any;

    /**
     * 序列号
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    serial_no?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    message_attachment_count?: any;

    /**
     * 成本
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    cost?: any;

    /**
     * 下次预防维护日期
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    next_action_date?: any;

    /**
     * 分配日期
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    assign_date?: any;

    /**
     * 实际日期
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    effective_date?: any;

    /**
     * 需要采取行动
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    message_needaction?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    message_channel_ids?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    active?: any;

    /**
     * 消息
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    message_ids?: any;

    /**
     * 报废日期
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    scrap_date?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    create_date?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    message_follower_ids?: any;

    /**
     * 预防维护间隔天数
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    period?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    message_has_error?: any;

    /**
     * 下一活动类型
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    activity_type_id?: any;

    /**
     * 颜色索引
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    color?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    message_partner_ids?: any;

    /**
     * 分配到部门
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    department_id_text?: any;

    /**
     * 分配到员工
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    employee_id_text?: any;

    /**
     * 技术员
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    technician_user_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    company_id_text?: any;

    /**
     * 设备类别
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    category_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    create_uid_text?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    partner_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    write_uid_text?: any;

    /**
     * 所有者
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    owner_user_id_text?: any;

    /**
     * 保养团队
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    maintenance_team_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    create_uid?: any;

    /**
     * 技术员
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    technician_user_id?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    partner_id?: any;

    /**
     * 设备类别
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    category_id?: any;

    /**
     * 分配到员工
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    employee_id?: any;

    /**
     * 保养团队
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    maintenance_team_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    company_id?: any;

    /**
     * 所有者
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    owner_user_id?: any;

    /**
     * 分配到部门
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    department_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Maintenance_equipment
     */
    write_uid?: any;
}