/**
 * 关注消息类型
 *
 * @export
 * @interface Mail_followers_mail_message_subtype_rel
 */
export interface Mail_followers_mail_message_subtype_rel {

    /**
     * ID
     *
     * @returns {*}
     * @memberof Mail_followers_mail_message_subtype_rel
     */
    id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Mail_followers_mail_message_subtype_rel
     */
    mail_message_subtype_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Mail_followers_mail_message_subtype_rel
     */
    mail_followers_id?: any;
}