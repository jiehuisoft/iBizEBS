/**
 * 分析账户
 *
 * @export
 * @interface Account_analytic_account
 */
export interface Account_analytic_account {

    /**
     * 分析明细行
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    line_ids?: any;

    /**
     * 网站信息
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    website_message_ids?: any;

    /**
     * 操作编号
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    message_needaction_counter?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    message_follower_ids?: any;

    /**
     * 参考
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    code?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    display_name?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    message_main_attachment_id?: any;

    /**
     * 需要采取行动
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    message_needaction?: any;

    /**
     * 是关注者
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    message_is_follower?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    create_date?: any;

    /**
     * 贷方
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    credit?: any;

    /**
     * 余额
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    balance?: any;

    /**
     * 消息
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    message_ids?: any;

    /**
     * 需要一个动作消息的编码
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    message_has_error_counter?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    message_attachment_count?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    write_date?: any;

    /**
     * 借方
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    debit?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    id?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    __last_update?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    message_channel_ids?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    active?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    message_unread_counter?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    message_partner_ids?: any;

    /**
     * 分析账户
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    name?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    message_unread?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    message_has_error?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    create_uid_text?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    currency_id?: any;

    /**
     * 组
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    group_id_text?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    partner_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    company_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    write_uid_text?: any;

    /**
     * 组
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    group_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    create_uid?: any;

    /**
     * 客户
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    partner_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    company_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_analytic_account
     */
    write_uid?: any;
}