/**
 * 休假类型
 *
 * @export
 * @interface Hr_leave_type
 */
export interface Hr_leave_type {

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    valid?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    write_date?: any;

    /**
     * 虚拟剩余休假
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    virtual_remaining_leaves?: any;

    /**
     * 休假早已使用
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    leaves_taken?: any;

    /**
     * 分配天数
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    group_days_allocation?: any;

    /**
     * 应用双重验证
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    double_validation?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    allocation_type?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    display_name?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    sequence?: any;

    /**
     * 没付款
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    unpaid?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    id?: any;

    /**
     * 最大允许
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    max_leaves?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    validity_stop?: any;

    /**
     * 验证人
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    validation_type?: any;

    /**
     * 请假
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    time_type?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    __last_update?: any;

    /**
     * 休假
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    request_unit?: any;

    /**
     * 集团假期
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    group_days_leave?: any;

    /**
     * 报表中的颜色
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    color_name?: any;

    /**
     * 剩余休假
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    remaining_leaves?: any;

    /**
     * 休假类型
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    name?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    validity_start?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    create_date?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    active?: any;

    /**
     * 会议类型
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    categ_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    create_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    company_id_text?: any;

    /**
     * 会议类型
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    categ_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    company_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    create_uid?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_leave_type
     */
    write_uid?: any;
}