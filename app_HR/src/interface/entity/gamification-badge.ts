/**
 * 游戏化徽章
 *
 * @export
 * @interface Gamification_badge
 */
export interface Gamification_badge {

    /**
     * 允许授予
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    rule_auth?: any;

    /**
     * 限制数量
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    rule_max_number?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    description?: any;

    /**
     * 月度限额发放
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    rule_max?: any;

    /**
     * 授权用户
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    rule_auth_user_ids?: any;

    /**
     * 需要徽章
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    rule_auth_badge_ids?: any;

    /**
     * 行动数量
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    message_needaction_counter?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    message_attachment_count?: any;

    /**
     * 其他的允许发送
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    remaining_sending?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    message_follower_ids?: any;

    /**
     * 消息
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    message_ids?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    message_main_attachment_id?: any;

    /**
     * 我的总计
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    stat_my?: any;

    /**
     * 图像
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    image?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    display_name?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    message_partner_ids?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    message_is_follower?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    message_channel_ids?: any;

    /**
     * 唯一的所有者
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    unique_owner_ids?: any;

    /**
     * 网站消息
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    website_message_ids?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    __last_update?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    message_unread?: any;

    /**
     * 错误数
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    message_has_error_counter?: any;

    /**
     * 论坛徽章等级
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    level?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    active?: any;

    /**
     * 所有者
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    owner_ids?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    message_has_error?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    message_unread_counter?: any;

    /**
     * 奖励按照
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    goal_definition_ids?: any;

    /**
     * 月度发放总数
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    stat_my_monthly_sending?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    id?: any;

    /**
     * 总计
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    stat_count?: any;

    /**
     * 徽章
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    name?: any;

    /**
     * 每月总数
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    stat_this_month?: any;

    /**
     * 需要激活
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    message_needaction?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    write_date?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    create_date?: any;

    /**
     * 用户数量
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    stat_count_distinct?: any;

    /**
     * 我的月份总计
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    stat_my_this_month?: any;

    /**
     * 挑战的奖励
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    challenge_ids?: any;

    /**
     * 授予的员工人数
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    granted_employees_count?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    create_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    create_uid?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Gamification_badge
     */
    write_uid?: any;
}