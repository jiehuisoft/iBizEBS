/**
 * 合同
 *
 * @export
 * @interface Hr_contract
 */
export interface Hr_contract {

    /**
     * 下一活动截止日期
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    activity_date_deadline?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    create_date?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    date_end?: any;

    /**
     * 补贴
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    advantages?: any;

    /**
     * 行动数量
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    message_needaction_counter?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    notes?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    message_follower_ids?: any;

    /**
     * 下一活动摘要
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    activity_summary?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    message_channel_ids?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    message_unread?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    __last_update?: any;

    /**
     * 合同参考
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    name?: any;

    /**
     * 工资
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    wage?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    date_start?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    active?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    activity_state?: any;

    /**
     * 信息
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    message_ids?: any;

    /**
     * 网站消息
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    website_message_ids?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    message_main_attachment_id?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    message_attachment_count?: any;

    /**
     * 试用期结束
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    trial_date_end?: any;

    /**
     * 是关注者
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    message_is_follower?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    activity_ids?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    message_partner_ids?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    message_has_error?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    message_unread_counter?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    write_date?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    state?: any;

    /**
     * 责任用户
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    activity_user_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    id?: any;

    /**
     * 需要采取行动
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    message_needaction?: any;

    /**
     * 错误数
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    message_has_error_counter?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    display_name?: any;

    /**
     * 下一活动类型
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    activity_type_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    create_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    company_id_text?: any;

    /**
     * 工作许可编号
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    permit_no?: any;

    /**
     * 工作安排
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    resource_calendar_id_text?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    department_id_text?: any;

    /**
     * 工作岗位
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    job_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    write_uid_text?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    currency_id?: any;

    /**
     * 员工
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    employee_id_text?: any;

    /**
     * 人力资源主管
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    hr_responsible_name?: any;

    /**
     * 签证到期日期
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    visa_expire?: any;

    /**
     * 签证号
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    visa_no?: any;

    /**
     * 工作安排
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    resource_calendar_id?: any;

    /**
     * 人力资源主管
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    hr_responsible_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    create_uid?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    write_uid?: any;

    /**
     * 员工
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    employee_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    company_id?: any;

    /**
     * 工作岗位
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    job_id?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof Hr_contract
     */
    department_id?: any;
}