import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_skillServiceBase from './hr-skill-service-base';


/**
 * 技能服务对象
 *
 * @export
 * @class Hr_skillService
 * @extends {Hr_skillServiceBase}
 */
export default class Hr_skillService extends Hr_skillServiceBase {

    /**
     * Creates an instance of  Hr_skillService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_skillService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}