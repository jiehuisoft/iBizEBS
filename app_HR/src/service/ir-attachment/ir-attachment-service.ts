import { Http } from '@/utils';
import { Util } from '@/utils';
import Ir_attachmentServiceBase from './ir-attachment-service-base';


/**
 * 附件服务对象
 *
 * @export
 * @class Ir_attachmentService
 * @extends {Ir_attachmentServiceBase}
 */
export default class Ir_attachmentService extends Ir_attachmentServiceBase {

    /**
     * Creates an instance of  Ir_attachmentService.
     * 
     * @param {*} [opts={}]
     * @memberof  Ir_attachmentService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}