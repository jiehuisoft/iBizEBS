import { Http } from '@/utils';
import { Util } from '@/utils';
import Account_accountServiceBase from './account-account-service-base';


/**
 * 科目服务对象
 *
 * @export
 * @class Account_accountService
 * @extends {Account_accountServiceBase}
 */
export default class Account_accountService extends Account_accountServiceBase {

    /**
     * Creates an instance of  Account_accountService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_accountService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}