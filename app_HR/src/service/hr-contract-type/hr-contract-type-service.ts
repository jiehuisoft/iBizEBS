import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_contract_typeServiceBase from './hr-contract-type-service-base';


/**
 * 合同类型服务对象
 *
 * @export
 * @class Hr_contract_typeService
 * @extends {Hr_contract_typeServiceBase}
 */
export default class Hr_contract_typeService extends Hr_contract_typeServiceBase {

    /**
     * Creates an instance of  Hr_contract_typeService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_contract_typeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}