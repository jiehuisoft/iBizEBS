import { Http } from '@/utils';
import { Util } from '@/utils';
import Res_currencyServiceBase from './res-currency-service-base';


/**
 * 币种服务对象
 *
 * @export
 * @class Res_currencyService
 * @extends {Res_currencyServiceBase}
 */
export default class Res_currencyService extends Res_currencyServiceBase {

    /**
     * Creates an instance of  Res_currencyService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_currencyService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}