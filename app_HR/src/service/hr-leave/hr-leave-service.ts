import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_leaveServiceBase from './hr-leave-service-base';


/**
 * 休假服务对象
 *
 * @export
 * @class Hr_leaveService
 * @extends {Hr_leaveServiceBase}
 */
export default class Hr_leaveService extends Hr_leaveServiceBase {

    /**
     * Creates an instance of  Hr_leaveService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_leaveService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}