import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 休假服务对象基类
 *
 * @export
 * @class Hr_leaveServiceBase
 * @extends {EntityServie}
 */
export default class Hr_leaveServiceBase extends EntityService {

    /**
     * Creates an instance of  Hr_leaveServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_leaveServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Hr_leaveServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='hr_leave';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'hr_leaves';
        this.APPDETEXT = 'name';
        this.APPNAME = 'hr';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_leaveServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/hr_leaves`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/hr_leaves`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_leaveServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_leave){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/hr_employees/${context.hr_employee}/hr_leaves/${context.hr_leave}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/hr_leaves/${context.hr_leave}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_leaveServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_leave){
            let res:any = Http.getInstance().delete(`/hr_employees/${context.hr_employee}/hr_leaves/${context.hr_leave}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/hr_leaves/${context.hr_leave}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_leaveServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_leave){
            let res:any = await Http.getInstance().get(`/hr_employees/${context.hr_employee}/hr_leaves/${context.hr_leave}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/hr_leaves/${context.hr_leave}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_leaveServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && true){
            let res:any = await Http.getInstance().get(`/hr_employees/${context.hr_employee}/hr_leaves/getdraft`,isloading);
            res.data.hr_leave = data.hr_leave;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/hr_leaves/getdraft`,isloading);
        res.data.hr_leave = data.hr_leave;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_leaveServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_leave){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/hr_leaves/${context.hr_leave}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/hr_leaves/${context.hr_leave}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_leaveServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_leave){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/hr_leaves/${context.hr_leave}/createbatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/hr_leaves/${context.hr_leave}/createbatch`,data,isloading);
            return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_leaveServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_leave){
            let res:any = Http.getInstance().delete(`/hr_employees/${context.hr_employee}/hr_leaves/${context.hr_leave}/removebatch`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/hr_leaves/${context.hr_leave}/removebatch`,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_leaveServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_leave){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/hr_leaves/${context.hr_leave}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/hr_leaves/${context.hr_leave}/save`,data,isloading);
            
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_leaveServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_leave){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/hr_employees/${context.hr_employee}/hr_leaves/${context.hr_leave}/updatebatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/hr_leaves/${context.hr_leave}/updatebatch`,data,isloading);
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_leaveServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/hr_employees/${context.hr_employee}/hr_leaves/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/hr_leaves/fetchdefault`,tempData,isloading);
        return res;
    }
}