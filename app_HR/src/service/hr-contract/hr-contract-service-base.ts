import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 合同服务对象基类
 *
 * @export
 * @class Hr_contractServiceBase
 * @extends {EntityServie}
 */
export default class Hr_contractServiceBase extends EntityService {

    /**
     * Creates an instance of  Hr_contractServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_contractServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Hr_contractServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='hr_contract';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'hr_contracts';
        this.APPDETEXT = 'name';
        this.APPNAME = 'hr';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_contractServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/hr_contracts`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/hr_contracts`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_contractServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_contract){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/hr_employees/${context.hr_employee}/hr_contracts/${context.hr_contract}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/hr_contracts/${context.hr_contract}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_contractServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_contract){
            let res:any = Http.getInstance().delete(`/hr_employees/${context.hr_employee}/hr_contracts/${context.hr_contract}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/hr_contracts/${context.hr_contract}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_contractServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_contract){
            let res:any = await Http.getInstance().get(`/hr_employees/${context.hr_employee}/hr_contracts/${context.hr_contract}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/hr_contracts/${context.hr_contract}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_contractServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && true){
            let res:any = await Http.getInstance().get(`/hr_employees/${context.hr_employee}/hr_contracts/getdraft`,isloading);
            res.data.hr_contract = data.hr_contract;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/hr_contracts/getdraft`,isloading);
        res.data.hr_contract = data.hr_contract;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_contractServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_contract){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/hr_contracts/${context.hr_contract}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/hr_contracts/${context.hr_contract}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_contractServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_contract){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/hr_contracts/${context.hr_contract}/createbatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/hr_contracts/${context.hr_contract}/createbatch`,data,isloading);
            return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_contractServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_contract){
            let res:any = Http.getInstance().delete(`/hr_employees/${context.hr_employee}/hr_contracts/${context.hr_contract}/removebatch`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/hr_contracts/${context.hr_contract}/removebatch`,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_contractServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_contract){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/hr_contracts/${context.hr_contract}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/hr_contracts/${context.hr_contract}/save`,data,isloading);
            
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_contractServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_contract){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/hr_employees/${context.hr_employee}/hr_contracts/${context.hr_contract}/updatebatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/hr_contracts/${context.hr_contract}/updatebatch`,data,isloading);
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_contractServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/hr_employees/${context.hr_employee}/hr_contracts/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/hr_contracts/fetchdefault`,tempData,isloading);
        return res;
    }
}