import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_contractServiceBase from './hr-contract-service-base';


/**
 * 合同服务对象
 *
 * @export
 * @class Hr_contractService
 * @extends {Hr_contractServiceBase}
 */
export default class Hr_contractService extends Hr_contractServiceBase {

    /**
     * Creates an instance of  Hr_contractService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_contractService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}