import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_skill_typeServiceBase from './hr-skill-type-service-base';


/**
 * 技能类型服务对象
 *
 * @export
 * @class Hr_skill_typeService
 * @extends {Hr_skill_typeServiceBase}
 */
export default class Hr_skill_typeService extends Hr_skill_typeServiceBase {

    /**
     * Creates an instance of  Hr_skill_typeService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_skill_typeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}