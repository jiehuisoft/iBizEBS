import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_resume_lineServiceBase from './hr-resume-line-service-base';


/**
 * 员工简历行服务对象
 *
 * @export
 * @class Hr_resume_lineService
 * @extends {Hr_resume_lineServiceBase}
 */
export default class Hr_resume_lineService extends Hr_resume_lineServiceBase {

    /**
     * Creates an instance of  Hr_resume_lineService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_resume_lineService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}