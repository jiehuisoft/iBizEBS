import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 员工简历行服务对象基类
 *
 * @export
 * @class Hr_resume_lineServiceBase
 * @extends {EntityServie}
 */
export default class Hr_resume_lineServiceBase extends EntityService {

    /**
     * Creates an instance of  Hr_resume_lineServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_resume_lineServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Hr_resume_lineServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='hr_resume_line';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'hr_resume_lines';
        this.APPDETEXT = 'name';
        this.APPNAME = 'hr';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_resume_lineServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_resume_line){
            let res:any = Http.getInstance().get(`/hr_employees/${context.hr_employee}/hr_resume_lines/${context.hr_resume_line}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/hr_resume_lines/${context.hr_resume_line}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_resume_lineServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/hr_resume_lines`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/hr_resume_lines`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_resume_lineServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_resume_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/hr_employees/${context.hr_employee}/hr_resume_lines/${context.hr_resume_line}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/hr_resume_lines/${context.hr_resume_line}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_resume_lineServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_resume_line){
            let res:any = Http.getInstance().delete(`/hr_employees/${context.hr_employee}/hr_resume_lines/${context.hr_resume_line}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/hr_resume_lines/${context.hr_resume_line}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_resume_lineServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_resume_line){
            let res:any = await Http.getInstance().get(`/hr_employees/${context.hr_employee}/hr_resume_lines/${context.hr_resume_line}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/hr_resume_lines/${context.hr_resume_line}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_resume_lineServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && true){
            let res:any = await Http.getInstance().get(`/hr_employees/${context.hr_employee}/hr_resume_lines/getdraft`,isloading);
            res.data.hr_resume_line = data.hr_resume_line;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/hr_resume_lines/getdraft`,isloading);
        res.data.hr_resume_line = data.hr_resume_line;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_resume_lineServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_resume_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/hr_resume_lines/${context.hr_resume_line}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/hr_resume_lines/${context.hr_resume_line}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_resume_lineServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && context.hr_resume_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/hr_employees/${context.hr_employee}/hr_resume_lines/${context.hr_resume_line}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/hr_resume_lines/${context.hr_resume_line}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Hr_resume_lineServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.hr_employee && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/hr_employees/${context.hr_employee}/hr_resume_lines/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/hr_resume_lines/fetchdefault`,tempData,isloading);
        return res;
    }
}