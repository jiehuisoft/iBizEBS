import { Http } from '@/utils';
import { Util } from '@/utils';
import Mail_followers_mail_message_subtype_relServiceBase from './mail-followers-mail-message-subtype-rel-service-base';


/**
 * 关注消息类型服务对象
 *
 * @export
 * @class Mail_followers_mail_message_subtype_relService
 * @extends {Mail_followers_mail_message_subtype_relServiceBase}
 */
export default class Mail_followers_mail_message_subtype_relService extends Mail_followers_mail_message_subtype_relServiceBase {

    /**
     * Creates an instance of  Mail_followers_mail_message_subtype_relService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_followers_mail_message_subtype_relService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}