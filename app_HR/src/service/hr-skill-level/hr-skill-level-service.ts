import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_skill_levelServiceBase from './hr-skill-level-service-base';


/**
 * 技能等级服务对象
 *
 * @export
 * @class Hr_skill_levelService
 * @extends {Hr_skill_levelServiceBase}
 */
export default class Hr_skill_levelService extends Hr_skill_levelServiceBase {

    /**
     * Creates an instance of  Hr_skill_levelService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_skill_levelService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}