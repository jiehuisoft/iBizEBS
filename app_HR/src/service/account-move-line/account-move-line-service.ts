import { Http } from '@/utils';
import { Util } from '@/utils';
import Account_move_lineServiceBase from './account-move-line-service-base';


/**
 * 日记账项目服务对象
 *
 * @export
 * @class Account_move_lineService
 * @extends {Account_move_lineServiceBase}
 */
export default class Account_move_lineService extends Account_move_lineServiceBase {

    /**
     * Creates an instance of  Account_move_lineService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_move_lineService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}