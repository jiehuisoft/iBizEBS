import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_employeeServiceBase from './hr-employee-service-base';


/**
 * 员工服务对象
 *
 * @export
 * @class Hr_employeeService
 * @extends {Hr_employeeServiceBase}
 */
export default class Hr_employeeService extends Hr_employeeServiceBase {

    /**
     * Creates an instance of  Hr_employeeService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_employeeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}