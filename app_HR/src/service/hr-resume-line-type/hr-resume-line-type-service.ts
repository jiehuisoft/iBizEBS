import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_resume_line_typeServiceBase from './hr-resume-line-type-service-base';


/**
 * 简历类型服务对象
 *
 * @export
 * @class Hr_resume_line_typeService
 * @extends {Hr_resume_line_typeServiceBase}
 */
export default class Hr_resume_line_typeService extends Hr_resume_line_typeServiceBase {

    /**
     * Creates an instance of  Hr_resume_line_typeService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_resume_line_typeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}