import Hr_jobUIServiceBase from './hr-job-ui-service-base';

/**
 * 工作岗位UI服务对象
 *
 * @export
 * @class Hr_jobUIService
 */
export default class Hr_jobUIService extends Hr_jobUIServiceBase {

    /**
     * Creates an instance of  Hr_jobUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_jobUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}