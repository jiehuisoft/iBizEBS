import Hr_resume_lineUIServiceBase from './hr-resume-line-ui-service-base';

/**
 * 员工简历行UI服务对象
 *
 * @export
 * @class Hr_resume_lineUIService
 */
export default class Hr_resume_lineUIService extends Hr_resume_lineUIServiceBase {

    /**
     * Creates an instance of  Hr_resume_lineUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_resume_lineUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}