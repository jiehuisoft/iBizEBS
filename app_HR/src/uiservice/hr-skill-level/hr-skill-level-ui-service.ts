import Hr_skill_levelUIServiceBase from './hr-skill-level-ui-service-base';

/**
 * 技能等级UI服务对象
 *
 * @export
 * @class Hr_skill_levelUIService
 */
export default class Hr_skill_levelUIService extends Hr_skill_levelUIServiceBase {

    /**
     * Creates an instance of  Hr_skill_levelUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_skill_levelUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}