import Account_move_lineUIServiceBase from './account-move-line-ui-service-base';

/**
 * 日记账项目UI服务对象
 *
 * @export
 * @class Account_move_lineUIService
 */
export default class Account_move_lineUIService extends Account_move_lineUIServiceBase {

    /**
     * Creates an instance of  Account_move_lineUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_move_lineUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}