import Hr_contractUIServiceBase from './hr-contract-ui-service-base';

/**
 * 合同UI服务对象
 *
 * @export
 * @class Hr_contractUIService
 */
export default class Hr_contractUIService extends Hr_contractUIServiceBase {

    /**
     * Creates an instance of  Hr_contractUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_contractUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}