import Resource_calendarUIServiceBase from './resource-calendar-ui-service-base';

/**
 * 资源工作时间UI服务对象
 *
 * @export
 * @class Resource_calendarUIService
 */
export default class Resource_calendarUIService extends Resource_calendarUIServiceBase {

    /**
     * Creates an instance of  Resource_calendarUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Resource_calendarUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}