import Gamification_badgeUIServiceBase from './gamification-badge-ui-service-base';

/**
 * 游戏化徽章UI服务对象
 *
 * @export
 * @class Gamification_badgeUIService
 */
export default class Gamification_badgeUIService extends Gamification_badgeUIServiceBase {

    /**
     * Creates an instance of  Gamification_badgeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Gamification_badgeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}