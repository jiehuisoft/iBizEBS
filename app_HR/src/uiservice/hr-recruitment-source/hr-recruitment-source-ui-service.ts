import Hr_recruitment_sourceUIServiceBase from './hr-recruitment-source-ui-service-base';

/**
 * 应聘者来源UI服务对象
 *
 * @export
 * @class Hr_recruitment_sourceUIService
 */
export default class Hr_recruitment_sourceUIService extends Hr_recruitment_sourceUIServiceBase {

    /**
     * Creates an instance of  Hr_recruitment_sourceUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_recruitment_sourceUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}