import Gamification_challengeUIServiceBase from './gamification-challenge-ui-service-base';

/**
 * 游戏化挑战UI服务对象
 *
 * @export
 * @class Gamification_challengeUIService
 */
export default class Gamification_challengeUIService extends Gamification_challengeUIServiceBase {

    /**
     * Creates an instance of  Gamification_challengeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Gamification_challengeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}