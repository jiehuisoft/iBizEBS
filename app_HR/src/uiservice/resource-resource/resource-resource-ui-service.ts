import Resource_resourceUIServiceBase from './resource-resource-ui-service-base';

/**
 * 资源UI服务对象
 *
 * @export
 * @class Resource_resourceUIService
 */
export default class Resource_resourceUIService extends Resource_resourceUIServiceBase {

    /**
     * Creates an instance of  Resource_resourceUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Resource_resourceUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}