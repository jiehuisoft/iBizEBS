import Hr_skill_typeUIServiceBase from './hr-skill-type-ui-service-base';

/**
 * 技能类型UI服务对象
 *
 * @export
 * @class Hr_skill_typeUIService
 */
export default class Hr_skill_typeUIService extends Hr_skill_typeUIServiceBase {

    /**
     * Creates an instance of  Hr_skill_typeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_skill_typeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}