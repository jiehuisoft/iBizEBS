import Mail_followers_mail_message_subtype_relUIServiceBase from './mail-followers-mail-message-subtype-rel-ui-service-base';

/**
 * 关注消息类型UI服务对象
 *
 * @export
 * @class Mail_followers_mail_message_subtype_relUIService
 */
export default class Mail_followers_mail_message_subtype_relUIService extends Mail_followers_mail_message_subtype_relUIServiceBase {

    /**
     * Creates an instance of  Mail_followers_mail_message_subtype_relUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_followers_mail_message_subtype_relUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}