import Mail_messageUIServiceBase from './mail-message-ui-service-base';

/**
 * 消息UI服务对象
 *
 * @export
 * @class Mail_messageUIService
 */
export default class Mail_messageUIService extends Mail_messageUIServiceBase {

    /**
     * Creates an instance of  Mail_messageUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_messageUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}