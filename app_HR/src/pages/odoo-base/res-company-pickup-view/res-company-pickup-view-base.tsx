import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import Res_companyService from '@/service/res-company/res-company-service';
import Res_companyAuthService from '@/authservice/res-company/res-company-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import Res_companyUIService from '@/uiservice/res-company/res-company-ui-service';

/**
 * 公司数据选择视图视图基类
 *
 * @export
 * @class Res_companyPickupViewBase
 * @extends {PickupViewBase}
 */
export class Res_companyPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Res_companyPickupViewBase
     */
    protected appDeName: string = 'res_company';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Res_companyPickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Res_companyPickupViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Res_companyService}
     * @memberof Res_companyPickupViewBase
     */
    protected appEntityService: Res_companyService = new Res_companyService;

    /**
     * 实体权限服务对象
     *
     * @type Res_companyUIService
     * @memberof Res_companyPickupViewBase
     */
    public appUIService: Res_companyUIService = new Res_companyUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Res_companyPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.res_company.views.pickupview.caption',
        srfTitle: 'entities.res_company.views.pickupview.title',
        srfSubTitle: 'entities.res_company.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Res_companyPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Res_companyPickupViewBase
     */
	protected viewtag: string = '89f22e53f7ad7ee53bf0abd030269dfe';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Res_companyPickupViewBase
     */ 
    protected viewName:string = "res_companyPickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Res_companyPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Res_companyPickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Res_companyPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'res_company',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_companyPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_companyPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_companyPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}