import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import Survey_surveyService from '@/service/survey-survey/survey-survey-service';
import Survey_surveyAuthService from '@/authservice/survey-survey/survey-survey-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import Survey_surveyUIService from '@/uiservice/survey-survey/survey-survey-ui-service';

/**
 * 问卷选择表格视图视图基类
 *
 * @export
 * @class Survey_surveyPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class Survey_surveyPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Survey_surveyPickupGridViewBase
     */
    protected appDeName: string = 'survey_survey';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Survey_surveyPickupGridViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Survey_surveyPickupGridViewBase
     */
    protected appDeMajor: string = 'title';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Survey_surveyPickupGridViewBase
     */ 
    protected dataControl:string = "grid";

    /**
     * 实体服务对象
     *
     * @type {Survey_surveyService}
     * @memberof Survey_surveyPickupGridViewBase
     */
    protected appEntityService: Survey_surveyService = new Survey_surveyService;

    /**
     * 实体权限服务对象
     *
     * @type Survey_surveyUIService
     * @memberof Survey_surveyPickupGridViewBase
     */
    public appUIService: Survey_surveyUIService = new Survey_surveyUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Survey_surveyPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.survey_survey.views.pickupgridview.caption',
        srfTitle: 'entities.survey_survey.views.pickupgridview.title',
        srfSubTitle: 'entities.survey_survey.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Survey_surveyPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Survey_surveyPickupGridViewBase
     */
	protected viewtag: string = 'e62157d555e4d41070c10edf4b8843d4';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Survey_surveyPickupGridViewBase
     */ 
    protected viewName:string = "survey_surveyPickupGridView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Survey_surveyPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Survey_surveyPickupGridViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Survey_surveyPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'survey_survey',
            majorPSDEField: 'title',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Survey_surveyPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Survey_surveyPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Survey_surveyPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Survey_surveyPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Survey_surveyPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Survey_surveyPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Survey_surveyPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof Survey_surveyPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}