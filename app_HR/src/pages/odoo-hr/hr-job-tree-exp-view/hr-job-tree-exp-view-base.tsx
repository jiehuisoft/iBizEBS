import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TreeExpViewBase } from '@/studio-core';
import Hr_jobService from '@/service/hr-job/hr-job-service';
import Hr_jobAuthService from '@/authservice/hr-job/hr-job-auth-service';
import TreeExpViewEngine from '@engine/view/tree-exp-view-engine';
import Hr_jobUIService from '@/uiservice/hr-job/hr-job-ui-service';

/**
 * 工作岗位树导航视图视图基类
 *
 * @export
 * @class Hr_jobTreeExpViewBase
 * @extends {TreeExpViewBase}
 */
export class Hr_jobTreeExpViewBase extends TreeExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobTreeExpViewBase
     */
    protected appDeName: string = 'hr_job';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobTreeExpViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobTreeExpViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Hr_jobService}
     * @memberof Hr_jobTreeExpViewBase
     */
    protected appEntityService: Hr_jobService = new Hr_jobService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_jobUIService
     * @memberof Hr_jobTreeExpViewBase
     */
    public appUIService: Hr_jobUIService = new Hr_jobUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Hr_jobTreeExpViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_jobTreeExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_job.views.treeexpview.caption',
        srfTitle: 'entities.hr_job.views.treeexpview.title',
        srfSubTitle: 'entities.hr_job.views.treeexpview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_jobTreeExpViewBase
     */
    protected containerModel: any = {
        view_treeexpbar: { name: 'treeexpbar', type: 'TREEEXPBAR' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobTreeExpViewBase
     */
	protected viewtag: string = '89b6dbe4fc4224a326f32d0d7adff239';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobTreeExpViewBase
     */ 
    protected viewName:string = "hr_jobTreeExpView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_jobTreeExpViewBase
     */
    public engine: TreeExpViewEngine = new TreeExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_jobTreeExpViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_jobTreeExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            treeexpbar: this.$refs.treeexpbar,
            keyPSDEField: 'hr_job',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * treeexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_jobTreeExpViewBase
     */
    public treeexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'selectionchange', $event);
    }

    /**
     * treeexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_jobTreeExpViewBase
     */
    public treeexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'activated', $event);
    }

    /**
     * treeexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_jobTreeExpViewBase
     */
    public treeexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('treeexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Hr_jobTreeExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Hr_jobTreeExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }



    /**
     * 视图唯一标识
     *
     * @type {string}
     * @memberof Hr_jobTreeExpView
     */
    public viewUID: string = 'odoo-hr-hr-job-tree-exp-view';


}