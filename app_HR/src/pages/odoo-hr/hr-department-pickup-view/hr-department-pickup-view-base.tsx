import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import Hr_departmentService from '@/service/hr-department/hr-department-service';
import Hr_departmentAuthService from '@/authservice/hr-department/hr-department-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import Hr_departmentUIService from '@/uiservice/hr-department/hr-department-ui-service';

/**
 * HR 部门数据选择视图视图基类
 *
 * @export
 * @class Hr_departmentPickupViewBase
 * @extends {PickupViewBase}
 */
export class Hr_departmentPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentPickupViewBase
     */
    protected appDeName: string = 'hr_department';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentPickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentPickupViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Hr_departmentService}
     * @memberof Hr_departmentPickupViewBase
     */
    protected appEntityService: Hr_departmentService = new Hr_departmentService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_departmentUIService
     * @memberof Hr_departmentPickupViewBase
     */
    public appUIService: Hr_departmentUIService = new Hr_departmentUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_departmentPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_department.views.pickupview.caption',
        srfTitle: 'entities.hr_department.views.pickupview.title',
        srfSubTitle: 'entities.hr_department.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_departmentPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentPickupViewBase
     */
	protected viewtag: string = '765a695b889be2aa0520261276422fb9';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentPickupViewBase
     */ 
    protected viewName:string = "hr_departmentPickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_departmentPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_departmentPickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_departmentPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'hr_department',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_departmentPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_departmentPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_departmentPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}