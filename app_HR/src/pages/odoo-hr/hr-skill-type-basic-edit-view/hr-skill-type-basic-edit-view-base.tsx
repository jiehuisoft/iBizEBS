import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Hr_skill_typeService from '@/service/hr-skill-type/hr-skill-type-service';
import Hr_skill_typeAuthService from '@/authservice/hr-skill-type/hr-skill-type-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Hr_skill_typeUIService from '@/uiservice/hr-skill-type/hr-skill-type-ui-service';

/**
 * 配置信息编辑视图视图基类
 *
 * @export
 * @class Hr_skill_typeBasicEditViewBase
 * @extends {EditViewBase}
 */
export class Hr_skill_typeBasicEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicEditViewBase
     */
    protected appDeName: string = 'hr_skill_type';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicEditViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicEditViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicEditViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Hr_skill_typeService}
     * @memberof Hr_skill_typeBasicEditViewBase
     */
    protected appEntityService: Hr_skill_typeService = new Hr_skill_typeService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_skill_typeUIService
     * @memberof Hr_skill_typeBasicEditViewBase
     */
    public appUIService: Hr_skill_typeUIService = new Hr_skill_typeUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Hr_skill_typeBasicEditViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_skill_typeBasicEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_skill_type.views.basiceditview.caption',
        srfTitle: 'entities.hr_skill_type.views.basiceditview.title',
        srfSubTitle: 'entities.hr_skill_type.views.basiceditview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_skill_typeBasicEditViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicEditViewBase
     */
	protected viewtag: string = '107307dcd3d80f3c8ff51c8a8894baba';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicEditViewBase
     */ 
    protected viewName:string = "hr_skill_typeBasicEditView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_skill_typeBasicEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_skill_typeBasicEditViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_skill_typeBasicEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'hr_skill_type',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_skill_typeBasicEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_skill_typeBasicEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_skill_typeBasicEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}