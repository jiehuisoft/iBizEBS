import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import Hr_employeeService from '@/service/hr-employee/hr-employee-service';
import Hr_employeeAuthService from '@/authservice/hr-employee/hr-employee-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import Hr_employeeUIService from '@/uiservice/hr-employee/hr-employee-ui-service';

/**
 * 员工选择表格视图视图基类
 *
 * @export
 * @class Hr_employeePickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class Hr_employeePickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeePickupGridViewBase
     */
    protected appDeName: string = 'hr_employee';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeePickupGridViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeePickupGridViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeePickupGridViewBase
     */ 
    protected dataControl:string = "grid";

    /**
     * 实体服务对象
     *
     * @type {Hr_employeeService}
     * @memberof Hr_employeePickupGridViewBase
     */
    protected appEntityService: Hr_employeeService = new Hr_employeeService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_employeeUIService
     * @memberof Hr_employeePickupGridViewBase
     */
    public appUIService: Hr_employeeUIService = new Hr_employeeUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_employeePickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_employee.views.pickupgridview.caption',
        srfTitle: 'entities.hr_employee.views.pickupgridview.title',
        srfSubTitle: 'entities.hr_employee.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_employeePickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeePickupGridViewBase
     */
	protected viewtag: string = '0626393582cbb7fd983d27f9b1341670';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeePickupGridViewBase
     */ 
    protected viewName:string = "hr_employeePickupGridView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_employeePickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_employeePickupGridViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_employeePickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'hr_employee',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeePickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeePickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeePickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeePickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeePickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeePickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeePickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof Hr_employeePickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}