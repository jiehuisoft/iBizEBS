import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import Hr_contractService from '@/service/hr-contract/hr-contract-service';
import Hr_contractAuthService from '@/authservice/hr-contract/hr-contract-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import Hr_contractUIService from '@/uiservice/hr-contract/hr-contract-ui-service';

/**
 * 合同快速新建视图视图基类
 *
 * @export
 * @class Hr_contractBasicQuickViewBase
 * @extends {OptionViewBase}
 */
export class Hr_contractBasicQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_contractBasicQuickViewBase
     */
    protected appDeName: string = 'hr_contract';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_contractBasicQuickViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_contractBasicQuickViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_contractBasicQuickViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Hr_contractService}
     * @memberof Hr_contractBasicQuickViewBase
     */
    protected appEntityService: Hr_contractService = new Hr_contractService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_contractUIService
     * @memberof Hr_contractBasicQuickViewBase
     */
    public appUIService: Hr_contractUIService = new Hr_contractUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Hr_contractBasicQuickViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_contractBasicQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_contract.views.basicquickview.caption',
        srfTitle: 'entities.hr_contract.views.basicquickview.title',
        srfSubTitle: 'entities.hr_contract.views.basicquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_contractBasicQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_contractBasicQuickViewBase
     */
	protected viewtag: string = '8cc954bbf41346fe5ddea7ec2f24787a';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_contractBasicQuickViewBase
     */ 
    protected viewName:string = "hr_contractBasicQuickView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_contractBasicQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_contractBasicQuickViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_contractBasicQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'hr_contract',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_contractBasicQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_contractBasicQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_contractBasicQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}