import Vue from 'vue';
import Router from 'vue-router';
import { AuthGuard } from '@/utils';
import qs from 'qs';
import { globalRoutes, indexRoutes} from '@/router'
import { AppService } from '@/studio-core/service/app-service/AppService';

Vue.use(Router);

const appService = new AppService();

const router = new Router({
    routes: [
                {
            path: '/hrindexview/:hrindexview?',
            beforeEnter: async (to: any, from: any, next: any) => {
                const routerParamsName = 'hrindexview';
                const params: any = {};
                if (to.params && to.params[routerParamsName]) {
                    params[routerParamsName] = to.params[routerParamsName];
                }
                const url: string = '/appdata';
                const bol = await AuthGuard.getInstance().authGuard(url, params, router);
                if (bol) {
                    appService.navHistory.indexMeta = {
                        caption: 'app.views.hrindexview.caption',
                        info:'',
                        viewType: 'APPINDEX',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                        ],
                        requireAuth: true,
                    };
                    next();
                }
            },
            meta: {  
                caption: 'app.views.hrindexview.caption',
                info:'',
                viewType: 'APPINDEX',
                parameters: [
                    { pathName: 'hrindexview', parameterName: 'hrindexview' },
                ],
                requireAuth: true,
            },
            component: () => import('@pages/odoo-hr/hrindex-view/hrindex-view.vue'),
            children: [
                {
                    path: 'hr_employees/:hr_employee?/hr_resume_lines/:hr_resume_line?/line/:line?',
                    meta: {
                        caption: 'entities.hr_resume_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'hr_resume_lines', parameterName: 'hr_resume_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-resume-line-line/hr-resume-line-line.vue'),
                },
                {
                    path: 'hr_resume_lines/:hr_resume_line?/line/:line?',
                    meta: {
                        caption: 'entities.hr_resume_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_resume_lines', parameterName: 'hr_resume_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-resume-line-line/hr-resume-line-line.vue'),
                },
                {
                    path: 'hr_departments/:hr_department?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.hr_department.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_departments', parameterName: 'hr_department' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-department-pickup-view/hr-department-pickup-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/maintenance_equipments/:maintenance_equipment?/editview/:editview?',
                    meta: {
                        caption: 'entities.maintenance_equipment.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'maintenance_equipments', parameterName: 'maintenance_equipment' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-maintenance/maintenance-equipment-edit-view/maintenance-equipment-edit-view.vue'),
                },
                {
                    path: 'maintenance_equipments/:maintenance_equipment?/editview/:editview?',
                    meta: {
                        caption: 'entities.maintenance_equipment.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'maintenance_equipments', parameterName: 'maintenance_equipment' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-maintenance/maintenance-equipment-edit-view/maintenance-equipment-edit-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/hr_contracts/:hr_contract?/basiceditview/:basiceditview?',
                    meta: {
                        caption: 'entities.hr_contract.views.basiceditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'hr_contracts', parameterName: 'hr_contract' },
                            { pathName: 'basiceditview', parameterName: 'basiceditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-contract-basic-edit-view/hr-contract-basic-edit-view.vue'),
                },
                {
                    path: 'hr_contracts/:hr_contract?/basiceditview/:basiceditview?',
                    meta: {
                        caption: 'entities.hr_contract.views.basiceditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_contracts', parameterName: 'hr_contract' },
                            { pathName: 'basiceditview', parameterName: 'basiceditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-contract-basic-edit-view/hr-contract-basic-edit-view.vue'),
                },
                {
                    path: 'mail_messages/:mail_message?/mainview/:mainview?',
                    meta: {
                        caption: 'entities.mail_message.views.mainview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'mail_messages', parameterName: 'mail_message' },
                            { pathName: 'mainview', parameterName: 'mainview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-message-main-view/mail-message-main-view.vue'),
                },
                {
                    path: 'hr_skill_types/:hr_skill_type?/hr_skill_levels/:hr_skill_level?/editview/:editview?',
                    meta: {
                        caption: 'entities.hr_skill_level.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_skill_types', parameterName: 'hr_skill_type' },
                            { pathName: 'hr_skill_levels', parameterName: 'hr_skill_level' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-skill-level-edit-view/hr-skill-level-edit-view.vue'),
                },
                {
                    path: 'hr_skill_levels/:hr_skill_level?/editview/:editview?',
                    meta: {
                        caption: 'entities.hr_skill_level.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_skill_levels', parameterName: 'hr_skill_level' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-skill-level-edit-view/hr-skill-level-edit-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.hr_employee.views.masterquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-employee-master-quick-view/hr-employee-master-quick-view.vue'),
                },
                {
                    path: 'resource_calendars/:resource_calendar?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.resource_calendar.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'resource_calendars', parameterName: 'resource_calendar' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-resource/resource-calendar-pickup-view/resource-calendar-pickup-view.vue'),
                },
                {
                    path: 'hr_resume_line_types/:hr_resume_line_type?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.hr_resume_line_type.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_resume_line_types', parameterName: 'hr_resume_line_type' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-resume-line-type-line-edit/hr-resume-line-type-line-edit.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/mastersummaryview/:mastersummaryview?',
                    meta: {
                        caption: 'entities.hr_employee.views.mastersummaryview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'mastersummaryview', parameterName: 'mastersummaryview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-employee-master-summary-view/hr-employee-master-summary-view.vue'),
                },
                {
                    path: 'hr_jobs/:hr_job?/mastereditview/:mastereditview?',
                    meta: {
                        caption: 'entities.hr_job.views.mastereditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_jobs', parameterName: 'hr_job' },
                            { pathName: 'mastereditview', parameterName: 'mastereditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-job-master-edit-view/hr-job-master-edit-view.vue'),
                },
                {
                    path: 'hr_skill_types/:hr_skill_type?/hr_skills/:hr_skill?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.hr_skill.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_skill_types', parameterName: 'hr_skill_type' },
                            { pathName: 'hr_skills', parameterName: 'hr_skill' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-skill-line-edit/hr-skill-line-edit.vue'),
                },
                {
                    path: 'hr_skills/:hr_skill?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.hr_skill.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_skills', parameterName: 'hr_skill' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-skill-line-edit/hr-skill-line-edit.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/hr_leaves/:hr_leave?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.hr_leave.views.gridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'hr_leaves', parameterName: 'hr_leave' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-leave-grid-view/hr-leave-grid-view.vue'),
                },
                {
                    path: 'hr_leaves/:hr_leave?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.hr_leave.views.gridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_leaves', parameterName: 'hr_leave' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-leave-grid-view/hr-leave-grid-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/hr_contracts/:hr_contract?/basicquickview/:basicquickview?',
                    meta: {
                        caption: 'entities.hr_contract.views.basicquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'hr_contracts', parameterName: 'hr_contract' },
                            { pathName: 'basicquickview', parameterName: 'basicquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-contract-basic-quick-view/hr-contract-basic-quick-view.vue'),
                },
                {
                    path: 'hr_contracts/:hr_contract?/basicquickview/:basicquickview?',
                    meta: {
                        caption: 'entities.hr_contract.views.basicquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_contracts', parameterName: 'hr_contract' },
                            { pathName: 'basicquickview', parameterName: 'basicquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-contract-basic-quick-view/hr-contract-basic-quick-view.vue'),
                },
                {
                    path: 'hr_resume_line_types/:hr_resume_line_type?/editview/:editview?',
                    meta: {
                        caption: 'entities.hr_resume_line_type.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_resume_line_types', parameterName: 'hr_resume_line_type' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-resume-line-type-edit-view/hr-resume-line-type-edit-view.vue'),
                },
                {
                    path: 'mail_activities/:mail_activity?/byreslistview/:byreslistview?',
                    meta: {
                        caption: 'entities.mail_activity.views.byreslistview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'mail_activities', parameterName: 'mail_activity' },
                            { pathName: 'byreslistview', parameterName: 'byreslistview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-activity-by-res-list-view/mail-activity-by-res-list-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.hr_employee.views.masterinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-employee-master-info-view/hr-employee-master-info-view.vue'),
                },
                {
                    path: 'hr_jobs/:hr_job?/masterinfoview/:masterinfoview?',
                    meta: {
                        caption: 'entities.hr_job.views.masterinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_jobs', parameterName: 'hr_job' },
                            { pathName: 'masterinfoview', parameterName: 'masterinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-job-master-info-view/hr-job-master-info-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.hr_employee.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-employee-pickup-view/hr-employee-pickup-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.res_partner.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pickup-grid-view/res-partner-pickup-grid-view.vue'),
                },
                {
                    path: 'hr_jobs/:hr_job?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.hr_job.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_jobs', parameterName: 'hr_job' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-job-pickup-grid-view/hr-job-pickup-grid-view.vue'),
                },
                {
                    path: 'hr_employee_categories/:hr_employee_category?/editview/:editview?',
                    meta: {
                        caption: 'entities.hr_employee_category.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employee_categories', parameterName: 'hr_employee_category' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-employee-category-edit-view/hr-employee-category-edit-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/mastereditview/:mastereditview?',
                    meta: {
                        caption: 'entities.hr_employee.views.mastereditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'mastereditview', parameterName: 'mastereditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-employee-master-edit-view/hr-employee-master-edit-view.vue'),
                },
                {
                    path: 'hr_skill_types/:hr_skill_type?/basicquickview/:basicquickview?',
                    meta: {
                        caption: 'entities.hr_skill_type.views.basicquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_skill_types', parameterName: 'hr_skill_type' },
                            { pathName: 'basicquickview', parameterName: 'basicquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-skill-type-basic-quick-view/hr-skill-type-basic-quick-view.vue'),
                },
                {
                    path: 'mail_activities/:mail_activity?/editview/:editview?',
                    meta: {
                        caption: 'entities.mail_activity.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'mail_activities', parameterName: 'mail_activity' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-activity-edit-view/mail-activity-edit-view.vue'),
                },
                {
                    path: 'account_analytic_lines/:account_analytic_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.account_analytic_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'account_analytic_lines', parameterName: 'account_analytic_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-account/account-analytic-line-edit-view/account-analytic-line-edit-view.vue'),
                },
                {
                    path: 'res_companies/:res_company?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.res_company.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'res_companies', parameterName: 'res_company' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-company-pickup-view/res-company-pickup-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/mastergridview/:mastergridview?',
                    meta: {
                        caption: 'entities.hr_employee.views.mastergridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'mastergridview', parameterName: 'mastergridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-employee-master-grid-view/hr-employee-master-grid-view.vue'),
                },
                {
                    path: 'hr_departments/:hr_department?/mastergridview/:mastergridview?',
                    meta: {
                        caption: 'entities.hr_department.views.mastergridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_departments', parameterName: 'hr_department' },
                            { pathName: 'mastergridview', parameterName: 'mastergridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-department-master-grid-view/hr-department-master-grid-view.vue'),
                },
                {
                    path: 'hr_skill_types/:hr_skill_type?/hr_skill_levels/:hr_skill_level?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.hr_skill_level.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_skill_types', parameterName: 'hr_skill_type' },
                            { pathName: 'hr_skill_levels', parameterName: 'hr_skill_level' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-skill-level-line-edit/hr-skill-level-line-edit.vue'),
                },
                {
                    path: 'hr_skill_levels/:hr_skill_level?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.hr_skill_level.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_skill_levels', parameterName: 'hr_skill_level' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-skill-level-line-edit/hr-skill-level-line-edit.vue'),
                },
                {
                    path: 'hr_jobs/:hr_job?/mastergridview/:mastergridview?',
                    meta: {
                        caption: 'entities.hr_job.views.mastergridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_jobs', parameterName: 'hr_job' },
                            { pathName: 'mastergridview', parameterName: 'mastergridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-job-master-grid-view/hr-job-master-grid-view.vue'),
                },
                {
                    path: 'res_users/:res_users?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.res_users.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-pickup-view/res-users-pickup-view.vue'),
                },
                {
                    path: 'hr_jobs/:hr_job?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.hr_job.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_jobs', parameterName: 'hr_job' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-job-pickup-view/hr-job-pickup-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/maintenance_equipments/:maintenance_equipment?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.maintenance_equipment.views.gridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'maintenance_equipments', parameterName: 'maintenance_equipment' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-maintenance/maintenance-equipment-grid-view/maintenance-equipment-grid-view.vue'),
                },
                {
                    path: 'maintenance_equipments/:maintenance_equipment?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.maintenance_equipment.views.gridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'maintenance_equipments', parameterName: 'maintenance_equipment' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-maintenance/maintenance-equipment-grid-view/maintenance-equipment-grid-view.vue'),
                },
                {
                    path: 'hr_departments/:hr_department?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.hr_department.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_departments', parameterName: 'hr_department' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-department-pickup-grid-view/hr-department-pickup-grid-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/hr_leaves/:hr_leave?/editview/:editview?',
                    meta: {
                        caption: 'entities.hr_leave.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'hr_leaves', parameterName: 'hr_leave' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-leave-edit-view/hr-leave-edit-view.vue'),
                },
                {
                    path: 'hr_leaves/:hr_leave?/editview/:editview?',
                    meta: {
                        caption: 'entities.hr_leave.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_leaves', parameterName: 'hr_leave' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-leave-edit-view/hr-leave-edit-view.vue'),
                },
                {
                    path: 'hr_skill_types/:hr_skill_type?/hr_skills/:hr_skill?/editview/:editview?',
                    meta: {
                        caption: 'entities.hr_skill.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_skill_types', parameterName: 'hr_skill_type' },
                            { pathName: 'hr_skills', parameterName: 'hr_skill' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-skill-edit-view/hr-skill-edit-view.vue'),
                },
                {
                    path: 'hr_skills/:hr_skill?/editview/:editview?',
                    meta: {
                        caption: 'entities.hr_skill.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_skills', parameterName: 'hr_skill' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-skill-edit-view/hr-skill-edit-view.vue'),
                },
                {
                    path: 'mail_messages/:mail_message?/remarkview/:remarkview?',
                    meta: {
                        caption: 'entities.mail_message.views.remarkview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'mail_messages', parameterName: 'mail_message' },
                            { pathName: 'remarkview', parameterName: 'remarkview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-message-remark-view/mail-message-remark-view.vue'),
                },
                {
                    path: 'res_users/:res_users?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.res_users.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'res_users', parameterName: 'res_users' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-users-pickup-grid-view/res-users-pickup-grid-view.vue'),
                },
                {
                    path: 'hr_departments/:hr_department?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.hr_department.views.mastertabinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_departments', parameterName: 'hr_department' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-department-master-tab-info-view/hr-department-master-tab-info-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.hr_employee.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-employee-pickup-grid-view/hr-employee-pickup-grid-view.vue'),
                },
                {
                    path: 'survey_surveys/:survey_survey?/editview/:editview?',
                    meta: {
                        caption: 'entities.survey_survey.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'survey_surveys', parameterName: 'survey_survey' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-survey/survey-survey-edit-view/survey-survey-edit-view.vue'),
                },
                {
                    path: 'mail_followers/:mail_followers?/byreslistview/:byreslistview?',
                    meta: {
                        caption: 'entities.mail_followers.views.byreslistview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'mail_followers', parameterName: 'mail_followers' },
                            { pathName: 'byreslistview', parameterName: 'byreslistview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-followers-by-res-list-view/mail-followers-by-res-list-view.vue'),
                },
                {
                    path: 'hr_jobs/:hr_job?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.hr_job.views.masterquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_jobs', parameterName: 'hr_job' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-job-master-quick-view/hr-job-master-quick-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/hr_contracts/:hr_contract?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.hr_contract.views.gridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'hr_contracts', parameterName: 'hr_contract' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-contract-grid-view/hr-contract-grid-view.vue'),
                },
                {
                    path: 'hr_contracts/:hr_contract?/gridview/:gridview?',
                    meta: {
                        caption: 'entities.hr_contract.views.gridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_contracts', parameterName: 'hr_contract' },
                            { pathName: 'gridview', parameterName: 'gridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-contract-grid-view/hr-contract-grid-view.vue'),
                },
                {
                    path: 'resource_resources/:resource_resource?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.resource_resource.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'resource_resources', parameterName: 'resource_resource' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-resource/resource-resource-pickup-view/resource-resource-pickup-view.vue'),
                },
                {
                    path: 'ir_attachments/:ir_attachment?/byresdataview/:byresdataview?',
                    meta: {
                        caption: 'entities.ir_attachment.views.byresdataview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'ir_attachments', parameterName: 'ir_attachment' },
                            { pathName: 'byresdataview', parameterName: 'byresdataview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-ir/ir-attachment-by-res-data-view/ir-attachment-by-res-data-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/hr_resume_lines/:hr_resume_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.hr_resume_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'hr_resume_lines', parameterName: 'hr_resume_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-resume-line-edit-view/hr-resume-line-edit-view.vue'),
                },
                {
                    path: 'hr_resume_lines/:hr_resume_line?/editview/:editview?',
                    meta: {
                        caption: 'entities.hr_resume_line.views.editview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_resume_lines', parameterName: 'hr_resume_line' },
                            { pathName: 'editview', parameterName: 'editview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-resume-line-edit-view/hr-resume-line-edit-view.vue'),
                },
                {
                    path: 'resource_resources/:resource_resource?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.resource_resource.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'resource_resources', parameterName: 'resource_resource' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-resource/resource-resource-pickup-grid-view/resource-resource-pickup-grid-view.vue'),
                },
                {
                    path: 'survey_surveys/:survey_survey?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.survey_survey.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'survey_surveys', parameterName: 'survey_survey' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-survey/survey-survey-pickup-view/survey-survey-pickup-view.vue'),
                },
                {
                    path: 'hr_departments/:hr_department?/masterquickview/:masterquickview?',
                    meta: {
                        caption: 'entities.hr_department.views.masterquickview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_departments', parameterName: 'hr_department' },
                            { pathName: 'masterquickview', parameterName: 'masterquickview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-department-master-quick-view/hr-department-master-quick-view.vue'),
                },
                {
                    path: 'hr_jobs/:hr_job?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.hr_job.views.mastertabinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_jobs', parameterName: 'hr_job' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-job-master-tab-info-view/hr-job-master-tab-info-view.vue'),
                },
                {
                    path: 'resource_calendars/:resource_calendar?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.resource_calendar.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'resource_calendars', parameterName: 'resource_calendar' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-resource/resource-calendar-pickup-grid-view/resource-calendar-pickup-grid-view.vue'),
                },
                {
                    path: 'res_partners/:res_partner?/pickupview/:pickupview?',
                    meta: {
                        caption: 'entities.res_partner.views.pickupview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'res_partners', parameterName: 'res_partner' },
                            { pathName: 'pickupview', parameterName: 'pickupview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-partner-pickup-view/res-partner-pickup-view.vue'),
                },
                {
                    path: 'account_analytic_lines/:account_analytic_line?/line/:line?',
                    meta: {
                        caption: 'entities.account_analytic_line.views.line.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'account_analytic_lines', parameterName: 'account_analytic_line' },
                            { pathName: 'line', parameterName: 'line' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/account-analytic-line-line/account-analytic-line-line.vue'),
                },
                {
                    path: 'mail_messages/:mail_message?/mastertabview/:mastertabview?',
                    meta: {
                        caption: 'entities.mail_message.views.mastertabview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'mail_messages', parameterName: 'mail_message' },
                            { pathName: 'mastertabview', parameterName: 'mastertabview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-message-master-tab-view/mail-message-master-tab-view.vue'),
                },
                {
                    path: 'hr_skill_types/:hr_skill_type?/basiceditview/:basiceditview?',
                    meta: {
                        caption: 'entities.hr_skill_type.views.basiceditview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_skill_types', parameterName: 'hr_skill_type' },
                            { pathName: 'basiceditview', parameterName: 'basiceditview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-skill-type-basic-edit-view/hr-skill-type-basic-edit-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/treeexpview/:treeexpview?',
                    meta: {
                        caption: 'entities.hr_employee.views.treeexpview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'treeexpview', parameterName: 'treeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-employee-tree-exp-view/hr-employee-tree-exp-view.vue'),
                },
                {
                    path: 'mail_messages/:mail_message?/byreslistview/:byreslistview?',
                    meta: {
                        caption: 'entities.mail_message.views.byreslistview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'mail_messages', parameterName: 'mail_message' },
                            { pathName: 'byreslistview', parameterName: 'byreslistview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-message-by-res-list-view/mail-message-by-res-list-view.vue'),
                },
                {
                    path: 'res_companies/:res_company?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.res_company.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'res_companies', parameterName: 'res_company' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-base/res-company-pickup-grid-view/res-company-pickup-grid-view.vue'),
                },
                {
                    path: 'hr_employees/:hr_employee?/mastertabinfoview/:mastertabinfoview?',
                    meta: {
                        caption: 'entities.hr_employee.views.mastertabinfoview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employees', parameterName: 'hr_employee' },
                            { pathName: 'mastertabinfoview', parameterName: 'mastertabinfoview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-employee-master-tab-info-view/hr-employee-master-tab-info-view.vue'),
                },
                {
                    path: 'hr_skill_types/:hr_skill_type?/basiclistexpview/:basiclistexpview?',
                    meta: {
                        caption: 'entities.hr_skill_type.views.basiclistexpview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_skill_types', parameterName: 'hr_skill_type' },
                            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-skill-type-basic-list-exp-view/hr-skill-type-basic-list-exp-view.vue'),
                },
                {
                    path: 'hr_jobs/:hr_job?/treeexpview/:treeexpview?',
                    meta: {
                        caption: 'entities.hr_job.views.treeexpview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_jobs', parameterName: 'hr_job' },
                            { pathName: 'treeexpview', parameterName: 'treeexpview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-job-tree-exp-view/hr-job-tree-exp-view.vue'),
                },
                {
                    path: 'mail_messages/:mail_message?/sendview/:sendview?',
                    meta: {
                        caption: 'entities.mail_message.views.sendview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'mail_messages', parameterName: 'mail_message' },
                            { pathName: 'sendview', parameterName: 'sendview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-mail/mail-message-send-view/mail-message-send-view.vue'),
                },
                {
                    path: 'survey_surveys/:survey_survey?/pickupgridview/:pickupgridview?',
                    meta: {
                        caption: 'entities.survey_survey.views.pickupgridview.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'survey_surveys', parameterName: 'survey_survey' },
                            { pathName: 'pickupgridview', parameterName: 'pickupgridview' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-survey/survey-survey-pickup-grid-view/survey-survey-pickup-grid-view.vue'),
                },
                {
                    path: 'hr_employee_categories/:hr_employee_category?/lineedit/:lineedit?',
                    meta: {
                        caption: 'entities.hr_employee_category.views.lineedit.caption',
                        info:'',
                        parameters: [
                            { pathName: 'hrindexview', parameterName: 'hrindexview' },
                            { pathName: 'hr_employee_categories', parameterName: 'hr_employee_category' },
                            { pathName: 'lineedit', parameterName: 'lineedit' },
                        ],
                        requireAuth: true,
                    },
                    component: () => import('@pages/odoo-hr/hr-employee-category-line-edit/hr-employee-category-line-edit.vue'),
                },
            ...indexRoutes,
            ],
        },
        ...globalRoutes,
        {
            path: '/login/:login?',
            name: 'login',
            meta: {  
                caption: '登录',
                viewType: 'login',
                requireAuth: false,
                ignoreAddPage: true,
            },
            beforeEnter: (to: any, from: any, next: any) => {
                appService.navHistory.reset();
                next();
            },
            component: () => import('@components/login/login'),
        },
        {
            path: '/403/:p403?',
            name: '403',
            component: () => import('@components/403/403.vue')
        },
        {
            path: '/404/:p404?',
            name: '404',
            component: () => import('@components/404/404.vue')
        },
        {
            path: '/500/:p500',
            name: '500',
            component: () => import('@components/500/500.vue')
        },
        {
            path: '*',
            redirect: 'hrindexview'
        }
    ]
});

router.beforeEach((to: any, from: any, next: any) => {
    if (to.meta && !to.meta.ignoreAddPage) {
        appService.navHistory.add(to);
    }
    next();
});

// 解决路由跳转时报 => 路由重复
const originalPush = Router.prototype.push
Router.prototype.push = function push(location: any) {
    let result: any = originalPush.call(this, location);
    return result.catch((err: any) => err);
}

export default router;