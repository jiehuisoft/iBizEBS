import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import Resource_calendarService from '@/service/resource-calendar/resource-calendar-service';
import Resource_calendarAuthService from '@/authservice/resource-calendar/resource-calendar-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import Resource_calendarUIService from '@/uiservice/resource-calendar/resource-calendar-ui-service';

/**
 * 资源工作时间数据选择视图视图基类
 *
 * @export
 * @class Resource_calendarPickupViewBase
 * @extends {PickupViewBase}
 */
export class Resource_calendarPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Resource_calendarPickupViewBase
     */
    protected appDeName: string = 'resource_calendar';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Resource_calendarPickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Resource_calendarPickupViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Resource_calendarService}
     * @memberof Resource_calendarPickupViewBase
     */
    protected appEntityService: Resource_calendarService = new Resource_calendarService;

    /**
     * 实体权限服务对象
     *
     * @type Resource_calendarUIService
     * @memberof Resource_calendarPickupViewBase
     */
    public appUIService: Resource_calendarUIService = new Resource_calendarUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Resource_calendarPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.resource_calendar.views.pickupview.caption',
        srfTitle: 'entities.resource_calendar.views.pickupview.title',
        srfSubTitle: 'entities.resource_calendar.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Resource_calendarPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Resource_calendarPickupViewBase
     */
	protected viewtag: string = '9634daea422961cc68105c0284fcd66d';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Resource_calendarPickupViewBase
     */ 
    protected viewName:string = "resource_calendarPickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Resource_calendarPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Resource_calendarPickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Resource_calendarPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'resource_calendar',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_calendarPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_calendarPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_calendarPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}