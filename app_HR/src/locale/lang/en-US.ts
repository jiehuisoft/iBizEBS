import enUSUser from '../user/en-US.user';
import { Util } from '@/utils/util/util';
import account_analytic_line_en_US from '@locale/lanres/entities/account-analytic-line/account-analytic-line_en_US';
import hr_employee_en_US from '@locale/lanres/entities/hr-employee/hr-employee_en_US';
import res_currency_en_US from '@locale/lanres/entities/res-currency/res-currency_en_US';
import hr_job_en_US from '@locale/lanres/entities/hr-job/hr-job_en_US';
import mail_followers_en_US from '@locale/lanres/entities/mail-followers/mail-followers_en_US';
import account_move_line_en_US from '@locale/lanres/entities/account-move-line/account-move-line_en_US';
import mail_activity_en_US from '@locale/lanres/entities/mail-activity/mail-activity_en_US';
import hr_skill_type_en_US from '@locale/lanres/entities/hr-skill-type/hr-skill-type_en_US';
import account_analytic_group_en_US from '@locale/lanres/entities/account-analytic-group/account-analytic-group_en_US';
import hr_resume_line_en_US from '@locale/lanres/entities/hr-resume-line/hr-resume-line_en_US';
import hr_skill_en_US from '@locale/lanres/entities/hr-skill/hr-skill_en_US';
import res_company_en_US from '@locale/lanres/entities/res-company/res-company_en_US';
import hr_recruitment_source_en_US from '@locale/lanres/entities/hr-recruitment-source/hr-recruitment-source_en_US';
import hr_leave_en_US from '@locale/lanres/entities/hr-leave/hr-leave_en_US';
import res_users_en_US from '@locale/lanres/entities/res-users/res-users_en_US';
import survey_survey_en_US from '@locale/lanres/entities/survey-survey/survey-survey_en_US';
import account_analytic_account_en_US from '@locale/lanres/entities/account-analytic-account/account-analytic-account_en_US';
import ir_attachment_en_US from '@locale/lanres/entities/ir-attachment/ir-attachment_en_US';
import hr_skill_level_en_US from '@locale/lanres/entities/hr-skill-level/hr-skill-level_en_US';
import hr_department_en_US from '@locale/lanres/entities/hr-department/hr-department_en_US';
import gamification_goal_en_US from '@locale/lanres/entities/gamification-goal/gamification-goal_en_US';
import hr_contract_en_US from '@locale/lanres/entities/hr-contract/hr-contract_en_US';
import hr_employee_category_en_US from '@locale/lanres/entities/hr-employee-category/hr-employee-category_en_US';
import resource_calendar_en_US from '@locale/lanres/entities/resource-calendar/resource-calendar_en_US';
import mail_message_en_US from '@locale/lanres/entities/mail-message/mail-message_en_US';
import hr_contract_type_en_US from '@locale/lanres/entities/hr-contract-type/hr-contract-type_en_US';
import res_config_settings_en_US from '@locale/lanres/entities/res-config-settings/res-config-settings_en_US';
import product_product_en_US from '@locale/lanres/entities/product-product/product-product_en_US';
import hr_resume_line_type_en_US from '@locale/lanres/entities/hr-resume-line-type/hr-resume-line-type_en_US';
import gamification_challenge_en_US from '@locale/lanres/entities/gamification-challenge/gamification-challenge_en_US';
import mail_tracking_value_en_US from '@locale/lanres/entities/mail-tracking-value/mail-tracking-value_en_US';
import product_category_en_US from '@locale/lanres/entities/product-category/product-category_en_US';
import res_partner_en_US from '@locale/lanres/entities/res-partner/res-partner_en_US';
import sale_order_line_en_US from '@locale/lanres/entities/sale-order-line/sale-order-line_en_US';
import resource_resource_en_US from '@locale/lanres/entities/resource-resource/resource-resource_en_US';
import gamification_badge_en_US from '@locale/lanres/entities/gamification-badge/gamification-badge_en_US';
import hr_leave_type_en_US from '@locale/lanres/entities/hr-leave-type/hr-leave-type_en_US';
import maintenance_equipment_en_US from '@locale/lanres/entities/maintenance-equipment/maintenance-equipment_en_US';
import account_account_en_US from '@locale/lanres/entities/account-account/account-account_en_US';
import components_en_US from '@locale/lanres/components/components_en_US';
import codelist_en_US from '@locale/lanres/codelist/codelist_en_US';
import userCustom_en_US from '@locale/lanres/userCustom/userCustom_en_US';

const data: any = {
    app: {
        commonWords:{
            error: "Error",
            success: "Success",
            ok: "OK",
            cancel: "Cancel",
            save: "Save",
            codeNotExist: 'Code list does not exist',
            reqException: "Request exception",
            sysException: "System abnormality",
            warning: "Warning",
            wrong: "Error",
            rulesException: "Abnormal value check rule",
            saveSuccess: "Saved successfully",
            saveFailed: "Save failed",
            deleteSuccess: "Successfully deleted!",
            deleteError: "Failed to delete",
            delDataFail: "Failed to delete data",
            noData: "No data",
            startsuccess:"Start successful",
            createFailed: 'Unable to create',
            isExist: 'existed'
        },
        local:{
            new: "New",
            add: "Add",
        },
        gridpage: {
            choicecolumns: "Choice columns",
            refresh: "refresh",
            show: "Show",
            records: "records",
            totle: "totle",
            noData: "No data",
            valueVail: "Value cannot be empty",
            notConfig: {
                fetchAction: "The view table fetchaction parameter is not configured",
                removeAction: "The view table removeaction parameter is not configured",
                createAction: "The view table createaction parameter is not configured",
                updateAction: "The view table updateaction parameter is not configured",
                loaddraftAction: "The view table loadtrafaction parameter is not configured",
            },
            data: "Data",
            delDataFail: "Failed to delete data",
            delSuccess: "Delete successfully!",
            confirmDel: "Are you sure you want to delete",
            notRecoverable: "delete will not be recoverable?",
            notBatch: "Batch addition not implemented",
            grid: "Grid",
            exportFail: "Data export failed",
            sum: "Total",
            formitemFailed: "Form item update failed",
        },
        list: {
            notConfig: {
                fetchAction: "View list fetchAction parameter is not configured",
                removeAction: "View table removeAction parameter is not configured",
                createAction: "View list createAction parameter is not configured",
                updateAction: "View list updateAction parameter is not configured",
            },
            confirmDel: "Are you sure you want to delete",
            notRecoverable: "delete will not be recoverable?",
        },
        listExpBar: {
            title: "List navigation bar",
        },
        wfExpBar: {
            title: "Process navigation bar",
        },
        calendarExpBar:{
            title: "Calendar navigation bar",
        },
        treeExpBar: {
            title: "Tree view navigation bar",
        },
        portlet: {
            noExtensions: "No extensions",
        },
        tabpage: {
            sureclosetip: {
                title: "Close warning",
                content: "Form data Changed, are sure close?",
            },
            closeall: "Close all",
            closeother: "Close other",
        },
        fileUpload: {
            caption: "Upload",
        },
        searchButton: {
            search: "Search",
            reset: "Reset",
        },
        calendar:{
          today: "today",
          month: "month",
          week: "week",
          day: "day",
          list: "list",
          dateSelectModalTitle: "select the time you wanted",
          gotoDate: "goto",
          from: "From",
          to: "To",
        },
        // 非实体视图
        views: {
            hrindexview: {
                caption: "员工",
                title: "员工首页视图",
            },
        },
        utilview:{
            importview:"Import Data",
            warning:"warning",
            info:"Please configure the data import item"    
        },
        menus: {
            hrindexview: {
                user_menus: "用户菜单",
                top_menus: "顶部菜单",
                left_exp: "左侧菜单",
                menuitem1: "员工",
                menuitem4: "员工",
                menuitem5: "合同",
                menuitem6: "工作岗位",
                menuitem3: "配置",
                menuitem2: "设置",
                menuitem14: "员工",
                menuitem15: "标签",
                menuitem16: "技能",
                menuitem7: "部门",
                menuitem17: "简历",
                menuitem18: "类型",
                menuitem8: "活动规划",
                menuitem19: "规划类型",
                menuitem10: "计划",
                menuitem9: "挑战",
                menuitem11: "徽章",
                menuitem12: "挑战",
                menuitem13: "历史目标",
                bottom_exp: "底部内容",
                footer_left: "底部左侧",
                footer_center: "底部中间",
                footer_right: "底部右侧",
            },
        },
        formpage:{
            error: "Error",
            desc1: "Operation failed, failed to find current form item",
            desc2: "Can't continue",
            notconfig: {
                loadaction: "View form loadAction parameter is not configured",
                loaddraftaction: "View form loaddraftAction parameter is not configured",
                actionname: "View form actionName parameter is not configured",
                removeaction: "View form removeAction parameter is not configured",
            },
            saveerror: "Error saving data",
            savecontent: "The data is inconsistent. The background data may have been modified. Do you want to reload the data?",
            valuecheckex: "Value rule check exception",
            savesuccess: "Saved successfully!",
            deletesuccess: "Successfully deleted!",  
            workflow: {
                starterror: "Workflow started successfully",
                startsuccess: "Workflow failed to start",
                submiterror: "Workflow submission failed",
                submitsuccess: "Workflow submitted successfully",
            },
            updateerror: "Form item update failed",       
        },
        gridBar: {
            title: "Table navigation bar",
        },
        multiEditView: {
            notConfig: {
                fetchAction: "View multi-edit view panel fetchAction parameter is not configured",
                loaddraftAction: "View multi-edit view panel loaddraftAction parameter is not configured",
            },
        },
        dataViewExpBar: {
            title: "Card view navigation bar",
        },
        kanban: {
            notConfig: {
                fetchAction: "View list fetchAction parameter is not configured",
                removeAction: "View table removeAction parameter is not configured",
            },
            delete1: "Confirm to delete ",
            delete2: "the delete operation will be unrecoverable!",
        },
        dashBoard: {
            handleClick: {
                title: "Panel design",
            },
        },
        dataView: {
            sum: "total",
            data: "data",
        },
        chart: {
            undefined: "Undefined",
            quarter: "Quarter",   
            year: "Year",
        },
        searchForm: {
            notConfig: {
                loadAction: "View search form loadAction parameter is not configured",
                loaddraftAction: "View search form loaddraftAction parameter is not configured",
            },
            custom: "Store custom queries",
            title: "Name",
        },
        wizardPanel: {
            back: "Back",
            next: "Next",
            complete: "Complete",
            preactionmessage:"The calculation of the previous behavior is not configured"
        },
        viewLayoutPanel: {
            appLogoutView: {
                prompt1: "Dear customer, you have successfully exited the system, after",
                prompt2: "seconds, we will jump to the",
                logingPage: "login page",
            },
            appWfstepTraceView: {
                title: "Application process processing record view",
            },
            appWfstepDataView: {
                title: "Application process tracking view",
            },
            appLoginView: {
                username: "Username",
                password: "Password",
                login: "Login",
            },
        },
    },
    form: {
        group: {
            show_more: "Show More",
            hidden_more: "Hide More"
        }
    },
    entities: {
        account_analytic_line: account_analytic_line_en_US,
        hr_employee: hr_employee_en_US,
        res_currency: res_currency_en_US,
        hr_job: hr_job_en_US,
        mail_followers: mail_followers_en_US,
        account_move_line: account_move_line_en_US,
        mail_activity: mail_activity_en_US,
        hr_skill_type: hr_skill_type_en_US,
        account_analytic_group: account_analytic_group_en_US,
        hr_resume_line: hr_resume_line_en_US,
        hr_skill: hr_skill_en_US,
        res_company: res_company_en_US,
        hr_recruitment_source: hr_recruitment_source_en_US,
        hr_leave: hr_leave_en_US,
        res_users: res_users_en_US,
        survey_survey: survey_survey_en_US,
        account_analytic_account: account_analytic_account_en_US,
        ir_attachment: ir_attachment_en_US,
        hr_skill_level: hr_skill_level_en_US,
        hr_department: hr_department_en_US,
        gamification_goal: gamification_goal_en_US,
        hr_contract: hr_contract_en_US,
        hr_employee_category: hr_employee_category_en_US,
        resource_calendar: resource_calendar_en_US,
        mail_message: mail_message_en_US,
        hr_contract_type: hr_contract_type_en_US,
        res_config_settings: res_config_settings_en_US,
        product_product: product_product_en_US,
        hr_resume_line_type: hr_resume_line_type_en_US,
        gamification_challenge: gamification_challenge_en_US,
        mail_tracking_value: mail_tracking_value_en_US,
        product_category: product_category_en_US,
        res_partner: res_partner_en_US,
        sale_order_line: sale_order_line_en_US,
        resource_resource: resource_resource_en_US,
        gamification_badge: gamification_badge_en_US,
        hr_leave_type: hr_leave_type_en_US,
        maintenance_equipment: maintenance_equipment_en_US,
        account_account: account_account_en_US,
    },
    components: components_en_US,
    codelist: codelist_en_US,
    userCustom: userCustom_en_US,
};
// 合并用户自定义多语言
Util.mergeDeepObject(data, enUSUser);
// 默认导出
export default data;