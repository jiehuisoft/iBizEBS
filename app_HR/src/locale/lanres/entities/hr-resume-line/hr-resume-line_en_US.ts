export default {
  fields: {
    id: "ID",
    write_date: "最后更新时间",
    name: "名称",
    create_date: "创建时间",
    date_start: "开始日期",
    date_end: "结束日期",
    display_type: "显示类型",
    line_type_id_text: "类型",
    employee_id_text: "员工",
    employee_id: "员工",
    create_uid: "ID",
    line_type_id: "类型",
    write_uid: "ID",
  },
	views: {
		line: {
			caption: "员工简历行",
      		title: "行编辑表格视图",
		},
		lineedit: {
			caption: "员工简历行",
      		title: "行编辑表格视图",
		},
		editview: {
			caption: "员工简历行",
      		title: "员工简历行编辑视图",
		},
	},
	main_form: {
		details: {
			group1: "员工简历行基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	lineedit_grid: {
		nodata: "",
		columns: {
			name: "名称",
			display_type: "显示类型",
			line_type_id_text: "类型",
			date_start: "开始日期",
			date_end: "结束日期",
			employee_id: "员工",
			line_type_id: "类型",
			write_uid: "ID",
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
};