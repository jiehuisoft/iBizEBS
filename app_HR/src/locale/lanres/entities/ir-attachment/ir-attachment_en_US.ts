export default {
  fields: {
    res_field: "res_field",
    file_size: "file_size",
    store_fname: "store_fname",
    ibizpublic: "public",
    type: "type",
    res_id: "res_id",
    checksum: "checksum",
    res_model: "res_model",
    description: "description",
    access_token: "access_token",
    url: "url",
    id: "ID",
    key: "key",
    name: "名称",
    mimetype: "mimetype",
    index_content: "index_content",
    company_id: "公司",
  },
	views: {
		byresdataview: {
			caption: "附件",
      		title: "附件数据视图",
		},
	},
	card_dataview: {
		nodata: "",
		uiactions: {
		},
	},
};