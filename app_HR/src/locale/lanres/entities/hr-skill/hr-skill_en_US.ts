export default {
  fields: {
    create_date: "创建时间",
    write_date: "最后更新时间",
    id: "ID",
    name: "名称",
    skill_type_name: "类型",
    create_uid: "ID",
    write_uid: "ID",
    skill_type_id: "技能类型",
  },
	views: {
		editview: {
			caption: "技能",
      		title: "技能编辑视图",
		},
		lineedit: {
			caption: "技能",
      		title: "行编辑表格视图",
		},
	},
	main_form: {
		details: {
			group1: "技能基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "名称", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	lineedit_grid: {
		nodata: "",
		columns: {
			id: "ID",
			name: "名称",
			skill_type_id: "技能类型",
			skill_type_name: "类型",
		},
		uiactions: {
		},
	},
	lineedittoolbar_toolbar: {
		tbitem24: {
			caption: "行编辑",
			tip: "行编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem25: {
			caption: "新建行",
			tip: "新建行",
		},
		tbitem26: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "保存行",
			tip: "保存行",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem11: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
};