import Hr_leave_typeAuthServiceBase from './hr-leave-type-auth-service-base';


/**
 * 休假类型权限服务对象
 *
 * @export
 * @class Hr_leave_typeAuthService
 * @extends {Hr_leave_typeAuthServiceBase}
 */
export default class Hr_leave_typeAuthService extends Hr_leave_typeAuthServiceBase {

    /**
     * Creates an instance of  Hr_leave_typeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_leave_typeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}