import Hr_skill_levelAuthServiceBase from './hr-skill-level-auth-service-base';


/**
 * 技能等级权限服务对象
 *
 * @export
 * @class Hr_skill_levelAuthService
 * @extends {Hr_skill_levelAuthServiceBase}
 */
export default class Hr_skill_levelAuthService extends Hr_skill_levelAuthServiceBase {

    /**
     * Creates an instance of  Hr_skill_levelAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_skill_levelAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}