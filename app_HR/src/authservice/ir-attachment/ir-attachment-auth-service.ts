import Ir_attachmentAuthServiceBase from './ir-attachment-auth-service-base';


/**
 * 附件权限服务对象
 *
 * @export
 * @class Ir_attachmentAuthService
 * @extends {Ir_attachmentAuthServiceBase}
 */
export default class Ir_attachmentAuthService extends Ir_attachmentAuthServiceBase {

    /**
     * Creates an instance of  Ir_attachmentAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Ir_attachmentAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}