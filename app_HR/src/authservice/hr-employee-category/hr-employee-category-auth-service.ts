import Hr_employee_categoryAuthServiceBase from './hr-employee-category-auth-service-base';


/**
 * 员工类别权限服务对象
 *
 * @export
 * @class Hr_employee_categoryAuthService
 * @extends {Hr_employee_categoryAuthServiceBase}
 */
export default class Hr_employee_categoryAuthService extends Hr_employee_categoryAuthServiceBase {

    /**
     * Creates an instance of  Hr_employee_categoryAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_employee_categoryAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}