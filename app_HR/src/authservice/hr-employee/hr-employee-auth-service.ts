import Hr_employeeAuthServiceBase from './hr-employee-auth-service-base';


/**
 * 员工权限服务对象
 *
 * @export
 * @class Hr_employeeAuthService
 * @extends {Hr_employeeAuthServiceBase}
 */
export default class Hr_employeeAuthService extends Hr_employeeAuthServiceBase {

    /**
     * Creates an instance of  Hr_employeeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_employeeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}