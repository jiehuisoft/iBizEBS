import Account_accountAuthServiceBase from './account-account-auth-service-base';


/**
 * 科目权限服务对象
 *
 * @export
 * @class Account_accountAuthService
 * @extends {Account_accountAuthServiceBase}
 */
export default class Account_accountAuthService extends Account_accountAuthServiceBase {

    /**
     * Creates an instance of  Account_accountAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_accountAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}