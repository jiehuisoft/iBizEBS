import Account_move_lineAuthServiceBase from './account-move-line-auth-service-base';


/**
 * 日记账项目权限服务对象
 *
 * @export
 * @class Account_move_lineAuthService
 * @extends {Account_move_lineAuthServiceBase}
 */
export default class Account_move_lineAuthService extends Account_move_lineAuthServiceBase {

    /**
     * Creates an instance of  Account_move_lineAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_move_lineAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}