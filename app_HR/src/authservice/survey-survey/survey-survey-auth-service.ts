import Survey_surveyAuthServiceBase from './survey-survey-auth-service-base';


/**
 * 问卷权限服务对象
 *
 * @export
 * @class Survey_surveyAuthService
 * @extends {Survey_surveyAuthServiceBase}
 */
export default class Survey_surveyAuthService extends Survey_surveyAuthServiceBase {

    /**
     * Creates an instance of  Survey_surveyAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Survey_surveyAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}