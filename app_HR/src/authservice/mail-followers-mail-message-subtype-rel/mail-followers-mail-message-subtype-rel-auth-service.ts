import Mail_followers_mail_message_subtype_relAuthServiceBase from './mail-followers-mail-message-subtype-rel-auth-service-base';


/**
 * 关注消息类型权限服务对象
 *
 * @export
 * @class Mail_followers_mail_message_subtype_relAuthService
 * @extends {Mail_followers_mail_message_subtype_relAuthServiceBase}
 */
export default class Mail_followers_mail_message_subtype_relAuthService extends Mail_followers_mail_message_subtype_relAuthServiceBase {

    /**
     * Creates an instance of  Mail_followers_mail_message_subtype_relAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_followers_mail_message_subtype_relAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}