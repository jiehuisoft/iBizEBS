/**
 * 实体权限服务注册中心
 *
 * @export
 * @class AuthServiceRegister
 */
export class AuthServiceRegister {

    /**
     * 所有实体权限服务Map
     *
     * @protected
     * @type {*}
     * @memberof AuthServiceRegister
     */
    protected allAuthService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载实体权限服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof AuthServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of AuthServiceRegister.
     * @memberof AuthServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof AuthServiceRegister
     */
    protected init(): void {
                this.allAuthService.set('account_analytic_line', () => import('@/authservice/account-analytic-line/account-analytic-line-auth-service'));
        this.allAuthService.set('hr_employee', () => import('@/authservice/hr-employee/hr-employee-auth-service'));
        this.allAuthService.set('res_currency', () => import('@/authservice/res-currency/res-currency-auth-service'));
        this.allAuthService.set('hr_job', () => import('@/authservice/hr-job/hr-job-auth-service'));
        this.allAuthService.set('mail_followers', () => import('@/authservice/mail-followers/mail-followers-auth-service'));
        this.allAuthService.set('account_move_line', () => import('@/authservice/account-move-line/account-move-line-auth-service'));
        this.allAuthService.set('mail_activity', () => import('@/authservice/mail-activity/mail-activity-auth-service'));
        this.allAuthService.set('hr_skill_type', () => import('@/authservice/hr-skill-type/hr-skill-type-auth-service'));
        this.allAuthService.set('account_analytic_group', () => import('@/authservice/account-analytic-group/account-analytic-group-auth-service'));
        this.allAuthService.set('hr_resume_line', () => import('@/authservice/hr-resume-line/hr-resume-line-auth-service'));
        this.allAuthService.set('hr_skill', () => import('@/authservice/hr-skill/hr-skill-auth-service'));
        this.allAuthService.set('res_company', () => import('@/authservice/res-company/res-company-auth-service'));
        this.allAuthService.set('hr_recruitment_source', () => import('@/authservice/hr-recruitment-source/hr-recruitment-source-auth-service'));
        this.allAuthService.set('hr_leave', () => import('@/authservice/hr-leave/hr-leave-auth-service'));
        this.allAuthService.set('res_users', () => import('@/authservice/res-users/res-users-auth-service'));
        this.allAuthService.set('survey_survey', () => import('@/authservice/survey-survey/survey-survey-auth-service'));
        this.allAuthService.set('account_analytic_account', () => import('@/authservice/account-analytic-account/account-analytic-account-auth-service'));
        this.allAuthService.set('ir_attachment', () => import('@/authservice/ir-attachment/ir-attachment-auth-service'));
        this.allAuthService.set('hr_skill_level', () => import('@/authservice/hr-skill-level/hr-skill-level-auth-service'));
        this.allAuthService.set('hr_department', () => import('@/authservice/hr-department/hr-department-auth-service'));
        this.allAuthService.set('gamification_goal', () => import('@/authservice/gamification-goal/gamification-goal-auth-service'));
        this.allAuthService.set('hr_contract', () => import('@/authservice/hr-contract/hr-contract-auth-service'));
        this.allAuthService.set('hr_employee_category', () => import('@/authservice/hr-employee-category/hr-employee-category-auth-service'));
        this.allAuthService.set('resource_calendar', () => import('@/authservice/resource-calendar/resource-calendar-auth-service'));
        this.allAuthService.set('mail_message', () => import('@/authservice/mail-message/mail-message-auth-service'));
        this.allAuthService.set('hr_contract_type', () => import('@/authservice/hr-contract-type/hr-contract-type-auth-service'));
        this.allAuthService.set('res_config_settings', () => import('@/authservice/res-config-settings/res-config-settings-auth-service'));
        this.allAuthService.set('product_product', () => import('@/authservice/product-product/product-product-auth-service'));
        this.allAuthService.set('hr_resume_line_type', () => import('@/authservice/hr-resume-line-type/hr-resume-line-type-auth-service'));
        this.allAuthService.set('gamification_challenge', () => import('@/authservice/gamification-challenge/gamification-challenge-auth-service'));
        this.allAuthService.set('mail_tracking_value', () => import('@/authservice/mail-tracking-value/mail-tracking-value-auth-service'));
        this.allAuthService.set('product_category', () => import('@/authservice/product-category/product-category-auth-service'));
        this.allAuthService.set('res_partner', () => import('@/authservice/res-partner/res-partner-auth-service'));
        this.allAuthService.set('sale_order_line', () => import('@/authservice/sale-order-line/sale-order-line-auth-service'));
        this.allAuthService.set('resource_resource', () => import('@/authservice/resource-resource/resource-resource-auth-service'));
        this.allAuthService.set('gamification_badge', () => import('@/authservice/gamification-badge/gamification-badge-auth-service'));
        this.allAuthService.set('hr_leave_type', () => import('@/authservice/hr-leave-type/hr-leave-type-auth-service'));
        this.allAuthService.set('maintenance_equipment', () => import('@/authservice/maintenance-equipment/maintenance-equipment-auth-service'));
        this.allAuthService.set('account_account', () => import('@/authservice/account-account/account-account-auth-service'));
    }

    /**
     * 加载实体权限服务
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof AuthServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allAuthService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体权限服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof AuthServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const authService: any = await this.loadService(name);
        if (authService && authService.default) {
            const instance: any = new authService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const authServiceRegister: AuthServiceRegister = new AuthServiceRegister();