import Sale_order_lineAuthServiceBase from './sale-order-line-auth-service-base';


/**
 * 销售订单行权限服务对象
 *
 * @export
 * @class Sale_order_lineAuthService
 * @extends {Sale_order_lineAuthServiceBase}
 */
export default class Sale_order_lineAuthService extends Sale_order_lineAuthServiceBase {

    /**
     * Creates an instance of  Sale_order_lineAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Sale_order_lineAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}