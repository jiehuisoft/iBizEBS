/**
 * EF_MasterQuick 部件模型
 *
 * @export
 * @class EF_MasterQuickModel
 */
export default class EF_MasterQuickModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_MasterQuickModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'job_id',
        prop: 'job_id',
        dataType: 'PICKUP',
      },
      {
        name: 'job_id_text',
        prop: 'job_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'mobile_phone',
        prop: 'mobile_phone',
        dataType: 'TEXT',
      },
      {
        name: 'work_phone',
        prop: 'work_phone',
        dataType: 'TEXT',
      },
      {
        name: 'work_email',
        prop: 'work_email',
        dataType: 'TEXT',
      },
      {
        name: 'work_location',
        prop: 'work_location',
        dataType: 'TEXT',
      },
      {
        name: 'company_id',
        prop: 'company_id',
        dataType: 'PICKUP',
      },
      {
        name: 'company_id_text',
        prop: 'company_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'department_id',
        prop: 'department_id',
        dataType: 'PICKUP',
      },
      {
        name: 'department_id_text',
        prop: 'department_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'parent_id',
        prop: 'parent_id',
        dataType: 'PICKUP',
      },
      {
        name: 'parent_id_text',
        prop: 'parent_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'resource_id',
        prop: 'resource_id',
        dataType: 'PICKUP',
      },
      {
        name: 'hr_employee',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}