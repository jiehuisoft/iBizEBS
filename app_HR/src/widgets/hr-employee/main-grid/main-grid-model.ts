/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'name',
          prop: 'name',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'expense_manager_id',
          prop: 'expense_manager_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'resource_calendar_id',
          prop: 'resource_calendar_id',
          dataType: 'PICKUP',
        },
        {
          name: 'last_attendance_id',
          prop: 'last_attendance_id',
          dataType: 'PICKUP',
        },
        {
          name: 'department_id',
          prop: 'department_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'country_id',
          prop: 'country_id',
          dataType: 'PICKUP',
        },
        {
          name: 'leave_manager_id',
          prop: 'leave_manager_id',
          dataType: 'PICKUP',
        },
        {
          name: 'bank_account_id',
          prop: 'bank_account_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'coach_id',
          prop: 'coach_id',
          dataType: 'PICKUP',
        },
        {
          name: 'resource_id',
          prop: 'resource_id',
          dataType: 'PICKUP',
        },
        {
          name: 'address_home_id',
          prop: 'address_home_id',
          dataType: 'PICKUP',
        },
        {
          name: 'address_id',
          prop: 'address_id',
          dataType: 'PICKUP',
        },
        {
          name: 'user_id',
          prop: 'user_id',
          dataType: 'PICKUP',
        },
        {
          name: 'country_of_birth',
          prop: 'country_of_birth',
          dataType: 'PICKUP',
        },
        {
          name: 'parent_id',
          prop: 'parent_id',
          dataType: 'PICKUP',
        },
        {
          name: 'job_id',
          prop: 'job_id',
          dataType: 'PICKUP',
        },
        {
          name: 'hr_employee',
          prop: 'id',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}