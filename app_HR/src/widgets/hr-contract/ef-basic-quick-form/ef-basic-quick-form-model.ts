/**
 * EF_BasicQuick 部件模型
 *
 * @export
 * @class EF_BasicQuickModel
 */
export default class EF_BasicQuickModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_BasicQuickModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'company_id_text',
        prop: 'company_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'department_id_text',
        prop: 'department_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'employee_id_text',
        prop: 'employee_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'job_id_text',
        prop: 'job_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'company_id',
        prop: 'company_id',
        dataType: 'PICKUP',
      },
      {
        name: 'employee_id',
        prop: 'employee_id',
        dataType: 'PICKUP',
      },
      {
        name: 'department_id',
        prop: 'department_id',
        dataType: 'PICKUP',
      },
      {
        name: 'job_id',
        prop: 'job_id',
        dataType: 'PICKUP',
      },
      {
        name: 'date_start',
        prop: 'date_start',
        dataType: 'DATE',
      },
      {
        name: 'wage',
        prop: 'wage',
        dataType: 'FLOAT',
      },
      {
        name: 'date_end',
        prop: 'date_end',
        dataType: 'DATE',
      },
      {
        name: 'resource_calendar_id_text',
        prop: 'resource_calendar_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'trial_date_end',
        prop: 'trial_date_end',
        dataType: 'DATE',
      },
      {
        name: 'state',
        prop: 'state',
        dataType: 'SSCODELIST',
      },
      {
        name: 'resource_calendar_id',
        prop: 'resource_calendar_id',
        dataType: 'PICKUP',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'hr_contract',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}