/**
 * Master 部件模型
 *
 * @export
 * @class MasterModel
 */
export default class MasterModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MasterGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MasterGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'id',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'name',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'display_name',
          prop: 'display_name',
          dataType: 'TEXT',
        },
        {
          name: 'complete_name',
          prop: 'complete_name',
          dataType: 'TEXT',
        },
        {
          name: 'manager_id',
          prop: 'manager_id',
          dataType: 'PICKUP',
        },
        {
          name: 'manager_id_text',
          prop: 'manager_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'company_id_text',
          prop: 'company_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'parent_id',
          prop: 'parent_id',
          dataType: 'PICKUP',
        },
        {
          name: 'parent_id_text',
          prop: 'parent_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'active',
          prop: 'active',
          dataType: 'TRUEFALSE',
        },
        {
          name: 'note',
          prop: 'note',
          dataType: 'LONGTEXT',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'hr_department',
          prop: 'id',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}