import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, GridControlBase } from '@/studio-core';
import Hr_departmentService from '@/service/hr-department/hr-department-service';
import MasterService from './master-grid-service';
import Hr_departmentUIService from '@/uiservice/hr-department/hr-department-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {MasterGridBase}
 */
export class MasterGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MasterGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {MasterService}
     * @memberof MasterGridBase
     */
    public service: MasterService = new MasterService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Hr_departmentService}
     * @memberof MasterGridBase
     */
    public appEntityService: Hr_departmentService = new Hr_departmentService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MasterGridBase
     */
    protected appDeName: string = 'hr_department';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MasterGridBase
     */
    protected appDeLogicName: string = 'HR 部门';

    /**
     * 界面UI服务对象
     *
     * @type {Hr_departmentUIService}
     * @memberof MasterBase
     */  
    public appUIService:Hr_departmentUIService = new Hr_departmentUIService(this.$store);


    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof MasterBase
     */  
    public ActionModel: any = {
    };

    /**
     * 主信息表格列
     *
     * @type {string}
     * @memberof MasterBase
     */  
    public majorInfoColName:string = "name";

    /**
     * 列主键属性名称
     *
     * @type {string}
     * @memberof MasterGridBase
     */
    public columnKeyName: string = "id";

    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof MasterBase
     */
    protected localStorageTag: string = 'hr_department_master_grid';

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof MasterGridBase
     */
    public allColumns: any[] = [
        {
            name: 'id',
            label: 'ID',
            langtag: 'entities.hr_department.master_grid.columns.id',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'name',
            label: '部门名称',
            langtag: 'entities.hr_department.master_grid.columns.name',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'display_name',
            label: '显示名称',
            langtag: 'entities.hr_department.master_grid.columns.display_name',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'complete_name',
            label: '完整名称',
            langtag: 'entities.hr_department.master_grid.columns.complete_name',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'manager_id',
            label: '经理',
            langtag: 'entities.hr_department.master_grid.columns.manager_id',
            show: false,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'manager_id_text',
            label: '经理',
            langtag: 'entities.hr_department.master_grid.columns.manager_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'company_id',
            label: '公司',
            langtag: 'entities.hr_department.master_grid.columns.company_id',
            show: false,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'company_id_text',
            label: '公司',
            langtag: 'entities.hr_department.master_grid.columns.company_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'parent_id',
            label: '上级部门',
            langtag: 'entities.hr_department.master_grid.columns.parent_id',
            show: false,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'parent_id_text',
            label: '上级部门',
            langtag: 'entities.hr_department.master_grid.columns.parent_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'active',
            label: '有效',
            langtag: 'entities.hr_department.master_grid.columns.active',
            show: true,
            unit: 'STAR',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'note',
            label: '笔记',
            langtag: 'entities.hr_department.master_grid.columns.note',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof MasterGridBase
     */
    public getGridRowModel(){
        return {
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MasterGridBase
     */
    public rules: any = {
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof MasterBase
     */
    public deRules:any = {
    };

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof MasterBase
     */
    public hasRowEdit: any = {
        'id':false,
        'name':false,
        'display_name':false,
        'complete_name':false,
        'manager_id':false,
        'manager_id_text':false,
        'company_id':false,
        'company_id_text':false,
        'parent_id':false,
        'parent_id_text':false,
        'active':false,
        'note':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof MasterBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof MasterGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'active',
                srfkey: 'Odoo_truefalse',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }


    /**
     * 更新默认值
     * @param {*}  row 行数据
     * @memberof MasterBase
     */
    public updateDefault(row: any){                    
    }

    /**
     * 计算数据对象类型的默认值
     * @param {string}  action 行为
     * @param {string}  param 默认值参数
     * @param {*}  data 当前行数据
     * @memberof MasterBase
     */
    public computeDefaultValueWithParam(action:string,param:string,data:any){
        if(Object.is(action,"UPDATE")){
            const nativeData:any = this.service.getCopynativeData();
            if(nativeData && (nativeData instanceof Array) && nativeData.length >0){
                let targetData:any = nativeData.find((item:any) =>{
                    return item.id === data.srfkey;
                })
                if(targetData){
                    return targetData[param]?targetData[param]:null;
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
           return this.service.getRemoteCopyData()[param]?this.service.getRemoteCopyData()[param]:null;
        }
    }


}