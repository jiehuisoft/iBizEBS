import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, GridControlBase } from '@/studio-core';
import Account_analytic_lineService from '@/service/account-analytic-line/account-analytic-line-service';
import LineEditService from './line-edit-grid-service';
import Account_analytic_lineUIService from '@/uiservice/account-analytic-line/account-analytic-line-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {LineEditGridBase}
 */
export class LineEditGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof LineEditGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {LineEditService}
     * @memberof LineEditGridBase
     */
    public service: LineEditService = new LineEditService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Account_analytic_lineService}
     * @memberof LineEditGridBase
     */
    public appEntityService: Account_analytic_lineService = new Account_analytic_lineService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LineEditGridBase
     */
    protected appDeName: string = 'account_analytic_line';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof LineEditGridBase
     */
    protected appDeLogicName: string = '分析行';

    /**
     * 界面UI服务对象
     *
     * @type {Account_analytic_lineUIService}
     * @memberof LineEditBase
     */  
    public appUIService:Account_analytic_lineUIService = new Account_analytic_lineUIService(this.$store);


    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof LineEditBase
     */  
    public ActionModel: any = {
    };

    /**
     * 主信息表格列
     *
     * @type {string}
     * @memberof LineEditBase
     */  
    public majorInfoColName:string = "name";


    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof LineEditBase
     */
    protected localStorageTag: string = 'account_analytic_line_lineedit_grid';

    /**
     * 是否支持分页
     *
     * @type {boolean}
     * @memberof LineEditGridBase
     */
    public isEnablePagingBar: boolean = false;

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof LineEditGridBase
     */
    public allColumns: any[] = [
        {
            name: 'account_id_text',
            label: '分析账户',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.account_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'user_id_text',
            label: '用户',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.user_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'amount',
            label: '金额',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.amount',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'company_id_text',
            label: '公司',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.company_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 0 ,
        },
        {
            name: 'create_uid_text',
            label: '创建人',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.create_uid_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 0 ,
        },
        {
            name: 'create_date',
            label: '创建时间',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.create_date',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 0 ,
        },
        {
            name: 'currency_id_text',
            label: '币种',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.currency_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 0 ,
        },
        {
            name: 'general_account_id_text',
            label: '财务会计',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.general_account_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 0 ,
        },
        {
            name: 'group_id_text',
            label: '组',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.group_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 0 ,
        },
        {
            name: 'move_id_text',
            label: '日记账项目',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.move_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'name',
            label: '说明',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.name',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'partner_id_text',
            label: '业务伙伴',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.partner_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'product_id_text',
            label: '产品',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.product_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'so_line_text',
            label: '销售订单项目',
            langtag: 'entities.account_analytic_line.lineedit_grid.columns.so_line_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof LineEditGridBase
     */
    public getGridRowModel(){
        return {
          create_uid_text: new FormItemModel(),
          company_id: new FormItemModel(),
          product_id: new FormItemModel(),
          currency_id_text: new FormItemModel(),
          create_uid: new FormItemModel(),
          general_account_id: new FormItemModel(),
          product_id_text: new FormItemModel(),
          srfkey: new FormItemModel(),
          amount: new FormItemModel(),
          user_id_text: new FormItemModel(),
          so_line_text: new FormItemModel(),
          create_date: new FormItemModel(),
          name: new FormItemModel(),
          user_id: new FormItemModel(),
          partner_id: new FormItemModel(),
          currency_id: new FormItemModel(),
          account_id: new FormItemModel(),
          group_id: new FormItemModel(),
          so_line: new FormItemModel(),
          group_id_text: new FormItemModel(),
          account_id_text: new FormItemModel(),
          general_account_id_text: new FormItemModel(),
          company_id_text: new FormItemModel(),
          move_id_text: new FormItemModel(),
          move_id: new FormItemModel(),
          partner_id_text: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof LineEditGridBase
     */
    public rules: any = {
        create_uid_text: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '创建人 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '创建人 值不能为空', trigger: 'blur' },
        ],
        company_id: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        product_id: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        currency_id_text: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '币种 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '币种 值不能为空', trigger: 'blur' },
        ],
        create_uid: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        general_account_id: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        product_id_text: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '产品 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '产品 值不能为空', trigger: 'blur' },
        ],
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'blur' },
        ],
        amount: [
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '金额 值不能为空', trigger: 'change' },
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '金额 值不能为空', trigger: 'blur' },
        ],
        user_id_text: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '用户 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '用户 值不能为空', trigger: 'blur' },
        ],
        so_line_text: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '销售订单项目 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '销售订单项目 值不能为空', trigger: 'blur' },
        ],
        create_date: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '创建时间 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '创建时间 值不能为空', trigger: 'blur' },
        ],
        name: [
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '说明 值不能为空', trigger: 'change' },
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '说明 值不能为空', trigger: 'blur' },
        ],
        user_id: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        partner_id: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        currency_id: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        account_id: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        group_id: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        so_line: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        group_id_text: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '组 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '组 值不能为空', trigger: 'blur' },
        ],
        account_id_text: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '分析账户 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '分析账户 值不能为空', trigger: 'blur' },
        ],
        general_account_id_text: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '财务会计 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '财务会计 值不能为空', trigger: 'blur' },
        ],
        company_id_text: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '公司 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '公司 值不能为空', trigger: 'blur' },
        ],
        move_id_text: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '日记账项目 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '日记账项目 值不能为空', trigger: 'blur' },
        ],
        move_id: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        partner_id_text: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '业务伙伴 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '业务伙伴 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof LineEditBase
     */
    public deRules:any = {
    };

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof LineEditBase
     */
    public hasRowEdit: any = {
        'account_id_text':false,
        'user_id_text':false,
        'amount':false,
        'company_id_text':false,
        'create_uid_text':false,
        'create_date':false,
        'currency_id_text':false,
        'general_account_id_text':false,
        'group_id_text':false,
        'move_id_text':false,
        'name':false,
        'partner_id_text':false,
        'product_id_text':false,
        'so_line_text':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof LineEditBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof LineEditGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }


    /**
     * 更新默认值
     * @param {*}  row 行数据
     * @memberof LineEditBase
     */
    public updateDefault(row: any){                    
    }

    /**
     * 计算数据对象类型的默认值
     * @param {string}  action 行为
     * @param {string}  param 默认值参数
     * @param {*}  data 当前行数据
     * @memberof LineEditBase
     */
    public computeDefaultValueWithParam(action:string,param:string,data:any){
        if(Object.is(action,"UPDATE")){
            const nativeData:any = this.service.getCopynativeData();
            if(nativeData && (nativeData instanceof Array) && nativeData.length >0){
                let targetData:any = nativeData.find((item:any) =>{
                    return item.id === data.srfkey;
                })
                if(targetData){
                    return targetData[param]?targetData[param]:null;
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
           return this.service.getRemoteCopyData()[param]?this.service.getRemoteCopyData()[param]:null;
        }
    }


}