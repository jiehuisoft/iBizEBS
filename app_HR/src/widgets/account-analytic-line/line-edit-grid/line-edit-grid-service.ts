import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Account_analytic_lineService from '@/service/account-analytic-line/account-analytic-line-service';
import LineEditModel from './line-edit-grid-model';
import Res_usersService from '@/service/res-users/res-users-service';
import Res_currencyService from '@/service/res-currency/res-currency-service';
import Product_productService from '@/service/product-product/product-product-service';
import Sale_order_lineService from '@/service/sale-order-line/sale-order-line-service';
import Account_analytic_groupService from '@/service/account-analytic-group/account-analytic-group-service';
import Account_analytic_accountService from '@/service/account-analytic-account/account-analytic-account-service';
import Account_accountService from '@/service/account-account/account-account-service';
import Res_companyService from '@/service/res-company/res-company-service';
import Account_move_lineService from '@/service/account-move-line/account-move-line-service';
import Res_partnerService from '@/service/res-partner/res-partner-service';


/**
 * LineEdit 部件服务对象
 *
 * @export
 * @class LineEditService
 */
export default class LineEditService extends ControlService {

    /**
     * 分析行服务对象
     *
     * @type {Account_analytic_lineService}
     * @memberof LineEditService
     */
    public appEntityService: Account_analytic_lineService = new Account_analytic_lineService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof LineEditService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of LineEditService.
     * 
     * @param {*} [opts={}]
     * @memberof LineEditService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new LineEditModel();
    }

    /**
     * 备份原生数据
     *
     * @type {*}
     * @memberof LineEditService
     */
    private copynativeData:any;

    /**
     * 远端数据
     *
     * @type {*}
     * @memberof LineEditService
     */
    private remoteCopyData:any = {};


    /**
     * 用户服务对象
     *
     * @type {Res_usersService}
     * @memberof LineEditService
     */
    public res_usersService: Res_usersService = new Res_usersService();

    /**
     * 币种服务对象
     *
     * @type {Res_currencyService}
     * @memberof LineEditService
     */
    public res_currencyService: Res_currencyService = new Res_currencyService();

    /**
     * 产品服务对象
     *
     * @type {Product_productService}
     * @memberof LineEditService
     */
    public product_productService: Product_productService = new Product_productService();

    /**
     * 销售订单行服务对象
     *
     * @type {Sale_order_lineService}
     * @memberof LineEditService
     */
    public sale_order_lineService: Sale_order_lineService = new Sale_order_lineService();

    /**
     * 分析类别服务对象
     *
     * @type {Account_analytic_groupService}
     * @memberof LineEditService
     */
    public account_analytic_groupService: Account_analytic_groupService = new Account_analytic_groupService();

    /**
     * 分析账户服务对象
     *
     * @type {Account_analytic_accountService}
     * @memberof LineEditService
     */
    public account_analytic_accountService: Account_analytic_accountService = new Account_analytic_accountService();

    /**
     * 科目服务对象
     *
     * @type {Account_accountService}
     * @memberof LineEditService
     */
    public account_accountService: Account_accountService = new Account_accountService();

    /**
     * 公司服务对象
     *
     * @type {Res_companyService}
     * @memberof LineEditService
     */
    public res_companyService: Res_companyService = new Res_companyService();

    /**
     * 日记账项目服务对象
     *
     * @type {Account_move_lineService}
     * @memberof LineEditService
     */
    public account_move_lineService: Account_move_lineService = new Account_move_lineService();

    /**
     * 联系人服务对象
     *
     * @type {Res_partnerService}
     * @memberof LineEditService
     */
    public res_partnerService: Res_partnerService = new Res_partnerService();

    /**
     * 处理数据
     *
     * @public
     * @param {Promise<any>} promise
     * @returns {Promise<any>}
     * @memberof LineEditService
     */
    public doItems(promise: Promise<any>, deKeyField: string, deName: string): Promise<any> {
        return new Promise((resolve, reject) => {
            promise.then((response: any) => {
                if (response && response.status === 200) {
                    const data = response.data;
                    data.forEach((item:any,index:number) =>{
                        item[deName] = item[deKeyField];
                        data[index] = item;
                    });
                    resolve(data);
                } else {
                    reject([])
                }
            }).catch((response: any) => {
                reject([])
            });
        });
    }

    /**
     * 获取跨实体数据集合
     *
     * @param {string} serviceName 服务名称
     * @param {string} interfaceName 接口名称
     * @param {*} data
     * @param {boolean} [isloading]
     * @returns {Promise<any[]>}
     * @memberof  LineEditService
     */
    @Errorlog
    public getItems(serviceName: string, interfaceName: string, context: any = {}, data: any, isloading?: boolean): Promise<any[]> {
        data.page = data.page ? data.page : 0;
        data.size = data.size ? data.size : 1000;
        if (Object.is(serviceName, 'Res_usersService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.res_usersService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'res_users');
        }
        if (Object.is(serviceName, 'Product_productService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.product_productService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'product_product');
        }
        if (Object.is(serviceName, 'Sale_order_lineService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.sale_order_lineService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'sale_order_line');
        }
        if (Object.is(serviceName, 'Account_analytic_groupService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.account_analytic_groupService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'account_analytic_group');
        }
        if (Object.is(serviceName, 'Account_analytic_accountService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.account_analytic_accountService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'account_analytic_account');
        }
        if (Object.is(serviceName, 'Account_accountService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.account_accountService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'account_account');
        }
        if (Object.is(serviceName, 'Res_companyService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.res_companyService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'res_company');
        }
        if (Object.is(serviceName, 'Account_move_lineService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.account_move_lineService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'account_move_line');
        }
        if (Object.is(serviceName, 'Res_partnerService') && Object.is(interfaceName, 'FetchDefault')) {
            return this.doItems(this.res_partnerService.FetchDefault(JSON.parse(JSON.stringify(context)),data, isloading), 'id', 'res_partner');
        }

        return Promise.reject([])
    }

    /**
     * 添加数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LineEditService
     */
    @Errorlog
    public add(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestDataWithUpdate(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.Create(Context,Data, isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }

    /**
     * 删除数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LineEditService
     */
    @Errorlog
    public delete(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.Remove(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }

    /**
     * 修改数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LineEditService
     */
    @Errorlog
    public update(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestDataWithUpdate(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data,isloading);
            }else{
                result =_appEntityService.Update(Context,Data,isloading);
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }

    /**
     * 获取数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LineEditService
     */
    @Errorlog
    public get(action: string, context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                result = this.appEntityService.Get(Context,Data, isloading);
            }
            result.then((response) => {
                //处理返回数据，补充判断标识
                if(response.data){
                    Object.assign(response.data,{srfuf:0});
                }
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LineEditService
     */
    @Errorlog
    public search(action: string,context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                this.setCopynativeData(response.data);
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }


    /**
     * 加载草稿
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LineEditService
     */
    @Errorlog
    public loadDraft(action: string, context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                result = this.appEntityService.GetDraft(Context,Data, isloading);
            }
            result.then((response) => {
                //处理返回数据，补充判断标识
                if(response.data){
                    Object.assign(response.data,{srfuf:'0'});
                    //仿真主键数据
                    response.data.id = Util.createUUID();
                }
                this.setRemoteCopyData(response);
                this.handleResponse(action, response, true);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        });
    }


    /**
     * 前台逻辑
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof LineEditService
     */
    @Errorlog
    public frontLogic(action:string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any)=>{
            let result: Promise<any>;
            const _appEntityService: any = this.appEntityService;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            } else {
                return Promise.reject({ status: 500, data: { title: '失败', message: '系统异常' } });
            }
            result.then((response) => {
                this.handleResponse(action, response);
                resolve(response);
            }).catch(response => {
                reject(response);
            });
        })
    }

    /**
     * 处理请求数据(修改或增加数据)
     * 
     * @param action 行为 
     * @param data 数据
     * @memberof LineEditService
     */
    public handleRequestDataWithUpdate(action: string,context:any ={},data: any = {},isMerge:boolean = false){
        let model: any = this.getMode();
        if (!model && model.getDataItems instanceof Function) {
            return data;
        }
        let dataItems: any[] = model.getDataItems();
        let requestData:any = {};
        if(isMerge && (data && data.viewparams)){
            Object.assign(requestData,data.viewparams);
        }
        dataItems.forEach((item:any) =>{
            if(item && item.dataType && Object.is(item.dataType,'FONTKEY')){
                if(item && item.prop && item.name ){
                    requestData[item.prop] = context[item.name];
                }
            }else{
                if(item && item.isEditable && item.prop && item.name && data.hasOwnProperty(item.name)){
                    requestData[item.prop] = data[item.name];
                }
            }
        });
        let tempContext:any = JSON.parse(JSON.stringify(context));
        if(tempContext && tempContext.srfsessionid){
            tempContext.srfsessionkey = tempContext.srfsessionid;
            delete tempContext.srfsessionid;
        }
        return {context:tempContext,data:requestData};
    }
    /**
     * 设置远端数据
     * 
     * @param result 远端请求结果 
     * @memberof LineEditService
     */
    public setRemoteCopyData(result:any){
        if (result && result.status === 200) {
            this.remoteCopyData = Util.deepCopy(result.data);
        }
    }

    /**
     * 获取远端数据
     * 
     * @memberof LineEditService
     */
    public getRemoteCopyData(){
        return this.remoteCopyData;
    }

    /**
     * 设置备份原生数据
     * 
     * @param data 远端请求结果 
     * @memberof LineEditService
     */
    public setCopynativeData(data:any){
        this.copynativeData = Util.deepCopy(data);
    }

    /**
     * 获取备份原生数据
     * 
     * @memberof LineEditService
     */
    public getCopynativeData(){
        return this.copynativeData;
    }    
}