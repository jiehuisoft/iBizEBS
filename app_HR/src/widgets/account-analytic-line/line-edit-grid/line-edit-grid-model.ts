/**
 * LineEdit 部件模型
 *
 * @export
 * @class LineEditModel
 */
export default class LineEditModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof LineEditGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof LineEditGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'account_id_text',
          prop: 'account_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'user_id_text',
          prop: 'user_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'amount',
          prop: 'amount',
          dataType: 'DECIMAL',
          isEditable:true
        },
        {
          name: 'company_id_text',
          prop: 'company_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'create_uid_text',
          prop: 'create_uid_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'create_date',
          prop: 'create_date',
          dataType: 'DATETIME',
          isEditable:true
        },
        {
          name: 'currency_id_text',
          prop: 'currency_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'general_account_id_text',
          prop: 'general_account_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'group_id_text',
          prop: 'group_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'move_id_text',
          prop: 'move_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'name',
          prop: 'name',
          dataType: 'TEXT',
          isEditable:true
        },
        {
          name: 'partner_id_text',
          prop: 'partner_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'product_id_text',
          prop: 'product_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'so_line_text',
          prop: 'so_line_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'product_uom_id',
          prop: 'product_uom_id',
          dataType: 'PICKUP',
        },
        {
          name: 'product_id',
          prop: 'product_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'general_account_id',
          prop: 'general_account_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'user_id',
          prop: 'user_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'partner_id',
          prop: 'partner_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'currency_id',
          prop: 'currency_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'account_id',
          prop: 'account_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'group_id',
          prop: 'group_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'so_line',
          prop: 'so_line',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'move_id',
          prop: 'move_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'account_analytic_line',
          prop: 'id',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}