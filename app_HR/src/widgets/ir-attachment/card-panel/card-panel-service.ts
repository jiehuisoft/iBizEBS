import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Card 部件服务对象
 *
 * @export
 * @class CardService
 */
export default class CardService extends ControlService {
}