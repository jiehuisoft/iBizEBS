import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, PanelControlBase } from '@/studio-core';
import Ir_attachmentService from '@/service/ir-attachment/ir-attachment-service';
import CardService from './card-panel-service';
import Ir_attachmentUIService from '@/uiservice/ir-attachment/ir-attachment-ui-service';
import { PanelDetailModel,PanelRawitemModel,PanelTabPanelModel,PanelTabPageModel,PanelFieldModel,PanelContainerModel,PanelControlModel,PanelUserControlModel,PanelButtonModel } from '@/model/panel-detail';
import CardModel from './card-panel-model';
import CodeListService from "@service/app/codelist-service";


/**
 * itemlayoutpanel部件基类
 *
 * @export
 * @class PanelControlBase
 * @extends {CardPanelBase}
 */
export class CardPanelBase extends PanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof CardPanelBase
     */
    protected controlType: string = 'PANEL';

    /**
     * 建构部件服务对象
     *
     * @type {CardService}
     * @memberof CardPanelBase
     */
    public service: CardService = new CardService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Ir_attachmentService}
     * @memberof CardPanelBase
     */
    public appEntityService: Ir_attachmentService = new Ir_attachmentService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof CardPanelBase
     */
    protected appDeName: string = 'ir_attachment';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof CardPanelBase
     */
    protected appDeLogicName: string = '附件';

    /**
     * 界面UI服务对象
     *
     * @type {Ir_attachmentUIService}
     * @memberof CardBase
     */  
    public appUIService:Ir_attachmentUIService = new Ir_attachmentUIService(this.$store);


    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Card
     */
    public detailsModel: any = {
        name: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'name', panel: this })
,
        container1: new PanelContainerModel({ caption: '', itemType: 'CONTAINER',visible: true, disabled: false, name: 'container1', panel: this })
,
    };

    /**
     * 面板逻辑
     *
     * @public
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof Card
     */
    public panelLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
                


    }

    /**
     * 数据模型对象
     *
     * @type {CardModel}
     * @memberof Card
     */
    public dataModel: CardModel = new CardModel();

    /**
     * 界面行为
     *
     * @param {*} row
     * @param {*} tag
     * @param {*} $event
     * @memberof Card
     */
    public uiAction(row: any, tag: any, $event: any) {
    }
}