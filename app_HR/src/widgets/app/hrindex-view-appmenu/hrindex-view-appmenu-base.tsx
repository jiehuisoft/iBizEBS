import { Vue } from 'vue-property-decorator';

/**
 * 应用菜单基类
 */
export class HRIndexViewBase extends Vue {

    /**
     * 获取应用上下文
     *
     * @memberof HRIndexViewBase
     */
    get context(): any {
        return this.$appService.contextStore.appContext || {};
    }

    /**
     * 菜单点击
     *
     * @param {*} item 菜单数据
     * @memberof HRIndexViewBase
     */
    public click(item: any) {
        if (item) {
            let judge = true;
            switch (item.appfunctag) {
                case 'AppFunc2': 
                    this.clickAppFunc2(item); break;
                case 'AppFunc8': 
                    this.clickAppFunc8(item); break;
                case 'AppFunc3': 
                    this.clickAppFunc3(item); break;
                case 'AppFunc5': 
                    this.clickAppFunc5(item); break;
                case 'AppFunc7': 
                    this.clickAppFunc7(item); break;
                case 'AppFunc4': 
                    this.clickAppFunc4(item); break;
                case 'AppFunc6': 
                    this.clickAppFunc6(item); break;
                default:
                    judge = false;
                    console.warn('未指定应用功能');
            }
            if (judge && this.$uiState.isStyle2()) {
                this.$appService.navHistory.reset();
                this.$appService.viewStore.reset();
            }
        }
    }
    
    /**
     * 合同
     *
     * @param {*} [item={}]
     * @memberof HRIndexView
     */
    public clickAppFunc2(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'hr_contracts', parameterName: 'hr_contract' },
            { pathName: 'gridview', parameterName: 'gridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 简历类型
     *
     * @param {*} [item={}]
     * @memberof HRIndexView
     */
    public clickAppFunc8(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'hr_resume_line_types', parameterName: 'hr_resume_line_type' },
            { pathName: 'lineedit', parameterName: 'lineedit' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 工作岗位
     *
     * @param {*} [item={}]
     * @memberof HRIndexView
     */
    public clickAppFunc3(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'hr_jobs', parameterName: 'hr_job' },
            { pathName: 'treeexpview', parameterName: 'treeexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 员工技能
     *
     * @param {*} [item={}]
     * @memberof HRIndexView
     */
    public clickAppFunc5(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'hr_skill_types', parameterName: 'hr_skill_type' },
            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 部门
     *
     * @param {*} [item={}]
     * @memberof HRIndexView
     */
    public clickAppFunc7(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'hr_departments', parameterName: 'hr_department' },
            { pathName: 'mastergridview', parameterName: 'mastergridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * [临时]员工树视图
     *
     * @param {*} [item={}]
     * @memberof HRIndexView
     */
    public clickAppFunc4(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'hr_employees', parameterName: 'hr_employee' },
            { pathName: 'treeexpview', parameterName: 'treeexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 员工类别
     *
     * @param {*} [item={}]
     * @memberof HRIndexView
     */
    public clickAppFunc6(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'hr_employee_categories', parameterName: 'hr_employee_category' },
            { pathName: 'lineedit', parameterName: 'lineedit' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }

    /**
     * 绘制内容
     *
     * @private
     * @memberof HRIndexViewBase
     */
    public render(): any {
        return <span style="display: none;"/>
    }

}