import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import HRIndexViewModel from './hrindex-view-appmenu-model';


/**
 * HRIndexView 部件服务对象
 *
 * @export
 * @class HRIndexViewService
 */
export default class HRIndexViewService extends ControlService {

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof HRIndexViewService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of HRIndexViewService.
     * 
     * @param {*} [opts={}]
     * @memberof HRIndexViewService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new HRIndexViewModel();
    }

    /**
     * 获取数据
     *
     * @returns {Promise<any>}
     * @memberof HRIndexView
     */
    @Errorlog
    public get(params: any = {}): Promise<any> {
        return Http.getInstance().get('v7/hrindex-viewappmenu', params);
    }

}