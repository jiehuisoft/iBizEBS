/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'tz',
      },
      {
        name: 'name',
      },
      {
        name: '__last_update',
      },
      {
        name: 'write_date',
      },
      {
        name: 'resource_calendar',
        prop: 'id',
      },
      {
        name: 'global_leave_ids',
      },
      {
        name: 'attendance_ids',
      },
      {
        name: 'hours_per_day',
      },
      {
        name: 'leave_ids',
      },
      {
        name: 'display_name',
      },
      {
        name: 'create_date',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'company_id',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'write_uid',
      },
    ]
  }


}