import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, PanelControlBase } from '@/studio-core';
import Mail_messageService from '@/service/mail-message/mail-message-service';
import ListService from './list-panel-service';
import Mail_messageUIService from '@/uiservice/mail-message/mail-message-ui-service';
import { PanelDetailModel,PanelRawitemModel,PanelTabPanelModel,PanelTabPageModel,PanelFieldModel,PanelContainerModel,PanelControlModel,PanelUserControlModel,PanelButtonModel } from '@/model/panel-detail';
import ListModel from './list-panel-model';
import CodeListService from "@service/app/codelist-service";


/**
 * list_itempanel部件基类
 *
 * @export
 * @class PanelControlBase
 * @extends {ListPanelBase}
 */
export class ListPanelBase extends PanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ListPanelBase
     */
    protected controlType: string = 'PANEL';

    /**
     * 建构部件服务对象
     *
     * @type {ListService}
     * @memberof ListPanelBase
     */
    public service: ListService = new ListService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Mail_messageService}
     * @memberof ListPanelBase
     */
    public appEntityService: Mail_messageService = new Mail_messageService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ListPanelBase
     */
    protected appDeName: string = 'mail_message';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ListPanelBase
     */
    protected appDeLogicName: string = '消息';

    /**
     * 界面UI服务对象
     *
     * @type {Mail_messageUIService}
     * @memberof ListBase
     */  
    public appUIService:Mail_messageUIService = new Mail_messageUIService(this.$store);


    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof List
     */
    public detailsModel: any = {
        date: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'date', panel: this })
,
        subject: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'subject', panel: this })
,
        body: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'body', panel: this })
,
        container1: new PanelContainerModel({ caption: '', itemType: 'CONTAINER',visible: true, disabled: false, name: 'container1', panel: this })
,
    };

    /**
     * 面板逻辑
     *
     * @public
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof List
     */
    public panelLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
                




    }

    /**
     * 数据模型对象
     *
     * @type {ListModel}
     * @memberof List
     */
    public dataModel: ListModel = new ListModel();

    /**
     * 界面行为
     *
     * @param {*} row
     * @param {*} tag
     * @param {*} $event
     * @memberof List
     */
    public uiAction(row: any, tag: any, $event: any) {
    }
}