/**
 * List 部件模型
 *
 * @export
 * @class ListModel
 */
export default class ListModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof ListListMode
	 */
	public getDataItems(): any[] {
		return [
			{
				name: 'author_id',
			},
			{
				name: 'body',
			},
			{
				name: 'subject',
			},
			{
				name: 'date',
			},
			{
				name: 'srfkey',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'srfmajortext',
				prop: 'id',
				dataType: 'ACID',
			},
			{
				name: 'write_uid',
				prop: 'write_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'mail_activity_type_id',
				prop: 'mail_activity_type_id',
				dataType: 'PICKUP',
			},
			{
				name: 'moderator_id',
				prop: 'moderator_id',
				dataType: 'PICKUP',
			},
			{
				name: 'create_uid',
				prop: 'create_uid',
				dataType: 'PICKUP',
			},
			{
				name: 'parent_id',
				prop: 'parent_id',
				dataType: 'PICKUP',
			},
			{
				name: 'subtype_id',
				prop: 'subtype_id',
				dataType: 'PICKUP',
			},
			{
				name: 'mail_message',
				prop: 'id',
				dataType: 'FONTKEY',
			},
      {
        name:'size',
        prop:'size'
      },
      {
        name:'query',
        prop:'query'
      },
      {
        name:'sort',
        prop:'sort'
      },
      {
        name:'page',
        prop:'page'
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
		]
	}

}