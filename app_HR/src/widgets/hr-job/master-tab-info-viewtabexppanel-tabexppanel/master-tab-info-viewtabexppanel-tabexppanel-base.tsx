import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, TabExpPanelControlBase } from '@/studio-core';
import Hr_jobService from '@/service/hr-job/hr-job-service';
import MasterTabInfoViewtabexppanelService from './master-tab-info-viewtabexppanel-tabexppanel-service';
import Hr_jobUIService from '@/uiservice/hr-job/hr-job-ui-service';
import Hr_jobAuthService from '@/authservice/hr-job/hr-job-auth-service';
import { Environment } from '@/environments/environment';


/**
 * tabexppanel部件基类
 *
 * @export
 * @class TabExpPanelControlBase
 * @extends {MasterTabInfoViewtabexppanelTabexppanelBase}
 */
export class MasterTabInfoViewtabexppanelTabexppanelBase extends TabExpPanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    protected controlType: string = 'TABEXPPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {MasterTabInfoViewtabexppanelService}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    public service: MasterTabInfoViewtabexppanelService = new MasterTabInfoViewtabexppanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Hr_jobService}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    public appEntityService: Hr_jobService = new Hr_jobService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    protected appDeName: string = 'hr_job';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    protected appDeLogicName: string = '工作岗位';

    /**
     * 界面UI服务对象
     *
     * @type {Hr_jobUIService}
     * @memberof MasterTabInfoViewtabexppanelBase
     */  
    public appUIService:Hr_jobUIService = new Hr_jobUIService(this.$store);

    /**
     * 是否初始化
     *
     * @protected
     * @returns {any}
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected isInit: any = {
        tabviewpanel21:  true ,
    }

    /**
     * 被激活的分页面板
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected activatedTabViewPanel: string = 'tabviewpanel21';

    /**
     * 实体权限服务对象
     *
     * @protected
     * @type Hr_jobAuthServiceBase
     * @memberof TabExpViewtabexppanelBase
     */
    protected appAuthService: Hr_jobAuthService = new Hr_jobAuthService();

    /**
     * 分页面板权限标识存储对象
     *
     * @protected
     * @type {*}
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected authResourceObject:any = {'tabviewpanel21':{resourcetag:null,visabled: true,disabled: false}};

    /**
     * 组件创建完毕
     *
     * @protected
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected ctrlCreated(): void {
        //设置分页导航srfparentdename和srfparentkey
        if (this.context.hr_job) {
            Object.assign(this.context, { srfparentdename: 'Hr_job', srfparentkey: this.context.hr_job });
        }
        super.ctrlCreated();
    }
}