/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'report_header',
      },
      {
        name: 'country_id',
      },
      {
        name: 'sale_quotation_onboarding_state',
      },
      {
        name: 'quotation_validity_days',
      },
      {
        name: 'account_onboarding_invoice_layout_state',
      },
      {
        name: 'account_no',
      },
      {
        name: 'bank_account_code_prefix',
      },
      {
        name: 'bank_journal_ids',
      },
      {
        name: 'zip',
      },
      {
        name: 'period_lock_date',
      },
      {
        name: 'state_id',
      },
      {
        name: 'tax_exigibility',
      },
      {
        name: 'resource_calendar_ids',
      },
      {
        name: 'bank_ids',
      },
      {
        name: 'account_dashboard_onboarding_state',
      },
      {
        name: 'create_date',
      },
      {
        name: 'snailmail_color',
      },
      {
        name: 'city',
      },
      {
        name: 'account_setup_coa_state',
      },
      {
        name: 'catchall',
      },
      {
        name: 'display_name',
      },
      {
        name: 'anglo_saxon_accounting',
      },
      {
        name: 'snailmail_duplex',
      },
      {
        name: 'social_github',
      },
      {
        name: 'account_setup_bank_data_state',
      },
      {
        name: 'res_company',
        prop: 'id',
      },
      {
        name: 'street2',
      },
      {
        name: 'expects_chart_of_accounts',
      },
      {
        name: 'transfer_account_code_prefix',
      },
      {
        name: 'fiscalyear_last_day',
      },
      {
        name: 'user_ids',
      },
      {
        name: 'account_bank_reconciliation_start',
      },
      {
        name: 'portal_confirmation_pay',
      },
      {
        name: 'qr_code',
      },
      {
        name: 'street',
      },
      {
        name: 'account_invoice_onboarding_state',
      },
      {
        name: 'child_ids',
      },
      {
        name: 'sequence',
      },
      {
        name: 'nomenclature_id',
      },
      {
        name: 'payment_acquirer_onboarding_state',
      },
      {
        name: 'report_footer',
      },
      {
        name: 'payment_onboarding_payment_method',
      },
      {
        name: 'write_date',
      },
      {
        name: 'po_double_validation',
      },
      {
        name: 'po_lead',
      },
      {
        name: 'sale_onboarding_sample_quotation_state',
      },
      {
        name: 'sale_onboarding_order_confirmation_state',
      },
      {
        name: 'external_report_layout_id',
      },
      {
        name: 'sale_onboarding_payment_method',
      },
      {
        name: 'account_onboarding_sample_invoice_state',
      },
      {
        name: 'base_onboarding_company_state',
      },
      {
        name: 'social_linkedin',
      },
      {
        name: 'manufacturing_lead',
      },
      {
        name: 'po_double_validation_amount',
      },
      {
        name: 'po_lock',
      },
      {
        name: 'social_twitter',
      },
      {
        name: 'social_instagram',
      },
      {
        name: 'account_setup_fy_data_state',
      },
      {
        name: 'tax_calculation_rounding_method',
      },
      {
        name: 'cash_account_code_prefix',
      },
      {
        name: 'account_onboarding_sale_tax_state',
      },
      {
        name: 'security_lead',
      },
      {
        name: 'website_theme_onboarding_done',
      },
      {
        name: 'invoice_is_print',
      },
      {
        name: 'company_registry',
      },
      {
        name: '__last_update',
      },
      {
        name: 'logo_web',
      },
      {
        name: 'fiscalyear_lock_date',
      },
      {
        name: 'invoice_is_snailmail',
      },
      {
        name: 'website_sale_onboarding_payment_acquirer_state',
      },
      {
        name: 'social_facebook',
      },
      {
        name: 'portal_confirmation_sign',
      },
      {
        name: 'paperformat_id',
      },
      {
        name: 'fiscalyear_last_month',
      },
      {
        name: 'invoice_is_email',
      },
      {
        name: 'social_youtube',
      },
      {
        name: 'expense_currency_exchange_account_id',
      },
      {
        name: 'partner_gid',
      },
      {
        name: 'phone',
      },
      {
        name: 'logo',
      },
      {
        name: 'property_stock_account_input_categ_id_text',
      },
      {
        name: 'account_purchase_tax_id_text',
      },
      {
        name: 'name',
      },
      {
        name: 'incoterm_id_text',
      },
      {
        name: 'account_opening_journal_id',
      },
      {
        name: 'transfer_account_id_text',
      },
      {
        name: 'income_currency_exchange_account_id',
      },
      {
        name: 'currency_id_text',
      },
      {
        name: 'account_opening_date',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'chart_template_id_text',
      },
      {
        name: 'account_sale_tax_id_text',
      },
      {
        name: 'account_opening_move_id_text',
      },
      {
        name: 'email',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'property_stock_account_output_categ_id_text',
      },
      {
        name: 'property_stock_valuation_account_id_text',
      },
      {
        name: 'parent_id_text',
      },
      {
        name: 'tax_cash_basis_journal_id_text',
      },
      {
        name: 'internal_transit_location_id_text',
      },
      {
        name: 'website',
      },
      {
        name: 'vat',
      },
      {
        name: 'resource_calendar_id_text',
      },
      {
        name: 'currency_exchange_journal_id_text',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'parent_id',
      },
      {
        name: 'currency_id',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'property_stock_account_output_categ_id',
      },
      {
        name: 'property_stock_valuation_account_id',
      },
      {
        name: 'account_opening_move_id',
      },
      {
        name: 'internal_transit_location_id',
      },
      {
        name: 'account_purchase_tax_id',
      },
      {
        name: 'chart_template_id',
      },
      {
        name: 'account_sale_tax_id',
      },
      {
        name: 'tax_cash_basis_journal_id',
      },
      {
        name: 'property_stock_account_input_categ_id',
      },
      {
        name: 'partner_id',
      },
      {
        name: 'incoterm_id',
      },
      {
        name: 'resource_calendar_id',
      },
      {
        name: 'transfer_account_id',
      },
      {
        name: 'currency_exchange_journal_id',
      },
    ]
  }


}