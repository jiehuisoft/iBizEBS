import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, PanelControlBase } from '@/studio-core';
import Mail_activityService from '@/service/mail-activity/mail-activity-service';
import ListService from './list-panel-service';
import Mail_activityUIService from '@/uiservice/mail-activity/mail-activity-ui-service';
import { PanelDetailModel,PanelRawitemModel,PanelTabPanelModel,PanelTabPageModel,PanelFieldModel,PanelContainerModel,PanelControlModel,PanelUserControlModel,PanelButtonModel } from '@/model/panel-detail';
import ListModel from './list-panel-model';
import CodeListService from "@service/app/codelist-service";


/**
 * list_itempanel部件基类
 *
 * @export
 * @class PanelControlBase
 * @extends {ListPanelBase}
 */
export class ListPanelBase extends PanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ListPanelBase
     */
    protected controlType: string = 'PANEL';

    /**
     * 建构部件服务对象
     *
     * @type {ListService}
     * @memberof ListPanelBase
     */
    public service: ListService = new ListService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Mail_activityService}
     * @memberof ListPanelBase
     */
    public appEntityService: Mail_activityService = new Mail_activityService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ListPanelBase
     */
    protected appDeName: string = 'mail_activity';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ListPanelBase
     */
    protected appDeLogicName: string = '活动';

    /**
     * 界面UI服务对象
     *
     * @type {Mail_activityUIService}
     * @memberof ListBase
     */  
    public appUIService:Mail_activityUIService = new Mail_activityUIService(this.$store);

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public list_itempanel_button1_click(params: any = {}, tag?: any, $event?: any) {
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this;
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Mail_activityUIService  = new Mail_activityUIService();
        curUIService.Mail_activity_action_done(datas,contextJO, paramJO,  $event, xData,this,"Mail_activity");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public list_itempanel_button2_click(params: any = {}, tag?: any, $event?: any) {
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this;
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Mail_activityUIService  = new Mail_activityUIService();
        curUIService.Mail_activity_Edit(datas,contextJO, paramJO,  $event, xData,this,"Mail_activity");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public list_itempanel_button3_click(params: any = {}, tag?: any, $event?: any) {
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        xData = this;
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Mail_activityUIService  = new Mail_activityUIService();
        curUIService.Mail_activity_Remove(datas,contextJO, paramJO,  $event, xData,this,"Mail_activity");
    }


    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof List
     */
    public detailsModel: any = {
        icon: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'icon', panel: this })
,
        container4: new PanelContainerModel({ caption: '', itemType: 'CONTAINER',visible: true, disabled: false, name: 'container4', panel: this })
,
        activity_type_id_text: new PanelFieldModel({ caption: '活动类型', itemType: 'FIELD',visible: true, disabled: false, name: 'activity_type_id_text', panel: this })
,
        create_date: new PanelFieldModel({ caption: '创建时间', itemType: 'FIELD',visible: true, disabled: false, name: 'create_date', panel: this })
,
        user_id_text: new PanelFieldModel({ caption: '分派给', itemType: 'FIELD',visible: true, disabled: false, name: 'user_id_text', panel: this })
,
        container6: new PanelContainerModel({ caption: '', itemType: 'CONTAINER',visible: true, disabled: false, name: 'container6', panel: this })
,
        note: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'note', panel: this })
,
        container5: new PanelContainerModel({ caption: '', itemType: 'CONTAINER',visible: true, disabled: false, name: 'container5', panel: this })
,
        container2: new PanelContainerModel({ caption: '', itemType: 'CONTAINER',visible: true, disabled: false, name: 'container2', panel: this })
,
        button1: new PanelButtonModel({ caption: '标记完成', itemType: 'BUTTON',visible: true, disabled: false, name: 'button1', panel: this, uiaction: { type: 'DEUIACTION', tag: 'action_done',actiontarget: 'SINGLEKEY',noprivdisplaymode:2,visabled: true,disabled: false} })
,
        button2: new PanelButtonModel({ caption: '编辑', itemType: 'BUTTON',visible: true, disabled: false, name: 'button2', panel: this, uiaction: { type: 'DEUIACTION', tag: 'Edit',actiontarget: 'SINGLEKEY',noprivdisplaymode:2,visabled: true,disabled: false} })
,
        button3: new PanelButtonModel({ caption: '取消', itemType: 'BUTTON',visible: true, disabled: false, name: 'button3', panel: this, uiaction: { type: 'DEUIACTION', tag: 'Remove',actiontarget: 'SINGLEKEY',noprivdisplaymode:2,visabled: true,disabled: false} })
,
        container7: new PanelContainerModel({ caption: '', itemType: 'CONTAINER',visible: true, disabled: false, name: 'container7', panel: this })
,
        container3: new PanelContainerModel({ caption: '', itemType: 'CONTAINER',visible: true, disabled: false, name: 'container3', panel: this })
,
        container1: new PanelContainerModel({ caption: '', itemType: 'CONTAINER',visible: true, disabled: false, name: 'container1', panel: this })
,
    };

    /**
     * 面板逻辑
     *
     * @public
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof List
     */
    public panelLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
                















    }

    /**
     * 数据模型对象
     *
     * @type {ListModel}
     * @memberof List
     */
    public dataModel: ListModel = new ListModel();

    /**
     * 界面行为
     *
     * @param {*} row
     * @param {*} tag
     * @param {*} $event
     * @memberof List
     */
    public uiAction(row: any, tag: any, $event: any) {
        if(Object.is('action_done', tag)) {
            this.list_itempanel_button1_click(row, tag, $event);
        }
        if(Object.is('Edit', tag)) {
            this.list_itempanel_button2_click(row, tag, $event);
        }
        if(Object.is('Remove', tag)) {
            this.list_itempanel_button3_click(row, tag, $event);
        }
    }
}