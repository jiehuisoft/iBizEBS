iBiz商业套件（Odoo）是iBiz为中国的中小企业量身定制的一套开源的企业管理软件。

iBizEBS（Odoo）是iBiz商业套件的Odoo版本。在Odoo14标准业务模型基础上借助iBizOdoo中台服务重新构建的具备现代化界面呈现与符合国内用户习惯的企业管理应用。

iBiz商业套件全面采取中台模式、SpringBoot+VUE前后台分离架构、MDD/MDA全方位建模技术，满足上万级用户的高性能需求，致力于提供高可用度、全业务覆盖的重度开源项目。

# iBizOdoo中台理念
* iBizOdoo中台及前端应用组织系统架构：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0321/124456_2aacf238_7582525.png "iBizOdoo中台架构.png")

* iBiz生产体系中台三步走战略：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0321/124556_035d3db3_7582525.png "iBizOdoo中台三步走战略.png")

# 技术框架
**后台技术模板[iBiz4j Spring R7](http://demo.ibizlab.cn/ibizr7sfstdtempl/ibiz4jr7)**
* 核心框架：Spring Boot
* 持久层框架: Mybatis-plus
* 服务发现：Nacos
* 日志管理：Logback
* 项目管理框架: Maven

**前端技术模板[iBiz-Vue-Studio](https://gitee.com/ibizr7pfstdtempl/iBiz-Vue-Studio)**
* 前端MVVM框架：vue.js 2.6.10
* 路由：vue-router 3.1.3
* 状态管理：vue-router 3.1.3
* 国际化：vue-i18n 8.15.3
* 数据交互：axios 0.19.1
* UI框架：element-ui 2.13.0, view-design 4.1.0
* 工具库：qs, path-to-regexp, rxjs
* 图标库：font-awesome 4.7.0
* 引入组件： tinymce 4.8.5
* 代码风格检测：eslint


# 开发环境
* JDK
* Maven
* Node.js
* Yarn
* Vue Cli

# 开源说明
* 本系统100%开源，遵守MulanPSL-2.0协议

# 系统概览

* 人力资源模块

![输入图片说明](https://images.gitee.com/uploads/images/2021/0321/125359_90972ff7_7582525.png "员工表格.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0321/125441_59376a2a_7582525.png "员工主信息.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0321/125528_fbd51afd_7582525.png "合同看板.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0321/125630_e080ec64_7582525.png "员工技能列表导航.png")

* 采购模块

![输入图片说明](https://images.gitee.com/uploads/images/2020/1030/132322_ee4d4cf1_7582525.png "采购申请表格.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1030/132425_9da184dc_7582525.png "采购申请主信息.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1030/132507_a6787899_7582525.png "采购申请编辑.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1030/135128_2f51a66b_7582525.png "供应商价格表列表导航.png")
