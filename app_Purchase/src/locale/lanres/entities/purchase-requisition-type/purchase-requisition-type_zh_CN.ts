export default {
  fields: {
    name: "申请类型",
    exclusive: "供应商选择",
    id: "ID",
    quantity_copy: "设置数量方式",
    write_date: "最后更新时间",
    create_date: "创建时间",
    __last_update: "最后修改日",
    line_copy: "明细行",
    create_uname: "创建人",
    write_uname: "最后更新人",
    write_uid: "最后更新人",
    create_uid: "创建人",
  },
	views: {
		basiclistexpview: {
			caption: "采购申请类型",
      		title: "配置列表导航视图",
		},
		basiceditview: {
			caption: "供应商价格表",
      		title: "配置信息编辑视图",
		},
		pickupview: {
			caption: "采购申请类型",
      		title: "采购申请类型数据选择视图",
		},
		basicquickview: {
			caption: "快速新建",
      		title: "快速新建视图",
		},
		pickupgridview: {
			caption: "采购申请类型",
      		title: "采购申请类型选择表格视图",
		},
	},
	ef_basic_form: {
		details: {
			grouppanel1: "申请类型", 
			grouppanel2: "新采购报价单", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "申请类型", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			name: "申请类型", 
			exclusive: "供应商选择", 
			line_copy: "明细行", 
			quantity_copy: "设置数量方式", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	ef_basicquick_form: {
		details: {
			grouppanel1: "分组面板", 
			formpage1: "基本信息", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "申请类型", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			name: "申请类型", 
			exclusive: "供应商选择", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	main_grid: {
		nodata: "",
		columns: {
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	basiclistexpviewlistexpbar_toolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
	},
	basic_list: {
		nodata: "",
		uiactions: {
		},
	},
};