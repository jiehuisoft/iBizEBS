export default {
  fields: {
    id: "ID",
    create_date: "创建时间",
    display_name: "显示名称",
    type_tax_use: "税范围",
    active: "有效",
    children_tax_ids: "子级税",
    analytic: "包含在分析成本",
    price_include: "包含在价格中",
    description: "发票上的标签",
    write_date: "最后更新时间",
    tax_exigibility: "应有税金",
    name: "税率名称",
    tag_ids: "标签",
    amount_type: "税率计算",
    __last_update: "最后修改日",
    amount: "金额",
    sequence: "序号",
    include_base_amount: "影响后续税收的基础",
    cash_basis_base_account_id_text: "基本税应收科目",
    hide_tax_exigibility: "隐藏现金收付制选项",
    tax_group_id_text: "税组",
    company_id_text: "公司",
    write_uid_text: "最后更新人",
    create_uid_text: "创建人",
    create_uid: "创建人",
    write_uid: "最后更新人",
    cash_basis_base_account_id: "基本税应收科目",
    tax_group_id: "税组",
    company_id: "公司",
  },
	views: {
		purchasepickupgridview: {
			caption: "税率",
      		title: "税率选择表格视图",
		},
		mpickupview: {
			caption: "税率",
      		title: "税率数据多项选择视图",
		},
		pickupgridview: {
			caption: "税率",
      		title: "税率选择表格视图",
		},
		purchasempickupview: {
			caption: "税率",
      		title: "税率",
		},
	},
	main_grid: {
		nodata: "",
		columns: {
			name: "税率名称",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};