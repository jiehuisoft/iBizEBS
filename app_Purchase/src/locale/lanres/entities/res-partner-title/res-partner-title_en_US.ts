export default {
  fields: {
    display_name: "显示名称",
    write_date: "最后更新时间",
    __last_update: "最后修改日",
    create_date: "创建时间",
    id: "ID",
    shortcut: "简称",
    name: "称谓",
    create_uid_text: "创建人",
    write_uid_text: "最后更新者",
    create_uid: "创建人",
    write_uid: "最后更新者",
  },
};