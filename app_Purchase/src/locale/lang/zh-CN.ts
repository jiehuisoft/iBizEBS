import { Util } from '@/utils/util/util';
import zhCNUser from '../user/zh-CN.user';
import res_partner_zh_CN from '@locale/lanres/entities/res-partner/res-partner_zh_CN';
import stock_location_zh_CN from '@locale/lanres/entities/stock-location/stock-location_zh_CN';
import ir_attachment_zh_CN from '@locale/lanres/entities/ir-attachment/ir-attachment_zh_CN';
import account_tax_zh_CN from '@locale/lanres/entities/account-tax/account-tax_zh_CN';
import res_bank_zh_CN from '@locale/lanres/entities/res-bank/res-bank_zh_CN';
import account_fiscal_position_zh_CN from '@locale/lanres/entities/account-fiscal-position/account-fiscal-position_zh_CN';
import mail_activity_zh_CN from '@locale/lanres/entities/mail-activity/mail-activity_zh_CN';
import mail_message_zh_CN from '@locale/lanres/entities/mail-message/mail-message_zh_CN';
import uom_uom_zh_CN from '@locale/lanres/entities/uom-uom/uom-uom_zh_CN';
import account_incoterms_zh_CN from '@locale/lanres/entities/account-incoterms/account-incoterms_zh_CN';
import stock_picking_type_zh_CN from '@locale/lanres/entities/stock-picking-type/stock-picking-type_zh_CN';
import res_config_settings_zh_CN from '@locale/lanres/entities/res-config-settings/res-config-settings_zh_CN';
import product_template_zh_CN from '@locale/lanres/entities/product-template/product-template_zh_CN';
import product_template_attribute_line_zh_CN from '@locale/lanres/entities/product-template-attribute-line/product-template-attribute-line_zh_CN';
import res_supplier_zh_CN from '@locale/lanres/entities/res-supplier/res-supplier_zh_CN';
import delivery_carrier_zh_CN from '@locale/lanres/entities/delivery-carrier/delivery-carrier_zh_CN';
import res_users_zh_CN from '@locale/lanres/entities/res-users/res-users_zh_CN';
import purchase_requisition_type_zh_CN from '@locale/lanres/entities/purchase-requisition-type/purchase-requisition-type_zh_CN';
import res_partner_category_zh_CN from '@locale/lanres/entities/res-partner-category/res-partner-category_zh_CN';
import mail_activity_type_zh_CN from '@locale/lanres/entities/mail-activity-type/mail-activity-type_zh_CN';
import res_currency_zh_CN from '@locale/lanres/entities/res-currency/res-currency_zh_CN';
import res_company_zh_CN from '@locale/lanres/entities/res-company/res-company_zh_CN';
import mail_followers_zh_CN from '@locale/lanres/entities/mail-followers/mail-followers_zh_CN';
import res_partner_bank_zh_CN from '@locale/lanres/entities/res-partner-bank/res-partner-bank_zh_CN';
import product_pricelist_zh_CN from '@locale/lanres/entities/product-pricelist/product-pricelist_zh_CN';
import res_partner_title_zh_CN from '@locale/lanres/entities/res-partner-title/res-partner-title_zh_CN';
import purchase_order_zh_CN from '@locale/lanres/entities/purchase-order/purchase-order_zh_CN';
import uom_category_zh_CN from '@locale/lanres/entities/uom-category/uom-category_zh_CN';
import res_country_zh_CN from '@locale/lanres/entities/res-country/res-country_zh_CN';
import product_category_zh_CN from '@locale/lanres/entities/product-category/product-category_zh_CN';
import purchase_requisition_line_zh_CN from '@locale/lanres/entities/purchase-requisition-line/purchase-requisition-line_zh_CN';
import res_country_state_zh_CN from '@locale/lanres/entities/res-country-state/res-country-state_zh_CN';
import res_country_group_zh_CN from '@locale/lanres/entities/res-country-group/res-country-group_zh_CN';
import product_supplierinfo_zh_CN from '@locale/lanres/entities/product-supplierinfo/product-supplierinfo_zh_CN';
import purchase_order_line_zh_CN from '@locale/lanres/entities/purchase-order-line/purchase-order-line_zh_CN';
import mail_tracking_value_zh_CN from '@locale/lanres/entities/mail-tracking-value/mail-tracking-value_zh_CN';
import mail_compose_message_zh_CN from '@locale/lanres/entities/mail-compose-message/mail-compose-message_zh_CN';
import account_payment_term_zh_CN from '@locale/lanres/entities/account-payment-term/account-payment-term_zh_CN';
import product_product_zh_CN from '@locale/lanres/entities/product-product/product-product_zh_CN';
import purchase_requisition_zh_CN from '@locale/lanres/entities/purchase-requisition/purchase-requisition_zh_CN';
import components_zh_CN from '@locale/lanres/components/components_zh_CN';
import codelist_zh_CN from '@locale/lanres/codelist/codelist_zh_CN';
import userCustom_zh_CN from '@locale/lanres/userCustom/userCustom_zh_CN';

const data: any = {
    app: {
        commonWords:{
            error: "失败",
            success: "成功",
            ok: "确认",
            cancel: "取消",
            save: "保存",
            codeNotExist: "代码表不存在",
            reqException: "请求异常",
            sysException: "系统异常",
            warning: "警告",
            wrong: "错误",
            rulesException: "值规则校验异常",
            saveSuccess: "保存成功",
            saveFailed: "保存失败",
            deleteSuccess: "删除成功！",
            deleteError: "删除失败！",
            delDataFail: "删除数据失败",
            noData: "暂无数据",
            startsuccess:"启动成功",
            createFailed: '无法创建',
            isExist: '已存在'
        },
        local:{
            new: "新建",
            add: "增加",
        },
        gridpage: {
            choicecolumns: "选择列",
            refresh: "刷新",
            show: "显示",
            records: "条",
            totle: "共",
            noData: "无数据",
            valueVail: "值不能为空",
            notConfig: {
                fetchAction: "视图表格fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
                createAction: "视图表格createAction参数未配置",
                updateAction: "视图表格updateAction参数未配置",
                loaddraftAction: "视图表格loaddraftAction参数未配置",
            },
            data: "数据",
            delDataFail: "删除数据失败",
            delSuccess: "删除成功!",
            confirmDel: "确认要删除",
            notRecoverable: "删除操作将不可恢复？",
            notBatch: "批量添加未实现",
            grid: "表",
            exportFail: "数据导出失败",
            sum: "合计",
            formitemFailed: "表单项更新失败",
        },
        list: {
            notConfig: {
                fetchAction: "视图列表fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
                createAction: "视图列表createAction参数未配置",
                updateAction: "视图列表updateAction参数未配置",
            },
            confirmDel: "确认要删除",
            notRecoverable: "删除操作将不可恢复？",
        },
        listExpBar: {
            title: "列表导航栏",
        },
        wfExpBar: {
            title: "流程导航栏",
        },
        calendarExpBar:{
            title: "日历导航栏",
        },
        treeExpBar: {
            title: "树视图导航栏",
        },
        portlet: {
            noExtensions: "无扩展插件",
        },
        tabpage: {
            sureclosetip: {
                title: "关闭提醒",
                content: "表单数据已经修改，确定要关闭？",
            },
            closeall: "关闭所有",
            closeother: "关闭其他",
        },
        fileUpload: {
            caption: "上传",
        },
        searchButton: {
            search: "搜索",
            reset: "重置",
        },
        calendar:{
          today: "今天",
          month: "月",
          week: "周",
          day: "天",
          list: "列",
          dateSelectModalTitle: "选择要跳转的时间",
          gotoDate: "跳转",
          from: "从",
          to: "至",
        },
        // 非实体视图
        views: {
            purchaseindexview: {
                caption: "采购",
                title: "采购首页视图",
            },
        },
        utilview:{
            importview:"导入数据",
            warning:"警告",
            info:"请配置数据导入项" 
        },
        menus: {
            purchaseindexview: {
                user_menus: "用户菜单",
                top_menus: "顶部菜单",
                left_exp: "左侧菜单",
                menuitem1: "订单",
                menuitem2: "采购申请",
                menuitem3: "询价单",
                menuitem4: "采购订单",
                menuitem5: "供应商",
                menuitem7: "产品",
                menuitem6: "产品",
                menuitem8: "产品变种",
                menuitem9: "配置",
                menuitem15: "采购设置",
                menuitem10: "供应商价格表",
                menuitem11: "采购申请类型",
                menuitem12: "产品类别",
                menuitem13: "计量单位",
                menuitem14: "计量单位类别",
                bottom_exp: "底部内容",
                footer_left: "底部左侧",
                footer_center: "底部中间",
                footer_right: "底部右侧",
            },
        },
        formpage:{
            desc1: "操作失败,未能找到当前表单项",
            desc2: "无法继续操作",
            notconfig: {
                loadaction: "视图表单loadAction参数未配置",
                loaddraftaction: "视图表单loaddraftAction参数未配置",
                actionname: "视图表单'+actionName+'参数未配置",
                removeaction: "视图表单removeAction参数未配置",
            },
            saveerror: "保存数据发生错误",
            savecontent: "数据不一致，可能后台数据已经被修改,是否要重新加载数据？",
            valuecheckex: "值规则校验异常",
            savesuccess: "保存成功！",
            deletesuccess: "删除成功！",  
            workflow: {
                starterror: "工作流启动失败",
                startsuccess: "工作流启动成功",
                submiterror: "工作流提交失败",
                submitsuccess: "工作流提交成功",
            },
            updateerror: "表单项更新失败",     
        },
        gridBar: {
            title: "表格导航栏",
        },
        multiEditView: {
            notConfig: {
                fetchAction: "视图多编辑视图面板fetchAction参数未配置",
                loaddraftAction: "视图多编辑视图面板loaddraftAction参数未配置",
            },
        },
        dataViewExpBar: {
            title: "卡片视图导航栏",
        },
        kanban: {
            notConfig: {
                fetchAction: "视图列表fetchAction参数未配置",
                removeAction: "视图表格removeAction参数未配置",
            },
            delete1: "确认要删除 ",
            delete2: "删除操作将不可恢复？",
        },
        dashBoard: {
            handleClick: {
                title: "面板设计",
            },
        },
        dataView: {
            sum: "共",
            data: "条数据",
        },
        chart: {
            undefined: "未定义",
            quarter: "季度",   
            year: "年",
        },
        searchForm: {
            notConfig: {
                loadAction: "视图搜索表单loadAction参数未配置",
                loaddraftAction: "视图搜索表单loaddraftAction参数未配置",
            },
            custom: "存储自定义查询",
            title: "名称",
        },
        wizardPanel: {
            back: "上一步",
            next: "下一步",
            complete: "完成",
            preactionmessage:"未配置计算上一步行为"
        },
        viewLayoutPanel: {
            appLogoutView: {
                prompt1: "尊敬的客户您好，您已成功退出系统，将在",
                prompt2: "秒后跳转至",
                logingPage: "登录页",
            },
            appWfstepTraceView: {
                title: "应用流程处理记录视图",
            },
            appWfstepDataView: {
                title: "应用流程跟踪视图",
            },
            appLoginView: {
                username: "用户名",
                password: "密码",
                login: "登录",
            },
        },
    },
    form: {
        group: {
            show_more: "显示更多",
            hidden_more: "隐藏更多"
        }
    },
    entities: {
        res_partner: res_partner_zh_CN,
        stock_location: stock_location_zh_CN,
        ir_attachment: ir_attachment_zh_CN,
        account_tax: account_tax_zh_CN,
        res_bank: res_bank_zh_CN,
        account_fiscal_position: account_fiscal_position_zh_CN,
        mail_activity: mail_activity_zh_CN,
        mail_message: mail_message_zh_CN,
        uom_uom: uom_uom_zh_CN,
        account_incoterms: account_incoterms_zh_CN,
        stock_picking_type: stock_picking_type_zh_CN,
        res_config_settings: res_config_settings_zh_CN,
        product_template: product_template_zh_CN,
        product_template_attribute_line: product_template_attribute_line_zh_CN,
        res_supplier: res_supplier_zh_CN,
        delivery_carrier: delivery_carrier_zh_CN,
        res_users: res_users_zh_CN,
        purchase_requisition_type: purchase_requisition_type_zh_CN,
        res_partner_category: res_partner_category_zh_CN,
        mail_activity_type: mail_activity_type_zh_CN,
        res_currency: res_currency_zh_CN,
        res_company: res_company_zh_CN,
        mail_followers: mail_followers_zh_CN,
        res_partner_bank: res_partner_bank_zh_CN,
        product_pricelist: product_pricelist_zh_CN,
        res_partner_title: res_partner_title_zh_CN,
        purchase_order: purchase_order_zh_CN,
        uom_category: uom_category_zh_CN,
        res_country: res_country_zh_CN,
        product_category: product_category_zh_CN,
        purchase_requisition_line: purchase_requisition_line_zh_CN,
        res_country_state: res_country_state_zh_CN,
        res_country_group: res_country_group_zh_CN,
        product_supplierinfo: product_supplierinfo_zh_CN,
        purchase_order_line: purchase_order_line_zh_CN,
        mail_tracking_value: mail_tracking_value_zh_CN,
        mail_compose_message: mail_compose_message_zh_CN,
        account_payment_term: account_payment_term_zh_CN,
        product_product: product_product_zh_CN,
        purchase_requisition: purchase_requisition_zh_CN,
    },
    components: components_zh_CN,
    codelist: codelist_zh_CN,
    userCustom: userCustom_zh_CN,
};
// 合并用户自定义多语言
Util.mergeDeepObject(data, zhCNUser);
// 默认导出
export default data;