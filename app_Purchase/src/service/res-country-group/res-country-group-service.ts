import { Http } from '@/utils';
import { Util } from '@/utils';
import Res_country_groupServiceBase from './res-country-group-service-base';


/**
 * 国家/地区群组服务对象
 *
 * @export
 * @class Res_country_groupService
 * @extends {Res_country_groupServiceBase}
 */
export default class Res_country_groupService extends Res_country_groupServiceBase {

    /**
     * Creates an instance of  Res_country_groupService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_country_groupService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}