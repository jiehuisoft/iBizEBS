import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 产品服务对象基类
 *
 * @export
 * @class Product_productServiceBase
 * @extends {EntityServie}
 */
export default class Product_productServiceBase extends EntityService {

    /**
     * Creates an instance of  Product_productServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_productServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Product_productServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='product_product';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'product_products';
        this.APPDETEXT = 'name';
        this.APPNAME = 'purchase';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_productServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_product_supplierinfos',JSON.stringify(res.data.product_supplierinfos?res.data.product_supplierinfos:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_order_lines',JSON.stringify(res.data.purchase_order_lines?res.data.purchase_order_lines:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_requisition_lines',JSON.stringify(res.data.purchase_requisition_lines?res.data.purchase_requisition_lines:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/product_products`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_product_supplierinfos',JSON.stringify(res.data.product_supplierinfos?res.data.product_supplierinfos:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_order_lines',JSON.stringify(res.data.purchase_order_lines?res.data.purchase_order_lines:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_requisition_lines',JSON.stringify(res.data.purchase_requisition_lines?res.data.purchase_requisition_lines:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_productServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_templates/${context.product_template}/product_products/${context.product_product}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/product_products/${context.product_product}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_productServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product){
            let res:any = Http.getInstance().delete(`/product_templates/${context.product_template}/product_products/${context.product_product}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/product_products/${context.product_product}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_productServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product){
            let res:any = await Http.getInstance().get(`/product_templates/${context.product_template}/product_products/${context.product_product}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/product_products/${context.product_product}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_productServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && true){
            let res:any = await Http.getInstance().get(`/product_templates/${context.product_template}/product_products/getdraft`,isloading);
            res.data.product_product = data.product_product;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/product_products/getdraft`,isloading);
        res.data.product_product = data.product_product;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_productServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/product_products/${context.product_product}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_productServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/createbatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/product_products/${context.product_product}/createbatch`,data,isloading);
            return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_productServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product){
            let res:any = Http.getInstance().delete(`/product_templates/${context.product_template}/product_products/${context.product_product}/removebatch`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/product_products/${context.product_product}/removebatch`,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_productServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/product_products/${context.product_product}/save`,data,isloading);
            
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_productServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_templates/${context.product_template}/product_products/${context.product_product}/updatebatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/product_products/${context.product_product}/updatebatch`,data,isloading);
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_productServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/product_templates/${context.product_template}/product_products/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/product_products/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * FetchMaster接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_productServiceBase
     */
    public async FetchMaster(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/product_templates/${context.product_template}/product_products/fetchmaster`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/product_products/fetchmaster`,tempData,isloading);
        return res;
    }
}