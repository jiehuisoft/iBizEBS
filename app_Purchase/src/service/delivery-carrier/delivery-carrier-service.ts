import { Http } from '@/utils';
import { Util } from '@/utils';
import Delivery_carrierServiceBase from './delivery-carrier-service-base';


/**
 * 送货方式服务对象
 *
 * @export
 * @class Delivery_carrierService
 * @extends {Delivery_carrierServiceBase}
 */
export default class Delivery_carrierService extends Delivery_carrierServiceBase {

    /**
     * Creates an instance of  Delivery_carrierService.
     * 
     * @param {*} [opts={}]
     * @memberof  Delivery_carrierService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}