/**
 * 实体数据服务注册中心
 *
 * @export
 * @class EntityServiceRegister
 */
export class EntityServiceRegister {

    /**
     * 所有实体数据服务Map
     *
     * @protected
     * @type {*}
     * @memberof EntityServiceRegister
     */
    protected allEntityService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载实体数据服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof EntityServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of EntityServiceRegister.
     * @memberof EntityServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof EntityServiceRegister
     */
    protected init(): void {
                this.allEntityService.set('res_partner', () => import('@/service/res-partner/res-partner-service'));
        this.allEntityService.set('stock_location', () => import('@/service/stock-location/stock-location-service'));
        this.allEntityService.set('ir_attachment', () => import('@/service/ir-attachment/ir-attachment-service'));
        this.allEntityService.set('account_tax', () => import('@/service/account-tax/account-tax-service'));
        this.allEntityService.set('res_bank', () => import('@/service/res-bank/res-bank-service'));
        this.allEntityService.set('account_fiscal_position', () => import('@/service/account-fiscal-position/account-fiscal-position-service'));
        this.allEntityService.set('mail_activity', () => import('@/service/mail-activity/mail-activity-service'));
        this.allEntityService.set('mail_message', () => import('@/service/mail-message/mail-message-service'));
        this.allEntityService.set('uom_uom', () => import('@/service/uom-uom/uom-uom-service'));
        this.allEntityService.set('account_incoterms', () => import('@/service/account-incoterms/account-incoterms-service'));
        this.allEntityService.set('stock_picking_type', () => import('@/service/stock-picking-type/stock-picking-type-service'));
        this.allEntityService.set('res_config_settings', () => import('@/service/res-config-settings/res-config-settings-service'));
        this.allEntityService.set('product_template', () => import('@/service/product-template/product-template-service'));
        this.allEntityService.set('product_template_attribute_line', () => import('@/service/product-template-attribute-line/product-template-attribute-line-service'));
        this.allEntityService.set('res_supplier', () => import('@/service/res-supplier/res-supplier-service'));
        this.allEntityService.set('delivery_carrier', () => import('@/service/delivery-carrier/delivery-carrier-service'));
        this.allEntityService.set('res_users', () => import('@/service/res-users/res-users-service'));
        this.allEntityService.set('purchase_requisition_type', () => import('@/service/purchase-requisition-type/purchase-requisition-type-service'));
        this.allEntityService.set('res_partner_category', () => import('@/service/res-partner-category/res-partner-category-service'));
        this.allEntityService.set('mail_activity_type', () => import('@/service/mail-activity-type/mail-activity-type-service'));
        this.allEntityService.set('res_currency', () => import('@/service/res-currency/res-currency-service'));
        this.allEntityService.set('res_company', () => import('@/service/res-company/res-company-service'));
        this.allEntityService.set('mail_followers', () => import('@/service/mail-followers/mail-followers-service'));
        this.allEntityService.set('res_partner_bank', () => import('@/service/res-partner-bank/res-partner-bank-service'));
        this.allEntityService.set('product_pricelist', () => import('@/service/product-pricelist/product-pricelist-service'));
        this.allEntityService.set('res_partner_title', () => import('@/service/res-partner-title/res-partner-title-service'));
        this.allEntityService.set('purchase_order', () => import('@/service/purchase-order/purchase-order-service'));
        this.allEntityService.set('uom_category', () => import('@/service/uom-category/uom-category-service'));
        this.allEntityService.set('res_country', () => import('@/service/res-country/res-country-service'));
        this.allEntityService.set('product_category', () => import('@/service/product-category/product-category-service'));
        this.allEntityService.set('purchase_requisition_line', () => import('@/service/purchase-requisition-line/purchase-requisition-line-service'));
        this.allEntityService.set('res_country_state', () => import('@/service/res-country-state/res-country-state-service'));
        this.allEntityService.set('res_country_group', () => import('@/service/res-country-group/res-country-group-service'));
        this.allEntityService.set('product_supplierinfo', () => import('@/service/product-supplierinfo/product-supplierinfo-service'));
        this.allEntityService.set('purchase_order_line', () => import('@/service/purchase-order-line/purchase-order-line-service'));
        this.allEntityService.set('mail_tracking_value', () => import('@/service/mail-tracking-value/mail-tracking-value-service'));
        this.allEntityService.set('mail_compose_message', () => import('@/service/mail-compose-message/mail-compose-message-service'));
        this.allEntityService.set('account_payment_term', () => import('@/service/account-payment-term/account-payment-term-service'));
        this.allEntityService.set('product_product', () => import('@/service/product-product/product-product-service'));
        this.allEntityService.set('purchase_requisition', () => import('@/service/purchase-requisition/purchase-requisition-service'));
    }

    /**
     * 加载实体数据服务
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof EntityServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allEntityService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体数据服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof EntityServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const entityService: any = await this.loadService(name);
        if (entityService && entityService.default) {
            const instance: any = new entityService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const entityServiceRegister: EntityServiceRegister = new EntityServiceRegister();