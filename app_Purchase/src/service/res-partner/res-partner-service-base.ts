import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 联系人服务对象基类
 *
 * @export
 * @class Res_partnerServiceBase
 * @extends {EntityServie}
 */
export default class Res_partnerServiceBase extends EntityService {

    /**
     * Creates an instance of  Res_partnerServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partnerServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Res_partnerServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='res_partner';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'res_partners';
        this.APPDETEXT = 'name';
        this.APPNAME = 'purchase';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/res_partners`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/res_partners`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/res_partners/${context.res_partner}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/res_partners/${context.res_partner}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/res_partners/${context.res_partner}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/res_partners/${context.res_partner}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/res_partners/${context.res_partner}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/res_partners/${context.res_partner}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && true){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/res_partners/getdraft`,isloading);
            res.data.res_partner = data.res_partner;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/res_partners/getdraft`,isloading);
        res.data.res_partner = data.res_partner;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/res_partners/${context.res_partner}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/res_partners/${context.res_partner}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/res_partners/${context.res_partner}/createbatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/res_partners/${context.res_partner}/createbatch`,data,isloading);
            return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/res_partners/${context.res_partner}/removebatch`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/res_partners/${context.res_partner}/removebatch`,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/res_partners/${context.res_partner}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/res_partners/${context.res_partner}/save`,data,isloading);
            
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/res_partners/${context.res_partner}/updatebatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/res_partners/${context.res_partner}/updatebatch`,data,isloading);
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partnerServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/res_partners/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/res_partners/fetchdefault`,tempData,isloading);
        return res;
    }
}