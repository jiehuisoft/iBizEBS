import { Http } from '@/utils';
import { Util } from '@/utils';
import Account_payment_termServiceBase from './account-payment-term-service-base';


/**
 * 付款条款服务对象
 *
 * @export
 * @class Account_payment_termService
 * @extends {Account_payment_termServiceBase}
 */
export default class Account_payment_termService extends Account_payment_termServiceBase {

    /**
     * Creates an instance of  Account_payment_termService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_payment_termService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}