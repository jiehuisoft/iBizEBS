import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 采购申请服务对象基类
 *
 * @export
 * @class Purchase_requisitionServiceBase
 * @extends {EntityServie}
 */
export default class Purchase_requisitionServiceBase extends EntityService {

    /**
     * Creates an instance of  Purchase_requisitionServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_requisitionServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Purchase_requisitionServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='purchase_requisition';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'purchase_requisitions';
        this.APPDETEXT = 'name';
        this.APPNAME = 'purchase';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition){
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_orders',JSON.stringify(res.data.purchase_orders?res.data.purchase_orders:[]));
            this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_requisition_lines',JSON.stringify(res.data.purchase_requisition_lines?res.data.purchase_requisition_lines:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/purchase_requisitions`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_orders',JSON.stringify(res.data.purchase_orders?res.data.purchase_orders:[]));
        this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_requisition_lines',JSON.stringify(res.data.purchase_requisition_lines?res.data.purchase_requisition_lines:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/purchase_requisitions/${context.purchase_requisition}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/purchase_requisitions/${context.purchase_requisition}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && true){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/getdraft`,isloading);
            res.data.purchase_requisition = data.purchase_requisition;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/purchase_requisitions/getdraft`,isloading);
        res.data.purchase_requisition = data.purchase_requisition;
        
        return res;
    }

    /**
     * Action_cancel接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async Action_cancel(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/action_cancel`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/action_cancel`,data,isloading);
            return res;
    }

    /**
     * Action_done接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async Action_done(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/action_done`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/action_done`,data,isloading);
            return res;
    }

    /**
     * Action_draft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async Action_draft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/action_draft`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/action_draft`,data,isloading);
            return res;
    }

    /**
     * Action_in_progress接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async Action_in_progress(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/action_in_progress`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/action_in_progress`,data,isloading);
            return res;
    }

    /**
     * Action_open接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async Action_open(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/action_open`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/action_open`,data,isloading);
            return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/checkkey`,data,isloading);
            return res;
    }

    /**
     * MasterTabCount接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async MasterTabCount(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/mastertabcount`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/mastertabcount`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/purchase_requisitions/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * FetchMaster接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisitionServiceBase
     */
    public async FetchMaster(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/fetchmaster`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/purchase_requisitions/fetchmaster`,tempData,isloading);
        return res;
    }
}