import { Http } from '@/utils';
import { Util } from '@/utils';
import Purchase_requisitionServiceBase from './purchase-requisition-service-base';


/**
 * 采购申请服务对象
 *
 * @export
 * @class Purchase_requisitionService
 * @extends {Purchase_requisitionServiceBase}
 */
export default class Purchase_requisitionService extends Purchase_requisitionServiceBase {

    /**
     * Creates an instance of  Purchase_requisitionService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_requisitionService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}