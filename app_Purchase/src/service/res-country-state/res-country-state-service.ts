import { Http } from '@/utils';
import { Util } from '@/utils';
import Res_country_stateServiceBase from './res-country-state-service-base';


/**
 * 国家/地区州/省服务对象
 *
 * @export
 * @class Res_country_stateService
 * @extends {Res_country_stateServiceBase}
 */
export default class Res_country_stateService extends Res_country_stateServiceBase {

    /**
     * Creates an instance of  Res_country_stateService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_country_stateService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}