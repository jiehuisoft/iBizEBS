import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 供应商价格表服务对象基类
 *
 * @export
 * @class Product_supplierinfoServiceBase
 * @extends {EntityServie}
 */
export default class Product_supplierinfoServiceBase extends EntityService {

    /**
     * Creates an instance of  Product_supplierinfoServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_supplierinfoServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Product_supplierinfoServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='product_supplierinfo';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'product_supplierinfos';
        this.APPDETEXT = 'name_text';
        this.APPNAME = 'purchase';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_supplierinfoServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/product_supplierinfos`,data,isloading);
            
            return res;
        }
        if(context.product_template && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_supplierinfos`,data,isloading);
            
            return res;
        }
        if(context.product_product && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/product_supplierinfos`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/product_supplierinfos`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/product_supplierinfos`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_supplierinfoServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_templates/${context.product_template}/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_templates/${context.product_template}/product_supplierinfos/${context.product_supplierinfo}`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/product_supplierinfos/${context.product_supplierinfo}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/product_supplierinfos/${context.product_supplierinfo}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_supplierinfoServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product && context.product_supplierinfo){
            let res:any = Http.getInstance().delete(`/product_templates/${context.product_template}/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}`,isloading);
            return res;
        }
        if(context.product_template && context.product_supplierinfo){
            let res:any = Http.getInstance().delete(`/product_templates/${context.product_template}/product_supplierinfos/${context.product_supplierinfo}`,isloading);
            return res;
        }
        if(context.product_product && context.product_supplierinfo){
            let res:any = Http.getInstance().delete(`/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}`,isloading);
            return res;
        }
        if(context.res_supplier && context.product_supplierinfo){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/product_supplierinfos/${context.product_supplierinfo}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/product_supplierinfos/${context.product_supplierinfo}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_supplierinfoServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product && context.product_supplierinfo){
            let res:any = await Http.getInstance().get(`/product_templates/${context.product_template}/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}`,isloading);
            
            return res;
        }
        if(context.product_template && context.product_supplierinfo){
            let res:any = await Http.getInstance().get(`/product_templates/${context.product_template}/product_supplierinfos/${context.product_supplierinfo}`,isloading);
            
            return res;
        }
        if(context.product_product && context.product_supplierinfo){
            let res:any = await Http.getInstance().get(`/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}`,isloading);
            
            return res;
        }
        if(context.res_supplier && context.product_supplierinfo){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/product_supplierinfos/${context.product_supplierinfo}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/product_supplierinfos/${context.product_supplierinfo}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_supplierinfoServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product && true){
            let res:any = await Http.getInstance().get(`/product_templates/${context.product_template}/product_products/${context.product_product}/product_supplierinfos/getdraft`,isloading);
            res.data.product_supplierinfo = data.product_supplierinfo;
            
            return res;
        }
        if(context.product_template && true){
            let res:any = await Http.getInstance().get(`/product_templates/${context.product_template}/product_supplierinfos/getdraft`,isloading);
            res.data.product_supplierinfo = data.product_supplierinfo;
            
            return res;
        }
        if(context.product_product && true){
            let res:any = await Http.getInstance().get(`/product_products/${context.product_product}/product_supplierinfos/getdraft`,isloading);
            res.data.product_supplierinfo = data.product_supplierinfo;
            
            return res;
        }
        if(context.res_supplier && true){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/product_supplierinfos/getdraft`,isloading);
            res.data.product_supplierinfo = data.product_supplierinfo;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/product_supplierinfos/getdraft`,isloading);
        res.data.product_supplierinfo = data.product_supplierinfo;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_supplierinfoServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_supplierinfos/${context.product_supplierinfo}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/product_supplierinfos/${context.product_supplierinfo}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/product_supplierinfos/${context.product_supplierinfo}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_supplierinfoServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}/createbatch`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_supplierinfos/${context.product_supplierinfo}/createbatch`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}/createbatch`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/product_supplierinfos/${context.product_supplierinfo}/createbatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/product_supplierinfos/${context.product_supplierinfo}/createbatch`,data,isloading);
            return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_supplierinfoServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product && context.product_supplierinfo){
            let res:any = Http.getInstance().delete(`/product_templates/${context.product_template}/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}/removebatch`,isloading);
            return res;
        }
        if(context.product_template && context.product_supplierinfo){
            let res:any = Http.getInstance().delete(`/product_templates/${context.product_template}/product_supplierinfos/${context.product_supplierinfo}/removebatch`,isloading);
            return res;
        }
        if(context.product_product && context.product_supplierinfo){
            let res:any = Http.getInstance().delete(`/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}/removebatch`,isloading);
            return res;
        }
        if(context.res_supplier && context.product_supplierinfo){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/product_supplierinfos/${context.product_supplierinfo}/removebatch`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/product_supplierinfos/${context.product_supplierinfo}/removebatch`,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_supplierinfoServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}/save`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_supplierinfos/${context.product_supplierinfo}/save`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}/save`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/product_supplierinfos/${context.product_supplierinfo}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/product_supplierinfos/${context.product_supplierinfo}/save`,data,isloading);
            
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_supplierinfoServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_templates/${context.product_template}/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}/updatebatch`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_templates/${context.product_template}/product_supplierinfos/${context.product_supplierinfo}/updatebatch`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_products/${context.product_product}/product_supplierinfos/${context.product_supplierinfo}/updatebatch`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.product_supplierinfo){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/product_supplierinfos/${context.product_supplierinfo}/updatebatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/product_supplierinfos/${context.product_supplierinfo}/updatebatch`,data,isloading);
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_supplierinfoServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_product && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/product_templates/${context.product_template}/product_products/${context.product_product}/product_supplierinfos/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.product_template && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/product_templates/${context.product_template}/product_supplierinfos/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.product_product && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/product_products/${context.product_product}/product_supplierinfos/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.res_supplier && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/product_supplierinfos/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/product_supplierinfos/fetchdefault`,tempData,isloading);
        return res;
    }
}