import { Http } from '@/utils';
import { Util } from '@/utils';
import Product_supplierinfoServiceBase from './product-supplierinfo-service-base';


/**
 * 供应商价格表服务对象
 *
 * @export
 * @class Product_supplierinfoService
 * @extends {Product_supplierinfoServiceBase}
 */
export default class Product_supplierinfoService extends Product_supplierinfoServiceBase {

    /**
     * Creates an instance of  Product_supplierinfoService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_supplierinfoService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}