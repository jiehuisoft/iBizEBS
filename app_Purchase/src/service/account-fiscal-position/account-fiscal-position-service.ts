import { Http } from '@/utils';
import { Util } from '@/utils';
import Account_fiscal_positionServiceBase from './account-fiscal-position-service-base';


/**
 * 税科目调整服务对象
 *
 * @export
 * @class Account_fiscal_positionService
 * @extends {Account_fiscal_positionServiceBase}
 */
export default class Account_fiscal_positionService extends Account_fiscal_positionServiceBase {

    /**
     * Creates an instance of  Account_fiscal_positionService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_fiscal_positionService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}