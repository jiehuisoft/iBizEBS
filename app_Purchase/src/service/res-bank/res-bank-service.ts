import { Http } from '@/utils';
import { Util } from '@/utils';
import Res_bankServiceBase from './res-bank-service-base';


/**
 * 银行服务对象
 *
 * @export
 * @class Res_bankService
 * @extends {Res_bankServiceBase}
 */
export default class Res_bankService extends Res_bankServiceBase {

    /**
     * Creates an instance of  Res_bankService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_bankService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}