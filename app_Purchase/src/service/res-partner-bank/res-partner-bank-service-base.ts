import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 银行账户服务对象基类
 *
 * @export
 * @class Res_partner_bankServiceBase
 * @extends {EntityServie}
 */
export default class Res_partner_bankServiceBase extends EntityService {

    /**
     * Creates an instance of  Res_partner_bankServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partner_bankServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Res_partner_bankServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='res_partner_bank';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'res_partner_banks';
        this.APPDETEXT = 'id';
        this.APPNAME = 'purchase';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_bankServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/res_partner_banks`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/res_partner_banks`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_bankServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner_bank){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/res_partner_banks/${context.res_partner_bank}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/res_partner_banks/${context.res_partner_bank}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_bankServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner_bank){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/res_partner_banks/${context.res_partner_bank}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/res_partner_banks/${context.res_partner_bank}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_bankServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner_bank){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/res_partner_banks/${context.res_partner_bank}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/res_partner_banks/${context.res_partner_bank}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_bankServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && true){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/res_partner_banks/getdraft`,isloading);
            res.data.res_partner_bank = data.res_partner_bank;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/res_partner_banks/getdraft`,isloading);
        res.data.res_partner_bank = data.res_partner_bank;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_bankServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner_bank){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/res_partner_banks/${context.res_partner_bank}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/res_partner_banks/${context.res_partner_bank}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_bankServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner_bank){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/res_partner_banks/${context.res_partner_bank}/createbatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/res_partner_banks/${context.res_partner_bank}/createbatch`,data,isloading);
            return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_bankServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner_bank){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/res_partner_banks/${context.res_partner_bank}/removebatch`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/res_partner_banks/${context.res_partner_bank}/removebatch`,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_bankServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner_bank){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/res_partner_banks/${context.res_partner_bank}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/res_partner_banks/${context.res_partner_bank}/save`,data,isloading);
            
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_bankServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.res_partner_bank){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/res_partner_banks/${context.res_partner_bank}/updatebatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/res_partner_banks/${context.res_partner_bank}/updatebatch`,data,isloading);
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Res_partner_bankServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/res_partner_banks/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/res_partner_banks/fetchdefault`,tempData,isloading);
        return res;
    }
}