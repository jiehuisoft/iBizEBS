import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Res_supplierService from '@/service/res-supplier/res-supplier-service';
import MasterTabInfoViewtabexppanelModel from './master-tab-info-viewtabexppanel-tabexppanel-model';


/**
 * MasterTabInfoViewtabexppanel 部件服务对象
 *
 * @export
 * @class MasterTabInfoViewtabexppanelService
 */
export default class MasterTabInfoViewtabexppanelService extends ControlService {

    /**
     * 供应商服务对象
     *
     * @type {Res_supplierService}
     * @memberof MasterTabInfoViewtabexppanelService
     */
    public appEntityService: Res_supplierService = new Res_supplierService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof MasterTabInfoViewtabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of MasterTabInfoViewtabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof MasterTabInfoViewtabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new MasterTabInfoViewtabexppanelModel();
    }

    
}