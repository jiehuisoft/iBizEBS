import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Res_supplierService from '@/service/res-supplier/res-supplier-service';
import EF_MasterQuickService from './ef-master-quick-form-service';
import Res_supplierUIService from '@/uiservice/res-supplier/res-supplier-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {EF_MasterQuickEditFormBase}
 */
export class EF_MasterQuickEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterQuickEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {EF_MasterQuickService}
     * @memberof EF_MasterQuickEditFormBase
     */
    public service: EF_MasterQuickService = new EF_MasterQuickService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Res_supplierService}
     * @memberof EF_MasterQuickEditFormBase
     */
    public appEntityService: Res_supplierService = new Res_supplierService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterQuickEditFormBase
     */
    protected appDeName: string = 'res_supplier';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterQuickEditFormBase
     */
    protected appDeLogicName: string = '供应商';

    /**
     * 界面UI服务对象
     *
     * @type {Res_supplierUIService}
     * @memberof EF_MasterQuickBase
     */  
    public appUIService:Res_supplierUIService = new Res_supplierUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof EF_MasterQuickEditFormBase
     */
    public data: any = {
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        name: null,
        is_company: null,
        parent_name: null,
        ibizfunction: null,
        phone: null,
        mobile: null,
        title_text: null,
        id: null,
        title: null,
        parent_id: null,
        res_supplier:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof EF_MasterQuickEditFormBase
     */
    public majorMessageField: string = "name";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterQuickEditFormBase
     */
    public rules():any{
        return {
        is_company: [
            { required: this.detailsModel.is_company.required, type: 'string', message: '公司 值不能为空', trigger: 'change' },
            { required: this.detailsModel.is_company.required, type: 'string', message: '公司 值不能为空', trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterQuickBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof EF_MasterQuickEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.res_supplier.ef_masterquick_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: '名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '名称', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        is_company: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'is_company', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        parent_name: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'parent_name', visible: false, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        ibizfunction: new FormItemModel({ caption: '工作岗位', detailType: 'FORMITEM', name: 'ibizfunction', visible: false, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        phone: new FormItemModel({ caption: '电话', detailType: 'FORMITEM', name: 'phone', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        mobile: new FormItemModel({ caption: '手机', detailType: 'FORMITEM', name: 'mobile', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        title_text: new FormItemModel({ caption: '称谓', detailType: 'FORMITEM', name: 'title_text', visible: false, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        title: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'title', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        parent_id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'parent_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

    };

    /**
     * 表单项逻辑
     *
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @returns {Promise<void>}
     * @memberof EF_MasterQuickEditFormBase
     */
    public async formLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): Promise<void> {
                











        if (Object.is(name, '') || Object.is(name, 'is_company')) {
            let ret = false;
            const _is_company = this.data.is_company;
            if (this.$verify.testCond(_is_company, 'EQ', 'false')) {
                ret = true;
            }
            this.detailsModel.parent_name.setVisible(ret);
        }

        if (Object.is(name, '') || Object.is(name, 'is_company')) {
            let ret = false;
            const _is_company = this.data.is_company;
            if (this.$verify.testCond(_is_company, 'EQ', 'false')) {
                ret = true;
            }
            this.detailsModel.ibizfunction.setVisible(ret);
        }



        if (Object.is(name, '') || Object.is(name, 'is_company')) {
            let ret = false;
            const _is_company = this.data.is_company;
            if (this.$verify.testCond(_is_company, 'EQ', 'false')) {
                ret = true;
            }
            this.detailsModel.title_text.setVisible(ret);
        }




    }

    /**
     * 新建默认值
     * @memberof EF_MasterQuickEditFormBase
     */
    public createDefault(){                    
        if (this.data.hasOwnProperty('name')) {
            this.data['name'] = 'NEW';
        }
    }
}