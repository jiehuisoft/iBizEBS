import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Product_templateService from '@/service/product-template/product-template-service';
import EF_MasterService from './ef-master-form-service';
import Product_templateUIService from '@/uiservice/product-template/product-template-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {EF_MasterEditFormBase}
 */
export class EF_MasterEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {EF_MasterService}
     * @memberof EF_MasterEditFormBase
     */
    public service: EF_MasterService = new EF_MasterService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Product_templateService}
     * @memberof EF_MasterEditFormBase
     */
    public appEntityService: Product_templateService = new Product_templateService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterEditFormBase
     */
    protected appDeName: string = 'product_template';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterEditFormBase
     */
    protected appDeLogicName: string = '产品模板';

    /**
     * 界面UI服务对象
     *
     * @type {Product_templateUIService}
     * @memberof EF_MasterBase
     */  
    public appUIService:Product_templateUIService = new Product_templateUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        name: null,
        sale_ok: null,
        purchase_ok: null,
        can_be_expensed: null,
        type: null,
        categ_id_text: null,
        default_code: null,
        barcode: null,
        list_price: null,
        taxes_id: null,
        standard_price: null,
        company_id_text: null,
        uom_name: null,
        uom_po_id_text: null,
        id: null,
        company_id: null,
        uom_id: null,
        categ_id: null,
        uom_po_id: null,
        product_template:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public majorMessageField: string = "name";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public rules():any{
        return {
        name: [
            { required: this.detailsModel.name.required, type: 'string', message: '名称 值不能为空', trigger: 'change' },
            { required: this.detailsModel.name.required, type: 'string', message: '名称 值不能为空', trigger: 'blur' },
        ],
        type: [
            { required: this.detailsModel.type.required, type: 'string', message: '产品类型 值不能为空', trigger: 'change' },
            { required: this.detailsModel.type.required, type: 'string', message: '产品类型 值不能为空', trigger: 'blur' },
        ],
        categ_id_text: [
            { required: this.detailsModel.categ_id_text.required, type: 'string', message: '产品种类 值不能为空', trigger: 'change' },
            { required: this.detailsModel.categ_id_text.required, type: 'string', message: '产品种类 值不能为空', trigger: 'blur' },
        ],
        uom_name: [
            { required: this.detailsModel.uom_name.required, type: 'string', message: '单位 值不能为空', trigger: 'change' },
            { required: this.detailsModel.uom_name.required, type: 'string', message: '单位 值不能为空', trigger: 'blur' },
        ],
        uom_po_id_text: [
            { required: this.detailsModel.uom_po_id_text.required, type: 'string', message: '采购计量单位 值不能为空', trigger: 'change' },
            { required: this.detailsModel.uom_po_id_text.required, type: 'string', message: '采购计量单位 值不能为空', trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.product_template.ef_master_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '最后更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: '名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '名称', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        sale_ok: new FormItemModel({ caption: '销售', detailType: 'FORMITEM', name: 'sale_ok', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        purchase_ok: new FormItemModel({ caption: '采购', detailType: 'FORMITEM', name: 'purchase_ok', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        can_be_expensed: new FormItemModel({ caption: '报销', detailType: 'FORMITEM', name: 'can_be_expensed', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        type: new FormItemModel({ caption: '产品类型', detailType: 'FORMITEM', name: 'type', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        categ_id_text: new FormItemModel({ caption: '产品种类', detailType: 'FORMITEM', name: 'categ_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        default_code: new FormItemModel({ caption: '内部参考', detailType: 'FORMITEM', name: 'default_code', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        barcode: new FormItemModel({ caption: '条码', detailType: 'FORMITEM', name: 'barcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        list_price: new FormItemModel({ caption: '销售价格', detailType: 'FORMITEM', name: 'list_price', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        taxes_id: new FormItemModel({ caption: '销项税', detailType: 'FORMITEM', name: 'taxes_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        standard_price: new FormItemModel({ caption: '成本', detailType: 'FORMITEM', name: 'standard_price', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        company_id_text: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        uom_name: new FormItemModel({ caption: '单位', detailType: 'FORMITEM', name: 'uom_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 0 }),

        uom_po_id_text: new FormItemModel({ caption: '采购计量单位', detailType: 'FORMITEM', name: 'uom_po_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        company_id: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        uom_id: new FormItemModel({ caption: '计量单位', detailType: 'FORMITEM', name: 'uom_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        categ_id: new FormItemModel({ caption: '产品种类', detailType: 'FORMITEM', name: 'categ_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        uom_po_id: new FormItemModel({ caption: '采购计量单位', detailType: 'FORMITEM', name: 'uom_po_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

    };
}