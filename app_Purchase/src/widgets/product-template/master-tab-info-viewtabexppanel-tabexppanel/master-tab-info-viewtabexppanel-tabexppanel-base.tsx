import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, TabExpPanelControlBase } from '@/studio-core';
import Product_templateService from '@/service/product-template/product-template-service';
import MasterTabInfoViewtabexppanelService from './master-tab-info-viewtabexppanel-tabexppanel-service';
import Product_templateUIService from '@/uiservice/product-template/product-template-ui-service';
import Product_templateAuthService from '@/authservice/product-template/product-template-auth-service';
import { Environment } from '@/environments/environment';


/**
 * tabexppanel部件基类
 *
 * @export
 * @class TabExpPanelControlBase
 * @extends {MasterTabInfoViewtabexppanelTabexppanelBase}
 */
export class MasterTabInfoViewtabexppanelTabexppanelBase extends TabExpPanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    protected controlType: string = 'TABEXPPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {MasterTabInfoViewtabexppanelService}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    public service: MasterTabInfoViewtabexppanelService = new MasterTabInfoViewtabexppanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Product_templateService}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    public appEntityService: Product_templateService = new Product_templateService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    protected appDeName: string = 'product_template';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    protected appDeLogicName: string = '产品模板';

    /**
     * 界面UI服务对象
     *
     * @type {Product_templateUIService}
     * @memberof MasterTabInfoViewtabexppanelBase
     */  
    public appUIService:Product_templateUIService = new Product_templateUIService(this.$store);

    /**
     * 是否初始化
     *
     * @protected
     * @returns {any}
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected isInit: any = {
        tabviewpanel21:  true ,
        tabviewpanel22:  false ,
    }

    /**
     * 被激活的分页面板
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected activatedTabViewPanel: string = 'tabviewpanel21';

    /**
     * 实体权限服务对象
     *
     * @protected
     * @type Product_templateAuthServiceBase
     * @memberof TabExpViewtabexppanelBase
     */
    protected appAuthService: Product_templateAuthService = new Product_templateAuthService();

    /**
     * 分页面板权限标识存储对象
     *
     * @protected
     * @type {*}
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected authResourceObject:any = {'tabviewpanel21':{resourcetag:null,visabled: true,disabled: false},'tabviewpanel22':{resourcetag:null,visabled: true,disabled: false}};

    /**
     * 组件创建完毕
     *
     * @protected
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected ctrlCreated(): void {
        //设置分页导航srfparentdename和srfparentkey
        if (this.context.product_template) {
            Object.assign(this.context, { srfparentdename: 'Product_template', srfparentkey: this.context.product_template });
        }
        super.ctrlCreated();
    }
}