import { ViewTool } from '@/utils';

/**
 * PurchaseIndexView 部件模型
 *
 * @export
 * @class PurchaseIndexViewModel
 */
export default class PurchaseIndexViewModel {

    /**
     * 菜单项集合
     *
     * @private
     * @type {any[]}
     * @memberof PurchaseIndexViewModel
     */
    private items: any[] = [
                {
        	id: 'f4544f3d016e5fb604fd0456f1a32454',
        	name: 'user_menus',
        	text: '用户菜单',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '用户菜单',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'Purchase-PurchaseIndexView-user_menus',
        }
        ,
                {
        	id: 'd8feda1391fcacd886e4465ee402a647',
        	name: 'top_menus',
        	text: '顶部菜单',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '顶部菜单',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'Purchase-PurchaseIndexView-top_menus',
        }
        ,
                {
        	id: 'f73e95565861fc7af722df56da462602',
        	name: 'left_exp',
        	text: '左侧菜单',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '左侧菜单',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'Purchase-PurchaseIndexView-left_exp',
        	items: [
                		        {
                	id: '1220a09ac290ff7dd0a1c8fe8ddda4dc',
                	name: 'menuitem1',
                	text: '订单',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '订单',
                	expanded: true,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-file-text-o',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	authtag:'Purchase-PurchaseIndexView-menuitem1',
                	items: [
                        		        {
                        	id: '7bce46ca071d99d3795dac44260d3926',
                        	name: 'menuitem2',
                        	text: '采购申请',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '采购申请',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-pencil-square-o',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'purchase-requisition-master-grid-view',
                        	authtag:'Purchase-PurchaseIndexView-menuitem2',
                        }
                        ,
                        		        {
                        	id: '99d4d1ad756a66ba71ba50e9dd06d8b3',
                        	name: 'menuitem3',
                        	text: '询价单',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '询价单',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-phone',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc2',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'purchase-order-master-grid-view-enquiry',
                        	authtag:'Purchase-PurchaseIndexView-menuitem3',
                        }
                        ,
                        		        {
                        	id: '4610919ce0cd1f5c75b4e9cdd31708a1',
                        	name: 'menuitem4',
                        	text: '采购订单',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '采购订单',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-file-text',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc3',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'purchase-order-master-grid-view-order',
                        	authtag:'Purchase-PurchaseIndexView-menuitem4',
                        }
                        ,
                        		        {
                        	id: 'd37fda11325d9248b91da6777f39b71f',
                        	name: 'menuitem5',
                        	text: '供应商',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '供应商',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-users',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc4',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'res-supplier-master-grid-view',
                        	authtag:'Purchase-PurchaseIndexView-menuitem5',
                        }
                        ,
                	],
                }
                ,
                		        {
                	id: 'a8981990310dec97e014330bd8f3ba0e',
                	name: 'menuitem7',
                	text: '产品',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '产品',
                	expanded: true,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-cube',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	authtag:'Purchase-PurchaseIndexView-menuitem7',
                	items: [
                        		        {
                        	id: 'c74ecc448396092d0ddcfa47bfc07a20',
                        	name: 'menuitem6',
                        	text: '产品',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '产品',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cube',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc5',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'product-template-master-grid-view',
                        	authtag:'Purchase-PurchaseIndexView-menuitem6',
                        }
                        ,
                        		        {
                        	id: '88efdc699f8066fec7d7a18d7c1eb1a1',
                        	name: 'menuitem8',
                        	text: '产品变种',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '产品变种',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cubes',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc6',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'product-product-master-grid-view',
                        	authtag:'Purchase-PurchaseIndexView-menuitem8',
                        }
                        ,
                	],
                }
                ,
                		        {
                	id: '1f7476c2799e1a61c0823ba4007ef7b8',
                	name: 'menuitem9',
                	text: '配置',
                	type: 'MENUITEM',
                	counterid: '',
                	tooltip: '配置',
                	expanded: true,
                	separator: false,
                	hidden: false,
                	hidesidebar: false,
                	opendefault: false,
                	iconcls: 'fa fa-cogs',
                	icon: '',
                	textcls: '',
                	appfunctag: '',
                	authtag:'Purchase-PurchaseIndexView-menuitem9',
                	items: [
                        		        {
                        	id: '2d6976744e750888734f825b9956bc75',
                        	name: 'menuitem15',
                        	text: '采购设置',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '采购设置',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc12',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'res-config-settings-purchase-edit-view',
                        	authtag:'Purchase-PurchaseIndexView-menuitem15',
                        }
                        ,
                        		        {
                        	id: 'd99503d5d6e13217f7c129b27f02cbb0',
                        	name: 'menuitem10',
                        	text: '供应商价格表',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '供应商价格表',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-list-ol',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc7',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'product-supplierinfo-basic-list-exp-view',
                        	authtag:'Purchase-PurchaseIndexView-menuitem10',
                        }
                        ,
                        		        {
                        	id: '504ed105c34439c612e7e457441f0d0f',
                        	name: 'menuitem11',
                        	text: '采购申请类型',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '采购申请类型',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc8',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'purchase-requisition-type-basic-list-exp-view',
                        	authtag:'Purchase-PurchaseIndexView-menuitem11',
                        }
                        ,
                        		        {
                        	id: 'a1d39bf75aa7a925bd3d1db1b89e41a5',
                        	name: 'menuitem12',
                        	text: '产品类别',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '产品类别',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-dashcube',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc9',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'product-category-basic-list-exp-view',
                        	authtag:'Purchase-PurchaseIndexView-menuitem12',
                        }
                        ,
                        		        {
                        	id: '9b0837782821d05dcaa86f74d2be2deb',
                        	name: 'menuitem13',
                        	text: '计量单位',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '计量单位',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc10',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'uom-uom-basic-list-exp-view',
                        	authtag:'Purchase-PurchaseIndexView-menuitem13',
                        }
                        ,
                        		        {
                        	id: '02e12fb0b17a0f9dae3f2eef6ebce827',
                        	name: 'menuitem14',
                        	text: '计量单位类别',
                        	type: 'MENUITEM',
                        	counterid: '',
                        	tooltip: '计量单位类别',
                        	expanded: false,
                        	separator: false,
                        	hidden: false,
                        	hidesidebar: false,
                        	opendefault: false,
                        	iconcls: 'fa fa-cog',
                        	icon: '',
                        	textcls: '',
                        	appfunctag: 'AppFunc11',
                        	appfuncyype: 'APPVIEW',
                        	viewname: 'uom-category-basic-list-exp-view',
                        	authtag:'Purchase-PurchaseIndexView-menuitem14',
                        }
                        ,
                	],
                }
                ,
        	],
        }
        ,
                {
        	id: '7f0092f838d0eadf499172aa0ea465ae',
        	name: 'bottom_exp',
        	text: '底部内容',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '底部内容',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'Purchase-PurchaseIndexView-bottom_exp',
        }
        ,
                {
        	id: '9ca6e13581f37bc069572c03208f5d3c',
        	name: 'footer_left',
        	text: '底部左侧',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '底部左侧',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'Purchase-PurchaseIndexView-footer_left',
        }
        ,
                {
        	id: '1e24737e8eb269ca9d2183c3a3b096e5',
        	name: 'footer_center',
        	text: '底部中间',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '底部中间',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'Purchase-PurchaseIndexView-footer_center',
        }
        ,
                {
        	id: '21253fa5a62ed6fc1a15b7a78cf47827',
        	name: 'footer_right',
        	text: '底部右侧',
        	type: 'MENUITEM',
        	counterid: '',
        	tooltip: '底部右侧',
        	expanded: false,
        	separator: false,
        	hidden: false,
        	hidesidebar: false,
        	opendefault: false,
        	iconcls: '',
        	icon: '',
        	textcls: '',
        	appfunctag: '',
        	authtag:'Purchase-PurchaseIndexView-footer_right',
        }
        ,
    ];

	/**
	 * 应用功能集合
	 *
	 * @private
	 * @type {any[]}
	 * @memberof PurchaseIndexViewModel
	 */
	private funcs: any[] = [
        {
            appfunctag: 'AppFunc2',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'purchase_ordermastergridview_enquiry',
            deResParameters: [],
            routepath: '/purchaseindexview/:purchaseindexview?/purchase_orders/:purchase_order?/mastergridview_enquiry/:mastergridview_enquiry?',
            parameters: [
                { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                { pathName: 'mastergridview_enquiry', parameterName: 'mastergridview_enquiry' },
            ],
        },
        {
            appfunctag: 'AppFunc11',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'uom_categorybasiclistexpview',
            deResParameters: [],
            routepath: '/purchaseindexview/:purchaseindexview?/uom_categories/:uom_category?/basiclistexpview/:basiclistexpview?',
            parameters: [
                { pathName: 'uom_categories', parameterName: 'uom_category' },
                { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
            ],
        },
        {
            appfunctag: 'AppFunc9',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'product_categorybasiclistexpview',
            deResParameters: [],
            routepath: '/purchaseindexview/:purchaseindexview?/product_categories/:product_category?/basiclistexpview/:basiclistexpview?',
            parameters: [
                { pathName: 'product_categories', parameterName: 'product_category' },
                { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
            ],
        },
        {
            appfunctag: 'AppFunc6',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'product_productmastergridview',
            deResParameters: [],
            routepath: '/purchaseindexview/:purchaseindexview?/product_products/:product_product?/mastergridview/:mastergridview?',
            parameters: [
                { pathName: 'product_products', parameterName: 'product_product' },
                { pathName: 'mastergridview', parameterName: 'mastergridview' },
            ],
        },
        {
            appfunctag: 'AppFunc10',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'uom_uombasiclistexpview',
            deResParameters: [],
            routepath: '/purchaseindexview/:purchaseindexview?/uom_uoms/:uom_uom?/basiclistexpview/:basiclistexpview?',
            parameters: [
                { pathName: 'uom_uoms', parameterName: 'uom_uom' },
                { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
            ],
        },
        {
            appfunctag: 'AppFunc5',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'product_templatemastergridview',
            deResParameters: [],
            routepath: '/purchaseindexview/:purchaseindexview?/product_templates/:product_template?/mastergridview/:mastergridview?',
            parameters: [
                { pathName: 'product_templates', parameterName: 'product_template' },
                { pathName: 'mastergridview', parameterName: 'mastergridview' },
            ],
        },
        {
            appfunctag: 'AppFunc3',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'purchase_ordermastergridview_order',
            deResParameters: [],
            routepath: '/purchaseindexview/:purchaseindexview?/purchase_orders/:purchase_order?/mastergridview_order/:mastergridview_order?',
            parameters: [
                { pathName: 'purchase_orders', parameterName: 'purchase_order' },
                { pathName: 'mastergridview_order', parameterName: 'mastergridview_order' },
            ],
        },
        {
            appfunctag: 'AppFunc12',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'res_config_settingspurchaseeditview',
            deResParameters: [],
            routepath: '/purchaseindexview/:purchaseindexview?/res_config_settings/:res_config_settings?/purchaseeditview/:purchaseeditview?',
            parameters: [
                { pathName: 'res_config_settings', parameterName: 'res_config_settings' },
                { pathName: 'purchaseeditview', parameterName: 'purchaseeditview' },
            ],
        },
        {
            appfunctag: 'AppFunc7',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'product_supplierinfobasiclistexpview',
            deResParameters: [],
            routepath: '/purchaseindexview/:purchaseindexview?/product_supplierinfos/:product_supplierinfo?/basiclistexpview/:basiclistexpview?',
            parameters: [
                { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
                { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
            ],
        },
        {
            appfunctag: 'AppFunc4',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'res_suppliermastergridview',
            deResParameters: [],
            routepath: '/purchaseindexview/:purchaseindexview?/res_suppliers/:res_supplier?/mastergridview/:mastergridview?',
            parameters: [
                { pathName: 'res_suppliers', parameterName: 'res_supplier' },
                { pathName: 'mastergridview', parameterName: 'mastergridview' },
            ],
        },
        {
            appfunctag: 'AppFunc8',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'purchase_requisition_typebasiclistexpview',
            deResParameters: [],
            routepath: '/purchaseindexview/:purchaseindexview?/purchase_requisition_types/:purchase_requisition_type?/basiclistexpview/:basiclistexpview?',
            parameters: [
                { pathName: 'purchase_requisition_types', parameterName: 'purchase_requisition_type' },
                { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
            ],
        },
        {
            appfunctag: 'AppFunc',
            appfuncyype: 'APPVIEW',
            openmode: '',
            codename: 'purchase_requisitionmastergridview',
            deResParameters: [],
            routepath: '/purchaseindexview/:purchaseindexview?/purchase_requisitions/:purchase_requisition?/mastergridview/:mastergridview?',
            parameters: [
                { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
                { pathName: 'mastergridview', parameterName: 'mastergridview' },
            ],
        },
	];

	/**
	 * 根据当前路由查找激活菜单
	 *
	 * @param {*} route
	 * @returns {*}
	 * @memberof PurchaseIndexViewModel
	 */
	public findActiveMenuByRoute(route: any): any {
		if (route) {
			const func = this.funcs.find((item: any) => {
				if (item.openmode === '') {
					const url: string = ViewTool.buildUpRoutePath(route, route.params, [], item.parameters, [], {});
					return url === route.fullPath;
				}
			});
            if (func) {
			    return this.findMenuByFuncTag(func.appfunctag);
            }
		}
	}

	/**
	 * 根据应用功能id查找菜单项
	 *
	 * @param {string} tag
	 * @param {any[]} [menus=this.items]
	 * @returns {*}
	 * @memberof PurchaseIndexViewModel
	 */
	public findMenuByFuncTag(tag: string, menus: any[] = this.items): any {
		let menu: any;
		menus.every((item: any) => {
			if (item.appfunctag === tag) {
				menu = item;
				return false;
			}
			if (item.items) {
				menu = this.findMenuByFuncTag(tag, item.items);
				if (menu) {
					return false;
				}
			}
			return true;
		});
		return menu;
	}

	/**
	 * 查找默认打开菜单
	 *
	 * @param {any[]} [menus=this.items]
	 * @returns {*}
	 * @memberof PurchaseIndexViewModel
	 */
	public findDefaultOpenMenu(menus: any[] = this.items): any {
		let menu: any;
		menus.every((item: any) => {
			if (item.opendefault === true) {
				menu = item;
				return false;
			}
			if (item.items) {
				menu = this.findMenuByFuncTag(item.items);
				if (menu) {
					return false;
				}
			}
			return true;
		});
		return menu;
	}

    /**
     * 获取所有菜单项集合
     *
     * @returns {any[]}
     * @memberof PurchaseIndexViewModel
     */
    public getAppMenuItems(): any[] {
        return this.items;
    }

	/**
	 * 根据名称获取菜单组
	 *
	 * @param {string} name
	 * @returns {*}
	 * @memberof PurchaseIndexViewModel
	 */
	public getMenuGroup(name: string): any {
		return this.items.find((item: any) => Object.is(item.name, name));
	}

    /**
     * 获取所有应用功能集合
     *
     * @returns {any[]}
     * @memberof PurchaseIndexViewModel
     */
    public getAppFuncs(): any[] {
        return this.funcs;
    }
}