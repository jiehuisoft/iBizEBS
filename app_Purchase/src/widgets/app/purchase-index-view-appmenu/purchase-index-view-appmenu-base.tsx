import { Vue } from 'vue-property-decorator';

/**
 * 应用菜单基类
 */
export class PurchaseIndexViewBase extends Vue {

    /**
     * 获取应用上下文
     *
     * @memberof PurchaseIndexViewBase
     */
    get context(): any {
        return this.$appService.contextStore.appContext || {};
    }

    /**
     * 菜单点击
     *
     * @param {*} item 菜单数据
     * @memberof PurchaseIndexViewBase
     */
    public click(item: any) {
        if (item) {
            let judge = true;
            switch (item.appfunctag) {
                case 'AppFunc2': 
                    this.clickAppFunc2(item); break;
                case 'AppFunc11': 
                    this.clickAppFunc11(item); break;
                case 'AppFunc9': 
                    this.clickAppFunc9(item); break;
                case 'AppFunc6': 
                    this.clickAppFunc6(item); break;
                case 'AppFunc10': 
                    this.clickAppFunc10(item); break;
                case 'AppFunc5': 
                    this.clickAppFunc5(item); break;
                case 'AppFunc3': 
                    this.clickAppFunc3(item); break;
                case 'AppFunc12': 
                    this.clickAppFunc12(item); break;
                case 'AppFunc7': 
                    this.clickAppFunc7(item); break;
                case 'AppFunc4': 
                    this.clickAppFunc4(item); break;
                case 'AppFunc8': 
                    this.clickAppFunc8(item); break;
                case 'AppFunc': 
                    this.clickAppFunc(item); break;
                default:
                    judge = false;
                    console.warn('未指定应用功能');
            }
            if (judge && this.$uiState.isStyle2()) {
                this.$appService.navHistory.reset();
                this.$appService.viewStore.reset();
            }
        }
    }
    
    /**
     * 询价单
     *
     * @param {*} [item={}]
     * @memberof PurchaseIndexView
     */
    public clickAppFunc2(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
            { pathName: 'mastergridview_enquiry', parameterName: 'mastergridview_enquiry' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 计量单位类别
     *
     * @param {*} [item={}]
     * @memberof PurchaseIndexView
     */
    public clickAppFunc11(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'uom_categories', parameterName: 'uom_category' },
            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 产品类别
     *
     * @param {*} [item={}]
     * @memberof PurchaseIndexView
     */
    public clickAppFunc9(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'product_categories', parameterName: 'product_category' },
            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 产品变种
     *
     * @param {*} [item={}]
     * @memberof PurchaseIndexView
     */
    public clickAppFunc6(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'product_products', parameterName: 'product_product' },
            { pathName: 'mastergridview', parameterName: 'mastergridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 计量单位
     *
     * @param {*} [item={}]
     * @memberof PurchaseIndexView
     */
    public clickAppFunc10(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'uom_uoms', parameterName: 'uom_uom' },
            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 产品
     *
     * @param {*} [item={}]
     * @memberof PurchaseIndexView
     */
    public clickAppFunc5(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'product_templates', parameterName: 'product_template' },
            { pathName: 'mastergridview', parameterName: 'mastergridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 采购订单
     *
     * @param {*} [item={}]
     * @memberof PurchaseIndexView
     */
    public clickAppFunc3(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'purchase_orders', parameterName: 'purchase_order' },
            { pathName: 'mastergridview_order', parameterName: 'mastergridview_order' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 采购设置
     *
     * @param {*} [item={}]
     * @memberof PurchaseIndexView
     */
    public clickAppFunc12(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'res_config_settings', parameterName: 'res_config_settings' },
            { pathName: 'purchaseeditview', parameterName: 'purchaseeditview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 供应商价格表
     *
     * @param {*} [item={}]
     * @memberof PurchaseIndexView
     */
    public clickAppFunc7(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 供应商
     *
     * @param {*} [item={}]
     * @memberof PurchaseIndexView
     */
    public clickAppFunc4(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
            { pathName: 'mastergridview', parameterName: 'mastergridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 采购申请类型
     *
     * @param {*} [item={}]
     * @memberof PurchaseIndexView
     */
    public clickAppFunc8(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'purchase_requisition_types', parameterName: 'purchase_requisition_type' },
            { pathName: 'basiclistexpview', parameterName: 'basiclistexpview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }
    
    /**
     * 采购申请
     *
     * @param {*} [item={}]
     * @memberof PurchaseIndexView
     */
    public clickAppFunc(item: any = {}) {
        const viewparam: any = {};
        Object.assign(viewparam, {});
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'purchase_requisitions', parameterName: 'purchase_requisition' },
            { pathName: 'mastergridview', parameterName: 'mastergridview' },
        ];
        const path: string = this.$viewTool.buildUpRoutePath(this.$route, {}, deResParameters, parameters, [], viewparam);
        if(Object.is(this.$route.fullPath,path)){
            return;
        }
        this.$nextTick(function(){
            this.$router.push(path);
        })
    }

    /**
     * 绘制内容
     *
     * @private
     * @memberof PurchaseIndexViewBase
     */
    public render(): any {
        return <span style="display: none;"/>
    }

}