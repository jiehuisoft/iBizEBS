import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, GridControlBase } from '@/studio-core';
import Purchase_requisition_lineService from '@/service/purchase-requisition-line/purchase-requisition-line-service';
import LineEditService from './line-edit-grid-service';
import Purchase_requisition_lineUIService from '@/uiservice/purchase-requisition-line/purchase-requisition-line-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {LineEditGridBase}
 */
export class LineEditGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof LineEditGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {LineEditService}
     * @memberof LineEditGridBase
     */
    public service: LineEditService = new LineEditService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Purchase_requisition_lineService}
     * @memberof LineEditGridBase
     */
    public appEntityService: Purchase_requisition_lineService = new Purchase_requisition_lineService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LineEditGridBase
     */
    protected appDeName: string = 'purchase_requisition_line';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof LineEditGridBase
     */
    protected appDeLogicName: string = '采购申请行';

    /**
     * 界面UI服务对象
     *
     * @type {Purchase_requisition_lineUIService}
     * @memberof LineEditBase
     */  
    public appUIService:Purchase_requisition_lineUIService = new Purchase_requisition_lineUIService(this.$store);


    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof LineEditBase
     */  
    public ActionModel: any = {
    };

    /**
     * 主信息表格列
     *
     * @type {string}
     * @memberof LineEditBase
     */  
    public majorInfoColName:string = "";


    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof LineEditBase
     */
    protected localStorageTag: string = 'purchase_requisition_line_lineedit_grid';

    /**
     * 是否支持分页
     *
     * @type {boolean}
     * @memberof LineEditGridBase
     */
    public isEnablePagingBar: boolean = false;

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof LineEditGridBase
     */
    public allColumns: any[] = [
        {
            name: 'product_name',
            label: '产品',
            langtag: 'entities.purchase_requisition_line.lineedit_grid.columns.product_name',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
            enableCond: 3 ,
        },
        {
            name: 'product_qty',
            label: '数量',
            langtag: 'entities.purchase_requisition_line.lineedit_grid.columns.product_qty',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
            enableCond: 3 ,
        },
        {
            name: 'qty_ordered',
            label: '已订购数量',
            langtag: 'entities.purchase_requisition_line.lineedit_grid.columns.qty_ordered',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
            enableCond: 3 ,
        },
        {
            name: 'product_uom_name',
            label: '单位',
            langtag: 'entities.purchase_requisition_line.lineedit_grid.columns.product_uom_name',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
            enableCond: 3 ,
        },
        {
            name: 'schedule_date',
            label: '安排的日期',
            langtag: 'entities.purchase_requisition_line.lineedit_grid.columns.schedule_date',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
            enableCond: 3 ,
        },
        {
            name: 'price_unit',
            label: '价格',
            langtag: 'entities.purchase_requisition_line.lineedit_grid.columns.price_unit',
            show: true,
            unit: 'PX',
            isEnableRowEdit: true,
            enableCond: 3 ,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof LineEditGridBase
     */
    public getGridRowModel(){
        return {
          product_qty: new FormItemModel(),
          product_uom_id: new FormItemModel(),
          product_uom_name: new FormItemModel(),
          product_id: new FormItemModel(),
          qty_ordered: new FormItemModel(),
          product_name: new FormItemModel(),
          price_unit: new FormItemModel(),
          srfkey: new FormItemModel(),
          schedule_date: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof LineEditGridBase
     */
    public rules: any = {
        product_qty: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '数量 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '数量 值不能为空', trigger: 'blur' },
        ],
        product_uom_id: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        product_uom_name: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '单位 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '单位 值不能为空', trigger: 'blur' },
        ],
        product_id: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        qty_ordered: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '已订购数量 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '已订购数量 值不能为空', trigger: 'blur' },
        ],
        product_name: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '产品 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '产品 值不能为空', trigger: 'blur' },
        ],
        price_unit: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '价格 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '价格 值不能为空', trigger: 'blur' },
        ],
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'blur' },
        ],
        schedule_date: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '安排的日期 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '安排的日期 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof LineEditBase
     */
    public deRules:any = {
    };

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof LineEditBase
     */
    public hasRowEdit: any = {
        'product_name':true,
        'product_qty':true,
        'qty_ordered':true,
        'product_uom_name':true,
        'schedule_date':true,
        'price_unit':true,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof LineEditBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof LineEditGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }


    /**
     * 更新默认值
     * @param {*}  row 行数据
     * @memberof LineEditBase
     */
    public updateDefault(row: any){                    
    }

    /**
     * 计算数据对象类型的默认值
     * @param {string}  action 行为
     * @param {string}  param 默认值参数
     * @param {*}  data 当前行数据
     * @memberof LineEditBase
     */
    public computeDefaultValueWithParam(action:string,param:string,data:any){
        if(Object.is(action,"UPDATE")){
            const nativeData:any = this.service.getCopynativeData();
            if(nativeData && (nativeData instanceof Array) && nativeData.length >0){
                let targetData:any = nativeData.find((item:any) =>{
                    return item.id === data.srfkey;
                })
                if(targetData){
                    return targetData[param]?targetData[param]:null;
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
           return this.service.getRemoteCopyData()[param]?this.service.getRemoteCopyData()[param]:null;
        }
    }


}