/**
 * Master 部件模型
 *
 * @export
 * @class MasterModel
 */
export default class MasterModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MasterGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MasterGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'name',
          prop: 'name',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'default_code',
          prop: 'default_code',
          dataType: 'TEXT',
        },
        {
          name: 'attribute_value_ids',
          prop: 'attribute_value_ids',
          dataType: 'LONGTEXT',
        },
        {
          name: 'list_price',
          prop: 'list_price',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'standard_price',
          prop: 'standard_price',
          dataType: 'FLOAT',
        },
        {
          name: 'qty_available',
          prop: 'qty_available',
          dataType: 'FLOAT',
        },
        {
          name: 'virtual_available',
          prop: 'virtual_available',
          dataType: 'FLOAT',
        },
        {
          name: 'uom_name',
          prop: 'uom_name',
          dataType: 'PICKUPDATA',
        },
        {
          name: 'product_tmpl_id',
          prop: 'product_tmpl_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'product_product',
          prop: 'id',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}