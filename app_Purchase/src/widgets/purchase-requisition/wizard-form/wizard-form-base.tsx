import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Purchase_requisitionService from '@/service/purchase-requisition/purchase-requisition-service';
import WizardService from './wizard-form-service';
import Purchase_requisitionUIService from '@/uiservice/purchase-requisition/purchase-requisition-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * statewizardpanel_form_cancel部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {WizardEditFormBase}
 */
export class WizardEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof WizardEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {WizardService}
     * @memberof WizardEditFormBase
     */
    public service: WizardService = new WizardService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Purchase_requisitionService}
     * @memberof WizardEditFormBase
     */
    public appEntityService: Purchase_requisitionService = new Purchase_requisitionService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof WizardEditFormBase
     */
    protected appDeName: string = 'purchase_requisition';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof WizardEditFormBase
     */
    protected appDeLogicName: string = '采购申请';

    /**
     * 界面UI服务对象
     *
     * @type {Purchase_requisitionUIService}
     * @memberof WizardBase
     */  
    public appUIService:Purchase_requisitionUIService = new Purchase_requisitionUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof WizardEditFormBase
     */
    public data: any = {
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        name: null,
        id: null,
        purchase_requisition:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof WizardEditFormBase
     */
    public majorMessageField: string = "name";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof WizardEditFormBase
     */
    public rules():any{
        return {
        name: [
            { required: this.detailsModel.name.required, type: 'string', message: '申请编号 值不能为空', trigger: 'change' },
            { required: this.detailsModel.name.required, type: 'string', message: '申请编号 值不能为空', trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof WizardBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof WizardEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '采购申请基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.purchase_requisition.wizard_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '申请编号', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '申请编号', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

    };
}