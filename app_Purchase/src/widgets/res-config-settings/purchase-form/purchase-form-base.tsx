import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Res_config_settingsService from '@/service/res-config-settings/res-config-settings-service';
import PurchaseService from './purchase-form-service';
import Res_config_settingsUIService from '@/uiservice/res-config-settings/res-config-settings-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {PurchaseEditFormBase}
 */
export class PurchaseEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof PurchaseEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {PurchaseService}
     * @memberof PurchaseEditFormBase
     */
    public service: PurchaseService = new PurchaseService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Res_config_settingsService}
     * @memberof PurchaseEditFormBase
     */
    public appEntityService: Res_config_settingsService = new Res_config_settingsService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof PurchaseEditFormBase
     */
    protected appDeName: string = 'res_config_settings';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof PurchaseEditFormBase
     */
    protected appDeLogicName: string = '配置设定';

    /**
     * 界面UI服务对象
     *
     * @type {Res_config_settingsUIService}
     * @memberof PurchaseBase
     */  
    public appUIService:Res_config_settingsUIService = new Res_config_settingsUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof PurchaseEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        company_id: null,
        po_order_approval: null,
        po_double_validation: null,
        po_double_validation_amount: null,
        lock_confirmed_po: null,
        group_warning_purchase: null,
        module_purchase_requisition: null,
        po_lock: null,
        po_lead: null,
        purchase_tax_id: null,
        default_purchase_method: null,
        module_account_3way_match: null,
        module_stock_dropshipping: null,
        id: null,
        res_config_settings:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof PurchaseEditFormBase
     */
    public majorMessageField: string = "";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof PurchaseEditFormBase
     */
    public rules():any{
        return {
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof PurchaseBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof PurchaseEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '订单', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.res_config_settings.purchase_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '开票', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.res_config_settings.purchase_form', extractMode: 'ITEM', details: [] } }),

        grouppanel2: new FormGroupPanelModel({ caption: '产品', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.res_config_settings.purchase_form', extractMode: 'ITEM', details: [] } }),

        grouppanel3: new FormGroupPanelModel({ caption: '物流', detailType: 'GROUPPANEL', name: 'grouppanel3', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.res_config_settings.purchase_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '最后更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        company_id: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        po_order_approval: new FormItemModel({ caption: '采购订单批准', detailType: 'FORMITEM', name: 'po_order_approval', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        po_double_validation: new FormItemModel({ caption: '审批层级 *', detailType: 'FORMITEM', name: 'po_double_validation', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        po_double_validation_amount: new FormItemModel({ caption: '最小金额', detailType: 'FORMITEM', name: 'po_double_validation_amount', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        lock_confirmed_po: new FormItemModel({ caption: '锁定确认订单', detailType: 'FORMITEM', name: 'lock_confirmed_po', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        group_warning_purchase: new FormItemModel({ caption: '采购警告', detailType: 'FORMITEM', name: 'group_warning_purchase', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        module_purchase_requisition: new FormItemModel({ caption: '采购招标', detailType: 'FORMITEM', name: 'module_purchase_requisition', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        po_lock: new FormItemModel({ caption: '采购订单修改 *', detailType: 'FORMITEM', name: 'po_lock', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        po_lead: new FormItemModel({ caption: '采购提前时间', detailType: 'FORMITEM', name: 'po_lead', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        purchase_tax_id: new FormItemModel({ caption: '默认进项税', detailType: 'FORMITEM', name: 'purchase_tax_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        default_purchase_method: new FormItemModel({ caption: '账单控制', detailType: 'FORMITEM', name: 'default_purchase_method', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        module_account_3way_match: new FormItemModel({ caption: '三方匹配:采购，收货和发票', detailType: 'FORMITEM', name: 'module_account_3way_match', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        module_stock_dropshipping: new FormItemModel({ caption: '代发货', detailType: 'FORMITEM', name: 'module_stock_dropshipping', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

    };
}