import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * MessageInfo 部件服务对象
 *
 * @export
 * @class MessageInfoService
 */
export default class MessageInfoService extends ControlService {
}
