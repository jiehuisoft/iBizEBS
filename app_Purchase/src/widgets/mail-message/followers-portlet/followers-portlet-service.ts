import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Followers 部件服务对象
 *
 * @export
 * @class FollowersService
 */
export default class FollowersService extends ControlService {
}
