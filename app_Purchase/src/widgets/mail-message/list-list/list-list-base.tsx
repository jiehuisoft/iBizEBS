import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, ListControlBase } from '@/studio-core';
import Mail_messageService from '@/service/mail-message/mail-message-service';
import ListService from './list-list-service';
import Mail_messageUIService from '@/uiservice/mail-message/mail-message-ui-service';


/**
 * list部件基类
 *
 * @export
 * @class ListControlBase
 * @extends {ListListBase}
 */
export class ListListBase extends ListControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ListListBase
     */
    protected controlType: string = 'LIST';

    /**
     * 建构部件服务对象
     *
     * @type {ListService}
     * @memberof ListListBase
     */
    public service: ListService = new ListService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Mail_messageService}
     * @memberof ListListBase
     */
    public appEntityService: Mail_messageService = new Mail_messageService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ListListBase
     */
    protected appDeName: string = 'mail_message';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ListListBase
     */
    protected appDeLogicName: string = '消息';

    /**
     * 界面UI服务对象
     *
     * @type {Mail_messageUIService}
     * @memberof ListBase
     */  
    public appUIService:Mail_messageUIService = new Mail_messageUIService(this.$store);


    /**
     * 分页条数
     *
     * @type {number}
     * @memberof ListListBase
     */
    public limit: number = 1000;

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof ListListBase
     */
    public minorSortDir: string = 'DESC';

    /**
     * 排序字段
     *
     * @type {string}
     * @memberof ListListBase
     */
    public minorSortPSDEF: string = 'id';


}