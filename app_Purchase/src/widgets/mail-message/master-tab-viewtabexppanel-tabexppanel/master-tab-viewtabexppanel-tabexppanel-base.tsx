import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, TabExpPanelControlBase } from '@/studio-core';
import Mail_messageService from '@/service/mail-message/mail-message-service';
import MasterTabViewtabexppanelService from './master-tab-viewtabexppanel-tabexppanel-service';
import Mail_messageUIService from '@/uiservice/mail-message/mail-message-ui-service';
import Mail_messageAuthService from '@/authservice/mail-message/mail-message-auth-service';
import { Environment } from '@/environments/environment';


/**
 * tabexppanel部件基类
 *
 * @export
 * @class TabExpPanelControlBase
 * @extends {MasterTabViewtabexppanelTabexppanelBase}
 */
export class MasterTabViewtabexppanelTabexppanelBase extends TabExpPanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MasterTabViewtabexppanelTabexppanelBase
     */
    protected controlType: string = 'TABEXPPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {MasterTabViewtabexppanelService}
     * @memberof MasterTabViewtabexppanelTabexppanelBase
     */
    public service: MasterTabViewtabexppanelService = new MasterTabViewtabexppanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Mail_messageService}
     * @memberof MasterTabViewtabexppanelTabexppanelBase
     */
    public appEntityService: Mail_messageService = new Mail_messageService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabViewtabexppanelTabexppanelBase
     */
    protected appDeName: string = 'mail_message';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabViewtabexppanelTabexppanelBase
     */
    protected appDeLogicName: string = '消息';

    /**
     * 界面UI服务对象
     *
     * @type {Mail_messageUIService}
     * @memberof MasterTabViewtabexppanelBase
     */  
    public appUIService:Mail_messageUIService = new Mail_messageUIService(this.$store);

    /**
     * 是否初始化
     *
     * @protected
     * @returns {any}
     * @memberof MasterTabViewtabexppanelBase
     */
    protected isInit: any = {
        tabviewpanel21:  true ,
        tabviewpanel22:  false ,
        tabviewpanel23:  false ,
        tabviewpanel24:  false ,
    }

    /**
     * 被激活的分页面板
     *
     * @protected
     * @type {string}
     * @memberof MasterTabViewtabexppanelBase
     */
    protected activatedTabViewPanel: string = 'tabviewpanel21';

    /**
     * 实体权限服务对象
     *
     * @protected
     * @type Mail_messageAuthServiceBase
     * @memberof TabExpViewtabexppanelBase
     */
    protected appAuthService: Mail_messageAuthService = new Mail_messageAuthService();

    /**
     * 分页面板权限标识存储对象
     *
     * @protected
     * @type {*}
     * @memberof MasterTabViewtabexppanelBase
     */
    protected authResourceObject:any = {'tabviewpanel21':{resourcetag:null,visabled: true,disabled: false},'tabviewpanel22':{resourcetag:null,visabled: true,disabled: false},'tabviewpanel23':{resourcetag:null,visabled: true,disabled: false},'tabviewpanel24':{resourcetag:null,visabled: true,disabled: false}};

    /**
     * 组件创建完毕
     *
     * @protected
     * @memberof MasterTabViewtabexppanelBase
     */
    protected ctrlCreated(): void {
        //设置分页导航srfparentdename和srfparentkey
        if (this.context.mail_message) {
            Object.assign(this.context, { srfparentdename: 'Mail_message', srfparentkey: this.context.mail_message });
        }
        super.ctrlCreated();
    }
}