/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'name',
      },
      {
        name: 'exclusive',
      },
      {
        name: 'purchase_requisition_type',
        prop: 'id',
      },
      {
        name: 'quantity_copy',
      },
      {
        name: 'write_date',
      },
      {
        name: 'create_date',
      },
      {
        name: '__last_update',
      },
      {
        name: 'line_copy',
      },
      {
        name: 'create_uname',
      },
      {
        name: 'write_uname',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'create_uid',
      },
    ]
  }


}