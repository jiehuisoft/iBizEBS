import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, ListControlBase } from '@/studio-core';
import Purchase_requisition_typeService from '@/service/purchase-requisition-type/purchase-requisition-type-service';
import BasicService from './basic-list-service';
import Purchase_requisition_typeUIService from '@/uiservice/purchase-requisition-type/purchase-requisition-type-ui-service';


/**
 * listexpbar_list部件基类
 *
 * @export
 * @class ListControlBase
 * @extends {BasicListBase}
 */
export class BasicListBase extends ListControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected controlType: string = 'LIST';

    /**
     * 建构部件服务对象
     *
     * @type {BasicService}
     * @memberof BasicListBase
     */
    public service: BasicService = new BasicService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Purchase_requisition_typeService}
     * @memberof BasicListBase
     */
    public appEntityService: Purchase_requisition_typeService = new Purchase_requisition_typeService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected appDeName: string = 'purchase_requisition_type';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected appDeLogicName: string = '采购申请类型';

    /**
     * 界面UI服务对象
     *
     * @type {Purchase_requisition_typeUIService}
     * @memberof BasicBase
     */  
    public appUIService:Purchase_requisition_typeUIService = new Purchase_requisition_typeUIService(this.$store);


    /**
     * 分页条数
     *
     * @type {number}
     * @memberof BasicListBase
     */
    public limit: number = 1000;

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof BasicListBase
     */
    public minorSortDir: string = '';


}