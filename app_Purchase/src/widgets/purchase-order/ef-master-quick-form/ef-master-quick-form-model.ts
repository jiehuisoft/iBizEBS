/**
 * EF_MasterQuick 部件模型
 *
 * @export
 * @class EF_MasterQuickModel
 */
export default class EF_MasterQuickModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_MasterQuickModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'requisition_id_text',
        prop: 'requisition_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'partner_id_text',
        prop: 'partner_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'currency_id_text',
        prop: 'currency_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'date_order',
        prop: 'date_order',
        dataType: 'DATETIME',
      },
      {
        name: 'company_id_text',
        prop: 'company_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'picking_type_id_text',
        prop: 'picking_type_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'company_id',
        prop: 'company_id',
        dataType: 'PICKUP',
      },
      {
        name: 'partner_id',
        prop: 'partner_id',
        dataType: 'PICKUP',
      },
      {
        name: 'currency_id',
        prop: 'currency_id',
        dataType: 'PICKUP',
      },
      {
        name: 'picking_type_id',
        prop: 'picking_type_id',
        dataType: 'PICKUP',
      },
      {
        name: 'requisition_id',
        prop: 'requisition_id',
        dataType: 'PICKUP',
      },
      {
        name: 'purchase_order',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}