import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Purchase_orderService from '@/service/purchase-order/purchase-order-service';
import EF_MasterService from './ef-master-form-service';
import Purchase_orderUIService from '@/uiservice/purchase-order/purchase-order-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {EF_MasterEditFormBase}
 */
export class EF_MasterEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {EF_MasterService}
     * @memberof EF_MasterEditFormBase
     */
    public service: EF_MasterService = new EF_MasterService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Purchase_orderService}
     * @memberof EF_MasterEditFormBase
     */
    public appEntityService: Purchase_orderService = new Purchase_orderService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterEditFormBase
     */
    protected appDeName: string = 'purchase_order';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterEditFormBase
     */
    protected appDeLogicName: string = '采购订单';

    /**
     * 界面UI服务对象
     *
     * @type {Purchase_orderUIService}
     * @memberof EF_MasterBase
     */  
    public appUIService:Purchase_orderUIService = new Purchase_orderUIService(this.$store);


    /**
     * 关系界面数量
     *
     * @protected
     * @type {number}
     * @memberof EF_MasterEditFormBase
     */
    protected drCount: number = 1;
    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        name: null,
        partner_id_text: null,
        partner_ref: null,
        requisition_id_text: null,
        currency_id_text: null,
        date_order: null,
        company_id_text: null,
        date_planned: null,
        picking_type_id_text: null,
        incoterm_id_text: null,
        user_id_text: null,
        payment_term_id_text: null,
        fiscal_position_id_text: null,
        id: null,
        company_id: null,
        partner_id: null,
        payment_term_id: null,
        currency_id: null,
        incoterm_id: null,
        fiscal_position_id: null,
        user_id: null,
        picking_type_id: null,
        requisition_id: null,
        purchase_order:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public majorMessageField: string = "name";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public rules():any{
        return {
        partner_id_text: [
            { required: this.detailsModel.partner_id_text.required, type: 'string', message: '供应商 值不能为空', trigger: 'change' },
            { required: this.detailsModel.partner_id_text.required, type: 'string', message: '供应商 值不能为空', trigger: 'blur' },
        ],
        currency_id_text: [
            { required: this.detailsModel.currency_id_text.required, type: 'string', message: '币种 值不能为空', trigger: 'change' },
            { required: this.detailsModel.currency_id_text.required, type: 'string', message: '币种 值不能为空', trigger: 'blur' },
        ],
        date_order: [
            { required: this.detailsModel.date_order.required, type: 'string', message: '单据日期 值不能为空', trigger: 'change' },
            { required: this.detailsModel.date_order.required, type: 'string', message: '单据日期 值不能为空', trigger: 'blur' },
        ],
        company_id_text: [
            { required: this.detailsModel.company_id_text.required, type: 'string', message: '公司 值不能为空', trigger: 'change' },
            { required: this.detailsModel.company_id_text.required, type: 'string', message: '公司 值不能为空', trigger: 'blur' },
        ],
        picking_type_id_text: [
            { required: this.detailsModel.picking_type_id_text.required, type: 'string', message: '交货到 值不能为空', trigger: 'change' },
            { required: this.detailsModel.picking_type_id_text.required, type: 'string', message: '交货到 值不能为空', trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.purchase_order.ef_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '其他信息', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.purchase_order.ef_master_form', extractMode: 'ITEM', details: [] } }),

        druipart1: new FormDRUIPartModel({ caption: '', detailType: 'DRUIPART', name: 'druipart1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel2: new FormGroupPanelModel({ caption: '产品', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.purchase_order.ef_master_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '最后更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: '订单关联', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '订单关联', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        partner_id_text: new FormItemModel({ caption: '供应商', detailType: 'FORMITEM', name: 'partner_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        partner_ref: new FormItemModel({ caption: '供应商参考', detailType: 'FORMITEM', name: 'partner_ref', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        requisition_id_text: new FormItemModel({ caption: '采购申请单', detailType: 'FORMITEM', name: 'requisition_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        currency_id_text: new FormItemModel({ caption: '币种', detailType: 'FORMITEM', name: 'currency_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        date_order: new FormItemModel({ caption: '单据日期', detailType: 'FORMITEM', name: 'date_order', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        company_id_text: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        date_planned: new FormItemModel({ caption: '计划日期', detailType: 'FORMITEM', name: 'date_planned', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        picking_type_id_text: new FormItemModel({ caption: '交货到', detailType: 'FORMITEM', name: 'picking_type_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        incoterm_id_text: new FormItemModel({ caption: '国际贸易术语', detailType: 'FORMITEM', name: 'incoterm_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        user_id_text: new FormItemModel({ caption: '采购员', detailType: 'FORMITEM', name: 'user_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        payment_term_id_text: new FormItemModel({ caption: '付款条款', detailType: 'FORMITEM', name: 'payment_term_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        fiscal_position_id_text: new FormItemModel({ caption: '税科目调整', detailType: 'FORMITEM', name: 'fiscal_position_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        company_id: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        partner_id: new FormItemModel({ caption: '供应商', detailType: 'FORMITEM', name: 'partner_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        payment_term_id: new FormItemModel({ caption: '付款条款', detailType: 'FORMITEM', name: 'payment_term_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        currency_id: new FormItemModel({ caption: '币种', detailType: 'FORMITEM', name: 'currency_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        incoterm_id: new FormItemModel({ caption: '国际贸易术语', detailType: 'FORMITEM', name: 'incoterm_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        fiscal_position_id: new FormItemModel({ caption: '税科目调整', detailType: 'FORMITEM', name: 'fiscal_position_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        user_id: new FormItemModel({ caption: '采购员', detailType: 'FORMITEM', name: 'user_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        picking_type_id: new FormItemModel({ caption: '交货到', detailType: 'FORMITEM', name: 'picking_type_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        requisition_id: new FormItemModel({ caption: '采购申请单', detailType: 'FORMITEM', name: 'requisition_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

    };
}