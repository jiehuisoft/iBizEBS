/**
 * Master_Enquiry 部件模型
 *
 * @export
 * @class Master_EnquiryModel
 */
export default class Master_EnquiryModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Master_EnquiryGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Master_EnquiryGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'name',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'date_order',
          prop: 'date_order',
          dataType: 'DATETIME',
        },
        {
          name: 'partner_id_text',
          prop: 'partner_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'company_id_text',
          prop: 'company_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'user_id_text',
          prop: 'user_id_text',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'origin',
          prop: 'origin',
          dataType: 'TEXT',
        },
        {
          name: 'amount_total',
          prop: 'amount_total',
          dataType: 'DECIMAL',
        },
        {
          name: 'state',
          prop: 'state',
          dataType: 'SSCODELIST',
        },
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'partner_id',
          prop: 'partner_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'currency_id',
          prop: 'currency_id',
          dataType: 'PICKUP',
        },
        {
          name: 'incoterm_id',
          prop: 'incoterm_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'fiscal_position_id',
          prop: 'fiscal_position_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'payment_term_id',
          prop: 'payment_term_id',
          dataType: 'PICKUP',
        },
        {
          name: 'dest_address_id',
          prop: 'dest_address_id',
          dataType: 'PICKUP',
        },
        {
          name: 'picking_type_id',
          prop: 'picking_type_id',
          dataType: 'PICKUP',
        },
        {
          name: 'user_id',
          prop: 'user_id',
          dataType: 'PICKUP',
        },
        {
          name: 'requisition_id',
          prop: 'requisition_id',
          dataType: 'PICKUP',
        },
        {
          name: 'purchase_order',
          prop: 'id',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}