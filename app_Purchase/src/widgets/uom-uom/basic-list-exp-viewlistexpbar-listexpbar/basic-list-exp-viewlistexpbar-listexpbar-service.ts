import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Uom_uomService from '@/service/uom-uom/uom-uom-service';
import BasicListExpViewlistexpbarModel from './basic-list-exp-viewlistexpbar-listexpbar-model';


/**
 * BasicListExpViewlistexpbar 部件服务对象
 *
 * @export
 * @class BasicListExpViewlistexpbarService
 */
export default class BasicListExpViewlistexpbarService extends ControlService {

    /**
     * 产品计量单位服务对象
     *
     * @type {Uom_uomService}
     * @memberof BasicListExpViewlistexpbarService
     */
    public appEntityService: Uom_uomService = new Uom_uomService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof BasicListExpViewlistexpbarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of BasicListExpViewlistexpbarService.
     * 
     * @param {*} [opts={}]
     * @memberof BasicListExpViewlistexpbarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new BasicListExpViewlistexpbarModel();
    }

}