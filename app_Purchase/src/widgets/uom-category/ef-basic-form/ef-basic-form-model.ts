/**
 * EF_Basic 部件模型
 *
 * @export
 * @class EF_BasicModel
 */
export default class EF_BasicModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_BasicModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'measure_type',
        prop: 'measure_type',
        dataType: 'SSCODELIST',
      },
      {
        name: 'is_pos_groupable',
        prop: 'is_pos_groupable',
        dataType: 'TRUEFALSE',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'uom_category',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}