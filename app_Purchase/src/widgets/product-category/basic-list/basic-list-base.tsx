import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, ListControlBase } from '@/studio-core';
import Product_categoryService from '@/service/product-category/product-category-service';
import BasicService from './basic-list-service';
import Product_categoryUIService from '@/uiservice/product-category/product-category-ui-service';


/**
 * listexpbar_list部件基类
 *
 * @export
 * @class ListControlBase
 * @extends {BasicListBase}
 */
export class BasicListBase extends ListControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected controlType: string = 'LIST';

    /**
     * 建构部件服务对象
     *
     * @type {BasicService}
     * @memberof BasicListBase
     */
    public service: BasicService = new BasicService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Product_categoryService}
     * @memberof BasicListBase
     */
    public appEntityService: Product_categoryService = new Product_categoryService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected appDeName: string = 'product_category';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected appDeLogicName: string = '产品种类';

    /**
     * 界面UI服务对象
     *
     * @type {Product_categoryUIService}
     * @memberof BasicBase
     */  
    public appUIService:Product_categoryUIService = new Product_categoryUIService(this.$store);


    /**
     * 分页条数
     *
     * @type {number}
     * @memberof BasicListBase
     */
    public limit: number = 1000;

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof BasicListBase
     */
    public minorSortDir: string = '';


}