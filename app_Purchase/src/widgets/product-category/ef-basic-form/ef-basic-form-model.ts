/**
 * EF_Basic 部件模型
 *
 * @export
 * @class EF_BasicModel
 */
export default class EF_BasicModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_BasicModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'parent_id_text',
        prop: 'parent_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'property_cost_method',
        prop: 'property_cost_method',
        dataType: 'SSCODELIST',
      },
      {
        name: 'route_ids',
        prop: 'route_ids',
        dataType: 'LONGTEXT',
      },
      {
        name: 'removal_strategy_id_text',
        prop: 'removal_strategy_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'removal_strategy_id',
        prop: 'removal_strategy_id',
        dataType: 'PICKUP',
      },
      {
        name: 'parent_id',
        prop: 'parent_id',
        dataType: 'PICKUP',
      },
      {
        name: 'product_category',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}