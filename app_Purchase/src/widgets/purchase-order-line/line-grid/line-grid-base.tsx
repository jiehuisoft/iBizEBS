import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, GridControlBase } from '@/studio-core';
import Purchase_order_lineService from '@/service/purchase-order-line/purchase-order-line-service';
import LineService from './line-grid-service';
import Purchase_order_lineUIService from '@/uiservice/purchase-order-line/purchase-order-line-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {LineGridBase}
 */
export class LineGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof LineGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {LineService}
     * @memberof LineGridBase
     */
    public service: LineService = new LineService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Purchase_order_lineService}
     * @memberof LineGridBase
     */
    public appEntityService: Purchase_order_lineService = new Purchase_order_lineService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LineGridBase
     */
    protected appDeName: string = 'purchase_order_line';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof LineGridBase
     */
    protected appDeLogicName: string = '采购订单行';

    /**
     * 界面UI服务对象
     *
     * @type {Purchase_order_lineUIService}
     * @memberof LineBase
     */  
    public appUIService:Purchase_order_lineUIService = new Purchase_order_lineUIService(this.$store);


    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof LineBase
     */  
    public ActionModel: any = {
    };

    /**
     * 主信息表格列
     *
     * @type {string}
     * @memberof LineBase
     */  
    public majorInfoColName:string = "name";


    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof LineBase
     */
    protected localStorageTag: string = 'purchase_order_line_line_grid';

    /**
     * 是否支持分页
     *
     * @type {boolean}
     * @memberof LineGridBase
     */
    public isEnablePagingBar: boolean = false;

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof LineGridBase
     */
    public allColumns: any[] = [
        {
            name: 'product_id_text',
            label: '产品',
            langtag: 'entities.purchase_order_line.line_grid.columns.product_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'name',
            label: '说明',
            langtag: 'entities.purchase_order_line.line_grid.columns.name',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'product_qty',
            label: '数量',
            langtag: 'entities.purchase_order_line.line_grid.columns.product_qty',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'product_uom_text',
            label: '计量单位',
            langtag: 'entities.purchase_order_line.line_grid.columns.product_uom_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'price_unit',
            label: '单价',
            langtag: 'entities.purchase_order_line.line_grid.columns.price_unit',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'taxes_id',
            label: '税率',
            langtag: 'entities.purchase_order_line.line_grid.columns.taxes_id',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'price_subtotal',
            label: '小计',
            langtag: 'entities.purchase_order_line.line_grid.columns.price_subtotal',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 0 ,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof LineGridBase
     */
    public getGridRowModel(){
        return {
          product_qty: new FormItemModel(),
          taxes_id: new FormItemModel(),
          name: new FormItemModel(),
          price_subtotal: new FormItemModel(),
          price_unit: new FormItemModel(),
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof LineGridBase
     */
    public rules: any = {
        product_qty: [
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '数量 值不能为空', trigger: 'change' },
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '数量 值不能为空', trigger: 'blur' },
        ],
        taxes_id: [
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '税率 值不能为空', trigger: 'change' },
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '税率 值不能为空', trigger: 'blur' },
        ],
        name: [
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '说明 值不能为空', trigger: 'change' },
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '说明 值不能为空', trigger: 'blur' },
        ],
        price_subtotal: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '小计 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '小计 值不能为空', trigger: 'blur' },
        ],
        price_unit: [
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '单价 值不能为空', trigger: 'change' },
            { required: true, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '单价 值不能为空', trigger: 'blur' },
        ],
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof LineBase
     */
    public deRules:any = {
    };

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof LineBase
     */
    public hasRowEdit: any = {
        'product_id_text':false,
        'name':false,
        'product_qty':false,
        'product_uom_text':false,
        'price_unit':false,
        'taxes_id':false,
        'price_subtotal':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof LineBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof LineGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
        ]);
    }


    /**
     * 更新默认值
     * @param {*}  row 行数据
     * @memberof LineBase
     */
    public updateDefault(row: any){                    
    }

    /**
     * 计算数据对象类型的默认值
     * @param {string}  action 行为
     * @param {string}  param 默认值参数
     * @param {*}  data 当前行数据
     * @memberof LineBase
     */
    public computeDefaultValueWithParam(action:string,param:string,data:any){
        if(Object.is(action,"UPDATE")){
            const nativeData:any = this.service.getCopynativeData();
            if(nativeData && (nativeData instanceof Array) && nativeData.length >0){
                let targetData:any = nativeData.find((item:any) =>{
                    return item.id === data.srfkey;
                })
                if(targetData){
                    return targetData[param]?targetData[param]:null;
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
           return this.service.getRemoteCopyData()[param]?this.service.getRemoteCopyData()[param]:null;
        }
    }


}