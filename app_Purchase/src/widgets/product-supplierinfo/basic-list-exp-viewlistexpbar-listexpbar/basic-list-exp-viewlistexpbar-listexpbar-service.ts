import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Product_supplierinfoService from '@/service/product-supplierinfo/product-supplierinfo-service';
import BasicListExpViewlistexpbarModel from './basic-list-exp-viewlistexpbar-listexpbar-model';


/**
 * BasicListExpViewlistexpbar 部件服务对象
 *
 * @export
 * @class BasicListExpViewlistexpbarService
 */
export default class BasicListExpViewlistexpbarService extends ControlService {

    /**
     * 供应商价格表服务对象
     *
     * @type {Product_supplierinfoService}
     * @memberof BasicListExpViewlistexpbarService
     */
    public appEntityService: Product_supplierinfoService = new Product_supplierinfoService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof BasicListExpViewlistexpbarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of BasicListExpViewlistexpbarService.
     * 
     * @param {*} [opts={}]
     * @memberof BasicListExpViewlistexpbarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new BasicListExpViewlistexpbarModel();
    }

}