import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Product_supplierinfoService from '@/service/product-supplierinfo/product-supplierinfo-service';
import EF_BasicQuickService from './ef-basic-quick-form-service';
import Product_supplierinfoUIService from '@/uiservice/product-supplierinfo/product-supplierinfo-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {EF_BasicQuickEditFormBase}
 */
export class EF_BasicQuickEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicQuickEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {EF_BasicQuickService}
     * @memberof EF_BasicQuickEditFormBase
     */
    public service: EF_BasicQuickService = new EF_BasicQuickService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Product_supplierinfoService}
     * @memberof EF_BasicQuickEditFormBase
     */
    public appEntityService: Product_supplierinfoService = new Product_supplierinfoService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicQuickEditFormBase
     */
    protected appDeName: string = 'product_supplierinfo';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicQuickEditFormBase
     */
    protected appDeLogicName: string = '供应商价格表';

    /**
     * 界面UI服务对象
     *
     * @type {Product_supplierinfoUIService}
     * @memberof EF_BasicQuickBase
     */  
    public appUIService:Product_supplierinfoUIService = new Product_supplierinfoUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof EF_BasicQuickEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        product_tmpl_id_text: null,
        name_text: null,
        price: null,
        min_qty: null,
        delay: null,
        id: null,
        product_tmpl_id: null,
        name: null,
        product_supplierinfo:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof EF_BasicQuickEditFormBase
     */
    public majorMessageField: string = "name_text";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_BasicQuickEditFormBase
     */
    public rules():any{
        return {
        product_tmpl_id_text: [
            { required: this.detailsModel.product_tmpl_id_text.required, type: 'string', message: '产品模板 值不能为空', trigger: 'change' },
            { required: this.detailsModel.product_tmpl_id_text.required, type: 'string', message: '产品模板 值不能为空', trigger: 'blur' },
        ],
        name_text: [
            { required: this.detailsModel.name_text.required, type: 'string', message: '供应商 值不能为空', trigger: 'change' },
            { required: this.detailsModel.name_text.required, type: 'string', message: '供应商 值不能为空', trigger: 'blur' },
        ],
        price: [
            { required: this.detailsModel.price.required, type: 'number', message: '价格 值不能为空', trigger: 'change' },
            { required: this.detailsModel.price.required, type: 'number', message: '价格 值不能为空', trigger: 'blur' },
        ],
        min_qty: [
            { required: this.detailsModel.min_qty.required, type: 'number', message: '最少数量 值不能为空', trigger: 'change' },
            { required: this.detailsModel.min_qty.required, type: 'number', message: '最少数量 值不能为空', trigger: 'blur' },
        ],
        delay: [
            { required: this.detailsModel.delay.required, type: 'number', message: '交货提前时间 值不能为空', trigger: 'change' },
            { required: this.detailsModel.delay.required, type: 'number', message: '交货提前时间 值不能为空', trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_BasicQuickBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof EF_BasicQuickEditFormBase
     */
    public detailsModel: any = {
        grouppanel1: new FormGroupPanelModel({ caption: '分组面板', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.product_supplierinfo.ef_basicquick_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '最后更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: '供应商', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        product_tmpl_id_text: new FormItemModel({ caption: '产品模板', detailType: 'FORMITEM', name: 'product_tmpl_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        name_text: new FormItemModel({ caption: '供应商', detailType: 'FORMITEM', name: 'name_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        price: new FormItemModel({ caption: '价格', detailType: 'FORMITEM', name: 'price', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        min_qty: new FormItemModel({ caption: '最少数量', detailType: 'FORMITEM', name: 'min_qty', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        delay: new FormItemModel({ caption: '交货提前时间', detailType: 'FORMITEM', name: 'delay', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        product_tmpl_id: new FormItemModel({ caption: '产品模板', detailType: 'FORMITEM', name: 'product_tmpl_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '供应商', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

    };
}