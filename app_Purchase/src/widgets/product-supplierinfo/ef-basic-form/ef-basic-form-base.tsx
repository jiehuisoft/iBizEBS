import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Product_supplierinfoService from '@/service/product-supplierinfo/product-supplierinfo-service';
import EF_BasicService from './ef-basic-form-service';
import Product_supplierinfoUIService from '@/uiservice/product-supplierinfo/product-supplierinfo-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {EF_BasicEditFormBase}
 */
export class EF_BasicEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {EF_BasicService}
     * @memberof EF_BasicEditFormBase
     */
    public service: EF_BasicService = new EF_BasicService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Product_supplierinfoService}
     * @memberof EF_BasicEditFormBase
     */
    public appEntityService: Product_supplierinfoService = new Product_supplierinfoService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicEditFormBase
     */
    protected appDeName: string = 'product_supplierinfo';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EF_BasicEditFormBase
     */
    protected appDeLogicName: string = '供应商价格表';

    /**
     * 界面UI服务对象
     *
     * @type {Product_supplierinfoUIService}
     * @memberof EF_BasicBase
     */  
    public appUIService:Product_supplierinfoUIService = new Product_supplierinfoUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof EF_BasicEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        name_text: null,
        product_code: null,
        product_name: null,
        product_id_text: null,
        delay: null,
        product_tmpl_id_text: null,
        min_qty: null,
        price: null,
        date_start: null,
        date_end: null,
        id: null,
        product_tmpl_id: null,
        product_id: null,
        name: null,
        product_supplierinfo:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof EF_BasicEditFormBase
     */
    public majorMessageField: string = "name_text";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_BasicEditFormBase
     */
    public rules():any{
        return {
        name_text: [
            { required: this.detailsModel.name_text.required, type: 'string', message: '供应商 值不能为空', trigger: 'change' },
            { required: this.detailsModel.name_text.required, type: 'string', message: '供应商 值不能为空', trigger: 'blur' },
        ],
        delay: [
            { required: this.detailsModel.delay.required, type: 'number', message: '交货提前时间 值不能为空', trigger: 'change' },
            { required: this.detailsModel.delay.required, type: 'number', message: '交货提前时间 值不能为空', trigger: 'blur' },
        ],
        product_tmpl_id_text: [
            { required: this.detailsModel.product_tmpl_id_text.required, type: 'string', message: '产品 值不能为空', trigger: 'change' },
            { required: this.detailsModel.product_tmpl_id_text.required, type: 'string', message: '产品 值不能为空', trigger: 'blur' },
        ],
        min_qty: [
            { required: this.detailsModel.min_qty.required, type: 'number', message: '最少数量 值不能为空', trigger: 'change' },
            { required: this.detailsModel.min_qty.required, type: 'number', message: '最少数量 值不能为空', trigger: 'blur' },
        ],
        price: [
            { required: this.detailsModel.price.required, type: 'number', message: '价格 值不能为空', trigger: 'change' },
            { required: this.detailsModel.price.required, type: 'number', message: '价格 值不能为空', trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_BasicBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof EF_BasicEditFormBase
     */
    public detailsModel: any = {
        grouppanel1: new FormGroupPanelModel({ caption: '基本信息', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.product_supplierinfo.ef_basic_form', extractMode: 'ITEM', details: [] } }),

        grouppanel2: new FormGroupPanelModel({ caption: '价格表', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.product_supplierinfo.ef_basic_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '最后更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: '供应商', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name_text: new FormItemModel({ caption: '供应商', detailType: 'FORMITEM', name: 'name_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        product_code: new FormItemModel({ caption: '供应商产品代码', detailType: 'FORMITEM', name: 'product_code', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        product_name: new FormItemModel({ caption: '供应商产品名称', detailType: 'FORMITEM', name: 'product_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        product_id_text: new FormItemModel({ caption: '产品变体', detailType: 'FORMITEM', name: 'product_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        delay: new FormItemModel({ caption: '交货提前时间', detailType: 'FORMITEM', name: 'delay', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        product_tmpl_id_text: new FormItemModel({ caption: '产品', detailType: 'FORMITEM', name: 'product_tmpl_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        min_qty: new FormItemModel({ caption: '最少数量', detailType: 'FORMITEM', name: 'min_qty', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        price: new FormItemModel({ caption: '价格', detailType: 'FORMITEM', name: 'price', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        date_start: new FormItemModel({ caption: '开始日期', detailType: 'FORMITEM', name: 'date_start', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        date_end: new FormItemModel({ caption: '结束日期', detailType: 'FORMITEM', name: 'date_end', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        product_tmpl_id: new FormItemModel({ caption: '产品模板', detailType: 'FORMITEM', name: 'product_tmpl_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        product_id: new FormItemModel({ caption: '产品变体', detailType: 'FORMITEM', name: 'product_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '供应商', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

    };
}