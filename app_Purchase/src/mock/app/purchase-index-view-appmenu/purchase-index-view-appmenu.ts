import { MockAdapter } from '@/mock/mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'
const Random = Mock.Random;

// 获取应用数据
mock.onGet('v7/purchase-index-viewappmenu').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status, {
        name: 'appmenu',
        items:  [
            {
	id: 'f4544f3d016e5fb604fd0456f1a32454',
	name: 'user_menus',
	text: '用户菜单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '用户菜单',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
},
            {
	id: 'd8feda1391fcacd886e4465ee402a647',
	name: 'top_menus',
	text: '顶部菜单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '顶部菜单',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
},
            {
	id: 'f73e95565861fc7af722df56da462602',
	name: 'left_exp',
	text: '左侧菜单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '左侧菜单',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: '1220a09ac290ff7dd0a1c8fe8ddda4dc',
	name: 'menuitem1',
	text: '订单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '订单',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-file-text-o',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: '7bce46ca071d99d3795dac44260d3926',
	name: 'menuitem2',
	text: '采购申请',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '采购申请',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-pencil-square-o',
	icon: '',
	textcls: '',
	appfunctag: 'AppFunc',
	resourcetag: '',
},
		{
	id: '99d4d1ad756a66ba71ba50e9dd06d8b3',
	name: 'menuitem3',
	text: '询价单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '询价单',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-phone',
	icon: '',
	textcls: '',
	appfunctag: 'AppFunc2',
	resourcetag: '',
},
		{
	id: '4610919ce0cd1f5c75b4e9cdd31708a1',
	name: 'menuitem4',
	text: '采购订单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '采购订单',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-file-text',
	icon: '',
	textcls: '',
	appfunctag: 'AppFunc3',
	resourcetag: '',
},
		{
	id: 'd37fda11325d9248b91da6777f39b71f',
	name: 'menuitem5',
	text: '供应商',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '供应商',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-users',
	icon: '',
	textcls: '',
	appfunctag: 'AppFunc4',
	resourcetag: '',
},
	],
},
		{
	id: 'a8981990310dec97e014330bd8f3ba0e',
	name: 'menuitem7',
	text: '产品',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '产品',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-cube',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: 'c74ecc448396092d0ddcfa47bfc07a20',
	name: 'menuitem6',
	text: '产品',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '产品',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-cube',
	icon: '',
	textcls: '',
	appfunctag: 'AppFunc5',
	resourcetag: '',
},
		{
	id: '88efdc699f8066fec7d7a18d7c1eb1a1',
	name: 'menuitem8',
	text: '产品变种',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '产品变种',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-cubes',
	icon: '',
	textcls: '',
	appfunctag: 'AppFunc6',
	resourcetag: '',
},
	],
},
		{
	id: '1f7476c2799e1a61c0823ba4007ef7b8',
	name: 'menuitem9',
	text: '配置',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '配置',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-cogs',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: '2d6976744e750888734f825b9956bc75',
	name: 'menuitem15',
	text: '采购设置',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '采购设置',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-cog',
	icon: '',
	textcls: '',
	appfunctag: 'AppFunc12',
	resourcetag: '',
},
		{
	id: 'd99503d5d6e13217f7c129b27f02cbb0',
	name: 'menuitem10',
	text: '供应商价格表',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '供应商价格表',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-list-ol',
	icon: '',
	textcls: '',
	appfunctag: 'AppFunc7',
	resourcetag: '',
},
		{
	id: '504ed105c34439c612e7e457441f0d0f',
	name: 'menuitem11',
	text: '采购申请类型',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '采购申请类型',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-cog',
	icon: '',
	textcls: '',
	appfunctag: 'AppFunc8',
	resourcetag: '',
},
		{
	id: 'a1d39bf75aa7a925bd3d1db1b89e41a5',
	name: 'menuitem12',
	text: '产品类别',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '产品类别',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-dashcube',
	icon: '',
	textcls: '',
	appfunctag: 'AppFunc9',
	resourcetag: '',
},
		{
	id: '9b0837782821d05dcaa86f74d2be2deb',
	name: 'menuitem13',
	text: '计量单位',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '计量单位',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-cog',
	icon: '',
	textcls: '',
	appfunctag: 'AppFunc10',
	resourcetag: '',
},
		{
	id: '02e12fb0b17a0f9dae3f2eef6ebce827',
	name: 'menuitem14',
	text: '计量单位类别',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '计量单位类别',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-cog',
	icon: '',
	textcls: '',
	appfunctag: 'AppFunc11',
	resourcetag: '',
},
	],
},
	],
},
            {
	id: '7f0092f838d0eadf499172aa0ea465ae',
	name: 'bottom_exp',
	text: '底部内容',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '底部内容',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
},
            {
	id: '9ca6e13581f37bc069572c03208f5d3c',
	name: 'footer_left',
	text: '底部左侧',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '底部左侧',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
},
            {
	id: '1e24737e8eb269ca9d2183c3a3b096e5',
	name: 'footer_center',
	text: '底部中间',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '底部中间',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
},
            {
	id: '21253fa5a62ed6fc1a15b7a78cf47827',
	name: 'footer_right',
	text: '底部右侧',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '底部右侧',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
},
        ],
    }];
});

