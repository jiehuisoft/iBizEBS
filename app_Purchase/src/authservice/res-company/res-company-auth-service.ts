import Res_companyAuthServiceBase from './res-company-auth-service-base';


/**
 * 公司权限服务对象
 *
 * @export
 * @class Res_companyAuthService
 * @extends {Res_companyAuthServiceBase}
 */
export default class Res_companyAuthService extends Res_companyAuthServiceBase {

    /**
     * Creates an instance of  Res_companyAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_companyAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}