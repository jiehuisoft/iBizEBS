import Product_productAuthServiceBase from './product-product-auth-service-base';


/**
 * 产品权限服务对象
 *
 * @export
 * @class Product_productAuthService
 * @extends {Product_productAuthServiceBase}
 */
export default class Product_productAuthService extends Product_productAuthServiceBase {

    /**
     * Creates an instance of  Product_productAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_productAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}