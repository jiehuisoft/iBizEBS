import Res_country_stateAuthServiceBase from './res-country-state-auth-service-base';


/**
 * 国家/地区州/省权限服务对象
 *
 * @export
 * @class Res_country_stateAuthService
 * @extends {Res_country_stateAuthServiceBase}
 */
export default class Res_country_stateAuthService extends Res_country_stateAuthServiceBase {

    /**
     * Creates an instance of  Res_country_stateAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_country_stateAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}