import Res_partner_categoryAuthServiceBase from './res-partner-category-auth-service-base';


/**
 * 业务伙伴标签权限服务对象
 *
 * @export
 * @class Res_partner_categoryAuthService
 * @extends {Res_partner_categoryAuthServiceBase}
 */
export default class Res_partner_categoryAuthService extends Res_partner_categoryAuthServiceBase {

    /**
     * Creates an instance of  Res_partner_categoryAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partner_categoryAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}