import Res_partner_bankAuthServiceBase from './res-partner-bank-auth-service-base';


/**
 * 银行账户权限服务对象
 *
 * @export
 * @class Res_partner_bankAuthService
 * @extends {Res_partner_bankAuthServiceBase}
 */
export default class Res_partner_bankAuthService extends Res_partner_bankAuthServiceBase {

    /**
     * Creates an instance of  Res_partner_bankAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partner_bankAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}