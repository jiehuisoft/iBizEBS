import Purchase_requisition_typeAuthServiceBase from './purchase-requisition-type-auth-service-base';


/**
 * 采购申请类型权限服务对象
 *
 * @export
 * @class Purchase_requisition_typeAuthService
 * @extends {Purchase_requisition_typeAuthServiceBase}
 */
export default class Purchase_requisition_typeAuthService extends Purchase_requisition_typeAuthServiceBase {

    /**
     * Creates an instance of  Purchase_requisition_typeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_requisition_typeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}