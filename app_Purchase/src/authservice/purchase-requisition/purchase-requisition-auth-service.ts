import Purchase_requisitionAuthServiceBase from './purchase-requisition-auth-service-base';


/**
 * 采购申请权限服务对象
 *
 * @export
 * @class Purchase_requisitionAuthService
 * @extends {Purchase_requisitionAuthServiceBase}
 */
export default class Purchase_requisitionAuthService extends Purchase_requisitionAuthServiceBase {

    /**
     * Creates an instance of  Purchase_requisitionAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_requisitionAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}