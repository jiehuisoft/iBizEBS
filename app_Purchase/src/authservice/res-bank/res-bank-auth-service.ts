import Res_bankAuthServiceBase from './res-bank-auth-service-base';


/**
 * 银行权限服务对象
 *
 * @export
 * @class Res_bankAuthService
 * @extends {Res_bankAuthServiceBase}
 */
export default class Res_bankAuthService extends Res_bankAuthServiceBase {

    /**
     * Creates an instance of  Res_bankAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_bankAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}