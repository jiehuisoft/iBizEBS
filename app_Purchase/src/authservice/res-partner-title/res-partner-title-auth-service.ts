import Res_partner_titleAuthServiceBase from './res-partner-title-auth-service-base';


/**
 * 业务伙伴称谓权限服务对象
 *
 * @export
 * @class Res_partner_titleAuthService
 * @extends {Res_partner_titleAuthServiceBase}
 */
export default class Res_partner_titleAuthService extends Res_partner_titleAuthServiceBase {

    /**
     * Creates an instance of  Res_partner_titleAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partner_titleAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}