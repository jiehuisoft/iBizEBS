import Res_config_settingsAuthServiceBase from './res-config-settings-auth-service-base';


/**
 * 配置设定权限服务对象
 *
 * @export
 * @class Res_config_settingsAuthService
 * @extends {Res_config_settingsAuthServiceBase}
 */
export default class Res_config_settingsAuthService extends Res_config_settingsAuthServiceBase {

    /**
     * Creates an instance of  Res_config_settingsAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_config_settingsAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}