import Purchase_orderAuthServiceBase from './purchase-order-auth-service-base';


/**
 * 采购订单权限服务对象
 *
 * @export
 * @class Purchase_orderAuthService
 * @extends {Purchase_orderAuthServiceBase}
 */
export default class Purchase_orderAuthService extends Purchase_orderAuthServiceBase {

    /**
     * Creates an instance of  Purchase_orderAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_orderAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}