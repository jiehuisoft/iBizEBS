import Mail_compose_messageUIServiceBase from './mail-compose-message-ui-service-base';

/**
 * 邮件撰写向导UI服务对象
 *
 * @export
 * @class Mail_compose_messageUIService
 */
export default class Mail_compose_messageUIService extends Mail_compose_messageUIServiceBase {

    /**
     * Creates an instance of  Mail_compose_messageUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_compose_messageUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}