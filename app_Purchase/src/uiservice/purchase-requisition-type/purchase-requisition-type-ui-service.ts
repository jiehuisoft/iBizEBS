import Purchase_requisition_typeUIServiceBase from './purchase-requisition-type-ui-service-base';

/**
 * 采购申请类型UI服务对象
 *
 * @export
 * @class Purchase_requisition_typeUIService
 */
export default class Purchase_requisition_typeUIService extends Purchase_requisition_typeUIServiceBase {

    /**
     * Creates an instance of  Purchase_requisition_typeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_requisition_typeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}