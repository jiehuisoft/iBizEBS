import Uom_uomUIServiceBase from './uom-uom-ui-service-base';

/**
 * 产品计量单位UI服务对象
 *
 * @export
 * @class Uom_uomUIService
 */
export default class Uom_uomUIService extends Uom_uomUIServiceBase {

    /**
     * Creates an instance of  Uom_uomUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Uom_uomUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}