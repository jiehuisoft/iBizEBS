import Account_incotermsUIServiceBase from './account-incoterms-ui-service-base';

/**
 * 贸易条款UI服务对象
 *
 * @export
 * @class Account_incotermsUIService
 */
export default class Account_incotermsUIService extends Account_incotermsUIServiceBase {

    /**
     * Creates an instance of  Account_incotermsUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_incotermsUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}