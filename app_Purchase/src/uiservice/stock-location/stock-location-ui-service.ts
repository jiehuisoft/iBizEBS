import Stock_locationUIServiceBase from './stock-location-ui-service-base';

/**
 * 库存位置UI服务对象
 *
 * @export
 * @class Stock_locationUIService
 */
export default class Stock_locationUIService extends Stock_locationUIServiceBase {

    /**
     * Creates an instance of  Stock_locationUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Stock_locationUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}