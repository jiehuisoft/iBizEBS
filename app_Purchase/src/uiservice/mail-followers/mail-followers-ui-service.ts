import Mail_followersUIServiceBase from './mail-followers-ui-service-base';

/**
 * 文档关注者UI服务对象
 *
 * @export
 * @class Mail_followersUIService
 */
export default class Mail_followersUIService extends Mail_followersUIServiceBase {

    /**
     * Creates an instance of  Mail_followersUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_followersUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}