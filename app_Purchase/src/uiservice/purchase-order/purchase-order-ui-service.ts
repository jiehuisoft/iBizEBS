import Purchase_orderUIServiceBase from './purchase-order-ui-service-base';

/**
 * 采购订单UI服务对象
 *
 * @export
 * @class Purchase_orderUIService
 */
export default class Purchase_orderUIService extends Purchase_orderUIServiceBase {

    /**
     * Creates an instance of  Purchase_orderUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_orderUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}