import Product_productUIServiceBase from './product-product-ui-service-base';

/**
 * 产品UI服务对象
 *
 * @export
 * @class Product_productUIService
 */
export default class Product_productUIService extends Product_productUIServiceBase {

    /**
     * Creates an instance of  Product_productUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_productUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}