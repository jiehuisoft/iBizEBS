import Mail_activity_typeUIServiceBase from './mail-activity-type-ui-service-base';

/**
 * 活动类型UI服务对象
 *
 * @export
 * @class Mail_activity_typeUIService
 */
export default class Mail_activity_typeUIService extends Mail_activity_typeUIServiceBase {

    /**
     * Creates an instance of  Mail_activity_typeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_activity_typeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}