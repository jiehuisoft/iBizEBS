
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { GridViewBase } from '@/studio-core';
import Res_partner_bankService from '@/service/res-partner-bank/res-partner-bank-service';
import Res_partner_bankAuthService from '@/authservice/res-partner-bank/res-partner-bank-auth-service';
import GridViewEngine from '@engine/view/grid-view-engine';
import Res_partner_bankUIService from '@/uiservice/res-partner-bank/res-partner-bank-ui-service';
import CodeListService from "@service/app/codelist-service";


/**
 * 行编辑表格视图视图基类
 *
 * @export
 * @class Res_partner_bankLineBase
 * @extends {GridViewBase}
 */
export class Res_partner_bankLineBase extends GridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_bankLineBase
     */
    protected appDeName: string = 'res_partner_bank';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_bankLineBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_bankLineBase
     */
    protected appDeMajor: string = 'id';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_bankLineBase
     */ 
    protected dataControl:string = "grid";

    /**
     * 实体服务对象
     *
     * @type {Res_partner_bankService}
     * @memberof Res_partner_bankLineBase
     */
    protected appEntityService: Res_partner_bankService = new Res_partner_bankService;

    /**
     * 实体权限服务对象
     *
     * @type Res_partner_bankUIService
     * @memberof Res_partner_bankLineBase
     */
    public appUIService: Res_partner_bankUIService = new Res_partner_bankUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Res_partner_bankLineBase
     */
    protected model: any = {
        srfCaption: 'entities.res_partner_bank.views.line.caption',
        srfTitle: 'entities.res_partner_bank.views.line.title',
        srfSubTitle: 'entities.res_partner_bank.views.line.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Res_partner_bankLineBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_bankLineBase
     */
	protected viewtag: string = '73a5fcd7f7450ec1c38adecb5fd8fc38';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_bankLineBase
     */ 
    protected viewName:string = "res_partner_bankLine";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Res_partner_bankLineBase
     */
    public engine: GridViewEngine = new GridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Res_partner_bankLineBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Res_partner_bankLineBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            opendata: (args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) => {
                this.opendata(args,fullargs, params, $event, xData);
            },
            newdata: (args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) => {
                this.newdata(args,fullargs, params, $event, xData);
            },
            grid: this.$refs.grid,
            keyPSDEField: 'res_partner_bank',
            majorPSDEField: 'id',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_bankLineBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_bankLineBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_bankLineBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_bankLineBase
     */
    public grid_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'remove', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_bankLineBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Res_partner_bankLine
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.res_partner_bank;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.res_supplier && true){
            deResParameters = [
            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'res_partner_banks', parameterName: 'res_partner_bank' },
            { pathName: 'editview', parameterName: 'editview' },
        ];
        const _this: any = this;
        const openIndexViewTab = (data: any) => {
            const _data: any = { w: (new Date().getTime()) };
            Object.assign(_data, data);
            const routePath = this.$viewTool.buildUpRoutePath(this.$route, tempContext, deResParameters, parameters, args, _data);
            this.$router.push(routePath);
        }
        openIndexViewTab(data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Res_partner_bankLine
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
        const localContext: any = null;
        const localViewParam: any =null;
        const data: any = {};
        let tempContext = JSON.parse(JSON.stringify(this.context));
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.res_supplier && true){
            deResParameters = [
            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'res_partner_banks', parameterName: 'res_partner_bank' },
            { pathName: 'editview', parameterName: 'editview' },
        ];
        const _this: any = this;
        const openIndexViewTab = (data: any) => {
            const routePath = this.$viewTool.buildUpRoutePath(this.$route, tempContext, deResParameters, parameters, args, data);
            this.$router.push(routePath);
        }
        openIndexViewTab(data);
    }



    /**
     * 表格行数据默认激活模式
     * 0 不激活
     * 1 单击激活
     * 2 双击激活
     *
     * @protected
     * @type {(0 | 1 | 2)}
     * @memberof Res_partner_bankLineBase
     */
    protected gridRowActiveMode: 0 | 1 | 2 = 0;
}