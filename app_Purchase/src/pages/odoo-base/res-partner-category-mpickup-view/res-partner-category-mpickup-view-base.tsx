import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { MPickupViewBase } from '@/studio-core';
import Res_partner_categoryService from '@/service/res-partner-category/res-partner-category-service';
import Res_partner_categoryAuthService from '@/authservice/res-partner-category/res-partner-category-auth-service';
import MPickupViewEngine from '@engine/view/mpickup-view-engine';
import Res_partner_categoryUIService from '@/uiservice/res-partner-category/res-partner-category-ui-service';

/**
 * 业务伙伴标签数据多项选择视图视图基类
 *
 * @export
 * @class Res_partner_categoryMPickupViewBase
 * @extends {MPickupViewBase}
 */
export class Res_partner_categoryMPickupViewBase extends MPickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_categoryMPickupViewBase
     */
    protected appDeName: string = 'res_partner_category';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_categoryMPickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_categoryMPickupViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Res_partner_categoryService}
     * @memberof Res_partner_categoryMPickupViewBase
     */
    protected appEntityService: Res_partner_categoryService = new Res_partner_categoryService;

    /**
     * 实体权限服务对象
     *
     * @type Res_partner_categoryUIService
     * @memberof Res_partner_categoryMPickupViewBase
     */
    public appUIService: Res_partner_categoryUIService = new Res_partner_categoryUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Res_partner_categoryMPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.res_partner_category.views.mpickupview.caption',
        srfTitle: 'entities.res_partner_category.views.mpickupview.title',
        srfSubTitle: 'entities.res_partner_category.views.mpickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Res_partner_categoryMPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_categoryMPickupViewBase
     */
	protected viewtag: string = '0df51febb18b55dd835055f5fcb60e46';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_categoryMPickupViewBase
     */ 
    protected viewName:string = "res_partner_categoryMPickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Res_partner_categoryMPickupViewBase
     */
    public engine: MPickupViewEngine = new MPickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Res_partner_categoryMPickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Res_partner_categoryMPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'res_partner_category',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_categoryMPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_categoryMPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_categoryMPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }



    /**
     * 添加左侧面板所有数据到右侧
     *
     * @memberof Res_partner_categoryMPickupView
     */
    public onCLickAllRight(): void {
        Object.values(this.containerModel).forEach((model: any) => {
            if (!Object.is(model.type, 'PICKUPVIEWPANEL')) {
                return;
            }
            if (model.datas.length > 0) {
                model.datas.forEach((data: any, index: any) => {
                    Object.assign(data, { srfmajortext: data['name'] });
                })
            }
            model.datas.forEach((item: any) => {
                const index: number = this.viewSelections.findIndex((selection: any) => Object.is(item.srfkey, selection.srfkey));
                if (index === -1) {
                    item._select = false;
                    this.viewSelections.push(item);
                }
            });
        });
        this.selectedData = JSON.stringify(this.viewSelections);
    }


}