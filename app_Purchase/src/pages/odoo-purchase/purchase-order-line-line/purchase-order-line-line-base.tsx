
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { GridViewBase } from '@/studio-core';
import Purchase_order_lineService from '@/service/purchase-order-line/purchase-order-line-service';
import Purchase_order_lineAuthService from '@/authservice/purchase-order-line/purchase-order-line-auth-service';
import GridViewEngine from '@engine/view/grid-view-engine';
import Purchase_order_lineUIService from '@/uiservice/purchase-order-line/purchase-order-line-ui-service';
import CodeListService from "@service/app/codelist-service";


/**
 * 行编辑表格视图视图基类
 *
 * @export
 * @class Purchase_order_lineLineBase
 * @extends {GridViewBase}
 */
export class Purchase_order_lineLineBase extends GridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_order_lineLineBase
     */
    protected appDeName: string = 'purchase_order_line';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Purchase_order_lineLineBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Purchase_order_lineLineBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_order_lineLineBase
     */ 
    protected dataControl:string = "grid";

    /**
     * 实体服务对象
     *
     * @type {Purchase_order_lineService}
     * @memberof Purchase_order_lineLineBase
     */
    protected appEntityService: Purchase_order_lineService = new Purchase_order_lineService;

    /**
     * 实体权限服务对象
     *
     * @type Purchase_order_lineUIService
     * @memberof Purchase_order_lineLineBase
     */
    public appUIService: Purchase_order_lineUIService = new Purchase_order_lineUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Purchase_order_lineLineBase
     */
    protected model: any = {
        srfCaption: 'entities.purchase_order_line.views.line.caption',
        srfTitle: 'entities.purchase_order_line.views.line.title',
        srfSubTitle: 'entities.purchase_order_line.views.line.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Purchase_order_lineLineBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Purchase_order_lineLineBase
     */
	protected viewtag: string = 'f3659046722b66fc4a405e6967b1cc86';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_order_lineLineBase
     */ 
    protected viewName:string = "purchase_order_lineLine";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Purchase_order_lineLineBase
     */
    public engine: GridViewEngine = new GridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Purchase_order_lineLineBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Purchase_order_lineLineBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            opendata: (args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) => {
                this.opendata(args,fullargs, params, $event, xData);
            },
            newdata: (args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) => {
                this.newdata(args,fullargs, params, $event, xData);
            },
            grid: this.$refs.grid,
            keyPSDEField: 'purchase_order_line',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_order_lineLineBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_order_lineLineBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_order_lineLineBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_order_lineLineBase
     */
    public grid_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'remove', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_order_lineLineBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Purchase_order_lineLine
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.purchase_order_line;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.product_product && true){
            deResParameters = [
            { pathName: 'product_products', parameterName: 'product_product' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
            { pathName: 'editview', parameterName: 'editview' },
        ];
        const _this: any = this;
        const openIndexViewTab = (data: any) => {
            const _data: any = { w: (new Date().getTime()) };
            Object.assign(_data, data);
            const routePath = this.$viewTool.buildUpRoutePath(this.$route, tempContext, deResParameters, parameters, args, _data);
            this.$router.push(routePath);
        }
        openIndexViewTab(data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Purchase_order_lineLine
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
        const localContext: any = null;
        const localViewParam: any =null;
        const data: any = {};
        let tempContext = JSON.parse(JSON.stringify(this.context));
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.product_product && true){
            deResParameters = [
            { pathName: 'product_products', parameterName: 'product_product' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'purchase_order_lines', parameterName: 'purchase_order_line' },
            { pathName: 'editview', parameterName: 'editview' },
        ];
        const _this: any = this;
        const openIndexViewTab = (data: any) => {
            const routePath = this.$viewTool.buildUpRoutePath(this.$route, tempContext, deResParameters, parameters, args, data);
            this.$router.push(routePath);
        }
        openIndexViewTab(data);
    }



    /**
     * 表格行数据默认激活模式
     * 0 不激活
     * 1 单击激活
     * 2 双击激活
     *
     * @protected
     * @type {(0 | 1 | 2)}
     * @memberof Purchase_order_lineLineBase
     */
    protected gridRowActiveMode: 0 | 1 | 2 = 0;
}