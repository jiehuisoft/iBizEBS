import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Purchase_orderService from '@/service/purchase-order/purchase-order-service';
import Purchase_orderAuthService from '@/authservice/purchase-order/purchase-order-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Purchase_orderUIService from '@/uiservice/purchase-order/purchase-order-ui-service';

/**
 * 主信息概览视图基类
 *
 * @export
 * @class Purchase_orderMasterInfoViewBase
 * @extends {EditViewBase}
 */
export class Purchase_orderMasterInfoViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterInfoViewBase
     */
    protected appDeName: string = 'purchase_order';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterInfoViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterInfoViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterInfoViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Purchase_orderService}
     * @memberof Purchase_orderMasterInfoViewBase
     */
    protected appEntityService: Purchase_orderService = new Purchase_orderService;

    /**
     * 实体权限服务对象
     *
     * @type Purchase_orderUIService
     * @memberof Purchase_orderMasterInfoViewBase
     */
    public appUIService: Purchase_orderUIService = new Purchase_orderUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Purchase_orderMasterInfoViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Purchase_orderMasterInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.purchase_order.views.masterinfoview.caption',
        srfTitle: 'entities.purchase_order.views.masterinfoview.title',
        srfSubTitle: 'entities.purchase_order.views.masterinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Purchase_orderMasterInfoViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterInfoViewBase
     */
	protected viewtag: string = '2f2403c07f98ac47a93c2ccbf1485a9c';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterInfoViewBase
     */ 
    protected viewName:string = "purchase_orderMasterInfoView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Purchase_orderMasterInfoViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Purchase_orderMasterInfoViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Purchase_orderMasterInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'purchase_order',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_orderMasterInfoViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_orderMasterInfoViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_orderMasterInfoViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}