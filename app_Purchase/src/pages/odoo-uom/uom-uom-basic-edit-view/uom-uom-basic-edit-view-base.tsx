import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Uom_uomService from '@/service/uom-uom/uom-uom-service';
import Uom_uomAuthService from '@/authservice/uom-uom/uom-uom-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Uom_uomUIService from '@/uiservice/uom-uom/uom-uom-ui-service';

/**
 * 配置信息编辑视图视图基类
 *
 * @export
 * @class Uom_uomBasicEditViewBase
 * @extends {EditViewBase}
 */
export class Uom_uomBasicEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_uomBasicEditViewBase
     */
    protected appDeName: string = 'uom_uom';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Uom_uomBasicEditViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Uom_uomBasicEditViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_uomBasicEditViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Uom_uomService}
     * @memberof Uom_uomBasicEditViewBase
     */
    protected appEntityService: Uom_uomService = new Uom_uomService;

    /**
     * 实体权限服务对象
     *
     * @type Uom_uomUIService
     * @memberof Uom_uomBasicEditViewBase
     */
    public appUIService: Uom_uomUIService = new Uom_uomUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Uom_uomBasicEditViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Uom_uomBasicEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.uom_uom.views.basiceditview.caption',
        srfTitle: 'entities.uom_uom.views.basiceditview.title',
        srfSubTitle: 'entities.uom_uom.views.basiceditview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Uom_uomBasicEditViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Uom_uomBasicEditViewBase
     */
	protected viewtag: string = 'e3a30586132f7bb68b424b8057900c39';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_uomBasicEditViewBase
     */ 
    protected viewName:string = "uom_uomBasicEditView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Uom_uomBasicEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Uom_uomBasicEditViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Uom_uomBasicEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'uom_uom',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_uomBasicEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_uomBasicEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_uomBasicEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}