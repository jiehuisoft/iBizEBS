
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { ListViewBase } from '@/studio-core';
import Mail_activityService from '@/service/mail-activity/mail-activity-service';
import Mail_activityAuthService from '@/authservice/mail-activity/mail-activity-auth-service';
import ListViewEngine from '@engine/view/list-view-engine';
import Mail_activityUIService from '@/uiservice/mail-activity/mail-activity-ui-service';
import CodeListService from "@service/app/codelist-service";


/**
 * 活动列表视图视图基类
 *
 * @export
 * @class Mail_activityByResListViewBase
 * @extends {ListViewBase}
 */
export class Mail_activityByResListViewBase extends ListViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_activityByResListViewBase
     */
    protected appDeName: string = 'mail_activity';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Mail_activityByResListViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Mail_activityByResListViewBase
     */
    protected appDeMajor: string = 'id';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_activityByResListViewBase
     */ 
    protected dataControl:string = "list";

    /**
     * 实体服务对象
     *
     * @type {Mail_activityService}
     * @memberof Mail_activityByResListViewBase
     */
    protected appEntityService: Mail_activityService = new Mail_activityService;

    /**
     * 实体权限服务对象
     *
     * @type Mail_activityUIService
     * @memberof Mail_activityByResListViewBase
     */
    public appUIService: Mail_activityUIService = new Mail_activityUIService(this.$store);

	/**
	 * 自定义视图导航参数集合
	 *
     * @protected
	 * @type {*}
	 * @memberof Mail_activityByResListViewBase
	 */
    protected customViewParams: any = {
        'n_res_model_eq': { isRawValue: false, value: 'n_res_model_eq' },
        'n_res_id_eq': { isRawValue: false, value: 'n_res_id_eq' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Mail_activityByResListViewBase
     */
    protected model: any = {
        srfCaption: 'entities.mail_activity.views.byreslistview.caption',
        srfTitle: 'entities.mail_activity.views.byreslistview.title',
        srfSubTitle: 'entities.mail_activity.views.byreslistview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Mail_activityByResListViewBase
     */
    protected containerModel: any = {
        view_list: { name: 'list', type: 'LIST' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Mail_activityByResListViewBase
     */
	protected viewtag: string = 'e79e0aa4e64b7c70a08332f6cc3aaa93';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_activityByResListViewBase
     */ 
    protected viewName:string = "mail_activityByResListView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Mail_activityByResListViewBase
     */
    public engine: ListViewEngine = new ListViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Mail_activityByResListViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Mail_activityByResListViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            list: this.$refs.list,
            opendata: (args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) => {
                this.opendata(args,fullargs, params, $event, xData);
            },
            newdata: (args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) => {
                this.newdata(args,fullargs, params, $event, xData);
            },
            keyPSDEField: 'mail_activity',
            majorPSDEField: 'id',
            isLoadDefault: true,
        });
    }

    /**
     * list 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_activityByResListViewBase
     */
    public list_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('list', 'selectionchange', $event);
    }

    /**
     * list 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_activityByResListViewBase
     */
    public list_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('list', 'beforeload', $event);
    }

    /**
     * list 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_activityByResListViewBase
     */
    public list_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('list', 'rowdblclick', $event);
    }

    /**
     * list 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_activityByResListViewBase
     */
    public list_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('list', 'remove', $event);
    }

    /**
     * list 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_activityByResListViewBase
     */
    public list_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('list', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Mail_activityByResListView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.mail_activity;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'mail_activities', parameterName: 'mail_activity' },
            { pathName: 'editview', parameterName: 'editview' },
        ];
        const _this: any = this;
        const openIndexViewTab = (data: any) => {
            const _data: any = { w: (new Date().getTime()) };
            Object.assign(_data, data);
            const routePath = this.$viewTool.buildUpRoutePath(this.$route, tempContext, deResParameters, parameters, args, _data);
            this.$router.push(routePath);
        }
        openIndexViewTab(data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Mail_activityByResListView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
        const localContext: any = null;
        const localViewParam: any =null;
        const data: any = {};
        let tempContext = JSON.parse(JSON.stringify(this.context));
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'mail_activities', parameterName: 'mail_activity' },
            { pathName: 'editview', parameterName: 'editview' },
        ];
        const _this: any = this;
        const openIndexViewTab = (data: any) => {
            const routePath = this.$viewTool.buildUpRoutePath(this.$route, tempContext, deResParameters, parameters, args, data);
            this.$router.push(routePath);
        }
        openIndexViewTab(data);
    }


}