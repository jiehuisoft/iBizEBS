
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { ListViewBase } from '@/studio-core';
import Mail_messageService from '@/service/mail-message/mail-message-service';
import Mail_messageAuthService from '@/authservice/mail-message/mail-message-auth-service';
import ListViewEngine from '@engine/view/list-view-engine';
import Mail_messageUIService from '@/uiservice/mail-message/mail-message-ui-service';
import CodeListService from "@service/app/codelist-service";


/**
 * 消息列表视图视图基类
 *
 * @export
 * @class Mail_messageByResListViewBase
 * @extends {ListViewBase}
 */
export class Mail_messageByResListViewBase extends ListViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageByResListViewBase
     */
    protected appDeName: string = 'mail_message';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageByResListViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageByResListViewBase
     */
    protected appDeMajor: string = 'id';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageByResListViewBase
     */ 
    protected dataControl:string = "list";

    /**
     * 实体服务对象
     *
     * @type {Mail_messageService}
     * @memberof Mail_messageByResListViewBase
     */
    protected appEntityService: Mail_messageService = new Mail_messageService;

    /**
     * 实体权限服务对象
     *
     * @type Mail_messageUIService
     * @memberof Mail_messageByResListViewBase
     */
    public appUIService: Mail_messageUIService = new Mail_messageUIService(this.$store);

	/**
	 * 自定义视图导航参数集合
	 *
     * @protected
	 * @type {*}
	 * @memberof Mail_messageByResListViewBase
	 */
    protected customViewParams: any = {
        'n_model_eq': { isRawValue: false, value: 'n_res_model_eq' },
        'n_res_id_eq': { isRawValue: false, value: 'n_res_id_eq' }
    };

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Mail_messageByResListViewBase
     */
    protected model: any = {
        srfCaption: 'entities.mail_message.views.byreslistview.caption',
        srfTitle: 'entities.mail_message.views.byreslistview.title',
        srfSubTitle: 'entities.mail_message.views.byreslistview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Mail_messageByResListViewBase
     */
    protected containerModel: any = {
        view_list: { name: 'list', type: 'LIST' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageByResListViewBase
     */
	protected viewtag: string = 'b08e5b415c5f01a339a70045e7d3bbd2';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageByResListViewBase
     */ 
    protected viewName:string = "mail_messageByResListView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Mail_messageByResListViewBase
     */
    public engine: ListViewEngine = new ListViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Mail_messageByResListViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Mail_messageByResListViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            list: this.$refs.list,
            opendata: (args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) => {
                this.opendata(args,fullargs, params, $event, xData);
            },
            newdata: (args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) => {
                this.newdata(args,fullargs, params, $event, xData);
            },
            keyPSDEField: 'mail_message',
            majorPSDEField: 'id',
            isLoadDefault: true,
        });
    }

    /**
     * list 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_messageByResListViewBase
     */
    public list_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('list', 'selectionchange', $event);
    }

    /**
     * list 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_messageByResListViewBase
     */
    public list_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('list', 'beforeload', $event);
    }

    /**
     * list 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_messageByResListViewBase
     */
    public list_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('list', 'rowdblclick', $event);
    }

    /**
     * list 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_messageByResListViewBase
     */
    public list_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('list', 'remove', $event);
    }

    /**
     * list 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_messageByResListViewBase
     */
    public list_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('list', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Mail_messageByResListView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Mail_messageByResListView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }


}