import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import Mail_messageService from '@/service/mail-message/mail-message-service';
import Mail_messageAuthService from '@/authservice/mail-message/mail-message-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import Mail_messageUIService from '@/uiservice/mail-message/mail-message-ui-service';

/**
 * 消息数据看板视图视图基类
 *
 * @export
 * @class Mail_messageMainViewBase
 * @extends {DashboardViewBase}
 */
export class Mail_messageMainViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageMainViewBase
     */
    protected appDeName: string = 'mail_message';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageMainViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageMainViewBase
     */
    protected appDeMajor: string = 'id';

    /**
     * 实体服务对象
     *
     * @type {Mail_messageService}
     * @memberof Mail_messageMainViewBase
     */
    protected appEntityService: Mail_messageService = new Mail_messageService;

    /**
     * 实体权限服务对象
     *
     * @type Mail_messageUIService
     * @memberof Mail_messageMainViewBase
     */
    public appUIService: Mail_messageUIService = new Mail_messageUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Mail_messageMainViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Mail_messageMainViewBase
     */
    protected model: any = {
        srfCaption: 'entities.mail_message.views.mainview.caption',
        srfTitle: 'entities.mail_message.views.mainview.title',
        srfSubTitle: 'entities.mail_message.views.mainview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Mail_messageMainViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageMainViewBase
     */
	protected viewtag: string = '47b5eabc445ed79c0fd04d8da748c776';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Mail_messageMainViewBase
     */ 
    protected viewName:string = "mail_messageMainView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Mail_messageMainViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Mail_messageMainViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Mail_messageMainViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'mail_message',
            majorPSDEField: 'id',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Mail_messageMainViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}