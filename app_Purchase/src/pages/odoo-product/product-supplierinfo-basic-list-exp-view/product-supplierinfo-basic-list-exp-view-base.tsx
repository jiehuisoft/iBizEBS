import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { ListExpViewBase } from '@/studio-core';
import Product_supplierinfoService from '@/service/product-supplierinfo/product-supplierinfo-service';
import Product_supplierinfoAuthService from '@/authservice/product-supplierinfo/product-supplierinfo-auth-service';
import ListExpViewEngine from '@engine/view/list-exp-view-engine';
import Product_supplierinfoUIService from '@/uiservice/product-supplierinfo/product-supplierinfo-ui-service';

/**
 * 配置列表导航视图视图基类
 *
 * @export
 * @class Product_supplierinfoBasicListExpViewBase
 * @extends {ListExpViewBase}
 */
export class Product_supplierinfoBasicListExpViewBase extends ListExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
    protected appDeName: string = 'product_supplierinfo';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
    protected appDeMajor: string = 'name_text';

    /**
     * 实体服务对象
     *
     * @type {Product_supplierinfoService}
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
    protected appEntityService: Product_supplierinfoService = new Product_supplierinfoService;

    /**
     * 实体权限服务对象
     *
     * @type Product_supplierinfoUIService
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
    public appUIService: Product_supplierinfoUIService = new Product_supplierinfoUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product_supplierinfo.views.basiclistexpview.caption',
        srfTitle: 'entities.product_supplierinfo.views.basiclistexpview.title',
        srfSubTitle: 'entities.product_supplierinfo.views.basiclistexpview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
    protected containerModel: any = {
        view_listexpbar: { name: 'listexpbar', type: 'LISTEXPBAR' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof Product_supplierinfoBasicListExpView
     */
    public basiclistexpviewlistexpbar_toolbarModels: any = {
        tbitem3: { name: 'tbitem3', caption: '新建', 'isShowCaption': true, 'isShowIcon': true, tooltip: '新建', iconcls: 'fa fa-file-text-o', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'New', target: '', class: '' } },

        tbitem7: {  name: 'tbitem7', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem8: { name: 'tbitem8', caption: '删除', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Remove', target: 'MULTIKEY', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
	protected viewtag: string = '23d299cd56de1e4e8cabd5d5dc155438';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicListExpViewBase
     */ 
    protected viewName:string = "product_supplierinfoBasicListExpView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
    public engine: ListExpViewEngine = new ListExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Product_supplierinfoBasicListExpViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            listexpbar: this.$refs.listexpbar,
            keyPSDEField: 'product_supplierinfo',
            majorPSDEField: 'name_text',
            isLoadDefault: true,
        });
    }

    /**
     * listexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
    public listexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'selectionchange', $event);
    }

    /**
     * listexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
    public listexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'activated', $event);
    }

    /**
     * listexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_supplierinfoBasicListExpViewBase
     */
    public listexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Product_supplierinfoBasicListExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.product_supplierinfo;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        let deResParameters: any[] = [];
        if(tempContext.res_supplier && true){
            deResParameters = [
            { pathName: 'res_suppliers', parameterName: 'res_supplier' },
            ]
        }
        const parameters: any[] = [
            { pathName: 'product_supplierinfos', parameterName: 'product_supplierinfo' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'product-supplierinfo-basic-quick-view', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.product_supplierinfo.views.basicquickview.title'),
            placement: 'DRAWER_RIGHT',
        };
        openDrawer(view, data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Product_supplierinfoBasicListExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }




    /**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_supplierinfoBasicListExpView
     */
    protected viewUID: string = 'odoo-product-product-supplierinfo-basic-list-exp-view';


}