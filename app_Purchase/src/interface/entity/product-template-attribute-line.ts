/**
 * 产品模板属性明细行
 *
 * @export
 * @interface Product_template_attribute_line
 */
export interface Product_template_attribute_line {

    /**
     * 产品属性值
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    product_template_value_ids?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    create_date?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    __last_update?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    display_name?: any;

    /**
     * 属性值
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    value_ids?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    write_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    id?: any;

    /**
     * 产品模板
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    product_tmpl_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    write_uid_text?: any;

    /**
     * 属性
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    attribute_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    create_uid_text?: any;

    /**
     * 产品模板
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    product_tmpl_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    write_uid?: any;

    /**
     * 属性
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    attribute_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Product_template_attribute_line
     */
    create_uid?: any;
}