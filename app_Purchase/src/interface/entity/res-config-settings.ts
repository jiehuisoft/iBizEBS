/**
 * 配置设定
 *
 * @export
 * @interface Res_config_settings
 */
export interface Res_config_settings {

    /**
     * 导入.qif 文件
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_bank_statement_import_qif?: any;

    /**
     * 导入.ofx格式
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_bank_statement_import_ofx?: any;

    /**
     * Google 地图
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    has_google_maps?: any;

    /**
     * 欧盟数字商品增值税
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_l10n_eu_service?: any;

    /**
     * 内容发布网络 (CDN)
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    cdn_activated?: any;

    /**
     * 默认语言代码
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    website_default_lang_code?: any;

    /**
     * 允许在登录页开启密码重置功能
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    auth_signup_reset_password?: any;

    /**
     * 国家/地区分组
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    website_country_group_ids?: any;

    /**
     * Instagram 账号
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    social_instagram?: any;

    /**
     * 采购招标
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_purchase_requisition?: any;

    /**
     * Easypost 接口
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_delivery_easypost?: any;

    /**
     * 折扣
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_discount_per_so_line?: any;

    /**
     * 自动开票
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    automatic_invoice?: any;

    /**
     * Plaid 接口
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_plaid?: any;

    /**
     * Google 客户 ID
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    google_management_client_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    id?: any;

    /**
     * 允许支票打印和存款
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_check_printing?: any;

    /**
     * 自动填充公司数据
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_partner_autocomplete?: any;

    /**
     * 语言数量
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    language_count?: any;

    /**
     * 链接跟踪器
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_website_links?: any;

    /**
     * 联系表单上的技术数据
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    website_form_enable_metadata?: any;

    /**
     * 设置社交平台
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    has_social_network?: any;

    /**
     * 客户地址
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_sale_delivery_address?: any;

    /**
     * 谷歌分析密钥
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    google_analytics_key?: any;

    /**
     * LDAP认证
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_auth_ldap?: any;

    /**
     * 具体用户账号
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    specific_user_account?: any;

    /**
     * 在线发布
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_website_hr_recruitment?: any;

    /**
     * 预测
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_project_forecast?: any;

    /**
     * 寄售
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_stock_tracking_owner?: any;

    /**
     * 允许用户同步Google日历
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_google_calendar?: any;

    /**
     * 开票
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account?: any;

    /**
     * 附加Google文档到记录
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_google_drive?: any;

    /**
     * 自动汇率
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_currency_rate_live?: any;

    /**
     * 形式发票
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_proforma_sales?: any;

    /**
     * FedEx 接口
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_delivery_fedex?: any;

    /**
     * 特定的EMail
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_product_email_template?: any;

    /**
     * 显示效果
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    show_effect?: any;

    /**
     * 拣货策略
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    default_picking_policy?: any;

    /**
     * Youtube账号
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    social_youtube?: any;

    /**
     * 网站公司
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    website_company_id?: any;

    /**
     * USPS 接口
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_delivery_usps?: any;

    /**
     * DHL 接口
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_delivery_dhl?: any;

    /**
     * 使用项目评级
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_project_rating?: any;

    /**
     * Google 地图 API 密钥
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    google_maps_api_key?: any;

    /**
     * 线索
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_use_lead?: any;

    /**
     * 交货包裹
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_stock_tracking_lot?: any;

    /**
     * 多步路由
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_stock_adv_location?: any;

    /**
     * 公司有科目表
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    has_chart_of_accounts?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    write_date?: any;

    /**
     * 保留
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_procurement_jit?: any;

    /**
     * 允许员工通过EMail记录费用
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    use_mailgateway?: any;

    /**
     * 为每个客户使用价格表来适配您的价格
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_sale_pricelist?: any;

    /**
     * 默认销售团队
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    crm_default_team_id?: any;

    /**
     * 储存位置
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_stock_multi_locations?: any;

    /**
     * 默认制造提前期
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    use_manufacturing_lead?: any;

    /**
     * Google 电子表格
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_google_spreadsheet?: any;

    /**
     * 税目汇总表
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    show_line_subtotals_tax_selection?: any;

    /**
     * 语言
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    language_ids?: any;

    /**
     * 电商物流成本
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_website_sale_delivery?: any;

    /**
     * 主生产排程
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_mrp_mps?: any;

    /**
     * 信用不足
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    partner_autocomplete_insufficient_credit?: any;

    /**
     * 显示组织架构图
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_hr_org_chart?: any;

    /**
     * 到期日
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_product_expiry?: any;

    /**
     * bpost 接口
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_delivery_bpost?: any;

    /**
     * 条码扫描器
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_stock_barcode?: any;

    /**
     * GitHub账户
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    social_github?: any;

    /**
     * 国际贸易统计组织
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_intrastat?: any;

    /**
     * 顾客账号
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    auth_signup_uninvited?: any;

    /**
     * 报价单模板
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_sale_order_template?: any;

    /**
     * 使用SEPA直接计入借方
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_sepa_direct_debit?: any;

    /**
     * 默认报价有效期
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    use_quotation_validity_days?: any;

    /**
     * 使用批量付款
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_batch_payment?: any;

    /**
     * Twitter账号
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    social_twitter?: any;

    /**
     * 预算管理
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_budget?: any;

    /**
     * MRP 工单
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_mrp_routings?: any;

    /**
     * 现金舍入
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_cash_rounding?: any;

    /**
     * 到岸成本
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_stock_landed_costs?: any;

    /**
     * 网站名称
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    website_name?: any;

    /**
     * 库存
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_website_sale_stock?: any;

    /**
     * 耿宗并计划
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_website_event_track?: any;

    /**
     * 运输成本
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_delivery?: any;

    /**
     * 自动票据处理
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_invoice_extract?: any;

    /**
     * 网站直播频道
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    channel_id?: any;

    /**
     * 销售订单警告
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_warning_sale?: any;

    /**
     * CDN基本网址
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    cdn_url?: any;

    /**
     * 条码
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_event_barcode?: any;

    /**
     * 别名域
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    alias_domain?: any;

    /**
     * 领英账号
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    social_linkedin?: any;

    /**
     * 多仓库
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_stock_multi_warehouses?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    salesperson_id?: any;

    /**
     * 动态报告
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_reports?: any;

    /**
     * 显示产品的价目表
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_product_pricelist?: any;

    /**
     * 技能管理
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_hr_skills?: any;

    /**
     * A / B测试
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_website_version?: any;

    /**
     * 允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_base_import?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    __last_update?: any;

    /**
     * 以 CSV 格式导入
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_bank_statement_import_csv?: any;

    /**
     * 科目税
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_taxcloud?: any;

    /**
     * 放弃时长
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    cart_abandoned_delay?: any;

    /**
     * 网站域名
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    website_domain?: any;

    /**
     * Accounting
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_accountant?: any;

    /**
     * 毛利
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_sale_margin?: any;

    /**
     * 摘要邮件
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    digest_emails?: any;

    /**
     * 协作pad
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_pad?: any;

    /**
     * 发票警告
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_warning_account?: any;

    /**
     * 贸易条款
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_display_incoterm?: any;

    /**
     * 心愿单
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_website_sale_wishlist?: any;

    /**
     * 默认访问权限
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    user_default_rights?: any;

    /**
     * 账单控制
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    default_purchase_method?: any;

    /**
     * 送货地址
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_delivery_invoice_address?: any;

    /**
     * 显示批次 / 序列号
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_lot_on_delivery_slip?: any;

    /**
     * 入场券
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_event_sale?: any;

    /**
     * 显示含税明细行在汇总表(B2B).
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_show_line_subtotals_tax_included?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    display_name?: any;

    /**
     * 变体和选项
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_product_variant?: any;

    /**
     * SEPA贷记交易
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_sepa?: any;

    /**
     * 多币种
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_multi_currency?: any;

    /**
     * 分析会计
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_analytic_accounting?: any;

    /**
     * 产品包装
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_stock_packaging?: any;

    /**
     * 谷歌文档密钥
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    website_slide_google_app_key?: any;

    /**
     * 采购订单批准
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    po_order_approval?: any;

    /**
     * 销售模块是否已安装
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    is_installed_sale?: any;

    /**
     * 发票在线付款
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_payment?: any;

    /**
     * 分析标签
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_analytic_tags?: any;

    /**
     * Asterisk (开源VoIP平台)
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_voip?: any;

    /**
     * 购物车恢复EMail
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    cart_recovery_mail_template?: any;

    /**
     * 多网站
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_multi_website?: any;

    /**
     * 使用外部验证提供者 (OAuth)
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_auth_oauth?: any;

    /**
     * 送货管理
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    sale_delivery_settings?: any;

    /**
     * 报价单生成器
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_sale_quotation_builder?: any;

    /**
     * 管理公司间交易
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_inter_company_rules?: any;

    /**
     * 销售的安全提前期
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    use_security_lead?: any;

    /**
     * 开票策略
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    default_invoice_policy?: any;

    /**
     * 锁定确认订单
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    lock_confirmed_po?: any;

    /**
     * 重量单位
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    product_weight_in_lbs?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    create_date?: any;

    /**
     * 谷歌分析仪表板
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    has_google_analytics_dashboard?: any;

    /**
     * 批量拣货
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_stock_picking_batch?: any;

    /**
     * 网站
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    website_id?: any;

    /**
     * 默认的费用别名
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    expense_alias_prefix?: any;

    /**
     * 脸书账号
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    social_facebook?: any;

    /**
     * Unsplash图像库
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_web_unsplash?: any;

    /**
     * 群发邮件营销
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_mass_mailing_campaign?: any;

    /**
     * 用 CAMT.053 格式导入
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_bank_statement_import_camt?: any;

    /**
     * 允许产品毛利
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_product_margin?: any;

    /**
     * 子任务
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_subtask_project?: any;

    /**
     * 三方匹配:采购，收货和发票
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_3way_match?: any;

    /**
     * 数字内容
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_website_sale_digital?: any;

    /**
     * 优惠券和促销
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_sale_coupon?: any;

    /**
     * 在取消订阅页面上显示黑名单按钮
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    show_blacklist_buttons?: any;

    /**
     * 库存警报
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_warning_stock?: any;

    /**
     * 员工 PIN
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_attendance_use_pin?: any;

    /**
     * 销售团队
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    salesteam_id?: any;

    /**
     * 图标
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    favicon?: any;

    /**
     * 产品比较工具
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_website_sale_comparison?: any;

    /**
     * 可用阈值
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    available_threshold?: any;

    /**
     * 安全交货时间
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    use_po_lead?: any;

    /**
     * 财年
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_fiscal_year?: any;

    /**
     * 任务日志
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_hr_timesheet?: any;

    /**
     * 产品生命周期管理 (PLM)
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_mrp_plm?: any;

    /**
     * 银行接口－自动同步银行费用
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_account_yodlee?: any;

    /**
     * CDN筛选
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    cdn_filters?: any;

    /**
     * UPS 接口
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_delivery_ups?: any;

    /**
     * 失败的邮件
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    fail_counter?: any;

    /**
     * 批次和序列号
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_stock_production_lot?: any;

    /**
     * 有会计分录
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    has_accounting_entries?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_uom?: any;

    /**
     * 指定邮件服务器
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    mass_mailing_outgoing_mail_server?: any;

    /**
     * 默认线索别名
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    crm_alias_prefix?: any;

    /**
     * Google+账户
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    social_googleplus?: any;

    /**
     * Google 客户端密钥
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    google_management_client_secret?: any;

    /**
     * 默认社交分享图片
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    social_default_image?: any;

    /**
     * Google 分析
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    has_google_analytics?: any;

    /**
     * 调查登记
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_website_event_questions?: any;

    /**
     * 默认语言
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    website_default_lang_id?: any;

    /**
     * 库存可用性
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    inventory_availability?: any;

    /**
     * 采购警告
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_warning_purchase?: any;

    /**
     * 质量
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_quality_control?: any;

    /**
     * 手动分配EMail
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    generate_lead_from_alias?: any;

    /**
     * 外部邮件服务器
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    external_email_server_default?: any;

    /**
     * 访问秘钥
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    unsplash_access_key?: any;

    /**
     * 用Gengo翻译您的网站
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_base_gengo?: any;

    /**
     * 在线票务
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_website_event_sale?: any;

    /**
     * 默认销售人员
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    crm_default_user_id?: any;

    /**
     * 代发货
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_stock_dropshipping?: any;

    /**
     * 邮件服务器
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    mass_mailing_mail_server_id?: any;

    /**
     * 明细行汇总含税(B2B).
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    group_show_line_subtotals_tax_excluded?: any;

    /**
     * 面试表单
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_hr_recruitment_survey?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_mrp_workorder?: any;

    /**
     * 集成卡支付
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    module_pos_mercury?: any;

    /**
     * 公司货币
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    company_currency_id?: any;

    /**
     * 现金收付制
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    tax_exigibility?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    write_uid_text?: any;

    /**
     * 银行核销阈值
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    account_bank_reconciliation_start?: any;

    /**
     * 税率现金收付制日记账
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    tax_cash_basis_journal_id?: any;

    /**
     * 采购提前时间
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    po_lead?: any;

    /**
     * 彩色打印
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    snailmail_color?: any;

    /**
     * 纸张格式
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    paperformat_id?: any;

    /**
     * 在线签名
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    portal_confirmation_sign?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    currency_id?: any;

    /**
     * 摘要邮件
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    digest_id_text?: any;

    /**
     * 用作通过注册创建的新用户的模版
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    auth_signup_template_user_id_text?: any;

    /**
     * 打印
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    invoice_is_print?: any;

    /**
     * 押金产品
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    deposit_default_product_id_text?: any;

    /**
     * 公司的上班时间
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    resource_calendar_id?: any;

    /**
     * 采购订单修改 *
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    po_lock?: any;

    /**
     * 透过邮递
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    invoice_is_snailmail?: any;

    /**
     * 制造提前期(日)
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    manufacturing_lead?: any;

    /**
     * 审批层级 *
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    po_double_validation?: any;

    /**
     * 自定义报表页脚
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    report_footer?: any;

    /**
     * EMail模板
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    template_id_text?: any;

    /**
     * 发送EMail
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    invoice_is_email?: any;

    /**
     * 在线支付
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    portal_confirmation_pay?: any;

    /**
     * 显示SEPA QR码
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    qr_code?: any;

    /**
     * 双面打印
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    snailmail_duplex?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    create_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    company_id_text?: any;

    /**
     * 安全时间
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    security_lead?: any;

    /**
     * 文档模板
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    external_report_layout_id?: any;

    /**
     * 默认进项税
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    purchase_tax_id?: any;

    /**
     * 默认报价有效期（日）
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    quotation_validity_days?: any;

    /**
     * 税率计算的舍入方法
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    tax_calculation_rounding_method?: any;

    /**
     * 汇兑损益
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    currency_exchange_journal_id?: any;

    /**
     * 最小金额
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    po_double_validation_amount?: any;

    /**
     * 模板
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    chart_template_id_text?: any;

    /**
     * 默认销售税
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    sale_tax_id?: any;

    /**
     * 默认模板
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    default_sale_order_template_id_text?: any;

    /**
     * 默认模板
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    default_sale_order_template_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    company_id?: any;

    /**
     * 摘要邮件
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    digest_id?: any;

    /**
     * EMail模板
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    template_id?: any;

    /**
     * 模板
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    chart_template_id?: any;

    /**
     * 押金产品
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    deposit_default_product_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    create_uid?: any;

    /**
     * 用作通过注册创建的新用户的模版
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    auth_signup_template_user_id?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_config_settings
     */
    write_uid?: any;
}