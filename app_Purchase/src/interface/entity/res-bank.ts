/**
 * 银行
 *
 * @export
 * @interface Res_bank
 */
export interface Res_bank {

    /**
     * 街道 2
     *
     * @returns {*}
     * @memberof Res_bank
     */
    street2?: any;

    /**
     * 电话
     *
     * @returns {*}
     * @memberof Res_bank
     */
    phone?: any;

    /**
     * 城市
     *
     * @returns {*}
     * @memberof Res_bank
     */
    city?: any;

    /**
     * 街道
     *
     * @returns {*}
     * @memberof Res_bank
     */
    street?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Res_bank
     */
    name?: any;

    /**
     * EMail
     *
     * @returns {*}
     * @memberof Res_bank
     */
    email?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Res_bank
     */
    active?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Res_bank
     */
    display_name?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Res_bank
     */
    __last_update?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_bank
     */
    id?: any;

    /**
     * 银行识别代码
     *
     * @returns {*}
     * @memberof Res_bank
     */
    bic?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Res_bank
     */
    create_date?: any;

    /**
     * 邮政编码
     *
     * @returns {*}
     * @memberof Res_bank
     */
    zip?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Res_bank
     */
    write_date?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_bank
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_bank
     */
    create_uid_text?: any;

    /**
     * 国家/地区
     *
     * @returns {*}
     * @memberof Res_bank
     */
    country_text?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Res_bank
     */
    state_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_bank
     */
    create_uid?: any;

    /**
     * 国家/地区
     *
     * @returns {*}
     * @memberof Res_bank
     */
    country?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Res_bank
     */
    state?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_bank
     */
    write_uid?: any;
}