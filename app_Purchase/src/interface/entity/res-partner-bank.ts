/**
 * 银行账户
 *
 * @export
 * @interface Res_partner_bank
 */
export interface Res_partner_bank {

    /**
     * 核对银行账号
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    sanitized_acc_number?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    display_name?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    sequence?: any;

    /**
     * 会计日记账
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    journal_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    id?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    create_date?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    __last_update?: any;

    /**
     * 账户号码
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    acc_number?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    acc_type?: any;

    /**
     * ‎有所有必需的参数‎
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    qr_code_valid?: any;

    /**
     * 账户持有人名称
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    acc_holder_name?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    write_date?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    bank_name?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    create_uid_text?: any;

    /**
     * 账户持有人
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    partner_id_text?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    currency_id_text?: any;

    /**
     * 银行识别代码
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    bank_bic?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    write_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    company_id_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    write_uid?: any;

    /**
     * 银行
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    bank_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    company_id?: any;

    /**
     * 账户持有人
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    partner_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    create_uid?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Res_partner_bank
     */
    currency_id?: any;
}