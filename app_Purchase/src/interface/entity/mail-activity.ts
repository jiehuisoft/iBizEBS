/**
 * 活动
 *
 * @export
 * @interface Mail_activity
 */
export interface Mail_activity {

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    display_name?: any;

    /**
     * 邮件模板
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    mail_template_ids?: any;

    /**
     * 自动活动
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    automated?: any;

    /**
     * 文档名称
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    res_name?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    state?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    id?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    create_date?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    write_date?: any;

    /**
     * 到期时间
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    date_deadline?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    __last_update?: any;

    /**
     * 摘要
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    summary?: any;

    /**
     * 下一活动可用
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    has_recommended_activities?: any;

    /**
     * 相关文档编号
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    res_id?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    note?: any;

    /**
     * 文档模型
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    res_model_id?: any;

    /**
     * 相关的文档模型
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    res_model?: any;

    /**
     * 分派给
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    user_id_text?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    activity_category?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    activity_type_id_text?: any;

    /**
     * 前一活动类型
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    previous_activity_type_id_text?: any;

    /**
     * 相关便签
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    note_id_text?: any;

    /**
     * 图标
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    icon?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    create_uid_text?: any;

    /**
     * 排版类型
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    activity_decoration?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    write_uid_text?: any;

    /**
     * 自动安排下一个活动
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    force_next?: any;

    /**
     * 推荐的活动类型
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    recommended_activity_type_id_text?: any;

    /**
     * 日历会议
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    calendar_event_id_text?: any;

    /**
     * 推荐的活动类型
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    recommended_activity_type_id?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    activity_type_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    create_uid?: any;

    /**
     * 分派给
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    user_id?: any;

    /**
     * 相关便签
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    note_id?: any;

    /**
     * 前一活动类型
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    previous_activity_type_id?: any;

    /**
     * 日历会议
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    calendar_event_id?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Mail_activity
     */
    write_uid?: any;
}