/**
 * 采购申请行
 *
 * @export
 * @interface Purchase_requisition_line
 */
export interface Purchase_requisition_line {

    /**
     * 数量
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    product_qty?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    __last_update?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    write_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    id?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    create_date?: any;

    /**
     * 安排的日期
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    schedule_date?: any;

    /**
     * 已订购数量
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    qty_ordered?: any;

    /**
     * 价格
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    price_unit?: any;

    /**
     * 公司名称
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    company_name?: any;

    /**
     * 采购申请
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    requisition_name?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    product_uom_name?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    move_dest_name?: any;

    /**
     * 分析账户
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    account_analytic_name?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    write_uname?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    product_name?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    create_uname?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    write_uid?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    product_id?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    product_uom_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    create_uid?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    move_dest_id?: any;

    /**
     * 分析账户
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    account_analytic_id?: any;

    /**
     * 采购申请
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    requisition_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Purchase_requisition_line
     */
    company_id?: any;
}