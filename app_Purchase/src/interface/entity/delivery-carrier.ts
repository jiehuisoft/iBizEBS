/**
 * 送货方式
 *
 * @export
 * @interface Delivery_carrier
 */
export interface Delivery_carrier {

    /**
     * 是否公开
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    is_published?: any;

    /**
     * 固定价格
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    fixed_price?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    delivery_type?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    write_date?: any;

    /**
     * 交易退货标签
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    return_label_on_delivery?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    name?: any;

    /**
     * 发票原则
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    invoice_policy?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    create_date?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    sequence?: any;

    /**
     * 利润率
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    margin?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    id?: any;

    /**
     * 金额
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    amount?: any;

    /**
     * 调试记录
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    debug_logging?: any;

    /**
     * 邮编到
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    zip_to?: any;

    /**
     * 邮编从
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    zip_from?: any;

    /**
     * 如果订货量大则免费
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    free_over?: any;

    /**
     * 集成级别
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    integration_level?: any;

    /**
     * 生产环境
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    prod_environment?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    active?: any;

    /**
     * 从门户获取返回标签
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    get_return_label_from_portal?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    product_name?: any;

    /**
     * 公司名称
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    company_name?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    write_uname?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    create_uname?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    write_uid?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    company_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    product_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Delivery_carrier
     */
    create_uid?: any;
}