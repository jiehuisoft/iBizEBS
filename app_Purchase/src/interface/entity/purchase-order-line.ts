/**
 * 采购订单行
 *
 * @export
 * @interface Purchase_order_line
 */
export interface Purchase_order_line {

    /**
     * 单价
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    price_unit?: any;

    /**
     * 已接收数量
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    qty_received?: any;

    /**
     * 总计
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    price_total?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    name?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    product_qty?: any;

    /**
     * 税率
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    price_tax?: any;

    /**
     * 保留
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    move_ids?: any;

    /**
     * 小计
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    price_subtotal?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    __last_update?: any;

    /**
     * 序列
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    sequence?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    display_name?: any;

    /**
     * 数量总计
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    product_uom_qty?: any;

    /**
     * 税率
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    taxes_id?: any;

    /**
     * 账单明细行
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    invoice_lines?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    create_date?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    write_date?: any;

    /**
     * 计划日期
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    date_planned?: any;

    /**
     * 开票数量
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    qty_invoiced?: any;

    /**
     * 分析标签
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    analytic_tag_ids?: any;

    /**
     * 下游移动
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    move_dest_ids?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    id?: any;

    /**
     * 业务伙伴
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    partner_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    create_uid_text?: any;

    /**
     * 订货点
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    orderpoint_id_text?: any;

    /**
     * 销售订单
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    sale_order_id_text?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    product_id_text?: any;

    /**
     * 产品类型
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    product_type?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    write_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    company_id_text?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    state?: any;

    /**
     * 分析账户
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    account_analytic_id_text?: any;

    /**
     * 产品图片
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    product_image?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    product_uom_text?: any;

    /**
     * 订单关联
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    order_id_text?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    currency_id_text?: any;

    /**
     * 原销售项
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    sale_line_id_text?: any;

    /**
     * 计量单位类别
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    product_uom_category_id?: any;

    /**
     * 单据日期
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    date_order?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    company_id?: any;

    /**
     * 分析账户
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    account_analytic_id?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    currency_id?: any;

    /**
     * 销售订单
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    sale_order_id?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    product_uom?: any;

    /**
     * 原销售项
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    sale_line_id?: any;

    /**
     * 订单关联
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    order_id?: any;

    /**
     * 订货点
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    orderpoint_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    create_uid?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    write_uid?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    product_id?: any;

    /**
     * 业务伙伴
     *
     * @returns {*}
     * @memberof Purchase_order_line
     */
    partner_id?: any;
}