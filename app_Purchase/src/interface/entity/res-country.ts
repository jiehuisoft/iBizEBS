/**
 * 国家/地区
 *
 * @export
 * @interface Res_country
 */
export interface Res_country {

    /**
     * 输入视图
     *
     * @returns {*}
     * @memberof Res_country
     */
    address_view_id?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Res_country
     */
    display_name?: any;

    /**
     * 图像
     *
     * @returns {*}
     * @memberof Res_country
     */
    image?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Res_country
     */
    __last_update?: any;

    /**
     * 国家/地区分组
     *
     * @returns {*}
     * @memberof Res_country
     */
    country_group_ids?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Res_country
     */
    write_date?: any;

    /**
     * 省份
     *
     * @returns {*}
     * @memberof Res_country
     */
    state_ids?: any;

    /**
     * 客户姓名位置
     *
     * @returns {*}
     * @memberof Res_country
     */
    name_position?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Res_country
     */
    create_date?: any;

    /**
     * 国家/地区长途区号
     *
     * @returns {*}
     * @memberof Res_country
     */
    phone_code?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_country
     */
    id?: any;

    /**
     * 国家/地区代码
     *
     * @returns {*}
     * @memberof Res_country
     */
    code?: any;

    /**
     * 增值税标签
     *
     * @returns {*}
     * @memberof Res_country
     */
    vat_label?: any;

    /**
     * 国家/地区名称
     *
     * @returns {*}
     * @memberof Res_country
     */
    name?: any;

    /**
     * 报表布局
     *
     * @returns {*}
     * @memberof Res_country
     */
    address_format?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_country
     */
    create_uid_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_country
     */
    write_uid_text?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Res_country
     */
    currency_id_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_country
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_country
     */
    create_uid?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Res_country
     */
    currency_id?: any;
}