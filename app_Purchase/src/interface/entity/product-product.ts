/**
 * 产品
 *
 * @export
 * @interface Product_product
 */
export interface Product_product {

    /**
     * 变种卖家
     *
     * @returns {*}
     * @memberof Product_product
     */
    variant_seller_ids?: any;

    /**
     * 模板属性值
     *
     * @returns {*}
     * @memberof Product_product
     */
    product_template_attribute_value_ids?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Product_product
     */
    product_variant_ids?: any;

    /**
     * 小尺寸图像
     *
     * @returns {*}
     * @memberof Product_product
     */
    image_small?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Product_product
     */
    message_unread?: any;

    /**
     * 体积
     *
     * @returns {*}
     * @memberof Product_product
     */
    volume?: any;

    /**
     * 标价
     *
     * @returns {*}
     * @memberof Product_product
     */
    lst_price?: any;

    /**
     * 有效的产品属性
     *
     * @returns {*}
     * @memberof Product_product
     */
    valid_product_attribute_ids?: any;

    /**
     * 库存FIFO手工凭证
     *
     * @returns {*}
     * @memberof Product_product
     */
    stock_fifo_manual_move_ids?: any;

    /**
     * 即时库存
     *
     * @returns {*}
     * @memberof Product_product
     */
    stock_quant_ids?: any;

    /**
     * 进项税
     *
     * @returns {*}
     * @memberof Product_product
     */
    supplier_taxes_id?: any;

    /**
     * 价格表明细
     *
     * @returns {*}
     * @memberof Product_product
     */
    pricelist_item_ids?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Product_product
     */
    __last_update?: any;

    /**
     * 附件产品
     *
     * @returns {*}
     * @memberof Product_product
     */
    accessory_product_ids?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof Product_product
     */
    seller_ids?: any;

    /**
     * Valid Product Attribute Values Without No Variant Attributes
     *
     * @returns {*}
     * @memberof Product_product
     */
    valid_product_attribute_value_wnva_ids?: any;

    /**
     * 客户单号
     *
     * @returns {*}
     * @memberof Product_product
     */
    partner_ref?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof Product_product
     */
    product_image_ids?: any;

    /**
     * 已生产
     *
     * @returns {*}
     * @memberof Product_product
     */
    mrp_product_qty?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Product_product
     */
    write_date?: any;

    /**
     * Valid Product Attribute Lines
     *
     * @returns {*}
     * @memberof Product_product
     */
    valid_product_template_attribute_line_ids?: any;

    /**
     * 下一活动类型
     *
     * @returns {*}
     * @memberof Product_product
     */
    activity_type_id?: any;

    /**
     * 网站产品目录
     *
     * @returns {*}
     * @memberof Product_product
     */
    public_categ_ids?: any;

    /**
     * 活动入场券
     *
     * @returns {*}
     * @memberof Product_product
     */
    event_ticket_ids?: any;

    /**
     * 价格
     *
     * @returns {*}
     * @memberof Product_product
     */
    price?: any;

    /**
     * 产品属性
     *
     * @returns {*}
     * @memberof Product_product
     */
    attribute_line_ids?: any;

    /**
     * 预测数量
     *
     * @returns {*}
     * @memberof Product_product
     */
    virtual_available?: any;

    /**
     * 订货规则
     *
     * @returns {*}
     * @memberof Product_product
     */
    nbr_reordering_rules?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Product_product
     */
    active?: any;

    /**
     * 是关注者
     *
     * @returns {*}
     * @memberof Product_product
     */
    message_is_follower?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Product_product
     */
    message_unread_counter?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Product_product
     */
    id?: any;

    /**
     * 网站价格差异
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_price_difference?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Product_product
     */
    message_follower_ids?: any;

    /**
     * 购物车数量
     *
     * @returns {*}
     * @memberof Product_product
     */
    cart_qty?: any;

    /**
     * 网站公开价格
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_public_price?: any;

    /**
     * 评级
     *
     * @returns {*}
     * @memberof Product_product
     */
    rating_ids?: any;

    /**
     * BOM组件
     *
     * @returns {*}
     * @memberof Product_product
     */
    bom_line_ids?: any;

    /**
     * 网站价格
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_price?: any;

    /**
     * 出向
     *
     * @returns {*}
     * @memberof Product_product
     */
    outgoing_qty?: any;

    /**
     * 已售出
     *
     * @returns {*}
     * @memberof Product_product
     */
    sales_count?: any;

    /**
     * Valid Product Attributes Without No Variant Attributes
     *
     * @returns {*}
     * @memberof Product_product
     */
    valid_product_attribute_wnva_ids?: any;

    /**
     * 中等尺寸图像
     *
     * @returns {*}
     * @memberof Product_product
     */
    image_medium?: any;

    /**
     * Valid Existing Variants
     *
     * @returns {*}
     * @memberof Product_product
     */
    valid_existing_variant_ids?: any;

    /**
     * 库存货币价值
     *
     * @returns {*}
     * @memberof Product_product
     */
    stock_value_currency_id?: any;

    /**
     * 值
     *
     * @returns {*}
     * @memberof Product_product
     */
    stock_value?: any;

    /**
     * 样式
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_style_ids?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Product_product
     */
    message_channel_ids?: any;

    /**
     * 重量
     *
     * @returns {*}
     * @memberof Product_product
     */
    weight?: any;

    /**
     * 物料清单
     *
     * @returns {*}
     * @memberof Product_product
     */
    bom_ids?: any;

    /**
     * 错误数
     *
     * @returns {*}
     * @memberof Product_product
     */
    message_has_error_counter?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Product_product
     */
    activity_state?: any;

    /**
     * Valid Product Attribute Lines Without No Variant Attributes
     *
     * @returns {*}
     * @memberof Product_product
     */
    valid_product_template_attribute_line_wnva_ids?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Product_product
     */
    message_main_attachment_id?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Product_product
     */
    message_partner_ids?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Product_product
     */
    create_date?: any;

    /**
     * 有效的产品属性值
     *
     * @returns {*}
     * @memberof Product_product
     */
    valid_product_attribute_value_ids?: any;

    /**
     * 在手数量
     *
     * @returns {*}
     * @memberof Product_product
     */
    qty_available?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Product_product
     */
    message_attachment_count?: any;

    /**
     * 变体图像
     *
     * @returns {*}
     * @memberof Product_product
     */
    image_variant?: any;

    /**
     * 库存移动
     *
     * @returns {*}
     * @memberof Product_product
     */
    stock_move_ids?: any;

    /**
     * 需要采取行动
     *
     * @returns {*}
     * @memberof Product_product
     */
    message_needaction?: any;

    /**
     * 网站信息
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_message_ids?: any;

    /**
     * 库存FIFO实时计价
     *
     * @returns {*}
     * @memberof Product_product
     */
    stock_fifo_real_time_aml_ids?: any;

    /**
     * 下一活动截止日期
     *
     * @returns {*}
     * @memberof Product_product
     */
    activity_date_deadline?: any;

    /**
     * 参照
     *
     * @returns {*}
     * @memberof Product_product
     */
    code?: any;

    /**
     * 重订货最小数量
     *
     * @returns {*}
     * @memberof Product_product
     */
    reordering_min_qty?: any;

    /**
     * 大尺寸图像
     *
     * @returns {*}
     * @memberof Product_product
     */
    image?: any;

    /**
     * 路线
     *
     * @returns {*}
     * @memberof Product_product
     */
    route_ids?: any;

    /**
     * 销项税
     *
     * @returns {*}
     * @memberof Product_product
     */
    taxes_id?: any;

    /**
     * # 物料清单
     *
     * @returns {*}
     * @memberof Product_product
     */
    bom_count?: any;

    /**
     * 动作数量
     *
     * @returns {*}
     * @memberof Product_product
     */
    message_needaction_counter?: any;

    /**
     * 产品包裹
     *
     * @returns {*}
     * @memberof Product_product
     */
    packaging_ids?: any;

    /**
     * Valid Archived Variants
     *
     * @returns {*}
     * @memberof Product_product
     */
    valid_archived_variant_ids?: any;

    /**
     * 责任用户
     *
     * @returns {*}
     * @memberof Product_product
     */
    activity_user_id?: any;

    /**
     * 价格表项目
     *
     * @returns {*}
     * @memberof Product_product
     */
    item_ids?: any;

    /**
     * 已采购
     *
     * @returns {*}
     * @memberof Product_product
     */
    purchased_product_qty?: any;

    /**
     * 重订货最大数量
     *
     * @returns {*}
     * @memberof Product_product
     */
    reordering_max_qty?: any;

    /**
     * 最小库存规则
     *
     * @returns {*}
     * @memberof Product_product
     */
    orderpoint_ids?: any;

    /**
     * 可选产品
     *
     * @returns {*}
     * @memberof Product_product
     */
    optional_product_ids?: any;

    /**
     * 是产品变体
     *
     * @returns {*}
     * @memberof Product_product
     */
    is_product_variant?: any;

    /**
     * # BOM 使用的地方
     *
     * @returns {*}
     * @memberof Product_product
     */
    used_in_bom_count?: any;

    /**
     * 数量
     *
     * @returns {*}
     * @memberof Product_product
     */
    qty_at_date?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Product_product
     */
    message_has_error?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Product_product
     */
    activity_ids?: any;

    /**
     * 消息
     *
     * @returns {*}
     * @memberof Product_product
     */
    message_ids?: any;

    /**
     * 条码
     *
     * @returns {*}
     * @memberof Product_product
     */
    barcode?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Product_product
     */
    display_name?: any;

    /**
     * 成本
     *
     * @returns {*}
     * @memberof Product_product
     */
    standard_price?: any;

    /**
     * 属性值
     *
     * @returns {*}
     * @memberof Product_product
     */
    attribute_value_ids?: any;

    /**
     * 变体价格额外
     *
     * @returns {*}
     * @memberof Product_product
     */
    price_extra?: any;

    /**
     * BOM产品变体.
     *
     * @returns {*}
     * @memberof Product_product
     */
    variant_bom_ids?: any;

    /**
     * 替代产品
     *
     * @returns {*}
     * @memberof Product_product
     */
    alternative_product_ids?: any;

    /**
     * 内部参考
     *
     * @returns {*}
     * @memberof Product_product
     */
    default_code?: any;

    /**
     * 类别路线
     *
     * @returns {*}
     * @memberof Product_product
     */
    route_from_categ_ids?: any;

    /**
     * 下一活动摘要
     *
     * @returns {*}
     * @memberof Product_product
     */
    activity_summary?: any;

    /**
     * 入库
     *
     * @returns {*}
     * @memberof Product_product
     */
    incoming_qty?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Product_product
     */
    currency_id?: any;

    /**
     * 追踪
     *
     * @returns {*}
     * @memberof Product_product
     */
    tracking?: any;

    /**
     * 拣货说明
     *
     * @returns {*}
     * @memberof Product_product
     */
    description_picking?: any;

    /**
     * 库存出货科目
     *
     * @returns {*}
     * @memberof Product_product
     */
    property_stock_account_output?: any;

    /**
     * 销售
     *
     * @returns {*}
     * @memberof Product_product
     */
    sale_ok?: any;

    /**
     * 网站的说明
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_description?: any;

    /**
     * 网站opengraph图像
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_meta_og_img?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Product_product
     */
    company_id?: any;

    /**
     * 称重
     *
     * @returns {*}
     * @memberof Product_product
     */
    to_weight?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Product_product
     */
    description?: any;

    /**
     * 收货说明
     *
     * @returns {*}
     * @memberof Product_product
     */
    description_pickingin?: any;

    /**
     * 销售价格
     *
     * @returns {*}
     * @memberof Product_product
     */
    list_price?: any;

    /**
     * 隐藏费用政策
     *
     * @returns {*}
     * @memberof Product_product
     */
    hide_expense_policy?: any;

    /**
     * 销售说明
     *
     * @returns {*}
     * @memberof Product_product
     */
    description_sale?: any;

    /**
     * 成本方法
     *
     * @returns {*}
     * @memberof Product_product
     */
    cost_method?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Product_product
     */
    sequence?: any;

    /**
     * 销售订单行消息
     *
     * @returns {*}
     * @memberof Product_product
     */
    sale_line_warn_msg?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof Product_product
     */
    warehouse_id?: any;

    /**
     * 出租
     *
     * @returns {*}
     * @memberof Product_product
     */
    rental?: any;

    /**
     * 价格差异科目
     *
     * @returns {*}
     * @memberof Product_product
     */
    property_account_creditor_price_difference?: any;

    /**
     * 重量计量单位标签
     *
     * @returns {*}
     * @memberof Product_product
     */
    weight_uom_name?: any;

    /**
     * 成本币种
     *
     * @returns {*}
     * @memberof Product_product
     */
    cost_currency_id?: any;

    /**
     * 库存进货科目
     *
     * @returns {*}
     * @memberof Product_product
     */
    property_stock_account_input?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Product_product
     */
    name?: any;

    /**
     * 制造提前期(日)
     *
     * @returns {*}
     * @memberof Product_product
     */
    produce_delay?: any;

    /**
     * SEO优化
     *
     * @returns {*}
     * @memberof Product_product
     */
    is_seo_optimized?: any;

    /**
     * 网站网址
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_url?: any;

    /**
     * 最新反馈评级
     *
     * @returns {*}
     * @memberof Product_product
     */
    rating_last_feedback?: any;

    /**
     * 尺寸 Y
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_size_y?: any;

    /**
     * 是一张活动票吗？
     *
     * @returns {*}
     * @memberof Product_product
     */
    event_ok?: any;

    /**
     * 库存可用性
     *
     * @returns {*}
     * @memberof Product_product
     */
    inventory_availability?: any;

    /**
     * 采购
     *
     * @returns {*}
     * @memberof Product_product
     */
    purchase_ok?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Product_product
     */
    create_uid_text?: any;

    /**
     * 最新值评级
     *
     * @returns {*}
     * @memberof Product_product
     */
    rating_last_value?: any;

    /**
     * 网站meta标题
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_meta_title?: any;

    /**
     * 最新图像评级
     *
     * @returns {*}
     * @memberof Product_product
     */
    rating_last_image?: any;

    /**
     * 采购说明
     *
     * @returns {*}
     * @memberof Product_product
     */
    description_purchase?: any;

    /**
     * 网站
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_id?: any;

    /**
     * 报销
     *
     * @returns {*}
     * @memberof Product_product
     */
    can_be_expensed?: any;

    /**
     * 销售订单行
     *
     * @returns {*}
     * @memberof Product_product
     */
    sale_line_warn?: any;

    /**
     * 尺寸 X
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_size_x?: any;

    /**
     * 自动采购
     *
     * @returns {*}
     * @memberof Product_product
     */
    service_to_purchase?: any;

    /**
     * 网站序列
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_sequence?: any;

    /**
     * 库存位置
     *
     * @returns {*}
     * @memberof Product_product
     */
    property_stock_inventory?: any;

    /**
     * 地点
     *
     * @returns {*}
     * @memberof Product_product
     */
    location_id?: any;

    /**
     * 库存计价
     *
     * @returns {*}
     * @memberof Product_product
     */
    property_valuation?: any;

    /**
     * 已发布
     *
     * @returns {*}
     * @memberof Product_product
     */
    is_published?: any;

    /**
     * 重开收据规则
     *
     * @returns {*}
     * @memberof Product_product
     */
    expense_policy?: any;

    /**
     * 测量的重量单位
     *
     * @returns {*}
     * @memberof Product_product
     */
    weight_uom_id?: any;

    /**
     * 颜色索引
     *
     * @returns {*}
     * @memberof Product_product
     */
    color?: any;

    /**
     * 生产位置
     *
     * @returns {*}
     * @memberof Product_product
     */
    property_stock_production?: any;

    /**
     * 在当前网站显示
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_published?: any;

    /**
     * 网站meta关键词
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_meta_keywords?: any;

    /**
     * 出库单说明
     *
     * @returns {*}
     * @memberof Product_product
     */
    description_pickingout?: any;

    /**
     * 价格表
     *
     * @returns {*}
     * @memberof Product_product
     */
    pricelist_id?: any;

    /**
     * 评级数
     *
     * @returns {*}
     * @memberof Product_product
     */
    rating_count?: any;

    /**
     * 网站元说明
     *
     * @returns {*}
     * @memberof Product_product
     */
    website_meta_description?: any;

    /**
     * 计价
     *
     * @returns {*}
     * @memberof Product_product
     */
    valuation?: any;

    /**
     * 开票策略
     *
     * @returns {*}
     * @memberof Product_product
     */
    invoice_policy?: any;

    /**
     * 采购订单明细的消息
     *
     * @returns {*}
     * @memberof Product_product
     */
    purchase_line_warn_msg?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Product_product
     */
    write_uid_text?: any;

    /**
     * 收入科目
     *
     * @returns {*}
     * @memberof Product_product
     */
    property_account_income_id?: any;

    /**
     * 成本方法
     *
     * @returns {*}
     * @memberof Product_product
     */
    property_cost_method?: any;

    /**
     * 产品种类
     *
     * @returns {*}
     * @memberof Product_product
     */
    categ_id?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof Product_product
     */
    uom_id?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Product_product
     */
    product_variant_id?: any;

    /**
     * 产品类型
     *
     * @returns {*}
     * @memberof Product_product
     */
    type?: any;

    /**
     * 控制策略
     *
     * @returns {*}
     * @memberof Product_product
     */
    purchase_method?: any;

    /**
     * 跟踪服务
     *
     * @returns {*}
     * @memberof Product_product
     */
    service_type?: any;

    /**
     * 单位名称
     *
     * @returns {*}
     * @memberof Product_product
     */
    uom_name?: any;

    /**
     * 可用阈值
     *
     * @returns {*}
     * @memberof Product_product
     */
    available_threshold?: any;

    /**
     * 采购订单行
     *
     * @returns {*}
     * @memberof Product_product
     */
    purchase_line_warn?: any;

    /**
     * # 产品变体
     *
     * @returns {*}
     * @memberof Product_product
     */
    product_variant_count?: any;

    /**
     * POS类别
     *
     * @returns {*}
     * @memberof Product_product
     */
    pos_categ_id?: any;

    /**
     * 自定义消息
     *
     * @returns {*}
     * @memberof Product_product
     */
    custom_message?: any;

    /**
     * 费用科目
     *
     * @returns {*}
     * @memberof Product_product
     */
    property_account_expense_id?: any;

    /**
     * 客户前置时间
     *
     * @returns {*}
     * @memberof Product_product
     */
    sale_delay?: any;

    /**
     * 采购计量单位
     *
     * @returns {*}
     * @memberof Product_product
     */
    uom_po_id?: any;

    /**
     * POS可用
     *
     * @returns {*}
     * @memberof Product_product
     */
    available_in_pos?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Product_product
     */
    create_uid?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Product_product
     */
    write_uid?: any;

    /**
     * 产品模板
     *
     * @returns {*}
     * @memberof Product_product
     */
    product_tmpl_id?: any;
}