/**
 * 产品模板
 *
 * @export
 * @interface Product_template
 */
export interface Product_template {

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Product_template
     */
    activity_ids?: any;

    /**
     * 样式
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_style_ids?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Product_template
     */
    message_attachment_count?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Product_template
     */
    message_unread?: any;

    /**
     * 网站网址
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_url?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Product_template
     */
    message_main_attachment_id?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Product_template
     */
    __last_update?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Product_template
     */
    message_has_error?: any;

    /**
     * 采购
     *
     * @returns {*}
     * @memberof Product_template
     */
    purchase_ok?: any;

    /**
     * 订货规则
     *
     * @returns {*}
     * @memberof Product_template
     */
    nbr_reordering_rules?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof Product_template
     */
    warehouse_id?: any;

    /**
     * 消息
     *
     * @returns {*}
     * @memberof Product_template
     */
    message_ids?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Product_template
     */
    message_partner_ids?: any;

    /**
     * 下一活动类型
     *
     * @returns {*}
     * @memberof Product_template
     */
    activity_type_id?: any;

    /**
     * 需要采取行动
     *
     * @returns {*}
     * @memberof Product_template
     */
    message_needaction?: any;

    /**
     * 产品包裹
     *
     * @returns {*}
     * @memberof Product_template
     */
    packaging_ids?: any;

    /**
     * 是关注者
     *
     * @returns {*}
     * @memberof Product_template
     */
    message_is_follower?: any;

    /**
     * 最新反馈评级
     *
     * @returns {*}
     * @memberof Product_template
     */
    rating_last_feedback?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Product_template
     */
    currency_id?: any;

    /**
     * Valid Product Attributes Without No Variant Attributes
     *
     * @returns {*}
     * @memberof Product_template
     */
    valid_product_attribute_wnva_ids?: any;

    /**
     * 可用阈值
     *
     * @returns {*}
     * @memberof Product_template
     */
    available_threshold?: any;

    /**
     * 采购订单行
     *
     * @returns {*}
     * @memberof Product_template
     */
    purchase_line_warn?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof Product_template
     */
    seller_ids?: any;

    /**
     * 已生产
     *
     * @returns {*}
     * @memberof Product_template
     */
    mrp_product_qty?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Product_template
     */
    product_variant_id?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Product_template
     */
    name?: any;

    /**
     * 评级数
     *
     * @returns {*}
     * @memberof Product_template
     */
    rating_count?: any;

    /**
     * 成本方法
     *
     * @returns {*}
     * @memberof Product_template
     */
    property_cost_method?: any;

    /**
     * 自动采购
     *
     * @returns {*}
     * @memberof Product_template
     */
    service_to_purchase?: any;

    /**
     * 库存进货科目
     *
     * @returns {*}
     * @memberof Product_template
     */
    property_stock_account_input?: any;

    /**
     * 采购订单明细的消息
     *
     * @returns {*}
     * @memberof Product_template
     */
    purchase_line_warn_msg?: any;

    /**
     * 网站价格差异
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_price_difference?: any;

    /**
     * 网站meta标题
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_meta_title?: any;

    /**
     * 收货说明
     *
     * @returns {*}
     * @memberof Product_template
     */
    description_pickingin?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Product_template
     */
    display_name?: any;

    /**
     * POS类别
     *
     * @returns {*}
     * @memberof Product_template
     */
    pos_categ_id?: any;

    /**
     * 可选产品
     *
     * @returns {*}
     * @memberof Product_template
     */
    optional_product_ids?: any;

    /**
     * 测量的重量单位
     *
     * @returns {*}
     * @memberof Product_template
     */
    weight_uom_id?: any;

    /**
     * 隐藏费用政策
     *
     * @returns {*}
     * @memberof Product_template
     */
    hide_expense_policy?: any;

    /**
     * 动作数量
     *
     * @returns {*}
     * @memberof Product_template
     */
    message_needaction_counter?: any;

    /**
     * 图像
     *
     * @returns {*}
     * @memberof Product_template
     */
    image?: any;

    /**
     * 开票策略
     *
     * @returns {*}
     * @memberof Product_template
     */
    invoice_policy?: any;

    /**
     * 网站价格
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_price?: any;

    /**
     * 产品类型
     *
     * @returns {*}
     * @memberof Product_template
     */
    type?: any;

    /**
     * 有效的产品属性值
     *
     * @returns {*}
     * @memberof Product_template
     */
    valid_product_attribute_value_ids?: any;

    /**
     * Valid Archived Variants
     *
     * @returns {*}
     * @memberof Product_template
     */
    valid_archived_variant_ids?: any;

    /**
     * 重开收据规则
     *
     * @returns {*}
     * @memberof Product_template
     */
    expense_policy?: any;

    /**
     * SEO优化
     *
     * @returns {*}
     * @memberof Product_template
     */
    is_seo_optimized?: any;

    /**
     * 最新值评级
     *
     * @returns {*}
     * @memberof Product_template
     */
    rating_last_value?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Product_template
     */
    id?: any;

    /**
     * 收入科目
     *
     * @returns {*}
     * @memberof Product_template
     */
    property_account_income_id?: any;

    /**
     * 替代产品
     *
     * @returns {*}
     * @memberof Product_template
     */
    alternative_product_ids?: any;

    /**
     * Valid Existing Variants
     *
     * @returns {*}
     * @memberof Product_template
     */
    valid_existing_variant_ids?: any;

    /**
     * # 产品变体
     *
     * @returns {*}
     * @memberof Product_template
     */
    product_variant_count?: any;

    /**
     * 已采购
     *
     * @returns {*}
     * @memberof Product_template
     */
    purchased_product_qty?: any;

    /**
     * Valid Product Attribute Lines
     *
     * @returns {*}
     * @memberof Product_template
     */
    valid_product_template_attribute_line_ids?: any;

    /**
     * 拣货说明
     *
     * @returns {*}
     * @memberof Product_template
     */
    description_picking?: any;

    /**
     * 是一张活动票吗？
     *
     * @returns {*}
     * @memberof Product_template
     */
    event_ok?: any;

    /**
     * 成本
     *
     * @returns {*}
     * @memberof Product_template
     */
    standard_price?: any;

    /**
     * 网站信息
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_message_ids?: any;

    /**
     * BOM组件
     *
     * @returns {*}
     * @memberof Product_template
     */
    bom_line_ids?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Product_template
     */
    create_date?: any;

    /**
     * 最新图像评级
     *
     * @returns {*}
     * @memberof Product_template
     */
    rating_last_image?: any;

    /**
     * 在手数量
     *
     * @returns {*}
     * @memberof Product_template
     */
    qty_available?: any;

    /**
     * 成本方法
     *
     * @returns {*}
     * @memberof Product_template
     */
    cost_method?: any;

    /**
     * 网站opengraph图像
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_meta_og_img?: any;

    /**
     * 网站
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_id?: any;

    /**
     * 网站meta关键词
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_meta_keywords?: any;

    /**
     * 小尺寸图像
     *
     * @returns {*}
     * @memberof Product_template
     */
    image_small?: any;

    /**
     * 价格表
     *
     * @returns {*}
     * @memberof Product_template
     */
    pricelist_id?: any;

    /**
     * 尺寸 X
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_size_x?: any;

    /**
     * 价格
     *
     * @returns {*}
     * @memberof Product_template
     */
    price?: any;

    /**
     * 出租
     *
     * @returns {*}
     * @memberof Product_template
     */
    rental?: any;

    /**
     * 出向
     *
     * @returns {*}
     * @memberof Product_template
     */
    outgoing_qty?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Product_template
     */
    sequence?: any;

    /**
     * 库存出货科目
     *
     * @returns {*}
     * @memberof Product_template
     */
    property_stock_account_output?: any;

    /**
     * 路线
     *
     * @returns {*}
     * @memberof Product_template
     */
    route_ids?: any;

    /**
     * 费用科目
     *
     * @returns {*}
     * @memberof Product_template
     */
    property_account_expense_id?: any;

    /**
     * 已售出
     *
     * @returns {*}
     * @memberof Product_template
     */
    sales_count?: any;

    /**
     * 重订货最小数量
     *
     * @returns {*}
     * @memberof Product_template
     */
    reordering_min_qty?: any;

    /**
     * 称重
     *
     * @returns {*}
     * @memberof Product_template
     */
    to_weight?: any;

    /**
     * Valid Product Attribute Values Without No Variant Attributes
     *
     * @returns {*}
     * @memberof Product_template
     */
    valid_product_attribute_value_wnva_ids?: any;

    /**
     * 价格表项目
     *
     * @returns {*}
     * @memberof Product_template
     */
    item_ids?: any;

    /**
     * 进项税
     *
     * @returns {*}
     * @memberof Product_template
     */
    supplier_taxes_id?: any;

    /**
     * 体积
     *
     * @returns {*}
     * @memberof Product_template
     */
    volume?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Product_template
     */
    description?: any;

    /**
     * 生产位置
     *
     * @returns {*}
     * @memberof Product_template
     */
    property_stock_production?: any;

    /**
     * 责任用户
     *
     * @returns {*}
     * @memberof Product_template
     */
    activity_user_id?: any;

    /**
     * 库存位置
     *
     * @returns {*}
     * @memberof Product_template
     */
    property_stock_inventory?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Product_template
     */
    active?: any;

    /**
     * 销售订单行
     *
     * @returns {*}
     * @memberof Product_template
     */
    sale_line_warn?: any;

    /**
     * 销售价格
     *
     * @returns {*}
     * @memberof Product_template
     */
    list_price?: any;

    /**
     * 网站产品目录
     *
     * @returns {*}
     * @memberof Product_template
     */
    public_categ_ids?: any;

    /**
     * 计价
     *
     * @returns {*}
     * @memberof Product_template
     */
    valuation?: any;

    /**
     * 出库单说明
     *
     * @returns {*}
     * @memberof Product_template
     */
    description_pickingout?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Product_template
     */
    message_unread_counter?: any;

    /**
     * 已发布
     *
     * @returns {*}
     * @memberof Product_template
     */
    is_published?: any;

    /**
     * 颜色索引
     *
     * @returns {*}
     * @memberof Product_template
     */
    color?: any;

    /**
     * 附件产品
     *
     * @returns {*}
     * @memberof Product_template
     */
    accessory_product_ids?: any;

    /**
     * 类别路线
     *
     * @returns {*}
     * @memberof Product_template
     */
    route_from_categ_ids?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Product_template
     */
    write_date?: any;

    /**
     * 网站序列
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_sequence?: any;

    /**
     * 是产品变体
     *
     * @returns {*}
     * @memberof Product_template
     */
    is_product_variant?: any;

    /**
     * 地点
     *
     * @returns {*}
     * @memberof Product_template
     */
    location_id?: any;

    /**
     * 下一活动截止日期
     *
     * @returns {*}
     * @memberof Product_template
     */
    activity_date_deadline?: any;

    /**
     * 入库
     *
     * @returns {*}
     * @memberof Product_template
     */
    incoming_qty?: any;

    /**
     * 评级
     *
     * @returns {*}
     * @memberof Product_template
     */
    rating_ids?: any;

    /**
     * 网站元说明
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_meta_description?: any;

    /**
     * 物料清单
     *
     * @returns {*}
     * @memberof Product_template
     */
    bom_ids?: any;

    /**
     * 销售订单行消息
     *
     * @returns {*}
     * @memberof Product_template
     */
    sale_line_warn_msg?: any;

    /**
     * 控制策略
     *
     * @returns {*}
     * @memberof Product_template
     */
    purchase_method?: any;

    /**
     * 制造提前期(日)
     *
     * @returns {*}
     * @memberof Product_template
     */
    produce_delay?: any;

    /**
     * # 物料清单
     *
     * @returns {*}
     * @memberof Product_template
     */
    bom_count?: any;

    /**
     * 销项税
     *
     * @returns {*}
     * @memberof Product_template
     */
    taxes_id?: any;

    /**
     * 错误数
     *
     * @returns {*}
     * @memberof Product_template
     */
    message_has_error_counter?: any;

    /**
     * 报销
     *
     * @returns {*}
     * @memberof Product_template
     */
    can_be_expensed?: any;

    /**
     * 销售
     *
     * @returns {*}
     * @memberof Product_template
     */
    sale_ok?: any;

    /**
     * 跟踪服务
     *
     * @returns {*}
     * @memberof Product_template
     */
    service_type?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Product_template
     */
    activity_state?: any;

    /**
     * 追踪
     *
     * @returns {*}
     * @memberof Product_template
     */
    tracking?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Product_template
     */
    message_channel_ids?: any;

    /**
     * Valid Product Attribute Lines Without No Variant Attributes
     *
     * @returns {*}
     * @memberof Product_template
     */
    valid_product_template_attribute_line_wnva_ids?: any;

    /**
     * 价格差异科目
     *
     * @returns {*}
     * @memberof Product_template
     */
    property_account_creditor_price_difference?: any;

    /**
     * 库存可用性
     *
     * @returns {*}
     * @memberof Product_template
     */
    inventory_availability?: any;

    /**
     * 尺寸 Y
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_size_y?: any;

    /**
     * 中等尺寸图像
     *
     * @returns {*}
     * @memberof Product_template
     */
    image_medium?: any;

    /**
     * 有效的产品属性
     *
     * @returns {*}
     * @memberof Product_template
     */
    valid_product_attribute_ids?: any;

    /**
     * 公开价格
     *
     * @returns {*}
     * @memberof Product_template
     */
    lst_price?: any;

    /**
     * 自定义消息
     *
     * @returns {*}
     * @memberof Product_template
     */
    custom_message?: any;

    /**
     * POS可用
     *
     * @returns {*}
     * @memberof Product_template
     */
    available_in_pos?: any;

    /**
     * 重量计量单位标签
     *
     * @returns {*}
     * @memberof Product_template
     */
    weight_uom_name?: any;

    /**
     * 成本币种
     *
     * @returns {*}
     * @memberof Product_template
     */
    cost_currency_id?: any;

    /**
     * 产品属性
     *
     * @returns {*}
     * @memberof Product_template
     */
    attribute_line_ids?: any;

    /**
     * 重量
     *
     * @returns {*}
     * @memberof Product_template
     */
    weight?: any;

    /**
     * 预测数量
     *
     * @returns {*}
     * @memberof Product_template
     */
    virtual_available?: any;

    /**
     * 图片
     *
     * @returns {*}
     * @memberof Product_template
     */
    product_image_ids?: any;

    /**
     * # BOM 使用的地方
     *
     * @returns {*}
     * @memberof Product_template
     */
    used_in_bom_count?: any;

    /**
     * 内部参考
     *
     * @returns {*}
     * @memberof Product_template
     */
    default_code?: any;

    /**
     * 条码
     *
     * @returns {*}
     * @memberof Product_template
     */
    barcode?: any;

    /**
     * 下一活动摘要
     *
     * @returns {*}
     * @memberof Product_template
     */
    activity_summary?: any;

    /**
     * 库存计价
     *
     * @returns {*}
     * @memberof Product_template
     */
    property_valuation?: any;

    /**
     * 网站的说明
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_description?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Product_template
     */
    product_variant_ids?: any;

    /**
     * 在当前网站显示
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_published?: any;

    /**
     * 网站公开价格
     *
     * @returns {*}
     * @memberof Product_template
     */
    website_public_price?: any;

    /**
     * 重订货最大数量
     *
     * @returns {*}
     * @memberof Product_template
     */
    reordering_max_qty?: any;

    /**
     * 客户前置时间
     *
     * @returns {*}
     * @memberof Product_template
     */
    sale_delay?: any;

    /**
     * 变种卖家
     *
     * @returns {*}
     * @memberof Product_template
     */
    variant_seller_ids?: any;

    /**
     * 采购说明
     *
     * @returns {*}
     * @memberof Product_template
     */
    description_purchase?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Product_template
     */
    message_follower_ids?: any;

    /**
     * 销售说明
     *
     * @returns {*}
     * @memberof Product_template
     */
    description_sale?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Product_template
     */
    company_id_text?: any;

    /**
     * 采购计量单位
     *
     * @returns {*}
     * @memberof Product_template
     */
    uom_po_id_text?: any;

    /**
     * 单位名称
     *
     * @returns {*}
     * @memberof Product_template
     */
    uom_name?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Product_template
     */
    create_uid_text?: any;

    /**
     * 产品种类
     *
     * @returns {*}
     * @memberof Product_template
     */
    categ_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Product_template
     */
    write_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Product_template
     */
    company_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Product_template
     */
    create_uid?: any;

    /**
     * 产品种类
     *
     * @returns {*}
     * @memberof Product_template
     */
    categ_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Product_template
     */
    write_uid?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof Product_template
     */
    uom_id?: any;

    /**
     * 采购计量单位
     *
     * @returns {*}
     * @memberof Product_template
     */
    uom_po_id?: any;
}